﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Lemonaid_web_api.Models
{
    public class Dal2_v1
    {
        internal static int UpdateCoachProfile(CoachProfileDTO coachProfileDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachProfile2_1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachid = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachid.Direction = ParameterDirection.Input;
                    coachid.Value = coachProfileDto.CoachId;

                    SqlParameter description = cmd.Parameters.Add("@description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = coachProfileDto.Description;

                    SqlParameter facebookUrl = cmd.Parameters.Add("@FacebookURL", SqlDbType.VarChar, 2000);
                    facebookUrl.Direction = ParameterDirection.Input;
                    facebookUrl.Value = coachProfileDto.FacebookURL;

                    SqlParameter twitterUrl = cmd.Parameters.Add("@TwitterURL", SqlDbType.VarChar, 2000);
                    twitterUrl.Direction = ParameterDirection.Input;
                    twitterUrl.Value = coachProfileDto.TwitterURL;

                    SqlParameter instagramUrl = cmd.Parameters.Add("@InstagramURL", SqlDbType.VarChar, 2000);
                    instagramUrl.Direction = ParameterDirection.Input;
                    instagramUrl.Value = coachProfileDto.InstagramURL;

                    SqlParameter youtubeUrl = cmd.Parameters.Add("@YoutubeURL", SqlDbType.VarChar, 2000);
                    youtubeUrl.Direction = ParameterDirection.Input;
                    youtubeUrl.Value = coachProfileDto.YoutubeURL;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = coachProfileDto.CoachName ?? string.Empty;

                    SqlParameter title = cmd.Parameters.Add("@Title", SqlDbType.VarChar, 10);
                    title.Direction = ParameterDirection.Input;
                    title.Value = coachProfileDto.Title ?? string.Empty;

                    SqlParameter phone1 = cmd.Parameters.Add("@Phone1", SqlDbType.VarChar, 20);
                    phone1.Direction = ParameterDirection.Input;
                    phone1.Value = coachProfileDto.Phone1 ?? string.Empty;

                    SqlParameter phone2 = cmd.Parameters.Add("@Phone2", SqlDbType.VarChar, 20);
                    phone2.Direction = ParameterDirection.Input;
                    phone2.Value = coachProfileDto.Phone2 ?? string.Empty;

                    SqlParameter accolades = cmd.Parameters.Add("@Accolades", SqlDbType.VarChar, 4000);
                    accolades.Direction = ParameterDirection.Input;
                    accolades.Value = coachProfileDto.Accolades ?? string.Empty;

                    SqlParameter collegeName = cmd.Parameters.Add("@CollegeName", SqlDbType.VarChar, 200);
                    collegeName.Direction = ParameterDirection.Input;
                    collegeName.Value = coachProfileDto.CollegeName ?? string.Empty;

                    SqlParameter year = cmd.Parameters.Add("@Year", SqlDbType.Int);
                    year.Direction = ParameterDirection.Input;
                    year.Value = coachProfileDto.Year;

                    SqlParameter InstituteName = cmd.Parameters.Add("@InstituteName", SqlDbType.VarChar, 100);
                    InstituteName.Direction = ParameterDirection.Input;
                    InstituteName.Value = coachProfileDto.InstituteName ?? string.Empty;

                    SqlParameter InstituteState = cmd.Parameters.Add("@InstituteState", SqlDbType.VarChar, 100);
                    InstituteState.Direction = ParameterDirection.Input;
                    InstituteState.Value = coachProfileDto.InstituteState ?? string.Empty;

                    SqlParameter InstituteCity = cmd.Parameters.Add("@InstituteCity", SqlDbType.VarChar, 100);
                    InstituteCity.Direction = ParameterDirection.Input;
                    InstituteCity.Value = coachProfileDto.InstituteCity ?? string.Empty; 

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v1/UpdateCoachProfile", "", ex.Message, "Exception", 0);
            }


            return id;
        }


        internal static FamilyFriendsDTO AddfamilyFriends2_v1(FamilyFriendsDTO familyFriendsDTO)
        {
            FamilyFriendsDTO v = new FamilyFriendsDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddfamilyFriends2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = familyFriendsDTO.Name;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    Gender.Direction = ParameterDirection.Input;
                    Gender.Value = familyFriendsDTO.Gender;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = familyFriendsDTO.Emailaddress;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = (familyFriendsDTO.Description == null) ? string.Empty : familyFriendsDTO.Description;

                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = familyFriendsDTO.ProfilePicURL;

                    SqlParameter DOB = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    DOB.Direction = ParameterDirection.Input;
                    DOB.Value = familyFriendsDTO.DOB;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = familyFriendsDTO.FacebookId;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = familyFriendsDTO.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = familyFriendsDTO.Longitude;

                    SqlParameter Phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    Phonenumber.Direction = ParameterDirection.Input;
                    Phonenumber.Value = familyFriendsDTO.Phonenumber;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = (familyFriendsDTO.DeviceType == null) ? string.Empty : familyFriendsDTO.DeviceType;

                    SqlParameter DeviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 50);
                    DeviceId.Direction = ParameterDirection.Input;
                    DeviceId.Value = (familyFriendsDTO.DeviceId == null) ? string.Empty : familyFriendsDTO.DeviceId;

                    SqlParameter StateId = cmd.Parameters.Add("@StateId", SqlDbType.Int);
                    StateId.Direction = ParameterDirection.Input;
                    StateId.Value = familyFriendsDTO.StateId;

                    SqlParameter Almamater = cmd.Parameters.Add("@Almamater", SqlDbType.VarChar, 100);
                    Almamater.Direction = ParameterDirection.Input;
                    Almamater.Value = familyFriendsDTO.Almamater;

                    SqlParameter Sport = cmd.Parameters.Add("@Sport", SqlDbType.VarChar, 100);
                    Sport.Direction = ParameterDirection.Input;
                    Sport.Value = familyFriendsDTO.Sport;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.Phonenumber = (string)myData["Phonenumber"].ToString();
                        v.DeviceId = (string)myData["DeviceId"].ToString();
                        v.Phonenumber = (string)myData["Phonenumber"].ToString();
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        v.Sport = (myData["Sport"] == DBNull.Value) ? string.Empty : (string)myData["Sport"];
                        v.Almamater = (myData["Almamater"] == DBNull.Value) ? string.Empty : (string)myData["Almamater"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/AddfamilyFriends", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }

        //internal static void IBMWatson()
        //{
        //    //String baseURL = "https://gateway.watsonplatform.net/personality-insights/api/v2/profile";
        //    //String username = "47b30ac6-6e0b-48eb-88ad-a37108364ab6";
        //    //String password = "tpEvnb1KVEXJ";

        //    String baseURL = "https://gateway.watsonplatform.net/personality-insights/api/v2/profile";
        //    String username = "088a186a-e746-4ac7-a192-32609744e9ae";
        //    String password = "0lScpkDjGKuQ";

        //    //Input text file here
        //    string inPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bio.txt");
        //    string text =  File.ReadAllText(inPath);
        //    StringBuilder uri = new StringBuilder();
        //    HttpWebRequest profileRequest = (HttpWebRequest)WebRequest.Create(baseURL);
        //    string _auth = string.Format("{0}:{1}", username, password);
        //    string _enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(_auth));
        //    string _cred = string.Format("{0} {1}", "Basic", _enc);
        //    profileRequest.Headers[HttpRequestHeader.Authorization] = _cred;
        //    profileRequest.Accept = "application/json";
        //    profileRequest.ContentType = "text/plain";
        //    byte[] bytes = Encoding.UTF8.GetBytes(text);
        //    profileRequest.Method = "POST";
        //    profileRequest.ContentLength = bytes.Length;
        //    using (Stream requeststream = profileRequest.GetRequestStream())
        //    {
        //        requeststream.Write(bytes, 0, bytes.Length);
        //        requeststream.Close();
        //    }
        //    string response;
        //    string outPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Output.json");
        //    StreamWriter file = new  StreamWriter(outPath);

        //    using (HttpWebResponse webResponse = (HttpWebResponse)profileRequest.GetResponse())
        //    {
        //        using (StreamReader sr = new StreamReader(webResponse.GetResponseStream()))
        //        {
        //            response = sr.ReadToEnd();
        //            //jsonResponse.InnerText = response.ToString();
        //            dynamic formatted = JsonConvert.DeserializeObject(response);
        //            JsonConvert.SerializeObject(formatted, Formatting.Indented);
        //            file.Write(formatted);
        //            Console.Write(formatted);
        //            sr.Close();
        //        }
        //        webResponse.Close();
        //    }
        //}

        internal static List<CoachDTO> GetAllUniversitiesRank(int offset, int max)
        { 
           List<CoachDTO> ranksDTO = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUniversitiesRank]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id  = (myData["Id"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Id"]);
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty :  myData["Name"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : myData["ProfilePicURL"].ToString();
                        v.Rank = (myData["Rank"] == DBNull.Value) ? 0 :Convert.ToInt16(myData["Rank"]);
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["TotalCount"]);
                        v.AreasInterested = myData["AreasInterested"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Emailaddress = myData["Emailaddress"].ToString();
                        v.Description = myData["Description"].ToString();
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = myData["ClassificationName"].ToString();
                        v.State = myData["State"].ToString();
                        v.City = myData["City"].ToString();
                        v.Zip = myData["Zip"].ToString();
                        v.WebsiteURL = myData["WebsiteURL"].ToString();
                        v.FaidURL = myData["FaidURL"].ToString();
                        v.ApplURL = myData["ApplURL"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.EnrollmentNo = myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = myData["ApplicationNo"].ToString();
                        v.AdmissionNo = myData["AdmissionNo"].ToString(); 
                        v.FacebookId = myData["FacebookId"].ToString(); 
                        v.CoverPicURL = myData["CoverPicURL"].ToString(); 
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"]; 
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];  
                        v.AthleticConference = (myData["AthleticConference"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleticConference"]);
                        v.EnrollmentFee = (myData["EnrollmentFee"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentFee"]);
                        v.Type = (myData["Type"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Type"]);
                        v.TuitionFee = (myData["TuitionFee"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionFee"]);
                        v.Scholarship = (myData["Scholarship"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Scholarship"]);
                        v.EnrollmentNo = (myData["EnrollmentNo"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentNo"]);
                        v.ScholarshipMen = (myData["ScholarshipMen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipMen"]);
                        v.ScholarshipWomen = (myData["ScholarshipWomen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipWomen"]);
                        v.TuitionInState = (myData["TuitionInState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionInState"]);
                        v.TuitionOutState = (myData["TuitionOutState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionOutState"]);
                        ranksDTO.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v1/GetAllUniversitiesRank", "", ex.Message, "Exception", 0);
                throw;
            }

            return ranksDTO;
        }


        internal static List<RanksDTO> GetNAIAUniversitiesRank(int offset, int max)
        {
            List<RanksDTO> ranksDTO = new List<RanksDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetNAIAUniversitiesRank]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        RanksDTO v = new RanksDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty : myData["Name"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : myData["ProfilePicURL"].ToString();
                        v.Rank = (myData["Rank"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Rank"]);
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["TotalCount"]);
                        ranksDTO.Add(v);

                    }

                  
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v1/GetNAIAUniversitiesRank", "", ex.Message, "Exception", 0);
                throw;
            }

            return ranksDTO;
        }
    }
}