﻿using Lemonaid_web_api.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Lemonaid_web_api.Models
{
    public class Dal2_v2
    {
        internal static AthleteDTO AddAthlete2_v2(AthleteDTO athleteDto)
        {
            AthleteDTO v = new AthleteDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthlete2_v2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    name.Direction = ParameterDirection.Input;
                    name.Value = athleteDto.Name;

                    SqlParameter gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    gender.Direction = ParameterDirection.Input;
                    gender.Value = athleteDto.Gender;

                    SqlParameter school = cmd.Parameters.Add("@School", SqlDbType.VarChar, 100);
                    school.Direction = ParameterDirection.Input;
                    school.Value = athleteDto.School ?? string.Empty;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = athleteDto.Emailaddress;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = athleteDto.Description ?? string.Empty;

                    SqlParameter gpa = cmd.Parameters.Add("@GPA", SqlDbType.Float);
                    gpa.Direction = ParameterDirection.Input;
                    gpa.Value = athleteDto.GPA;

                    SqlParameter social = cmd.Parameters.Add("@Social", SqlDbType.Int);
                    social.Direction = ParameterDirection.Input;
                    social.Value = athleteDto.Social;

                    SqlParameter sat = cmd.Parameters.Add("@SAT", SqlDbType.Int);
                    sat.Direction = ParameterDirection.Input;
                    sat.Value = athleteDto.SAT;

                    SqlParameter act = cmd.Parameters.Add("@ACT", SqlDbType.Int);
                    act.Direction = ParameterDirection.Input;
                    act.Value = athleteDto.ACT;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = athleteDto.InstituteId;

                    SqlParameter profilePicUrl = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    profilePicUrl.Direction = ParameterDirection.Input;
                    profilePicUrl.Value = athleteDto.ProfilePicURL;

                    SqlParameter dob = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    dob.Direction = ParameterDirection.Input;
                    dob.Value = athleteDto.DOB;

                    SqlParameter address = cmd.Parameters.Add("@Address", SqlDbType.VarChar, 100);
                    address.Direction = ParameterDirection.Input;
                    address.Value = athleteDto.Address;

                    SqlParameter facebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    facebookId.Direction = ParameterDirection.Input;
                    facebookId.Value = athleteDto.FacebookId;

                    SqlParameter latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    latitude.Direction = ParameterDirection.Input;
                    latitude.Value = athleteDto.Latitude;

                    SqlParameter longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    longitude.Direction = ParameterDirection.Input;
                    longitude.Value = athleteDto.Longitude;


                    SqlParameter academicLevel = cmd.Parameters.Add("@AcademicLevel", SqlDbType.Int);
                    academicLevel.Direction = ParameterDirection.Input;
                    academicLevel.Value = athleteDto.AcademicLevel;


                    SqlParameter yearLevel = cmd.Parameters.Add("@YearLevel", SqlDbType.Int);
                    yearLevel.Direction = ParameterDirection.Input;
                    yearLevel.Value = athleteDto.YearLevel;


                    SqlParameter premium = cmd.Parameters.Add("@Premium", SqlDbType.Int);
                    premium.Direction = ParameterDirection.Input;
                    premium.Value = athleteDto.Premium;

                    SqlParameter height1 = cmd.Parameters.Add("@Height1", SqlDbType.VarChar, 20);
                    height1.Direction = ParameterDirection.Input;
                    height1.Value = athleteDto.Height ?? string.Empty;

                    SqlParameter height2 = cmd.Parameters.Add("@Height2", SqlDbType.VarChar, 20);
                    height2.Direction = ParameterDirection.Input;
                    height2.Value = athleteDto.Height2 ?? string.Empty;

                    SqlParameter heightType = cmd.Parameters.Add("@HeightType", SqlDbType.VarChar, 20);
                    heightType.Direction = ParameterDirection.Input;
                    heightType.Value = athleteDto.HeightType ?? string.Empty;

                    SqlParameter weight = cmd.Parameters.Add("@Weight", SqlDbType.VarChar, 20);
                    weight.Direction = ParameterDirection.Input;
                    weight.Value = athleteDto.Weight ?? string.Empty;

                    SqlParameter weightType = cmd.Parameters.Add("@WeightType", SqlDbType.VarChar, 20);
                    weightType.Direction = ParameterDirection.Input;
                    weightType.Value = athleteDto.WeightType ?? string.Empty;

                    SqlParameter wingSpan = cmd.Parameters.Add("@WingSpan", SqlDbType.VarChar, 20);
                    wingSpan.Direction = ParameterDirection.Input;
                    wingSpan.Value = athleteDto.WingSpan ?? string.Empty;

                    SqlParameter wingSpanType = cmd.Parameters.Add("@WingSpanType", SqlDbType.VarChar, 20);
                    wingSpanType.Direction = ParameterDirection.Input;
                    wingSpanType.Value = athleteDto.WingSpanType ?? string.Empty;

                    SqlParameter shoeSize = cmd.Parameters.Add("@ShoeSize", SqlDbType.VarChar, 20);
                    shoeSize.Direction = ParameterDirection.Input;
                    shoeSize.Value = athleteDto.ShoeSize ?? string.Empty;

                    SqlParameter shoeSizeType = cmd.Parameters.Add("@ShoeSizeType", SqlDbType.VarChar, 20);
                    shoeSizeType.Direction = ParameterDirection.Input;
                    shoeSizeType.Value = athleteDto.ShoeSizeType ?? string.Empty;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = string.IsNullOrWhiteSpace(athleteDto.ClubName) ? string.Empty : athleteDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = string.IsNullOrWhiteSpace(athleteDto.CoachName)
                        ? string.Empty
                        : athleteDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = string.IsNullOrWhiteSpace(athleteDto.CoachEmailId)
                        ? string.Empty
                        : athleteDto.CoachEmailId;

                    SqlParameter atheleteType = cmd.Parameters.Add("@AtheleteType", SqlDbType.Int);
                    atheleteType.Direction = ParameterDirection.Input;
                    atheleteType.Value = athleteDto.AtheleteType;

                    SqlParameter active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    active.Direction = ParameterDirection.Input;
                    active.Value = athleteDto.Active;

                    SqlParameter major = cmd.Parameters.Add("@Major", SqlDbType.VarChar, 50);
                    major.Direction = ParameterDirection.Input;
                    major.Value = string.IsNullOrWhiteSpace(athleteDto.Major) ? string.Empty : athleteDto.Major;

                    SqlParameter phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    phonenumber.Direction = ParameterDirection.Input;
                    phonenumber.Value = string.IsNullOrWhiteSpace(athleteDto.Phonenumber)
                        ? string.Empty
                        : athleteDto.Phonenumber;

                    SqlParameter username = cmd.Parameters.Add("@Username", SqlDbType.VarChar, 500);
                    username.Direction = ParameterDirection.Input;
                    username.Value = string.IsNullOrWhiteSpace(athleteDto.UserName) ? string.Empty : athleteDto.UserName;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = (athleteDto.LoginType == null) ? 0 : athleteDto.LoginType;

                    SqlParameter deviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    deviceType.Direction = ParameterDirection.Input;
                    deviceType.Value = (athleteDto.DeviceType == null) ? string.Empty : athleteDto.DeviceType;

                    SqlParameter deviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 500);
                    deviceId.Direction = ParameterDirection.Input;
                    deviceId.Value = (athleteDto.DeviceId == null) ? string.Empty : athleteDto.DeviceId;

                    SqlParameter verticalJump = cmd.Parameters.Add("@VerticalJump", SqlDbType.VarChar, 20);
                    verticalJump.Direction = ParameterDirection.Input;
                    verticalJump.Value = (athleteDto.VerticalJump == null) ? string.Empty : athleteDto.VerticalJump;

                    SqlParameter broadJump = cmd.Parameters.Add("@BroadJump", SqlDbType.VarChar, 20);
                    broadJump.Direction = ParameterDirection.Input;
                    broadJump.Value = (athleteDto.BroadJump == null) ? string.Empty : athleteDto.BroadJump;

                    SqlParameter stateId = cmd.Parameters.Add("@StateId", SqlDbType.Int);
                    stateId.Direction = ParameterDirection.Input;
                    stateId.Value = athleteDto.StateId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (athleteDto.SchoolCoachName == null)
                        ? string.Empty
                        : athleteDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (athleteDto.SchoolCoachEmail == null)
                        ? string.Empty
                        : athleteDto.SchoolCoachEmail;

                    SqlParameter schoolZipCode = cmd.Parameters.Add("@SchoolZipCode", SqlDbType.VarChar, 100);
                    schoolZipCode.Direction = ParameterDirection.Input;
                    schoolZipCode.Value = (athleteDto.SchoolZipCode == null) ? string.Empty : athleteDto.SchoolZipCode;

                    SqlParameter gapYearLevel = cmd.Parameters.Add("@GapYearLevel", SqlDbType.Int);
                    gapYearLevel.Direction = ParameterDirection.Input;
                    gapYearLevel.Value = athleteDto.GapyearLevel;

                    SqlParameter gapYearDesc = cmd.Parameters.Add("@GapyearDescription", SqlDbType.VarChar, 2000);
                    gapYearDesc.Direction = ParameterDirection.Input;
                    gapYearDesc.Value = (athleteDto.GapyearDescription == null)
                        ? string.Empty
                        : athleteDto.GapyearDescription;

                    SqlParameter ap = cmd.Parameters.Add("@AP", SqlDbType.Float);
                    ap.Direction = ParameterDirection.Input;
                    ap.Value = athleteDto.AP;

                    SqlParameter country = cmd.Parameters.Add("@Country", SqlDbType.VarChar, 100);
                    country.Direction = ParameterDirection.Input;
                    country.Value = (athleteDto.Country == null) ? string.Empty : athleteDto.Country;

                    SqlParameter countryId = cmd.Parameters.Add("@CountryId", SqlDbType.Int);
                    countryId.Direction = ParameterDirection.Input;
                    countryId.Value = athleteDto.CountryId;

                    SqlParameter _20YardShuttleRun = cmd.Parameters.Add("@20YardShuttleRun", SqlDbType.VarChar, 50);
                    _20YardShuttleRun.Direction = ParameterDirection.Input;
                    _20YardShuttleRun.Value = (athleteDto._20YardShuttleRun == null)
                        ? string.Empty
                        : athleteDto._20YardShuttleRun;

                    SqlParameter _60YardShuttleRun = cmd.Parameters.Add("@60YardShuttleRun", SqlDbType.VarChar, 50);
                    _60YardShuttleRun.Direction = ParameterDirection.Input;
                    _60YardShuttleRun.Value = (athleteDto._60YardShuttleRun == null)
                        ? string.Empty
                        : athleteDto._60YardShuttleRun;

                    SqlParameter KneelingMedBallToss = cmd.Parameters.Add("@KneelingMedBallToss", SqlDbType.VarChar, 50);
                    KneelingMedBallToss.Direction = ParameterDirection.Input;
                    KneelingMedBallToss.Value = (athleteDto.KneelingMedBallToss == null)
                        ? string.Empty
                        : athleteDto.KneelingMedBallToss;

                    SqlParameter RotationalMedBallToss = cmd.Parameters.Add("@RotationalMedBallToss", SqlDbType.VarChar, 50);
                    RotationalMedBallToss.Direction = ParameterDirection.Input;
                    RotationalMedBallToss.Value = (athleteDto.RotationalMedBallToss == null)
                        ? string.Empty
                        : athleteDto.RotationalMedBallToss;

                    SqlParameter cityId = cmd.Parameters.Add("@CityId", SqlDbType.Int);
                    cityId.Direction = ParameterDirection.Input;
                    cityId.Value = (athleteDto.CityId == null)
                    ? 0
                    : athleteDto.CityId;

                    SqlParameter basicStateIdPar = cmd.Parameters.Add("@BasicStateId", SqlDbType.Int);
                    basicStateIdPar.Direction = ParameterDirection.Input;
                    basicStateIdPar.Value = (athleteDto.BasicStateId == null)
                    ? 0
                    : athleteDto.BasicStateId;  

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    v = Dal2.GetAthlete(myData);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v2/AddAthlete2_v2", "", ex.Message, "Exception", 0);
            }

            return v;
        }


        internal static FamilyFriendsDTO AddfamilyFriends2_v2(FamilyFriendsDTO familyFriendsDTO)
        {
            FamilyFriendsDTO v = new FamilyFriendsDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddfamilyFriends2_v2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = familyFriendsDTO.Name;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    Gender.Direction = ParameterDirection.Input;
                    Gender.Value = familyFriendsDTO.Gender;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = familyFriendsDTO.Emailaddress;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = (familyFriendsDTO.Description == null) ? string.Empty : familyFriendsDTO.Description;

                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = familyFriendsDTO.ProfilePicURL;

                    SqlParameter DOB = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    DOB.Direction = ParameterDirection.Input;
                    DOB.Value = familyFriendsDTO.DOB;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = familyFriendsDTO.FacebookId;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = familyFriendsDTO.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = familyFriendsDTO.Longitude;

                    SqlParameter Phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    Phonenumber.Direction = ParameterDirection.Input;
                    Phonenumber.Value = familyFriendsDTO.Phonenumber;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = (familyFriendsDTO.DeviceType == null) ? string.Empty : familyFriendsDTO.DeviceType;

                    SqlParameter DeviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 50);
                    DeviceId.Direction = ParameterDirection.Input;
                    DeviceId.Value = (familyFriendsDTO.DeviceId == null) ? string.Empty : familyFriendsDTO.DeviceId;

                    SqlParameter StateId = cmd.Parameters.Add("@StateId", SqlDbType.Int);
                    StateId.Direction = ParameterDirection.Input;
                    StateId.Value = familyFriendsDTO.StateId;

                    SqlParameter Almamater = cmd.Parameters.Add("@Almamater", SqlDbType.VarChar, 100);
                    Almamater.Direction = ParameterDirection.Input;
                    Almamater.Value = familyFriendsDTO.Almamater;

                    SqlParameter Sport = cmd.Parameters.Add("@Sport", SqlDbType.VarChar, 100);
                    Sport.Direction = ParameterDirection.Input;
                    Sport.Value = familyFriendsDTO.Sport;

                    SqlParameter LoginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    LoginType.Direction = ParameterDirection.Input;
                    LoginType.Value = (familyFriendsDTO.LoginType == null) ? 0 : Convert.ToInt32(familyFriendsDTO.LoginType);

                    SqlParameter userName = cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100);
                    userName.Direction = ParameterDirection.Input;
                    userName.Value = (familyFriendsDTO.Username == null) ? string.Empty : familyFriendsDTO.Username;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.Phonenumber = (string)myData["Phonenumber"].ToString();
                        v.DeviceId = (string)myData["DeviceId"].ToString();
                        v.Phonenumber = (string)myData["Phonenumber"].ToString();
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        v.Sport = (myData["Sport"] == DBNull.Value) ? string.Empty : (string)myData["Sport"];
                        v.Almamater = (myData["Almamater"] == DBNull.Value) ? string.Empty : (string)myData["Almamater"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : (string)myData["Username"];
                        v.DeviceId = familyFriendsDTO.DeviceId;
                        v.DeviceType = familyFriendsDTO.DeviceType;

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v2/AddfamilyFriends2_v2", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }

        internal static ClubCoachDTO AddClubCoach2_v2(ClubCoachDTO clubCoachDto)
        {
            ClubCoachDTO v = new ClubCoachDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddClubCoach2_V2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    name.Direction = ParameterDirection.Input;
                    name.Value = clubCoachDto.Name;

                    SqlParameter gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    gender.Direction = ParameterDirection.Input;
                    gender.Value = clubCoachDto.Gender;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = clubCoachDto.Emailaddress;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = (clubCoachDto.Description == null) ? string.Empty : clubCoachDto.Description;

                    SqlParameter profilePicUrl = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    profilePicUrl.Direction = ParameterDirection.Input;
                    profilePicUrl.Value = clubCoachDto.ProfilePicURL;

                    SqlParameter dob = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    dob.Direction = ParameterDirection.Input;
                    dob.Value = clubCoachDto.DOB; 

                    SqlParameter facebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    facebookId.Direction = ParameterDirection.Input;
                    facebookId.Value = clubCoachDto.FacebookId;

                    SqlParameter coverPicUrl = cmd.Parameters.Add("@CoverPicURL", SqlDbType.VarChar, 1000);
                    coverPicUrl.Direction = ParameterDirection.Input;
                    coverPicUrl.Value = clubCoachDto.CoverPicURL;

                    SqlParameter latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    latitude.Direction = ParameterDirection.Input;
                    latitude.Value = clubCoachDto.Latitude;

                    SqlParameter longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    longitude.Direction = ParameterDirection.Input;
                    longitude.Value = clubCoachDto.Longitude;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = string.IsNullOrWhiteSpace(clubCoachDto.ClubName)
                        ? string.Empty
                        : clubCoachDto.ClubName;

                    SqlParameter active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    active.Direction = ParameterDirection.Input;
                    active.Value = clubCoachDto.Active;

                    SqlParameter phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    phonenumber.Direction = ParameterDirection.Input;
                    phonenumber.Value = string.IsNullOrWhiteSpace(clubCoachDto.Phonenumber)
                        ? string.Empty
                        : clubCoachDto.Phonenumber;

                    SqlParameter deviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    deviceType.Direction = ParameterDirection.Input;
                    deviceType.Value = (clubCoachDto.DeviceType == null) ? string.Empty : clubCoachDto.DeviceType;

                    SqlParameter deviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 50);
                    deviceId.Direction = ParameterDirection.Input;
                    deviceId.Value = (clubCoachDto.DeviceId == null) ? string.Empty : clubCoachDto.DeviceId;

                    SqlParameter teamType = cmd.Parameters.Add("@TeamType", SqlDbType.Int);
                    teamType.Direction = ParameterDirection.Input;
                    teamType.Value = (clubCoachDto.TeamType == null) ? 0 : Convert.ToInt32(clubCoachDto.TeamType);

                    SqlParameter location = cmd.Parameters.Add("@Location", SqlDbType.VarChar, 200);
                    location.Direction = ParameterDirection.Input;
                    location.Value = (clubCoachDto.Location == null) ? string.Empty : clubCoachDto.Location;

                    SqlParameter schoolName = cmd.Parameters.Add("@SchoolName", SqlDbType.VarChar, 50);
                    schoolName.Direction = ParameterDirection.Input;
                    schoolName.Value = (clubCoachDto.SchoolName == null) ? string.Empty : clubCoachDto.SchoolName;

                    SqlParameter zipCode = cmd.Parameters.Add("@ZipCode", SqlDbType.VarChar, 50);
                    zipCode.Direction = ParameterDirection.Input;
                    zipCode.Value = (clubCoachDto.ZipCode == null) ? string.Empty : clubCoachDto.ZipCode;

                    SqlParameter stateId = cmd.Parameters.Add("@StateId", SqlDbType.Int);
                    stateId.Direction = ParameterDirection.Input;
                    stateId.Value = (clubCoachDto.StateId == null) ? 0 : Convert.ToInt32(clubCoachDto.StateId);

                    SqlParameter schoolType = cmd.Parameters.Add("@SchoolType", SqlDbType.Int);
                    schoolType.Direction = ParameterDirection.Input;
                    schoolType.Value = (clubCoachDto.SchoolType == null) ? 0 : Convert.ToInt32(clubCoachDto.SchoolType);

                    SqlParameter clubPhoneNumber = cmd.Parameters.Add("@ClubPhoneNumber", SqlDbType.VarChar, 50);
                    clubPhoneNumber.Direction = ParameterDirection.Input;
                    clubPhoneNumber.Value = string.IsNullOrWhiteSpace(clubCoachDto.ClubPhoneNumber)
                        ? string.Empty
                        : clubCoachDto.ClubPhoneNumber;

                    SqlParameter schoolId = cmd.Parameters.Add("@SchoolId", SqlDbType.Int);
                    schoolId.Direction = ParameterDirection.Input;
                    schoolId.Value = (clubCoachDto.SchoolId == null) ? 0 : Convert.ToInt32(clubCoachDto.SchoolId);

                    SqlParameter LoginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    LoginType.Direction = ParameterDirection.Input;
                    LoginType.Value = (clubCoachDto.LoginType == null) ? 0 : Convert.ToInt32(clubCoachDto.LoginType);

                    SqlParameter userName = cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100);
                    userName.Direction = ParameterDirection.Input;
                    userName.Value = (clubCoachDto.Username == null) ? string.Empty :  clubCoachDto.Username;

                    SqlParameter Almamater = cmd.Parameters.Add("@Almamater", SqlDbType.VarChar, 100);
                    Almamater.Direction = ParameterDirection.Input;
                    Almamater.Value = (clubCoachDto.Almamater == null) ? string.Empty : clubCoachDto.Almamater;

                    SqlParameter IdPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    IdPar.Direction = ParameterDirection.Input;
                    IdPar.Value = (clubCoachDto.Id == null) ? 0 : Convert.ToInt32(clubCoachDto.Id);

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        //v.ExistedId = (myData["AlreadyExistedId1"] == DBNull.Value) ? 0 : (int)myData["AlreadyExistedId1"];
                        //v.ExistedEmailAddress = (myData["AlreadyExistedEmailaddress1"] == DBNull.Value) ? string.Empty : (string)myData["AlreadyExistedEmailaddress1"];
                        //v.ExistedLoginType = (myData["AlreadyExistedLoginType1"] == DBNull.Value) ? 0 : (int)myData["AlreadyExistedLoginType1"];
                        //v.ExistedUsername = (myData["AlreadyExistedUserName1"] == DBNull.Value) ? string.Empty : (string)myData["AlreadyExistedUserName1"];
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.ClubAddress = string.Empty;
                        v.ClubName = myData["ClubName"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Active = (myData["Active"] != DBNull.Value) && Convert.ToBoolean(myData["Active"]);
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        v.Location = myData["Location"].ToString();
                        v.SchoolName = myData["SchoolName"].ToString();
                        v.ZipCode = myData["ZipCode"].ToString();
                        v.StateId = myData["StateId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = myData["StateName"].ToString();
                        v.SchoolType = myData["SchoolType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolType"]);
                        v.ClubPhoneNumber = (myData["ClubPhoneNumber"] == DBNull.Value) ? string.Empty : myData["ClubPhoneNumber"].ToString();
                        v.SchoolId = myData["SchoolId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolId"]);
                        v.ClubId = (myData["ClubId"] == DBNull.Value) ? 0 : (int)myData["ClubId"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["LoginType"]);
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : (string)myData["Username"];
                        v.DeviceId = (myData["DeviceName"] == DBNull.Value) ? string.Empty : (string)myData["DeviceName"];
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"];
                        v.Almamater = (myData["Almamater"] == DBNull.Value) ? string.Empty : (string)myData["Almamater"];
                     
                    }
                    v.DeviceId = clubCoachDto.DeviceId;
                    v.DeviceType = clubCoachDto.DeviceType;
                  

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V2/AddClubCoach", "", ex.Message, "Exception", 0);
            }

            return v;
        }


        internal static int InsertUniversity(CoachDTO coachDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[InsertInstitution]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = coachDTO.Name;

                    SqlParameter AreasInterested = cmd.Parameters.Add("@AreasInterested", SqlDbType.VarChar, 2000);
                    AreasInterested.Direction = ParameterDirection.Input;
                    AreasInterested.Value = coachDTO.AreasInterested;

                    SqlParameter Address = cmd.Parameters.Add("@Address", SqlDbType.VarChar, 1000);
                    Address.Direction = ParameterDirection.Input;
                    Address.Value = coachDTO.Address;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = coachDTO.Description;

                    SqlParameter YearOfEstablish = cmd.Parameters.Add("@YearOfEstablish", SqlDbType.Int);
                    YearOfEstablish.Direction = ParameterDirection.Input;
                    YearOfEstablish.Value = coachDTO.YearOfEstablish;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = coachDTO.Emailaddress;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = coachDTO.FacebookId;

                    SqlParameter NCAA = cmd.Parameters.Add("@NCAA", SqlDbType.Int);
                    NCAA.Direction = ParameterDirection.Input;
                    NCAA.Value = coachDTO.NCAA;

                    SqlParameter Conference = cmd.Parameters.Add("@Conference", SqlDbType.Int);
                    Conference.Direction = ParameterDirection.Input;
                    Conference.Value = coachDTO.Conference;

                    SqlParameter AdmissionStandard = cmd.Parameters.Add("@AdmissionStandard", SqlDbType.Int);
                    AdmissionStandard.Direction = ParameterDirection.Input;
                    AdmissionStandard.Value = coachDTO.AdmissionStandard;

                    SqlParameter ClassificationName = cmd.Parameters.Add("@ClassificationName", SqlDbType.VarChar, 500);
                    ClassificationName.Direction = ParameterDirection.Input;
                    ClassificationName.Value = coachDTO.ClassificationName;

                    SqlParameter State = cmd.Parameters.Add("@State", SqlDbType.VarChar, 50);
                    State.Direction = ParameterDirection.Input;
                    State.Value = coachDTO.State;

                    SqlParameter City = cmd.Parameters.Add("@City", SqlDbType.VarChar, 50);
                    City.Direction = ParameterDirection.Input;
                    City.Value = coachDTO.City;

                    SqlParameter Zip = cmd.Parameters.Add("@Zip", SqlDbType.VarChar, 20);
                    Zip.Direction = ParameterDirection.Input;
                    Zip.Value = coachDTO.Zip;

                    SqlParameter WebsiteURL = cmd.Parameters.Add("@WebsiteURL", SqlDbType.VarChar, 200);
                    WebsiteURL.Direction = ParameterDirection.Input;
                    WebsiteURL.Value = coachDTO.WebsiteURL;

                    SqlParameter FaidURL = cmd.Parameters.Add("@FaidURL", SqlDbType.VarChar, 200);
                    FaidURL.Direction = ParameterDirection.Input;
                    FaidURL.Value = coachDTO.FaidURL;

                    SqlParameter ApplURL = cmd.Parameters.Add("@ApplURL", SqlDbType.VarChar, 200);
                    ApplURL.Direction = ParameterDirection.Input;
                    ApplURL.Value = coachDTO.ApplURL;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = coachDTO.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = coachDTO.Longitude;

                    SqlParameter EnrollmentNo = cmd.Parameters.Add("@EnrollmentNo", SqlDbType.Int);
                    EnrollmentNo.Direction = ParameterDirection.Input;
                    EnrollmentNo.Value = coachDTO.EnrollmentNo;

                    SqlParameter ApplicationNo = cmd.Parameters.Add("@ApplicationNo", SqlDbType.Int);
                    ApplicationNo.Direction = ParameterDirection.Input;
                    ApplicationNo.Value = coachDTO.ApplicationNo;

                    SqlParameter AdmissionNo = cmd.Parameters.Add("@AdmissionNo", SqlDbType.Int);
                    AdmissionNo.Direction = ParameterDirection.Input;
                    AdmissionNo.Value = coachDTO.AdmissionNo;


                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = coachDTO.ProfilePicURL;


                    SqlParameter CoverPicURL = cmd.Parameters.Add("@CoverPicURL", SqlDbType.VarChar, 1000);
                    CoverPicURL.Direction = ParameterDirection.Input;
                    CoverPicURL.Value = coachDTO.CoverPicURL;

                    SqlParameter Cost = cmd.Parameters.Add("@Cost", SqlDbType.Int);
                    Cost.Direction = ParameterDirection.Input;
                    Cost.Value = coachDTO.Cost;

                    SqlParameter MensTeam = cmd.Parameters.Add("@MensTeam", SqlDbType.Int);
                    MensTeam.Direction = ParameterDirection.Input;
                    MensTeam.Value = coachDTO.MensTeam;

                    SqlParameter WomenTeam = cmd.Parameters.Add("@WomenTeam", SqlDbType.Int);
                    WomenTeam.Direction = ParameterDirection.Input;
                    WomenTeam.Value = coachDTO.WomenTeam;

                    SqlParameter AdmittanceRate = cmd.Parameters.Add("@AdmittanceRate", SqlDbType.Float);
                    AdmittanceRate.Direction = ParameterDirection.Input;
                    AdmittanceRate.Value = coachDTO.AdmittanceRate;

                    SqlParameter AcceptanceAdmission = cmd.Parameters.Add("@AcceptanceAdmission", SqlDbType.Float);
                    AcceptanceAdmission.Direction = ParameterDirection.Input;
                    AcceptanceAdmission.Value = 0;

                    SqlParameter Enabled = cmd.Parameters.Add("@Enabled", SqlDbType.Int);
                    Enabled.Direction = ParameterDirection.Input;
                    Enabled.Value = coachDTO.Enabled;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v2/InsertUniversity", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }



        internal static int InsertCoachUser(CoachUserDTO coachUserDTO)
        {

            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[InsertCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FacebookId = cmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 100);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = coachUserDTO.Emailaddress;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = coachUserDTO.InstituteId;

                    SqlParameter Password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 100);
                    Password.Direction = ParameterDirection.Input;
                    Password.Value = coachUserDTO.Password;

                    SqlParameter active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    active.Direction = ParameterDirection.Input;
                    active.Value = coachUserDTO.Active;

                    SqlParameter teamType = cmd.Parameters.Add("@TeamType", SqlDbType.Int);
                    teamType.Direction = ParameterDirection.Input;
                    teamType.Value = coachUserDTO.TeamType;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v2/InsertCoachUser", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static ClubCoachDTO GetClubCoachProfile2_v2(string emailaddress,int loginType)
        {
            ClubCoachDTO v = new ClubCoachDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetClubCoachProfile2_v2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter logintypePar = cmd.Parameters.Add("@logintype", SqlDbType.Int);
                    logintypePar.Direction = ParameterDirection.Input;
                    logintypePar.Value = loginType;


                    SqlDataReader myData = cmd.ExecuteReader();

                    v = Dal2.GetClubCoach(myData);


                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetClubCoachProfile", "", ex.Message, "Exception", 0);
            }


            return v;
        }


        internal static FamilyFriendsDTO GetFamilyFriendsProfile2_V2(string emailaddress,int loginType)
        {
            FamilyFriendsDTO v = new FamilyFriendsDTO();
            try
            {

                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFamilyFriendsProfile2_v2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter logintypePar = cmd.Parameters.Add("@logintype", SqlDbType.Int);
                    logintypePar.Direction = ParameterDirection.Input;
                    logintypePar.Value = loginType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.Phonenumber = (string)myData["Phonenumber"].ToString();
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        v.Sport = (myData["Sport"] == DBNull.Value) ? string.Empty : (string)myData["Sport"];
                        v.Almamater = (myData["Almamater"] == DBNull.Value) ? string.Empty : (string)myData["Almamater"];
                        v.DeviceId = (myData["DeviceName"] == DBNull.Value) ? string.Empty : (string)myData["DeviceName"];
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : (string)myData["Username"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_V2/GetFamilyFriendsProfile2_v2", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        //internal static int ValidateCoach(string emailaddress, string password)
        //{
        //    int v = 0;

        //    try
        //    {
        //        SqlConnection myConn = UsersDal.ConnectToDb();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[ValidateCoach]", myConn);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
        //            emailaddressPar.Direction = ParameterDirection.Input;
        //            emailaddressPar.Value = emailaddress;

        //            SqlParameter passwordPar = cmd.Parameters.Add("@password", SqlDbType.Int);
        //            passwordPar.Direction = ParameterDirection.Input;
        //            passwordPar.Value = password;


        //            SqlDataReader myData = cmd.ExecuteReader();
        //            while (myData.Read())
        //            {
        //                v = (myData["Result"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Result"]);
        //            }

        //            myData.Close();
        //            myConn.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        EmailLogModel emaillog = new EmailLogModel();
        //        emaillog.AddEmailLog("Dal2_v2/ValidateCoach", "", ex.Message, "Exception", 0);
        //    }


        //    return v;
        //} 

        //internal static int GetCoachIdOnLogin(string emailaddress, string password)
        //{
        //    int v = 0;

        //    try
        //    {
        //        SqlConnection myConn = UsersDal.ConnectToDb();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[GetCoachIdOnLogin]", myConn);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
        //            emailaddressPar.Direction = ParameterDirection.Input;
        //            emailaddressPar.Value = emailaddress;

        //            SqlParameter passwordPar = cmd.Parameters.Add("@password", SqlDbType.Int);
        //            passwordPar.Direction = ParameterDirection.Input;
        //            passwordPar.Value = password;


        //            SqlDataReader myData = cmd.ExecuteReader();
        //            while (myData.Read())
        //            {
        //                v = (myData["Result"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Result"]);
        //            }

        //            myData.Close();
        //            myConn.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        EmailLogModel emaillog = new EmailLogModel();
        //        emaillog.AddEmailLog("Dal2_v2/ValidateCoach", "", ex.Message, "Exception", 0);
        //    }


        //    return v;
        //}
    }


}