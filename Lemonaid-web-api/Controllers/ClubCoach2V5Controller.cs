﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Lemonaid_web_api.Models;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using System.Web;

namespace Lemonaid_web_api.Controllers
{
    public class ClubCoach2V5Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse ApproveMediaFromAthlete(int clubCoachId, int shareVideoId)
        {
            try
            {
                var res = DAL2V5.ApproveMediaFromAthleteByClC(clubCoachId, shareVideoId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = res; 
                _result.Message = CustomConstants.Media_Content_Approved;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse ShareAthleteVideoToClubCoach(MediaContent mediaContent)
        {
            try
            {
                ClubCoachDTO clubCoach = Dal2.GetClubCoachProfile(mediaContent.ClubCoachEmail); 
                SupportMailDTO support;
                support = UsersDal.GetSupportMails("SupportEmail");
                MailMessage msg = new MailMessage();
                string athleteName = string.Empty, athleteEmailAddress = string.Empty; string deviceName = string.Empty; string deviceId = string.Empty; int loginType = 0; string username = string.Empty;
                UsersDalV4.GetAthleteDetails_v4(mediaContent.AthleteId, ref athleteName, ref athleteEmailAddress, ref deviceId, ref deviceName, ref loginType, ref username);
                // UsersDal.PushNotification(message, DeviceName);


                string baseURL = ConfigurationManager.AppSettings["BaseURL"];

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AthleteShareVideoToClubCoach.html"));
                string readFile = reader.ReadToEnd();
                string myString;
                myString = readFile;
                myString = myString.Replace("$$ClubCoachName$$", mediaContent.ClubCoachEmail);
                myString = myString.Replace("$$Name$$", athleteName);
                myString = myString.Replace("$$AtheleteId$$", Convert.ToString(mediaContent.AthleteId));
                myString = myString.Replace("$$ShareVideoId$$", Convert.ToString(mediaContent.MediaContentId));
                myString = myString.Replace("$$BaseURL$$", baseURL);

                if (clubCoach.Id > 0)
                {
                    string message = "Dear " + mediaContent.ClubCoachEmail + ", Can you please verify the media content which was sent by an athlete.";


                    //Notifications Insertion 
                    NotificationDTO not = new NotificationDTO();
                    not.Status = 0;
                    not.UserID = clubCoach.Id; 
                    not.SenderId = mediaContent.AthleteId;
                    not.NotificationId = 10;
                    not.ShortMessage = message;
                    not.LongMessage = myString;
                    not.SenderType = 0;
                    not.ReceiverType = 2;
                    UsersDal.AddNotifications(not); 
                }
                else
                {
                    SendGridMailModel sendGridModel = new SendGridMailModel();
                    sendGridModel.toMail = new List<string>();
                    sendGridModel.toMail.Add(mediaContent.ClubCoachEmail);
                    sendGridModel.fromMail = support.EmailAddress;
                    sendGridModel.fromName = "Lemonaid";
                    sendGridModel.subject = " Athlete Badges to coaches";
                    sendGridModel.Message = myString;
                    sendGridModel.email = support.EmailAddress;
                    sendGridModel.password = support.Password;
                    SendGridSMTPMail.SendEmail(sendGridModel); 
                    EmailLogModel emaillog = new EmailLogModel();
                    emaillog.AddEmailLog(support.EmailAddress, mediaContent.ClubCoachEmail, msg.Subject, "Mail", 1);

                }

                 int id = DAL2V5.UpdateShareStatusFromAthleteTOClC(mediaContent.MediaContentId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = mediaContent.AthleteId;
                _result.Message = CustomConstants.Media_Content_Shared;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            } 
        }
 
    }
}
