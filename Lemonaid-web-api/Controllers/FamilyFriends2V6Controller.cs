﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;


namespace Lemonaid_web_api.Controllers
{
    public class FamilyFriends2V6Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddfamilyFriends(FamilyFriendsDTO familyFriendsDto)
        {
            FamilyFriendsController ffctrl = new FamilyFriendsController();
            try
            {
                if (!familyFriendsDto.ProfilePicURL.StartsWith("http"))
                {
                    familyFriendsDto.ProfilePicURL = UsersDal.UploadImageFromBase64(familyFriendsDto.ProfilePicURL, "png");
                }

                if (familyFriendsDto.Id == 0)
                {
                    List<SportsDTO> sports = ffctrl.AddFFFToWrapperDb(familyFriendsDto);
                }

                var familyFriends = Dal2_v2.AddfamilyFriends2_v2(familyFriendsDto);
                familyFriends.SportId = familyFriendsDto.SportId;
                if (familyFriends.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = familyFriends;
                    _result.Message = CustomConstants.FamilyFriend_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends2V6/AddfamilyFriends", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;

        }

        [HttpPost] 
        public Utility.CustomResponse GetInstitutesForFamilyFriends(FamilyFriendsDeckDTO familyFriendsDeck)
        {
            CoachController coachController = new CoachController();
            ClubCoachController clubcoachController = new ClubCoachController();
            try
            {
                var coaches = UsersDalV5.GetInstitutesForFamilyFriends(familyFriendsDeck.offset, familyFriendsDeck.max, familyFriendsDeck.searchText, familyFriendsDeck.familyFriendId);
                List<AdDTO> ads = DAL2V6.GetChatAdCards(familyFriendsDeck.Emailaddress, familyFriendsDeck.loginType, familyFriendsDeck.SportId, Convert.ToInt32(Utility.AdType.NormalAd), Utility.UserTypes.FamilyFriends);
                coaches = coachController.GetAdCoaches(ads, coaches);
                List<AdDTO> ChatAds = DAL2V6.GetChatAdCards(familyFriendsDeck.Emailaddress, familyFriendsDeck.loginType, familyFriendsDeck.SportId, Convert.ToInt32(Utility.AdType.FrontRushAd), Utility.UserTypes.FamilyFriends);
                coaches = coachController.GetAdCoaches(ChatAds, coaches);
                coaches = clubcoachController.AddCoachVideos(coaches);

                if (coaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = string.Empty;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends2V6/GetInstitutesForFamilyFriends", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

       [HttpPost] 
        public Utility.CustomResponse GetAthletesFromState(FamilyFriendsDeckDTO familyFriendsDeck)
        {
            AthleteV5Controller athv5 = new AthleteV5Controller();
            AthleteController atc = new AthleteController();

            try
            {
                string state;
                state = athv5.GetStateWithLatLong(familyFriendsDeck.latitude, familyFriendsDeck.longitude);

                List<AthleteDTO> athletes;
                athletes = UsersDalV5.GetAthletesFromState(state, familyFriendsDeck.offset, familyFriendsDeck.max, familyFriendsDeck.searchText, familyFriendsDeck.familyFriendId);
                foreach (AthleteDTO t in athletes)
                {
                    t.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(t.Id);
                    t.AtheleteVideos = UsersDalV4.GetAtheleteIdealVideos_v4(t.Emailaddress, t.LoginType);
                    int measurementcount = atc.GetMeasurementCount(t.Height, t.Height2, t.Weight, t.WingSpan, t.ShoeSize);
                    t.MeasurementCount = measurementcount;
                }

                if (athletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = string.Empty;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends2V6/GetAthletesFromState", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse GetFamilyFriendsInstituteFavourites(FamilyFriendsDeckDTO familyFriendsDeckDTO)
        { 
            try
            {
                
                ClubCoachController clubcoachController = new ClubCoachController();
               
                var institutes = DAL2V6.GetFamilyFriendsInstituteFavourites_2V6(familyFriendsDeckDTO.familyFriendId);
               institutes = clubcoachController.AddCoachVideos(institutes);
                for (int i = 0; i < institutes.Count; i++)
                {
                    //List<int> coachIds = Dal2.GetUniversityCoaches(institutes[i].Id);
                    //List<VideoDTO> totalcoachVideos = new List<VideoDTO>();
                    //List<VideoDTO> coachVideos = new List<VideoDTO>();
                    //foreach (var j in coachIds)
                    //{
                    //    coachVideos = Dal2.GetCoachVideos(j);
                    //    foreach (var Video in coachVideos)
                    //    {
                    //        totalcoachVideos.Add(Video);
                    //    }
                    //}
                    //institutes[i].CoachVideos = totalcoachVideos;

                    List<AthleteDTO> atheletes = UsersDalV5.GetRecommendAthletesinInstitutesbyfff(institutes[i].Id, familyFriendsDeckDTO.familyFriendId);
                    institutes[i].RecommendedAtheletes = atheletes;
                   
                }
                AthleteFavouriteDTO athleteFavouriteDTO = new AthleteFavouriteDTO();
                athleteFavouriteDTO.AthleteEmail = familyFriendsDeckDTO.Emailaddress;
                athleteFavouriteDTO.LoginType = familyFriendsDeckDTO.loginType;
                athleteFavouriteDTO.UserType = Utility.UserTypes.FamilyFriends;
                athleteFavouriteDTO.Offset = familyFriendsDeckDTO.offset;
                athleteFavouriteDTO.Max = familyFriendsDeckDTO.max;
                athleteFavouriteDTO.SportId = familyFriendsDeckDTO.SportId;

                athleteFavouriteDTO.UserType = Utility.UserTypes.FamilyFriends;

                var ChatAdMatches = DAL2V6.GetAthleteChatAdFavourites(athleteFavouriteDTO);
                //GetUserID -- From athleteemail , logintype, FFF
                int AthleteWrapperId = DAL2V6.GetUserId(athleteFavouriteDTO.AthleteEmail, athleteFavouriteDTO.LoginType, athleteFavouriteDTO.UserType);
                foreach (var a in ChatAdMatches)
                {
                    int chatCount = DAL2V4.GetAthleteChatAdChatCount(a.Id, AthleteWrapperId);
                    a.ChatCount = chatCount;
                }
                institutes = AddChatAdsToList(ChatAdMatches, institutes);
                IEnumerable<CoachDTO> enumcollegeAtheleteCoaches = institutes.Skip(familyFriendsDeckDTO.offset).Take(familyFriendsDeckDTO.max);
                var institutes1 = enumcollegeAtheleteCoaches.ToList();

                if (institutes1.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = institutes1;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = string.Empty;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends2v6/GetFamilyFriendsInstituteFavourites", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        public List<CoachDTO> AddChatAdsToList(List<AthleteDTO> ChatAdMatches, List<CoachDTO> collegeAtheleteCoaches)
        {
            for (int i = 0; i < ChatAdMatches.Count; i++)
            {
                CoachDTO atheletes = new CoachDTO();
                atheletes.Id = ChatAdMatches[i].Id;
                atheletes.Name = ChatAdMatches[i].Name;
                atheletes.ProfilePicURL = ChatAdMatches[i].ProfilePicURL;
                atheletes.CoverPicURL = ChatAdMatches[i].CoverPicURL;
                atheletes.Description = ChatAdMatches[i].Description;
                atheletes.AdType = ChatAdMatches[i].AdType;
                atheletes.AtheleteCoachType = 3; //For Chat Ad Cards
                atheletes.ChatCount = ChatAdMatches[i].ChatCount;
                collegeAtheleteCoaches.Add(atheletes);
            }
            return collegeAtheleteCoaches;
        }

    }
}
