﻿using System;
using System.Web.Http;
using Lemonaid_web_api.Models;
using System.Collections.Generic;
using System.Linq;


namespace Lemonaid_web_api.Controllers
{
    public class Blogs2V6Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse GetAthletesBlogsBadges(BlogInputDTO blogInputDTO)
        {
            try
            {
                Athletev4Controller athleteV4 = new Athletev4Controller();
                AtheleteBlogDTO badges = new AtheleteBlogDTO();

                badges.AtheletesBadgesDTO = UsersDalV4.GetAtheleteBadges_v4(blogInputDTO.Id);
                badges.AtheleteInvitationDTO = UsersDalV4.GetAtheleteInvitations_v4(blogInputDTO.Id);

                var coachVideos = DAL2V5.GetAtheleteFavouritesVideos(blogInputDTO.Id);

                var blogs = UsersDalV4.GetBlog_v4(blogInputDTO.Id);
                var searchBlogs = blogs; 
                if (blogInputDTO.SearchText != string.Empty)
                {
                    searchBlogs = blogs.FindAll(x => x.Title.ToLower().Contains(blogInputDTO.SearchText.ToLower()));

                }
                List<BlogDTO> blogRes = searchBlogs.ToList();
                int totalCount = blogRes.Count;
                foreach (var k in blogRes)
                {
                    k.TotalCount = totalCount;
                }

                if (totalCount > 0)
                {
                    var res = AddBlogsToMisc(blogRes, coachVideos);

                    var Shuffleres = res.OrderBy(a => Guid.NewGuid());

                    IEnumerable<MiscDTO> enumcollegeAtheleteCoaches = Shuffleres.Skip(blogInputDTO.Offset).Take(Shuffleres.ToList().Count - blogInputDTO.Offset);
                    badges.BlogsDTO = enumcollegeAtheleteCoaches.ToList(); 
                    if (badges.BlogsDTO.Count > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = badges;
                        _result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = null;
                        _result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = null;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Blogs2V6/GetAthletesBlogsBadges", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        private List<MiscDTO> AddBlogsToMisc(List<BlogDTO> blogs, List<MiscDTO> blogsDTO)
        {
            foreach (var r in blogs)
            {
                MiscDTO m = new MiscDTO();
                m.Id = r.Id;
                m.Description = r.Description;
                m.ImageURL = r.ImageURL;
                m.ShortDesc = r.ShortDesc;
                m.Title = r.Title;
                m.URL = r.URL;
                m.Date = r.Date;
                m.Type = r.Type;
                blogsDTO.Add(m);
            }
            return blogsDTO;
        }

        [HttpPost]
        public Utility.CustomResponse GetCoachBlogsBadges(BlogInputDTO blogInputDTO)
        {
            try
            {
                CoachV5Controller coachV5 = new CoachV5Controller();
                AthleteController athCtrl = new AthleteController();
                CoachesBlogDTO badges = new CoachesBlogDTO();

                var atheleteVideos = DAL2V5.GetCoachesFavouritesVideos(blogInputDTO.Emailaddress);

                badges.coachBadgesDTO = UsersDal.GetInstituteBadges(blogInputDTO.Emailaddress);
                var blogs = UsersDal.GetBlog(blogInputDTO.Emailaddress);
                var searchBlogs = blogs;
                if (blogInputDTO.SearchText != string.Empty)
                {
                    searchBlogs = blogs.FindAll(x => x.Title.ToLower().Contains(blogInputDTO.SearchText.ToLower()));

                }
                List<BlogDTO> blogRes = searchBlogs.ToList();
                int totalCount = blogRes.Count;
                foreach (var k in blogRes)
                {
                    k.TotalCount = totalCount;
                }

                if (totalCount > 0)
                {
                    var res = AddBlogsToMisc(blogRes, atheleteVideos);

                    var Shuffleres = res.OrderBy(a => Guid.NewGuid());
                    IEnumerable<MiscDTO> enumcollegeAtheleteCoaches = Shuffleres.Skip(blogInputDTO.Offset).Take(Shuffleres.ToList().Count - blogInputDTO.Offset);
                    badges.BlogsDTO = enumcollegeAtheleteCoaches.ToList();

                    if (badges.BlogsDTO.Count > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = badges;
                        _result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = null;
                        _result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = null;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Blogs2V6/GetCoachBlogsBadges", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse GetClubCoachBlogs(BlogInputDTO blogInputDTO)
        {
            try
            {
                Athletev4Controller athleteV4 = new Athletev4Controller();
                CoachV5Controller coachV5 = new CoachV5Controller();
                ClubCoachsBlogDTO clubCoachBlogs = new ClubCoachsBlogDTO();

                var atheleteVideos1 = DAL2V5.GetAthletesinClubCoachClubVideos(blogInputDTO.Id);
                var atheleteSchoolVideos = DAL2V5.GetAthletesinClubCoachSchoolVideos(blogInputDTO.Id);
                var atheleteVideos = AddAthleteSchoolandClubVideos(atheleteVideos1, atheleteSchoolVideos);

                var CoachVideos = DAL2V5.GetClubCoachFavouritesVideos(blogInputDTO.Id);

                var athleteCoachVideos = AddAthleteCoachVideos(atheleteVideos, CoachVideos);
                var blogs = UsersDal.GetBlog();
                var searchBlogs = blogs;
                if (blogInputDTO.SearchText != string.Empty)
                {
                    searchBlogs = blogs.FindAll(x => x.Title.ToLower().Contains(blogInputDTO.SearchText.ToLower()));

                }

                List<BlogDTO> blogRes = searchBlogs.ToList();
                int totalCount = blogRes.Count;
                foreach(var k in blogRes)
                {
                    k.TotalCount = totalCount;
                }
                if (totalCount > 0)
                {
                    var res = AddBlogsToMisc(blogRes, athleteCoachVideos);

                    var Shuffleres = res.OrderBy(a => Guid.NewGuid());
                    IEnumerable<MiscDTO> enumcollegeAtheleteCoaches = Shuffleres.Skip(blogInputDTO.Offset).Take(Shuffleres.ToList().Count - blogInputDTO.Offset);
                    clubCoachBlogs.BlogsDTO = enumcollegeAtheleteCoaches.ToList();

                    if (clubCoachBlogs.BlogsDTO.Count > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = clubCoachBlogs;
                        _result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = null;
                        _result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = null;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Blogs2V6/GetClubCoachBlogs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        private List<MiscDTO> AddAthleteSchoolandClubVideos(List<MiscDTO> atheleteVideos, List<MiscDTO> atheleteSchoolVideos)
        {
            foreach (var i in atheleteSchoolVideos)
            {
                atheleteVideos.Add(i);
            }

            return atheleteVideos;
        }

        private List<MiscDTO> AddAthleteCoachVideos(List<MiscDTO> atheleteVideos, List<MiscDTO> coachVideos)
        {
            foreach (var i in coachVideos)
            {
                atheleteVideos.Add(i);
            }

            return atheleteVideos;
        }

        [HttpPost]
        public Utility.CustomResponse GetFamilyFriendsBlogs(BlogInputDTO blogInputDTO)
        {
            try
            {
                Athletev4Controller athleteV4 = new Athletev4Controller();
                CoachV5Controller coachV5 = new CoachV5Controller();
                ClubCoachsBlogDTO clubCoachBlogs = new ClubCoachsBlogDTO();

                var atheleteVideos = DAL2V5.GetFamilyFriendsAtheleteFavouritesVideos(blogInputDTO.Id);
                var CoachVideos = DAL2V5.GetFamilyFriendsInstituteFavouritesVideos(blogInputDTO.Id);

                var athleteCoachVideos = AddAthleteCoachVideos(atheleteVideos, CoachVideos);

                var blogs = UsersDal.GetBlog();
                var searchBlogs = blogs;
                if (blogInputDTO.SearchText != string.Empty)
                {
                    searchBlogs = blogs.FindAll(x => x.Title.ToLower().Contains(blogInputDTO.SearchText.ToLower()));

                }
                List<BlogDTO> blogRes = searchBlogs.ToList();
                int totalCount = blogRes.Count;
                foreach (var k in blogRes)
                {
                    k.TotalCount = totalCount;
                }

                if (totalCount > 0)
                {
                    var res = AddBlogsToMisc(blogRes, athleteCoachVideos);

                    var Shuffleres = res.OrderBy(a => Guid.NewGuid());
                    IEnumerable<MiscDTO> enumcollegeAtheleteCoaches = Shuffleres.Skip(blogInputDTO.Offset).Take(Shuffleres.ToList().Count - blogInputDTO.Offset);
                    clubCoachBlogs.BlogsDTO = enumcollegeAtheleteCoaches.ToList();

                    if (clubCoachBlogs.BlogsDTO.Count > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = clubCoachBlogs;
                        _result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = null;
                        _result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = null;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Blogs2V6/GetFamilyFriendsBlogs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse GetCollegeAthleteBlogs(BlogInputDTO blogInputDTO)
        {
            try
            {
                Athletev4Controller athleteV4 = new Athletev4Controller();
                CoachV5Controller coachV5 = new CoachV5Controller();
                //CollegeAthleteBlogDTO collegeAthleteBlogs = new CollegeAthleteBlogDTO(); 

                AtheleteBlogDTO badges = new AtheleteBlogDTO();

                badges.AtheletesBadgesDTO = UsersDalV4.GetAtheleteBadges_v4(blogInputDTO.Id);
                badges.AtheleteInvitationDTO = UsersDalV4.GetAtheleteInvitations_v4(blogInputDTO.Id);

                var atheleteVideos = DAL2V5.GetCollegeAtheleteFavouritesVideos(blogInputDTO.Id);

                var blogs = UsersDal.GetBlog();
                var searchBlogs = blogs;
                if (blogInputDTO.SearchText != string.Empty)
                {
                    searchBlogs = blogs.FindAll(x => x.Title.ToLower().Contains(blogInputDTO.SearchText.ToLower()));

                }
                List<BlogDTO> blogRes = searchBlogs.ToList();
                int totalCount = blogRes.Count;
                foreach (var k in blogRes)
                {
                    k.TotalCount = totalCount;
                }

                if (totalCount > 0)
                {
                    var res = AddBlogsToMisc(blogRes, atheleteVideos); 

                    var Shuffleres = res.OrderBy(a => Guid.NewGuid());
                    IEnumerable<MiscDTO> enumcollegeAtheleteCoaches = Shuffleres.Skip(blogInputDTO.Offset).Take(Shuffleres.ToList().Count - blogInputDTO.Offset);
                    badges.BlogsDTO = enumcollegeAtheleteCoaches.ToList();

                    if (badges.BlogsDTO.Count > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = badges;
                        _result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = null;
                        _result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = null;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Blogs2V6/GetCollegeAthleteBlogs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
    }
}
