﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.ServiceModel;
using System.ServiceModel.Syndication;

namespace Lemonaid_web_api
{
    public partial class Rss : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var feed = ReadFeed("https://script.googleusercontent.com/macros/echo?user_content_key=AlMFjs0Z3nIJJxXHZZGZpHdYfCJg48ZyOsd7Lwqv9KV3Q3eXOY0kDVxaAAkqpXL8w28QlXlJ9b2E-DAmc5_a81R79PMMUBT0m5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnDGfb4xHv6DtJX17ARFnPCqlfsQZA6lL_6ZaQ_8TghARWAkK8dJV-j-XSyG10Sf7FwingLaFe8Yjwy_DfXN4kXwYpmL6JtcpQcMaCe3IyygC&lib=Mv8Fnzg7fAF3wSsXEthPqmxT5HIHZevCt");
        // https://www.facebook.com/feeds/notifications.php?id=604144760&viewer=604144760&key=AWhh59B-sShxlpW5&format=rss20 FaceBook
        // http://widget.websta.me/rss/n/bindu.bhargavi - Instagram
        // https://script.google.com/macros/s/AKfycbxUNkY2h3aLHAR-zkZYJ3Db64DTqD4hlJt3yGyjmdk24X9QFJHg/exec?683896418398932992 Twitter
            foreach (var item in feed)
            {
                Response.Write("***********************************************");
                //Console.WriteLine("Title:" + item.title);
                Response.Write("Link:" + item.Rsslink);
                Response.Write("Description:" + item.description);
                Response.Write("Language:" + item.language);
                Response.Write("Publish Date:" + item.pubdate);
                Response.Write("***********************************************");
            }
          

            //string url = "https://www.facebook.com/feeds/notifications.php?id=604144760&viewer=604144760&key=AWhh59B-sShxlpW5&format=rss20";
            //var req = (HttpWebRequest)WebRequest.Create(url);
            //req.Method = "GET";
            //req.UserAgent = "Fiddler";

            //var rep = req.GetResponse();
            //var reader = XmlReader.Create(rep.GetResponseStream()); 
            //SyndicationFeed feed = SyndicationFeed.Load(reader);

        }


        public static List<RssFeedItem> ReadFeed(string url)
        {
            List<RssFeedItem> rssItems = new List<RssFeedItem>();
            HttpWebRequest rssFeed = (HttpWebRequest)WebRequest.Create(url);
            rssFeed.UserAgent = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 2.0.50727; .NET4.0C; .NET4.0E)";
            using (DataSet rssData = new DataSet())
            {
                var response = rssFeed.GetResponse();
                var data = response.GetResponseStream();
                rssData.ReadXml(data);
                foreach (DataRow datarow in rssData.Tables["item"].Rows)
                {
                    rssItems.Add(new RssFeedItem
                    {
                        description = Convert.ToString(datarow[1]),
                        language = Convert.ToString(datarow[2]),
                        Rsslink = Convert.ToString(datarow[3]),
                        pubdate = Convert.ToString(datarow[4])
                     //   title = Convert.ToString(datarow["title"])
                    });
                }
            }
            return rssItems;
        }


        public class RssFeedItem
        {
            public string title { get; set; }
            public string Rsslink { get; set; }
            public string description { get; set; }
            public string language { get; set; }
            public string pubdate { get; set; }
        }
    }
}