﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
 

namespace Lemonaid_web_api.Controllers
{
    public class MetricsController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        public string WrapperApi = ConfigurationManager.AppSettings["WrapperBaseURL"];

        [HttpPost]
        public Utility.CustomResponse AddSwimmingmetrics(MetricsDto.SwimmingMetricsDto swimmingMetrics)
        {
            try
            {
                var metrics = DalMetrics.AddSwimmingMetrics(swimmingMetrics);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddSwimmingmetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddRowingMetrics(MetricsDto.RowingMetricsDto rowingMetrics)
        {
            try
            {
                var metrics = DalMetrics.AddRowingMetrics(rowingMetrics);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddRowingMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddDivingMetrics(MetricsDto.DivingMetricsDto divingMetrics)
        {
            try
            {
                var metrics = DalMetrics.AddDivingMetrics(divingMetrics);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddDivingMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddTennisMetrics(MetricsDto.TennisMetricsDto tennisMetrics)
        {
            try
            {
                var metrics = DalMetrics.AddTennisMetrics(tennisMetrics);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddTennisMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse AddTrackMetrics(MetricsDto.TrackMetricsDto trackMetrics)
        {
            try
            {
                var metrics = DalMetrics.AddTrackMetrics(trackMetrics);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddTrackMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse AddLacrosseMetrics(MetricsDto.LacrosseMetricsDto lacrosseMetrics)
        {
            try
            {
                var metrics = DalMetrics.AddLacrosseMetrics(lacrosseMetrics);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddLacrosseMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse AddGolfMetrics(MetricsDto.GolfMetricsDto golfMetrics)
        {
            try
            {
                var metrics = DalMetrics.AddGolfMetrics(golfMetrics);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddGolfMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse AddWaterpoloMetrics(MetricsDto.WaterpoloMetricsDto waterpoloMetrics)
        {
            try
            {
                var metrics = DalMetrics.AddWaterpoloMetrics(waterpoloMetrics);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddWaterpoloMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddBaseballMetrics(MetricsDto.BaseballMetricsDto baseballMetricsDto)
        {
            try
            {
                var metrics = DalMetrics.AddBaseballMetrics(baseballMetricsDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddBaseballMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddBasketballMetrics(MetricsDto.BasketballMetricsDto basketballMetricsDto)
        {
            try
            {
                var metrics = DalMetrics.AddBasketballMetrics(basketballMetricsDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddBasketballMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddFootballMetrics(MetricsDto.FootballMetricsDto footballMetricsDto)
        {
            try
            {
                var metrics = DalMetrics.AddFootballMetrics(footballMetricsDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddFootballMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddSoccerMetrics(MetricsDto.SoccerMetricsDto soccerMetricsDto)
        {
            try
            {
                var metrics = DalMetrics.AddSoccerMetrics(soccerMetricsDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddFootballMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddSoftballMetrics(MetricsDto.SoftballMetricsDto softballMetricsDto)
        {
            try
            {
                var metrics = DalMetrics.AddSoftballMetrics(softballMetricsDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddSoftballMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddIceHockeyMetrics(MetricsDto.IceHockeyMetricsDto iceHockeyMetricsDto)
        {
            try
            {
                var metrics = DalMetrics.AddIceHockeyMetrics(iceHockeyMetricsDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddIceHockeyMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddWrestlingMetrics(MetricsDto.WrestlingMetricsDto wrestlingMetricsDto)
        {
            try
            {
                var metrics = DalMetrics.AddWrestlingMetrics(wrestlingMetricsDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddWrestlingMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddVolleyballMetrics(MetricsDto.VolleyballMetricsDto volleyballmetricsDto)
        {
            try
            {
                var metrics = DalMetrics.AddVolleyballMetrics(volleyballmetricsDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Metrics_added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/AddVolleyballMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetMetrics(int athleteId, int sportId)
        {
            try
            {
                object metrics = null;

                if (sportId == 1)
                {
                    metrics = DalMetrics.GetSwimmingMetrics(athleteId);

                }
                else if (sportId == 2)
                {
                    metrics = DalMetrics.GetRowingMetrics(athleteId);
                }
                else if (sportId == 3)
                {
                    metrics = DalMetrics.GetDivingMetrics(athleteId);
                }
                else if (sportId == 4)
                {
                    metrics = DalMetrics.GetTennisMetrics(athleteId);
                }
                else if (sportId == 5)
                {
                    metrics = DalMetrics.GetTrackMetrics(athleteId);
                }
                else if (sportId == 6)
                {
                    metrics = DalMetrics.GetLacrosseMetrics(athleteId);
                }
                else if (sportId == 7)
                {
                    metrics = DalMetrics.GetGolfMetrics(athleteId);
                }
                else if (sportId == 8)
                {
                    metrics = DalMetrics.GetWaterPoloMetrics(athleteId);
                }
                else if (sportId == 9)
                {
                    metrics = DalMetrics.GetFootballMetrics(athleteId);
                }
                else if (sportId == 10)
                {
                    metrics = DalMetrics.GetBasketballMetrics(athleteId);
                }
                else if (sportId == 11)
                {
                    metrics = DalMetrics.GetBaseballMetrics(athleteId);
                }
                else if (sportId == 12)
                {
                    metrics = DalMetrics.GetSoccerMetrics(athleteId);
                }
                else if (sportId == 13)
                {
                    metrics = DalMetrics.GetSoftballMetrics(athleteId);
                }
                else if (sportId == 14)
                { 
                    metrics = DalMetrics.GetWrestlingMetrics(athleteId);
                }
                else if (sportId == 15)
                {
                    metrics = DalMetrics.GetIceHockeyMetrics(athleteId);
                }
                else if (sportId == 16)
                {
                    metrics = DalMetrics.GetVolleyballMetrics(athleteId);
                }
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = metrics;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/GetMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse LacrossePositions()
        {

            try
            {
                var positions = DalMetrics.GetLacrossePositions();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = positions;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/LacrossePositions", "", ex.Message, "Exception", 0);
                return _result;
            }


        }

        [HttpGet]
        public Utility.CustomResponse WaterpoloPositions()
        {

            try
            {
                var positions = DalMetrics.GetWaterpoloPositions();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = positions;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/TrackPositions", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse XCEvents()
        {

            try
            {
                var xcEvents = DalMetrics.GetXCEvents();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = xcEvents;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/XCEvents", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse FieldEvents(string filterName)
        {

            try
            {
                var fieldEvents = DalMetrics.GetFieldEvents(filterName);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = fieldEvents;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/FieldEvents", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse TrackEvents(string filterName)
        {

            try
            {
                var fieldEvents = DalMetrics.GetTrackEvents(filterName);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = fieldEvents;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/TrackEvents", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetTrackandFieldDropdowns()
        {

            try
            {

                var xcEvents = DalMetrics.GetXCEvents();
                var trackEvents = DalMetrics.GetTrackEvents("ALL");
                var fieldEvents = DalMetrics.GetFieldEvents("ALL");
                var trackDecHeptEvents = DalMetrics.GetTRACKDecHeptEvents("ALL");
                MetricsDto.TrackandFieldDps track = new MetricsDto.TrackandFieldDps();
                track.TrackEvents = trackEvents;
                track.XCEvents = xcEvents;
                track.FieldEvents = fieldEvents;
                track.TRACKDecHeptEvents = trackDecHeptEvents;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = track;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/GetTrackandFieldDropdowns", "", ex.Message, "Exception", 0);
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse BaseballPositions()
        {
            try
            {
                var positions = DalMetrics.GetBaseballPositions();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = positions;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/BaseballPositions", "", ex.Message, "Exception", 0);
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse BasketballPositions()
        {
            try
            {
                var positions = DalMetrics.GetBasketballPositions();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = positions;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/BasketballPositions", "", ex.Message, "Exception", 0);
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse FootballPositions()
        {
            try
            {
                var positions = DalMetrics.GetFootballPositions();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = positions;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/FootballPositions", "", ex.Message, "Exception", 0);
                return _result;
            }
        }
         

        [HttpGet]
        public Utility.CustomResponse SoccerPositions()
        {
            try
            {
                var positions = DalMetrics.GetSoccerPositions();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = positions;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/SoccerPositions", "", ex.Message, "Exception", 0);
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse SoftballPositions()
        {
            try
            {
                var positions = DalMetrics.GetSoftballPositions();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = positions;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/SoftballPositions", "", ex.Message, "Exception", 0);
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse IceHockeyPositions()
        {
            try
            {
                var positions = DalMetrics.GetIceHockeyPositions();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = positions;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/IceHockeyPositions", "", ex.Message, "Exception", 0);
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse WrestlingWeightClasses()
        {
            try
            {
                var classes = DalMetrics.GetWrestlingWeightClasses();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = classes;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/WrestlingWeightClasses", "", ex.Message, "Exception", 0);
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse VolleyballPositions()
        {
            try
            {
                var positions = DalMetrics.GetVolleyballPositions();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = positions;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Metrics/VolleyballPositions", "", ex.Message, "Exception", 0);
                return _result;
            }
        }
    }
}
