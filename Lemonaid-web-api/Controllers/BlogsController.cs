﻿using System;
using System.Web.Http;
using Lemonaid_web_api.Models;
using System.Collections.Generic;
using System.Linq;

namespace Lemonaid_web_api.Controllers
{ 
    public class BlogsController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse GetAthletesBlogsBadges(int athleteId)
        {
            try
            {
                Athletev4Controller athleteV4 = new Athletev4Controller();
                AtheleteBlogDTO badges = new AtheleteBlogDTO();

                badges.AtheletesBadgesDTO = UsersDalV4.GetAtheleteBadges_v4(athleteId);
                badges.AtheleteInvitationDTO = UsersDalV4.GetAtheleteInvitations_v4(athleteId);

                var coachVideos = DAL2V5.GetAtheleteFavouritesVideos(athleteId);

                var blogs = UsersDalV4.GetBlog_v4(athleteId);

                var res = AddBlogsToMisc(blogs, coachVideos);
                var  Shuffleres = res.OrderBy(a => Guid.NewGuid());
                badges.BlogsDTO = Shuffleres.ToList();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Blogs/GetAthletesBlogsBadges", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        private List<MiscDTO> AddBlogsToMisc(List<BlogDTO> blogs, List<MiscDTO> blogsDTO)
        {
            foreach(var r in blogs)
            {
                MiscDTO m = new MiscDTO();
                m.Id = r.Id;
                m.Description = r.Description;
                m.ImageURL = r.ImageURL;
                m.ShortDesc = r.ShortDesc;
                m.Title = r.Title;
                m.URL = r.URL;
                m.Date = r.Date;
                m.Type = r.Type;
                blogsDTO.Add(m);
            }
            return blogsDTO;
        }  

        [HttpGet]
        public Utility.CustomResponse GetCoachBlogsBadges(string emailaddress)
        {
            try
            {
                CoachV5Controller coachV5 = new CoachV5Controller();
                AthleteController athCtrl = new AthleteController();
                CoachesBlogDTO badges = new CoachesBlogDTO();

                var atheleteVideos = DAL2V5.GetCoachesFavouritesVideos(emailaddress);

                badges.coachBadgesDTO = UsersDal.GetInstituteBadges(emailaddress);
                var blogs = UsersDal.GetBlog(emailaddress);

                var res = AddBlogsToMisc(blogs, atheleteVideos);
                var Shuffleres = res.OrderBy(a => Guid.NewGuid());
                badges.BlogsDTO = Shuffleres.ToList();
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully; 

                return _result;  
          
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Blogs/GetCoachBlogsBadges", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        } 
        
        [HttpGet]
        public Utility.CustomResponse GetClubCoachBlogs(int clubCoachId)
        {
            try
            {
                Athletev4Controller athleteV4 = new Athletev4Controller();
                CoachV5Controller coachV5 = new CoachV5Controller();
                ClubCoachsBlogDTO clubCoachBlogs = new ClubCoachsBlogDTO();

                var atheleteVideos1 = DAL2V5.GetAthletesinClubCoachClubVideos(clubCoachId);
                var atheleteSchoolVideos = DAL2V5.GetAthletesinClubCoachSchoolVideos(clubCoachId);
                var atheleteVideos = AddAthleteSchoolandClubVideos(atheleteVideos1, atheleteSchoolVideos);

                var CoachVideos = DAL2V5.GetClubCoachFavouritesVideos(clubCoachId);

                var athleteCoachVideos = AddAthleteCoachVideos(atheleteVideos, CoachVideos);
               var blogs = UsersDal.GetBlog();

                var res = AddBlogsToMisc(blogs, athleteCoachVideos);
                var Shuffleres = res.OrderBy(a => Guid.NewGuid());
                clubCoachBlogs.BlogsDTO = Shuffleres.ToList();

                if (blogs.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = clubCoachBlogs;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Blogs/GetClubCoachBlogs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        private List<MiscDTO> AddAthleteSchoolandClubVideos(List<MiscDTO> atheleteVideos, List<MiscDTO> atheleteSchoolVideos)
        {
            foreach (var i in atheleteSchoolVideos)
            {
                atheleteVideos.Add(i);
            }

            return atheleteVideos;
        }

        private List<MiscDTO> AddAthleteCoachVideos(List<MiscDTO> atheleteVideos, List<MiscDTO> coachVideos)
        {
            foreach(var i in coachVideos)
            {
                atheleteVideos.Add(i);
            }

            return atheleteVideos;
        }

        [HttpGet]
        public Utility.CustomResponse GetFamilyFriendsBlogs(int familyfriendId)
        {
            try
            {
                Athletev4Controller athleteV4 = new Athletev4Controller();
                CoachV5Controller coachV5 = new CoachV5Controller();
                ClubCoachsBlogDTO clubCoachBlogs = new ClubCoachsBlogDTO();

                var atheleteVideos = DAL2V5.GetFamilyFriendsAtheleteFavouritesVideos(familyfriendId);
                var  CoachVideos = DAL2V5.GetFamilyFriendsInstituteFavouritesVideos(familyfriendId);

                var athleteCoachVideos = AddAthleteCoachVideos(atheleteVideos, CoachVideos);

                var blogs = UsersDal.GetBlog();

                var res = AddBlogsToMisc(blogs, athleteCoachVideos);
                var Shuffleres = res.OrderBy(a => Guid.NewGuid());
                clubCoachBlogs.BlogsDTO = Shuffleres.ToList();

                if (blogs.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = clubCoachBlogs;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Blogs/GetFamilyFriendsBlogs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }  

        [HttpGet]
        public Utility.CustomResponse GetCollegeAthleteBlogs(int collegeAthleteId)
        {
            try
            {
                Athletev4Controller athleteV4 = new Athletev4Controller();
                CoachV5Controller coachV5 = new CoachV5Controller();
                //CollegeAthleteBlogDTO collegeAthleteBlogs = new CollegeAthleteBlogDTO(); 

                AtheleteBlogDTO badges = new AtheleteBlogDTO();

                badges.AtheletesBadgesDTO = UsersDalV4.GetAtheleteBadges_v4(collegeAthleteId);
                badges.AtheleteInvitationDTO = UsersDalV4.GetAtheleteInvitations_v4(collegeAthleteId);

                var atheleteVideos = DAL2V5.GetCollegeAtheleteFavouritesVideos(collegeAthleteId);

                var blogs = UsersDal.GetBlog();

                var res = AddBlogsToMisc(blogs, atheleteVideos);
                var Shuffleres = res.OrderBy(a => Guid.NewGuid());
                badges.BlogsDTO = Shuffleres.ToList();


                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Blogs/GetCollegeAthleteBlogs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
    }
}
