﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonaid_web_api.Models
{
    public class DALConnection
    {
        internal static List<string> GetStagingConnection()
        {
            List<string> connStrings = new List<string>();
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server =tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidRowingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidDivingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTennisStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTrackStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidLacrosseStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidGolfStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWaterPoloStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidFootballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBasketballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBaseballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoccerStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoftBallStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrestlingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidIceHockeyStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidVolleyBallStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            return connStrings;
        }


        internal static List<string> GetProductionConnection()
        {
            List<string> connStrings = new List<string>();
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBiz;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server =tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidRowing;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidDiving;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTennis;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTrack;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidLacrosse;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidGolf;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWaterPolo;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidFootball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBasketball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBaseball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoccer;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoftBall;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrestling;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidIceHockey;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidVolleyBall;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            return connStrings;
        }
    }
}