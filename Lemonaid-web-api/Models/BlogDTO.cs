﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;



namespace Lemonaid_web_api.Models
{
    public partial class AtheleteBlogDTO
    {
        public BadgesDTO AtheletesBadgesDTO { get; set; }
        public List<GetAtheleteInvitationDTO> AtheleteInvitationDTO { get; set; }
        public List<MiscDTO> BlogsDTO { get; set; }
    }

    public partial class CoachesBlogDTO
    {
        public BadgesDTO coachBadgesDTO { get; set; }
        public List<MiscDTO> BlogsDTO { get; set; }
        //public List<AthleteDTO> Athletes { get; set; }

    }

    public partial class ClubCoachsBlogDTO
    {
        public List<MiscDTO> BlogsDTO { get; set; }
        //public List<AthleteDTO> Athletes { get; set; }
        //public List<CoachDTO> coaches { get; set; }

    }

    public partial class CollegeAthleteBlogDTO
    {
        public List<MiscDTO> BlogsDTO { get; set; }
    }

        public partial class MiscDTO
    { 
        public int Id { get; set; } 
        public string Description { get; set; }
        public string Title { get; set; }
        public string ImageURL { get; set; }
        public string URL { get; set; }
        public DateTime? Date { get; set; }
        public string Type { get; set; }
        public string ShortDesc { get; set; }
        public string Emailaddress { get; set; }
        public int LoginType { get; set; }
        public string UserType { get;  set; }
    }
}