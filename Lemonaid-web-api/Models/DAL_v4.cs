﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json.Linq;
using Microsoft.WindowsAzure.Storage;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;

namespace Lemonaid_web_api.Models
{
    public static class UsersDalV4
    { 
        internal static List<CoachDTO> GetAtheleteFavourites_v4(int atheleteId, int offset, int max)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();
            DateTime dtAtheleteDate = new DateTime();
            DateTime dtInstituteDate = new DateTime();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteFavourites_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["CoachEmail"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? string.Empty : (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.FacebookId = (string)myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        dtAtheleteDate = (myData["AtheleteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["AtheleteDate"];
                        dtInstituteDate = (myData["InstituteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InstituteDate"];
                        v.Liked = ((int)myData["Likes"] != 0) ? true : false;
                        if (v.Liked)
                        { v.FavDate = (dtAtheleteDate > dtInstituteDate) ? dtAtheleteDate : dtInstituteDate; }
                        else
                        { v.FavDate = dtAtheleteDate; }
                        v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                        v.Accept = (myData["Accept"] == DBNull.Value) ? false : (bool)(myData["Accept"]);
                        v.TotalAtheleteAcceptedCount = (myData["TotalAcceptedCount"] == DBNull.Value) ? 0 : (int)(myData["TotalAcceptedCount"]);
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.AtheleteId = (int)myData["AtheleteId"];
                        v.CoachId = (int)myData["CoachId"];
                        v.AtheleteName = (string)myData["AtheleteName"].ToString();
                        v.AtheleteProfilePic = (string)myData["AtheleteProfilePic"].ToString();
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.Payment_Paid = (myData["payment_paid"] == DBNull.Value) ? false : (bool)myData["payment_paid"];
                        v.TotalChatCount = (myData["TotalChatCount"] == DBNull.Value) ? 0 : (int)myData["TotalChatCount"];
                        v.AthleticConference = (myData["AthleticConference"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleticConference"]);
                        v.ScholarshipMen = (myData["ScholarshipMen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipMen"]);
                        v.ScholarshipWomen = (myData["ScholarshipWomen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipWomen"]);
                        v.TuitionInState = (myData["TuitionInState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionInState"]);
                        v.TuitionOutState = (myData["TuitionOutState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionOutState"]);
                        v.DeviceId = (myData["DeviceId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceId"]);
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceType"]);
                        v.VirtualVideoURL = (myData["VirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualVideoURL"]);
                        v.VirtualThumbnailURL = (myData["VirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualThumbnailURL"]);
                        string athletematchesVirtualURL = (myData["AthleteMatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleteMatchesVirtualVideoURL"]);
                        if (athletematchesVirtualURL == string.Empty)
                        {
                            v.MatchesVirtualVideoURL = (myData["MatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualVideoURL"]);
                            v.MatchesVirtualThumbnailURL = (myData["MatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualThumbnailURL"]);
                        }
                        else
                        {
                            v.MatchesVirtualVideoURL = (myData["AthleteMatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleteMatchesVirtualVideoURL"]);
                            v.MatchesVirtualThumbnailURL = (myData["AthleteMatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleteMatchesVirtualThumbnailURL"]);
                        }
                        v.AthleteMatchesVirtualVideoURL = (myData["AthleteMatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleteMatchesVirtualVideoURL"]);
                        v.AthleteMatchesVirtualThumbnailURL = (myData["AthleteMatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleteMatchesVirtualThumbnailURL"]);
                        Coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAthleteFavourites_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }


        internal static List<AthleteDTO> GetCollegeAtheletesInAtheleteFavourites_v4(int atheleteId, int offset, int max)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();
            DateTime dtAtheleteDate = new DateTime();
            DateTime dtInstituteDate = new DateTime();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();  

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCollegeAtheletesinAtheleteFavourites_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdpar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdpar.Direction = ParameterDirection.Input;
                    atheleteIdpar.Value = atheleteId;

                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlDataReader myData = cmd.ExecuteReader();
                  
                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        dtAtheleteDate = (myData["AtheleteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["AtheleteDate"];
                        dtInstituteDate = (myData["InstituteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(myData["InstituteDate"]);
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.Liked = ((int)myData["Likes"] != 0) ? true : false;
                        if (v.Liked)
                        { v.FavDate = (dtAtheleteDate > dtInstituteDate) ? dtAtheleteDate : dtInstituteDate; }
                        else
                        { v.FavDate = dtAtheleteDate; }
                        // v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                        v.AtheleteId = (int)myData["AtheleteId"];
                        v.CoachId = (int)myData["CoachId"];
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.UniversityName = (string)myData["UniversityName"].ToString();
                        v.UniversityProfilePic = (string)myData["UniversityProfilePic"].ToString();
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        v.SchoolCoachName = (myData["SchoolCoachName"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachName"];
                        v.SchoolCoachEmail = (myData["SchoolCoachEmail"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachEmail"];
                        v.SchoolZipCode = (myData["SchoolZipCode"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolZipCode"];
                        v.GapyearLevel = (myData["GapyearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["GapyearLevel"]);
                        v.GapyearDescription = (myData["GapyearDescription"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["GapyearDescription"];
                        v.Country = (myData["Country"] == DBNull.Value) ? string.Empty : (string)myData["Country"];
                        v.CountryId = (myData["CountryId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CountryId"]);
                        v.AP = (myData["AP"] == DBNull.Value) ? 0.0 : Convert.ToDouble(myData["AP"]);
                        v.AthleteTransition = (myData["AthleteTransition"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteTransition"]);
                        v._20YardShuttleRun = (myData["20YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["20YardShuttleRun"];
                        v._60YardShuttleRun = (myData["60YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["60YardShuttleRun"];
                        v.KneelingMedBallToss = (myData["KneelingMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["KneelingMedBallToss"];
                        v.RotationalMedBallToss = (myData["RotationalMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["RotationalMedBallToss"];
                        v.DeviceId = (myData["DeviceId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceId"]);
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceType"]);
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetCollegeAtheletesInAtheleteFavourites_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return athletes;
        }

        internal static int DeleteAthleteBesttimes_v4(int atheleteId)
        {
            int numofRows = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAthleteBesttimes_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    numofRows = cmd.ExecuteNonQuery();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/DeleteAthleteBesttimes_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return numofRows;
        }

        internal static int AddAthleteBesttimes_v4(AthleteEventDTO athleteDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteBesttimes_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = athleteDto.AtheleteId;

                    SqlParameter event_Type_Id = cmd.Parameters.Add("@Event_Type_Id", SqlDbType.Int);
                    event_Type_Id.Direction = ParameterDirection.Input;
                    event_Type_Id.Value = athleteDto.EventTypeId;

                    SqlParameter bestTimeValue = cmd.Parameters.Add("@BestTimeValue", SqlDbType.VarChar, 50);
                    bestTimeValue.Direction = ParameterDirection.Input;
                    bestTimeValue.Value = athleteDto.BesttimeValue.Trim();

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }


                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/AddAthleteBesttimes_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int UpdateFbFriendsandShares_v4(FriendsSharesDTO friendsSharesDto)
        {
            int num = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateFbFriendsandShares_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter friends = cmd.Parameters.Add("@friends", SqlDbType.Int);
                    friends.Direction = ParameterDirection.Input;
                    friends.Value = friendsSharesDto.Friends;

                    SqlParameter shares = cmd.Parameters.Add("@shares", SqlDbType.Int);
                    shares.Direction = ParameterDirection.Input;
                    shares.Value = friendsSharesDto.Shares;

                    SqlParameter flag = cmd.Parameters.Add("@flag", SqlDbType.Int);
                    flag.Direction = ParameterDirection.Input;
                    flag.Value = friendsSharesDto.Flag;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = friendsSharesDto.AtheleteId;

                    num = cmd.ExecuteNonQuery();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/UpdateFbFriendsandShares_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return num;
        }


        internal static int AddAtheleteMatch_v4(AtheleteMatchDTO atheleteMatchDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAtheleteInstMatch_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = atheleteMatchDto.AtheleteId;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = atheleteMatchDto.InstituteId;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    status.Direction = ParameterDirection.Input;
                    status.Value = atheleteMatchDto.Status;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/AddAtheleteMatch_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int DeleteAtheleteMatch_v4(AtheleteMatchDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAtheleteInstMatch_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = atheleteMatchDTO.AtheleteId;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = atheleteMatchDTO.InstituteId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/DeleteAtheleteMatch_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int DeleteAtheleteCoaches_v4(int atheleteId)
        {

            int i = 0; 
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAtheleteCoaches_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.VarChar, 100);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/DeleteAtheleteCoaches_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return i;
        }





        internal static int AddAthleteInvitation_v4(AtheleteInvitationDTO atheleteInvitationDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteInvitation_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = atheleteInvitationDTO.AtheleteId;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = atheleteInvitationDTO.InstituteId;

                    SqlParameter InvitationDate = cmd.Parameters.Add("@InvitationDate", SqlDbType.DateTime);
                    InvitationDate.Direction = ParameterDirection.Input;
                    InvitationDate.Value = atheleteInvitationDTO.InvitationDate;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }


                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/AddAthleteInvitation_v4", "", ex.Message, "Exception", 0);
                throw;
            }
            return id;
        }


        internal static int UpdateAthleteAccept_v4(AtheleteInvitationDTO atheleteInvitationDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateAthleteAccept_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = atheleteInvitationDto.AtheleteId;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = atheleteInvitationDto.CoachEmailAddress;

                    SqlParameter accept = cmd.Parameters.Add("@Accept", SqlDbType.Bit);
                    accept.Direction = ParameterDirection.Input;
                    accept.Value = atheleteInvitationDto.Accept;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/UpdateAthleteAccept_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static AtheleteBadgesDTO GetAtheletesBlogsBadges_v4(int atheleteId)
        {

            AtheleteBadgesDTO badges = new AtheleteBadgesDTO();

            try
            {
                badges.AtheletesBadgesDTO = GetAtheleteBadges_v4(atheleteId);
                badges.AtheleteInvitationDTO = GetAtheleteInvitations_v4(atheleteId);
                badges.BlogsDTO = GetBlog_v4(atheleteId);

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAtheletesBlogsBadges_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return badges;
        }

        internal static BadgesDTO GetAtheleteBadges_v4(int atheleteId)
        {

            BadgesDTO atheleteBadgesDTO = new BadgesDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteLemonaidStandBadges_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        atheleteBadgesDTO.Academic = (bool)myData["AcademicStand"];
                        atheleteBadgesDTO.Social = (bool)myData["SocialStand"];
                        atheleteBadgesDTO.Athelete = (bool)myData["AtheleteStand"];
                        atheleteBadgesDTO.Diamond = (bool)myData["DiamondStand"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAtheleteBadges_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return atheleteBadgesDTO;
        }


        internal static List<GetAtheleteInvitationDTO> GetAtheleteInvitations_v4(int atheleteId)
        {

            List<GetAtheleteInvitationDTO> getAtheleteInvitationDTO = new List<GetAtheleteInvitationDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteInvitations_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        GetAtheleteInvitationDTO athInvDTO = new GetAtheleteInvitationDTO();
                        athInvDTO.UniversityName = (string)myData["Name"];
                        athInvDTO.InvitationDate = (string)myData["InvitedDate"];
                        getAtheleteInvitationDTO.Add(athInvDTO);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAtheleteInvitations_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return getAtheleteInvitationDTO;
        }


        internal static List<BlogDTO> GetBlog_v4(int atheleteId)
        {

            List<BlogDTO> blogs = new List<BlogDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstituteBlogs_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BlogDTO blogDTO = new BlogDTO();
                        blogDTO.Title = (string)myData["Title"];
                        blogDTO.Description = (string)myData["Description"];
                        blogDTO.ImageURL = (string)myData["ImageURL"];
                        blogDTO.URL = (string)myData["URL"];
                        blogDTO.Date = (DateTime)myData["CreatedDate"];
                        blogDTO.ShortDesc = myData["ShortDesc"].ToString();
                        blogDTO.Type = (myData["Type"] == DBNull.Value) ? string.Empty : (string)myData["Type"];
                        blogs.Add(blogDTO);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetBlog_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return blogs;
        }



        internal static VideoDTO DeleteAtheleteVideos_v4(int atheleteId, int VideoId)
        {
           VideoDTO v = new VideoDTO(); 

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAthleteVideo_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlParameter videoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    videoId.Direction = ParameterDirection.Input;
                    videoId.Value = VideoId; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    { 
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : (string)myData["VideoURL"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value) ? "" : (string)myData["ThumbnailURL"].ToString();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? "" : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.Count = (int)myData["CntVideos"];
                        v.VideoNumber = (int)myData["VideoVideonumber"];
                        v.IsFirstVideoDeleted = (int)myData["IsFirstDeleted"];
                        v.Duration = (int)myData["Duration"]; 
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/DeleteAtheleteVideos_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }

        internal static int AddVideos_v4(VideoDTO videoDTO)
        {
            List<VideoDTO> videos = new List<VideoDTO>();
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVideos_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = string.Empty;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    VideoURL.Direction = ParameterDirection.Input;
                    VideoURL.Value = videoDTO.VideoURL;

                    SqlParameter ThumbnailURL = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                    ThumbnailURL.Direction = ParameterDirection.Input;
                    ThumbnailURL.Value = videoDTO.ThumbnailURL;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = videoDTO.Status;

                    SqlParameter AtheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.VarChar, 100);
                    AtheleteId.Direction = ParameterDirection.Input;
                    AtheleteId.Value = videoDTO.AtheleteId;

                    SqlParameter videoFormat = cmd.Parameters.Add("@VideoFormat", SqlDbType.VarChar, 50);
                    videoFormat.Direction = ParameterDirection.Input;
                    videoFormat.Value = videoDTO.VideoFormat;

                    SqlParameter videoNumber = cmd.Parameters.Add("@videoNumber", SqlDbType.Int);
                    videoNumber.Direction = ParameterDirection.Input;
                    videoNumber.Value = videoDTO.VideoNumber;

                    SqlParameter duration = cmd.Parameters.Add("@duration", SqlDbType.VarChar, 50);
                    duration.Direction = ParameterDirection.Input;
                    duration.Value = videoDTO.Duration;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;
                     
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/AddVideos_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<VideoDTO> UpdateVideoThumbnail_v4(ThumbnailDTO thumbnailDTO)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                string ThumbnailURL = UsersDal.UploadImageFromBase64(thumbnailDTO.ThumbnailURL, thumbnailDTO.ThumbnailFormat);

                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateVideoThumbnail_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Thumbnailurl = cmd.Parameters.Add("@Thumbnailurl", SqlDbType.VarChar, 1024);
                    Thumbnailurl.Direction = ParameterDirection.Input;
                    Thumbnailurl.Value = ThumbnailURL;

                    SqlParameter AthleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteIdPar.Direction = ParameterDirection.Input;
                    AthleteIdPar.Value = thumbnailDTO.AthleteId;

                    SqlParameter VideoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    VideoId.Direction = ParameterDirection.Input;
                    VideoId.Value = thumbnailDTO.VideoId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : (string)myData["VideoURL"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value) ? "" : (string)myData["ThumbnailURL"].ToString();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? "" : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.Count = (int)myData["AtheleteVideoCount"];
                        videos.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/UpdateVideoThumbnail_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return videos;
        }



        internal static List<VideoDTO> GetAtheleteVideos_v4(int atheleteId)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteVideos_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : (string)myData["VideoURL"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value) ? "" : (string)myData["ThumbnailURL"].ToString();
                        v.UploadVideoUrl = (myData["VideoURL"] == DBNull.Value) ? "" : (string)myData["VideoURL"].ToString();
                        v.UploadThumbnailUrl = (myData["ThumbnailURL"] == DBNull.Value) ? "" : (string)myData["ThumbnailURL"].ToString();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? "" : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.Count = (int)myData["AtheleteVideoCount"];
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAtheleteVideos_v4", "", ex.Message, "Exception", 0);
                throw;
            }


            return videos;
        }


        internal static List<AthleteDTO> GetAllAthletesByCollegeAthelete_v4(int atheleteId, int offset, int max, string Description, string SearchText)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllAtheletesByCollegeAtheleteEmail_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;


                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;


                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlParameter description = cmd.Parameters.Add("@description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = Description;

                    SqlParameter searchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchText.Direction = ParameterDirection.Input;
                    searchText.Value = SearchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicLevel"]);
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["YearLevel"]);
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteRate"]);

                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Premium"]);
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.RightSwiped = ((int)myData["RightSwiped"] == 0) ? false : true;
                        BadgesDTO badges = UsersDalV4.GetAtheleteBadges_v4(v.Id);
                        v.AthleteBadges = badges;
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAllAthletesByCollegeAthelete_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return athletes;
        }


        internal static List<AthleteDTO> GetCollegeAtheleteFavourites_v4(int atheleteId, int offset, int max)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();
            DateTime dtAtheleteDate = new DateTime();
            DateTime dtInstituteDate = new DateTime();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCollegeAtheleteFavourites_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        dtAtheleteDate = (myData["AtheleteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["AtheleteDate"];
                        dtInstituteDate = (myData["InstituteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(myData["InstituteDate"]);
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.Liked = ((int)myData["Likes"] != 0) ? true : false;
                        if (v.Liked)
                        { v.FavDate = (dtAtheleteDate > dtInstituteDate) ? dtAtheleteDate : dtInstituteDate; }
                        else
                        { v.FavDate = dtAtheleteDate; }
                        // v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                        v.AtheleteId = (int)myData["AtheleteId"];
                        v.CoachId = (int)myData["CoachId"];
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.UniversityName = (string)myData["UniversityName"].ToString();
                        v.UniversityProfilePic = (string)myData["UniversityProfilePic"].ToString();
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetCollegeAtheleteFavourites_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return athletes;
        }

        internal static int AddAtheleteandCollegeAthleteMatch_v4(AtheleteCollegeAtheleteDTO atheleteMatchDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAtheleteCollegeAthleteMatch_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.VarChar, 500);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteMatchDto.AtheleteId;

                    SqlParameter collegeAtheleteId = cmd.Parameters.Add("@CollegeAtheleteId", SqlDbType.VarChar, 500);
                    collegeAtheleteId.Direction = ParameterDirection.Input;
                    collegeAtheleteId.Value = atheleteMatchDto.CollegeAtheleteId;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    status.Direction = ParameterDirection.Input;
                    status.Value = atheleteMatchDto.Status;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/AddAtheleteandCollegeAthleteMatch_v4", "", ex.Message, "Exception", 0);
                throw;
            }


            return id;
        }


        internal static int AddCollegeAtheleteandAtheleteMatch_v4(AtheleteCollegeAtheleteDTO atheleteMatchDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCollegeAtheleteAtheleteMatch_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.VarChar, 500);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteMatchDto.AtheleteId;

                    SqlParameter CollegeAtheleteId = cmd.Parameters.Add("@CollegeAtheleteId", SqlDbType.VarChar, 500);
                    CollegeAtheleteId.Direction = ParameterDirection.Input;
                    CollegeAtheleteId.Value = atheleteMatchDto.CollegeAtheleteId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = atheleteMatchDto.Status;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;
                    // id = (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/AddCollegeAtheleteandAtheleteMatch_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int DeleteAtheleteandCollegeAthleteMatch_v4(AtheleteCollegeAtheleteDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAtheleteCollegeAthleteMatch_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteMatchDTO.AtheleteId;

                    SqlParameter CollegeAtheleteId = cmd.Parameters.Add("@CollegeAtheleteId", SqlDbType.VarChar, 500);
                    CollegeAtheleteId.Direction = ParameterDirection.Input;
                    CollegeAtheleteId.Value = atheleteMatchDTO.CollegeAtheleteId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/DeleteAtheleteandCollegeAthleteMatch_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int DeleteCollegeAtheleteandAtheleteMatch_v4(AtheleteCollegeAtheleteDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCollegeAtheleteAtheleteMatch_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteMatchDTO.AtheleteId;

                    SqlParameter CollegeAtheleteId = cmd.Parameters.Add("@CollegeAtheleteId", SqlDbType.VarChar, 500);
                    CollegeAtheleteId.Direction = ParameterDirection.Input;
                    CollegeAtheleteId.Value = atheleteMatchDTO.CollegeAtheleteId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/DeleteCollegeAtheleteandAtheleteMatch_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static AthleteDTO GetAthleteProfile_v4(string emailaddress, int type)
        {
            AthleteDTO v = new AthleteDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteProfile_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter typePar = cmd.Parameters.Add("@type", SqlDbType.Int);
                    typePar.Direction = ParameterDirection.Input;
                    typePar.Value = type;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = 1;// (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivAddress"]);
                        v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivEmailAddress"]);
                        v.UniversityName = (myData["UniversityName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UniversityName"]);
                        v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnviPhoneNumber"]);
                        v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivProfilePic"]);
                        v.UniversitySize = (myData["UnivSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivSize"]);
                        v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClassificationName"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"]; 
                       
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAthleteProfile_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static int DeleteCollegeAtheleteAtheletes_v4(int atheleteId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCollegeAtheleteAtheletes_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/DeleteCollegeAtheleteAtheletes_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return i;
        }


        internal static AthleteDTO AddAthlete_v4(AthleteDTO athleteDTO)
        {
            AthleteDTO v = new AthleteDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthlete_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = athleteDTO.Name;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    Gender.Direction = ParameterDirection.Input;
                    Gender.Value = athleteDTO.Gender;

                    SqlParameter School = cmd.Parameters.Add("@School", SqlDbType.VarChar, 100);
                    School.Direction = ParameterDirection.Input;
                    School.Value = (athleteDTO.School == null) ? string.Empty : athleteDTO.School;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = athleteDTO.Emailaddress;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = (athleteDTO.Description == null) ? string.Empty : athleteDTO.Description;

                    SqlParameter GPA = cmd.Parameters.Add("@GPA", SqlDbType.Float);
                    GPA.Direction = ParameterDirection.Input;
                    GPA.Value = athleteDTO.GPA;

                    SqlParameter Social = cmd.Parameters.Add("@Social", SqlDbType.Int);
                    Social.Direction = ParameterDirection.Input;
                    Social.Value = athleteDTO.Social;

                    SqlParameter SAT = cmd.Parameters.Add("@SAT", SqlDbType.Int);
                    SAT.Direction = ParameterDirection.Input;
                    SAT.Value = athleteDTO.SAT;

                    SqlParameter ACT = cmd.Parameters.Add("@ACT", SqlDbType.Int);
                    ACT.Direction = ParameterDirection.Input;
                    ACT.Value = athleteDTO.ACT;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = athleteDTO.InstituteId;

                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = athleteDTO.ProfilePicURL;

                    SqlParameter DOB = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    DOB.Direction = ParameterDirection.Input;
                    DOB.Value = athleteDTO.DOB;

                    SqlParameter Address = cmd.Parameters.Add("@Address", SqlDbType.VarChar, 100);
                    Address.Direction = ParameterDirection.Input;
                    Address.Value = athleteDTO.Address;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = athleteDTO.FacebookId;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = athleteDTO.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = athleteDTO.Longitude;


                    SqlParameter AcademicLevel = cmd.Parameters.Add("@AcademicLevel", SqlDbType.Int);
                    AcademicLevel.Direction = ParameterDirection.Input;
                    AcademicLevel.Value = athleteDTO.AcademicLevel;


                    SqlParameter YearLevel = cmd.Parameters.Add("@YearLevel", SqlDbType.Int);
                    YearLevel.Direction = ParameterDirection.Input;
                    YearLevel.Value = athleteDTO.YearLevel;


                    SqlParameter Premium = cmd.Parameters.Add("@Premium", SqlDbType.Int);
                    Premium.Direction = ParameterDirection.Input;
                    Premium.Value = athleteDTO.Premium;

                    SqlParameter Height1 = cmd.Parameters.Add("@Height1", SqlDbType.VarChar, 20);
                    Height1.Direction = ParameterDirection.Input;
                    Height1.Value = (athleteDTO.Height == null) ? string.Empty : athleteDTO.Height;

                    SqlParameter Height2 = cmd.Parameters.Add("@Height2", SqlDbType.VarChar, 20);
                    Height2.Direction = ParameterDirection.Input;
                    Height2.Value = (athleteDTO.Height2 == null) ? string.Empty : athleteDTO.Height2;

                    SqlParameter HeightType = cmd.Parameters.Add("@HeightType", SqlDbType.VarChar, 20);
                    HeightType.Direction = ParameterDirection.Input;
                    HeightType.Value = (athleteDTO.HeightType == null) ? string.Empty : athleteDTO.HeightType;

                    SqlParameter Weight = cmd.Parameters.Add("@Weight", SqlDbType.VarChar, 20);
                    Weight.Direction = ParameterDirection.Input;
                    Weight.Value = (athleteDTO.Weight == null) ? string.Empty : athleteDTO.Weight;

                    SqlParameter WeightType = cmd.Parameters.Add("@WeightType", SqlDbType.VarChar, 20);
                    WeightType.Direction = ParameterDirection.Input;
                    WeightType.Value = (athleteDTO.WeightType == null) ? string.Empty : athleteDTO.WeightType;

                    SqlParameter WingSpan = cmd.Parameters.Add("@WingSpan", SqlDbType.VarChar, 20);
                    WingSpan.Direction = ParameterDirection.Input;
                    WingSpan.Value = (athleteDTO.WingSpan == null) ? string.Empty : athleteDTO.WingSpan;

                    SqlParameter WingSpanType = cmd.Parameters.Add("@WingSpanType", SqlDbType.VarChar, 20);
                    WingSpanType.Direction = ParameterDirection.Input;
                    WingSpanType.Value = (athleteDTO.WingSpanType == null) ? string.Empty : athleteDTO.WingSpanType;

                    SqlParameter ShoeSize = cmd.Parameters.Add("@ShoeSize", SqlDbType.VarChar, 20);
                    ShoeSize.Direction = ParameterDirection.Input;
                    ShoeSize.Value = (athleteDTO.ShoeSize == null) ? string.Empty : athleteDTO.ShoeSize;

                    SqlParameter ShoeSizeType = cmd.Parameters.Add("@ShoeSizeType", SqlDbType.VarChar, 20);
                    ShoeSizeType.Direction = ParameterDirection.Input;
                    ShoeSizeType.Value = (athleteDTO.ShoeSizeType == null) ? string.Empty : athleteDTO.ShoeSizeType;

                    SqlParameter ClubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    ClubName.Direction = ParameterDirection.Input;
                    ClubName.Value = string.IsNullOrWhiteSpace(athleteDTO.ClubName) ? string.Empty : athleteDTO.ClubName;

                    SqlParameter CoachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    CoachName.Direction = ParameterDirection.Input;
                    CoachName.Value = string.IsNullOrWhiteSpace(athleteDTO.CoachName) ? string.Empty : athleteDTO.CoachName;

                    SqlParameter CoachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    CoachEmailId.Direction = ParameterDirection.Input;
                    CoachEmailId.Value = string.IsNullOrWhiteSpace(athleteDTO.CoachEmailId) ? string.Empty : athleteDTO.CoachEmailId;

                    SqlParameter AtheleteType = cmd.Parameters.Add("@AtheleteType", SqlDbType.Int);
                    AtheleteType.Direction = ParameterDirection.Input;
                    AtheleteType.Value = athleteDTO.AtheleteType;

                    SqlParameter Active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    Active.Direction = ParameterDirection.Input;
                    Active.Value = athleteDTO.Active;

                    SqlParameter Major = cmd.Parameters.Add("@Major", SqlDbType.VarChar, 50);
                    Major.Direction = ParameterDirection.Input;
                    Major.Value = string.IsNullOrWhiteSpace(athleteDTO.Major) ? string.Empty : athleteDTO.Major;

                    SqlParameter Phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    Phonenumber.Direction = ParameterDirection.Input;
                    Phonenumber.Value = string.IsNullOrWhiteSpace(athleteDTO.Phonenumber) ? string.Empty : athleteDTO.Phonenumber;

                    SqlParameter Username = cmd.Parameters.Add("@Username", SqlDbType.VarChar, 500);
                    Username.Direction = ParameterDirection.Input;
                    Username.Value = string.IsNullOrWhiteSpace(athleteDTO.UserName) ? string.Empty : athleteDTO.UserName;

                    SqlParameter LoginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    LoginType.Direction = ParameterDirection.Input;
                    LoginType.Value = (athleteDTO.LoginType == null) ? 0 : athleteDTO.LoginType;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = (athleteDTO.DeviceType == null) ? string.Empty : athleteDTO.DeviceType;

                    SqlParameter DeviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 50);
                    DeviceId.Direction = ParameterDirection.Input;
                    DeviceId.Value = (athleteDTO.DeviceId == null) ? string.Empty : athleteDTO.DeviceId;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = 1;
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivAddress"]);
                        v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivEmailAddress"]);
                        v.UniversityName = (myData["UniversityName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UniversityName"]);
                        v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnviPhoneNumber"]);
                        v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivProfilePic"]);
                        v.UniversitySize = (myData["UnivSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivSize"]);
                        v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClassificationName"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/AddAthlete_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }



        internal static List<CoachDTO> GetAllCoachesPagingWithSearch_v4(int atheleteId, int offset, int max, int NCAA, int Conference, int Admission, string Description, string SearchText)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllInstitutesByAtheleteEmail_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteidPar = cmd.Parameters.Add("@atheleteid", SqlDbType.Int);
                    atheleteidPar.Direction = ParameterDirection.Input;
                    atheleteidPar.Value = atheleteId;

                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlParameter ncaa = cmd.Parameters.Add("@ncaa", SqlDbType.Int);
                    ncaa.Direction = ParameterDirection.Input;
                    ncaa.Value = NCAA;

                    SqlParameter conference = cmd.Parameters.Add("@conference", SqlDbType.Int);
                    conference.Direction = ParameterDirection.Input;
                    conference.Value = Conference;

                    SqlParameter admission = cmd.Parameters.Add("@admission", SqlDbType.Int);
                    admission.Direction = ParameterDirection.Input;
                    admission.Value = Admission;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = Description;

                    SqlParameter searchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchText.Direction = ParameterDirection.Input;
                    searchText.Value = SearchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["CoachEmail"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.FacebookId = (string)myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AdType = 2;
                        v.Recommended = ((int)myData["recommended"] == 0) ? false : true;
                        v.RightSwiped = ((int)myData["RightSwiped"] == 0) ? false : true;
                        Coaches.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAllCoachesPagingWithSearch_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }



        internal static List<AthleteDTO> GetCollegeAtheletes_v4(string SearchText, int atheleteId)
        {
            List<AthleteDTO> Atheletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCollegeAtheletes_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter searchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchText.Direction = ParameterDirection.Input;
                    searchText.Value = SearchText;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.RightSwiped = ((int)myData["RightSwiped"] == 0) ? false : true;
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        v.SchoolCoachName = (myData["SchoolCoachName"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachName"];
                        v.SchoolCoachEmail = (myData["SchoolCoachEmail"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachEmail"];
                        v.SchoolZipCode = (myData["SchoolZipCode"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolZipCode"];
                        v.GapyearLevel = (myData["GapyearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["GapyearLevel"]);
                        v.GapyearDescription = (myData["GapyearDescription"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["GapyearDescription"];
                        v.Country = (myData["Country"] == DBNull.Value) ? string.Empty : (string)myData["Country"];
                        v.CountryId = (myData["CountryId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CountryId"]);
                        v.AP = (myData["AP"] == DBNull.Value) ? 0.0 : Convert.ToDouble(myData["AP"]); 
                        v.AthleteTransition = (myData["AthleteTransition"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteTransition"]);
                        v._20YardShuttleRun = (myData["20YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["20YardShuttleRun"];
                        v._60YardShuttleRun = (myData["60YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["60YardShuttleRun"];
                        v.KneelingMedBallToss = (myData["KneelingMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["KneelingMedBallToss"];
                        v.RotationalMedBallToss = (myData["RotationalMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["RotationalMedBallToss"];
                        Atheletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetCollegeAtheletes_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return Atheletes;
        }


        internal static List<QuestionaireDTO> GetQuestionaire_v4(int atheleteId)
        {

            List<QuestionaireDTO> questionaire = new List<QuestionaireDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetQuestionaire_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AtheleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AtheleteIdPar.Direction = ParameterDirection.Input;
                    AtheleteIdPar.Value = atheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        QuestionaireDTO v = new QuestionaireDTO();
                        v.QuestionId = (int)myData["id"];
                        v.Question = (string)myData["Question"].ToString();

                        questionaire.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetQuestionaire_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return questionaire;
        }


        internal static int AddCoachMatch_v4(AtheleteMatchDTO atheleteMatchDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddInstAtheleteMatch_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FacebookId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = atheleteMatchDto.AtheleteId;

                    SqlParameter CoachEmail = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    CoachEmail.Direction = ParameterDirection.Input;
                    CoachEmail.Value = atheleteMatchDto.InstituteId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = atheleteMatchDto.Status;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/AddCoachMatch_v4", "", ex.Message, "Exception", 0);
                throw;
            }
    

            return id;
        }


        internal static int DeleteCoachMatch_v4(AtheleteMatchDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteInstAtheleteMatch_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = atheleteMatchDTO.AtheleteId;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = atheleteMatchDTO.InstituteId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/DeleteCoachMatch_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<VideoDTO> GetAtheleteIdealVideos_v4(string emailaddress, int logintype)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteIdealVideos_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter logintypepar = cmd.Parameters.Add("@logintype", SqlDbType.Int);
                    logintypepar.Direction = ParameterDirection.Input;
                    logintypepar.Value = logintype;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? string.Empty : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : (int)myData["VideoType"];
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : (int)myData["Duration"];
                        v.VideoNumber = (myData["VideoNumber"] == DBNull.Value) ? 0 : (int)myData["VideoNumber"];
                        v.Count = (int)myData["AtheleteVideoCount"];
                        
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAtheleteIdealVideos_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return videos;
        }


        internal static CoachUserDTO AddCoachUser_v4(CoachUserDTO coachUserDTO)
        {


            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoach_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FacebookId = cmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 100);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = coachUserDTO.Emailaddress;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = coachUserDTO.InstituteId;

                    SqlParameter Password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 100);
                    Password.Direction = ParameterDirection.Input;
                    Password.Value =  Encrypt(coachUserDTO.Password);
                   // Password.Value = coachUserDTO.Password;

                    SqlParameter active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    active.Direction = ParameterDirection.Input;
                    active.Value = coachUserDTO.Active;

                    SqlParameter teamType = cmd.Parameters.Add("@TeamType", SqlDbType.Int);
                    teamType.Direction = ParameterDirection.Input;
                    teamType.Value = coachUserDTO.TeamType;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = (coachUserDTO.DeviceType == null) ? string.Empty : coachUserDTO.DeviceType;

                    SqlParameter DeviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 500);
                    DeviceId.Direction = ParameterDirection.Input;
                    DeviceId.Value = (coachUserDTO.DeviceId == null) ? string.Empty : coachUserDTO.DeviceId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    
                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        coachUserDTO.CoachId = (int)myData["Id"];
                        coachUserDTO.ActivationCode = (myData["activation_code"] == DBNull.Value) ? "" : (string)myData["activation_code"];
                        coachUserDTO.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        coachUserDTO.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        coachUserDTO.Name = (myData["UniversityName"] == DBNull.Value) ? "" : (string)myData["UniversityName"];
                        int Head = (int)myData["HeadCoach"];
                        coachUserDTO.HeadCoach = (Head == 1) ? true : false;
                        coachUserDTO.Password = (myData["Password"] == DBNull.Value) ? "" : (string)myData["Password"];
                        //coachUserDTO.DeviceId = (myData["DeviceId"] == DBNull.Value) ? "" : (string)myData["DeviceId"];
                        //coachUserDTO.DeviceType = (myData["DeviceType"] == DBNull.Value) ? "" : (string)myData["DeviceType"];
                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/AddCoachUser_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return coachUserDTO;
        }

        internal static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        internal static string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        internal static int GetChatCount(int senderId, int receiverId)
        {

            int chatCount = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetChatCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SenderIdPar = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderIdPar.Direction = ParameterDirection.Input;
                    SenderIdPar.Value = senderId;

                    SqlParameter ReceiverIdPar = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverIdPar.Direction = ParameterDirection.Input;
                    ReceiverIdPar.Value = receiverId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        chatCount = (int)myData["ChatCount"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetChatCount", "", ex.Message, "Exception", 0);
                throw;
            }

            return chatCount;
        }

        internal static int UpdateAthleteBadgesStatus_v4(AtheleteBadgesDTO athleteBadges)
        {
            int id = 0;
            try
            {
                using (SqlConnection myConn = UsersDal.ConnectToDb())
                {
                    if (null != myConn)
                    {
                        SqlCommand cmd = new SqlCommand("[UpdateAthleteBadgesStatus_v4]", myConn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter AtheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                        AtheleteId.Direction = ParameterDirection.Input;
                        AtheleteId.Value = athleteBadges.AtheleteId;

                        SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.VarChar, 50);
                        Status.Direction = ParameterDirection.Input;
                        Status.Value = athleteBadges.Status;

                        SqlParameter BadgeTypeId = cmd.Parameters.Add("@BadgeTypeId", SqlDbType.Int);
                        BadgeTypeId.Direction = ParameterDirection.Input;
                        BadgeTypeId.Value = athleteBadges.BadgeTypeId;

                        SqlParameter BadgeCount = cmd.Parameters.Add("@BadgeCount", SqlDbType.Int);
                        BadgeCount.Direction = ParameterDirection.Input;
                        BadgeCount.Value = athleteBadges.BadgeCount;

                        SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                        Id.Direction = ParameterDirection.Output;

                        cmd.ExecuteNonQuery();

                        id = (int)cmd.Parameters["@Id"].Value;


                    }//myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/UpdateAthleteBadgesStatus_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int UpdateCoachProfile_v4(CoachProfileDTO coachProfileDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachProfile_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachid = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachid.Direction = ParameterDirection.Input;
                    coachid.Value = coachProfileDTO.CoachId;

                    SqlParameter description = cmd.Parameters.Add("@description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = coachProfileDTO.Description;

                    SqlParameter PreferredStrokes = cmd.Parameters.Add("@PreferredStrokes", SqlDbType.VarChar, 500);
                    PreferredStrokes.Direction = ParameterDirection.Input;
                    PreferredStrokes.Value = coachProfileDTO.PreferredStrokes;

                    SqlParameter PhoneNumber = cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar, 20);
                    PhoneNumber.Direction = ParameterDirection.Input;
                    PhoneNumber.Value = coachProfileDTO.Phonenumber;

                    SqlParameter AcademicRate = cmd.Parameters.Add("@AcademicRate", SqlDbType.Int);
                    AcademicRate.Direction = ParameterDirection.Input;
                    AcademicRate.Value = coachProfileDTO.AcademicRate;

                    SqlParameter FacebookURL = cmd.Parameters.Add("@FacebookURL", SqlDbType.VarChar, 2000);
                    FacebookURL.Direction = ParameterDirection.Input;
                    FacebookURL.Value = coachProfileDTO.FacebookURL;

                    SqlParameter TwitterURL = cmd.Parameters.Add("@TwitterURL", SqlDbType.VarChar, 2000);
                    TwitterURL.Direction = ParameterDirection.Input;
                    TwitterURL.Value = coachProfileDTO.TwitterURL;

                    SqlParameter InstagramURL = cmd.Parameters.Add("@InstagramURL", SqlDbType.VarChar, 2000);
                    InstagramURL.Direction = ParameterDirection.Input;
                    InstagramURL.Value = coachProfileDTO.InstagramURL;

                    SqlParameter YoutubeURL = cmd.Parameters.Add("@YoutubeURL", SqlDbType.VarChar, 2000);
                    YoutubeURL.Direction = ParameterDirection.Input;
                    YoutubeURL.Value = coachProfileDTO.YoutubeURL;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/UpdateCoachProfile_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static CoachProfileDTO GetCoachProfile_v4(int coachId)
        {
            CoachProfileDTO v = new CoachProfileDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachProfile_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter CoachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    CoachId.Direction = ParameterDirection.Input;
                    CoachId.Value = coachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.CoachId = (int)myData["ID"];
                        v.Description = (myData["description"] == DBNull.Value) ? string.Empty : (string)myData["description"].ToString();
                        v.PreferredStrokes = (myData["PreferredStrokes"] == DBNull.Value) ? string.Empty : (string)myData["PreferredStrokes"].ToString();
                        v.Emailaddress = (myData["emailaddress"] == DBNull.Value) ? string.Empty : (string)myData["emailaddress"].ToString();
                        int Head = (int)myData["HeadCoach"];
                        v.HeadCoach = (Head == 1) ? true : false;
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.payment_paid = (myData["Payment_Paid"] == DBNull.Value) ? false : (bool)myData["Payment_Paid"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.FacebookURL = (myData["FacebookURL"] == DBNull.Value) ? string.Empty : (string)myData["FacebookURL"];
                        v.TwitterURL = (myData["TwitterURL"] == DBNull.Value) ? string.Empty : (string)myData["TwitterURL"];
                        v.InstagramURL = (myData["InstagramURL"] == DBNull.Value) ? string.Empty : (string)myData["InstagramURL"];
                        v.YoutubeURL = (myData["YoutubeURL"] == DBNull.Value) ? string.Empty : (string)myData["YoutubeURL"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetCoachProfile_v4", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static void GetAthleteDetails_v4(int athleteId, ref string athleteName, ref string athleteEmailAddress, ref string deviceId, ref string deviceName, ref int loginType, ref string username)
        {

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteDetails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter AthleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteId.Direction = ParameterDirection.Input;
                    AthleteId.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {
                        athleteName = (myData["Name"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Name"]);
                        athleteEmailAddress = (myData["EmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EmailAddress"]);
                        deviceId = (myData["DeviceId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceId"]);
                        deviceName = (myData["DeviceName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceName"]);
                        loginType = (myData["LoginType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["LoginType"]);
                        username = (myData["Username"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Username"]);
                    }
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAthleteDetails_v4", "", ex.Message, "Exception", 0);
                throw;
            }
        }

        internal static void UpdateCoachPassword(int coachId, string password)
        {
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachEncryptPassword]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 10000;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@coachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = coachId;

                    SqlParameter passwordPar = cmd.Parameters.Add("@password", SqlDbType.VarChar, 100);
                    passwordPar.Direction = ParameterDirection.Input;
                    passwordPar.Value = Encrypt(password); 

                    SqlDataReader myData = cmd.ExecuteReader();
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/UpdateCoachPassword", "", ex.Message, "Exception", 0);
                throw;
            }
             
        }
        internal static List<CoachUserDTO> GetAllCoaches()
        {
            List<CoachUserDTO> coaches = new List<CoachUserDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllCoaches]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 10000;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachUserDTO  v = new  CoachUserDTO();
                        v.CoachId = (int)myData["ID"]; 
                        v.Password = (myData["Password"] == DBNull.Value) ? string.Empty : (string)myData["Password"].ToString();
                        v.EncryptedPassword = (myData["EncryptPassword"] == DBNull.Value) ? string.Empty : (string)myData["EncryptPassword"].ToString(); 
                        coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetAllCoaches", "", ex.Message, "Exception", 0);
                throw;
            }

            return coaches;
        }

    }
}