﻿using NReco.VideoConverter;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Markup;
using ImageMagick;
using System.Net;
 

namespace Lemonaid_web_api
{
    public partial class TestVideoThumb : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GetVideoThumbnail();
        }

        private void GetVideoThumbnail()
        {
            //string videoUrl =  "https://ikkosvideos.blob.core.windows.net/videosstore/97571460416473.mp4"; //Server.MapPath("~/Content/97571460416473.mp4");
            WebClient webClient = new WebClient();
            webClient.DownloadFile("https://newchurchblob.blob.core.windows.net:443/asset-7107445d-1500-80c4-cf80-f1e5c6593aed/fn1250594446.mp4?sv=2012-02-12&sr=c&si=6248bc2e-ca0e-4a24-876d-ce59b915f5e6&sig=rLM0OA4hQ%2FR%2FCgmng6YmMN6ZcAxwt0gkBFmPKh2Mirg%3D&se=2017-01-28T07%3A23%3A51Z", Server.MapPath("~/Content/Video.mp4"));
            string videoUrl = Server.MapPath("~/Content/Video.mp4");
            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            int totDuration = 5;
            float startPoint = 0;
            float dur = (float)totDuration / 6;
            float frame = 0;
            Bitmap[] imgBitmapArray = new Bitmap[5];
            for (int i = 0; i < 5; i++)
            {
                frame = startPoint + (float)Math.Round(dur, 2);
                string thumbnailJPEGpath = Server.MapPath("~/Content/videoThumb" + i + ".jpg");
                ffMpeg.GetVideoThumbnail(videoUrl, thumbnailJPEGpath, frame);
                startPoint = startPoint + (float)Math.Round(dur, 2);
                System.Drawing.Bitmap bmp = new Bitmap(thumbnailJPEGpath);
                imgBitmapArray[i] = bmp;
            }

            using (MagickImageCollection collection = new MagickImageCollection())
            {
                int i = 0;
                foreach (Bitmap bmpImage in imgBitmapArray)
                {
                    // Add first image and set the animation delay to 100ms
                    collection.Add(new MagickImage(bmpImage));
                    collection[i].AnimationDelay = 50;
                    //collection[i].Flip();
                    i++;
                }

                // Optionally reduce colors
                QuantizeSettings settings = new QuantizeSettings();
                settings.Colors = 256;
                collection.Quantize(settings);
                // Optionally optimize the images (images should have the same size).
                collection.Optimize();
                // Save gif
                string outPutPath = Server.MapPath("~/Content/");
                collection.Write(outPutPath + "Animated.gif");
            }
        }

     

    }

}