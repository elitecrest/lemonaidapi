﻿using LemonaidPortal.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace LemonaidPortal.Controllers
{
    public class BlogController : Controller
    {
        string baseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
        // GET: Blog
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(baseURL);

                    client.DefaultRequestHeaders.Accept.Add(
                       new MediaTypeWithQualityHeaderValue("application/json"));


                    BlogDTO blog = new BlogDTO();
                    blog.Id = Convert.ToInt32(collection["Id"]);
                    blog.Title = collection["Title"].Trim();
                    blog.Description = collection["Description"].Trim();
                    blog.ImageURL = collection["ImageURL"].Trim();
                    blog.URL = collection["URL"].Trim();
                    blog.Type = "Blogs";
                    HttpResponseMessage response1 = client.PostAsJsonAsync("Blogs/AddBlog", blog).Result;

                    if (response1.IsSuccessStatusCode)
                    {


                        Response.Write("Added Successfully");
                    }
                    return RedirectToAction("ListBlogData");
                }

            }
            catch
            {
                return View();
            }
            return View();
        }


        [HttpGet]
        public ActionResult ListBlogData()
        { 
            return View(FetchData());
        }

        [HttpGet]
        public List<BlogDTO> FetchData()
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // HTTP GET
            BlogDTO obje = new BlogDTO();
            HttpResponseMessage response = client.GetAsync("Blogs/GetBlog").Result;
            List<BlogDTO> objBlog = new List<BlogDTO>();
            if (response.IsSuccessStatusCode)
            {
                Stream st = response.Content.ReadAsStreamAsync().Result;
                StreamReader reader = new StreamReader(st);
                string content = reader.ReadToEnd();

                var values = JsonConvert.DeserializeObject<dynamic>(content);
                var resp = values["Response"];
                if (resp != null)
                {
                    foreach (var p in resp)
                    {
                        BlogDTO obj = new BlogDTO();
                        obj.Id = p.Id;
                        obj.Title = p.Title;
                        obj.Description = p.Description;
                        obj.ImageURL = p.ImageURL;
                        obj.URL = p.URL;
                        obj.Date = p.Date;
                        obj.Type = p.Type;
                        objBlog.Add(obj);

                    }
                }
            }
            return objBlog.ToList();
        }

        public ActionResult Edit(int id)
        {
            List<BlogDTO> BlogList = new List<BlogDTO>();
            BlogList = FetchData();
            BlogDTO objBlog = BlogList.Find(x => x.Id == id);
            if (objBlog == null)
            {
                return HttpNotFound();
            }
            return View(objBlog);
        }

        //
        // POST: /Church/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(baseURL);

                    client.DefaultRequestHeaders.Accept.Add(
                       new MediaTypeWithQualityHeaderValue("application/json"));


                    BlogDTO blog = new BlogDTO();
                    blog.Id = Convert.ToInt32(id);
                    blog.Title = collection["Title"].Trim();
                    blog.Description = collection["Description"].Trim();
                    blog.ImageURL = collection["ImageURL"].Trim();
                    blog.URL = collection["URL"].Trim();
                    blog.Type = "Blogs";
                    blog.CoachId = 0;
                    HttpResponseMessage response1 = client.PostAsJsonAsync("Blogs/EditBlog", blog).Result;

                    if (response1.IsSuccessStatusCode)
                    { 
                        Response.Write("Updated Successfully");
                    }
                    return RedirectToAction("ListBlogData");
                }

            }
            catch
            {
                return View();
            }
            return View();
             
        }

        //
        // GET: /Church/Delete/5

        public ActionResult Delete(int id)
        {
            List<BlogDTO> churchList = new List<BlogDTO>();
            churchList = FetchData();
            BlogDTO objChurch = churchList.Find(x => x.Id == id);
            if (objChurch == null)
            {
                return HttpNotFound();
            }
            return View(objChurch);

        }

        //
        // POST: /Church/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseURL);

                client.DefaultRequestHeaders.Accept.Add(
                   new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.DeleteAsync("Blogs/DeleteBlog?Blogid=" + id).Result;

                if (response.IsSuccessStatusCode)
                {

                    Response.Write("Deleted Successfully");
                }
                return RedirectToAction("ListBlogData");
            }
            catch
            {
                return View();
            }
        }
    }
}