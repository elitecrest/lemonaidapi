﻿
using Lemonaid_web_api.Controllers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Lemonaid_web_api.Models
{
    public class DAL2V5
    {
        internal static int AddAthleteVideoContent(MediaContent mediaContent)
        {

            int mediaid = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteVideoContent]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter videoURLPar = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    videoURLPar.Direction = ParameterDirection.Input;
                    videoURLPar.Value = mediaContent.VideoURL;

                    SqlParameter formatPar = cmd.Parameters.Add("@Format", SqlDbType.VarChar, 50);
                    formatPar.Direction = ParameterDirection.Input;
                    formatPar.Value = mediaContent.VideoFormat;

                    SqlParameter durationPar = cmd.Parameters.Add("@Duration", SqlDbType.Int);
                    durationPar.Direction = ParameterDirection.Input;
                    durationPar.Value = mediaContent.Duration;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = mediaContent.AthleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        mediaid = (int)myData["Id"];
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/AddAthleteVideoContent", "", ex.Message, "Exception", 0);
            }


            return mediaid;
        }

       
        internal static int UpdateAthleteThumbnailContent(MediaContent mediaContent)
        {

            int mediaid = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();


                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateAthleteThumbnailContent]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    namePar.Direction = ParameterDirection.Input;
                    namePar.Value = mediaContent.Name;

                    SqlParameter thumbnailURLPar = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                    thumbnailURLPar.Direction = ParameterDirection.Input;
                    thumbnailURLPar.Value = mediaContent.ThumbnailURL;

                    SqlParameter videoTypePar = cmd.Parameters.Add("@VideoType", SqlDbType.Int);
                    videoTypePar.Direction = ParameterDirection.Input;
                    videoTypePar.Value = mediaContent.VideoType;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = mediaContent.AthleteId;

                    SqlParameter captionPar = cmd.Parameters.Add("@Caption", SqlDbType.VarChar, 500);
                    captionPar.Direction = ParameterDirection.Input;
                    captionPar.Value = mediaContent.Caption;

                    SqlParameter latitudePar = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 100);
                    latitudePar.Direction = ParameterDirection.Input;
                    latitudePar.Value = mediaContent.Latitude;

                    SqlParameter longitudePar = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 100);
                    longitudePar.Direction = ParameterDirection.Input;
                    longitudePar.Value = mediaContent.Longitude;

                    SqlParameter mediacontentidPar = cmd.Parameters.Add("@mediacontentid", SqlDbType.Int);
                    mediacontentidPar.Direction = ParameterDirection.Input;
                    mediacontentidPar.Value = mediaContent.MediaContentId;

                    SqlParameter recordedDatePar = cmd.Parameters.Add("@RecordedDate", SqlDbType.DateTime);
                    recordedDatePar.Direction = ParameterDirection.Input;
                    recordedDatePar.Value = mediaContent.RecordedDate;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        mediaid = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/UpdateAthleteThumbnailContent", "", ex.Message, "Exception", 0);
            }

            return mediaid;
        }

       

        internal static int AddCoachVideoContent(MediaContent mediaContent)
        {

            int mediaid = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoachVideoContent]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter videoURLPar = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    videoURLPar.Direction = ParameterDirection.Input;
                    videoURLPar.Value = mediaContent.VideoURL;

                    SqlParameter formatPar = cmd.Parameters.Add("@Format", SqlDbType.VarChar, 50);
                    formatPar.Direction = ParameterDirection.Input;
                    formatPar.Value = mediaContent.VideoFormat;

                    SqlParameter durationPar = cmd.Parameters.Add("@Duration", SqlDbType.Int);
                    durationPar.Direction = ParameterDirection.Input;
                    durationPar.Value = mediaContent.Duration;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = mediaContent.CoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        mediaid = (int)myData["Id"];
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/AddCoachVideoContent", "", ex.Message, "Exception", 0);
            }


            return mediaid;
        }



        internal static int UpdateCoachThumbnailContent(MediaContent mediaContent)
        {

            int mediaid = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();


                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachThumbnailContent]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    namePar.Direction = ParameterDirection.Input;
                    namePar.Value = mediaContent.Name;

                    SqlParameter thumbnailURLPar = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                    thumbnailURLPar.Direction = ParameterDirection.Input;
                    thumbnailURLPar.Value = mediaContent.ThumbnailURL;

                    SqlParameter videoTypePar = cmd.Parameters.Add("@VideoType", SqlDbType.Int);
                    videoTypePar.Direction = ParameterDirection.Input;
                    videoTypePar.Value = mediaContent.VideoType;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = mediaContent.CoachId;

                    SqlParameter captionPar = cmd.Parameters.Add("@Caption", SqlDbType.VarChar, 500);
                    captionPar.Direction = ParameterDirection.Input;
                    captionPar.Value = mediaContent.Caption;

                    SqlParameter latitudePar = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 100);
                    latitudePar.Direction = ParameterDirection.Input;
                    latitudePar.Value = mediaContent.Latitude;

                    SqlParameter longitudePar = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 100);
                    longitudePar.Direction = ParameterDirection.Input;
                    longitudePar.Value = mediaContent.Longitude;

                    SqlParameter mediacontentidPar = cmd.Parameters.Add("@mediacontentid", SqlDbType.Int);
                    mediacontentidPar.Direction = ParameterDirection.Input;
                    mediacontentidPar.Value = mediaContent.MediaContentId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        mediaid = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/UpdateCoachThumbnailContent", "", ex.Message, "Exception", 0);
            }

            return mediaid;
        }

        internal static List<MediaContent> GetAthleteMediaContentById(int athleteId)
        {
            List<MediaContent> lstAthleteMedia = new List<MediaContent>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteMediaContentById]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MediaContent v = new MediaContent();
                        v.AthleteId = Convert.ToInt16(myData["AthleteId"]);
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? string.Empty : myData["VideoURL"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : myData["ThumbnailURL"].ToString();
                        v.VideoFormat = (myData["Format"] == DBNull.Value) ? string.Empty : myData["Format"].ToString();
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Duration"]);
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["VideoType"]);
                        v.Caption = (myData["Caption"] == DBNull.Value) ? string.Empty : myData["Caption"].ToString();
                        v.Latitude = (myData["Latitude"] == DBNull.Value) ? string.Empty : myData["Latitude"].ToString();
                        v.Longitude = (myData["Latitude"] == DBNull.Value) ? string.Empty : myData["Latitude"].ToString();
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty : myData["Name"].ToString();
                        v.MediaContentId =  Convert.ToInt16(myData["Id"]);
                        v.IsShareVideo = (myData["isShareVideo"] == DBNull.Value) ? false :  Convert.ToBoolean(myData["isShareVideo"]);
                        lstAthleteMedia.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetAthleteMediaContentById", "", ex.Message, "Exception", 0);
            }

            return lstAthleteMedia;
        }

        //internal static CoachesBadgesDTO GetCoachBlogsBadges(string emailaddress, int SportId)
        //{

        //    CoachV5Controller coachV5 = new CoachV5Controller();
        //    AthleteController athCtrl = new AthleteController();
        //    CoachesBadgesDTO badges = new CoachesBadgesDTO();

        //    try
        //    {
        //        badges.coachBadgesDTO = UsersDal.GetInstituteBadges(emailaddress);
        //        badges.BlogsDTO = UsersDal.GetBlog(emailaddress);
        //        var atheletes = DAL2V4.GetCoachesFavourites(emailaddress, 0, 100, SportId);
        //        atheletes = coachV5.AddVideosToList(atheletes);
        //        foreach (var r in atheletes)
        //        {
        //            r.AthleteMediaContent = DAL2V5.GetAthleteMediaContentById(r.Id);
        //        }
        //        badges.Athletes = atheletes;
        //    }
        //    catch (Exception ex)
        //    {
        //        EmailLogModel emaillog = new EmailLogModel();
        //        emaillog.AddEmailLog("DAL2V5/GetCoachBlogsBadges", "", ex.Message, "Exception", 0);
        //        throw;
        //    }

        //    return badges;
        //}


        internal static int ApproveMediaFromAthleteByClC(int clubCoachId, int shareVideoId)
        {

            int mediaid = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();


                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[ApproveMediaFromAthleteByClC]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter shareVideoIdPar = cmd.Parameters.Add("@shareVideoId", SqlDbType.Int);
                    shareVideoIdPar.Direction = ParameterDirection.Input;
                    shareVideoIdPar.Value = shareVideoId;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@clubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = clubCoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        mediaid = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/ApproveMediaFromAthleteByClC", "", ex.Message, "Exception", 0);
            }

            return mediaid;
        }


        internal static int AddAthleteShareVideos(MediaContent mediaContent)
        {

            int mediaid = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteShareVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter videoURLPar = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    videoURLPar.Direction = ParameterDirection.Input;
                    videoURLPar.Value = mediaContent.VideoURL;

                    SqlParameter formatPar = cmd.Parameters.Add("@Format", SqlDbType.VarChar, 50);
                    formatPar.Direction = ParameterDirection.Input;
                    formatPar.Value = mediaContent.VideoFormat;

                    SqlParameter durationPar = cmd.Parameters.Add("@Duration", SqlDbType.Int);
                    durationPar.Direction = ParameterDirection.Input;
                    durationPar.Value = mediaContent.Duration;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = mediaContent.AthleteId;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = mediaContent.ClubCoachId;

                    SqlParameter approveFromClubCoachPar = cmd.Parameters.Add("@ApproveFromClubCoach", SqlDbType.Int);
                    approveFromClubCoachPar.Direction = ParameterDirection.Input;
                    approveFromClubCoachPar.Value = mediaContent.ApproveFromClubCoach;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        mediaid = (int)myData["Id"];
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/AddAthleteShareVideos", "", ex.Message, "Exception", 0);
            }


            return mediaid;
        }

        internal static int UpdateAthleteShareVideoThumbnail(MediaContent mediaContent)
        {

            int mediaid = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();


                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateAthleteShareVideoThumbnail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    namePar.Direction = ParameterDirection.Input;
                    namePar.Value = mediaContent.Name;

                    SqlParameter thumbnailURLPar = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                    thumbnailURLPar.Direction = ParameterDirection.Input;
                    thumbnailURLPar.Value = mediaContent.ThumbnailURL;

                    SqlParameter videoTypePar = cmd.Parameters.Add("@VideoType", SqlDbType.Int);
                    videoTypePar.Direction = ParameterDirection.Input;
                    videoTypePar.Value = mediaContent.VideoType;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = mediaContent.AthleteId;

                    SqlParameter captionPar = cmd.Parameters.Add("@Caption", SqlDbType.VarChar, 500);
                    captionPar.Direction = ParameterDirection.Input;
                    captionPar.Value = mediaContent.Caption;

                    SqlParameter latitudePar = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 100);
                    latitudePar.Direction = ParameterDirection.Input;
                    latitudePar.Value = mediaContent.Latitude;

                    SqlParameter longitudePar = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 100);
                    longitudePar.Direction = ParameterDirection.Input;
                    longitudePar.Value = mediaContent.Longitude;

                    SqlParameter mediacontentidPar = cmd.Parameters.Add("@sharevideoid", SqlDbType.Int);
                    mediacontentidPar.Direction = ParameterDirection.Input;
                    mediacontentidPar.Value = mediaContent.MediaContentId;

                    SqlParameter recordedDatePar = cmd.Parameters.Add("@RecordedDate", SqlDbType.DateTime);
                    recordedDatePar.Direction = ParameterDirection.Input;
                    recordedDatePar.Value = mediaContent.RecordedDate;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        mediaid =Convert.ToInt16(myData["Id"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/UpdateAthleteShareVideoThumbnail", "", ex.Message, "Exception", 0);
            }

            return mediaid;
        }


        internal static List<MediaContent> GetAthleteShareVideosById(int athleteId)
        {
            List<MediaContent> lstAthleteMedia = new List<MediaContent>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteShareVideosById]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MediaContent v = new MediaContent();
                        v.AthleteId = Convert.ToInt16(myData["AthleteId"]);
                        v.VideoURL = myData["VideoURL"].ToString();
                        v.ThumbnailURL = myData["ThumbnailURL"].ToString();
                        v.VideoFormat = myData["Format"].ToString();
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Duration"]);
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["VideoType"]);
                        v.Caption = myData["Caption"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Name = myData["Name"].ToString();
                        v.MediaContentId = (myData["Id"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Id"]);
                        v.ClubCoachId = (myData["ClubCoachId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["ClubCoachId"]);
                        v.ApproveFromClubCoach = (myData["ApproveFromClubCoach"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["ApproveFromClubCoach"]);
                        v.CoachEmailId = myData["CoachEmailId"].ToString();
                        v.SchoolCoachEmail = myData["SchoolCoachEmail"].ToString();
                        lstAthleteMedia.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetAthleteShareVideosById", "", ex.Message, "Exception", 0);
            }

            return lstAthleteMedia;
        }


        internal static List<MediaContent> GetAthleteShareVideosForClubCoach(int athleteId)
        {
            List<MediaContent> lstAthleteMedia = new List<MediaContent>();

            try 
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteShareVideosForClubCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MediaContent v = new MediaContent();
                        v.AthleteId = Convert.ToInt16(myData["AthleteId"]);
                        v.VideoURL = myData["VideoURL"].ToString();
                        v.ThumbnailURL = myData["ThumbnailURL"].ToString();
                        v.VideoFormat = myData["Format"].ToString();
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Duration"]);
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["VideoType"]);
                        v.Caption = myData["Caption"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Name = myData["Name"].ToString();
                        v.MediaContentId = (myData["Id"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Id"]);
                        v.ClubCoachId = (myData["ClubCoachId"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["ClubCoachId"]);
                        v.ApproveFromClubCoach = (myData["ApproveFromClubCoach"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["ApproveFromClubCoach"]);
                        v.CoachEmailId = myData["CoachEmailId"].ToString();
                        v.SchoolCoachEmail = myData["SchoolCoachEmail"].ToString();
                        lstAthleteMedia.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetAthleteShareVideosForClubCoach", "", ex.Message, "Exception", 0);
            }

            return lstAthleteMedia;
        }



        internal static int UpdateShareStatusFromAthleteTOClC(int shareVideoId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateShareStatusFromAthleteTOClC]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter shareVideoIdPar = cmd.Parameters.Add("@shareVideoId", SqlDbType.Int);
                    shareVideoIdPar.Direction = ParameterDirection.Input;
                    shareVideoIdPar.Value = shareVideoId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/UpdateShareStatusFromAthleteTOClC", "", ex.Message, "Exception", 0);
            }

            return id;
        }

        internal static int AddAthleteSharetoMediaContent(int shareVideoId, int athleteId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteSharetoMediaContent]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter shareVideoIdPar = cmd.Parameters.Add("@ShareVideoId", SqlDbType.Int);
                    shareVideoIdPar.Direction = ParameterDirection.Input;
                    shareVideoIdPar.Value = shareVideoId;

                    SqlParameter AthleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteIdPar.Direction = ParameterDirection.Input;
                    AthleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/AddAthleteSharetoMediaContent", "", ex.Message, "Exception", 0);
            }

            return id;
        }

        internal static VideoDTO AddAthleteSharetoAthleteVideos(int shareVideoId, int athleteId)
        {
            VideoDTO v = new VideoDTO();
          
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteSharetoAthleteVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter shareVideoIdPar = cmd.Parameters.Add("@ShareVideoId", SqlDbType.Int);
                    shareVideoIdPar.Direction = ParameterDirection.Input;
                    shareVideoIdPar.Value = shareVideoId;

                    SqlParameter AthleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteIdPar.Direction = ParameterDirection.Input;
                    AthleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["Id"];
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : (int)myData["Duration"];
                        v.VideoFormat = (myData["Format"] == DBNull.Value) ? string.Empty : (string)myData["Format"];
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? string.Empty : (string)myData["VideoURL"];
                        v.AtheleteId = athleteId; 
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/AddAthleteSharetoAthleteVideos", "", ex.Message, "Exception", 0);
            }

            return v;
        }



        internal static List<VideoDTO> GetShareTOAthleteVideosById(int athleteId)
        {
            List<VideoDTO> lstAthleteMedia = new List<VideoDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetShareAthleteVideosById]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.AtheleteId = Convert.ToInt16(myData["AthleteId"]);
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value)? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value)? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.UploadVideoUrl = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.UploadThumbnailUrl = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.VideoFormat = (myData["Format"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["Format"];
                        v.Id = (int)myData["Id"];
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : (int)myData["VideoType"];
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : (int)myData["Duration"];
                        v.Count = (int)myData["AtheleteVideoCount"];
                        v.ApproveFromClubCoach = (myData["ApproveFromClubCoach"] == DBNull.Value) ? 0 : (int)myData["ApproveFromClubCoach"];
                        lstAthleteMedia.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetShareAthleteVideosById", "", ex.Message, "Exception", 0);
            }

            return lstAthleteMedia;
        }




     
        internal static int DeleteAthleteShareVideo(int athleteShareVideoId)
        {

            int Id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAthleteShareVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteShareVideoIdPar = cmd.Parameters.Add("@athleteShareVideoId", SqlDbType.Int);
                    athleteShareVideoIdPar.Direction = ParameterDirection.Input;
                    athleteShareVideoIdPar.Value = athleteShareVideoId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Id = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/DeleteAthleteShareVideo", "", ex.Message, "Exception", 0);
                throw;
            }
            return Id;
        }

        internal static int DeleteAthleteMediaContent(int athleteMediaContentId)
        {

            int Id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAthleteMediaContent]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteMediaContentIdPar = cmd.Parameters.Add("@athleteMediaContentId", SqlDbType.Int);
                    athleteMediaContentIdPar.Direction = ParameterDirection.Input;
                    athleteMediaContentIdPar.Value = athleteMediaContentId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Id = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/DeleteAthleteMediaContent", "", ex.Message, "Exception", 0);
                throw;
            }
            return Id;
        }

        internal static List<MiscDTO> GetAtheleteFavouritesVideos(int athleteId)
        {
            List<MiscDTO> videos = new List<MiscDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteFavouritesVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MiscDTO v = new MiscDTO(); 
                        v.Title = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.URL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ImageURL = (myData["ThumbnailURL"] == DBNull.Value)? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim(); 
                        v.Id = (int)myData["Id"];
                        v.Type = (myData["Type"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Type"]).Trim();
                        v.Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? "" : (string)myData["Emailaddress"].ToString();
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetAtheleteFavouritesVideos", "", ex.Message, "Exception", 0);
            }

            return videos;
        }



        internal static List<MiscDTO> GetCoachesFavouritesVideos(string emailaddress)
        {
            List<MiscDTO> videos = new List<MiscDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachesFavouritesVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar,100);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MiscDTO v = new MiscDTO();
                        v.Title = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.URL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ImageURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.Id = (int)myData["Id"];
                        v.Type = "Athlete";
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        string Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? "" : (string)myData["Emailaddress"].ToString();
                        string Username = (myData["Username"] == DBNull.Value) ? "" : (string)myData["Username"].ToString();
                        v.Emailaddress = (v.LoginType == 1 || v.LoginType == 4) ? Emailaddress : Username;
                        v.Date = (myData["RecordedDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["RecordedDate"];
                        v.ShortDesc = (myData["Caption"] == DBNull.Value) ? "" : (string)myData["Caption"].ToString();
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetCoachesFavouritesVideos", "", ex.Message, "Exception", 0);
            }

            return videos;
        }

        internal static List<MiscDTO> GetAthletesinClubCoachClubVideos(int clubCoachId)
        {
            List<MiscDTO> videos = new List<MiscDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthletesinClubCoachClubVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@clubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = clubCoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MiscDTO v = new MiscDTO();
                        v.Title = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.URL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ImageURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.Id = (int)myData["Id"];
                        v.Type = "Athlete";
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        string Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? "" : (string)myData["Emailaddress"].ToString();
                        string Username = (myData["Username"] == DBNull.Value) ? "" : (string)myData["Username"].ToString();
                        v.Emailaddress = (v.LoginType == 1 || v.LoginType == 4) ? Emailaddress : Username;
                        v.Date = (myData["RecordedDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["RecordedDate"];
                        v.ShortDesc = (myData["Caption"] == DBNull.Value) ? "" : (string)myData["Caption"].ToString(); 
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetAthletesinClubCoachClubVideos", "", ex.Message, "Exception", 0);
            }

            return videos;
        }

        //internal static List<MiscDTO> GetClubCoachFavouritesVideos(int clubCoachId)
        //{
        //    List<MiscDTO> videos = new List<MiscDTO>();

        //    try
        //    {
        //        SqlConnection myConn = UsersDal.ConnectToDb();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[GetClubCoachFavouritesVideos]", myConn);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            SqlParameter clubCoachIdPar = cmd.Parameters.Add("@clubCoachId", SqlDbType.Int);
        //            clubCoachIdPar.Direction = ParameterDirection.Input;
        //            clubCoachIdPar.Value = clubCoachId;

        //            SqlDataReader myData = cmd.ExecuteReader();

        //            while (myData.Read())
        //            {
        //                MiscDTO v = new MiscDTO();
        //                v.Title = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
        //                v.URL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
        //                v.ImageURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
        //                v.Id = (int)myData["Id"];
        //                v.Type = "Videos";
        //                v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
        //                string Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? "" : (string)myData["Emailaddress"].ToString();
        //                string Username = (myData["Username"] == DBNull.Value) ? "" : (string)myData["Username"].ToString();
        //                v.Emailaddress = (v.LoginType == 1) ? Emailaddress : Username;
        //                v.Date = (myData["RecordedDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["RecordedDate"];
        //                v.ShortDesc = (myData["Caption"] == DBNull.Value) ? "" : (string)myData["Caption"].ToString();
        //                videos.Add(v);
        //            }
        //            myData.Close();
        //            myConn.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        EmailLogModel emaillog = new EmailLogModel();
        //        emaillog.AddEmailLog("Dal2v5/GetAthletesinClubCoachClubVideos", "", ex.Message, "Exception", 0);
        //    }

        //    return videos;
        //}


        internal static  CoachDTO  GetCoachDetails(int coachId)
        {
            CoachDTO v = new CoachDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachDetails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@coachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = coachId; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                       
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["CoachEmail"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? string.Empty : (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.FacebookId = (string)myData["CoachFacebook"].ToString(); 
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];   
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];  
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"]; 
                        v.AthleticConference = (myData["AthleticConference"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleticConference"]);
                        v.ScholarshipMen = (myData["ScholarshipMen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipMen"]);
                        v.ScholarshipWomen = (myData["ScholarshipWomen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipWomen"]);
                        v.TuitionInState = (myData["TuitionInState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionInState"]);
                        v.TuitionOutState = (myData["TuitionOutState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionOutState"]);
                         
                       
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/GetCoachDetails", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }

        internal static List<MiscDTO> GetFamilyFriendsAtheleteFavouritesVideos(int familyFriendId)
        {
            List<MiscDTO> videos = new List<MiscDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFamilyFriendsAtheleteFavouritesVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter familyFriendIdPar = cmd.Parameters.Add("@familyFriendId", SqlDbType.Int);
                    familyFriendIdPar.Direction = ParameterDirection.Input;
                    familyFriendIdPar.Value = familyFriendId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MiscDTO v = new MiscDTO();
                        v.Title = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.URL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ImageURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.Id = (int)myData["Id"];
                        v.Type = "Athlete";
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        string Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? "" : (string)myData["Emailaddress"].ToString();
                        string Username = (myData["Username"] == DBNull.Value) ? "" : (string)myData["Username"].ToString();
                        v.Emailaddress = (v.LoginType == 1 || v.LoginType == 4) ? Emailaddress : Username;
                        v.Date = (myData["RecordedDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["RecordedDate"];
                        v.ShortDesc = (myData["Caption"] == DBNull.Value) ? "" : (string)myData["Caption"].ToString();  
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetFamilyFriendsAtheleteFavouritesVideos", "", ex.Message, "Exception", 0);
            }

            return videos;
        }

        internal static List<MiscDTO> GetAthletesinClubCoachSchoolVideos(int clubCoachId)
        {
            List<MiscDTO> videos = new List<MiscDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthletesinClubCoachSchoolVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@clubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = clubCoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MiscDTO v = new MiscDTO();
                        v.Title = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.URL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ImageURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.Id = (int)myData["Id"];
                        v.Type = "Athlete";
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        string Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? "" : (string)myData["Emailaddress"].ToString();
                        string Username = (myData["Username"] == DBNull.Value) ? "" : (string)myData["Username"].ToString();
                        v.Emailaddress = (v.LoginType == 1 || v.LoginType == 4) ? Emailaddress : Username;
                        v.Date = (myData["RecordedDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["RecordedDate"];
                        v.ShortDesc = (myData["Caption"] == DBNull.Value) ? "" : (string)myData["Caption"].ToString();
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetAthletesinClubCoachClubVideos", "", ex.Message, "Exception", 0);
            }

            return videos;
        }

        internal static List<MiscDTO> GetClubCoachFavouritesVideos(int clubCoachId)
        {
            List<MiscDTO> videos = new List<MiscDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetClubCoachFavouritesVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = clubCoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MiscDTO v = new MiscDTO();
                        v.Title = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.URL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ImageURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.Id = (int)myData["Id"];
                        v.Type = "Coach";
                        v.Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? "" : (string)myData["Emailaddress"].ToString(); 
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetClubCoachFavouritesVideos", "", ex.Message, "Exception", 0);
            }

            return videos;
        }

        internal static List<MiscDTO> GetFamilyFriendsInstituteFavouritesVideos(int familyfriendId)
        {
            List<MiscDTO> videos = new List<MiscDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFamilyFriendsInstituteFavouritesVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter familyfriendIdPar = cmd.Parameters.Add("@familyfriendId", SqlDbType.Int);
                    familyfriendIdPar.Direction = ParameterDirection.Input;
                    familyfriendIdPar.Value = familyfriendId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MiscDTO v = new MiscDTO();
                        v.Title = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.URL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ImageURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.Id = (int)myData["Id"];
                        v.Type = "Coach";
                        v.Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? "" : (string)myData["Emailaddress"].ToString(); 
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetFamilyFriendsInstituteFavouritesVideos", "", ex.Message, "Exception", 0);
            }

            return videos;
        }


        internal static List<MiscDTO> GetCollegeAtheleteFavouritesVideos(int AthleteId)
        {
            List<MiscDTO> videos = new List<MiscDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCollegeAtheleteFavouritesVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = AthleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MiscDTO v = new MiscDTO();
                        v.Title = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.URL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ImageURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.Id = (int)myData["Id"];
                        v.Type = "Athlete";
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        string Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? "" : (string)myData["Emailaddress"].ToString();
                        string Username = (myData["Username"] == DBNull.Value) ? "" : (string)myData["Username"].ToString();
                        v.Emailaddress = (v.LoginType == 1 || v.LoginType == 4) ? Emailaddress : Username;
                        v.Date = (myData["RecordedDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["RecordedDate"];
                        v.ShortDesc = (myData["Caption"] == DBNull.Value) ? "" : (string)myData["Caption"].ToString();
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetFamilyFriendsAtheleteFavouritesVideos", "", ex.Message, "Exception", 0);
            }

            return videos;
        }


        internal static  UsageTrackingDTO GetUsageTracking(int userId,int Logintype)
        {
            UsageTrackingDTO usageTrackingDTO = new UsageTrackingDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUsersUsageTracking]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userIdPar = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userIdPar.Direction = ParameterDirection.Input;
                    userIdPar.Value = userId;

                    SqlParameter LogintypePar = cmd.Parameters.Add("@Logintype", SqlDbType.VarChar, 100);
                    LogintypePar.Direction = ParameterDirection.Input;
                    LogintypePar.Value = Logintype;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        usageTrackingDTO.TimeSinceLastUseOfApp = (myData["TimeSinceLastUseOfApp"] == DBNull.Value) ? "" : myData["TimeSinceLastUseOfApp"].ToString();
                        usageTrackingDTO.UsedShareButton = (int)(myData["UsedShareButton"]) > 0 ? true : false;
                        usageTrackingDTO.SendFirstMessage = (int)(myData["SendFirstMessage"]) > 0 ? true : false;
                        usageTrackingDTO.AllAcademicInfo = (int)(myData["AllAcademicInfo"]) > 0 ? true : false;
                        usageTrackingDTO.SportSpecificMetrics = (int)(myData["SportSpecificMetrics"]) > 0 ? true : false; 

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetUsageTracking", "", ex.Message, "Exception", 0);
            }

            return usageTrackingDTO;
        }


        internal static int UpdateNewClubCoachId(int newId , int oldId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateNewClubCoachId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter newIdPar = cmd.Parameters.Add("@newId", SqlDbType.Int);
                    newIdPar.Direction = ParameterDirection.Input;
                    newIdPar.Value = newId;

                    SqlParameter oldIdPar = cmd.Parameters.Add("@oldId", SqlDbType.Int);
                    oldIdPar.Direction = ParameterDirection.Input;
                    oldIdPar.Value = oldId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/AddAthleteSharetoMediaContent", "", ex.Message, "Exception", 0);
            }

            return id;
        }


        internal static int DeleteUserFromWrapper(string emailaddress, int sportId, int loginType, string userType)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteUserFromWrapper]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter sportIdPar = cmd.Parameters.Add("@sportid", SqlDbType.Int);
                    sportIdPar.Direction = ParameterDirection.Input;
                    sportIdPar.Value = sportId;

                    SqlParameter logintypePar = cmd.Parameters.Add("@logintype", SqlDbType.Int);
                    logintypePar.Direction = ParameterDirection.Input;
                    logintypePar.Value = loginType;

                    SqlParameter userTypePar = cmd.Parameters.Add("@usertype", SqlDbType.VarChar, 50);
                    userTypePar.Direction = ParameterDirection.Input;
                    userTypePar.Value = userType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/DeleteUserFromWrapper", "", e.Message, "Exception", 0);
            }

            return id;
        }

       

        internal static List<VideoDTO> GetVideosFromUniversityLatLongValues(int universityId)
        {
            List<VideoDTO> photos = new List<VideoDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVideosFromUniversityLatLongValues]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter universityIdPar = cmd.Parameters.Add("@universityId", SqlDbType.Int);
                    universityIdPar.Direction = ParameterDirection.Input;
                    universityIdPar.Value = universityId; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    { 
                        VideoDTO v = new VideoDTO(); 
                        v.Id = (myData["Id"] == DBNull.Value) ? 0 : (int)myData["Id"];
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString(); 
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["ThumbnailURL"]).Trim();

                        photos.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v5/GetVideosFromUniversityLatLongValues", "", ex.Message, "Exception", 0);
            }

            return photos;
        }


        internal static List<VideoDTO> UploadCoachPhotos_2V5(ThumbnailDTO thumbnailDto)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                string thumbnailUrl = UsersDal.UploadImageFromBase64(thumbnailDto.ThumbnailURL,
                    thumbnailDto.ThumbnailFormat);

                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoachPhotos_2V5]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namepar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    namepar.Direction = ParameterDirection.Input;
                    namepar.Value = "Photo_" + thumbnailDto.CoachId;

                    SqlParameter thumbnailurlpar = cmd.Parameters.Add("@Thumbnailurl", SqlDbType.VarChar, 1024);
                    thumbnailurlpar.Direction = ParameterDirection.Input;
                    thumbnailurlpar.Value = thumbnailUrl;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = thumbnailDto.CoachId;

                    SqlParameter thumbnailFormat = cmd.Parameters.Add("@ThumbnailFormat", SqlDbType.VarChar, 50);
                    thumbnailFormat.Direction = ParameterDirection.Input;
                    thumbnailFormat.Value = thumbnailDto.ThumbnailFormat;

                    SqlParameter latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 100);
                    latitude.Direction = ParameterDirection.Input;
                    latitude.Value = thumbnailDto.Latitude; 

                    SqlParameter longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 100);
                    longitude.Direction = ParameterDirection.Input;
                    longitude.Value = thumbnailDto.Longitude;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : myData["VideoURL"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value)
                            ? ""
                            : myData["ThumbnailURL"].ToString();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? "" : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        videos.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/UploadCoachPhotos", "", ex.Message, "Exception", 0);
            }


            return videos;
        }

        internal static List<AdDTO> GetDisplayCards(int endUserId)
        {

            List<AdDTO> AdDTO = new List<AdDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetDisplayAdsForSuperAdmins]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter endUserIdPar = cmd.Parameters.Add("@EndAdUserId", SqlDbType.VarChar, 100);
                    endUserIdPar.Direction = ParameterDirection.Input;
                    endUserIdPar.Value = endUserId;
                     

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AdDTO v = new AdDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? "" : (string)myData["Description"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? "" : (string)myData["ProfilePicURL"].ToString();
                        v.VideoURL = (myData["WebsiteURL"] == DBNull.Value) ? "" : (string)myData["WebsiteURL"];
                        v.Id = (int)myData["Id"];
                        v.AdType = 1;
                        AdDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2v5/GetDisplayCards", "", ex.Message, "Exception", 0);
                throw;
            }


            return AdDTO;
        }



        internal static int AddAtheleteShareVideoThumbnails(AthleteThumbnailDto athleteThumbnailDto)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAtheleteShareVideoThumbnails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter VideoId = cmd.Parameters.Add("@AthleteShareVideoId", SqlDbType.Int);
                    VideoId.Direction = ParameterDirection.Input;
                    VideoId.Value = athleteThumbnailDto.AthleteId; //Changed on 23 MAy 17

                    SqlParameter ThumbnailURL = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1000);
                    ThumbnailURL.Direction = ParameterDirection.Input;
                    ThumbnailURL.Value = athleteThumbnailDto.ThumbnailURL;

                    SqlParameter IdPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    IdPar.Direction = ParameterDirection.Output;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/AddAtheleteShareVideoThumbnails", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;

        }


        internal static int DeleteAtheleteShareVideoThumbnails(int AthleteId) //Changed on 23 mAy 17 sendAthleteid instead of VideoId
        {

            int i = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAtheleteShareVideoThumbnails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AthleteShareVideoIdPar = cmd.Parameters.Add("@AthleteShareVideoId", SqlDbType.Int);
                    AthleteShareVideoIdPar.Direction = ParameterDirection.Input;
                    AthleteShareVideoIdPar.Value = AthleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V5/DeleteAtheleteShareVideoThumbnails", "", ex.Message, "Exception", 0);
                throw;
            }

            return i;
        }


        internal static List<string> GetShareAtheleteVideosThumbnails(int VideoId)
        {

            List<string> ThumbnailURLs = new List<string>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetShareAtheleteVideosThumbnails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter VideoIdPar = cmd.Parameters.Add("@AthleteShareVideoId", SqlDbType.Int);
                    VideoIdPar.Direction = ParameterDirection.Input;
                    VideoIdPar.Value = VideoId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        string ThumbnailURL = string.Empty;
                        ThumbnailURL = (string)myData["ThumbnailURL"].ToString();
                        ThumbnailURLs.Add(ThumbnailURL);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetShareAtheleteVideosThumbnails", "", ex.Message, "Exception", 0);
                throw;
            }

            return ThumbnailURLs;
        }


        internal static int UpdateCoachEmailaddress(CoachProfileDTO coachProfile)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachEmailaddress]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter EmailaddressPar = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    EmailaddressPar.Direction = ParameterDirection.Input;
                    EmailaddressPar.Value = coachProfile.Emailaddress;

                    SqlParameter NamePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    NamePar.Direction = ParameterDirection.Input;
                    NamePar.Value = coachProfile.CollegeName;

                    SqlParameter PhoneNumberPar = cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar, 50);
                    PhoneNumberPar.Direction = ParameterDirection.Input;
                    PhoneNumberPar.Value = coachProfile.Phonenumber;

                    SqlParameter CoachIdpar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    CoachIdpar.Direction = ParameterDirection.Input;
                    CoachIdpar.Value = coachProfile.CoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V5/UpdateCoachEmailaddress", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }
    }
}