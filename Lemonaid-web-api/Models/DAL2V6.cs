﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Lemonaid_web_api.Models
{
    public class DAL2V6
    {
        internal static ClubCoachDTO AddClubCoach2_v6(ClubCoachDTO clubCoachDto)
        {
            ClubCoachDTO v = new ClubCoachDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddClubCoach2_v6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    name.Direction = ParameterDirection.Input;
                    name.Value = clubCoachDto.Name;

                    SqlParameter gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    gender.Direction = ParameterDirection.Input;
                    gender.Value = clubCoachDto.Gender;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = clubCoachDto.Emailaddress;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = (clubCoachDto.Description == null) ? string.Empty : clubCoachDto.Description;

                    SqlParameter profilePicUrl = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    profilePicUrl.Direction = ParameterDirection.Input;
                    profilePicUrl.Value = clubCoachDto.ProfilePicURL;

                    SqlParameter dob = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    dob.Direction = ParameterDirection.Input;
                    dob.Value = clubCoachDto.DOB;

                    SqlParameter facebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    facebookId.Direction = ParameterDirection.Input;
                    facebookId.Value = clubCoachDto.FacebookId;

                    SqlParameter coverPicUrl = cmd.Parameters.Add("@CoverPicURL", SqlDbType.VarChar, 1000);
                    coverPicUrl.Direction = ParameterDirection.Input;
                    coverPicUrl.Value = clubCoachDto.CoverPicURL;

                    SqlParameter latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    latitude.Direction = ParameterDirection.Input;
                    latitude.Value = clubCoachDto.Latitude;

                    SqlParameter longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    longitude.Direction = ParameterDirection.Input;
                    longitude.Value = clubCoachDto.Longitude;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = string.IsNullOrWhiteSpace(clubCoachDto.ClubName)
                        ? string.Empty
                        : clubCoachDto.ClubName;

                    SqlParameter active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    active.Direction = ParameterDirection.Input;
                    active.Value = clubCoachDto.Active;

                    SqlParameter phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    phonenumber.Direction = ParameterDirection.Input;
                    phonenumber.Value = string.IsNullOrWhiteSpace(clubCoachDto.Phonenumber)
                        ? string.Empty
                        : clubCoachDto.Phonenumber;

                    SqlParameter deviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    deviceType.Direction = ParameterDirection.Input;
                    deviceType.Value = (clubCoachDto.DeviceType == null) ? string.Empty : clubCoachDto.DeviceType;

                    SqlParameter deviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 50);
                    deviceId.Direction = ParameterDirection.Input;
                    deviceId.Value = (clubCoachDto.DeviceId == null) ? string.Empty : clubCoachDto.DeviceId;

                    SqlParameter teamType = cmd.Parameters.Add("@TeamType", SqlDbType.Int);
                    teamType.Direction = ParameterDirection.Input;
                    teamType.Value = (clubCoachDto.TeamType == null) ? 0 : Convert.ToInt32(clubCoachDto.TeamType);

                    SqlParameter location = cmd.Parameters.Add("@Location", SqlDbType.VarChar, 200);
                    location.Direction = ParameterDirection.Input;
                    location.Value = (clubCoachDto.Location == null) ? string.Empty : clubCoachDto.Location;

                    SqlParameter schoolName = cmd.Parameters.Add("@SchoolName", SqlDbType.VarChar, 50);
                    schoolName.Direction = ParameterDirection.Input;
                    schoolName.Value = (clubCoachDto.SchoolName == null) ? string.Empty : clubCoachDto.SchoolName;

                    SqlParameter zipCode = cmd.Parameters.Add("@ZipCode", SqlDbType.VarChar, 50);
                    zipCode.Direction = ParameterDirection.Input;
                    zipCode.Value = (clubCoachDto.ZipCode == null) ? string.Empty : clubCoachDto.ZipCode;

                    SqlParameter stateId = cmd.Parameters.Add("@StateId", SqlDbType.Int);
                    stateId.Direction = ParameterDirection.Input;
                    stateId.Value = (clubCoachDto.StateId == null) ? 0 : Convert.ToInt32(clubCoachDto.StateId);

                    SqlParameter schoolType = cmd.Parameters.Add("@SchoolType", SqlDbType.Int);
                    schoolType.Direction = ParameterDirection.Input;
                    schoolType.Value = (clubCoachDto.SchoolType == null) ? 0 : Convert.ToInt32(clubCoachDto.SchoolType);

                    SqlParameter clubPhoneNumber = cmd.Parameters.Add("@ClubPhoneNumber", SqlDbType.VarChar, 50);
                    clubPhoneNumber.Direction = ParameterDirection.Input;
                    clubPhoneNumber.Value = string.IsNullOrWhiteSpace(clubCoachDto.ClubPhoneNumber)
                        ? string.Empty
                        : clubCoachDto.ClubPhoneNumber;

                    SqlParameter schoolId = cmd.Parameters.Add("@SchoolId", SqlDbType.Int);
                    schoolId.Direction = ParameterDirection.Input;
                    schoolId.Value = (clubCoachDto.SchoolId == null) ? 0 : Convert.ToInt32(clubCoachDto.SchoolId);

                    SqlParameter LoginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    LoginType.Direction = ParameterDirection.Input;
                    LoginType.Value = (clubCoachDto.LoginType == null) ? 0 : Convert.ToInt32(clubCoachDto.LoginType);

                    SqlParameter userName = cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 100);
                    userName.Direction = ParameterDirection.Input;
                    userName.Value = (clubCoachDto.Username == null) ? string.Empty : clubCoachDto.Username;

                    SqlParameter Almamater = cmd.Parameters.Add("@Almamater", SqlDbType.VarChar, 100);
                    Almamater.Direction = ParameterDirection.Input;
                    Almamater.Value = (clubCoachDto.Almamater == null) ? string.Empty : clubCoachDto.Almamater;

                    SqlParameter trainedSport = cmd.Parameters.Add("@TrainedSport", SqlDbType.VarChar, 50);
                    trainedSport.Direction = ParameterDirection.Input;
                    trainedSport.Value = (clubCoachDto.TrainedSport == null) ? string.Empty : clubCoachDto.TrainedSport;

                    SqlParameter IdPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    IdPar.Direction = ParameterDirection.Input;
                    IdPar.Value = (clubCoachDto.Id == null) ? 0 : Convert.ToInt32(clubCoachDto.Id);

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        //v.ExistedId = (myData["AlreadyExistedId1"] == DBNull.Value) ? 0 : (int)myData["AlreadyExistedId1"];
                        //v.ExistedEmailAddress = (myData["AlreadyExistedEmailaddress1"] == DBNull.Value) ? string.Empty : (string)myData["AlreadyExistedEmailaddress1"];
                        //v.ExistedLoginType = (myData["AlreadyExistedLoginType1"] == DBNull.Value) ? 0 : (int)myData["AlreadyExistedLoginType1"];
                        //v.ExistedUsername = (myData["AlreadyExistedUserName1"] == DBNull.Value) ? string.Empty : (string)myData["AlreadyExistedUserName1"];
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.ClubAddress = string.Empty;
                        v.ClubName = myData["ClubName"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Active = (myData["Active"] != DBNull.Value) && Convert.ToBoolean(myData["Active"]);
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        v.Location = myData["Location"].ToString();
                        v.SchoolName = myData["SchoolName"].ToString();
                        v.ZipCode = myData["ZipCode"].ToString();
                        v.StateId = myData["StateId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = myData["StateName"].ToString();
                        v.SchoolType = myData["SchoolType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolType"]);
                        v.ClubPhoneNumber = (myData["ClubPhoneNumber"] == DBNull.Value) ? string.Empty : myData["ClubPhoneNumber"].ToString();
                        v.SchoolId = myData["SchoolId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolId"]);
                        v.ClubId = (myData["ClubId"] == DBNull.Value) ? 0 : (int)myData["ClubId"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["LoginType"]);
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : (string)myData["Username"];
                        v.DeviceId = (myData["DeviceName"] == DBNull.Value) ? string.Empty : (string)myData["DeviceName"];
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"];
                        v.Almamater = (myData["Almamater"] == DBNull.Value) ? string.Empty : (string)myData["Almamater"];
                        v.TrainedSport = (myData["TrainedSport"] == DBNull.Value) ? string.Empty : (string)myData["TrainedSport"];

                    }
                    v.DeviceId = clubCoachDto.DeviceId;
                    v.DeviceType = clubCoachDto.DeviceType;


                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/AddClubCoach", "", ex.Message, "Exception", 0);
            }

            return v;
        }


        internal static List<AdDTO> GetChatAdCards(string emailaddress, int loginType, int sportId, int AdType, string userType)
        {
            List<AdDTO> AdDTO = new List<AdDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetChatAdCards_2V6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter loginTypePar = cmd.Parameters.Add("@loginType", SqlDbType.Int);
                    loginTypePar.Direction = ParameterDirection.Input;
                    loginTypePar.Value = loginType;

                    SqlParameter sportIdPar = cmd.Parameters.Add("@sportId", SqlDbType.Int);
                    sportIdPar.Direction = ParameterDirection.Input;
                    sportIdPar.Value = sportId;

                    SqlParameter adTypePar = cmd.Parameters.Add("@adType", SqlDbType.Int);
                    adTypePar.Direction = ParameterDirection.Input;
                    adTypePar.Value = AdType;

                    SqlParameter userTypePar = cmd.Parameters.Add("@userType", SqlDbType.VarChar, 20);
                    userTypePar.Direction = ParameterDirection.Input;
                    userTypePar.Value = userType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AdDTO v = new AdDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? "" : (string)myData["Description"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? "" : (string)myData["ProfilePicURL"].ToString();
                        v.VideoURL = (myData["WebsiteURL"] == DBNull.Value) ? "" : (string)myData["WebsiteURL"];
                        v.Id = (int)myData["Id"];
                        v.AdType = AdType;
                        AdDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2v6/GetChatAdCards", "", ex.Message, "Exception", 0);
                throw;
            }


            return AdDTO;
        }

        //internal static List<AdDTO> GetDisplayCardsBySport(int sportId)
        //{

        //    List<AdDTO> AdDTO = new List<AdDTO>();

        //    try
        //    {
        //        SqlConnection myConn = UsersDal.ConnectToWrapperDb();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[GetDisplayAdsForSuperAdminsBySport]", myConn);
        //            cmd.CommandType = CommandType.StoredProcedure;  

        //            SqlParameter sportIdPar = cmd.Parameters.Add("@SportId", SqlDbType.VarChar, 100);
        //            sportIdPar.Direction = ParameterDirection.Input;
        //            sportIdPar.Value = sportId;


        //            SqlDataReader myData = cmd.ExecuteReader();

        //            while (myData.Read())
        //            {
        //                AdDTO v = new AdDTO();
        //                v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
        //                v.Description = (myData["Description"] == DBNull.Value) ? "" : (string)myData["Description"].ToString();
        //                v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? "" : (string)myData["ProfilePicURL"].ToString();
        //                v.VideoURL = (myData["WebsiteURL"] == DBNull.Value) ? "" : (string)myData["WebsiteURL"];
        //                v.Id = (int)myData["Id"];
        //                v.AdType = 1;
        //                AdDTO.Add(v);
        //            }
        //            myData.Close();
        //            myConn.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        EmailLogModel emaillog = new EmailLogModel();
        //        emaillog.AddEmailLog("DAL2v6/GetDisplayCardsBySport", "", ex.Message, "Exception", 0);
        //        throw;
        //    }


        //    return AdDTO;
        //}
         
        internal static bool PushNotification(string Name, string Message)
        {
            bool IsSend = false;
            try
            {
                FirebaseDTO firebase = GetFirebaseCredentials();
                string applicationID = firebase.FirebaseAppId;
                string senderId = firebase.FirebaseSenderId;

                string deviceId = Name;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = Message,
                        title = "Lemonaid Notification",
                        sound = "Enabled"

                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                                IsSend = true;
                            }
                        }
                    }
                }
               
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
            return IsSend;
        }


        internal static int AddCoachVideo360Content(Video360TypeDTO Video360)
        {

            int VideoId = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoachVideos360Type]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter NamePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    NamePar.Direction = ParameterDirection.Input;
                    NamePar.Value = Video360.Name;

                    SqlParameter videoURLPar = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    videoURLPar.Direction = ParameterDirection.Input;
                    videoURLPar.Value = Video360.VideoURL;

                    SqlParameter thumbnailURLPar = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                    thumbnailURLPar.Direction = ParameterDirection.Input;
                    thumbnailURLPar.Value = Video360.ThumbNailURL;

                    SqlParameter statusPar = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    statusPar.Direction = ParameterDirection.Input;
                    statusPar.Value = Video360.Status;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = Video360.CoachId;

                    SqlParameter videoformatPar = cmd.Parameters.Add("@VideoFormat", SqlDbType.VarChar, 50);
                    videoformatPar.Direction = ParameterDirection.Input;
                    videoformatPar.Value = Video360.VideoFormat;

                    SqlParameter durationPar = cmd.Parameters.Add("@duration", SqlDbType.Int);
                    durationPar.Direction = ParameterDirection.Input;
                    durationPar.Value = Video360.Duration;

                    SqlParameter latitudePar = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar,100);
                    latitudePar.Direction = ParameterDirection.Input;
                    latitudePar.Value = Video360.Latitude;

                    SqlParameter longitudePar = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 100);
                    longitudePar.Direction = ParameterDirection.Input;
                    longitudePar.Value = Video360.Longitude;

                    SqlParameter Descpar = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 500);
                    Descpar.Direction = ParameterDirection.Input;
                    Descpar.Value = Video360.Description;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoId = (int)myData["Video360ID"];
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V6/AddCoachVideo360Content", "", ex.Message, "Exception", 0);
            }


            return VideoId;
        }

        internal static int DeleteCoachVideo360Type(int Video360ID)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCoachVideo360Type]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter VideoID = cmd.Parameters.Add("@VideoID", SqlDbType.Int);
                    VideoID.Direction = ParameterDirection.Input;
                    VideoID.Value = Video360ID;

                   

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2V6/DeleteCoachVideo360Type", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<AthleteDTO> GetAthleteChatAdFavourites(AthleteFavouriteDTO athleteFavouriteDTO)
        {
            List<AthleteDTO> ChatAds = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteChatAdFavourites_2V6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar, 100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteFavouriteDTO.AthleteEmail;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = athleteFavouriteDTO.LoginType;

                    SqlParameter sportId = cmd.Parameters.Add("@SportId", SqlDbType.Int);
                    sportId.Direction = ParameterDirection.Input;
                    sportId.Value = athleteFavouriteDTO.SportId;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = athleteFavouriteDTO.Offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = athleteFavouriteDTO.Max;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = athleteFavouriteDTO.UserType;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.CoverPicURL = myData["WebsiteURL"].ToString();
                        v.Description = myData["Description"].ToString();
                        v.FavDate = (DateTime)myData["CreatedDate"];
                        v.AdType = (int)myData["AdType"];
                        v.Liked = true;
                        ChatAds.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/GetAthleteChatAdFavourites", "", ex.Message, "Exception", 0);
                throw;
            }

            return ChatAds;
        }

        internal static int GetUserId(string athleteEmail, int loginType,string userType)
        {
            int userId = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserId_2V6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar, 100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteEmail;

                    SqlParameter loginTypepar = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginTypepar.Direction = ParameterDirection.Input;
                    loginTypepar.Value = loginType;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = userType;

                    userId = Convert.ToInt32(cmd.ExecuteScalar());


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetUserId", "", ex.Message, "Exception", 0);
                throw;
            }

            return userId;
        }

        internal static List<CoachDTO> GetFamilyFriendsInstituteFavourites_2V6(int familyFriendId)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFamilyFriendsInstituteFavourites_2V6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter familyFriendIdPar = cmd.Parameters.Add("@familyFriendId", SqlDbType.Int);
                    familyFriendIdPar.Direction = ParameterDirection.Input;
                    familyFriendIdPar.Value = familyFriendId; 


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["Emailaddress"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.AthleticConference = (myData["AthleticConference"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleticConference"]);
                        v.EnrollmentFee = (myData["EnrollmentFee"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentFee"]);
                        v.Type = (myData["Type"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Type"]);
                        v.TuitionFee = (myData["TuitionFee"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionFee"]);
                        v.Scholarship = (myData["Scholarship"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Scholarship"]);
                        v.EnrollmentNo = (myData["EnrollmentNo"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentNo"]);
                        v.ScholarshipMen = (myData["ScholarshipMen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipMen"]);
                        v.ScholarshipWomen = (myData["ScholarshipWomen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipWomen"]);
                        v.TuitionInState = (myData["TuitionInState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionInState"]);
                        v.TuitionOutState = (myData["TuitionOutState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionOutState"]);
                        v.VirtualVideoURL = (myData["VirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualVideoURL"]);
                        v.VirtualThumbnailURL = (myData["VirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualThumbnailURL"]);
                        v.MatchesVirtualVideoURL = (myData["MatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualVideoURL"]);
                        v.MatchesVirtualThumbnailURL = (myData["MatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualThumbnailURL"]);
                        Coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/GetFamilyFriendsInstituteFavourites", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }


        internal static List<CoachDTO> GetClubCoachFavourites_2V6(int clubCoachId)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetClubCoachFavourites_2V6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = clubCoachId; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["Emailaddress"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.AthleticConference = (myData["AthleticConference"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleticConference"]);
                        v.ScholarshipMen = (myData["ScholarshipMen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipMen"]);
                        v.ScholarshipWomen = (myData["ScholarshipWomen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipWomen"]);
                        v.TuitionInState = (myData["TuitionInState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionInState"]);
                        v.TuitionOutState = (myData["TuitionOutState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionOutState"]);
                        v.VirtualVideoURL = (myData["VirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualVideoURL"]);
                        v.VirtualThumbnailURL = (myData["VirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualThumbnailURL"]);
                        v.MatchesVirtualVideoURL = (myData["MatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualVideoURL"]);
                        v.MatchesVirtualThumbnailURL = (myData["MatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualThumbnailURL"]);
                        Coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetClubCoachFavourites", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }


        internal static int AddDeviceForUser(DeviceDTO deviceDto)
        {
            int  id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddDeviceForUser]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter deviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 500);
                    deviceId.Direction = ParameterDirection.Input;
                    deviceId.Value = deviceDto.DeviceId;

                    SqlParameter deviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 100);
                    deviceType.Direction = ParameterDirection.Input;
                    deviceType.Value = deviceDto.DeviceType;

                    SqlParameter userType = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    userType.Direction = ParameterDirection.Input;
                    userType.Value = deviceDto.UserType;

                    SqlParameter userId = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userId.Direction = ParameterDirection.Input;
                    userId.Value = deviceDto.UserId;

                    id = Convert.ToInt32(cmd.ExecuteScalar());


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/AddDeviceForUser", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<Video360TypeDTO> GetCoachVideos360Type()
        {
            List<Video360TypeDTO> lstVideo360Type = new List<Video360TypeDTO>();

            try
            {

                var connectionString = "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrapper;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                var myConn = new SqlConnection(connectionString);
                myConn.Open();   

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachVideos360Type]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Video360TypeDTO v = new Video360TypeDTO();
                        v.Name = myData["Name"].ToString();
                        v.VideoURL = myData["VideoURL"].ToString();
                        v.ThumbNailURL = myData["ThumbnailURL"].ToString();
                        v.CoachId = Convert.ToInt16(myData["CoachId"]);
                        v.VideoFormat = myData["VideoFormat"].ToString();
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Duration"]);
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["VideoType"]);                      
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Description = myData["Description"].ToString();
                        lstVideo360Type.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/GetCoachVideos360Type", "", ex.Message, "Exception", 0);
            }

            return lstVideo360Type;
        }



        internal static List<CityDTO> GetCities(int stateId)
        {

            List<CityDTO> cityDTO = new List<CityDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCitiesByStateId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter stateIdpar = cmd.Parameters.Add("@StateId", SqlDbType.Int);
                    stateIdpar.Direction = ParameterDirection.Input;
                    stateIdpar.Value = stateId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                       CityDTO v = new CityDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["CityName"].ToString();
                        cityDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/GetCities", "", ex.Message, "Exception", 0);
                throw;
            }

            return cityDTO;
        }


        internal static int GetNotificationsCount(int UserId, int senderType)
        {

            int Count = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetNotificationsCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userIdPar = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userIdPar.Direction = ParameterDirection.Input;
                    userIdPar.Value = UserId;

                    SqlParameter senderTypePar = cmd.Parameters.Add("@senderType", SqlDbType.Int);
                    senderTypePar.Direction = ParameterDirection.Input;
                    senderTypePar.Value = senderType;

                    Count = (int)cmd.ExecuteScalar(); 
                    
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/GetNotificationsCount", "", ex.Message, "Exception", 0);
                throw;
            }

            return Count;
        }

        internal static int GetMutualLikesCount(int UserId, string UserType)
        {

            int Count = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetMutualLikesCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userIdPar = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userIdPar.Direction = ParameterDirection.Input;
                    userIdPar.Value = UserId;

                    SqlParameter UserTypePar = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    UserTypePar.Direction = ParameterDirection.Input;
                    UserTypePar.Value = UserType;

                    Count = (int)cmd.ExecuteScalar();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/GetMutualLikesCount", "", ex.Message, "Exception", 0);
                throw;
            }

            return Count;
        }


        internal static int RefreshChatAds(RefreshDTO refreshDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[RefreshChatAds]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = refreshDto.Emailaddress;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = refreshDto.LoginType; 

                    SqlParameter sportId = cmd.Parameters.Add("@SportId", SqlDbType.Int);
                    sportId.Direction = ParameterDirection.Input;
                    sportId.Value = refreshDto.SportId;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = refreshDto.UserType;  

                    cmd.ExecuteNonQuery();  
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2v6/RefreshChatAds ", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static CoachUserDTO AddCoachUser_2V6(CoachUserDTO coachUserDTO)
        {


            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoach_2v6]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FacebookId = cmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 100);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = coachUserDTO.Emailaddress;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = coachUserDTO.InstituteId;

                    SqlParameter Password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 100);
                    Password.Direction = ParameterDirection.Input;
                    Password.Value = UsersDalV4.Encrypt(coachUserDTO.Password);
                    // Password.Value = coachUserDTO.Password;

                    SqlParameter active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    active.Direction = ParameterDirection.Input;
                    active.Value = coachUserDTO.Active;

                    SqlParameter teamType = cmd.Parameters.Add("@TeamType", SqlDbType.Int);
                    teamType.Direction = ParameterDirection.Input;
                    teamType.Value = coachUserDTO.TeamType;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = (coachUserDTO.DeviceType == null) ? string.Empty : coachUserDTO.DeviceType;

                    SqlParameter DeviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 500);
                    DeviceId.Direction = ParameterDirection.Input;
                    DeviceId.Value = (coachUserDTO.DeviceId == null) ? string.Empty : coachUserDTO.DeviceId;

                    SqlParameter FirstName = cmd.Parameters.Add("@FirstName", SqlDbType.VarChar, 100);
                    FirstName.Direction = ParameterDirection.Input;
                    FirstName.Value = (coachUserDTO.FirstName == null) ? string.Empty : coachUserDTO.FirstName;

                    SqlParameter LastName = cmd.Parameters.Add("@LastName", SqlDbType.VarChar, 500);
                    LastName.Direction = ParameterDirection.Input;
                    LastName.Value = (coachUserDTO.LastName == null) ? string.Empty : coachUserDTO.LastName;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        coachUserDTO.CoachId = (int)myData["Id"];
                        coachUserDTO.ActivationCode = (myData["activation_code"] == DBNull.Value) ? "" : (string)myData["activation_code"];
                        coachUserDTO.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        coachUserDTO.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        coachUserDTO.Name = (myData["UniversityName"] == DBNull.Value) ? "" : (string)myData["UniversityName"];
                        int Head = (int)myData["HeadCoach"];
                        coachUserDTO.HeadCoach = (Head == 1) ? true : false;
                        coachUserDTO.Password = (myData["Password"] == DBNull.Value) ? "" : (string)myData["Password"];
                        coachUserDTO.FirstName = (myData["FirstName"] == DBNull.Value) ? "" : (string)myData["FirstName"];
                        coachUserDTO.LastName = (myData["LastName"] == DBNull.Value) ? "" : (string)myData["LastName"];
                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/AddCoachUser_2V6", "", ex.Message, "Exception", 0);
                throw;
            }

            return coachUserDTO;
        }

        internal static FirebaseDTO GetFirebaseCredentials()
        {
            FirebaseDTO firebaseDTO = new FirebaseDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFirebaseCredentials]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                   
                        firebaseDTO.FirebaseAppId = (myData["FirebaseAPPID"] == DBNull.Value) ? "" : (string)myData["FirebaseAPPID"];
                        firebaseDTO.FirebaseSenderId = (myData["FirebaseSenderId"] == DBNull.Value) ? "" : (string)myData["FirebaseSenderId"];
               
                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2v6/GetFirebaseCredentials ", "", ex.Message, "Exception", 0);
                throw;
            }

            return firebaseDTO;
        }


        internal static List<Video360TypeDTO> GetCoachVideos360Type(int CoachId)
        {
            List<Video360TypeDTO> lstVideo360Type = new List<Video360TypeDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachVideos360TypeByCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachId.Direction = ParameterDirection.Input;
                    coachId.Value = CoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Video360TypeDTO v = new Video360TypeDTO();
                        v.Id = Convert.ToInt16(myData["Id"]);
                        v.Name = myData["Name"].ToString();
                        v.VideoURL = myData["VideoURL"].ToString();
                        v.ThumbNailURL = myData["ThumbnailURL"].ToString();
                        v.CoachId = Convert.ToInt16(myData["CoachId"]);
                        v.VideoFormat = myData["VideoFormat"].ToString();
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Duration"]);
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["VideoType"]);
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Description = myData["Description"].ToString();
                        lstVideo360Type.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/GetCoachVideos360Type", "", ex.Message, "Exception", 0);
            }

            return lstVideo360Type;
        }


        internal static int AddCoachPayment(PaymentDTO payment)
        {

            int id = 0;
            try
            {

                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoachPayment]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = payment.UserID;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.Int);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = payment.UserType;

                    SqlParameter PaymentToken = cmd.Parameters.Add("@PaymentToken", SqlDbType.VarChar, 8000);
                    PaymentToken.Direction = ParameterDirection.Input;
                    PaymentToken.Value = payment.PaymentToken;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = payment.DeviceType;

                    SqlParameter PaymentType = cmd.Parameters.Add("@PaymentType", SqlDbType.VarChar, 50);
                    PaymentType.Direction = ParameterDirection.Input;
                    PaymentType.Value = payment.PaymentType;

                    SqlParameter subscriptionType = cmd.Parameters.Add("@SubscriptionType", System.Data.SqlDbType.VarChar, 20);
                    subscriptionType.Direction = System.Data.ParameterDirection.Input;
                    subscriptionType.Value = (payment.SubscriptionType == null) ? string.Empty : payment.SubscriptionType;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = payment.Emailaddress;

                    SqlParameter SKUIdPar = cmd.Parameters.Add("@SKUId", SqlDbType.VarChar, 100);
                    SKUIdPar.Direction = ParameterDirection.Input;
                    SKUIdPar.Value = (payment.SKUId == null) ? string.Empty : payment.SKUId;

                    SqlParameter TransactionStatusPar = cmd.Parameters.Add("@TransactionStatus", SqlDbType.VarChar, 100);
                    TransactionStatusPar.Direction = ParameterDirection.Input;
                    TransactionStatusPar.Value = (payment.TransactionStatus == null) ? string.Empty : payment.TransactionStatus;

                    SqlParameter PlanIdPar = cmd.Parameters.Add("@PlanId", SqlDbType.Int);
                    PlanIdPar.Direction = ParameterDirection.Input;
                    PlanIdPar.Value = (payment.PlanId == null) ? 0 : payment.PlanId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/AddCoachPayment", "", ex.Message, "Exception", 0);
                throw;
            }
            return id;
        }


        internal static int UpdateCoachPayment(PaymentDTO payment)
        {

            int id = 0;
            try
            {

                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachPayment]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter TransactionId = cmd.Parameters.Add("@TransactionId", SqlDbType.Int);
                    TransactionId.Direction = ParameterDirection.Input;
                    TransactionId.Value = payment.ID;
                      
                    SqlParameter PaymentToken = cmd.Parameters.Add("@PaymentToken", SqlDbType.VarChar, 8000);
                    PaymentToken.Direction = ParameterDirection.Input;
                    PaymentToken.Value = payment.PaymentToken; 

                    SqlParameter TransactionStatusPar = cmd.Parameters.Add("@TransactionStatus", SqlDbType.VarChar, 100);
                    TransactionStatusPar.Direction = ParameterDirection.Input;
                    TransactionStatusPar.Value = (payment.TransactionStatus == null) ? string.Empty : payment.TransactionStatus;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v6/UpdateCoachPayment", "", ex.Message, "Exception", 0);
                throw;
            }
            return id;
        }

        internal static PaymentDTO GetCoachPayment(int UserID)
        {
            PaymentDTO payment = new PaymentDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachPayment]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    userID.Direction = ParameterDirection.Input;
                    userID.Value = UserID;
                     

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        payment.UserID = (int)myData["UserID"];
                        payment.UserType = (int)myData["UserType"];
                        payment.PaymentToken = (string)myData["PaymentToken"];
                        payment.DeviceType = (string)myData["DeviceType"];
                        payment.PaymentType = (string)myData["PaymentType"];
                        payment.CreatedDate = (DateTime)myData["CreatedDate"];
                        payment.ExpiryDate = Convert.ToDateTime(myData["ExpiryDate"]);
                        payment.TransactionStatus = (string)myData["TransactionStatus"];
                        payment.SubscriptionType = (string)myData["SubscriptionType"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/GetCoachPayment", "", ex.Message, "Exception", 0);
                throw;
            }

            return payment;
        }

        internal static List<ClubCoachDTO> GetSchoolCoaches()
        {
            List<ClubCoachDTO> schoolCoaches = new List<ClubCoachDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSchoolCoaches]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure; 
            
                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ClubCoachDTO v = new ClubCoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.ClubAddress = string.Empty;
                        v.ClubName = myData["ClubName"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Active = (myData["Active"] != DBNull.Value) && Convert.ToBoolean(myData["Active"]);
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        v.Location = myData["Location"].ToString();
                        v.SchoolName = myData["SchoolName"].ToString();
                        v.ZipCode = myData["ZipCode"].ToString();
                        v.StateId = myData["StateId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = myData["StateName"].ToString();
                        v.SchoolType = myData["SchoolType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolType"]);
                        v.ClubPhoneNumber = (myData["ClubPhoneNumber"] == DBNull.Value) ? string.Empty : myData["ClubPhoneNumber"].ToString();
                        v.SchoolId = myData["SchoolId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolId"]);
                        v.ClubId = (myData["ClubId"] == DBNull.Value) ? 0 : (int)myData["ClubId"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["LoginType"]);
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : (string)myData["Username"];
                        v.DeviceId = (myData["DeviceName"] == DBNull.Value) ? string.Empty : (string)myData["DeviceName"];
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"];
                        v.Almamater = (myData["Almamater"] == DBNull.Value) ? string.Empty : (string)myData["Almamater"];
                        v.TrainedSport = (myData["TrainedSport"] == DBNull.Value) ? string.Empty : (string)myData["TrainedSport"];
                        schoolCoaches.Add(v);
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/GetSchoolCoaches", "", ex.Message, "Exception", 0);
            }


            return schoolCoaches;
        }

        internal static List<ClubCoachDTO> GetClubCoaches()
        {
            List<ClubCoachDTO> clubCoaches = new List<ClubCoachDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetClubCoaches]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ClubCoachDTO v = new ClubCoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.ClubAddress = string.Empty;
                        v.ClubName = myData["ClubName"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Active = (myData["Active"] != DBNull.Value) && Convert.ToBoolean(myData["Active"]);
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        v.Location = myData["Location"].ToString();
                        v.SchoolName = myData["SchoolName"].ToString();
                        v.ZipCode = myData["ZipCode"].ToString();
                        v.StateId = myData["StateId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = myData["StateName"].ToString();
                        v.SchoolType = myData["SchoolType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolType"]);
                        v.ClubPhoneNumber = (myData["ClubPhoneNumber"] == DBNull.Value) ? string.Empty : myData["ClubPhoneNumber"].ToString();
                        v.SchoolId = myData["SchoolId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolId"]);
                        v.ClubId = (myData["ClubId"] == DBNull.Value) ? 0 : (int)myData["ClubId"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["LoginType"]);
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : (string)myData["Username"];
                        v.DeviceId = (myData["DeviceName"] == DBNull.Value) ? string.Empty : (string)myData["DeviceName"];
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"];
                        v.Almamater = (myData["Almamater"] == DBNull.Value) ? string.Empty : (string)myData["Almamater"];
                        v.TrainedSport = (myData["TrainedSport"] == DBNull.Value) ? string.Empty : (string)myData["TrainedSport"];
                        clubCoaches.Add(v);
                    }
                 

                    
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/GetClubCoaches", "", ex.Message, "Exception", 0);
            }


            return clubCoaches;
        }


        internal static int ContactUs(ContactUsDTO contactdto)
        {

            int Id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddContactUs]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Namepar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 500);
                    Namepar.Direction = ParameterDirection.Input;
                    Namepar.Value = contactdto.Name;

                    SqlParameter Emailpar = cmd.Parameters.Add("@Email", SqlDbType.VarChar, 200);
                    Emailpar.Direction = ParameterDirection.Input;
                    Emailpar.Value = contactdto.Email;

                    SqlParameter CoachIdpar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    CoachIdpar.Direction = ParameterDirection.Input;
                    CoachIdpar.Value = contactdto.CoachId;

                    SqlParameter Messagepar = cmd.Parameters.Add("@Message", SqlDbType.VarChar, 2000);
                    Messagepar.Direction = ParameterDirection.Input;
                    Messagepar.Value = contactdto.Message;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        Id = (int)myData["id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/ContactUs", "", ex.Message, "Exception", 0);
                throw;
            }

            return Id;
        }

        internal static List<ClubCoachDTO> GetSchoolClubCoachesForCoach(int CoachId)
        {
            List<ClubCoachDTO> schoolCoaches = new List<ClubCoachDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSchoolClubCoachesForCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter CoachIdpar = cmd.Parameters.Add("@CoachId", SqlDbType.VarChar, 2000);
                    CoachIdpar.Direction = ParameterDirection.Input;
                    CoachIdpar.Value = CoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ClubCoachDTO v = new ClubCoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.ClubAddress = string.Empty;
                        v.ClubName = myData["ClubName"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Active = (myData["Active"] != DBNull.Value) && Convert.ToBoolean(myData["Active"]);
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        v.Location = myData["Location"].ToString();
                        v.SchoolName = myData["SchoolName"].ToString();
                        v.ZipCode = myData["ZipCode"].ToString();
                        v.StateId = myData["StateId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = myData["StateName"].ToString();
                        v.SchoolType = myData["SchoolType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolType"]);
                        v.ClubPhoneNumber = (myData["ClubPhoneNumber"] == DBNull.Value) ? string.Empty : myData["ClubPhoneNumber"].ToString();
                        v.SchoolId = myData["SchoolId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolId"]);
                        v.ClubId = (myData["ClubId"] == DBNull.Value) ? 0 : (int)myData["ClubId"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["LoginType"]);
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : (string)myData["Username"];
                        v.DeviceId = (myData["DeviceName"] == DBNull.Value) ? string.Empty : (string)myData["DeviceName"];
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"];
                        v.Almamater = (myData["Almamater"] == DBNull.Value) ? string.Empty : (string)myData["Almamater"];
                        v.TrainedSport = (myData["TrainedSport"] == DBNull.Value) ? string.Empty : (string)myData["TrainedSport"]; 
                        schoolCoaches.Add(v);
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/GetSchoolClubCoachesForCoach", "", ex.Message, "Exception", 0);
            }


            return schoolCoaches;
        }

        internal static List<ChatDTO> GetLatestMessagesForCoach(int CoachId)
        {
            List<ChatDTO> chats = new List<ChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetLatestMessagesForCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter CoachIdpar = cmd.Parameters.Add("@CoachId", SqlDbType.VarChar, 2000);
                    CoachIdpar.Direction = ParameterDirection.Input;
                    CoachIdpar.Value = CoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChatDTO v = new ChatDTO();
                        v.ReceiverId = (int)myData["ReceiverId"];
                        v.SenderId = (int)myData["SenderId"];
                        v.Date = Convert.ToDateTime(myData["Date"]);
                        v.Message = myData["Message"].ToString();
                        v.Name = (myData["name"] == DBNull.Value) ? string.Empty : (string)myData["name"];
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : (string)myData["ProfilePicURL"];
                        chats.Add(v);
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/GetLatestMessagesForCoach", "", ex.Message, "Exception", 0);
            }


            return chats;
        }


        internal static List<ChatDTO> UpdateIsSeenNotifications(int NotificationId)
        {
            List<ChatDTO> chats = new List<ChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateIsSeenNotifications]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter NotificationIdpar = cmd.Parameters.Add("@NotificationId", SqlDbType.VarChar, 2000);
                    NotificationIdpar.Direction = ParameterDirection.Input;
                    NotificationIdpar.Value = NotificationId;

                    cmd.ExecuteNonQuery(); 
                   
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V6/UpdateIsSeenNotifications", "", ex.Message, "Exception", 0);
            }


            return chats;
        }
    }
}