﻿using Lemonaid_web_api.Models;
using System;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq; 
using System.Collections.Generic;
using System.Linq;

namespace Lemonaid_web_api.Controllers
{
    public class BuzzFeedController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

         
        [HttpGet]
        public Utility.CustomResponse GetDataFromXMLandInsert()
        {
            try
            {
                string sURL2 = "http://blog.lemonaidrecruiting.com/rss/";
                XDocument ourBlog = XDocument.Load(sURL2);

                // Query the <item>s in the XML RSS data and select each one into a new Post()
                IEnumerable<BlogsDTO> blogs =
                    from post in ourBlog.Descendants("item")
                    select new BlogsDTO(post);
                foreach (var blogDTO in blogs)
                {
                    BlogDTO blog = new BlogDTO();
                    blog.Title = blogDTO.Title;
                    blog.ShortDesc = blogDTO.ShortDesc;
                    blog.URL = blogDTO.URL;
                    blog.Description = blogDTO.Description;
                    blog.Date = blogDTO.Date;
                    blog.ImageURL = blogDTO.ImageURL;
                    blog.Type = blogDTO.Type;
                    UsersDal.AddBlog(blog);
                }
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Blog_Added;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        class BlogsDTO
        {
            public string Description { get; set; }
            public string Title { get; set; }
            public string ImageURL { get; set; }
            public string URL { get; set; }
            public DateTime? Date { get; set; }
            public string Type { get; set; }
            public string ShortDesc { get; set; }

            private static string GetElementValue(XContainer element, string name)
            {
                if ((element == null) || (element.Element(name) == null))
                    return String.Empty;
                return element.Element(name).Value;
            }

            public BlogsDTO(XContainer post)
            {
                string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
                // Get the string properties from the post's element values
                Title = GetElementValue(post, "title");
                URL = GetElementValue(post, "link");
                ShortDesc = GetElementValue(post, "description");
                Description = GetElementValue(post,
                   "{http://purl.org/rss/1.0/modules/content/}encoded"); 
                 
                // The Date property is a nullable DateTime? -- if the pubDate element
                // can't be parsed into a valid date, the Date property is set to null
                DateTime result;
                if (DateTime.TryParse(GetElementValue(post, "pubDate"), out result))
                    Date = (DateTime?)result;
                Type = "Blogs";

                MatchCollection matchesImgSrc = Regex.Matches(Description, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                ImageURL = "default";
                if (matchesImgSrc.Count != 0)
                    ImageURL = matchesImgSrc[0].Groups[1].Value;

                Description = Regex.Replace(Description, regexImgSrc, "", RegexOptions.IgnoreCase);
            }


        }

        //public static string FirstWords(string input, int numberWords)
        //{
        //    try
        //    {
        //        // Number of words we still want to display.
        //        int words = numberWords;
        //        // Loop through entire summary.
        //        for (int i = 0; i < input.Length; i++)
        //        {
        //            // Increment words on a space.
        //            if (input[i] == ' ')
        //            {
        //                words--;
        //            }
        //            // If we have no more words to display, return the substring.
        //            if (words == 0)
        //            {
        //                return input.Substring(0, i);
        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        // Log the error.
        //    }
        //    return string.Empty;

        //}

        //public static string StripHTML(string input)
        //{
        //    return Regex.Replace(input, "<.*?>", String.Empty);
        //}


        //[HttpGet]
        //public Utility.CustomResponse GetDataFromXmLandInsert()
        //{
        //    try
        //    {
        //        XmlDocument doc = new XmlDocument();
        //        doc.Load("http://mix.chimpfeedr.com/55fcf-LemonAid");
        //        XPathNavigator navigator = doc.CreateNavigator();

        //        XmlNode root = doc.DocumentElement;

        //        XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
        //        nsmgr.AddNamespace("fe", "http://www.w3.org/2005/Atom");

        //        XmlNodeList nodeList = root.SelectNodes(
        //             "descendant::fe:entry", nsmgr);
        //        nodeList = nodeList.Count >= 10 ? root.SelectNodes("descendant::fe:entry[position() <= 10]", nsmgr) : root.SelectNodes("descendant::fe:entry", nsmgr);

        //        foreach (XmlNode entry in nodeList)
        //        {
        //            BlogDTO blogDto = new BlogDTO
        //            {
        //                Title = entry["title"].InnerText,
        //                URL = entry["id"].InnerText,
        //                ShortDesc = entry["content"].InnerText.Split(' ').Length > 20 ? StripHTML(String.Join(" ", entry["content"].InnerText.Split(' '), 0, 20)) : StripHTML(entry["content"].InnerText),
        //                Description = entry["content"].InnerText,
        //                Date = Convert.ToDateTime(StripHTML(entry["updated"].InnerText)),
        //                Type = "Blogs",
        //                ImageURL = entry.LastChild.Attributes["href"] != null ? entry.LastChild.Attributes["href"].InnerText : ""
        //            };
        //            UsersDal.AddBlog(blogDto);
        //        }
        //        _result.Status = Utility.CustomResponseStatus.Successful;
        //        _result.Message = CustomConstants.Blog_Added;
        //        return _result;
        //    }
        //    catch (Exception ex)
        //    {
        //        EmailLogModel emaillog = new EmailLogModel();
        //        emaillog.AddEmailLog("BuzzFeed/GetDataFromXmLandInsert", "", ex.Message, "Exception", 0);
        //        _result.Status = Utility.CustomResponseStatus.Exception;
        //        _result.Message = ex.Message;
        //        return _result;
        //    }
        //}

    }
}
