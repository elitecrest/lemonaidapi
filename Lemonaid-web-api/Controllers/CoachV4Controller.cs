﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Lemonaid_web_api.Models;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Configuration;

namespace Lemonaid_web_api.Controllers
{
    public class CoachV4Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse Enroll_v4(CoachUserDTO coachUser)
        {

            try
            { 
                coachUser = UsersDalV4.AddCoachUser_v4(coachUser);
                CoachDTO coachInfo = UsersDal.GetCoachExists(coachUser.Emailaddress,coachUser.Password);
                if (coachInfo.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coachInfo;
                    _result.Message = CustomConstants.Enrolled_successfully;

                    CoachController cc = new CoachController();
                    bool isvalid =  cc.CheckForValidEmail(coachInfo.InstituteEmailaddress,coachUser.Emailaddress);
                    //Compare with the domain name . if matched send to coach or else to support and UnivEmail
                    if (isvalid)
                    {
                        DuoLoginController.SendMail(coachUser.Emailaddress, coachUser.ActivationCode, coachUser.CoachId.ToString());
                    }
                    else
                    {
                        SendMailToSupportandUniversity(coachUser.Emailaddress, coachUser.ActivationCode, coachUser.CoachId.ToString(), coachInfo.InstituteEmailaddress);
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.User_Does_Not_Exist;

                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV4/Enroll_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        public int SendMailToSupportandUniversity(string email, string activationcode, string coachId, string UniversityEmail)
        {
            SupportMailDTO support = new SupportMailDTO();
            MailMessage msg = new MailMessage();
            try
            {
                string baseURL = ConfigurationManager.AppSettings["BaseURL"];

                support = UsersDal.GetSupportMails("SupportEmail");

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/VerifyEmailToSupport.html"));
                var readFile = reader.ReadToEnd();
                string myString = readFile;
                myString = myString.Replace("$$Name$$", email);
                myString = myString.Replace("$$ActivationCode$$", activationcode);
                myString = myString.Replace("$$CoachId$$", coachId);
                myString = myString.Replace("$$BaseURL$$", baseURL);

                //MailAddress fromMail = new MailAddress(support.EmailAddress);
                //msg.From = fromMail;
                ////msg.To.Add(new MailAddress("" + UniversityEmail  + ""));
                //msg.To.Add(new MailAddress("" + support.EmailAddress + ""));
                //msg.Subject = " Verification link to complete coach enrollment";
                //msg.Body = myString;
                //msg.IsBodyHtml = true;
                //var smtpMail = new SmtpClient();
                //smtpMail.Host = support.Host;
                //smtpMail.Port = Convert.ToInt32(support.Port);
                //smtpMail.EnableSsl = Convert.ToBoolean(support.SSL);
                //smtpMail.UseDefaultCredentials = false;
                //smtpMail.Credentials = new System.Net.NetworkCredential(support.EmailAddress, support.Password);
                //smtpMail.Timeout = 1000000;
                //smtpMail.Send(msg);


                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(UniversityEmail);
                sendGridModel.fromMail = support.EmailAddress;
                sendGridModel.fromName = "Lemonaid";
                sendGridModel.subject = " Verification link to complete coach enrollment";
                sendGridModel.Message = myString;
                sendGridModel.email = support.EmailAddress;
                sendGridModel.password = support.Password;
                SendGridSMTPMail.SendEmail(sendGridModel);

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(support.EmailAddress, support.EmailAddress, msg.Subject, "Mail", 1);
                return 0;
            }
            catch (Exception)
            { 
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(support.EmailAddress, support.EmailAddress, msg.Subject, "Mail", 0);
                return 1;

            }
        }


        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            cipherText = cipherText.Replace(' ', '+');
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                if (encryptor != null)
                {
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            return cipherText;
        }

        [HttpGet]
        public Utility.CustomResponse GetCoachesFavourites_v4(string emailaddress, int offset, int max)
        {

            try
            {
                AthleteController athCtrl = new AthleteController();
                var atheletes = UsersDal.GetCoachesFavourites(emailaddress, offset, max);
                CoachDTO coach = UsersDal.GetCoachProfileByEmail(emailaddress);
                int coachId = coach.CoachId;
                foreach (var a in atheletes)
                {
                    int chatCount = UsersDalV4.GetChatCount(a.Id,coachId);
                    a.ChatCount = chatCount;
                }

                int totalLikesCount = atheletes.Count(p => p.Liked);

                for (var i = 0; i < atheletes.Count; i++)
                {
                    atheletes[i].TotalLikedCount = totalLikesCount;
                }
                atheletes = athCtrl.AddBesttimesToList(atheletes);
                atheletes = athCtrl.AddVideosToList(atheletes);
                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Matches;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV4/GetCoachesFavourites_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse UpdateCoachProfile_V4(CoachProfileDTO coachProfileDto)
        {

            try
            {
                if (coachProfileDto.Description == "-1")
                {
                    coachProfileDto.Description = string.Empty;
                }

                if (coachProfileDto.Phonenumber == "-1")
                {
                    coachProfileDto.Phonenumber = string.Empty;
                }

                UsersDalV4.UpdateCoachProfile_v4(coachProfileDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Coach_Profile_Updated;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV4/UpdateCoachProfile_V4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse CoachProfile_v4(int coachId)
        {

            try
            {
                var coach = UsersDalV4.GetCoachProfile_v4(coachId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = coach;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV4/CoachProfile_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        //Get College Atheletes ForTeamRoaster 
        [HttpGet]
        public Utility.CustomResponse GetCollegeAtheletes_v4(int instituteId, int atheleteId, int offset, int max, int coachId)
        {
            try
            {
                //When Athelete logs in Coachid  = 0 and viceVersa
                int userId = 0;
                userId = (atheleteId == 0) ? coachId : atheleteId;
                userId = (coachId == 0) ? atheleteId : coachId;

                var collegeAthletes = UsersDal.GetCollegeAtheletesForInstitute(instituteId, atheleteId, offset, max);
                foreach (var a in collegeAthletes)
                {

                    //send Coachid as extra parameter
                    var chatCount = UsersDalV4.GetChatCount(a.Id, userId);
                    a.ChatCount = chatCount;
                }
                if (collegeAthletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = collegeAthletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Records_Roster;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV4/GetCollegeAtheletes_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        //Get College Coaches ForTeamRoaster 
        [HttpGet]
        public Utility.CustomResponse GetCollegeCoaches_v4(int instituteId, int coachId, int offset, int max, int atheleteId)
        {
            try
            {
                //When Athelete logs in Coachid  = 0 and viceVersa
                int UserId = 0;
                UserId = (atheleteId == 0) ? coachId : atheleteId;
                UserId = (coachId == 0) ? atheleteId : coachId;

                var collegeCoaches = UsersDal.GetCollegeCoachesForInstitute(instituteId, coachId, offset, max);
                foreach (var a in collegeCoaches)
                {

                    //send Coachid as extra parameter
                    int chatCount = UsersDalV4.GetChatCount(a.CoachId, UserId);
                    a.ChatCount = chatCount;
                }
                if (collegeCoaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = collegeCoaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Staff;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV4/GetCollegeCoaches_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        //ForTeamRoaster adding from AddMember
        [HttpGet]
        public Utility.CustomResponse AddAtheleteToTeamRoaster_v4(int atheleteId, int instituteId, string atheleteEmail, string coachEmail)
        {

            try
            { 
               
                try
                {
                    string message = "Dear " + atheleteEmail + ", A coach with emailaddress " + coachEmail + " has indicated you are a part of their team.Please accept your roster spot.";
                    string athleteName = string.Empty, athleteEmailAddress = string.Empty; string deviceName = string.Empty; string deviceId = string.Empty; int loginType
                        = 0; string username = string.Empty;
                    UsersDalV4.GetAthleteDetails_v4(atheleteId, ref athleteName, ref athleteEmailAddress, ref deviceId, ref deviceName,ref loginType, ref username);
                    DAL2V6.PushNotification(message, deviceName);
                    string baseURL = ConfigurationManager.AppSettings["BaseURL"];

                    StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/TeamRosterCoachToAthelete_v4.html"));
                    string readFile = reader.ReadToEnd();
                    var myString = readFile;
                    myString = myString.Replace("$$AtheleteEmail$$", atheleteEmail);
                    myString = myString.Replace("$$AtheleteId$$", atheleteId.ToString());
                    myString = myString.Replace("$$InstituteId$$", instituteId.ToString());
                    myString = myString.Replace("$$CoachEmail$$", coachEmail);  
                    myString = myString.Replace("$$BaseURL$$", baseURL);

                    //Notifications Insertion 
                    NotificationDTO not = new NotificationDTO();
                    not.Status = 0;
                    not.UserID = atheleteId;
                    not.SenderId = instituteId;
                    not.NotificationId = 7;
                    not.ShortMessage = message;
                    not.LongMessage = myString;
                    not.SenderType = 1;
                    not.ReceiverType = 0;
                    UsersDal.AddNotifications(not);

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.Notification_Sent;

                    return _result;
                }
                catch (Exception ex)
                {
                    EmailLogModel emaillog = new EmailLogModel();
                    emaillog.AddEmailLog("CoachV4/AddAtheleteToTeamRoaster_v4", "", ex.Message, "Exception", 0);
                    _result.Status = Utility.CustomResponseStatus.Exception;
                    _result.Message = ex.Message;
                    return _result;
                }


            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV4/AddAtheleteToTeamRoaster_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse UpdateCoachPassword()
        {

            try
            {
                List<CoachUserDTO> coaches;
                coaches = UsersDalV4.GetAllCoaches();
                for (int i = 0; i <= coaches.Count; i++)
                {
                    if (String.IsNullOrEmpty(coaches[i].EncryptedPassword))
                    {
                        UsersDalV4.UpdateCoachPassword(coaches[i].CoachId, coaches[i].Password);
                    }
                }
                _result.Status = Utility.CustomResponseStatus.Successful; 
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV4/UpdateCoachPassword", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
    }
}
