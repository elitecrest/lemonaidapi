﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net; 
using System.Web.Http;  
using System.Configuration;  
using System.IO;  
using System.Web;
using System.Net.Mail;
using Lemonaid_web_api.Models;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Lemonaid_web_api.Controllers
{
    public class AthleteController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse GetAthleteFavourites(string emailaddress, int offset, int max)
        {
          
            try
            {
                var atheletes = UsersDal.GetAtheleteFavourites(emailaddress, offset, max);
                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Matches;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpGet]
        public Utility.CustomResponse GetAthleteFavourites_v2(string emailaddress, int offset, int max)
        {
          
            try
            {
                CoachController coachController = new CoachController();
                var coaches = UsersDal.GetAtheleteFavourites_v2(emailaddress, offset, max);
                var collegeAtheletes = UsersDal.GetCollegeAtheletesInAtheleteFavourites(emailaddress, offset, max);

                List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches = new List<CollegeAtheleteCoachesDTO>();

                collegeAtheleteCoaches = coachController.AddCoachestoList(coaches, collegeAtheleteCoaches);
                collegeAtheleteCoaches = coachController.AddAtheletestoList(collegeAtheletes, collegeAtheleteCoaches);

                var totalLikesCount = collegeAtheleteCoaches.Count(p => p.Liked);

                foreach (CollegeAtheleteCoachesDTO t in collegeAtheleteCoaches)
                {
                    t.TotalLikedCount = totalLikesCount;
                }

                IEnumerable<CollegeAtheleteCoachesDTO> sortedCollegeAtheleteCoaches = collegeAtheleteCoaches.ToList().OrderByDescending(x => x.Liked);

                IEnumerable<CollegeAtheleteCoachesDTO> enumcollegeAtheleteCoaches = sortedCollegeAtheleteCoaches.Skip(offset).Take(max);


                if (enumcollegeAtheleteCoaches.ToList().Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = enumcollegeAtheleteCoaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse EventTypes() 
        {
             
            try
            {
                var eventTypes = UsersDal.GetEventTypes();
                if (eventTypes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = eventTypes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
         

        [HttpPost]
        public Utility.CustomResponse UpdateFbFriendsandShares(FriendsSharesDTO friendsSharesDto)
        {
          
            try
            {

                UsersDal.UpdateFbFriendsandShares(friendsSharesDto);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddAthleteBestimes(List<AthleteEventDTO> athleteEventDto)
        {  
            try
            { 
                UsersDal.DeleteAthleteBesttimes(athleteEventDto[1].Emailaddress);


                for (int i = 0; i < athleteEventDto.Count; i++)
                {
                    var athletes = UsersDal.AddAthleteBesttimes(athleteEventDto[i]);

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athletes;
                    _result.Message = CustomConstants.Athlete_Bestimes_Added;

                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddAthlete(AthleteDTO athleteDto)
        { 
            try
            {
                if (athleteDto.AtheleteType == 0)
                {
                    athleteDto.AtheleteType = Convert.ToInt32(Utility.AtheleteType.HighSchoolAthelete);
                    athleteDto.Active = false;
                }

                var athleteid = UsersDal.AddAthlete(athleteDto);
                if (athleteid > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteid;
                    _result.Message = CustomConstants.Athlete_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                } 

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddAthlete_v2(AthleteDTO athleteDto)
        {
          

            try
            {
                if (athleteDto.AtheleteType == 0)
                {
                    athleteDto.AtheleteType = Convert.ToInt32(Utility.AtheleteType.HighSchoolAthelete);
                    athleteDto.Active = false;
                }

                var athleteid = UsersDal.AddAthlete_v2(athleteDto);
                if (athleteid > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteid;
                    _result.Message = CustomConstants.Athlete_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
  
        [HttpPost]
        public Utility.CustomResponse AddChat(ChatDTO chatDto)
        { 
            try
            { 
                var chatid = UsersDal.AddChatMessages(chatDto);
                //Get Count from [GetChatMessagesUnreadCount] and insert into AddChatcoachcount
                if (chatDto.ReceiverType == 1)
                {
                    int count = Dal2_v3.GetChatMessagesUnreadCount(chatDto.SenderId, chatDto.ReceiverId);
                    //Insert into CaoachAthleteChatCount
                    Dal2_v3.AddCoachAthleteChatCount(chatDto.SenderId, chatDto.ReceiverId, count, 0, 1);
                }
                if (chatid > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = chatid;
                    _result.Message = CustomConstants.Chat_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse AddAtheleteMatch(AtheleteMatchDTO atheleteMatchDto)
        { 
            try
            {

                var matchId = UsersDal.AddAtheleteMatch(atheleteMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (atheleteMatchDto.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DeleteAtheleteMatch(AtheleteMatchDTO atheleteMatchDto)
        {
            
            try
            {

                var matchId = UsersDal.DeleteAtheleteMatch(atheleteMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse GetChatMessages(ChatDTO chatDto)
        { 
            try
            {

                var chats = UsersDal.GetChatMessages(chatDto);
                if(chatDto.ReceiverType == 1)
                {
                    Dal2_v3.AddCoachAthleteChatCount(chatDto.SenderId, chatDto.ReceiverId, 0, 0, 1);
                }
                if (chats.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = chats;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
         
        [HttpGet]
        public Utility.CustomResponse DeleteAtheleteCoaches(string emailAddress)
        {
            
            try
            {
                var badges = UsersDal.DeleteAtheleteCoaches(emailAddress);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetAllAthletesByLocation(string emailaddress, double latitude, double longitude, int offset, int max, int gpa, int social, int locationType, string strokes, string description) // Coach email address
        {
             
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }
                List<AthleteDTO> athletes = UsersDal.GetAllAthletePaging(emailaddress, offset, max, gpa, social, description, strokes);

                List<AthleteDTO> newList = new List<AthleteDTO>();
                List<AthleteDTO> newListFromStrokes = new List<AthleteDTO>();

                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = athletes;

                }
                else
                    if (locationType == 2)
                    {
                        foreach (AthleteDTO t in athletes)
                        {
                            double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                            double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                            double distance = DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(t);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            for (int i = 0; i < athletes.Count; i++)
                            {
                                double athleteLatitude = (athletes[i].Latitude == "") ? 0.0 : Convert.ToDouble(athletes[i].Latitude);
                                double athleteLongitude = (athletes[i].Longitude == "") ? 0.0 : Convert.ToDouble(athletes[i].Longitude);
                                double distance = DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                if (distance <= 5)
                                {
                                    newList.Add(athletes[i]);
                                }
                            }
                        }


                if (strokes != "ALL")
                {
                    string[] strStrokes = strokes.Split(',');

                    List<int> atheleteIds = new List<int>();
                    List<AtheleteStrokesDTO> atheleteStrokes = UsersDal.GetStrokesForAtheletes();
                    for (int i = 0; i < atheleteStrokes.Count; i++)
                    {
                        for (int j = 0; j < strStrokes.Length; j++)
                        {
                            if (strStrokes[j] == atheleteStrokes[i].Category)
                            {
                                atheleteIds.Add(atheleteStrokes[i].AtheleteId);
                            }
                        }
                    }

                    for (int i = 0; i < newList.Count; i++)
                    {
                        for (int j = 0; j < atheleteIds.Count; j++)
                        {
                            if (atheleteIds[j] == newList[i].Id)
                            {

                                newListFromStrokes.Add(newList[i]);
                            }
                        }
                    }
                    newListFromStrokes = newListFromStrokes.Distinct().ToList();
                }
                else
                {

                    newListFromStrokes = newList;
                }


                if (newListFromStrokes.Count > 0)
                {
                    //Add BestTimes
                    newListFromStrokes = AddBesttimesToList(newListFromStrokes);
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newListFromStrokes = GetAdAthletes(ads, newListFromStrokes);
                    //Add Athelete Videos
                    newListFromStrokes = AddVideosToList(newListFromStrokes);
                  
                   
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = newListFromStrokes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }




        public List<AthleteDTO> AddBadgesToList(List<AthleteDTO> newListFromStrokes)
        {
         
            BadgesDTO badges;
            for (int i = 0; i < newListFromStrokes.Count; i++)
            { 
                badges = UsersDal.GetAtheleteBadges(newListFromStrokes[i].Emailaddress);
                newListFromStrokes[i].Diamond = badges.Diamond;  
            }

            return newListFromStrokes;
            
        }



        public List<AthleteDTO> GetAdAthletes(List<AdDTO> ads, List<AthleteDTO> atheletes)
        {
            
            for (int i = 0; i < ads.Count; i++)
            {
                AthleteDTO athlete = new AthleteDTO();
                athlete.Id = ads[i].Id;
                athlete.Name = ads[i].Name;
                athlete.ProfilePicURL = ads[i].ProfilePicURL;
                athlete.Description = ads[i].Description;
                athlete.CoverPicURL = ads[i].VideoURL;
                athlete.AdType = ads[i].AdType;
                athlete.School = "";
                athlete.Emailaddress = "";
                athlete.GPA = 0.0;
                athlete.Social = 0;
                athlete.SAT = 0;
                athlete.ACT = 0;
                athlete.InstituteId = 0;
                athlete.Gender = 0;
                athlete.FacebookId = ""; 
                athlete.DOB = "";
                athlete.Address = "";
                athlete.Latitude = "";
                athlete.Longitude = "";
                athlete.TotalCount = 0;
                athlete.FriendsInApp = 0;
                athlete.SharesApp = 0;
                athlete.AverageRating = 0;
                athlete.AcademicLevel = 0;
                athlete.YearLevel = 0;
                athlete.FriendsRate = 0;
                athlete.ShareRate = 0;
                athlete.SocialRate = 0;
                athlete.AcademicRate = 0;
                athlete.AtheleteRate = 0;
                athlete.Class = 0;
                atheletes.Add(athlete);
            }

            return atheletes;
        }

        public List<AthleteDTO> AddBesttimesToList(List<AthleteDTO> newListFromStrokes)
        {

            List<AthleteEventDTO> atheleteEventDto;
            foreach (AthleteDTO t in newListFromStrokes)
            {
                atheleteEventDto = UsersDal.GetAthleteBesttimes(t.Id);
                t.AtheleteBesttimes = atheleteEventDto;
            }
            return newListFromStrokes;
        }

        public List<AthleteDTO> AddVideosToList(List<AthleteDTO> newListFromStrokes)
        {

            List<VideoDTO> atheleteVideoDto;
            foreach (AthleteDTO t in newListFromStrokes)
            {
                atheleteVideoDto = UsersDal.GetAtheleteVideos(t.Emailaddress);
                t.AtheleteVideos = atheleteVideoDto;
            }
            return newListFromStrokes;
        }



        [HttpPost]
        public Utility.CustomResponse AddAthleteInvitation(AtheleteInvitationDTO atheleteInvitationDto)
        { 
            try
            {

                var athleteIvnId = UsersDal.AddAthleteInvitation(atheleteInvitationDto);
                if (athleteIvnId > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteIvnId;
                    _result.Message = CustomConstants.Athlete_Invitation_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse UpdateAthleteAccept(AtheleteInvitationDTO atheleteInvitationDto)
        {
           

            try
            {

                var athleteIvnId = UsersDal.UpdateAthleteAccept(atheleteInvitationDto);
                if (athleteIvnId > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteIvnId;
                    _result.Message = CustomConstants.Athlete_Accepted_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpGet]
        public Utility.CustomResponse GetBadges(string emailAddress)
        {
            
            try
            {
                var badges = UsersDal.GetAtheletesBlogsBadges(emailAddress);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse  AtheleteProfile(string emailAddress)
        {
            try
            {
                AthleteDTO athleteDto = UsersDal.GetAthleteProfile(emailAddress);
                athleteDto.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athleteDto.Id);
                athleteDto.AtheleteVideos = UsersDal.GetAtheleteVideos(emailAddress);
                int measurementcount = GetMeasurementCount(athleteDto.Height,athleteDto.Height2, athleteDto.Weight, athleteDto.WingSpan, athleteDto.ShoeSize);
                athleteDto.MeasurementCount = measurementcount;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athleteDto;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }



        public int GetMeasurementCount(string height,string height1, string weight, string wingSpan, string shoeSize)
        {
          
            int totalHeightCount = 0;
            var heightCnt = (string.IsNullOrWhiteSpace(height) || Convert.ToString(height) == "0") ? 0 : 1;
            var height1Cnt = (string.IsNullOrWhiteSpace(height1)  || Convert.ToString(height1) == "0") ? 0 : 1;
            var weightCnt = (string.IsNullOrWhiteSpace(weight) || Convert.ToString(weight) == "0") ? 0 : 1;
            var wingSpanCnt = (string.IsNullOrWhiteSpace(wingSpan)  || Convert.ToString(wingSpan) == "0") ? 0 : 1;
            var shoeSizeCnt = (string.IsNullOrWhiteSpace(shoeSize)  || Convert.ToString(shoeSize) == "0") ? 0 : 1;
            if(heightCnt == 1 || height1Cnt == 1)
            { totalHeightCount = 1; }
             
            var count = totalHeightCount + weightCnt + wingSpanCnt + shoeSizeCnt;
            return count;
        }

        [HttpGet]
        public Utility.CustomResponse DeleteAtheleteVideo(string  emailAddress,int videoId)
        {
            try
            {
                UsersDal.DeleteAtheleteVideos(emailAddress, videoId); 

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = videoId;
                _result.Message = CustomConstants.Video_Deleted_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }

        [HttpPost]
        public Utility.CustomResponse AddVideos(VideoDTO videoDto)
        { 

            try
            {

                var video = UsersDal.AddVideos(videoDto);
                if (video > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = video;
                    _result.Message = CustomConstants.Video_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddVideos_V3(VideoDTO videoDto)
        {
           
            try
            {

                var video = UsersDal.AddVideos_v3(videoDto);
                if (video > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = video;
                    _result.Message = CustomConstants.Video_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse UpdateVideoThumbnail(ThumbnailDTO thumbnailDto)
        { 

            try
            {

                var video = UsersDal.UpdateVideoThumbnail(thumbnailDto);
                if (video.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = video;
                    _result.Message = CustomConstants.Thumbnail_Updated;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetAtheleteVideos(string emailAddress)
        {
          
            try
            {
                var videos = UsersDal.GetAtheleteVideos(emailAddress);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = videos;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }


        [HttpPost]
        public Utility.CustomResponse SendEmailToAthleteCoach(AtheleteSocialDTO athleteSocial)
        {
            // AtheleteSocialDTO athleteSocial = new AtheleteSocialDTO();
            AtheleteBadgesDTO athleteBadges = new AtheleteBadgesDTO();
            athleteBadges.AtheleteId = athleteSocial.AtheleteId;
            athleteBadges.Status = "Pending for Approval";
            UsersDal.AddAthleteBadges(athleteBadges, athleteSocial);
            //var athleteSocialInfo = UsersDAL.GetAthleteCoachEmail(athleteSocial); 
            SendBadgesToAthleteCoach(athleteSocial);
            _result.Status = Utility.CustomResponseStatus.Successful;
            _result.Message = CustomConstants.Mail_Sent;
            return _result;
        }

        [HttpGet]
        public Utility.CustomResponse EmailToAthlete(int athleteId, int coachId)
        {
            string athleteName = string.Empty, athleteEmailAddress = string.Empty;
            UsersDal.GetAthleteDetails(athleteId, ref athleteName, ref athleteEmailAddress);
            SendEmailToAthlete(athleteEmailAddress, athleteName);
            _result.Status = Utility.CustomResponseStatus.Successful;
            _result.Message = CustomConstants.Mail_Sent_To_Athlete;
            return _result;
        }

        private int SendEmailToAthlete(string emailAddress, string athleteName)
        {
            string Message = string.Empty;
            MailMessage Msg = new MailMessage();
            MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
            try
            {

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailToAthlete.html"));
                string readFile = reader.ReadToEnd();
                string myString = string.Empty;
                myString = readFile;
                myString = myString.Replace("$$AthleteName$$", athleteName);

                 
                //Msg.From = fromMail;
                //Msg.To.Add(new MailAddress("" + emailAddress + ""));
                //Msg.Subject = " Request to Upload the Videos into the APP";
                //Msg.Body = myString;
                //Msg.IsBodyHtml = true;
                //SmtpClient smtpMail = new SmtpClient();
                //smtpMail.Host = ConfigurationManager.AppSettings["EmailHost"];
                //smtpMail.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                //smtpMail.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
                //smtpMail.UseDefaultCredentials = false;
                //smtpMail.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                //smtpMail.Timeout = 1000000;
                //smtpMail.Send(Msg);

                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(emailAddress);
                sendGridModel.fromMail = fromMail.ToString();
                sendGridModel.fromName = "Lemonaid";
                sendGridModel.subject = " Request to Upload the Videos into the APP";
                sendGridModel.Message = myString;
                sendGridModel.email = ConfigurationManager.AppSettings["EmailAddress"];
                sendGridModel.password = ConfigurationManager.AppSettings["Password"];
                SendGridSMTPMail.SendEmail(sendGridModel);

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(fromMail.ToString(), emailAddress, Msg.Subject, "Mail", 1);
                return 0;
            }
            catch (SmtpException exx)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(fromMail.ToString(), emailAddress, Msg.Subject, "Mail", 0);
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }

        private int SendBadgesToAthleteCoach(AtheleteSocialDTO athleteSocialInfo)
        {
            string Message = string.Empty;
            string BadgeName = string.Empty;
            MailMessage Msg = new MailMessage();
            MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["ReqEmailAddress"].ToString());
            try
            {
                if(athleteSocialInfo.BadgeTypeId == 1)
                {
                    BadgeName = "All American";
                }
                else  if(athleteSocialInfo.BadgeTypeId == 2)
                {
                    BadgeName = "Academic All American";
                }
                else if (athleteSocialInfo.BadgeTypeId == 3)
                {
                    BadgeName = "Captain";
                }
                else if (athleteSocialInfo.BadgeTypeId == 4)
                {
                    BadgeName = "SwimSwamTop";
                }

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailToCoach.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$CoachName$$", athleteSocialInfo.CoachName);
                myString = myString.Replace("$$Name$$", athleteSocialInfo.AthleteName);
                myString = myString.Replace("$$AtheleteId$$", Convert.ToString(athleteSocialInfo.AtheleteId));
                myString = myString.Replace("$$TypeId$$", Convert.ToString(athleteSocialInfo.BadgeTypeId));
                myString = myString.Replace("$$Count$$", Convert.ToString(athleteSocialInfo.Count));
                myString = myString.Replace("$$BadgeName$$", BadgeName);
                string baseURL = ConfigurationManager.AppSettings["BaseURL"];
                myString = myString.Replace("$$BaseURL$$", baseURL);


                //Msg.From = fromMail;
                ////Msg.To.Add(new MailAddress("" + athleteSocialInfo.CoachEmailId + ""));
                //Msg.To.Add(athleteSocialInfo.CoachEmailId);
                //Msg.Subject = " Athlete Badges to coaches";
                //Msg.Body = myString ;
                //Msg.IsBodyHtml = true;
                //SmtpClient smtpMail = new SmtpClient();
                //smtpMail.Host = ConfigurationManager.AppSettings["ReqEmailHost"];
                //smtpMail.Port = Convert.ToInt32(ConfigurationManager.AppSettings["ReqPort"]);
                //smtpMail.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["ReqEnableSSL"]);
                //smtpMail.UseDefaultCredentials = false;
                //smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtpMail.Credentials = new NetworkCredential(Convert.ToString(ConfigurationManager.AppSettings["ReqEmailAddress"]), Convert.ToString(ConfigurationManager.AppSettings["ReqPassword"]));
                //smtpMail.Timeout = 1000000; 
                //smtpMail.Send(Msg);

                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(athleteSocialInfo.CoachEmailId);
                sendGridModel.fromMail = fromMail.ToString();
                sendGridModel.fromName = "Lemonaid";
                sendGridModel.subject = " Athlete Badges to coaches";
                sendGridModel.Message = myString;
                sendGridModel.email = Convert.ToString(ConfigurationManager.AppSettings["ReqEmailAddress"]);
                sendGridModel.password = Convert.ToString(ConfigurationManager.AppSettings["ReqPassword"]);
                SendGridSMTPMail.SendEmail(sendGridModel);

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(fromMail.ToString(), athleteSocialInfo.CoachEmailId, Msg.Subject, "Mail", 1);
                return 0;
            }
            catch (SmtpException exception)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(fromMail.ToString(), athleteSocialInfo.CoachEmailId, Msg.Subject, "Mail", 0);
              
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ChangeStatusOfAthleteBadges(string atheleteId, string typeId, string count)
        {

            AtheleteBadgesDTO athleteBadges = new AtheleteBadgesDTO();
         
            try
            {
              
                athleteBadges.AtheleteId = Convert.ToInt32(atheleteId);
                athleteBadges.Status = "Approved";


                var badges = UsersDal.UpdateAthleteBadgesStatus(athleteBadges);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse GetMutualLikesAthlete(int athleteId)
        {
            try
            {
                // AthleteController athCtrl = new AthleteController();
                var atheletes = UsersDal.GetMutualLikesAthlete(athleteId);

                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Universities;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AtheleteSignIn(AtheleteInstituteAccept atheleteInstituteAccept)
        {
            MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["ReqEmailAddress"]);
            MailMessage msg = new MailMessage();
            msg.From = fromMail;
            
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailToUniversityCoachAcceptance.html"));
                string readFile = reader.ReadToEnd();
                string myString = string.Empty;
                myString = readFile;
                myString = myString.Replace("$$UniversityEmail$$", atheleteInstituteAccept.InstituteEmail);
                myString = myString.Replace("$$AtheleteEmail$$", atheleteInstituteAccept.AtheleteEmail);
                myString = myString.Replace("$$AtheleteId$$", atheleteInstituteAccept.AtheleteId.ToString());
                myString = myString.Replace("$$InstituteId$$", atheleteInstituteAccept.InstituteId.ToString());
                string baseURL = ConfigurationManager.AppSettings["BaseURL"];
                myString = myString.Replace("$$BaseURL$$", baseURL);


                //msg.To.Add(new MailAddress("" + atheleteInstituteAccept.InstituteEmail + ""));
                //msg.Subject = " Athelete Acceptance for an University";
                //msg.Body = myString;
                //msg.IsBodyHtml = true;
                //SmtpClient smtpMail = new SmtpClient();
                //smtpMail.Host = ConfigurationManager.AppSettings["ReqEmailHost"];
                //smtpMail.Port = Convert.ToInt32(ConfigurationManager.AppSettings["ReqPort"]);
                //smtpMail.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["ReqEnableSSL"]);
                //smtpMail.UseDefaultCredentials = false;
                //// SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtpMail.Credentials = new  NetworkCredential(ConfigurationManager.AppSettings["ReqEmailAddress"], ConfigurationManager.AppSettings["ReqPassword"]);
                //smtpMail.Timeout = 1000000;
                //smtpMail.Send(msg);

                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(atheleteInstituteAccept.InstituteEmail);
                sendGridModel.fromMail = fromMail.ToString();
                sendGridModel.fromName = "Lemonaid";
                sendGridModel.subject = " Athelete Acceptance for an University";
                sendGridModel.Message = myString;
                sendGridModel.email = ConfigurationManager.AppSettings["ReqEmailAddress"];
                sendGridModel.password = ConfigurationManager.AppSettings["ReqPassword"];
                SendGridSMTPMail.SendEmail(sendGridModel);


                int atheleteId = UsersDal.UpdateAtheleteToCollegeAthlete(atheleteInstituteAccept.AtheleteId, 0); //After verification , instituteid will be updated.

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(fromMail.ToString(), atheleteInstituteAccept.InstituteEmail, msg.Subject, "Mail", 1);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = atheleteId;
                _result.Message = CustomConstants.SignIn_Mail;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(fromMail.ToString(), atheleteInstituteAccept.InstituteEmail, msg.Subject, "Mail", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AtheleteSignInFromBadges(AtheleteInstituteAccept atheleteInstituteAccept)
        {
            

            MailMessage msg = new MailMessage();
            MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["ReqEmailAddress"]);
            msg.From = fromMail;
            try
            {

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailToUniversityCoachAcceptance.html"));
                string readFile = reader.ReadToEnd();
                var myString = string.Empty;
                myString = readFile;
                myString = myString.Replace("$$UniversityEmail$$", atheleteInstituteAccept.InstituteEmail);
                myString = myString.Replace("$$AtheleteEmail$$", atheleteInstituteAccept.AtheleteEmail);
                myString = myString.Replace("$$AtheleteId$$", atheleteInstituteAccept.AtheleteId.ToString());
                myString = myString.Replace("$$InstituteId$$", atheleteInstituteAccept.InstituteId.ToString());
                string baseURL = ConfigurationManager.AppSettings["BaseURL"];
                myString = myString.Replace("$$BaseURL$$", baseURL);

                //msg.To.Add(new MailAddress("" + atheleteInstituteAccept.InstituteEmail + ""));
                //msg.Subject = " Athelete Acceptance for an University";
                //msg.Body = myString;
                //msg.IsBodyHtml = true;
                //SmtpClient smtpMail = new SmtpClient();
                //smtpMail.Host = ConfigurationManager.AppSettings["ReqEmailHost"];
                //smtpMail.Port = Convert.ToInt32(ConfigurationManager.AppSettings["ReqPort"]);
                //smtpMail.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["ReqEnableSSL"]);
                //smtpMail.UseDefaultCredentials = false;
                //smtpMail.Credentials = new  NetworkCredential(ConfigurationManager.AppSettings["ReqEmailAddress"], ConfigurationManager.AppSettings["ReqPassword"]);
                //smtpMail.Timeout = 1000000;
                //smtpMail.Send(msg);

                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(atheleteInstituteAccept.InstituteEmail);
                sendGridModel.fromMail = fromMail.ToString();
                sendGridModel.fromName = "Lemonaid";
                sendGridModel.subject = " Athelete Acceptance for an University";
                sendGridModel.Message = myString;
                SendGridSMTPMail.SendEmail(sendGridModel);

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(fromMail.ToString(), atheleteInstituteAccept.InstituteEmail, msg.Subject, "Mail", 1);

                // int atheleteId = UsersDAL.UpdateAtheleteToCollegeAthlete(atheleteInstituteAccept.AtheleteId, 0); //After verification , instituteid will be updated.

                _result.Status = Utility.CustomResponseStatus.Successful;
                //  Result.Response = atheleteId;
                _result.Message = CustomConstants.SignIn_Mail;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(fromMail.ToString(), atheleteInstituteAccept.InstituteEmail, msg.Subject, "Mail", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse UpdateAthleteInstitution(int atheleteId, int instituteId)
        {
      
            try
            {

                var athleteInsId = UsersDal.UpdateAthleteInstitution(atheleteId, instituteId);
                if (athleteInsId > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteInsId;
                    _result.Message = CustomConstants.Athlete_Institution_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpGet]
        public Utility.CustomResponse GetAllAtheletesByCollegeAtheleteEmail(string emailaddress, double latitude, double longitude, int offset, int max, int locationType, string description, string searchText) // CollegeAthlete email address
        {
 
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }
                List<AthleteDTO> athletes = UsersDal.GetAllAthletesByCollegeAthelete(emailaddress, offset, max, description, searchText);

                List<AthleteDTO> newList = new List<AthleteDTO>();
               

                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = athletes;

                }
                else
                    if (locationType == 2)
                    {
                        foreach (AthleteDTO t in athletes)
                        {
                            double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                            double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                            double distance = DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(t);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            foreach (AthleteDTO t in athletes)
                            {
                                double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                                double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                                double distance = DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                if (distance <= 5)
                                {
                                    newList.Add(t);
                                }
                            }
                        }


                if (newList.Count > 0)
                {
                    //Add BestTimes
                    newList = AddBesttimesToList(newList);
                   // List<AdDTO> ads = UsersDal.GetAds(offset)
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newList = GetAdAthletes(ads, newList);
                    //Add Athelete Videos
                    newList = AddVideosToList(newList);

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = newList;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        

        [HttpGet]
        public Utility.CustomResponse GetCollegeAthleteFavourites(string emailaddress, int offset, int max)
        {
         
            try
            {
                AthleteController athCtrl = new AthleteController();
                var atheletes = UsersDal.GetCollegeAtheleteFavourites(emailaddress, offset, max);
                atheletes = athCtrl.AddBesttimesToList(atheletes);
                atheletes = athCtrl.AddVideosToList(atheletes);
                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Matches;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddAtheleteandCollegeAthleteMatch(AtheleteCollegeAtheleteDTO atheleteMatchDto)
        { 
            try
            {

                var matchId = UsersDal.AddAtheleteandCollegeAthleteMatch(atheleteMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (atheleteMatchDto.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddCollegeAtheleteandAtheleteMatch(AtheleteCollegeAtheleteDTO atheleteMatchDto)
        {
          
            try
            {

                var matchId = UsersDal.AddCollegeAtheleteandAtheleteMatch(atheleteMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (atheleteMatchDto.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DeleteAtheleteandCollegeAthleteMatch(AtheleteCollegeAtheleteDTO atheleteCollegeAtheleteDto)
        { 

            try
            {

                var matchId = UsersDal.DeleteAtheleteandCollegeAthleteMatch(atheleteCollegeAtheleteDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DeleteCollegeAtheleteandAtheleteMatch(AtheleteCollegeAtheleteDTO atheleteCollegeAtheleteDto)
        { 
            try
            {

                var matchId = UsersDal.DeleteCollegeAtheleteandAtheleteMatch(atheleteCollegeAtheleteDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
         
        [HttpGet]
        public Utility.CustomResponse GetAllAthletesByLocationWithSearch(string emailaddress, double latitude, double longitude, int offset, int max, int gpa, int social, int locationType, string strokes, string description, string searchText) // Coach email address
        {
          
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }
                var athletes = UsersDal.GetAllAthletePagingWithSearch(emailaddress, offset, max, gpa, social, description, strokes, searchText);

                var newList = new List<AthleteDTO>();
                var newListFromStrokes = new List<AthleteDTO>();

                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = athletes;

                }
                else
                    if (locationType == 2)
                    {
                        foreach (var t in athletes)
                        {
                            double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                            double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                            double distance = DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(t);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            foreach (var t in athletes)
                            {
                                double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                                double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                                var distance = DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude );

                                if (distance <= 5)
                                {
                                    newList.Add(t);
                                }
                            }
                        }


                if (strokes != "ALL")
                {
                    string[] strStrokes = strokes.Split(',');

                    var atheleteIds = new List<int>();
                    var atheleteStrokes = UsersDal.GetStrokesForAtheletes();
                    foreach (var t in atheleteStrokes)
                    {
                        foreach (string t1 in strStrokes)
                        {
                            if (t1 == t.Category)
                            {
                                atheleteIds.Add(t.AtheleteId);
                            }
                        }
                    }

                    foreach (AthleteDTO t in newList)
                    {
                        foreach (int t1 in atheleteIds)
                        {
                            if (t1 == t.Id)
                            {

                                newListFromStrokes.Add(t);
                            }
                        }
                    }
                    newListFromStrokes = newListFromStrokes.Distinct().ToList();
                }
                else
                {

                    newListFromStrokes = newList;
                }


                if (newListFromStrokes.Count > 0)
                {
                    newListFromStrokes = AddBadgesToList(newListFromStrokes);
                    //Add BestTimes
                    newListFromStrokes = AddBesttimesToList(newListFromStrokes);
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newListFromStrokes = GetAdAthletes(ads, newListFromStrokes);
                    //Add Athelete Videos
                    newListFromStrokes = AddVideosToList(newListFromStrokes);


                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = newListFromStrokes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse AtheleteProfile_v2(string emailAddress)
        {
            try
            {

                AthleteDTO athleteDto = UsersDal.GetAthleteProfile_v2(emailAddress);
                if (athleteDto.Id > 0)
                {
                    athleteDto.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athleteDto.Id);
                    athleteDto.AtheleteVideos = UsersDal.GetAtheleteVideos(emailAddress);
                    int measurementcount = GetMeasurementCount(athleteDto.Height, athleteDto.Height2, athleteDto.Weight, athleteDto.WingSpan, athleteDto.ShoeSize);
                    athleteDto.MeasurementCount = measurementcount;
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteDto;
                    _result.Message = CustomConstants.Details_Get_Successfully;

                }
                else
                {
                    int cnt = DAL2V7.CheckAthleteExistsInAnySport(emailAddress);
                    if (cnt > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.AthleteInOtherSport;
                        _result.Response = 0;
                        _result.Message = CustomConstants.Athlete_In_Other_Sport;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = 0;
                        _result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }
        [HttpGet]
        public Utility.CustomResponse AtheleteProfile_v3(string emailAddress)
        {
            try
            {
                AthleteDTO athleteDto = UsersDal.GetAthleteProfile_v2(emailAddress);
                athleteDto.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athleteDto.Id);
                athleteDto.AtheleteVideos = UsersDal.GetAtheleteIdealVideos(emailAddress);
                int measurementcount = GetMeasurementCount(athleteDto.Height, athleteDto.Height2, athleteDto.Weight, athleteDto.WingSpan, athleteDto.ShoeSize);
                athleteDto.MeasurementCount = measurementcount;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athleteDto;
                _result.Message = CustomConstants.Details_Get_Successfully;  
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }

        [HttpGet]
        public Utility.CustomResponse DeleteCollegeAtheleteAtheletes(string emailAddress)
        {
          
            try
            {
                var badges = UsersDal.DeleteCollegeAtheleteAtheletes(emailAddress);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse IsCollegeAthleteActive(int atheleteId)
        {
          
            try
            {
                var results = UsersDal.IsCollegeAthleteActive(atheleteId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = results;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse AddAtheleteQuestionaire(QuestionaireDTO questionaireDto)
        {
            
            try
            {

                var questionaireid = UsersDal.AddAtheleteQuestionaire(questionaireDto);
                _result.Response = questionaireid;
                _result.Status = Utility.CustomResponseStatus.Successful; 
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetUserNotifications(int userid, int offset, int max)
        {

            try
            {
                var results = UsersDal.GetUserNotifications(userid, offset , max);
                if (results.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = results;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful; 
                    _result.Message = CustomConstants.No_Notifications;
                }
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }



        [HttpGet]
        public Utility.CustomResponse UpdateNotificationStatus(int id, int status)
        { 
            try
            { 
                var athleteInsId = UsersDal.UpdateNotificationStatus(id, status);


                if (status == 1)
                {
                    if (UpdateNotifications(id, status))
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = athleteInsId;
                        _result.Message = CustomConstants.Notification_Updated;
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteInsId;
                    _result.Message = CustomConstants.Notification_Updated;
                }
             
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        private bool UpdateNotifications(int id, int status)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            bool istrue = false;
            string baseUrl = ConfigurationManager.AppSettings["BaseURL"];
            NotificationDTO notification = UsersDal.GetNotificationsById(id);
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            if (notification.NotificationId == 4)
            {
                //get instituteId from CoachId
                CoachProfileDTO coach = UsersDal.GetCoachProfile(notification.UserID);
                var response = client.GetAsync("/Athlete/UpdateAthleteInstitution?AtheleteId=" + notification.SenderId + "&InstituteId=" + coach.InstituteId).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                    if (res.Status == Utility.CustomResponseStatus.Successful)
                    {
                        istrue = true;
                    }

                    //Delete from Wrapper and 
                }

            }
            else if (notification.NotificationId == 7)
            {
                var response = client.GetAsync("/Athlete/UpdateAthleteInstitution?AtheleteId=" + notification.UserID + "&InstituteId=" + notification.SenderId).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                    if (res.Status == Utility.CustomResponseStatus.Successful)
                    {
                        istrue = true;
                    }
                }
            }
            else if (notification.NotificationId == 5)
            {
                
                var response = client.GetAsync("/Athletev4/ChangeStatusOfAthleteBadges_v4?AtheleteId=" + notification.SenderId + "&typeId=" + notification.BadgeTypeId + "&count=" + notification.BadgeCount + "&status=" + status).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                    if (res.Status == Utility.CustomResponseStatus.Successful)
                    {
                        istrue = true;
                    }
                }
            } 
            else if (notification.NotificationId == 9)  
            {
                var response = client.GetAsync("/ClubCoach/UpdateClubCoachToAthelete?clubCoachId=" + notification.SenderId + "&atheleteId=" + notification.UserID).Result;
                if (response.IsSuccessStatusCode)
                {
                    res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                    if (res.Status == Utility.CustomResponseStatus.Successful)
                    {
                        istrue = true;
                    }
                }
            }


            return istrue;
        }
         

        public double DistanceTo(double lat1, double lon1, double lat2, double lon2, char unit = 'K')
        {
            double rlat1 = Math.PI * lat1 / 180;
            double rlat2 = Math.PI * lat2 / 180;
            double theta = lon1 - lon2;
            double rtheta = Math.PI * theta / 180;
            double dist =
                Math.Sin(rlat1) * Math.Sin(rlat2) + Math.Cos(rlat1) *
                Math.Cos(rlat2) * Math.Cos(rtheta);
            dist = Math.Acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            switch (unit)
            {
                case 'K': //Kilometers -> default
                    return dist * 1.609344;
                case 'N': //Nautical Miles 
                    return dist * 0.8684;
                case 'M': //Miles
                    return dist;
            }

            return dist;
        }



    }
}
 
