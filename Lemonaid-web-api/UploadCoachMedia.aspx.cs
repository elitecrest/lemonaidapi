﻿using Microsoft.WindowsAzure.Storage;
using Lemonaid_web_api.Models;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.WindowsAzure.MediaServices.Client;
using System.Text;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;

namespace Lemonaid_web_api
{
    public partial class UploadCoachMedia : System.Web.UI.Page
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _result = Handlefile();
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string jsonString = javaScriptSerializer.Serialize(_result);
                Response.Write(jsonString);
            }
        }

        private Utility.CustomResponse Handlefile()
        {
            string baseUrl = ConfigurationManager.AppSettings["BaseURL"];
            Stream stream = Request.InputStream;
            string videoFormat = Request.QueryString["VideoFormat"].Trim();
            string duration = Request.QueryString["Duration"].Trim();

            MediaContent video = new MediaContent();
            string videoUrl = Streamaudio(stream, videoFormat);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


            video.VideoURL = videoUrl;
            video.CoachId = Convert.ToInt32(Request.QueryString["CoachId"].Trim());
            video.VideoFormat = videoFormat;
            video.Duration = Convert.ToInt32(duration);

            video.ThumbnailURL = string.Empty;

            var response = client.PostAsJsonAsync("Athlete2V5/AddCoachVideoContent", video).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                var id = Convert.ToInt16(res.Response.ToString());

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = id;
                _result.Message = CustomConstants.Video_Added;
            }

            return _result;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }


        public string Streamaudio(Stream stream, string format)
        {
            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);
            var fileStream = File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();
            var ext = format;
            Stream fileStreambanners = stream;
            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string videoUrl = UploadImage1(fileNamebanners, path, ext, fileStreambanners);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            return videoUrl;
        }

        public string UploadImage1(string fileName, string path, string ext, Stream inputStream)
        {
            string account = ConfigurationManager.AppSettings["MediaServicesAccountName"];
            string key = ConfigurationManager.AppSettings["MediaServicesAccountKey"];
            CloudMediaContext context = new CloudMediaContext(account, key);
            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);
            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));
            assetFile.Upload(path);
            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 10000;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive), AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    Handlefile();
        //}
    }
}