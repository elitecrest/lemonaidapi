﻿
using System.Collections.Generic;
namespace Lemonaid_web_api.Models
{
    public class MetricsDto
    {

        public partial class ClubCoachInfoDto
        {
            public string ClubName { get; set; }

            public string CoachName { get; set; }

            public string CoachEmailId { get; set; }

            public string SchoolCoachName { get; set; }

            public string SchoolCoachEmail { get; set; }
        }



        public partial class SwimmingMetricsDto
        {
            public SwimmingMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int EventType1 { get; set; }
            public int EventType2 { get; set; }
            public int EventType3 { get; set; }
            public string EventTypeName1 { get; set; }
            public string EventTypeName2 { get; set; }
            public string EventTypeName3 { get; set; }
            public string BesttimeValue1 { get; set; }
            public string BesttimeValue2 { get; set; }
            public string BesttimeValue3 { get; set; }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
        }

        public partial class RowingMetricsDto
        {
            public RowingMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int EventType1 { get; set; }
            public int EventType2 { get; set; }
            public int EventType3 { get; set; }
            public string EventTypeName1 { get; set; }
            public string EventTypeName2 { get; set; }
            public string EventTypeName3 { get; set; }
            public string BesttimeValue1 { get; set; }
            public string BesttimeValue2 { get; set; }
            public string BesttimeValue3 { get; set; }
            public string Sweep { get; set; }
            public string Scull { get; set; }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
        }

        public partial class DivingMetricsDto
        {
            public DivingMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string _1MScore { get; set; }
            public string _3MScore { get; set; }
            public string _10MPlatform { get; set; }
            public string DivingExperience { get; set; }
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }

        }

        public partial class TennisMetricsDto
        {
            public TennisMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();
                this.Forehand = string.Empty; 

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string ITFRank { get; set; }
            public string USTARank { get; set; }
            public string Serve { get; set; }
            public string Forehand { get; set; }
            public string Backhand { get; set; }
            public string DominantHand { get; set; }
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }

        }

        public partial class TrackMetricsDto
        {
            public TrackMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto(); 
            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public int TrackEventId1 { get; set; }
            public int TrackEventId2 { get; set; }
            public int TrackEventId3 { get; set; }
            public int TrackEventId4 { get; set; }
            public string TrackEvent1 { get; set; }
            public string TrackEvent2 { get; set; }
            public string TrackEvent3 { get; set; }
            public string TrackEvent4 { get; set; }
            public int XCId { get; set; }
            public string XC { get; set; }
            public int FieldEventId1 { get; set; }
            public int FieldEventId2 { get; set; }
            public int FieldEventId3 { get; set; }
            public string FieldEvent1 { get; set; }
            public string FieldEvent2 { get; set; }
            public string FieldEvent3 { get; set; }
            public string VO2Max { get; set; } 
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string TrackEventName1 { get; set; }
            public string TrackEventName2 { get; set; }
            public string TrackEventName3 { get; set; }
            public string TrackEventName4 { get; set; }
            public string FieldEventName1 { get; set; }
            public string FieldEventName2 { get; set; }
            public string FieldEventName3 { get; set; }
            public string XCName { get; set; }
        }

        public partial class LacrosseMetricsDto
        {
            public LacrosseMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string Position { get; set; }
            public string DominantHand { get; set; }
            public string ShotSpeed { get; set; }
            public string _40YardDash { get; set; } 
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string PositionName { get; set; }

        }

        public partial class GolfMetricsDto
        {
            public GolfMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string USGAHandicap { get; set; }
            public string PoloGolfRanking { get; set; }
            public string AvgPuttsPerRound { get; set; }
            public string AvgDriveDistance { get; set; }
            public string AvgScore { get; set; }
            public string BestScore { get; set; }
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }

        }

        public partial class WaterpoloMetricsDto
        {
            public WaterpoloMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string PrimaryPosition { get; set; }
            public string SecondaryPosition { get; set; }
            public int SwimEventId1 { get; set; }
            public int SwimEventId2 { get; set; }
            public string SwimEvent1 { get; set; }
            public string SwimEvent2 { get; set; }
            public string DominantHand { get; set; }
            public string ShotSpeed { get; set; }
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string SwimEventName1 { get; set; }
            public string SwimEventName2 { get; set; }
            public string PrimaryPositionName { get; set; }
            public string SecondaryPositionName { get; set; }

        }

        public partial class PositionDTO
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
         

        public partial class TrackandFieldDps
        {
            public List<PositionDTO> FieldEvents  { get; set; }
            public List<PositionDTO> XCEvents { get; set; }
            public List<PositionDTO> TrackEvents  { get; set; }
            public List<PositionDTO> TRACKDecHeptEvents { get; set; }
        }



        public partial class BaseballMetricsDto
        {
            public BaseballMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string PrimaryPosition { get; set; }
            public string SecondaryPosition { get; set; }
            public string Throw { get; set; }
            public string Hit { get; set; }
            public string BattingAverage { get; set; }
            public string OBP { get; set; }
            public string Slugging { get; set; }
            public string _60YardDash { get; set; }
            public string PitchSpeed { get; set; }
            public string Innings { get; set; }
            public string ERA { get; set; }
            public string Pitches { get; set; }
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string PrimaryPositionName { get; set; }
            public string SecondaryPositionName { get; set; }
        }

        public partial class BasketballMetricsDto
        {
            public BasketballMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string PrimaryPosition { get; set; }
            public string SecondaryPosition { get; set; }
            public string PPG { get; set; }
            public string RPG { get; set; }
            public string APG { get; set; } 
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string PrimaryPositionName { get; set; }
            public string SecondaryPositionName { get; set; }
        }


        public partial class FootballMetricsDto
        {
            public FootballMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string PrimaryPosition { get; set; }
            public string SecondaryPosition { get; set; }
            public string ThirdPosition  { get; set; }
            public string Throw { get; set; }
            public string _40YardDash { get; set; }  
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string PrimaryPositionName { get; set; }
            public string SecondaryPositionName { get; set; }
            public string ThirdPositionName { get; set; } 
        }


        public partial class SoccerMetricsDto
        {
            public SoccerMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string PrimaryPosition { get; set; }
            public string SecondaryPosition { get; set; }
            public string DominantFoot { get; set; }
            public string _40YardDash { get; set; }
            public string Mile { get; set; } 
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string PrimaryPositionName { get; set; }
            public string SecondaryPositionName { get; set; }
        }

        public partial class SoftballMetricsDto
        {
            public SoftballMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string PrimaryPosition { get; set; }
            public string SecondaryPosition { get; set; }
            public string Throw { get; set; }
            public string Hit { get; set; }
            public string BattingAverage { get; set; }
            public string OBP { get; set; }
            public string Slugging { get; set; }
            public string _60YardDash { get; set; }
            public string PitchSpeed { get; set; }
            public string Innings { get; set; }
            public string ERA { get; set; }
            public string Pitches { get; set; }
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string PrimaryPositionName { get; set; }
            public string SecondaryPositionName { get; set; }
        }

        public partial class IceHockeyMetricsDto
        {
            public IceHockeyMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string PrimaryPosition { get; set; }
            public string SecondaryPosition { get; set; }
            public string DominantHand { get; set; }
            public string ShotSpeed { get; set; }  
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string PrimaryPositionName { get; set; }
            public string SecondaryPositionName { get; set; }
        }

        public partial class WrestlingMetricsDto
        {
            public WrestlingMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string PrimaryClass { get; set; }
            public string SecondaryClass { get; set; }
            public string WLRecordinHS { get; set; }
            public string Folkstyle { get; set; }
            public string GrecoRoman { get; set; }
            public string Freestyle { get; set; }
            public string FutureOlyRankings { get; set; }
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string PrimaryClassName { get; set; }
            public string SecondaryClassName { get; set; }
        }

        public partial class VolleyballMetricsDto
        {
            public VolleyballMetricsDto()
            {
                this.ClubCoachInfoDto = new ClubCoachInfoDto();

            }
            public int AthleteId { get; set; }
            public List<VideoDTO> AtheleteVideos { get; set; }
            public string PrimaryPosition { get; set; }
            public string SecondaryPosition { get; set; }
            public string DominantHand { get; set; }
            public string ShotSpeed { get; set; }
            public string StandingReach { get; set; }
            public string JumpBlock { get; set; }
            public string ApproachJump { get; set; }
            [Optional]
            public ClubCoachInfoDto ClubCoachInfoDto { get; set; }
            public string PrimaryPositionName { get; set; }
            public string SecondaryPositionName { get; set; }
        }
    }
}