﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Lemonaid_web_api.Models
{
    public class DAL_Notif
    {

        internal static List<UsertypeChatDTO> GetUsersChatAdChatCount(string UserType)
        {

            List<UsertypeChatDTO> UserTypeChats = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUsersChatAdChatCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usertypeparam = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 50);
                    usertypeparam.Direction = ParameterDirection.Input;
                    usertypeparam.Value = UserType;

                    SqlDataReader myData = cmd.ExecuteReader();
                    UserTypeChats = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL_Notif/GetUsersChatAdChatCount", "", ex.Message, "Exception", 0);
            }

            return UserTypeChats;
        }

        internal static List<UsertypeChatDTO> GetUsersChatCount(string UserType)
        {

            List<UsertypeChatDTO> UserTypeChats = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUsersChatCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usertypeparam = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 50);
                    usertypeparam.Direction = ParameterDirection.Input;
                    usertypeparam.Value = UserType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    UserTypeChats = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL_Notif/GetUsersChatCount", "", ex.Message, "Exception", 0);
            }

            return UserTypeChats;
        } 

        internal static List<UsertypeChatDTO> GetChatsReader(SqlDataReader myData)
        {
            List<UsertypeChatDTO> chatlist = new List<UsertypeChatDTO>();
            while (myData.Read())
            {
                UsertypeChatDTO Chats = new UsertypeChatDTO();
                Chats.ID = (myData["id"] == DBNull.Value) ? 0 : (int)myData["id"];
                Chats.Email = (string)myData["emailaddress"].ToString();
                Chats.LoginType = (myData["logintype"] == DBNull.Value) ? 0 : (int)myData["logintype"];
                Chats.UserName = (myData["username"] == DBNull.Value) ? string.Empty : (string)myData["username"].ToString();
                Chats.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"].ToString();
                Chats.DeviceID = (myData["DeviceId"] == DBNull.Value) ? string.Empty : (string)myData["DeviceId"].ToString();
                chatlist.Add(Chats);
            }
            return chatlist;
        }
         
        internal static List<UsertypeChatDTO> GetNoSwipesForAtheleteToInstitute()
        {

            List<UsertypeChatDTO> NoSwipesforAtheletetoInst = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoSwipesForAtheleteToInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoSwipesforAtheletetoInst = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL_Notif/GetNoSwipesForAtheleteToInstitute", "", ex.Message, "Exception", 0);
            }

            return NoSwipesforAtheletetoInst;
        }

        internal static List<UsertypeChatDTO> GetNoSwipesForInstituteToAthelete()
        {

            List<UsertypeChatDTO> NoSwipesforInsttoAthelete = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoSwipesForInstituteToAthelete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoSwipesforInsttoAthelete = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL_Notif/NoSwipesForInstituteToAthelete", "", ex.Message, "Exception", 0);
            }

            return NoSwipesforInsttoAthelete;
        }

        internal static List<UsertypeChatDTO> GetNoSwipesForFFFToAthelete()
        {

            List<UsertypeChatDTO> NoSwipesforFFFtoAthelete = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoSwipesForFFFToAthelete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoSwipesforFFFtoAthelete = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL_Notif/NoSwipesforFFFtoAthelete", "", ex.Message, "Exception", 0);
            }

            return NoSwipesforFFFtoAthelete;
        }

        internal static List<UsertypeChatDTO> GetNoSwipesForFFFToInstitute()
        {

            List<UsertypeChatDTO> NoSwipesforFFFtoInst = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoSwipesForFFFToInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoSwipesforFFFtoInst = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL_Notif/NoSwipesforFFFtoInst", "", ex.Message, "Exception", 0);
            }

            return NoSwipesforFFFtoInst;
        }

        internal static List<UsertypeChatDTO> GetNoSwipesForClubCoachToInstitute()
        {

            List<UsertypeChatDTO> NoSwipesforClctoInst = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoSwipesForClubCoachToInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoSwipesforClctoInst = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL_Notif/NoSwipesforClctoInst", "", ex.Message, "Exception", 0);
            }

            return NoSwipesforClctoInst;
        }

        internal static List<UsertypeChatDTO> GetNoRecomendsForCLCToInstitute()
        {

            List<UsertypeChatDTO> NoRecomendsforClctoInst = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoRecomendsForCLCToInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoRecomendsforClctoInst = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL_Notif/NoRecomendsforClctoInst", "", ex.Message, "Exception", 0);
            }

            return NoRecomendsforClctoInst;
        }

        internal static List<UsertypeChatDTO> GetNoRecomendsForFFFToInstitute()
        {

            List<UsertypeChatDTO> NoRecomendsforFFFtoInst = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoRecomendsForFFFToInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoRecomendsforFFFtoInst = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL_Notif/NoRecomendsforFFFtoInst", "", ex.Message, "Exception", 0);
            }

            return NoRecomendsforFFFtoInst;
        }

    }
}