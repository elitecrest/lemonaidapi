﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;

namespace Lemonaid_web_api.Controllers
{
    public class ClubCoach2V4Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse GetClubCoachInstituteMatchStatus(int clubCoachId, int instituteId)
        {
            try
            {
                var status = DAL2V4.GetClubCoachInstituteMatchStatus(clubCoachId, instituteId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = status;

                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
    }
}
