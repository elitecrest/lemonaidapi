﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;


namespace Lemonaid_web_api.Controllers
{
    public class RefreshController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse RefreshAtheleteCoaches(RefreshDTO refreshDto)
        {
            try
            {
                var badges = UsersDalV4.DeleteAtheleteCoaches_v4(refreshDto.Id);

                var chatads = DAL2V6.RefreshChatAds(refreshDto);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Refresh/RefreshAtheleteCoaches", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse RefreshCollegeAtheleteAtheletes(RefreshDTO refreshDto)
        { 
            try
            {
                var id = UsersDalV4.DeleteCollegeAtheleteAtheletes_v4(refreshDto.Id); 
                var chatads = DAL2V6.RefreshChatAds(refreshDto);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = id;
                _result.Message = CustomConstants.Details_Get_Successfully; 

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Refresh/RefreshCollegeAtheleteAtheletes", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse RefreshClubCoachInstitutes(RefreshDTO refreshDto)
        {

            try
            {
                var ccId = UsersDal.DeleteClubCoachInstitutes(refreshDto.Id);
                var chatads = DAL2V6.RefreshChatAds(refreshDto);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = ccId;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Refresh/RefreshClubCoachInstitutes", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse RefreshFamilyfriendInstitutes(RefreshDTO refreshDto)
        {

            try
            {
                var clubCoachId = UsersDalV5.DeleteFamilyfriendInstitutes(refreshDto.Id);
                var chatads = DAL2V6.RefreshChatAds(refreshDto);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = clubCoachId;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Refresh/RefreshFamilyfriendInstitutes", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse RefreshCoachAthletes(RefreshDTO refreshDto)
        {
            try
            {
                var badges = UsersDal.DeleteCoachAtheletes(refreshDto.Emailaddress);  
                var chatads = DAL2V6.RefreshChatAds(refreshDto);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Refresh/RefreshCoachAthletes", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
    }
}
