﻿using System.Data.SqlClient;
using System;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace Lemonaid.Worker
{
    public class BuzzFeed
    { 
        public static  void GetDataFromXMLandInsert(string Conn)
        {
            try
            { 
                int DeletedBlogCoount = DeleteBlogs(Conn);
                List<RssLinkDTO> LinksInEachSport = GetLinksBySport(Conn);
                foreach (var r in LinksInEachSport)
                {

                    string sURL2 = r.RssLink;
                    XDocument ourBlog = XDocument.Load(sURL2);

                    // Query the <item>s in the XML RSS data and select each one into a new Post()
                    IEnumerable<BlogsDTO> blogs =
                        from post in ourBlog.Descendants("item")
                        select new BlogsDTO(post);
                    foreach (var blogDTO in blogs)
                    {
                        BlogDTO blog = new BlogDTO();
                        blog.Title = blogDTO.Title;
                        blog.ShortDesc = blogDTO.ShortDesc;
                        blog.URL = blogDTO.URL;
                        blog.Description = blogDTO.Description;
                        blog.Date = blogDTO.Date;
                        blog.ImageURL = blogDTO.ImageURL;
                        blog.Type = blogDTO.Type;
                        AddBlog(blog, Conn);

                    }

                }

            }
            catch (Exception ex)
            {

            }
        }

        public partial class RssLinkDTO
        {
            public int Id { get; set; }
            public string RssLink { get; set; }
        }
        public class BlogDTO
        {
            public int Id { get; set; }
            public int CoachId { get; set; }
            public string Description { get; set; }
            public string Title { get; set; }
            public string ImageURL { get; set; }
            public string URL { get; set; }
            public DateTime? Date { get; set; }
            public string Type { get; set; }
            public string ShortDesc { get; set; }
        }

        public class BlogsDTO
        {
            public string Description { get; set; }
            public string Title { get; set; }
            public string ImageURL { get; set; }
            public string URL { get; set; }
            public DateTime? Date { get; set; }
            public string Type { get; set; }
            public string ShortDesc { get; set; }

            private static string GetElementValue(XContainer element, string name)
            {
                if ((element == null) || (element.Element(name) == null))
                    return String.Empty;
                return element.Element(name).Value;
            }

            public BlogsDTO(XContainer post)
            {
                string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
                // Get the string properties from the post's element values
                Title = GetElementValue(post, "title");
                URL = GetElementValue(post, "link");
                ShortDesc = GetElementValue(post, "description");
                Description = GetElementValue(post,
                   "{http://purl.org/rss/1.0/modules/content/}encoded");

                // The Date property is a nullable DateTime? -- if the pubDate element
                // can't be parsed into a valid date, the Date property is set to null
                DateTime result;
                if (DateTime.TryParse(GetElementValue(post, "pubDate"), out result))
                    Date = (DateTime?)result;
                Type = "Blogs";

                MatchCollection matchesImgSrc = Regex.Matches(Description, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
                ImageURL = "default";
                if (matchesImgSrc.Count != 0)
                    ImageURL = matchesImgSrc[0].Groups[1].Value;

                Description = Regex.Replace(Description, regexImgSrc, "", RegexOptions.IgnoreCase);
            }


        }

        internal static int AddBlog(BlogDTO blogDTO,string Conn)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = new SqlConnection(Conn);
                myConn.Open();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddBlog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = blogDTO.CoachId;

                    SqlParameter Title = cmd.Parameters.Add("@Title", SqlDbType.NVarChar, 100);
                    Title.Direction = ParameterDirection.Input;
                    Title.Value = blogDTO.Title;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.NVarChar, 4000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = blogDTO.Description;

                    SqlParameter ImageURL = cmd.Parameters.Add("@ImageURL", SqlDbType.NVarChar, 1000);
                    ImageURL.Direction = ParameterDirection.Input;
                    ImageURL.Value = blogDTO.ImageURL;

                    SqlParameter URL = cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 1000);
                    URL.Direction = ParameterDirection.Input;
                    URL.Value = blogDTO.URL;

                    SqlParameter type = cmd.Parameters.Add("@Type", SqlDbType.VarChar, 50);
                    type.Direction = ParameterDirection.Input;
                    type.Value = blogDTO.Type;

                    SqlParameter shortDesc = cmd.Parameters.Add("@ShortDesc", SqlDbType.NVarChar, 1000);
                    shortDesc.Direction = ParameterDirection.Input;
                    shortDesc.Value = blogDTO.ShortDesc;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("BuzzFeed/AddBlog", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static List<RssLinkDTO> GetLinksBySport(string Conn)
        {
            SqlConnection myConn = new SqlConnection(Conn);
            myConn.Open();

            List<RssLinkDTO> Links = new List<RssLinkDTO>();
            RssLinkDTO Link = new RssLinkDTO();

            SqlCommand cmd = new SqlCommand("[GetRssLinks]", myConn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataReader myData = cmd.ExecuteReader();

            while (myData.Read())
            {
                Link.Id = (int)myData["ID"];
                Link.RssLink = (string)myData["RssLink"]; 
                Links.Add(Link);
            }
            myData.Close();
            myConn.Close();

            return Links;
        }

        internal static int DeleteBlogs(string Conn)
        {

            int BlogCount = 0;
            try
            {
                SqlConnection myConn = new SqlConnection(Conn);
                myConn.Open();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAllBlogs]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BlogCount = (int)myData["DeletedCount"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Buzzfeed/DeleteBlogs", "", ex.Message, "Exception", 0);
            }


            return BlogCount;
        }

    }
}
