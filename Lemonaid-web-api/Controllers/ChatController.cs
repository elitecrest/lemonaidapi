﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Lemonaid_web_api.Models;

namespace Lemonaid_web_api.Controllers
{
    public class ChatController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse Chat(int coachId,int athleteId,string message)
        {

            try
            {
                var badges = "";//UsersDalV4.GetAtheletesBlogsBadges_v4(atheleteId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Chat/Chat", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

    }
}
