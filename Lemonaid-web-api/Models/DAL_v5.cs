﻿using Lemonaid_web_api.Controllers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
 


namespace Lemonaid_web_api.Models
{
    public static class UsersDalV5
    {

        internal static List<string> GetAthleteEmailaddress()
        {

            List<string> emails = new List<string>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteEmailaddress]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        string email = string.Empty;
                        email = (string)myData["emailaddress"].ToString();
                        emails.Add(email);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetAthleteEmailaddress", "", ex.Message, "Exception", 0);
                throw;
            }
            return emails;
        }


        internal static List<string> GetAthleteUsername()
        {

            List<string> emails = new List<string>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteUsername]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        string email = string.Empty;
                        email = (string)myData["username"].ToString();
                        emails.Add(email);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetAthleteUsername", "", ex.Message, "Exception", 0);
                throw;
            }

            return emails;
        }

        internal static int InsertEmails(string emailaddress)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[InsertEmails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/InsertEmails", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int InsertUsernames(string username)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[InsertUsernames]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usernamePar = cmd.Parameters.Add("@username", SqlDbType.VarChar, 100);
                    usernamePar.Direction = ParameterDirection.Input;
                    usernamePar.Value = username;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/InsertUsernames", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static AthleteDTO AddAthlete_v5(AthleteDTO athleteDTO)
        {
            AthleteDTO v = new AthleteDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthlete_v5]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = athleteDTO.Name;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    Gender.Direction = ParameterDirection.Input;
                    Gender.Value = athleteDTO.Gender;

                    SqlParameter School = cmd.Parameters.Add("@School", SqlDbType.VarChar, 100);
                    School.Direction = ParameterDirection.Input;
                    School.Value = (athleteDTO.School == null) ? string.Empty : athleteDTO.School;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = athleteDTO.Emailaddress;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = (athleteDTO.Description == null) ? string.Empty : athleteDTO.Description;

                    SqlParameter GPA = cmd.Parameters.Add("@GPA", SqlDbType.Float);
                    GPA.Direction = ParameterDirection.Input;
                    GPA.Value = athleteDTO.GPA;

                    SqlParameter Social = cmd.Parameters.Add("@Social", SqlDbType.Int);
                    Social.Direction = ParameterDirection.Input;
                    Social.Value = athleteDTO.Social;

                    SqlParameter SAT = cmd.Parameters.Add("@SAT", SqlDbType.Int);
                    SAT.Direction = ParameterDirection.Input;
                    SAT.Value = athleteDTO.SAT;

                    SqlParameter ACT = cmd.Parameters.Add("@ACT", SqlDbType.Int);
                    ACT.Direction = ParameterDirection.Input;
                    ACT.Value = athleteDTO.ACT;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = athleteDTO.InstituteId;

                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = athleteDTO.ProfilePicURL;

                    SqlParameter DOB = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    DOB.Direction = ParameterDirection.Input;
                    DOB.Value = athleteDTO.DOB;

                    SqlParameter Address = cmd.Parameters.Add("@Address", SqlDbType.VarChar, 100);
                    Address.Direction = ParameterDirection.Input;
                    Address.Value = athleteDTO.Address;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = athleteDTO.FacebookId;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = athleteDTO.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = athleteDTO.Longitude;


                    SqlParameter AcademicLevel = cmd.Parameters.Add("@AcademicLevel", SqlDbType.Int);
                    AcademicLevel.Direction = ParameterDirection.Input;
                    AcademicLevel.Value = athleteDTO.AcademicLevel;


                    SqlParameter YearLevel = cmd.Parameters.Add("@YearLevel", SqlDbType.Int);
                    YearLevel.Direction = ParameterDirection.Input;
                    YearLevel.Value = athleteDTO.YearLevel;


                    SqlParameter Premium = cmd.Parameters.Add("@Premium", SqlDbType.Int);
                    Premium.Direction = ParameterDirection.Input;
                    Premium.Value = athleteDTO.Premium;

                    SqlParameter Height1 = cmd.Parameters.Add("@Height1", SqlDbType.VarChar, 20);
                    Height1.Direction = ParameterDirection.Input;
                    Height1.Value = (athleteDTO.Height == null) ? string.Empty : athleteDTO.Height;

                    SqlParameter Height2 = cmd.Parameters.Add("@Height2", SqlDbType.VarChar, 20);
                    Height2.Direction = ParameterDirection.Input;
                    Height2.Value = (athleteDTO.Height2 == null) ? string.Empty : athleteDTO.Height2;

                    SqlParameter HeightType = cmd.Parameters.Add("@HeightType", SqlDbType.VarChar, 20);
                    HeightType.Direction = ParameterDirection.Input;
                    HeightType.Value = (athleteDTO.HeightType == null) ? string.Empty : athleteDTO.HeightType;

                    SqlParameter Weight = cmd.Parameters.Add("@Weight", SqlDbType.VarChar, 20);
                    Weight.Direction = ParameterDirection.Input;
                    Weight.Value = (athleteDTO.Weight == null) ? string.Empty : athleteDTO.Weight;

                    SqlParameter WeightType = cmd.Parameters.Add("@WeightType", SqlDbType.VarChar, 20);
                    WeightType.Direction = ParameterDirection.Input;
                    WeightType.Value = (athleteDTO.WeightType == null) ? string.Empty : athleteDTO.WeightType;

                    SqlParameter WingSpan = cmd.Parameters.Add("@WingSpan", SqlDbType.VarChar, 20);
                    WingSpan.Direction = ParameterDirection.Input;
                    WingSpan.Value = (athleteDTO.WingSpan == null) ? string.Empty : athleteDTO.WingSpan;

                    SqlParameter WingSpanType = cmd.Parameters.Add("@WingSpanType", SqlDbType.VarChar, 20);
                    WingSpanType.Direction = ParameterDirection.Input;
                    WingSpanType.Value = (athleteDTO.WingSpanType == null) ? string.Empty : athleteDTO.WingSpanType;

                    SqlParameter ShoeSize = cmd.Parameters.Add("@ShoeSize", SqlDbType.VarChar, 20);
                    ShoeSize.Direction = ParameterDirection.Input;
                    ShoeSize.Value = (athleteDTO.ShoeSize == null) ? string.Empty : athleteDTO.ShoeSize;

                    SqlParameter ShoeSizeType = cmd.Parameters.Add("@ShoeSizeType", SqlDbType.VarChar, 20);
                    ShoeSizeType.Direction = ParameterDirection.Input;
                    ShoeSizeType.Value = (athleteDTO.ShoeSizeType == null) ? string.Empty : athleteDTO.ShoeSizeType;

                    SqlParameter ClubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    ClubName.Direction = ParameterDirection.Input;
                    ClubName.Value = string.IsNullOrWhiteSpace(athleteDTO.ClubName) ? string.Empty : athleteDTO.ClubName;

                    SqlParameter CoachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    CoachName.Direction = ParameterDirection.Input;
                    CoachName.Value = string.IsNullOrWhiteSpace(athleteDTO.CoachName) ? string.Empty : athleteDTO.CoachName;

                    SqlParameter CoachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    CoachEmailId.Direction = ParameterDirection.Input;
                    CoachEmailId.Value = string.IsNullOrWhiteSpace(athleteDTO.CoachEmailId) ? string.Empty : athleteDTO.CoachEmailId;

                    SqlParameter AtheleteType = cmd.Parameters.Add("@AtheleteType", SqlDbType.Int);
                    AtheleteType.Direction = ParameterDirection.Input;
                    AtheleteType.Value = athleteDTO.AtheleteType;

                    SqlParameter Active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    Active.Direction = ParameterDirection.Input;
                    Active.Value = athleteDTO.Active;

                    SqlParameter Major = cmd.Parameters.Add("@Major", SqlDbType.VarChar, 50);
                    Major.Direction = ParameterDirection.Input;
                    Major.Value = string.IsNullOrWhiteSpace(athleteDTO.Major) ? string.Empty : athleteDTO.Major;

                    SqlParameter Phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    Phonenumber.Direction = ParameterDirection.Input;
                    Phonenumber.Value = string.IsNullOrWhiteSpace(athleteDTO.Phonenumber) ? string.Empty : athleteDTO.Phonenumber;

                    SqlParameter Username = cmd.Parameters.Add("@Username", SqlDbType.VarChar, 500);
                    Username.Direction = ParameterDirection.Input;
                    Username.Value = string.IsNullOrWhiteSpace(athleteDTO.UserName) ? string.Empty : athleteDTO.UserName;

                    SqlParameter LoginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    LoginType.Direction = ParameterDirection.Input;
                    LoginType.Value = (athleteDTO.LoginType == null) ? 0 : athleteDTO.LoginType;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = (athleteDTO.DeviceType == null) ? string.Empty : athleteDTO.DeviceType;

                    SqlParameter DeviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 50);
                    DeviceId.Direction = ParameterDirection.Input;
                    DeviceId.Value = (athleteDTO.DeviceId == null) ? string.Empty : athleteDTO.DeviceId;

                    SqlParameter VerticalJump = cmd.Parameters.Add("@VerticalJump", SqlDbType.VarChar, 20);
                    VerticalJump.Direction = ParameterDirection.Input;
                    VerticalJump.Value = (athleteDTO.VerticalJump == null) ? string.Empty : athleteDTO.VerticalJump;

                    SqlParameter BroadJump = cmd.Parameters.Add("@BroadJump", SqlDbType.VarChar, 20);
                    BroadJump.Direction = ParameterDirection.Input;
                    BroadJump.Value = (athleteDTO.BroadJump == null) ? string.Empty : athleteDTO.BroadJump;

                    SqlParameter StateId = cmd.Parameters.Add("@StateId", SqlDbType.Int);
                    StateId.Direction = ParameterDirection.Input;
                    StateId.Value = athleteDTO.StateId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = 1;
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivAddress"]);
                        v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivEmailAddress"]);
                        v.UniversityName = (myData["UniversityName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UniversityName"]);
                        v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnviPhoneNumber"]);
                        v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivProfilePic"]);
                        v.UniversitySize = (myData["UnivSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivSize"]);
                        v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClassificationName"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value) ? string.Empty : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/AddAthlete_v5", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }

        internal static AthleteDTO GetAthleteProfile_v5(string emailaddress, int type)
        {
            AthleteDTO v = new AthleteDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteProfile_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter typePar = cmd.Parameters.Add("@type", SqlDbType.Int);
                    typePar.Direction = ParameterDirection.Input;
                    typePar.Value = type;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = 1;// (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivAddress"]);
                        v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivEmailAddress"]);
                        v.UniversityName = (myData["UniversityName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UniversityName"]);
                        v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnviPhoneNumber"]);
                        v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivProfilePic"]);
                        v.UniversitySize = (myData["UnivSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivSize"]);
                        v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClassificationName"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value) ? string.Empty : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetAthleteProfile_v5", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }



        internal static List<AthleteDTO> GetAthleteLatLongValues()
        {

            List<AthleteDTO> athleteDTO = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteLatLongValues]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO athlete = new AthleteDTO();
                        athlete.Id = (int)myData["id"];
                        athlete.Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? string.Empty : (string)myData["Emailaddress"];
                        athlete.Latitude = (myData["Latitude"] == DBNull.Value) ? string.Empty : (string)myData["Latitude"];
                        athlete.Longitude = (myData["Longitude"] == DBNull.Value) ? string.Empty : (string)myData["Longitude"];
                        athleteDTO.Add(athlete);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetAthleteLatLongValues", "", ex.Message, "Exception", 0);
                throw;
            }

            return athleteDTO;
        }


        internal static void UpdateStateToAthlete(string state, int Id)
        {

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateStateToAthlete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter statePar = cmd.Parameters.Add("@state", SqlDbType.VarChar, 20);
                    statePar.Direction = ParameterDirection.Input;
                    statePar.Value = state;

                    SqlParameter IdPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    IdPar.Direction = ParameterDirection.Input;
                    IdPar.Value = Id;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/UpdateStateToAthlete", "", ex.Message, "Exception", 0);
                throw;
            }

        }

        internal static int AddVideosThumbnails(AthleteThumbnailDto athleteThumbnailDto)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVideosThumbnails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter VideoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    VideoId.Direction = ParameterDirection.Input;
                    VideoId.Value = athleteThumbnailDto.VideoId; 
                   
                    SqlParameter Athleteid = cmd.Parameters.Add("@Athleteid", SqlDbType.Int);
                    Athleteid.Direction = ParameterDirection.Input;
                    Athleteid.Value = athleteThumbnailDto.AthleteId;

                    SqlParameter ThumbnailURL = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1000);
                    ThumbnailURL.Direction = ParameterDirection.Input;
                    ThumbnailURL.Value = athleteThumbnailDto.ThumbnailURL;

                    SqlParameter IdPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    IdPar.Direction = ParameterDirection.Output;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/AddVideosThumbnails", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;

        }


        internal static List<AthleteDTO> GetAthletesFromState(string state, int offset, int max, string searchText, int familyFriendId)
        {

            List<AthleteDTO> athleteDTO = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthletesFromState]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter StatePar = cmd.Parameters.Add("@State", SqlDbType.VarChar, 100);
                    StatePar.Direction = ParameterDirection.Input;
                    StatePar.Value = state;

                    SqlParameter OffsetPar = cmd.Parameters.Add("@Offset", SqlDbType.Int);
                    OffsetPar.Direction = ParameterDirection.Input;
                    OffsetPar.Value = offset; 

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;

                    SqlParameter searchtextPar = cmd.Parameters.Add("@searchtext", SqlDbType.VarChar, 100);
                    searchtextPar.Direction = ParameterDirection.Input;
                    searchtextPar.Value = searchText;

                    SqlParameter familyFriendIdPar = cmd.Parameters.Add("@familyFriendId", SqlDbType.Int);
                    familyFriendIdPar.Direction = ParameterDirection.Input;
                    familyFriendIdPar.Value = familyFriendId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = 1;// (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivAddress"]);
                        v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivEmailAddress"]);
                        v.UniversityName = (myData["UniversityName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UniversityName"]);
                        v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnviPhoneNumber"]);
                        v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivProfilePic"]);
                        v.UniversitySize = (myData["UnivSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivSize"]);
                        v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClassificationName"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value) ? string.Empty : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        //GetBadges
                        BadgesDTO badges = UsersDalV4.GetAtheleteBadges_v4(v.Id);
                        v.AthleteBadges = badges;
                        athleteDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetAthletesFromState", "", ex.Message, "Exception", 0);
                throw;
            }

            return athleteDTO;
        }

        internal static FamilyFriendsDTO AddfamilyFriends(FamilyFriendsDTO familyFriendsDTO)
        {
            FamilyFriendsDTO v = new FamilyFriendsDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddFamilyFriends]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = familyFriendsDTO.Name;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    Gender.Direction = ParameterDirection.Input;
                    Gender.Value = familyFriendsDTO.Gender;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = familyFriendsDTO.Emailaddress;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = (familyFriendsDTO.Description == null) ? string.Empty : familyFriendsDTO.Description;

                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = familyFriendsDTO.ProfilePicURL;

                    SqlParameter DOB = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    DOB.Direction = ParameterDirection.Input;
                    DOB.Value = familyFriendsDTO.DOB;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = familyFriendsDTO.FacebookId;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = familyFriendsDTO.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = familyFriendsDTO.Longitude;

                    SqlParameter Phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    Phonenumber.Direction = ParameterDirection.Input;
                    Phonenumber.Value = familyFriendsDTO.Phonenumber;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = (familyFriendsDTO.DeviceType == null) ? string.Empty : familyFriendsDTO.DeviceType;

                    SqlParameter DeviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 50);
                    DeviceId.Direction = ParameterDirection.Input;
                    DeviceId.Value = (familyFriendsDTO.DeviceId == null) ? string.Empty : familyFriendsDTO.DeviceId;

                    SqlParameter StateId = cmd.Parameters.Add("@StateId", SqlDbType.Int);
                    StateId.Direction = ParameterDirection.Input;
                    StateId.Value = familyFriendsDTO.StateId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.Phonenumber = (string)myData["Phonenumber"].ToString();
                        v.DeviceId = (string)myData["DeviceId"].ToString();
                        v.Phonenumber = (string)myData["Phonenumber"].ToString();
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 :  Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/AddfamilyFriends", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }

        internal static FamilyFriendsDTO GetFamilyFriendsProfile(string emailaddress)
        {
            FamilyFriendsDTO v = new FamilyFriendsDTO();
            try
            {
              
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFamilyFriendsProfile]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.Phonenumber = (string)myData["Phonenumber"].ToString();
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        v.Sport = (myData["Sport"] == DBNull.Value) ? string.Empty : (string)myData["Sport"];
                        v.Almamater = (myData["Almamater"] == DBNull.Value) ? string.Empty : (string)myData["Almamater"];
                        //v.DeviceId = (myData["DeviceId"] == DBNull.Value) ? string.Empty : (string)myData["DeviceId"];
                        //v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : (string)myData["Username"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetFamilyFriendsProfile", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static int AddFamilyFriendsAthleteMatch(FamilyFriendsMatchDTO familyFriendsMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddFamilyFriendsAthleteMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FamilyFriendsId = cmd.Parameters.Add("@FamilyFriendsId", SqlDbType.Int);
                    FamilyFriendsId.Direction = ParameterDirection.Input;
                    FamilyFriendsId.Value = familyFriendsMatchDTO.FamilyFriendsId;

                    SqlParameter AthleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteId.Direction = ParameterDirection.Input;
                    AthleteId.Value = familyFriendsMatchDTO.AthleteId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = familyFriendsMatchDTO.Status;  

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output; 

                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value; 

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/AddFamilyFriendsAthleteMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int DeleteFamilyFriendsAthleteMatch(FamilyFriendsMatchDTO familyFriendsMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteFamilyFriendsAthleteMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FamilyFriendsId = cmd.Parameters.Add("@FamilyFriendsId", SqlDbType.Int);
                    FamilyFriendsId.Direction = ParameterDirection.Input;
                    FamilyFriendsId.Value = familyFriendsMatchDTO.FamilyFriendsId;

                    SqlParameter AthleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteId.Direction = ParameterDirection.Input;
                    AthleteId.Value = familyFriendsMatchDTO.AthleteId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output; 

                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;
                    
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/DeleteFamilyFriendsAthleteMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int AddFamilyFriendsInstituteMatches(FamilyFriendsMatchDTO familyFriendsMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddFamilyFriendsInstituteMatches]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FamilyFriendsId = cmd.Parameters.Add("@FamilyFriendsId", SqlDbType.Int);
                    FamilyFriendsId.Direction = ParameterDirection.Input;
                    FamilyFriendsId.Value = familyFriendsMatchDTO.FamilyFriendsId;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = familyFriendsMatchDTO.InstituteId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = familyFriendsMatchDTO.Status;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/AddFamilyFriendsInstituteMatches", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int DeleteFamilyFriendsInstituteMatches(FamilyFriendsMatchDTO familyFriendsMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteFamilyFriendsInstituteMatches]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FamilyFriendsId = cmd.Parameters.Add("@FamilyFriendsId", SqlDbType.Int);
                    FamilyFriendsId.Direction = ParameterDirection.Input;
                    FamilyFriendsId.Value = familyFriendsMatchDTO.FamilyFriendsId;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = familyFriendsMatchDTO.InstituteId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/DeleteFamilyFriendsInstituteMatches", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<AthleteDTO> GetFamilyFriendsAtheleteFavourites(int familyFriendId, int offset, int max)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFamilyFriendsAtheleteFavourites]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter familyFriendIdPar = cmd.Parameters.Add("@familyFriendId", SqlDbType.Int);
                    familyFriendIdPar.Direction = ParameterDirection.Input;
                    familyFriendIdPar.Value = familyFriendId;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = 1;// (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivAddress"]);
                        v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivEmailAddress"]);
                        v.UniversityName = (myData["UniversityName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UniversityName"]);
                        v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnviPhoneNumber"]);
                        v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivProfilePic"]);
                        v.UniversitySize = (myData["UnivSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivSize"]);
                        v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClassificationName"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value) ? string.Empty : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetFamilyFriendsAthleteFavourites", "", ex.Message, "Exception", 0);
                throw;
            }


            return athletes;
        }

        internal static List<CoachDTO> GetFamilyFriendsInstituteFavourites(int familyFriendId, int offset, int max)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFamilyFriendsInstituteFavourites]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter familyFriendIdPar = cmd.Parameters.Add("@familyFriendId", SqlDbType.Int);
                    familyFriendIdPar.Direction = ParameterDirection.Input;
                    familyFriendIdPar.Value = familyFriendId;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;
                        
                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["Emailaddress"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        Coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetFamilyFriendsInstituteFavourites", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }


        internal static List<CoachDTO> GetInstitutesForFamilyFriends(int offset, int max, string searchText, int familyFriendId)
        {

            List<CoachDTO> coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstitutesForFamilyFriends]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure; 

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxParameter = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxParameter.Direction = ParameterDirection.Input;
                    maxParameter.Value = max;

                    SqlParameter searchTextPar = cmd.Parameters.Add("@searchText", SqlDbType.VarChar, 100);
                    searchTextPar.Direction = ParameterDirection.Input;
                    searchTextPar.Value = searchText;

                    SqlParameter familyFriendIdPar = cmd.Parameters.Add("@familyFriendId", SqlDbType.Int);
                    familyFriendIdPar.Direction = ParameterDirection.Input;
                    familyFriendIdPar.Value = familyFriendId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.AreasInterested = myData["AreasInterested"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Emailaddress = myData["Emailaddress"].ToString(); 
                        v.Description = myData["Description"].ToString();
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = myData["ClassificationName"].ToString();
                        v.State = myData["State"].ToString();
                        v.City = myData["City"].ToString();
                        v.Zip = myData["Zip"].ToString();
                        v.WebsiteURL = myData["WebsiteURL"].ToString();
                        v.FaidURL = myData["FaidURL"].ToString();
                        v.ApplURL = myData["ApplURL"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.EnrollmentNo = myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = myData["ApplicationNo"].ToString();
                        v.AdmissionNo = myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.FacebookId = "0"; 
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AthleticConference = (myData["AthleticConference"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleticConference"]);
                        v.EnrollmentFee = (myData["EnrollmentFee"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentFee"]);
                        v.Type = (myData["Type"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Type"]);
                        v.TuitionFee = (myData["TuitionFee"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionFee"]);
                        v.Scholarship = (myData["Scholarship"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Scholarship"]);
                        v.EnrollmentNo = (myData["EnrollmentNo"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentNo"]);
                        v.ScholarshipMen = (myData["ScholarshipMen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipMen"]);
                        v.ScholarshipWomen = (myData["ScholarshipWomen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipWomen"]);
                        v.TuitionInState = (myData["TuitionInState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionInState"]);
                        v.TuitionOutState = (myData["TuitionOutState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionOutState"]);
                        v.AdType = 2;
                        v.AtheleteCoachType = 1; //Added on 16Mar
                        v.VirtualVideoURL = (myData["VirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualVideoURL"]);
                        v.VirtualThumbnailURL = (myData["VirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualThumbnailURL"]);
                        v.MatchesVirtualVideoURL = (myData["MatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualVideoURL"]);
                        v.MatchesVirtualThumbnailURL = (myData["MatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualThumbnailURL"]);

                        coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetInstitutesForFamilyFriends", "", ex.Message, "Exception", 0);
            }

            return coaches;
        }


        internal static int RecommendAthletesToUniversitiesByFFF(int athleteId, int familyFriendsId, int instituteId)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[RecommendAthletesToUniversitiesByFFF]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AtheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    AtheleteIdPar.Direction = ParameterDirection.Input;
                    AtheleteIdPar.Value = athleteId;

                    SqlParameter familyFriendsIdPar = cmd.Parameters.Add("@familyFriendsId", SqlDbType.Int);
                    familyFriendsIdPar.Direction = ParameterDirection.Input;
                    familyFriendsIdPar.Value = familyFriendsId;

                    SqlParameter InstituteIdPar = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteIdPar.Direction = ParameterDirection.Input;
                    InstituteIdPar.Value = instituteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["Id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/RecommendAthletesToUniversitiesByFFF", "", ex.Message, "Exception", 0);
                throw;
            }
            return id;
        }


        internal static List<CoachDTO> GetAllCoachesPagingWithSearch_v5(int atheleteId, int offset, int max, int NCAA, int Conference, int Admission, string Description, string SearchText)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllInstitutesByAtheleteEmail_v5]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteidPar = cmd.Parameters.Add("@atheleteid", SqlDbType.Int);
                    atheleteidPar.Direction = ParameterDirection.Input;
                    atheleteidPar.Value = atheleteId;

                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlParameter ncaa = cmd.Parameters.Add("@ncaa", SqlDbType.Int);
                    ncaa.Direction = ParameterDirection.Input;
                    ncaa.Value = NCAA;

                    SqlParameter conference = cmd.Parameters.Add("@conference", SqlDbType.Int);
                    conference.Direction = ParameterDirection.Input;
                    conference.Value = Conference;

                    SqlParameter admission = cmd.Parameters.Add("@admission", SqlDbType.Int);
                    admission.Direction = ParameterDirection.Input;
                    admission.Value = Admission;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = Description;

                    SqlParameter searchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchText.Direction = ParameterDirection.Input;
                    searchText.Value = SearchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = Convert.ToString(myData["name"]);
                        v.AreasInterested = Convert.ToString(myData["AreasInterested"]);
                        v.Address = Convert.ToString(myData["Address"]);
                        v.Emailaddress = Convert.ToString(myData["CoachEmail"]);
                        v.Description = Convert.ToString(myData["Description"]);
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = Convert.ToString(myData["ClassificationName"]);
                        v.State = Convert.ToString(myData["State"]);
                        v.City = Convert.ToString(myData["City"]);
                        v.Zip = Convert.ToString(myData["Zip"]);
                        v.WebsiteURL = Convert.ToString(myData["WebsiteURL"]);
                        v.FaidURL = Convert.ToString(myData["FaidURL"]);
                        v.ApplURL = Convert.ToString(myData["ApplURL"]);
                        v.Latitude = Convert.ToString(myData["Latitude"]);
                        v.Longitude = Convert.ToString(myData["Longitude"]);
                        v.EnrollmentNo = Convert.ToString(myData["EnrollmentNo"]);
                        v.ApplicationNo = Convert.ToString(myData["ApplicationNo"]);
                        v.AdmissionNo = Convert.ToString(myData["AdmissionNo"]);
                        v.ProfilePicURL = Convert.ToString(myData["ProfilePicURL"]);  
                        v.FacebookId = Convert.ToString(myData["CoachFacebook"]);
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = Convert.ToString(myData["CoverPicURL"]);
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AdType = 2;
                        v.Recommended = ((int)myData["recommended"] == 0) ? false : true;
                        v.RightSwiped = ((int)myData["RightSwiped"] == 0) ? false : true; 
                        if (v.RightSwiped)
                        {
                            var payment = GetPayment(atheleteId, 1);
                            if (payment.UserID > 0)
                            {
                                //if (Convert.ToDateTime(payment.ExpiryDate) < DateTime.Now)
                                //{
                                v.RightSwiped = true;
                                // }
                            }
                            else
                            {
                                v.RightSwiped = false;
                            }
                        }
                        v.RecommendedByFFF = ((int)myData["recommendedbyfff"] == 0) ? false : true;
                        v.AthleticConference = (myData["AthleticConference"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleticConference"]);
                        v.EnrollmentFee = (myData["EnrollmentFee"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentFee"]);
                        v.Type = (myData["Type"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Type"]);
                        v.TuitionFee = (myData["TuitionFee"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionFee"]);
                        v.Scholarship = (myData["Scholarship"] == DBNull.Value) ? string.Empty :Convert.ToString(myData["Scholarship"]);
                        v.EnrollmentNo = (myData["EnrollmentNo"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentNo"]);
                        v.ScholarshipMen = (myData["ScholarshipMen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipMen"]);
                        v.ScholarshipWomen = (myData["ScholarshipWomen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipWomen"]);
                        v.TuitionInState = (myData["TuitionInState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionInState"]);
                        v.TuitionOutState = (myData["TuitionOutState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionOutState"]);
                        v.VirtualVideoURL = (myData["VirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualVideoURL"]);
                        v.VirtualThumbnailURL = (myData["VirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualThumbnailURL"]);
                        v.MatchesVirtualVideoURL = (myData["MatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualVideoURL"]);                      
                        v.MatchesVirtualThumbnailURL = (myData["MatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualThumbnailURL"]);
                        Coaches.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetAllCoachesPagingWithSearch_v5", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }

        internal static List<AthleteDTO> GetRecommendAthletesinInstitutesbyfff(int UniversityId, int FamilyFriendId)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetRecommendAthletesinInstitutesbyfff]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter UniversityIdPar = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    UniversityIdPar.Direction = ParameterDirection.Input;
                    UniversityIdPar.Value = UniversityId;

                    SqlParameter FamilyFriendIdPar = cmd.Parameters.Add("@FamilyFriendId", SqlDbType.Int);
                    FamilyFriendIdPar.Direction = ParameterDirection.Input;
                    FamilyFriendIdPar.Value = FamilyFriendId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetRecommendAthletesinInstitutesbyfff", "", ex.Message, "Exception", 0);
            }


            return athletes;
        }



        internal static int DeleteFamilyfriendInstitutes(int familyFriendId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteFamilyfriendInstitutes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter familyFriendIdPar = cmd.Parameters.Add("@familyFriendId", SqlDbType.Int);
                    familyFriendIdPar.Direction = ParameterDirection.Input;
                    familyFriendIdPar.Value = familyFriendId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/DeleteFamilyfriendInstitutes", "", ex.Message, "Exception", 0);
                throw;
            }

            return i;
        }



        internal static int DeleteFamilyfriendAthletes(int familyFriendId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteFamilyfriendAthletes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter familyFriendIdPar = cmd.Parameters.Add("@familyFriendId", SqlDbType.Int);
                    familyFriendIdPar.Direction = ParameterDirection.Input;
                    familyFriendIdPar.Value = familyFriendId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/DeleteFamilyfriendAthletes", "", ex.Message, "Exception", 0);
                throw;
            }

            return i;
        }

        internal static void UpdatePaymentExpiryDate(string UserID, DateTime ExpiryDate)
        {
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdatePaymentExpiryDate]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter UserIDPar = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserIDPar.Direction = ParameterDirection.Input;
                    UserIDPar.Value = UserID; 

                    SqlParameter expiryDate = cmd.Parameters.Add("@ExpiryDate", System.Data.SqlDbType.DateTime);
                    expiryDate.Direction = System.Data.ParameterDirection.Input;
                    expiryDate.Value = ExpiryDate;


                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/UpdatePaymentExpiryDate", "", ex.Message, "Exception", 0);
                throw;
            }
        }

         internal static int AddPayment(PaymentDTO payment)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();  

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddPayment_v5]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = payment.UserID;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.Int);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = payment.UserType;

                    SqlParameter PaymentToken = cmd.Parameters.Add("@PaymentToken", SqlDbType.VarChar, 8000);
                    PaymentToken.Direction = ParameterDirection.Input;
                    PaymentToken.Value = payment.PaymentToken;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = payment.DeviceType;

                    SqlParameter PaymentType = cmd.Parameters.Add("@PaymentType", SqlDbType.VarChar, 50);
                    PaymentType.Direction = ParameterDirection.Input;
                    PaymentType.Value = payment.PaymentType;
  
                    SqlParameter subscriptionType = cmd.Parameters.Add("@SubscriptionType", System.Data.SqlDbType.VarChar, 20);
                    subscriptionType.Direction = System.Data.ParameterDirection.Input;
                    subscriptionType.Value = (payment.SubscriptionType == null) ? string.Empty : payment.SubscriptionType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/AddPayment", "", ex.Message, "Exception", 0);
                throw;
            }
            return id;
        }


         internal static PaymentDTO GetPayment(int UserID, int UserType)
         {
             PaymentDTO payment = new PaymentDTO();

             try
             {
                 SqlConnection myConn = UsersDal.ConnectToDb();

                 if (null != myConn)
                 {
                     SqlCommand cmd = new SqlCommand("[GetPayment_v5]", myConn);
                     cmd.CommandType = CommandType.StoredProcedure;

                     SqlParameter userID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                     userID.Direction = ParameterDirection.Input;
                     userID.Value = UserID;

                     SqlParameter userType = cmd.Parameters.Add("@UserType", SqlDbType.Int);
                     userType.Direction = ParameterDirection.Input;
                     userType.Value = UserType;

                     SqlDataReader myData = cmd.ExecuteReader();

                     while (myData.Read())
                     {
                         payment.UserID = (int)myData["UserID"];
                         payment.UserType = (int)myData["UserType"];
                         payment.PaymentToken = (string)myData["PaymentToken"];
                         payment.DeviceType = (string)myData["DeviceType"];
                         payment.PaymentType = (string)myData["PaymentType"];
                         payment.CreatedDate = (DateTime)myData["CreatedDate"];
                         payment.SubscriptionType = (myData["SubscriptionType"] == DBNull.Value) ? string.Empty : (string)myData["SubscriptionType"]; 
                         payment.ExpiryDate = (myData["ExpiryDate"] == DBNull.Value) ? Convert.ToDateTime("01/01/1900") : (DateTime)myData["ExpiryDate"];
                         payment.Link = (string)myData["Link"];
                        payment.SKUId = (myData["SKUId"] == DBNull.Value) ? string.Empty : (string)myData["SKUId"];
                    }
                     myData.Close();
                     myConn.Close();
                 }
             }
             catch (Exception ex)
             {
                 EmailLogModel emaillog = new EmailLogModel();
                 emaillog.AddEmailLog("UsersDalV5/GetPayment", "", ex.Message, "Exception", 0);
                 throw;
             }

             return payment;
         }


         internal static int AddAtheleteQuestionaire(QuestionaireDTO questionaireDto)
         {


             int id = 0;
             try
             {
                 SqlConnection myConn = UsersDal.ConnectToDb();

                 if (null != myConn)
                 {
                     SqlCommand cmd = new SqlCommand("[AddAtheleteQuestionaire_v5]", myConn);
                     cmd.CommandType = CommandType.StoredProcedure;

                     SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                     atheleteId.Direction = ParameterDirection.Input;
                     atheleteId.Value = questionaireDto.AtheleteId;

                     SqlParameter questionId = cmd.Parameters.Add("@QuestionId", SqlDbType.Int);
                     questionId.Direction = ParameterDirection.Input;
                     questionId.Value = questionaireDto.QuestionId;

                     SqlParameter answer = cmd.Parameters.Add("@Answer", SqlDbType.Int);
                     answer.Direction = ParameterDirection.Input;
                     answer.Value = questionaireDto.AnswerId;

                     SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.VarChar,5);
                     UserType.Direction = ParameterDirection.Input;
                     UserType.Value = questionaireDto.UserType;

                     SqlParameter idPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                     idPar.Direction = ParameterDirection.Output;

                     cmd.ExecuteNonQuery();

                     id = (int)cmd.Parameters["@Id"].Value;


                     myConn.Close();
                 }
             }
             catch (Exception ex)
             {
                 EmailLogModel emaillog = new EmailLogModel();
                 emaillog.AddEmailLog("UsersDalV5/AddAthleteQuestionaire", "", ex.Message, "Exception", 0);
                 throw;
             }

             return id;
         }

         internal static List<StatesDTO> GetStates()
         {

             List<StatesDTO> StatesDTO = new List<StatesDTO>();

             try
             {
                 SqlConnection myConn = UsersDal.ConnectToDb();

                 if (null != myConn)
                 {
                     SqlCommand cmd = new SqlCommand("[GetStates]", myConn);
                     cmd.CommandType = CommandType.StoredProcedure;


                     SqlDataReader myData = cmd.ExecuteReader();

                     while (myData.Read())
                     {
                         StatesDTO v = new StatesDTO();
                         v.Id = (int)myData["id"];
                         v.Name = (string)myData["name"].ToString();
                         StatesDTO.Add(v);
                     }
                     myData.Close();
                     myConn.Close();
                 }
             }
             catch (Exception ex)
             {
                 EmailLogModel emaillog = new EmailLogModel();
                 emaillog.AddEmailLog("UsersDalV5/GetStates", "", ex.Message, "Exception", 0);
                 throw;
             }

             return StatesDTO;
         }


         internal static VideoDTO AddVideos_v5(VideoDTO videoDTO)
         {
             VideoDTO v = new VideoDTO();
             int id = 0;
             try
             {
                 SqlConnection myConn = UsersDal.ConnectToDb();

                 if (null != myConn)
                 {
                     SqlCommand cmd = new SqlCommand("[AddVideos_v5]", myConn);
                     cmd.CommandType = CommandType.StoredProcedure;

                     SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                     Name.Direction = ParameterDirection.Input;
                     Name.Value = string.Empty;

                     SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                     VideoURL.Direction = ParameterDirection.Input;
                     VideoURL.Value = videoDTO.VideoURL;

                     SqlParameter ThumbnailURL = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                     ThumbnailURL.Direction = ParameterDirection.Input;
                     ThumbnailURL.Value = videoDTO.ThumbnailURL;

                     SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                     Status.Direction = ParameterDirection.Input;
                     Status.Value = videoDTO.Status;

                     SqlParameter AtheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.VarChar, 100);
                     AtheleteId.Direction = ParameterDirection.Input;
                     AtheleteId.Value = videoDTO.AtheleteId;

                     SqlParameter videoFormat = cmd.Parameters.Add("@VideoFormat", SqlDbType.VarChar, 50);
                     videoFormat.Direction = ParameterDirection.Input;
                     videoFormat.Value = videoDTO.VideoFormat;

                     SqlParameter videoNumber = cmd.Parameters.Add("@videoNumber", SqlDbType.Int);
                     videoNumber.Direction = ParameterDirection.Input;
                     videoNumber.Value = videoDTO.VideoNumber;

                     SqlParameter duration = cmd.Parameters.Add("@duration", SqlDbType.VarChar, 50);
                     duration.Direction = ParameterDirection.Input;
                     duration.Value = videoDTO.Duration;

                     SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                     Id.Direction = ParameterDirection.Output;

                     SqlDataReader myData = cmd.ExecuteReader();

                     while (myData.Read())
                     { 
                         v.Id = (int)myData["id"];
                         v.Name = (string)myData["Name"].ToString();
                         v.ThumbnailURL = (string)myData["ThumbnailURL"].ToString();
                         v.Status = (int)myData["Status"];
                         v.Duration = (int)myData["Duration"];
                         v.AtheleteId = (int)myData["AtheleteId"];
                         v.VideoNumber = (int)myData["VideoNumber"];
                         v.Firstvideonumber = (int)myData["Firstvideonumber"];
                         v.VideoURL = (string)myData["VideoURL"];
                         v.CountAthleteVideos = (int)myData["cnt"];
                     }
                     myData.Close();
                     myConn.Close();

                 }
             }
           catch (Exception ex)
             {
                 EmailLogModel emaillog = new EmailLogModel();
                 emaillog.AddEmailLog("UsersDalV5/AddVideos_v5", "", ex.Message, "Exception", 0);
                 throw;
             } 

             return v;
         }


         internal static List<QuestionaireDTO> GetQuestionaire_v5(int UserId, string UserType)
         {

             List<QuestionaireDTO> questionaire = new List<QuestionaireDTO>();

             try
             {
                 SqlConnection myConn = UsersDal.ConnectToDb();

                 if (null != myConn)
                 {
                     SqlCommand cmd = new SqlCommand("[GetQuestionaire_v5]", myConn);
                     cmd.CommandType = CommandType.StoredProcedure;

                     SqlParameter UserIdPar = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                     UserIdPar.Direction = ParameterDirection.Input;
                     UserIdPar.Value = UserId;

                     SqlParameter UserTypePar = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 5);
                     UserTypePar.Direction = ParameterDirection.Input;
                     UserTypePar.Value = UserType;


                     SqlDataReader myData = cmd.ExecuteReader();

                     while (myData.Read())
                     {
                         QuestionaireDTO v = new QuestionaireDTO();
                         v.QuestionId = (int)myData["id"];
                         v.Question = (string)myData["Question"].ToString(); 
                         questionaire.Add(v);
                     }
                     myData.Close();
                     myConn.Close();
                 }
             }
             catch (Exception ex)
             {
                 EmailLogModel emaillog = new EmailLogModel();
                 emaillog.AddEmailLog("UsersDalV5/GetQuestionaire_v5", "", ex.Message, "Exception", 0);
                 throw;
             }

             return questionaire;
         }


         internal static List<string> GetAtheleteVideosThumbnails(int VideoId)
         {

             List<string> ThumbnailURLs = new List<string>();

             try
             {
                 SqlConnection myConn = UsersDal.ConnectToDb();

                 if (null != myConn)
                 {
                     SqlCommand cmd = new SqlCommand("[GetAtheleteVideosThumbnails]", myConn);
                     cmd.CommandType = CommandType.StoredProcedure;


                     SqlParameter VideoIdPar = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                     VideoIdPar.Direction = ParameterDirection.Input;
                     VideoIdPar.Value = VideoId;

                     SqlDataReader myData = cmd.ExecuteReader();

                     while (myData.Read())
                     {
                         string ThumbnailURL = string.Empty;
                         ThumbnailURL = (string)myData["ThumbnailURL"].ToString();
                         ThumbnailURLs.Add(ThumbnailURL);
                     }
                     myData.Close();
                     myConn.Close();
                 }
             }
             catch (Exception ex)
             {
                 EmailLogModel emaillog = new EmailLogModel();
                 emaillog.AddEmailLog("UsersDalV5/GetAthleteVideosThumbnails", "", ex.Message, "Exception", 0);
                 throw;
             }

             return ThumbnailURLs;
         }

         internal static int DeleteAthleteVideoThumbnails(int videoId)
         {

             int i = 0;

             try
             {
                 SqlConnection myConn = UsersDal.ConnectToDb();

                 if (null != myConn)
                 {
                     SqlCommand cmd = new SqlCommand("[DeleteAthleteVideoThumbnails]", myConn);
                     cmd.CommandType = CommandType.StoredProcedure;

                     SqlParameter familyFriendIdPar = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                     familyFriendIdPar.Direction = ParameterDirection.Input;
                     familyFriendIdPar.Value = videoId;

                     SqlDataReader myData = cmd.ExecuteReader();

                     while (myData.Read())
                     {
                         i = (int)myData["Id"];
                     }
                     myData.Close();
                     myConn.Close();
                 }
             }
             catch (Exception ex)
             {
                 EmailLogModel emaillog = new EmailLogModel();
                 emaillog.AddEmailLog("UsersDalV5/DeleteAthleteVideoThumbnails", "", ex.Message, "Exception", 0);
                 throw;
             }

             return i;
         }


         internal static List<AthleteDTO> GetCoachesFavourites_v5(string emailaddress, int offset, int max)
         {
             AthleteController atc = new AthleteController();
             List<AthleteDTO> athletes = new List<AthleteDTO>();
             DateTime dtAtheleteDate;
             DateTime dtInstituteDate;

             try
             {
                 SqlConnection myConn = UsersDal.ConnectToDb();

                 if (null != myConn)
                 {
                     SqlCommand cmd = new SqlCommand("[GetInstituteFavourites_v5]", myConn);
                     cmd.CommandType = CommandType.StoredProcedure;

                     SqlParameter emailAddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                     emailAddress.Direction = ParameterDirection.Input;
                     emailAddress.Value = emailaddress;

                     SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                     offsetPar.Direction = ParameterDirection.Input;
                     offsetPar.Value = offset;

                     SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                     maxPar.Direction = ParameterDirection.Input;
                     maxPar.Value = max;

                     SqlDataReader myData = cmd.ExecuteReader();

                     while (myData.Read())
                     {

                         AthleteDTO v = new AthleteDTO();
                         v.Id = (int)myData["id"];
                         v.Name = myData["name"].ToString();
                         v.Description = myData["description"].ToString();
                         v.School = myData["School"].ToString();
                         v.Emailaddress = myData["emailaddress"].ToString();
                         v.GPA = Convert.ToDouble(myData["GPA"]);
                         v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                         v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                         v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                         v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                         v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                         v.FacebookId = myData["FacebookId"].ToString();
                         v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                         v.DOB = myData["DOB"].ToString();
                         v.Address = myData["Address"].ToString();
                         v.Latitude = myData["Latitude"].ToString();
                         v.Longitude = myData["Longitude"].ToString();
                         v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                         v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                         v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                         v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                         v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                         v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                         v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                         v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                         v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                         v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                         v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                         dtAtheleteDate = (myData["AtheleteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["AtheleteDate"];
                         dtInstituteDate = (myData["InstituteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InstituteDate"];
                         v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                         v.Liked = ((int)myData["Likes"] != 0);
                         if (v.Liked)
                         { v.FavDate = (dtAtheleteDate > dtInstituteDate) ? dtAtheleteDate : dtInstituteDate; }
                         else
                         { v.FavDate = dtInstituteDate; }
                         v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                         v.AtheleteId = (int)myData["AtheleteId"];
                         v.CoachId = (int)myData["CoachId"];
                         v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                         v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                         v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                         v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                         v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                         v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                         v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                         v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                         v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                         v.UniversityName = myData["UniversityName"].ToString();
                         v.UniversityProfilePic = myData["UniversityProfilePic"].ToString();
                         v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                         v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                         v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                         v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                         v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                         v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                         v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                         v.VerticalJump = (myData["VerticalJump"] == DBNull.Value) ? string.Empty : (string)myData["VerticalJump"];
                         v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                         int measurementcount = atc.GetMeasurementCount(v.Height, v.Height2, v.Weight, v.WingSpan, v.ShoeSize);
                         v.MeasurementCount = measurementcount;
                         v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                         v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];

                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);

                        string email  = string.Empty; 
                        email = (v.LoginType == 1 || v.LoginType == 4) ? v.Emailaddress : v.UserName;
                        v.MultiSportCount = Dal2.GetMultiSportCount(email, v.LoginType, Utility.UserTypes.HighSchooAthlete);
                         v.AP = (myData["AP"] == DBNull.Value) ? 0.0 : Convert.ToDouble(myData["AP"]);
                        v._20YardShuttleRun = (myData["20YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["20YardShuttleRun"];
                        v._60YardShuttleRun = (myData["60YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["60YardShuttleRun"];
                        v.KneelingMedBallToss = (myData["KneelingMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["KneelingMedBallToss"];
                        v.RotationalMedBallToss = (myData["RotationalMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["RotationalMedBallToss"]; 

                        athletes.Add(v);
                     }
                     myData.Close();
                     myConn.Close();
                 }
             }
             catch (Exception ex)
             {
                 EmailLogModel emaillog = new EmailLogModel();
                 emaillog.AddEmailLog("UsersDalV5/GetCoachesFavourites_v5", "", ex.Message, "Exception", 0);
             }


             return athletes;
         }



         //internal static CoachProfileDTO GetCoachProfile(string emailaddress)
         //{
         //    CoachProfileDTO v = new CoachProfileDTO(); 

         //    try
         //    {
         //        SqlConnection myConn =  UsersDal.ConnectToDb();

         //        if (null != myConn)
         //        {
         //            SqlCommand cmd = new SqlCommand("[GetCoachUserExists]", myConn);
         //            cmd.CommandType = CommandType.StoredProcedure;

         //            SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
         //            emailaddressPar.Direction = ParameterDirection.Input;
         //            emailaddressPar.Value = emailaddress;

         //            SqlDataReader myData = cmd.ExecuteReader();

         //            while (myData.Read())
         //            {
         //                v.CoachId = (int)myData["ID"];  
         //                v.Emailaddress = (myData["emailaddress"] == DBNull.Value) ? string.Empty : (string)myData["emailaddress"].ToString();
         //                int Head = (int)myData["HeadCoach"];
         //                v.HeadCoach = (Head == 1) ? true : false; 
         //                v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
         //                v.payment_paid = (myData["Payment_Paid"] == DBNull.Value) ? false : (bool)myData["Payment_Paid"];
         //                v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
         //                v.s
         //            }
         //            myData.Close();
         //            myConn.Close();
         //        }
         //    }
         //    catch (Exception ex)
         //    {

         //        throw;
         //    }

         //    return v;
         //}

         internal static List<CoachDTO> GetRecommendInstitutesinAthletesbyfff(int athleteId, int familyFriendId)
         {
             List<CoachDTO> coaches = new List<CoachDTO>();

             try
             {
                 SqlConnection myConn = UsersDal.ConnectToDb();

                 if (null != myConn)
                 {
                     SqlCommand cmd = new SqlCommand("[GetRecommendInstitutesinAthletesbyfff]", myConn);
                     cmd.CommandType = CommandType.StoredProcedure;


                     SqlParameter AthleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                     AthleteIdPar.Direction = ParameterDirection.Input;
                     AthleteIdPar.Value = athleteId;

                     SqlParameter FamilyFriendIdPar = cmd.Parameters.Add("@FamilyFriendId", SqlDbType.Int);
                     FamilyFriendIdPar.Direction = ParameterDirection.Input;
                     FamilyFriendIdPar.Value = familyFriendId;


                     SqlDataReader myData = cmd.ExecuteReader();

                     while (myData.Read())
                     {
                         CoachDTO v = new CoachDTO();
                         v.Id = (int)myData["id"];
                         v.Name = (string)myData["name"].ToString();
                         v.AreasInterested = (string)myData["AreasInterested"].ToString();
                         v.Address = (string)myData["Address"].ToString();
                         v.Emailaddress = (string)myData["Emailaddress"].ToString();
                         v.Description = (string)myData["Description"];
                         v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                         v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                         v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                         v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                         v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                         v.ClassificationName = (string)myData["ClassificationName"].ToString();
                         v.State = (string)myData["State"].ToString();
                         v.City = (string)myData["City"].ToString();
                         v.Zip = (string)myData["Zip"].ToString();
                         v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                         v.FaidURL = (string)myData["FaidURL"].ToString();
                         v.ApplURL = (string)myData["ApplURL"].ToString();
                         v.Latitude = (string)myData["Latitude"].ToString();
                         v.Longitude = (string)myData["Longitude"].ToString();
                         v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                         v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                         v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                         v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                         v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                         v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                         v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                         v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                         v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                         v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                         v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                         v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                         v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                         v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                         v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                         v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                         v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                         v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                         v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                         v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                         coaches.Add(v);
                     }
                     myData.Close();
                     myConn.Close();
                 }
             }
             catch (Exception ex)
             {
                 EmailLogModel emaillog = new EmailLogModel();
                 emaillog.AddEmailLog("UsersDalV5/GetRecommendInstitutesinAthletesbyfff", "", ex.Message, "Exception", 0);
             }


             return coaches;
         }
    }
}