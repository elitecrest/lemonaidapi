﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Lemonaid_web_api.Models
{
    public class DalMetrics
    {


        private static List<VideoDTO> GetVideosForAthlete(int athleteId)
        {
            List<VideoDTO> atheleteVideos = Dal2.GetAtheleteIdealVideos2_v1();
            List<VideoDTO> uploadedVideos = Dal2.GetAtheleteVideosFromAthleteId(athleteId);
            if (uploadedVideos.Count > 0)
            {
                foreach (var av in atheleteVideos)
                {
                    foreach (var uv in uploadedVideos)
                    {
                        if (av.VideoNumber == uv.VideoNumber)
                        {
                            av.UploadVideoId = uv.Id;
                            av.UploadDuration = uv.Duration;
                            av.UploadThumbnailUrl = uv.ThumbnailURL;
                            av.UploadVideoUrl = uv.VideoURL;
                        }
                    }
                }
            }
            return atheleteVideos;
        }

        internal static MetricsDto.SwimmingMetricsDto GetSwimmingMetrics(int athleteId)
        {
            MetricsDto.SwimmingMetricsDto metrics = new MetricsDto.SwimmingMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSwimmingMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = Convert.ToInt32(myData["AtheleteId"]);
                        metrics.EventType1 = (myData["EventType1"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["EventType1"]);
                        metrics.EventType2 = (myData["EventType2"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["EventType2"]);
                        metrics.EventType3 = (myData["EventType3"] == DBNull.Value) ? 0 :  Convert.ToInt32(myData["EventType3"]);
                        metrics.EventTypeName1 = Convert.ToString(myData["EventTypeName1"]);
                        metrics.EventTypeName2 = Convert.ToString(myData["EventTypeName2"]);
                        metrics.EventTypeName3 = Convert.ToString(myData["EventTypeName3"]);
                        metrics.BesttimeValue1 = Convert.ToString(myData["Besttime1"]);
                        metrics.BesttimeValue2 = Convert.ToString(myData["Besttime2"]);
                        metrics.BesttimeValue3 = Convert.ToString(myData["Besttime3"]);
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;

                    }
                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static int AddSwimmingMetrics(MetricsDto.SwimmingMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddSwimmingMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter eventType1 = cmd.Parameters.Add("@EventType1", SqlDbType.Int);
                    eventType1.Direction = ParameterDirection.Input;
                    eventType1.Value = metricsDto.EventType1;

                    SqlParameter bestTime1 = cmd.Parameters.Add("@BestTime1", SqlDbType.VarChar, 20);
                    bestTime1.Direction = ParameterDirection.Input;
                    bestTime1.Value = metricsDto.BesttimeValue1;

                    SqlParameter eventType2 = cmd.Parameters.Add("@EventType2", SqlDbType.Int);
                    eventType2.Direction = ParameterDirection.Input;
                    eventType2.Value = metricsDto.EventType2;

                    SqlParameter bestTime2 = cmd.Parameters.Add("@BestTime2", SqlDbType.VarChar, 20);
                    bestTime2.Direction = ParameterDirection.Input;
                    bestTime2.Value = metricsDto.BesttimeValue2;

                    SqlParameter eventType3 = cmd.Parameters.Add("@EventType3", SqlDbType.Int);
                    eventType3.Direction = ParameterDirection.Input;
                    eventType3.Value = metricsDto.EventType3;

                    SqlParameter bestTime3 = cmd.Parameters.Add("@BestTime3", SqlDbType.VarChar, 20);
                    bestTime3.Direction = ParameterDirection.Input;
                    bestTime3.Value = metricsDto.BesttimeValue3;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static MetricsDto.RowingMetricsDto GetRowingMetrics(int athleteId)
        {
            MetricsDto.RowingMetricsDto metrics = new MetricsDto.RowingMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetRowingMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = (myData["AtheleteId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteId"]); 
                        metrics.EventType1 = (myData["EventType1"] == DBNull.Value) ? 0 :  Convert.ToInt32(myData["EventType1"]);
                        metrics.EventType2 = (myData["EventType2"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["EventType2"]);
                        metrics.EventType3 = (myData["EventType3"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["EventType3"]);
                        metrics.EventTypeName1 = Convert.ToString(myData["EventTypeName1"]);
                        metrics.EventTypeName2 = Convert.ToString(myData["EventTypeName2"]);
                        metrics.EventTypeName3 = Convert.ToString(myData["EventTypeName3"]);
                        metrics.BesttimeValue1 = Convert.ToString(myData["Besttime1"]);
                        metrics.BesttimeValue2 = Convert.ToString(myData["Besttime2"]);
                        metrics.BesttimeValue3 = Convert.ToString(myData["Besttime3"]);
                        metrics.Scull = myData["Scull"].ToString();
                        metrics.Sweep = myData["Sweep"].ToString();
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                      
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }

                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static int AddRowingMetrics(MetricsDto.RowingMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddRowingMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter eventType1 = cmd.Parameters.Add("@EventType1", SqlDbType.Int);
                    eventType1.Direction = ParameterDirection.Input;
                    eventType1.Value = metricsDto.EventType1;

                    SqlParameter bestTime1 = cmd.Parameters.Add("@BestTime1", SqlDbType.VarChar, 20);
                    bestTime1.Direction = ParameterDirection.Input;
                    bestTime1.Value = metricsDto.BesttimeValue1;

                    SqlParameter eventType2 = cmd.Parameters.Add("@EventType2", SqlDbType.Int);
                    eventType2.Direction = ParameterDirection.Input;
                    eventType2.Value = metricsDto.EventType2;

                    SqlParameter bestTime2 = cmd.Parameters.Add("@BestTime2", SqlDbType.VarChar, 20);
                    bestTime2.Direction = ParameterDirection.Input;
                    bestTime2.Value = metricsDto.BesttimeValue2;

                    SqlParameter eventType3 = cmd.Parameters.Add("@EventType3", SqlDbType.Int);
                    eventType3.Direction = ParameterDirection.Input;
                    eventType3.Value = metricsDto.EventType3;

                    SqlParameter bestTime3 = cmd.Parameters.Add("@BestTime3", SqlDbType.VarChar, 20);
                    bestTime3.Direction = ParameterDirection.Input;
                    bestTime3.Value = metricsDto.BesttimeValue3;

                    SqlParameter scull = cmd.Parameters.Add("@Scull", SqlDbType.VarChar, 200);
                    scull.Direction = ParameterDirection.Input;
                    scull.Value = metricsDto.Scull;

                    SqlParameter sweep = cmd.Parameters.Add("@Sweep", SqlDbType.VarChar, 200);
                    sweep.Direction = ParameterDirection.Input;
                    sweep.Value = metricsDto.Sweep;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static MetricsDto.DivingMetricsDto GetDivingMetrics(int athleteId)
        {
            MetricsDto.DivingMetricsDto metrics = new MetricsDto.DivingMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetDivingMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = (myData["AthleteId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteId"]);  
                        metrics._1MScore = Convert.ToString(myData["1MScore"]);
                        metrics._3MScore = Convert.ToString(myData["3MScore"]);
                        metrics._10MPlatform = Convert.ToString(myData["10MPlatformScore"]);
                        metrics.DivingExperience = Convert.ToString(myData["DivingExperience"]);
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }

                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static int AddDivingMetrics(MetricsDto.DivingMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddDivingMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter _1MScore = cmd.Parameters.Add("@1MScore", SqlDbType.VarChar, 1000);
                    _1MScore.Direction = ParameterDirection.Input;
                    _1MScore.Value = metricsDto._1MScore;

                    SqlParameter _3MScore = cmd.Parameters.Add("@3MScore", SqlDbType.VarChar, 1000);
                    _3MScore.Direction = ParameterDirection.Input;
                    _3MScore.Value = metricsDto._3MScore;

                    SqlParameter _10MPlatform = cmd.Parameters.Add("@10MPlatformscore", SqlDbType.VarChar, 1000);
                    _10MPlatform.Direction = ParameterDirection.Input;
                    _10MPlatform.Value = metricsDto._10MPlatform;

                    SqlParameter divingExperience = cmd.Parameters.Add("@DivingExperience", SqlDbType.VarChar, 1000);
                    divingExperience.Direction = ParameterDirection.Input;
                    divingExperience.Value = metricsDto.DivingExperience;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static List<AthleteEventDTO> GetAthleteBestimes()
        {
            List<AthleteEventDTO> besttimes = new List<AthleteEventDTO>();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteBestitmes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteEventDTO bt = new AthleteEventDTO();
                        bt.AtheleteId = Convert.ToInt32(myData["atheleteid"]);
                        bt.EventTypeId = Convert.ToInt32(myData["Event_Type_Id"]);
                        bt.BesttimeValue = Convert.ToString(myData["BesttimeValue"]);
                        besttimes.Add(bt);

                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return besttimes;
        }

        internal static MetricsDto.TennisMetricsDto GetTennisMetrics(int athleteId)
        {
            MetricsDto.TennisMetricsDto metrics = new MetricsDto.TennisMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetTennisMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = (myData["AtheleteId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteId"]);  
                        metrics.ITFRank = Convert.ToString(myData["ITFRank"]);
                        metrics.USTARank = Convert.ToString(myData["USTARank"]);
                        metrics.Serve = Convert.ToString(myData["Serve"]);
                        metrics.Forehand = (myData["Forehand"] == DBNull.Value) ? "" : Convert.ToString(myData["Forehand"]);
                        metrics.Backhand = Convert.ToString(myData["Backhand"]);
                        metrics.DominantHand = Convert.ToString(myData["Dominant"]);
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }
                    

                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static int AddTennisMetrics(MetricsDto.TennisMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddTennisMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter ITFRank = cmd.Parameters.Add("@ITFRank", SqlDbType.VarChar, 100);
                    ITFRank.Direction = ParameterDirection.Input;
                    ITFRank.Value = metricsDto.ITFRank;

                    SqlParameter USTARank = cmd.Parameters.Add("@USTARank", SqlDbType.VarChar, 100);
                    USTARank.Direction = ParameterDirection.Input;
                    USTARank.Value = metricsDto.USTARank;

                    SqlParameter Serve = cmd.Parameters.Add("@Serve", SqlDbType.VarChar, 100);
                    Serve.Direction = ParameterDirection.Input;
                    Serve.Value = metricsDto.Serve;

                    SqlParameter Forehand = cmd.Parameters.Add("@Forehand", SqlDbType.VarChar, 100);
                    Forehand.Direction = ParameterDirection.Input;
                    Forehand.Value = metricsDto.Forehand;

                    SqlParameter Backhand = cmd.Parameters.Add("@Backhand", SqlDbType.VarChar, 100);
                    Backhand.Direction = ParameterDirection.Input;
                    Backhand.Value = metricsDto.Backhand;

                    SqlParameter Dominant = cmd.Parameters.Add("@Dominant", SqlDbType.VarChar, 100);
                    Dominant.Direction = ParameterDirection.Input;
                    Dominant.Value = metricsDto.DominantHand;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetTrackMetrics(int athleteId)
        {
            MetricsDto.TrackMetricsDto metrics = new MetricsDto.TrackMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetTrackMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = (myData["AtheleteId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteId"]); 
                        metrics.TrackEventId1 = (myData["TrackEventId1"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["TrackEventId1"]);
                        metrics.TrackEventId2 = (myData["TrackEventId2"] == DBNull.Value) ? 0 :  Convert.ToInt32(myData["TrackEventId2"]);
                        metrics.TrackEventId3 = (myData["TrackEventId3"] == DBNull.Value) ? 0 :  Convert.ToInt32(myData["TrackEventId3"]);
                        metrics.TrackEventId4 = (myData["TrackEventId4"] == DBNull.Value) ? 0 :  Convert.ToInt32(myData["TrackEventId4"]);
                        metrics.TrackEvent1 = Convert.ToString(myData["TrackEvent1"]);
                        metrics.TrackEvent2 = Convert.ToString(myData["TrackEvent2"]);
                        metrics.TrackEvent3 = Convert.ToString(myData["TrackEvent3"]);
                        metrics.TrackEvent4 = Convert.ToString(myData["TrackEvent4"]);
                        metrics.XCId = (myData["XCId"] == DBNull.Value) ? 0 :  Convert.ToInt32(myData["XCId"]);
                        metrics.XC =   Convert.ToString(myData["XC"]);
                        metrics.FieldEventId1 = (myData["FieldEventId1"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["FieldEventId1"]);
                        metrics.FieldEventId2 = (myData["FieldEventId2"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["FieldEventId2"]);
                        metrics.FieldEventId3 = (myData["FieldEventId3"] == DBNull.Value) ? 0 :  Convert.ToInt32(myData["FieldEventId3"]);
                        metrics.FieldEvent1 =   Convert.ToString(myData["FieldEvent1"]);
                        metrics.FieldEvent2 =  Convert.ToString(myData["FieldEvent2"]);
                        metrics.FieldEvent3 = Convert.ToString(myData["FieldEvent3"]);
                        metrics.VO2Max = Convert.ToString(myData["VO2Max"]);
                        metrics.TrackEventName1 = Convert.ToString(myData["TrackEventName1"]);
                        metrics.TrackEventName2 = Convert.ToString(myData["TrackEventName2"]);
                        metrics.TrackEventName3 = Convert.ToString(myData["TrackEventName3"]);
                        metrics.TrackEventName4 = Convert.ToString(myData["TrackEventName4"]);
                        metrics.XCName = Convert.ToString(myData["XCName"]);
                        metrics.FieldEventName1 = Convert.ToString(myData["FieldEventName1"]);
                        metrics.FieldEventName2 = Convert.ToString(myData["FieldEventName2"]);
                        metrics.FieldEventName3 = Convert.ToString(myData["FieldEventName3"]);
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;

                    }
                    myData.Close();

                }
                metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                myConn.Close();

            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static object AddTrackMetrics(MetricsDto.TrackMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddTrackMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter TrackEvent1 = cmd.Parameters.Add("@TrackEvent1", SqlDbType.VarChar, 100);
                    TrackEvent1.Direction = ParameterDirection.Input;
                    TrackEvent1.Value = metricsDto.TrackEvent1;

                    SqlParameter TrackEvent2 = cmd.Parameters.Add("@TrackEvent2", SqlDbType.VarChar, 100);
                    TrackEvent2.Direction = ParameterDirection.Input;
                    TrackEvent2.Value = metricsDto.TrackEvent2;

                    SqlParameter TrackEvent3 = cmd.Parameters.Add("@TrackEvent3", SqlDbType.VarChar, 100);
                    TrackEvent3.Direction = ParameterDirection.Input;
                    TrackEvent3.Value = metricsDto.TrackEvent3;

                    SqlParameter TrackEvent4 = cmd.Parameters.Add("@TrackEvent4", SqlDbType.VarChar, 100);
                    TrackEvent4.Direction = ParameterDirection.Input;
                    TrackEvent4.Value = metricsDto.TrackEvent4;

                    SqlParameter XC = cmd.Parameters.Add("@XC", SqlDbType.VarChar, 100);
                    XC.Direction = ParameterDirection.Input;
                    XC.Value = metricsDto.XC;

                    SqlParameter FieldEvent1 = cmd.Parameters.Add("@FieldEvent1", SqlDbType.VarChar, 100);
                    FieldEvent1.Direction = ParameterDirection.Input;
                    FieldEvent1.Value = metricsDto.FieldEvent1;

                    SqlParameter FieldEvent2 = cmd.Parameters.Add("@FieldEvent2", SqlDbType.VarChar, 100);
                    FieldEvent2.Direction = ParameterDirection.Input;
                    FieldEvent2.Value = metricsDto.FieldEvent2;

                    SqlParameter FieldEvent3 = cmd.Parameters.Add("@FieldEvent3", SqlDbType.VarChar, 100);
                    FieldEvent3.Direction = ParameterDirection.Input;
                    FieldEvent3.Value = metricsDto.FieldEvent3;

                    SqlParameter TrackEventId1 = cmd.Parameters.Add("@TrackEventId1", SqlDbType.Int);
                    TrackEventId1.Direction = ParameterDirection.Input;
                    TrackEventId1.Value = metricsDto.TrackEventId1;

                    SqlParameter TrackEventId2 = cmd.Parameters.Add("@TrackEventId2", SqlDbType.Int);
                    TrackEventId2.Direction = ParameterDirection.Input;
                    TrackEventId2.Value = metricsDto.TrackEventId2;

                    SqlParameter TrackEventId3 = cmd.Parameters.Add("@TrackEventId3", SqlDbType.Int);
                    TrackEventId3.Direction = ParameterDirection.Input;
                    TrackEventId3.Value = metricsDto.TrackEventId3;

                    SqlParameter TrackEventId4 = cmd.Parameters.Add("@TrackEventId4", SqlDbType.Int);
                    TrackEventId4.Direction = ParameterDirection.Input;
                    TrackEventId4.Value = metricsDto.TrackEventId4;

                    SqlParameter XCId = cmd.Parameters.Add("@XCId", SqlDbType.Int);
                    XCId.Direction = ParameterDirection.Input;
                    XCId.Value = metricsDto.XCId;

                    SqlParameter FieldEventId1 = cmd.Parameters.Add("@FieldEventId1", SqlDbType.Int);
                    FieldEventId1.Direction = ParameterDirection.Input;
                    FieldEventId1.Value = metricsDto.FieldEventId1;

                    SqlParameter FieldEventId2 = cmd.Parameters.Add("@FieldEventId2", SqlDbType.Int);
                    FieldEventId2.Direction = ParameterDirection.Input;
                    FieldEventId2.Value = metricsDto.FieldEventId2;

                    SqlParameter FieldEventId3 = cmd.Parameters.Add("FieldEventId3", SqlDbType.Int);
                    FieldEventId3.Direction = ParameterDirection.Input;
                    FieldEventId3.Value = metricsDto.FieldEventId3;

                    SqlParameter VO2Max = cmd.Parameters.Add("@VO2Max", SqlDbType.VarChar, 100);
                    VO2Max.Direction = ParameterDirection.Input;
                    VO2Max.Value = metricsDto.VO2Max;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetLacrosseMetrics(int athleteId)
        {
            MetricsDto.LacrosseMetricsDto metrics = new MetricsDto.LacrosseMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetLacrosseMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = (myData["AtheleteId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteId"]); 
                        metrics.Position = Convert.ToString(myData["Position"]);
                        metrics.DominantHand = Convert.ToString(myData["Dominant"]);
                        metrics.ShotSpeed = Convert.ToString(myData["ShotSpeed"]);
                        metrics._40YardDash = Convert.ToString(myData["40YardDash"]);
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.PositionName = Convert.ToString(myData["PositionName"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }

                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static object AddLacrosseMetrics(MetricsDto.LacrosseMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddLacrosseMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter Position = cmd.Parameters.Add("@Position", SqlDbType.VarChar, 100);
                    Position.Direction = ParameterDirection.Input;
                    Position.Value = metricsDto.Position;

                    SqlParameter Dominant = cmd.Parameters.Add("@Dominant", SqlDbType.VarChar, 100);
                    Dominant.Direction = ParameterDirection.Input;
                    Dominant.Value = metricsDto.DominantHand;

                    SqlParameter ShotSpeed = cmd.Parameters.Add("@ShotSpeed", SqlDbType.VarChar, 100);
                    ShotSpeed.Direction = ParameterDirection.Input;
                    ShotSpeed.Value = metricsDto.ShotSpeed;

                    SqlParameter _40YardDash = cmd.Parameters.Add("@40YardDash", SqlDbType.VarChar, 100);
                    _40YardDash.Direction = ParameterDirection.Input;
                    _40YardDash.Value = metricsDto._40YardDash;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetGolfMetrics(int athleteId)
        {
            MetricsDto.GolfMetricsDto metrics = new MetricsDto.GolfMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetGolfMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = (myData["AtheleteId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteId"]);
                        metrics.AvgDriveDistance = Convert.ToString(myData["AvgDriveDistance"]);
                        metrics.AvgPuttsPerRound = Convert.ToString(myData["AvgPuttsPerRound"]);
                        metrics.USGAHandicap = Convert.ToString(myData["USGAHandicap"]);
                        metrics.PoloGolfRanking = Convert.ToString(myData["PoloGolfRanking"]);
                        metrics.AvgScore = Convert.ToString(myData["AvgScore"]);
                        metrics.BestScore = Convert.ToString(myData["BestScore"]);
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }

                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static object AddGolfMetrics(MetricsDto.GolfMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddGolfMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter AvgDriveDistance = cmd.Parameters.Add("@AvgDriveDistance", SqlDbType.VarChar, 100);
                    AvgDriveDistance.Direction = ParameterDirection.Input;
                    AvgDriveDistance.Value = metricsDto.AvgDriveDistance;

                    SqlParameter AvgPuttsPerRound = cmd.Parameters.Add("@AvgPuttsPerRound", SqlDbType.VarChar, 100);
                    AvgPuttsPerRound.Direction = ParameterDirection.Input;
                    AvgPuttsPerRound.Value = metricsDto.AvgPuttsPerRound;

                    SqlParameter USGAHandicap = cmd.Parameters.Add("@USGAHandicap", SqlDbType.VarChar, 100);
                    USGAHandicap.Direction = ParameterDirection.Input;
                    USGAHandicap.Value = metricsDto.USGAHandicap;

                    SqlParameter PoloGolfRanking = cmd.Parameters.Add("@PoloGolfRanking", SqlDbType.VarChar, 100);
                    PoloGolfRanking.Direction = ParameterDirection.Input;
                    PoloGolfRanking.Value = metricsDto.PoloGolfRanking;

                    SqlParameter AvgScore = cmd.Parameters.Add("@AvgScore", SqlDbType.VarChar, 100);
                    AvgScore.Direction = ParameterDirection.Input;
                    AvgScore.Value = metricsDto.AvgScore;

                    SqlParameter BestScore = cmd.Parameters.Add("@BestScore", SqlDbType.VarChar, 100);
                    BestScore.Direction = ParameterDirection.Input;
                    BestScore.Value = metricsDto.BestScore;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetWaterPoloMetrics(int athleteId)
        {
            MetricsDto.WaterpoloMetricsDto metrics = new MetricsDto.WaterpoloMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetWaterPoloMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = (myData["AtheleteId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteId"]);
                        metrics.PrimaryPosition = Convert.ToString(myData["PrimaryPosition"]);
                        metrics.SecondaryPosition = Convert.ToString(myData["SecondaryPosition"]);
                        metrics.SwimEvent1 = Convert.ToString(myData["SwimEvent1"]);
                        metrics.SwimEvent2 = Convert.ToString(myData["SwimEvent2"]);
                        metrics.SwimEventId1 = Convert.ToInt32(myData["SwimEventId1"]);
                        metrics.SwimEventId2 = Convert.ToInt32(myData["SwimEventId2"]);
                        metrics.DominantHand = Convert.ToString(myData["Dominant"]);
                        metrics.ShotSpeed = Convert.ToString(myData["ShotSpeed"]);
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.SwimEventName1 = Convert.ToString(myData["SwimEventName1"]);
                        metrics.SwimEventName2 = Convert.ToString(myData["SwimEventName2"]);
                        metrics.PrimaryPositionName = Convert.ToString(myData["PrimaryPositionName"]);
                        metrics.SecondaryPositionName = Convert.ToString(myData["SecondaryPositionName"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }

                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static object AddWaterpoloMetrics(MetricsDto.WaterpoloMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddWaterpoloMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter PrimaryPosition = cmd.Parameters.Add("@PrimaryPosition", SqlDbType.VarChar, 100);
                    PrimaryPosition.Direction = ParameterDirection.Input;
                    PrimaryPosition.Value = metricsDto.PrimaryPosition;

                    SqlParameter SecondaryPosition = cmd.Parameters.Add("@SecondaryPosition", SqlDbType.VarChar, 100);
                    SecondaryPosition.Direction = ParameterDirection.Input;
                    SecondaryPosition.Value = metricsDto.SecondaryPosition;

                    SqlParameter SwimEvent1 = cmd.Parameters.Add("@SwimEvent1", SqlDbType.VarChar, 100);
                    SwimEvent1.Direction = ParameterDirection.Input;
                    SwimEvent1.Value = metricsDto.SwimEvent1;

                    SqlParameter SwimEvent2 = cmd.Parameters.Add("@SwimEvent2", SqlDbType.VarChar, 100);
                    SwimEvent2.Direction = ParameterDirection.Input;
                    SwimEvent2.Value = metricsDto.SwimEvent2;

                    SqlParameter SwimEventId1 = cmd.Parameters.Add("@SwimEventId1", SqlDbType.Int);
                    SwimEventId1.Direction = ParameterDirection.Input;
                    SwimEventId1.Value = metricsDto.SwimEventId1;

                    SqlParameter SwimEventId2 = cmd.Parameters.Add("@SwimEventId2", SqlDbType.Int);
                    SwimEventId2.Direction = ParameterDirection.Input;
                    SwimEventId2.Value = metricsDto.SwimEventId2;

                    SqlParameter Dominant = cmd.Parameters.Add("@Dominant", SqlDbType.VarChar, 100);
                    Dominant.Direction = ParameterDirection.Input;
                    Dominant.Value = metricsDto.DominantHand;

                    SqlParameter ShotSpeed = cmd.Parameters.Add("@ShotSpeed", SqlDbType.VarChar, 100);
                    ShotSpeed.Direction = ParameterDirection.Input;
                    ShotSpeed.Value = metricsDto.ShotSpeed;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }


        internal static List<MetricsDto.PositionDTO> GetLacrossePositions()
        {

            List<MetricsDto.PositionDTO> postions = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPosition]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        postions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetLacrossePositions", "", ex.Message, "Exception", 0);
                throw;
            }

            return postions;
        }

        internal static List<MetricsDto.PositionDTO> GetWaterpoloPositions()
        {
            List<MetricsDto.PositionDTO> postions = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPosition]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        postions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetWaterpoloPositions", "", ex.Message, "Exception", 0);
                throw;
            }

            return postions;
        }

        internal static List<MetricsDto.PositionDTO> GetXCEvents()
        {
            List<MetricsDto.PositionDTO> XCEvents = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetXCEvents]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        XCEvents.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetTrackPositions", "", ex.Message, "Exception", 0);
                throw;
            }

            return XCEvents;
        }


        internal static List<MetricsDto.PositionDTO> GetFieldEvents(string filterName)
        {
            List<MetricsDto.PositionDTO> FieldEvents = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFieldEvents]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter filterNamePar = cmd.Parameters.Add("@FilterName", SqlDbType.VarChar, 100);
                    filterNamePar.Direction = ParameterDirection.Input;
                    filterNamePar.Value = filterName;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        FieldEvents.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetFieldEvents", "", ex.Message, "Exception", 0);
                throw;
            }

            return FieldEvents;
        }


        internal static List<MetricsDto.PositionDTO> GetTrackEvents(string filterName)
        {
            List<MetricsDto.PositionDTO> trackEvents = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetTrackEvents]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter filterNamePar = cmd.Parameters.Add("@FilterName", SqlDbType.VarChar, 100);
                    filterNamePar.Direction = ParameterDirection.Input;
                    filterNamePar.Value = filterName;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        trackEvents.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetTrackEvents", "", ex.Message, "Exception", 0);
                throw;
            }

            return trackEvents;
        }


        internal static List<MetricsDto.PositionDTO> GetTRACKDecHeptEvents(string filterName)
        {
            List<MetricsDto.PositionDTO> trackDecHeptEventsDTO = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetTRACKDecHeptEvents]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter filterNamePar = cmd.Parameters.Add("@FilterName", SqlDbType.VarChar, 100);
                    filterNamePar.Direction = ParameterDirection.Input;
                    filterNamePar.Value = filterName;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        trackDecHeptEventsDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetTRACKDecHeptEvents", "", ex.Message, "Exception", 0);
                throw;
            }

            return trackDecHeptEventsDTO;
        }

        internal static object AddBaseballMetrics(MetricsDto.BaseballMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddBaseballMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter PrimaryPosition = cmd.Parameters.Add("@PrimaryPosition", SqlDbType.VarChar, 100);
                    PrimaryPosition.Direction = ParameterDirection.Input;
                    PrimaryPosition.Value = metricsDto.PrimaryPosition;

                    SqlParameter SecondaryPosition = cmd.Parameters.Add("@SecondaryPosition", SqlDbType.VarChar, 100);
                    SecondaryPosition.Direction = ParameterDirection.Input;
                    SecondaryPosition.Value = metricsDto.SecondaryPosition;

                    SqlParameter Throw = cmd.Parameters.Add("@Throw", SqlDbType.VarChar, 100);
                    Throw.Direction = ParameterDirection.Input;
                    Throw.Value = metricsDto.Throw;

                    SqlParameter Hit = cmd.Parameters.Add("@Hit", SqlDbType.VarChar, 100);
                    Hit.Direction = ParameterDirection.Input;
                    Hit.Value = metricsDto.Hit;

                    SqlParameter BattingAverage = cmd.Parameters.Add("@BattingAverage", SqlDbType.VarChar, 100);
                    BattingAverage.Direction = ParameterDirection.Input;
                    BattingAverage.Value = metricsDto.BattingAverage;

                    SqlParameter OBP = cmd.Parameters.Add("@OBP", SqlDbType.VarChar, 100);
                    OBP.Direction = ParameterDirection.Input;
                    OBP.Value = metricsDto.OBP;

                    SqlParameter Slugging = cmd.Parameters.Add("@Slugging", SqlDbType.VarChar, 100);
                    Slugging.Direction = ParameterDirection.Input;
                    Slugging.Value = metricsDto.Slugging;

                    SqlParameter _60yardDash = cmd.Parameters.Add("@60yardDash", SqlDbType.VarChar, 100);
                    _60yardDash.Direction = ParameterDirection.Input;
                    _60yardDash.Value = metricsDto._60YardDash;

                    SqlParameter PitchSpeed = cmd.Parameters.Add("@PitchSpeed", SqlDbType.VarChar, 100);
                    PitchSpeed.Direction = ParameterDirection.Input;
                    PitchSpeed.Value = metricsDto.PitchSpeed;

                    SqlParameter Innings = cmd.Parameters.Add("@Innings", SqlDbType.VarChar, 100);
                    Innings.Direction = ParameterDirection.Input;
                    Innings.Value = metricsDto.Innings;

                    SqlParameter ERA = cmd.Parameters.Add("@ERA", SqlDbType.VarChar, 100);
                    ERA.Direction = ParameterDirection.Input;
                    ERA.Value = metricsDto.ERA;

                    SqlParameter Pitches = cmd.Parameters.Add("@Pitches", SqlDbType.VarChar, 100);
                    Pitches.Direction = ParameterDirection.Input;
                    Pitches.Value = metricsDto.Pitches;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetBaseballMetrics(int athleteId)
        {
            MetricsDto.BaseballMetricsDto metrics = new MetricsDto.BaseballMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBaseballMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = Convert.ToInt32(myData["AtheleteId"]);
                        metrics.PrimaryPosition = Convert.ToString(myData["PrimaryPosition"]);
                        metrics.SecondaryPosition = Convert.ToString(myData["SecondaryPosition"]);
                        metrics.Throw = Convert.ToString(myData["Throw"]);
                        metrics.Hit = Convert.ToString(myData["Hit"]);
                        metrics.BattingAverage = Convert.ToString(myData["BattingAverage"]);
                        metrics.OBP = Convert.ToString(myData["OBP"]);
                        metrics.Slugging = Convert.ToString(myData["Slugging"]);
                        metrics._60YardDash = Convert.ToString(myData["60yardDash"]);
                        metrics.PitchSpeed = Convert.ToString(myData["PitchSpeed"]);
                        metrics.Innings = Convert.ToString(myData["Innings"]);
                        metrics.ERA = Convert.ToString(myData["ERA"]);
                        metrics.Pitches = Convert.ToString(myData["Pitches"]); 
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]); 
                        metrics.PrimaryPositionName = Convert.ToString(myData["PrimaryPositionName"]);
                        metrics.SecondaryPositionName = Convert.ToString(myData["SecondaryPositionName"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }

                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static List<MetricsDto.PositionDTO> GetBaseballPositions()
        {
            List<MetricsDto.PositionDTO> postions = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPosition]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        postions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetBaseballPositions", "", ex.Message, "Exception", 0);
                throw;
            }

            return postions;
        }

        internal static object AddBasketballMetrics(MetricsDto.BasketballMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddBasketballMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter PrimaryPosition = cmd.Parameters.Add("@PrimaryPosition", SqlDbType.VarChar, 100);
                    PrimaryPosition.Direction = ParameterDirection.Input;
                    PrimaryPosition.Value = metricsDto.PrimaryPosition;

                    SqlParameter SecondaryPosition = cmd.Parameters.Add("@SecondaryPosition", SqlDbType.VarChar, 100);
                    SecondaryPosition.Direction = ParameterDirection.Input;
                    SecondaryPosition.Value = metricsDto.SecondaryPosition;

                    SqlParameter ppg = cmd.Parameters.Add("@ppg", SqlDbType.VarChar, 100);
                    ppg.Direction = ParameterDirection.Input;
                    ppg.Value = metricsDto.PPG;

                    SqlParameter rpg = cmd.Parameters.Add("@rpg", SqlDbType.VarChar, 100);
                    rpg.Direction = ParameterDirection.Input;
                    rpg.Value = metricsDto.RPG;

                    SqlParameter apg = cmd.Parameters.Add("@apg", SqlDbType.VarChar, 100);
                    apg.Direction = ParameterDirection.Input;
                    apg.Value = metricsDto.APG; 

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetBasketballMetrics(int athleteId)
        {
            MetricsDto.BasketballMetricsDto metrics = new MetricsDto.BasketballMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBasketballMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = Convert.ToInt32(myData["AtheleteId"]);
                        metrics.PrimaryPosition = Convert.ToString(myData["PrimaryPosition"]);
                        metrics.SecondaryPosition = Convert.ToString(myData["SecondaryPosition"]);
                        metrics.PPG = Convert.ToString(myData["ppg"]);
                        metrics.APG = Convert.ToString(myData["apg"]);
                        metrics.RPG = Convert.ToString(myData["rpg"]); 
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.PrimaryPositionName = Convert.ToString(myData["PrimaryPositionName"]);
                        metrics.SecondaryPositionName = Convert.ToString(myData["SecondaryPositionName"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    } 
                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static List<MetricsDto.PositionDTO> GetBasketballPositions()
        {
            List<MetricsDto.PositionDTO> postions = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPosition]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        postions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetBasketballPositions", "", ex.Message, "Exception", 0);
                throw;
            }

            return postions;
        }

        internal static object AddFootballMetrics(MetricsDto.FootballMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddFootballMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter PrimaryPosition = cmd.Parameters.Add("@PrimaryPosition", SqlDbType.VarChar, 100);
                    PrimaryPosition.Direction = ParameterDirection.Input;
                    PrimaryPosition.Value = metricsDto.PrimaryPosition;

                    SqlParameter SecondaryPosition = cmd.Parameters.Add("@SecondaryPosition", SqlDbType.VarChar, 100);
                    SecondaryPosition.Direction = ParameterDirection.Input;
                    SecondaryPosition.Value = metricsDto.SecondaryPosition;

                    SqlParameter ThirdPosition = cmd.Parameters.Add("@ThirdPosition", SqlDbType.VarChar, 100);
                    ThirdPosition.Direction = ParameterDirection.Input;
                    ThirdPosition.Value = metricsDto.ThirdPosition;

                    SqlParameter Throw = cmd.Parameters.Add("@Throw", SqlDbType.VarChar, 100);
                    Throw.Direction = ParameterDirection.Input;
                    Throw.Value = metricsDto.Throw;

                    SqlParameter _40YardDash = cmd.Parameters.Add("@40YardDash", SqlDbType.VarChar, 100);
                    _40YardDash.Direction = ParameterDirection.Input;
                    _40YardDash.Value = metricsDto._40YardDash;  

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetFootballMetrics(int athleteId)
        {
            MetricsDto.FootballMetricsDto metrics = new MetricsDto.FootballMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFootballMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = Convert.ToInt32(myData["AtheleteId"]);
                        metrics.PrimaryPosition = Convert.ToString(myData["PrimaryPosition"]);
                        metrics.SecondaryPosition = Convert.ToString(myData["SecondaryPosition"]);
                        metrics.ThirdPosition = Convert.ToString(myData["ThirdPosition"]);
                        metrics.Throw = Convert.ToString(myData["Throw"]);
                        metrics._40YardDash = Convert.ToString(myData["40YardDash"]);
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.PrimaryPositionName = Convert.ToString(myData["PrimaryPositionName"]);
                        metrics.SecondaryPositionName = Convert.ToString(myData["SecondaryPositionName"]);
                        metrics.ThirdPositionName = Convert.ToString(myData["ThirdPositionName"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }
                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static List<MetricsDto.PositionDTO> GetFootballPositions()
        {
            List<MetricsDto.PositionDTO> postions = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPosition]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        postions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetFootballPositions", "", ex.Message, "Exception", 0);
                throw;
            }

            return postions;
        }



        internal static object AddSoccerMetrics(MetricsDto.SoccerMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddSoccerMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter PrimaryPosition = cmd.Parameters.Add("@PrimaryPosition", SqlDbType.Int);
                    PrimaryPosition.Direction = ParameterDirection.Input;
                    PrimaryPosition.Value = metricsDto.PrimaryPosition;

                    SqlParameter SecondaryPosition = cmd.Parameters.Add("@SecondaryPosition", SqlDbType.Int);
                    SecondaryPosition.Direction = ParameterDirection.Input;
                    SecondaryPosition.Value = metricsDto.SecondaryPosition;

                    SqlParameter DominantFoot = cmd.Parameters.Add("@DominantFoot", SqlDbType.VarChar, 100);
                    DominantFoot.Direction = ParameterDirection.Input;
                    DominantFoot.Value = metricsDto.DominantFoot;

                    SqlParameter Hit = cmd.Parameters.Add("@40YardDash", SqlDbType.VarChar, 100);
                    Hit.Direction = ParameterDirection.Input;
                    Hit.Value = metricsDto._40YardDash;

                    SqlParameter Mile = cmd.Parameters.Add("@Mile", SqlDbType.VarChar, 100);
                    Mile.Direction = ParameterDirection.Input;
                    Mile.Value = metricsDto.Mile; 

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetSoccerMetrics(int athleteId)
        {
            MetricsDto.SoccerMetricsDto metrics = new MetricsDto.SoccerMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSoccerMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = Convert.ToInt32(myData["AtheleteId"]);
                        metrics.PrimaryPosition = Convert.ToString(myData["PrimaryPosition"]);
                        metrics.SecondaryPosition = Convert.ToString(myData["SecondaryPosition"]);
                        metrics.DominantFoot = Convert.ToString(myData["DominantFoot"]);
                        metrics._40YardDash = Convert.ToString(myData["40YardDash"]);
                        metrics.Mile = Convert.ToString(myData["Mile"]); 
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.PrimaryPositionName = Convert.ToString(myData["PrimaryPositionName"]);
                        metrics.SecondaryPositionName = Convert.ToString(myData["SecondaryPositionName"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }

                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static List<MetricsDto.PositionDTO> GetSoccerPositions()
        {
            List<MetricsDto.PositionDTO> postions = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPosition]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        postions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetSoccerPositions", "", ex.Message, "Exception", 0);
                throw;
            }

            return postions;
        }


        internal static object AddSoftballMetrics(MetricsDto.SoftballMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddSoftballMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter PrimaryPosition = cmd.Parameters.Add("@PrimaryPosition", SqlDbType.Int);
                    PrimaryPosition.Direction = ParameterDirection.Input;
                    PrimaryPosition.Value = metricsDto.PrimaryPosition;

                    SqlParameter SecondaryPosition = cmd.Parameters.Add("@SecondaryPosition", SqlDbType.VarChar, 100);
                    SecondaryPosition.Direction = ParameterDirection.Input;
                    SecondaryPosition.Value = metricsDto.SecondaryPosition;

                    SqlParameter Throw = cmd.Parameters.Add("@Throw", SqlDbType.VarChar, 100);
                    Throw.Direction = ParameterDirection.Input;
                    Throw.Value = metricsDto.Throw;

                    SqlParameter Hit = cmd.Parameters.Add("@Hit", SqlDbType.VarChar, 100);
                    Hit.Direction = ParameterDirection.Input;
                    Hit.Value = metricsDto.Hit;

                    SqlParameter BattingAverage = cmd.Parameters.Add("@BattingAverage", SqlDbType.VarChar, 100);
                    BattingAverage.Direction = ParameterDirection.Input;
                    BattingAverage.Value = metricsDto.BattingAverage;

                    SqlParameter OBP = cmd.Parameters.Add("@OBP", SqlDbType.VarChar, 100);
                    OBP.Direction = ParameterDirection.Input;
                    OBP.Value = metricsDto.OBP;

                    SqlParameter Slugging = cmd.Parameters.Add("@Slugging", SqlDbType.VarChar, 100);
                    Slugging.Direction = ParameterDirection.Input;
                    Slugging.Value = metricsDto.Slugging;

                    SqlParameter _60yardDash = cmd.Parameters.Add("@60yardDash", SqlDbType.VarChar, 100);
                    _60yardDash.Direction = ParameterDirection.Input;
                    _60yardDash.Value = metricsDto._60YardDash;

                    SqlParameter PitchSpeed = cmd.Parameters.Add("@PitchSpeed", SqlDbType.VarChar, 100);
                    PitchSpeed.Direction = ParameterDirection.Input;
                    PitchSpeed.Value = metricsDto.PitchSpeed;

                    SqlParameter Innings = cmd.Parameters.Add("@Innings", SqlDbType.VarChar, 100);
                    Innings.Direction = ParameterDirection.Input;
                    Innings.Value = metricsDto.Innings;

                    SqlParameter ERA = cmd.Parameters.Add("@ERA", SqlDbType.VarChar, 100);
                    ERA.Direction = ParameterDirection.Input;
                    ERA.Value = metricsDto.ERA;

                    SqlParameter Pitches = cmd.Parameters.Add("@Pitches", SqlDbType.VarChar, 100);
                    Pitches.Direction = ParameterDirection.Input;
                    Pitches.Value = metricsDto.Pitches;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetSoftballMetrics(int athleteId)
        {
            MetricsDto.SoftballMetricsDto metrics = new MetricsDto.SoftballMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSoftballMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = Convert.ToInt32(myData["AtheleteId"]);
                        metrics.PrimaryPosition = Convert.ToString(myData["PrimaryPosition"]);
                        metrics.SecondaryPosition = Convert.ToString(myData["SecondaryPosition"]);
                        metrics.Throw = Convert.ToString(myData["Throw"]);
                        metrics.Hit = Convert.ToString(myData["Hit"]);
                        metrics.BattingAverage = Convert.ToString(myData["BattingAverage"]);
                        metrics.OBP = Convert.ToString(myData["OBP"]);
                        metrics.Slugging = Convert.ToString(myData["Slugging"]);
                        metrics._60YardDash = Convert.ToString(myData["60yardDash"]);
                        metrics.PitchSpeed = Convert.ToString(myData["PitchSpeed"]);
                        metrics.Innings = Convert.ToString(myData["Innings"]);
                        metrics.ERA = Convert.ToString(myData["ERA"]);
                        metrics.Pitches = Convert.ToString(myData["Pitches"]);
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.PrimaryPositionName = Convert.ToString(myData["PrimaryPositionName"]);
                        metrics.SecondaryPositionName = Convert.ToString(myData["SecondaryPositionName"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }

                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static List<MetricsDto.PositionDTO> GetSoftballPositions()
        {
            List<MetricsDto.PositionDTO> postions = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPosition]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        postions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetSoftballPositions", "", ex.Message, "Exception", 0);
                throw;
            }

            return postions;
        }

        internal static object AddIceHockeyMetrics(MetricsDto.IceHockeyMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddIceHockeyMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter PrimaryPosition = cmd.Parameters.Add("@PrimaryPosition", SqlDbType.Int);
                    PrimaryPosition.Direction = ParameterDirection.Input;
                    PrimaryPosition.Value = metricsDto.PrimaryPosition;

                    SqlParameter SecondaryPosition = cmd.Parameters.Add("@SecondaryPosition", SqlDbType.Int);
                    SecondaryPosition.Direction = ParameterDirection.Input;
                    SecondaryPosition.Value = metricsDto.SecondaryPosition;

                    SqlParameter DominantHand = cmd.Parameters.Add("@DominantHand", SqlDbType.VarChar, 100);
                    DominantHand.Direction = ParameterDirection.Input;
                    DominantHand.Value = metricsDto.DominantHand;

                    SqlParameter ShotSpeed = cmd.Parameters.Add("@ShotSpeed", SqlDbType.VarChar, 100);
                    ShotSpeed.Direction = ParameterDirection.Input;
                    ShotSpeed.Value = metricsDto.ShotSpeed; 

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetIceHockeyMetrics(int athleteId)
        {
            MetricsDto.IceHockeyMetricsDto metrics = new MetricsDto.IceHockeyMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetIceHockeyMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = Convert.ToInt32(myData["AtheleteId"]);
                        metrics.PrimaryPosition = Convert.ToString(myData["PrimaryPosition"]);
                        metrics.SecondaryPosition = Convert.ToString(myData["SecondaryPosition"]);
                        metrics.DominantHand = Convert.ToString(myData["DominantHand"]);
                        metrics.ShotSpeed = Convert.ToString(myData["ShotSpeed"]); 
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.PrimaryPositionName = Convert.ToString(myData["PrimaryPositionName"]);
                        metrics.SecondaryPositionName = Convert.ToString(myData["SecondaryPositionName"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }

                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static List<MetricsDto.PositionDTO> GetIceHockeyPositions()
        {
            List<MetricsDto.PositionDTO> postions = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPosition]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        postions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetIceHockeyPositions", "", ex.Message, "Exception", 0);
                throw;
            }

            return postions;
        }


        internal static object AddWrestlingMetrics(MetricsDto.WrestlingMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddWrestlingMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter PrimaryClass = cmd.Parameters.Add("@PrimaryClass", SqlDbType.Int);
                    PrimaryClass.Direction = ParameterDirection.Input;
                    PrimaryClass.Value = metricsDto.PrimaryClass;

                    SqlParameter SecondaryClass = cmd.Parameters.Add("@SecondaryClass", SqlDbType.Int);
                    SecondaryClass.Direction = ParameterDirection.Input;
                    SecondaryClass.Value = metricsDto.SecondaryClass;

                    SqlParameter WLRecordinHS = cmd.Parameters.Add("@WLRecordinHS", SqlDbType.VarChar, 100);
                    WLRecordinHS.Direction = ParameterDirection.Input;
                    WLRecordinHS.Value = metricsDto.WLRecordinHS;

                    SqlParameter Folkstyle = cmd.Parameters.Add("@Folkstyle", SqlDbType.VarChar, 100);
                    Folkstyle.Direction = ParameterDirection.Input;
                    Folkstyle.Value = metricsDto.Folkstyle;

                    SqlParameter GrecoRoman = cmd.Parameters.Add("@GrecoRoman", SqlDbType.VarChar, 100);
                    GrecoRoman.Direction = ParameterDirection.Input;
                    GrecoRoman.Value = metricsDto.GrecoRoman;

                    SqlParameter Freestyle = cmd.Parameters.Add("@Freestyle", SqlDbType.VarChar, 100);
                    Freestyle.Direction = ParameterDirection.Input;
                    Freestyle.Value = metricsDto.Freestyle;

                    SqlParameter FutureOlyRankings = cmd.Parameters.Add("@FutureOlyRankings", SqlDbType.VarChar, 100);
                    FutureOlyRankings.Direction = ParameterDirection.Input;
                    FutureOlyRankings.Value = metricsDto.FutureOlyRankings;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetWrestlingMetrics(int athleteId)
        {
            MetricsDto.WrestlingMetricsDto metrics = new MetricsDto.WrestlingMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetWrestlingMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = Convert.ToInt32(myData["AtheleteId"]);
                        metrics.PrimaryClass = Convert.ToString(myData["PrimaryClass"]);
                        metrics.SecondaryClass = Convert.ToString(myData["SecondaryClass"]);
                        metrics.WLRecordinHS = Convert.ToString(myData["WLRecordinHS"]);
                        metrics.Folkstyle = Convert.ToString(myData["Folkstyle"]);
                        metrics.GrecoRoman = Convert.ToString(myData["GrecoRoman"]);
                        metrics.Freestyle = Convert.ToString(myData["Freestyle"]);
                        metrics.FutureOlyRankings = Convert.ToString(myData["FutureOlyRankings"]); 
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.PrimaryClassName = Convert.ToString(myData["PrimaryClassName"]);
                        metrics.SecondaryClassName = Convert.ToString(myData["SecondaryClassName"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    } 
                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static List<MetricsDto.PositionDTO> GetWrestlingWeightClasses()
        {
            List<MetricsDto.PositionDTO> postions = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetWeightClasses]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        postions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetWeightClasses", "", ex.Message, "Exception", 0);
                throw;
            }

            return postions;
        }

        internal static object AddVolleyballMetrics(MetricsDto.VolleyballMetricsDto metricsDto)
        {
            int i = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVolleyballMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = metricsDto.AthleteId;

                    SqlParameter PrimaryPosition = cmd.Parameters.Add("@PrimaryPosition", SqlDbType.Int);
                    PrimaryPosition.Direction = ParameterDirection.Input;
                    PrimaryPosition.Value = metricsDto.PrimaryPosition;

                    SqlParameter SecondaryPosition = cmd.Parameters.Add("@SecondaryPosition", SqlDbType.Int);
                    SecondaryPosition.Direction = ParameterDirection.Input;
                    SecondaryPosition.Value = metricsDto.SecondaryPosition;

                    SqlParameter DominantHand = cmd.Parameters.Add("@DominantHand", SqlDbType.VarChar, 100);
                    DominantHand.Direction = ParameterDirection.Input;
                    DominantHand.Value = metricsDto.DominantHand;

                    SqlParameter ShotSpeed = cmd.Parameters.Add("@ShotSpeed", SqlDbType.VarChar, 100);
                    ShotSpeed.Direction = ParameterDirection.Input;
                    ShotSpeed.Value = metricsDto.ShotSpeed;

                    SqlParameter StandingReach = cmd.Parameters.Add("@StandingReach", SqlDbType.VarChar, 100);
                    StandingReach.Direction = ParameterDirection.Input;
                    StandingReach.Value = metricsDto.StandingReach;

                    SqlParameter JumpBlock = cmd.Parameters.Add("@JumpBlock", SqlDbType.VarChar, 100);
                    JumpBlock.Direction = ParameterDirection.Input;
                    JumpBlock.Value = metricsDto.JumpBlock;

                    SqlParameter ApproachJump = cmd.Parameters.Add("@ApproachJump", SqlDbType.VarChar, 100);
                    ApproachJump.Direction = ParameterDirection.Input;
                    ApproachJump.Value = metricsDto.ApproachJump;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = (metricsDto.ClubCoachInfoDto.ClubName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = (metricsDto.ClubCoachInfoDto.CoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = (metricsDto.ClubCoachInfoDto.CoachEmailId == null) ? string.Empty : metricsDto.ClubCoachInfoDto.CoachEmailId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachName == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (metricsDto.ClubCoachInfoDto.SchoolCoachEmail == null) ? string.Empty : metricsDto.ClubCoachInfoDto.SchoolCoachEmail;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return i;
        }

        internal static object GetVolleyballMetrics(int athleteId)
        {
            MetricsDto.VolleyballMetricsDto metrics = new MetricsDto.VolleyballMetricsDto();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVolleyballMetrics]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        metrics.AthleteId = Convert.ToInt32(myData["AtheleteId"]);
                        metrics.PrimaryPosition = Convert.ToString(myData["PrimaryPosition"]);
                        metrics.SecondaryPosition = Convert.ToString(myData["SecondaryPosition"]);
                        metrics.DominantHand = Convert.ToString(myData["DominantHand"]);
                        metrics.ShotSpeed = Convert.ToString(myData["ShotSpeed"]);
                        metrics.StandingReach = Convert.ToString(myData["StandingReach"]);
                        metrics.JumpBlock = Convert.ToString(myData["JumpBlock"]);
                        metrics.ApproachJump = Convert.ToString(myData["ApproachJump"]);
                        MetricsDto.ClubCoachInfoDto ClubCoachInfo = new MetricsDto.ClubCoachInfoDto();
                        ClubCoachInfo.ClubName = Convert.ToString(myData["ClubName"]);
                        ClubCoachInfo.CoachName = Convert.ToString(myData["CoachName"]);
                        ClubCoachInfo.CoachEmailId = Convert.ToString(myData["CoachEmailId"]);
                        ClubCoachInfo.SchoolCoachName = Convert.ToString(myData["SchoolCoachName"]);
                        ClubCoachInfo.SchoolCoachEmail = Convert.ToString(myData["SchoolCoachEmail"]);
                        metrics.PrimaryPositionName = Convert.ToString(myData["PrimaryPositionName"]);
                        metrics.SecondaryPositionName = Convert.ToString(myData["SecondaryPositionName"]);
                        metrics.ClubCoachInfoDto = ClubCoachInfo;
                    }
                    metrics.AtheleteVideos = GetVideosForAthlete(athleteId);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                throw;
            }

            return metrics;
        }

        internal static List<MetricsDto.PositionDTO> GetVolleyballPositions()
        {
            List<MetricsDto.PositionDTO> postions = new List<MetricsDto.PositionDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPosition]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        MetricsDto.PositionDTO v = new MetricsDto.PositionDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        postions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DalMetrics/GetVolleyballPositions", "", ex.Message, "Exception", 0);
                throw;
            }

            return postions;
        }
    }
}