﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic; 
using System.IO; 
using System.Net.Mail; 
using System.Web;
using System.Web.Http;

namespace Lemonaid_web_api.Controllers
{
    public class ClubCoachController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse ClubCoachProfile(string emailAddress)
        {
            try
            {
                ClubCoachDTO clubCoachDto = UsersDal.GetClubCoachProfile(emailAddress); 
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = clubCoachDto;
                _result.Message = CustomConstants.Details_Get_Successfully;
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/ClubCoachProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        } 

        [HttpPost]
        public Utility.CustomResponse AddClubCoach(ClubCoachDTO clubCoachDto)
        {
            try
            {

                var clubCoachId = UsersDal.AddClubCoach(clubCoachDto);
                if (clubCoachId.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = clubCoachId;
                    _result.Message = CustomConstants.clubCoach_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/AddClubCoach", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddClubCoachMatch(ClubCoachMatchDTO clubCoachmatchDto)
        {
            try
            {

                var matchId = UsersDal.AddClubCoachMatch(clubCoachmatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (clubCoachmatchDto.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/AddClubCoachMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetClubCoachFavourites(int clubCoachId, int offset, int max)
        {
            // NO need of videos in Fav - disussed on 15 Nov
            try
            {
                List<CoachDTO> coaches = UsersDal.GetClubCoachFavourites(clubCoachId, offset, max);
                for (int i = 0; i < coaches.Count; i++)
                {
                    List<AthleteDTO> atheletes = UsersDal.GetRecommendAthletesinInstitutes(coaches[i].Id, clubCoachId);
                    coaches[i].RecommendedAtheletes = atheletes;
                }
                if (coaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Matches;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/GetClubCoachFavourites", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse DeleteClubCoachMatch(ClubCoachMatchDTO clubCoachMatchDto)
        {

            try
            {

                var matchId = UsersDal.DeleteClubCoachMatch(clubCoachMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/DeleteClubCoachMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetAllInstitutesByClubCoach(int clubCoachId, int offset, int max, string searchText)
        {
            try
            {
                List<CoachDTO> institutes = UsersDal.GetAllInstitutesByClubCoach(clubCoachId, offset, max, searchText); 
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = institutes;
                _result.Message = CustomConstants.Details_Get_Successfully;
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/GetAllInstitutesByClubCoach", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetAllInstitutesByClubCoach_v5(int clubCoachId, int offset, int max, string searchText)
        {
            CoachController coachController = new CoachController();

            try
            {
                List<CoachDTO> listWithQuestionaire = new List<CoachDTO>();
                List<QuestionaireDTO> questionaireDto = UsersDalV5.GetQuestionaire_v5(clubCoachId, "CLC");
                listWithQuestionaire = AddQuestionairetoList(questionaireDto, listWithQuestionaire);
                List<CoachDTO> institutes = UsersDal.GetAllInstitutesByClubCoach(clubCoachId, offset, max, searchText);
                if (institutes.Count > 0)
                {
                    listWithQuestionaire = AddinstitutestoList(institutes, listWithQuestionaire);
                }
                if (listWithQuestionaire.Count > 0)
                {
                    listWithQuestionaire = AddCoachVideos(listWithQuestionaire);
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    listWithQuestionaire = coachController.GetAdCoaches(ads, listWithQuestionaire);

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = listWithQuestionaire;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                { 
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/GetAllInstitutesByClubCoach_v5", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        public List<CoachDTO> AddinstitutestoList(List<CoachDTO> newList, List<CoachDTO> listWithQuestionaire)
        {
            for (int i = 0; i < newList.Count; i++)
            {
                CoachDTO coaches = new CoachDTO();
                coaches.Id = newList[i].Id;
                coaches.Name = newList[i].Name;
                coaches.AreasInterested = newList[i].AreasInterested;
                coaches.Address = newList[i].Address;
                coaches.Emailaddress = newList[i].Emailaddress;
                coaches.Description = newList[i].Description;
                coaches.YearOfEstablish = newList[i].YearOfEstablish;
                coaches.NCAA = newList[i].NCAA;
                coaches.Conference = newList[i].Conference;
                coaches.AdmissionStandard = newList[i].AdmissionStandard;
                coaches.AverageRating = newList[i].AverageRating;
                coaches.FacebookId = newList[i].FacebookId;
                coaches.ClassificationName = newList[i].ClassificationName;
                coaches.State = newList[i].State;
                coaches.City = newList[i].City;
                coaches.Zip = newList[i].Zip;
                coaches.WebsiteURL = newList[i].WebsiteURL;
                coaches.FaidURL = newList[i].FaidURL;
                coaches.ApplURL = newList[i].ApplURL;
                coaches.Latitude = newList[i].Latitude;
                coaches.Longitude = newList[i].Longitude;
                coaches.EnrollmentNo = newList[i].EnrollmentNo;
                coaches.ApplicationNo = newList[i].ApplicationNo;
                coaches.AdmissionNo = newList[i].AdmissionNo;
                coaches.ProfilePicURL = newList[i].ProfilePicURL;
                coaches.CoverPicURL = newList[i].CoverPicURL;
                coaches.TotalCount = newList[i].TotalCount;
                coaches.Liked = newList[i].Liked;
                coaches.NoOfAthletesInApp = newList[i].NoOfAthletesInApp;
                coaches.AcceptanceRate = newList[i].AcceptanceRate;
                coaches.NCAARate = newList[i].NCAARate;
                coaches.ConferenceRate = newList[i].ConferenceRate;
                coaches.AdmissionRate = newList[i].AdmissionRate;
                coaches.SocialRate = newList[i].SocialRate;
                coaches.AtheleteRate = newList[i].AtheleteRate;
                coaches.Cost = newList[i].Cost;
                coaches.MensTeam = newList[i].MensTeam;
                coaches.WomenTeam = newList[i].WomenTeam;
                coaches.Size = newList[i].Size;
                coaches.Phonenumber = newList[i].Phonenumber;
                // coaches.AtheleteCoachType = 1;
                coaches.AdType = newList[i].AdType;
                coaches.FavDate = newList[i].FavDate;
                coaches.InvitationDate = newList[i].InvitationDate;
                coaches.Accept = newList[i].Accept;
                coaches.CoachId = newList[i].CoachId;
                coaches.Payment_Paid = newList[i].Payment_Paid;
                coaches.Recommended = newList[i].Recommended;
                coaches.RightSwiped = newList[i].RightSwiped;
                coaches.ChatCount = newList[i].ChatCount;
                coaches.AthleticConference = newList[i].AthleticConference;
                coaches.EnrollmentFee = newList[i].EnrollmentFee;
                coaches.Type = newList[i].Type;
                coaches.TuitionFee = newList[i].TuitionFee;
                coaches.Scholarship = newList[i].Scholarship;
                coaches.EnrollmentNo = newList[i].EnrollmentNo;
                coaches.TuitionInState = newList[i].TuitionInState;
                coaches.TuitionOutState = newList[i].TuitionOutState;
                coaches.ScholarshipMen = newList[i].ScholarshipMen;
                coaches.ScholarshipWomen = newList[i].ScholarshipWomen;
                coaches.VirtualVideoURL = newList[i].VirtualVideoURL;
                coaches.MatchesVirtualVideoURL = newList[i].MatchesVirtualVideoURL;
                coaches.VirtualThumbnailURL = newList[i].VirtualThumbnailURL;
                coaches.MatchesVirtualThumbnailURL = newList[i].MatchesVirtualThumbnailURL;
                listWithQuestionaire.Add(coaches);
            }

            return listWithQuestionaire;
        }

        public List<CoachDTO> AddQuestionairetoList(List<QuestionaireDTO> questionaire, List<CoachDTO> listWithQuestionaire)
        {
            for (int i = 0; i < questionaire.Count; i++)
            {
                CoachDTO coaches = new CoachDTO();
                coaches.ProfilePicURL = string.Empty;
                coaches.Name = string.Empty;
                coaches.QuestionId = questionaire[i].QuestionId;
                coaches.Question = questionaire[i].Question;
                coaches.AdType = 3;
                listWithQuestionaire.Add(coaches);
            }
            return listWithQuestionaire;

        }

        //Added on 24 Sep 2016

        public List<AthleteDTO> GetAdClubCoach(List<AdDTO> ads, List<AthleteDTO> atheletes)
        {
            AthleteDTO athlete = new AthleteDTO();
            for (int i = 0; i < ads.Count; i++)
            {
                athlete.Id = ads[i].Id;
                athlete.Name = ads[i].Name;
                athlete.ProfilePicURL = ads[i].ProfilePicURL;
                athlete.Description = ads[i].Description;
                athlete.CoverPicURL = ads[i].VideoURL;
                athlete.AdType = ads[i].AdType;
                athlete.School = "";
                athlete.Emailaddress = "";
                athlete.GPA = 0.0;
                athlete.Social = 0;
                athlete.SAT = 0;
                athlete.ACT = 0;
                athlete.InstituteId = 0;
                athlete.Gender = 0;
                athlete.FacebookId = "";
                athlete.DOB = "";
                athlete.Address = "";
                athlete.Latitude = "";
                athlete.Longitude = "";
                athlete.TotalCount = 0;
                athlete.FriendsInApp = 0;
                athlete.SharesApp = 0;
                athlete.AverageRating = 0;
                athlete.AcademicLevel = 0;
                athlete.YearLevel = 0;
                athlete.FriendsRate = 0;
                athlete.ShareRate = 0;
                athlete.SocialRate = 0;
                athlete.AcademicRate = 0;
                athlete.AtheleteRate = 0;
                athlete.Class = 0;
                atheletes.Add(athlete);
            }

            return atheletes;
        }


        //Recommend should not get the college athletes
        [HttpGet]
        public Utility.CustomResponse GetAthletesinClubCoachClub(int clubcoachid)
        {
            try
            {
                List<AthleteDTO> atheletes  = UsersDal.GetAthletesinClubCoachClub(clubcoachid);
                List<AthleteDTO> atheletesInSchool = UsersDal.GetAthletesinClubCoachSchool(clubcoachid); 
                 
                atheletes.AddRange(atheletesInSchool);

                for (int i = 0; i < atheletes.Count; i++)
                {
                    List<CoachDTO> institutes = UsersDal.GetRecommendInstitutesForAthletes(atheletes[i].Id, clubcoachid);
                    atheletes[i].RecommendedInstitutes = institutes;
                    atheletes[i].RecommendedTotalCount = atheletes[i].RecommendedInstitutes.Count;
                }
               
                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful; 
                    _result.Message = CustomConstants.No_Records_Roster;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/GetAthletesinClubCoachClub", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        //Team should get the college athletes
        [HttpGet]
        public Utility.CustomResponse GetAthletesinClubCoachClubForTeam(int clubcoachid)
        {
            try
            {
                List<AthleteDTO> atheletes = UsersDal.GetAthletesinClubCoachClubForTeam(clubcoachid);
                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/GetAthletesinClubCoachClubForTeam", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse RecommendAthletesToUniversities(int athleteId, int clubcoachId, int instituteId)
        {
            try
            {
                int id = UsersDal.RecommendAthletesToUniversities(athleteId, clubcoachId, instituteId);
                if (id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = id;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/RecommendAthletesToUniversities", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetBlogs()
        {
            try
            {
                List<BlogDTO> blogs = UsersDal.GetBlog();
                if (blogs.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = blogs;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/GetBlogs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse DeleteClubCoachInstitutes(int clubCoachId)
        {

            try
            {
                var ccId = UsersDal.DeleteClubCoachInstitutes(clubCoachId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = ccId;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/DeleteClubCoachInstitutes", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse UpdateClubCoachToAthelete(int clubCoachId, int atheleteId)
        {

            try
            { 
                UsersDal.UpdateClubCoachToAthelete(clubCoachId, atheleteId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Athlete_ClubCoach_Updated;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/UpdateClubCoachToAthelete", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
   
        [HttpPost]
        public Utility.CustomResponse SendInvitationtoAtheletefromClubCoach(ClubCoachInvitationDTO clubCoachInvitationDto)
        {
            SupportMailDTO support = new SupportMailDTO();
            MailMessage msg = new MailMessage();
            try
            {

                support = UsersDal.GetSupportMails("RequiredEmail");

                if (clubCoachInvitationDto.AtheleteId == 0)
                {
                    AthleteDTO athelete = UsersDal.GetAthleteProfile_v2(clubCoachInvitationDto.AtheleteEmail);
                    if (athelete.Id == 0)
                    {
                        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ClubCoachInvitationToAthlete.html"));
                        string readFile = reader.ReadToEnd();
                        string myString;
                        myString = readFile;
                        myString = myString.Replace("$$AtheleteEmail$$", clubCoachInvitationDto.AtheleteEmail);
                        myString = myString.Replace("$$ClubCoachEmail$$", clubCoachInvitationDto.ClubCoachEmail);

                        //MailAddress fromMail = new MailAddress(support.EmailAddress);
                        //msg.From = fromMail;
                        //msg.To.Add(clubCoachInvitationDto.AtheleteEmail);
                        //msg.Subject = "Your coach invites you to join LemonAid - College Recruiting | Streamlined";
                        //msg.Body = myString;
                        //msg.IsBodyHtml = true;
                        //var smtpMail = new SmtpClient();
                        //smtpMail.Host = support.Host;
                        //smtpMail.Port = Convert.ToInt32(support.Port);
                        //smtpMail.EnableSsl = Convert.ToBoolean(support.SSL);
                        //smtpMail.UseDefaultCredentials = false;
                        //smtpMail.Credentials = new System.Net.NetworkCredential(support.EmailAddress, support.Password);
                        //smtpMail.Timeout = 1000000;
                        //smtpMail.Send(msg);

                        SendGridMailModel sendGridModel = new SendGridMailModel();
                        sendGridModel.toMail = new List<string>();
                        sendGridModel.toMail.Add(clubCoachInvitationDto.AtheleteEmail);
                        sendGridModel.fromMail = support.EmailAddress;
                        sendGridModel.fromName = "Lemonaid";
                        sendGridModel.subject = "Your coach invites you to join LemonAid - College Recruiting | Streamlined";
                        sendGridModel.Message = myString;
                        sendGridModel.email = support.EmailAddress;
                        sendGridModel.password = support.Password;
                        SendGridSMTPMail.SendEmail(sendGridModel);

                        EmailLogModel emaillog = new EmailLogModel();
                        emaillog.AddEmailLog(support.EmailAddress, clubCoachInvitationDto.AtheleteEmail, msg.Subject, "Mail", 1);

                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = clubCoachInvitationDto.AtheleteId;
                        _result.Message = CustomConstants.Mail_Sent;

                    }
                    else if ((athelete.Id > 0) && (athelete.ClubId == 0))
                    {
                        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AtheleteInvitationFromClubCoach.html"));
                        string readFile = reader.ReadToEnd();
                        string myString;
                        myString = readFile;
                        myString = myString.Replace("$$AtheleteEmail$$", clubCoachInvitationDto.AtheleteEmail);
                        myString = myString.Replace("$$ClubCoachEmail$$", clubCoachInvitationDto.ClubCoachEmail);


                        string athleteName = string.Empty, athleteEmailAddress = string.Empty; string deviceName = string.Empty; string deviceId = string.Empty; int loginType = 0; string username = string.Empty;
                        string message = clubCoachInvitationDto.ClubCoachEmail + " has requested you to join the club.";
                        UsersDalV4.GetAthleteDetails_v4(athelete.Id, ref athleteName, ref athleteEmailAddress, ref deviceId, ref deviceName,ref loginType, ref username);
                        //UsersDal.PushNotification(message, deviceName);

                        //Notifications Insertion 
                        NotificationDTO not = new NotificationDTO();
                        not.Status = 0;
                        not.UserID = athelete.Id;
                        not.SenderId = clubCoachInvitationDto.ClubCoachId;
                        not.NotificationId = 9;
                        not.ShortMessage = message;
                        not.LongMessage = myString;
                        not.SenderType = 2;
                        not.ReceiverType = 0;
                        UsersDal.AddNotifications(not);
                        UsersDal.UpdateClubCoachToAthelete(clubCoachInvitationDto.ClubCoachId, athelete.Id);

                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = clubCoachInvitationDto.AtheleteId;
                        _result.Message = CustomConstants.Notification_Sent;

                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.Athelete_Already_ClubCoach;

                    }


                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/SendInvitationtoAtheletefromClubCoach", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;
        }


        [HttpPost]
        public Utility.CustomResponse AddClub(ClubDTO clubDto)
        {
            try
            {

                var id = UsersDal.AddClub(clubDto);
                if (id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = id;
                    _result.Message = CustomConstants.Club_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/AddClub", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetClubs()
        {
            try
            {
                List<ClubDTO>  clubs = UsersDal.GetClubs();
                if (clubs.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = clubs;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach/GetClubs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
         
        public List<CoachDTO> AddCoachVideos(List<CoachDTO> Coaches)
        { 
            foreach (var coach in Coaches)
            { 
                List<int> coachIds = Dal2.GetUniversityCoaches(coach.Id);
                List<VideoDTO> totalcoachVideos = new List<VideoDTO>();
                List<VideoDTO> coachVideos = new List<VideoDTO>();
                foreach (var j in coachIds)
                {
                    coachVideos = Dal2.GetCoachVideos(j);
                    foreach (var Video in coachVideos)
                    {
                        totalcoachVideos.Add(Video);
                    } 
                }

                coach.CoachVideos = totalcoachVideos;
            }

            return Coaches;
        }
    }
}
