﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace Lemonaid_web_api.Models
{
    public class DAL2V7
    {
        internal static int UpdateUniversityVirtualVideo(UniversityvirtualDTO universityvirtualDTO)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateUniversityVirtualVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter virtualVideoURL = cmd.Parameters.Add("@VirtualVideoURL", SqlDbType.VarChar, 2000);
                    virtualVideoURL.Direction = ParameterDirection.Input;
                    virtualVideoURL.Value = universityvirtualDTO.VirtualVideoURL;

                    SqlParameter MatchesVirtualThumbnailURL = cmd.Parameters.Add("@VirtualThumbnailURL", SqlDbType.VarChar, 2000);
                    MatchesVirtualThumbnailURL.Direction = ParameterDirection.Input;
                    MatchesVirtualThumbnailURL.Value = universityvirtualDTO.VirtualThumbnailURL;


                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = universityvirtualDTO.InstituteId;

                    id = Convert.ToInt32(cmd.ExecuteScalar());

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V7/UpdateUniversityVirtualVideo", "", ex.Message, "Exception", 0);
            }


            return id;
        }


        internal static int UpdateUniversityMatchedVirtualVideo(UniversityvirtualDTO universityvirtualDTO)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateUniversityMatchedVirtualVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter virtualVideoURL = cmd.Parameters.Add("@MatchedVirtualVideoURL", SqlDbType.VarChar, 2000);
                    virtualVideoURL.Direction = ParameterDirection.Input;
                    virtualVideoURL.Value = universityvirtualDTO.VirtualVideoURL;

                    SqlParameter MatchesVirtualThumbnailURL = cmd.Parameters.Add("@MatchesVirtualThumbnailURL", SqlDbType.VarChar, 2000);
                    MatchesVirtualThumbnailURL.Direction = ParameterDirection.Input;
                    MatchesVirtualThumbnailURL.Value = universityvirtualDTO.VirtualThumbnailURL;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = universityvirtualDTO.InstituteId;

                    SqlParameter CoachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    CoachId.Direction = ParameterDirection.Input;
                    CoachId.Value = universityvirtualDTO.CoachId;

                    SqlParameter AthleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteId.Direction = ParameterDirection.Input;
                    AthleteId.Value = universityvirtualDTO.AthleteId;

                    id = Convert.ToInt32(cmd.ExecuteScalar());

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V7/UpdateUniversityMatchedVirtualVideo", "", ex.Message, "Exception", 0);
            }


            return id;
        }


        internal static List<Video360TypeDTO> GetCoachVideos360TypeBySearchText(CoachVideo360 coachVideo360)
        {
            List<Video360TypeDTO> lstVideo360Type = new List<Video360TypeDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachVideos360TypeBySearchtext]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Text = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 100);
                    Text.Direction = ParameterDirection.Input;
                    Text.Value = coachVideo360.SearchText;

                    SqlParameter Offset = cmd.Parameters.Add("@Offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = coachVideo360.Offset;

                    SqlParameter Max = cmd.Parameters.Add("@Max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = coachVideo360.Max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Video360TypeDTO v = new Video360TypeDTO();
                        v.Name = myData["Name"].ToString();
                        v.VideoURL = myData["VideoURL"].ToString();
                        v.ThumbNailURL = myData["ThumbnailURL"].ToString();
                        v.CoachId = Convert.ToInt16(myData["CoachId"]);
                        v.VideoFormat = myData["VideoFormat"].ToString();
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Duration"]);
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["VideoType"]);
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Description = myData["Description"].ToString();
                        lstVideo360Type.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v7/GetCoachVideos360TypeBySearchText", "", ex.Message, "Exception", 0);
            }

            return lstVideo360Type;
        }



        internal static int AddStripeAccDetails(CoachStripeAccDetailsDTO accdetailsdto)
        {

            int Id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddStripeAccDetails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter CoachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    CoachId.Direction = ParameterDirection.Input;
                    CoachId.Value = accdetailsdto.CoachId;

                    SqlParameter StripeUserId = cmd.Parameters.Add("@StripeUserId", SqlDbType.VarChar, 200);
                    StripeUserId.Direction = ParameterDirection.Input;
                    StripeUserId.Value = accdetailsdto.StripeUserId;

                    SqlParameter CardToken = cmd.Parameters.Add("@CardToken", SqlDbType.VarChar, 4024);
                    CardToken.Direction = ParameterDirection.Input;
                    CardToken.Value = accdetailsdto.CardToken;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        Id = (int)myData["id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V7/AddStripeAccDetails", "", ex.Message, "Exception", 0);
                throw;
            }

            return Id;
        }


        internal static int AddCity(CityDTO cityDTO)
        {
            int id = 0;

            try
            {

                List<string> ConnStrings;
                var envirmt = ConfigurationManager.AppSettings["Environment"].ToString();
                if (envirmt == "2")
                    ConnStrings = DALConnection.GetStagingConnection();
                else
                    ConnStrings = DALConnection.GetProductionConnection();

                foreach (var conn in ConnStrings)
                {
                    SqlConnection myConn = new SqlConnection(conn);
                    myConn.Open();

                    try
                    {
                        if (null != myConn)
                        {
                            SqlCommand cmd = new SqlCommand("[AddCity]", myConn);
                            cmd.CommandType = CommandType.StoredProcedure;


                            SqlParameter CityName = cmd.Parameters.Add("@CityName", SqlDbType.VarChar, 50);
                            CityName.Direction = ParameterDirection.Input;
                            CityName.Value = cityDTO.Name;

                            SqlParameter stateID = cmd.Parameters.Add("@stateID", SqlDbType.Int);
                            stateID.Direction = ParameterDirection.Input;
                            stateID.Value = cityDTO.StateID;

                            id = Convert.ToInt32(cmd.ExecuteScalar());


                        }

                    }
                    catch (Exception ex)
                    {

                    }
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V7/AddCity", "", ex.Message, "Exception", 0);
            }


            return id;
        }

        internal static int GetCoach360VideosCount(string emailaddress)
        {
            int cnt = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[Get360VideoCountByCoachId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddresspar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddresspar.Direction = ParameterDirection.Input;
                    emailaddresspar.Value = emailaddress;

                    cnt = Convert.ToInt32(cmd.ExecuteScalar());

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v7/GetCoach360VideosCount", "", ex.Message, "Exception", 0);
            }

            return cnt;
        }

        internal static void DeleteVirtualMatchesVideos(MatchesVirtualVideoDTO matchesVirtualVideoDTO)
        {

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteVirtualMatchesVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = matchesVirtualVideoDTO.InstituteId;

                    SqlParameter videoFlag = cmd.Parameters.Add("@VideoFlag", SqlDbType.Int); //0 - virtual , 1 - matches virtual , 2 -- athleteMatchesVirtual
                    videoFlag.Direction = ParameterDirection.Input;
                    videoFlag.Value = matchesVirtualVideoDTO.VideoFlag;

                    SqlParameter AthleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteId.Direction = ParameterDirection.Input;
                    AthleteId.Value = matchesVirtualVideoDTO.AthleteId;

                    SqlParameter VideoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    VideoId.Direction = ParameterDirection.Input;
                    VideoId.Value = matchesVirtualVideoDTO.VideoId;

                    SqlParameter CoachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    CoachId.Direction = ParameterDirection.Input;
                    CoachId.Value = matchesVirtualVideoDTO.CoachId;

                    int id = Convert.ToInt32(cmd.ExecuteScalar());

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v7/DeleteVirtualMatchesVideos", "", ex.Message, "Exception", 0);
            }

        }

        internal static MatchesVirtualVideoDTO GetVirtualMatchesVideos(MatchesVirtualVideoDTO matchesVirtualVideoDTO)
        {
            MatchesVirtualVideoDTO videos = new MatchesVirtualVideoDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetVirtualMatchesVideosForInstitution]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = matchesVirtualVideoDTO.InstituteId;

                    SqlParameter AthleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteId.Direction = ParameterDirection.Input;
                    AthleteId.Value = matchesVirtualVideoDTO.AthleteId;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                       videos.VirtualVideoURL = (myData["VirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualVideoURL"]).Trim();
                        videos.VirtualThumbnailURL = (myData["VirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualThumbnailURL"]).Trim();
                        videos.MatchesVirtualVideoURL = (myData["MatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualVideoURL"]).Trim();
                        videos.MatchesVirtualThumbnailURL = (myData["MatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualThumbnailURL"]).Trim();
                        videos.AthleteMatchesVirtualVideoURL = (myData["AthleteMatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleteMatchesVirtualVideoURL"]).Trim();
                        videos.AthleteMatchesVirtualThumbnailURL = (myData["AthleteMatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleteMatchesVirtualThumbnailURL"]).Trim();
                        videos.VideoId = (myData["VideoId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["VideoId"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v7/GetVirtualMatchesVideos", "", ex.Message, "Exception", 0);
            }
            return videos;
        }

        internal static int CheckAthleteExistsInAnySport(string emailAddress)
        {
            int cnt = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CheckAthleteExistsInAnySport]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddresspar = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddresspar.Direction = ParameterDirection.Input;
                    Emailaddresspar.Value = emailAddress;


                    cnt =(int) cmd.ExecuteScalar(); 
                   
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v7/CheckAthleteExistsInAnySport", "", ex.Message, "Exception", 0);
            }
            return cnt;
        }


        internal static List<NotificationDTO> GetUserNotifications(UserNotifications userNotifications)
        {

            List<NotificationDTO> Notifications = new List<NotificationDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetNotifications_2V7]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userIdPar = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userIdPar.Direction = ParameterDirection.Input;
                    userIdPar.Value = userNotifications.Userid;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = userNotifications.Offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = userNotifications.Max;

                    SqlParameter senderTypePar = cmd.Parameters.Add("@senderType", SqlDbType.Int);
                    senderTypePar.Direction = ParameterDirection.Input;
                    senderTypePar.Value = userNotifications.SenderType;

               

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        NotificationDTO notification = new NotificationDTO();
                        notification.ID = (int)myData["Id"];
                        notification.NotificationId = (int)myData["NotificationId"];
                        notification.UserID = (int)myData["UserID"];
                        notification.Status = (int)myData["Status"];
                        notification.SenderId = (int)myData["SenderId"];
                        notification.Message = (string)myData["Message"];
                        notification.NotificationType = (int)myData["NotificationType"];
                        notification.Date = (DateTime)myData["CreatedDate"];
                        notification.TotalCount = (int)myData["TotalCount"];
                        notification.ShortMessage = (myData["ShortMessage1"] == DBNull.Value) ? string.Empty : (string)myData["ShortMessage1"];
                        notification.LongMessage = (myData["LongMessage1"] == DBNull.Value) ? string.Empty : (string)myData["LongMessage1"];
                        notification.BadgeTypeId = (myData["BadgeTypeId"] == DBNull.Value) ? 0 : (int)myData["BadgeTypeId"];
                        notification.BadgeCount = (myData["BadgeCount"] == DBNull.Value) ? 0 : (int)myData["BadgeCount"];
                        notification.SenderType = (myData["SenderType"] == DBNull.Value) ? 0 : (int)myData["SenderType"];
                        notification.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : (string)myData["ProfilePicURL"];
                        notification.SenderName = (myData["sendername"] == DBNull.Value) ? string.Empty : (string)myData["sendername"];
                        Notifications.Add(notification);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V7/GetUserNotifications", "", ex.Message, "Exception", 0);
                throw;
            }

            return Notifications;
        }

        internal static MatchesVirtualVideoDTO GetAthleteInstituteMatchedVirtualVideos(MatchesVirtualVideoDTO matchesVirtualVideoDTO)
        {
            MatchesVirtualVideoDTO videos = new MatchesVirtualVideoDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteInstituteMatchedVirtualVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = matchesVirtualVideoDTO.InstituteId;

                    SqlParameter AthleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    AthleteId.Direction = ParameterDirection.Input;
                    AthleteId.Value = matchesVirtualVideoDTO.AthleteId;

                    SqlParameter CoachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    CoachId.Direction = ParameterDirection.Input;
                    CoachId.Value = matchesVirtualVideoDTO.CoachId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    { 
                        videos.MatchesVirtualVideoURL = (myData["MatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualVideoURL"]).Trim();
                        videos.MatchesVirtualThumbnailURL = (myData["MatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualThumbnailURL"]).Trim();
              
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v7/GetAthleteInstituteMatchedVirtualVideos", "", ex.Message, "Exception", 0);
            }
            return videos;
        }
    }
}