 
ALTER PROCEDURE [dbo].[UpdateCoachEmailaddress] 
(
	@Emailaddress varchar(100),
	@Name varchar(100),
	@PhoneNumber varchar(50),
	@CoachId int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here  
	declare @instituteid int

	Begin try
	Begin Tran

	select @instituteid = InstituteId from Coaches where Id = @CoachId

	Update Coaches set Emailaddress = @Emailaddress  Where Id = @CoachId
	Update Institution set Name = @Name , Phonenumber = @PhoneNumber Where Id = @instituteid  

	Commit Tran
	End try
	Begin Catch
	  Rollback Tran
	End Catch

	select @CoachId as Id  
END 
 GO




Alter table ClubCoach add TrainedSport varchar(50)


 
 

CREATE PROCEDURE [dbo].[AddClubCoach2_V6] 
	@Name varchar(100),
	@Gender int, 
	@Emailaddress varchar(100),
	@Description varchar(1000), 
	@ProfilePicURL varchar(1000),
	@DOB varchar(50),
    --@ClubAddress varchar(100),
	@FacebookId varchar(500), 
	@CoverPicURL varchar(1000),
	@Latitude varchar(50),
	@Longitude varchar(50), 
	@Active bit,
	@Phonenumber varchar(50),
	@ClubName varchar(100),	 
    @DeviceType varchar(50),
	@DeviceId varchar(50),
	@TeamType int, --Club, HS
	@Location varchar(200),
	@SchoolName varchar(100),
	@ZipCode varchar(20),
	@StateId int,
	@SchoolType int,
	@ClubPhoneNumber varchar(20),
	@SchoolId int,
	@LoginType int,
	@Username varchar(100),
	@Almamater varchar(100),
	@TrainedSport varchar(50),
	@Id int 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	 declare @cnt int 
	 declare @ClubId int
	 declare @getDeviceId int 
	 declare @devicecnt int

	 if(@ClubName != '')
	 Begin
		 select @ClubId = Id from Clubs where Name =  @ClubName
		 If(isnull(@ClubId,0) = 0)
		 Begin
		   Insert into Clubs(Name,CreatedDate,CreatedBy)
		   values(@ClubName,getutcdate(),2)
		   set @ClubId = @@Identity
		 End
	 End
	 Else
	 Begin
	  set @ClubId = 0
	 End

	 print @ClubId
     -- exec @getDeviceId = [AddDevice] @DeviceId,@DeviceType
     select @devicecnt = count(*) from Device where  DeviceId = @DeviceId and  DeviceType = @DeviceType
	 if(@devicecnt = 0)
	 Begin
		insert into Device(DeviceID,DeviceType,CreateDate) 
		values (@DeviceId,@DeviceType,getutcdate())
		set @getDeviceId = @@Identity
	 End
	 else
	 begin
	  set @getDeviceId = (select Id from Device where DeviceID = @DeviceId and DeviceType=@DeviceType)
	 end
	 print @getDeviceId

	  if(@SchoolName != '')
	 Begin
		 select @SchoolId = Id from Schools where Name =  @SchoolName
		 If(isnull(@SchoolId,0) = 0)
		 Begin
		   Insert into Schools(Name,CreatedDate,CreatedBy)
		   values(@SchoolName,getutcdate(),2)
		   set @SchoolId = @@Identity
		 End
	 End
	 Else
	 Begin
	  set @SchoolId = 0
	 End

	  if(isnull(@LoginType,1) = 1)
	 Begin
	 select @cnt = count(*) from ClubCoach where Emailaddress = @Emailaddress and logintype = @LoginType
	 End
	 Else
	 Begin
	  select @cnt = count(*) from ClubCoach where Username = @Username  and logintype = @LoginType
	 End 
	  
	 
	 if(@cnt = 0)
	 Begin
		insert into ClubCoach (Name,Gender,Emailaddress,Description,ProfilePicURL,DOB, 
		FacebookId,CoverPicURL,[CreatedDate],Latitude,Longitude,Active,Phonenumber,ClubId,DeviceId,
		TeamType,Location,SchoolName,ZipCode,StateId,SchoolType,ClubPhoneNumber,SchoolId,LoginType,Username,
		Almamater,TrainedSport) 
		values (@Name,@Gender,@Emailaddress,@Description,@ProfilePicURL,@DOB, 
		@FacebookId,@CoverPicURL,GETUTCDATE(),@Latitude,@Longitude,@Active,@Phonenumber,@ClubId,@getDeviceId,  
        @TeamType,@Location,@SchoolName,@ZipCode,@StateId,@SchoolType,@ClubPhoneNumber,@SchoolId,@LoginType,@Username
		,@Almamater,@TrainedSport)
		set @Id = @@Identity 
	 
	 End
	 Else
	 Begin
	 if(isnull(@LoginType,1) = 1)
	  Begin
	 	Update ClubCoach set Name=@Name,Gender=@Gender, Emailaddress=@Emailaddress,
		Description=@Description, ProfilePicURL=@ProfilePicURL,DOB=@DOB, 
		FacebookId=@FacebookId,CoverPicURL=@CoverPicURL,Latitude = @Latitude,Longitude = @Longitude,
		Active=@Active,Phonenumber = @Phonenumber,  ClubId = @ClubId , DeviceId = @getDeviceId,
		TeamType=@TeamType,Location=@Location,SchoolName=@SchoolName,ZipCode=@ZipCode,StateId=@StateId,
		SchoolType=@SchoolType,ClubPhoneNumber=@ClubPhoneNumber,SchoolId=@SchoolId,Username=@Username,
		LoginType=@LoginType,Almamater=@Almamater,TrainedSport=@TrainedSport
	    where Emailaddress=@Emailaddress and logintype = @LoginType
	 End
	 Else
	 Begin
	 	Update ClubCoach set Name=@Name,Gender=@Gender, Emailaddress=@Emailaddress,
		Description=@Description, ProfilePicURL=@ProfilePicURL,DOB=@DOB, 
		FacebookId=@FacebookId,CoverPicURL=@CoverPicURL,Latitude = @Latitude,Longitude = @Longitude,
		Active=@Active,Phonenumber = @Phonenumber,  ClubId = @ClubId , DeviceId = @getDeviceId,
		TeamType=@TeamType,Location=@Location,SchoolName=@SchoolName,ZipCode=@ZipCode,StateId=@StateId,
		SchoolType=@SchoolType,ClubPhoneNumber=@ClubPhoneNumber,SchoolId=@SchoolId,Username=@Username,
		LoginType=@LoginType,Almamater=@Almamater,TrainedSport=@TrainedSport
	     where Username=@Username and logintype = @LoginType
	 End 
	End
   	  
	if(isnull(@LoginType,1) = 1)
	begin
	     set @Id = (select id from ClubCoach where Emailaddress=@Emailaddress and logintype = @LoginType)
	end
	else
	begin
	    set @Id = (select id from ClubCoach where Username = @Username and logintype = @LoginType)
	end
	  
		select cc.*,c.Name as ClubName,s.Name as StateName,@DeviceType as  DeviceType,@DeviceId as  DeviceName
		 from clubcoach cc 
		left join clubs c on c.id = cc.clubid
	    left join States s on s.id = cc.stateid
	    where cc.id = @Id
 
END
 










GO


 
ALTER PROCEDURE [dbo].[GetAtheleteFavourites_v4]     --[GetAtheleteFavourites_v4] 63,0,30
 @AtheleteId int,
 @offset int,
 @max int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @totalCount int  
     --declare @AtheleteId int 
	 declare @TotalAcceptedCount int
	 declare @totalChatCount int

   -- select @AtheleteId = id from Athelete where Emailaddress = @emailaddress
    
	select @TotalAcceptedCount = count(*) from AtheleteInvitations where AtheleteId = @AtheleteId and Accept = 1

	select @totalCount = count(*) from AtheleteInstituteMatches am
		left  join InstituteAtheleteMatches ia on ia.coachid = am.coachid
		and am.atheleteid = ia.atheleteid
		--inner join institution i on i.id = am.instituteid
		where am.AtheleteId = @AtheleteId and am.status = 1 

	 select @totalChatCount = count(*) from chats where SenderId = @AtheleteId 

	  select distinct i.*,isnull(ia.instituteid,0) as Likes,@totalCount as TotalCount,@totalChatCount as TotalChatCount,
	   am.CreatedDate as AtheleteDate, ia.CreatedDate as InstituteDate,
	    ait.InvitedDate,c.Emailaddress as CoachEmail , c.FacebookId as CoachFacebook,
		ait.Accept,@TotalAcceptedCount as TotalAcceptedCount,
		isnull(c.id,0) as CoachId, isnull(@AtheleteId,0) as AtheleteId
		 ,a.Name as AtheleteName,a.ProfilePicURL as AtheleteProfilePic,c.payment_paid,
		 d.DeviceType, d.DeviceId  
	    from AtheleteInstituteMatches am
	    inner join Coaches c on c.id = am.coachid
		left  join InstituteAtheleteMatches ia 
		on ia.coachid = am.coachid and am.atheleteid = ia.atheleteid and ia.status = 1
		inner join institution i on i.id = c.instituteid
		left join AtheleteInvitations ait on ait.instituteid = i.id and ait.atheleteid = am.atheleteid
		 left join Athelete a on a.id = am.AtheleteId 
		 left join Device d on d.Id = c.DeviceId
		where am.AtheleteId = @AtheleteId and am.status = 1 		
	    
        
END
 

GO



CREATE TABLE [dbo].[CoachVideos360Type](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[VideoURL] [varchar](1024) NOT NULL,
	[ThumbnailURL] [varchar](1024) NULL,
	[Status] [int] NULL,
	[CoachId] [int] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[VideoFormat] [varchar](50) NULL,
	[duration] [int] NULL,
	[VideoType] [int] NULL,
	[Latitude] [varchar](100) NULL,
	[Longitude] [varchar](100) NULL
)

GO

SET ANSI_PADDING OFF
GO




 
 
CREATE PROCEDURE [dbo].[DeleteCoachVideo360Type] 
	@VideoID int,
	@Id int Output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

 
    	Delete from  [CoachVideos360Type] where ID = @VideoID 
	

		select @ID as Id


END

 








































GO



 


CREATE PROCEDURE [dbo].[AddCoachVideos360Type] --[AddCoachVideos360Type] 
    @Name varchar(50),
	@VideoURL varchar(1024),
	@ThumbnailURL varchar(1024), 
	@Status int,
	@CoachId int,
	@VideoFormat varchar(50),
	@duration int,
	@Latitude varchar(100),
	@Longitude varchar(100),
	@Id int Output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON; 
	declare @VideoId int

	 
 Begin Try
 Begin Tran 
		 
		insert into CoachVideos360Type(Name,VideoURL,ThumbnailURL,Status,CoachId,CreatedDate,VideoFormat,duration,videoType,latitude,longitude) 
		values (@Name,@VideoURL,@ThumbnailURL,@Status,@CoachId,getutcdate(),@VideoFormat,@duration,360,@Latitude,@Longitude)
		set @VideoId = @@Identity

		select @VideoId as Video360ID
 Commit Tran 
 End Try
Begin catch
 Rollback Tran
 set @Id = -1
End catch

END

 








GO


 

 
 

 



ALTER PROCEDURE [dbo].[AddCoach_v4]  --exec [AddCoach] 'smdirfan406@gmail.com',4,'123456',0,1,0
	@EmailAddress varchar(100),
	@InstituteId int,
	@Password varchar(100) ,
        @Active Bit,
	@TeamType int,	
	@DeviceType varchar(50),
	@DeviceId varchar(500),
	@Id int Output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	 declare @cnt int
	 declare @UniVCount int
	 declare @HeadCoach int --0-notclaimed,1-HeadCoach,2-NotHeadCoach
	 declare @activation_code varchar(1000) 
	 declare @getDeviceId int 
	 declare @devicecnt int
 
 	-- exec @getDeviceId = [AddDevice] @DeviceId,@DeviceType
    select @devicecnt = count(*) from Device where  DeviceId = @DeviceId and  DeviceType = @DeviceType
	 if(@devicecnt = 0)
	 Begin
		insert into Device(DeviceID,DeviceType,CreateDate) 
		values (@DeviceId,@DeviceType,getutcdate())
		set @getDeviceId = @@Identity
	 End
	 else
	 begin
	  set @getDeviceId = (select Id from Device where DeviceID = @DeviceId and DeviceType=@DeviceType)
	 end

	 select @UniVCount = count(*) from Coaches where Instituteid = @InstituteId 
	 select @HeadCoach=HeadCoach from Coaches where Instituteid = @InstituteId 
 
		 if(@UniVCount = 1 and @HeadCoach=0)
		 Begin 
		 set @activation_code = (select newid()) 
		  Update Coaches set EmailAddress = @EmailAddress, password=@Password, 
           activation_code=@activation_code,PreferredStrokes='ALL',DeviceId=@getDeviceId
		   Where Instituteid = @InstituteId 
		   select @Id = Id from Coaches Where Emailaddress = @Emailaddress 
		 End
		 Else if(@UniVCount >= 1)
		 Begin
		    set @HeadCoach = 2
		    select @cnt = count(*) from Coaches where Emailaddress = @Emailaddress 
				if(@cnt = 0)
				Begin
				   set @activation_code = (select newid()) 
					insert into Coaches(EmailAddress,InstituteId,Password,active,CreatedDate,FacebookId,Payment_Paid,HeadCoach,activation_code,TeamType,PreferredStrokes,DeviceId) 
					values (@EmailAddress,@InstituteId,@Password,@Active,getutcdate(),0,0,@HeadCoach,@activation_code,@TeamType,'ALL',@getDeviceId)
					set @Id = @@Identity 
				End
				Else
				Begin 
				    select @Id = Id from Coaches Where Emailaddress = @Emailaddress 
				End
	     End
		 
	  
	 select c.*,i.name as UniversityName,d.DeviceId,d.DeviceType from Coaches c
	 inner join institution i on i.id = c.instituteid
	 left join Device d on d.Id = c.DeviceId
	 where c.id = @Id 
		
		 
END

  



























GO


 
 
 
CREATE PROCEDURE [dbo].[GetFamilyFriendsInstituteFavourites_2V6]  --[GetFamilyFriendsInstituteFavourites] 3,0,10
 @familyFriendId int 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @totalCount int   

	select @totalCount = count(*) from FamilyFriendsInstituteMatches ffim 
		where ffim.FamilyFriendsId = @familyFriendId and ffim.status = 1 


	 select i.*,@totalCount as TotalCount
	 from FamilyFriendsInstituteMatches ffim 
	 inner join institution i on i.id = ffim.instituteid
	 where ffim.FamilyFriendsId = @familyFriendId and ffim.status = 1 
	 order by i.Atheleterate desc
   
        
END
  


GO




 



 
CREATE PROCEDURE [dbo].[GetClubCoachFavourites_2V6]   
 @ClubCoachId int 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @totalCount int   
   

	select @totalCount = count(*) from ClubCoachInstituteMatches cc 
		where cc.ClubCoachId = @ClubCoachId and cc.status = 1 


	  select i.*,@totalCount as TotalCount 
	  from ClubCoachInstituteMatches cc  
	  inner join Institution i on i.id = cc.InstituteId	  
	   where cc.ClubCoachId = @ClubCoachId and cc.status = 1  
	   order by AtheleteRate desc  

	 
        
END
 



GO



 
  
 
ALTER PROCEDURE [dbo].[GetFamilyFriendsAtheleteFavourites]  
 @familyFriendId int,
 @offset int,
 @max int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @totalCount int   

	select @totalCount = count(*) from FamilyFriendsAthleteMatches ffam 
	inner join athelete a on ffam.athleteid = a.id
		where ffam.FamilyFriendsId = @familyFriendId and ffam.status = 1 
		 

	 select a.*,ab.*,I.Name as UniversityName,I.Address as UnivAddress, I.ClassificationName,
	 I.EmailAddress as UnivEmailAddress,
	  I.ProfilePicURL as UnivProfilePic, I.PhoneNumber as UnviPhoneNumber,I.Size as UnivSize
	 ,@totalCount as TotalCount
	 from FamilyFriendsAthleteMatches ffam 
	 inner join athelete a on a.id  = ffam.Athleteid 
	  left join AtheleteBadges ab on ab.AtheleteId = a.Id 
	  left join Institution I on I.Id = a.InstituteId
	 where ffam.FamilyFriendsId = @familyFriendId and ffam.status = 1 
	 and a.Atheletetype = 0
	  order by a.Atheleterate desc
	 OFFSET  @Offset  ROWS FETCH NEXT @max ROWS ONLY 
        
END

  
 
 





GO


 



 
ALTER PROCEDURE [dbo].[GetClubCoachFavourites_2V6] 
 @ClubCoachId int 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @totalCount int   
   

	select @totalCount = count(*) from ClubCoachInstituteMatches cc 
	 inner join Institution i on i.id = cc.InstituteId	  
		where cc.ClubCoachId = @ClubCoachId and cc.status = 1 


	  select i.*,@totalCount as TotalCount 
	  from ClubCoachInstituteMatches cc  
	  inner join Institution i on i.id = cc.InstituteId	  
	   where cc.ClubCoachId = @ClubCoachId and cc.status = 1  
	   order by AtheleteRate desc  

	 
        
END
 

GO



Drop table States

 
CREATE TABLE [dbo].[cities](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[cityName] [varchar](50) NOT NULL,
	[stateID] [int] NOT NULL DEFAULT ('0'),
	[countryID] [varchar](3) NOT NULL DEFAULT ('0'),
	[latitude] [varchar](500) NOT NULL DEFAULT ('0'),
	[longitude] [varchar](500) NOT NULL DEFAULT ('0'),
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[countries]    Script Date: 2/25/2017 3:37:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[countries](
	[countryID] [varchar](3) NULL,
	[countryName] [varchar](52) NULL,
	[latitude] [varchar](500) NULL,
	[longitude] [varchar](500) NULL
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[States]    Script Date: 2/25/2017 3:37:29 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[States](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[StateName] [varchar](100) NULL,
	[Country] [varchar](100) NULL
)

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[cities] ON 

GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (206, N'Mountain View', 5, N'USA', N'37.42', N'-122.06')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (207, N'Ackworth', 21, N'USA', N'41.36', N'-93.43')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (208, N'Far Rockaway', 42, N'USA', N'40.61', N'-73.79')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (209, N'Los Angeles', 5, N'USA', N'34.04', N'-118.25')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (210, N'Sunnyvale', 5, N'USA', N'37.42', N'-122.01')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (211, N'Newark', 30, N'USA', N'40.74', N'-74.17')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (212, N'Malden', 21, N'USA', N'42.43', N'-71.05')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (213, N'Trumbull', 7, N'USA', N'41.26', N'-73.21')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (214, N'Cupertino', 5, N'USA', N'37.3', N'-122.09')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (215, N'Fort Lauderdale', 21, N'USA', N'26.1', N'-80.27')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (216, N'South Amboy', 30, N'USA', N'40.48', N'-74.29')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (217, N'Keller', 43, N'USA', N'32.93', N'-97.25')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (218, N'Long Island City', 42, N'USA', N'40.74', N'-73.94')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (219, N'Brooklyn', 42, N'USA', N'40.69', N'-73.99')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (220, N'Army Post Office', 51, N'USA', N'56', N'-100')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (221, N'Mesquite', 43, N'USA', N'32.76', N'-96.61')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (222, N'Mc Minnville', 42, N'USA', N'35.65', N'-85.73')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (223, N'Flatwoods', 17, N'USA', N'38.5', N'-82.73')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (224, N'Bogart', 42, N'USA', N'33.91', N'-83.52')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (225, N'Cedar Hill', 43, N'USA', N'32.59', N'-96.97')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (226, N'Levittown', 42, N'USA', N'40.72', N'-73.52')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (227, N'Cornelia', 42, N'USA', N'34.5', N'-83.58')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (228, N'Des Plaines', 13, N'USA', N'42', N'-87.9')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (229, N'Denver', 6, N'USA', N'39.75', N'-105')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (230, N'Riverton', 44, N'USA', N'40.48', N'-112.01')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (231, N'Glen Burnie', 20, N'USA', N'39.16', N'-76.6')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (232, N'Miami', 21, N'USA', N'25.81', N'-80.24')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (233, N'Chicago', 13, N'USA', N'41.88', N'-87.63')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (234, N'Evansdale', 21, N'USA', N'42.49', N'-92.29')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (235, N'Houston', 43, N'USA', N'29.8', N'-95.42')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (236, N'Anderson', 40, N'USA', N'34.48', N'-82.68')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (237, N'Bozeman', 26, N'USA', N'45.71', N'-111.06')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (238, N'Marksville', 18, N'USA', N'31.18', N'-92.02')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (239, N'Billerica', 21, N'USA', N'42.55', N'-71.26')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (240, N'Memphis', 42, N'USA', N'35.03', N'-89.78')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (241, N'Seattle', 47, N'USA', N'47.58', N'-122.3')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (242, N'Prospect', 38, N'USA', N'40.9', N'-80.08')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (243, N'Bellevue', 47, N'USA', N'47.6', N'-122.16')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (244, N'Apollo Beach', 21, N'USA', N'27.77', N'-82.41')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (245, N'Elgin', 13, N'USA', N'42.03', N'-88.3')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (246, N'Cherryville', 33, N'USA', N'35.39', N'-81.39')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (247, N'Grand Blanc', 22, N'USA', N'42.92', N'-83.65')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (248, N'Indiana', 38, N'USA', N'40.62', N'-79.15')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (249, N'Morrilton', 4, N'USA', N'35.17', N'-92.74')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (250, N'Sharon', 21, N'USA', N'42.11', N'-71.18')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (251, N'Pompton Lakes', 30, N'USA', N'41', N'-74.28')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (252, N'Eau Claire', 49, N'USA', N'44.79', N'-91.54')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (253, N'Saint Petersburg', 21, N'USA', N'27.63', N'-82.7')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (254, N'Mount Holly', 30, N'USA', N'39.99', N'-74.79')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (255, N'Atlanta', 42, N'USA', N'33.8', N'-84.39')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (256, N'Hopkins', 40, N'USA', N'33.91', N'-80.83')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (257, N'Paris', 4, N'USA', N'35.28', N'-93.72')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (258, N'Cary', 33, N'USA', N'35.78', N'-78.82')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (259, N'Southgate', 22, N'USA', N'42.21', N'-83.21')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (260, N'Viola', 4, N'USA', N'36.38', N'-92')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (261, N'Euclid', 35, N'USA', N'41.57', N'-81.53')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (262, N'Benton', 4, N'USA', N'34.59', N'-92.67')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (263, N'Rio Rancho', 31, N'USA', N'35.25', N'-106.72')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (264, N'Hutchinson', 16, N'USA', N'38.13', N'-97.93')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (265, N'Montgomery', 38, N'USA', N'41.18', N'-76.95')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (266, N'Colorado Springs', 6, N'USA', N'38.83', N'-104.74')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (267, N'Moriarty', 31, N'USA', N'34.96', N'-106')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (268, N'Vail', 6, N'USA', N'39.64', N'-106.32')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (269, N'Sandy', 37, N'USA', N'45.38', N'-122.18')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (270, N'Fennville', 22, N'USA', N'42.57', N'-86.11')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (271, N'Kearney', 140, N'USA', N'40.76', N'-99.02')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (272, N'Kittanning', 38, N'USA', N'40.81', N'-79.42')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (273, N'Ripley', 48, N'USA', N'38.79', N'-81.68')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (274, N'Norwood', 21, N'USA', N'42.18', N'-71.2')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (275, N'Alexandria', 46, N'USA', N'38.81', N'-77.13')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (276, N'Richmond', 46, N'USA', N'37.54', N'-77.48')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (277, N'Belton', 25, N'USA', N'38.78', N'-94.55')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (278, N'Laurel', 43, N'USA', N'31.7', N'-89.11')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (279, N'Lolo', 26, N'USA', N'46.73', N'-114.36')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (280, N'Peoria', 13, N'USA', N'40.69', N'-89.59')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (281, N'Iowa City', 21, N'USA', N'41.64', N'-91.46')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (282, N'Pawtucket', 39, N'USA', N'41.87', N'-71.39')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (283, N'Madison', 49, N'USA', N'43.07', N'-89.39')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (284, N'Toledo', 35, N'USA', N'41.71', N'-83.54')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (285, N'Woodbridge', 46, N'USA', N'38.65', N'-77.31')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (286, N'Rolla', 25, N'USA', N'37.93', N'-91.78')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (287, N'Shelby', 33, N'USA', N'35.36', N'-81.57')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (288, N'Carbondale', 6, N'USA', N'39.18', N'-107.23')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (289, N'Decatur', 42, N'USA', N'33.69', N'-84.25')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (290, N'Baltimore', 20, N'USA', N'39.34', N'-76.69')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (291, N'Kokomo', 30, N'USA', N'40.44', N'-86.09')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (292, N'Broken Arrow', 36, N'USA', N'36.06', N'-95.81')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (293, N'Raleigh', 33, N'USA', N'35.93', N'-78.72')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (294, N'Greenville', 40, N'USA', N'34.86', N'-82.25')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (295, N'Saint Paul', 23, N'USA', N'45.07', N'-93.19')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (296, N'Fort Mitchell', 1, N'USA', N'32.29', N'-84.97')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (297, N'Akron', 35, N'USA', N'41.07', N'-81.54')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (298, N'Stamford', 7, N'USA', N'41.08', N'-73.54')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (299, N'Phoenix', 3, N'USA', N'33.43', N'-112.2')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (300, N'New Palestine', 30, N'USA', N'39.73', N'-85.89')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (301, N'Caldwell', 35, N'USA', N'39.73', N'-81.51')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (302, N'Dundalk', 20, N'USA', N'39.26', N'-76.5')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (303, N'Minneapolis', 23, N'USA', N'45.03', N'-93.3')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (304, N'Brookings', 186, N'USA', N'44.33', N'-96.81')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (305, N'Las Cruces', 31, N'USA', N'32.35', N'-106.77')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (306, N'Las Vegas', 51, N'USA', N'36.08', N'-115.09')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (307, N'Hazelwood', 25, N'USA', N'38.79', N'-90.38')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (308, N'Moncks Corner', 40, N'USA', N'33.12', N'-80.04')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (309, N'Bothell', 47, N'USA', N'47.84', N'-122.2')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (310, N'Gilbert', 3, N'USA', N'33.32', N'-111.76')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (311, N'Sioux Falls', 186, N'USA', N'43.52', N'-96.73')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (312, N'Richmond', 17, N'USA', N'37.77', N'-84.3')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (313, N'Huntsville', 1, N'USA', N'34.64', N'-86.75')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (314, N'Carter Lake', 21, N'USA', N'41.29', N'-95.92')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (315, N'Tupelo', 43, N'USA', N'34.22', N'-88.77')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (316, N'Menomonie', 49, N'USA', N'44.85', N'-92')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (317, N'Weare', 29, N'USA', N'43.08', N'-71.72')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (318, N'Amite', 18, N'USA', N'30.73', N'-90.61')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (319, N'Norfolk', 46, N'USA', N'36.89', N'-76.27')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (320, N'Portland', 19, N'USA', N'43.69', N'-70.29')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (321, N'Honolulu', 11, N'USA', N'21.3', N'-157.79')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (322, N'Overland Park', 16, N'USA', N'38.92', N'-94.7')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (323, N'Williamston', 22, N'USA', N'42.68', N'-84.27')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (324, N'Tempe', 3, N'USA', N'33.44', N'-111.92')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (325, N'Kennewick', 47, N'USA', N'46.08', N'-119.09')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (326, N'Birmingham', 1, N'USA', N'33.51', N'-86.8')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (327, N'Coquille', 37, N'USA', N'43.2', N'-124.12')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (328, N'Gardnerville', 51, N'USA', N'38.92', N'-119.8')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (329, N'Salt Lake City', 44, N'USA', N'40.71', N'-111.86')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (330, N'Quitman', 18, N'USA', N'32.35', N'-92.72')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (331, N'Windsor', 7, N'USA', N'41.86', N'-72.68')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (332, N'Fort Wayne', 30, N'USA', N'40.98', N'-85.12')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (333, N'Moulton', 1, N'USA', N'34.46', N'-87.3')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (334, N'Sierra Vista', 3, N'USA', N'31.59', N'-110.17')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (335, N'Valparaiso', 30, N'USA', N'41.46', N'-87.14')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (336, N'Washington', 52, N'USA', N'38.91', N'-77.08')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (337, N'Gatlinburg', 42, N'USA', N'35.67', N'-83.46')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (338, N'Dover', 8, N'USA', N'39.16', N'-75.49')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (339, N'Lexington', 17, N'USA', N'37.99', N'-84.49')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (340, N'Olney', 20, N'USA', N'39.15', N'-77.08')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (341, N'Knoxville', 42, N'USA', N'36.06', N'-83.91')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (342, N'Fraziers Bottom', 48, N'USA', N'38.6', N'-82.03')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (343, N'Lawrence', 16, N'USA', N'39.04', N'-95.21')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (344, N'New Orleans', 18, N'USA', N'29.96', N'-90.08')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (345, N'Columbia', 25, N'USA', N'39.04', N'-92.27')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (346, N'Waukesha', 49, N'USA', N'42.94', N'-88.3')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (347, N'Middleton', 12, N'USA', N'43.74', N'-116.58')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (348, N'Lenexa', 16, N'USA', N'38.95', N'-94.75')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (349, N'Pahala', 11, N'USA', N'19.2', N'-155.5')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (350, N'Pocatello', 12, N'USA', N'42.91', N'-112.4')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (351, N'Vanceburg', 17, N'USA', N'38.51', N'-83.41')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (352, N'Bristol', 7, N'USA', N'41.68', N'-72.94')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (353, N'Saint Cloud', 23, N'USA', N'45.49', N'-94.23')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (354, N'Ewa Beach', 11, N'USA', N'21.35', N'-158.02')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (355, N'Tulsa', 36, N'USA', N'36.14', N'-95.94')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (356, N'Newalla', 36, N'USA', N'35.35', N'-97.2')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (357, N'Hillsboro', 37, N'USA', N'45.51', N'-122.94')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (358, N'Portland', 37, N'USA', N'45.59', N'-122.71')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (359, N'Keaau', 11, N'USA', N'19.58', N'-155.02')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (360, N'Brewer', 19, N'USA', N'44.78', N'-68.74')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (361, N'Henderson', 51, N'USA', N'36.03', N'-115.07')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (362, N'Circle Pines', 23, N'USA', N'45.17', N'-93.12')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (363, N'Dubois', 50, N'USA', N'43.49', N'-109.64')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (364, N'Wilmington', 8, N'USA', N'39.72', N'-75.53')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (365, N'Olive Branch', 43, N'USA', N'34.92', N'-89.82')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (366, N'Wheeling', 48, N'USA', N'40.06', N'-80.64')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (367, N'Raymond', 29, N'USA', N'43.03', N'-71.2')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (368, N'Nashua', 29, N'USA', N'42.73', N'-71.46')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (369, N'Hampstead', 29, N'USA', N'42.88', N'-71.17')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (370, N'Biddeford', 19, N'USA', N'43.5', N'-70.49')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (371, N'Fernley', 51, N'USA', N'39.67', N'-119.07')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (372, N'Baltic', 186, N'USA', N'43.74', N'-96.76')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (373, N'Berwick', 19, N'USA', N'43.3', N'-70.84')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (374, N'Norman', 36, N'USA', N'35.25', N'-97.46')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (375, N'Claymont', 8, N'USA', N'39.8', N'-75.46')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (376, N'Sandpoint', 12, N'USA', N'48.34', N'-116.45')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (377, N'Boise', 12, N'USA', N'43.55', N'-116.29')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (378, N'Laramie', 50, N'USA', N'41.43', N'-105.52')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (379, N'Jackson', 43, N'USA', N'32.33', N'-90.2')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (380, N'Santa Fe', 31, N'USA', N'35.68', N'-105.96')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (381, N'Ogden', 44, N'USA', N'41.22', N'-111.97')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (382, N'Fleet Post Office', 51, N'USA', N'57', N'-100')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (383, N'Orem', 44, N'USA', N'40.29', N'-111.72')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (384, N'Bismarck', 34, N'USA', N'46.82', N'-100.71')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (385, N'Lincoln', 140, N'USA', N'40.83', N'-96.67')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (386, N'Woonsocket', 186, N'USA', N'44.05', N'-98.3')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (387, N'Omaha', 140, N'USA', N'41.29', N'-96.17')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (388, N'Kalispell', 26, N'USA', N'48.2', N'-114.39')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (389, N'Beatrice', 140, N'USA', N'40.26', N'-96.71')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (390, N'Jackson', 50, N'USA', N'43.46', N'-110.51')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (391, N'Ferrisburg', 45, N'USA', N'44.21', N'-73.22')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (392, N'Scott Depot', 48, N'USA', N'38.45', N'-81.89')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (393, N'Westerly', 39, N'USA', N'41.36', N'-71.8')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (394, N'Camden Wyoming', 8, N'USA', N'39.08', N'-75.61')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (395, N'Eagle River', 2, N'USA', N'61.21', N'-149.26')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (396, N'Woonsocket', 39, N'USA', N'42', N'-71.49')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (397, N'Gillette', 50, N'USA', N'43.9', N'-105.55')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (398, N'Livingston', 26, N'USA', N'45.71', N'-110.54')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (399, N'Seward', 2, N'USA', N'60.06', N'-149.34')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (400, N'Providence', 39, N'USA', N'41.83', N'-71.4')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (401, N'Juneau', 2, N'USA', N'58.58', N'-134.77')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (402, N'Fairbanks', 2, N'USA', N'64.82', N'-147.72')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (403, N'Lyndonville', 45, N'USA', N'44.53', N'-72.05')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (404, N'Barre', 45, N'USA', N'44.18', N'-72.47')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (405, N'Devils Lake', 34, N'USA', N'48.14', N'-98.89')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (406, N'Shelburne', 45, N'USA', N'44.4', N'-73.21')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (407, N'West Fargo', 34, N'USA', N'46.89', N'-96.93')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (408, N'Fargo', 34, N'USA', N'46.93', N'-96.83')
GO
INSERT [dbo].[cities] ([ID], [cityName], [stateID], [countryID], [latitude], [longitude]) VALUES (409, N'Washington D.c', 52, N'USA', N'38.9', N'-77.04')
GO
SET IDENTITY_INSERT [dbo].[cities] OFF
GO
INSERT [dbo].[countries] ([countryID], [countryName], [latitude], [longitude]) VALUES (N'USA', N'United States', N'38', N'-97')
GO
SET IDENTITY_INSERT [dbo].[States] ON 

GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (1, N'AL', N'Alabama', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (2, N'AK', N'Alaska', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (3, N'AZ', N'Arizona', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (4, N'AR', N'Arkansas', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (5, N'CA', N'California', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (6, N'CO', N'Colorado', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (7, N'CT', N'Connecticut', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (8, N'DE', N'Delaware', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (9, N'FL', N'Florida', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (10, N'GA', N'Georgia', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (11, N'HI', N'Hawaii', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (12, N'ID', N'Idaho', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (13, N'IL', N'Illinois', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (14, N'IN', N'Indiana', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (15, N'IA', N'Iowa', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (16, N'KS', N'Kansas', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (17, N'KY', N'Kentucky', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (18, N'LA', N'Louisiana', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (19, N'ME', N'Maine', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (20, N'MD', N'Maryland', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (21, N'MA', N'Massachusetts', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (22, N'MI', N'Michigan', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (23, N'MN', N'Minnesota', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (24, N'MS', N'Mississippi', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (25, N'MO', N'Missouri', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (26, N'MT', N'Montana', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (27, N'NE', N'Nebraka', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (28, N'NV', N'Nevada', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (29, N'NH', N'New Hampshire', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (30, N'NJ', N'New Jersey', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (31, N'NM', N'New Mexico', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (32, N'NY', N'New york', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (33, N'NC', N'North Carolina', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (34, N'ND', N'North Dakota', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (35, N'OH', N'Ohio', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (36, N'OK', N'Oklahoma', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (37, N'OR', N'Oregon', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (38, N'PA', N'Pennsylvania', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (39, N'RI', N'Rhode Island', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (40, N'SC', N'South Carolina', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (41, N'SD', N'Dakota', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (42, N'TN', N'Tennessee', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (43, N'TX', N'Texas', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (44, N'UT', N'Utah', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (45, N'VT', N'Vermont', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (46, N'VA', N'Virginia', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (47, N'WA', N'Washington', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (48, N'WV', N'West Virginia', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (49, N'WI', N'Wisconsin', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (50, N'WY', N'Wyoming', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (51, N'AF', N'Armed Forces US', NULL)
GO
INSERT [dbo].[States] ([ID], [Name], [StateName], [Country]) VALUES (52, N'DC', N'District of Columbia', NULL)
GO
SET IDENTITY_INSERT [dbo].[States] OFF
GO



CREATE PROCEDURE [dbo].[GetCitiesByStateId]  
@StateId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 
Select C.ID,C.cityName As CityName from Cities C where stateid=@StateId

  END
 


 


GO


Alter table Athelete add CityId int



 


  
  
ALTER PROCEDURE [dbo].[AddAthlete2_v2] 
	@Name varchar(100),
	@Gender int,
	@School varchar(100) ,
	@Emailaddress varchar(100),
	@Description varchar(1000),
	@GPA float,
	@Social int,
	@SAT int,
	@ACT int,
	@InstituteId int,
	@ProfilePicURL varchar(1000),
	@DOB varchar(50),
    @Address varchar(100),
	@FacebookId varchar(500), 
	@Latitude varchar(50),
	@Longitude varchar(50),
	@AcademicLevel int,
    @YearLevel int,
	@Premium int,
	@Height1 varchar(20),
	@Height2 varchar(20),
	@HeightType varchar(20),
	@Weight varchar(20),
	@WeightType varchar(20),
	@WingSpan varchar(20),
	@WingSpanType varchar(20),
	@ShoeSize varchar(20),
	@ShoeSizeType varchar(20),
	@ClubName varchar(100),
	@CoachName varchar(100),
	@CoachEmailId varchar(100),
	@AtheleteType int,
	@Major varchar(100),
	@Active bit,
	@Phonenumber varchar(50) = NULL,
	@Username varchar(500),
	@LoginType int,
	@DeviceType varchar(50),
	@DeviceId varchar(500),
	@VerticalJump varchar(50),
	@BroadJump varchar(20),
	@StateId int,
	@SchoolCoachName varchar(100),
    @SchoolCoachEmail varchar(100),
	@SchoolZipCode varchar(100), 
	@GapyearLevel int,
	@GapyearDescription varchar(2000),
	@Country varchar(100),
	@CountryId int,
	@AP float,
	@20YardShuttleRun varchar(50),
	@60YardShuttleRun varchar(50),
	@KneelingMedBallToss varchar(50),
	@RotationalMedBallToss varchar(50),
	@CityId int,
	@Id int Output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	 declare @cnt int
	 declare @avgRating int
	 declare @athletId int
	 declare @getDeviceId int 
	 declare @devicecnt int
	 declare @ClubId int 
     declare @isProfileCompleted Bit
	 declare @isAcademicProfileCompleted Bit

	 If(@ClubName != '')
	 begin
		 select @ClubId = Id from Clubs where Name =  @ClubName
		 If(isnull(@ClubId,0) = 0)
		 Begin
		   Insert into Clubs(Name,CreatedDate,CreatedBy)
		   values(@ClubName,getutcdate(),2)
		   set @ClubId = @@Identity
		 End
	 End
	 Else
	   set @ClubId = 0 

	If(@Country != '')
	 begin
		 select @CountryId = Id from Country where Name =  @Country
		 If(isnull(@CountryId,0) = 0)
		 Begin
		   Insert into Country(Name)
		   values(@Country)
		   set @CountryId = @@Identity
		 End
	 End
	 Else
	   set @CountryId = 0 


	 if(isnull(@LoginType,1) = 1)
	 Begin
	 select @cnt = count(*) from Athelete where Emailaddress = @Emailaddress and logintype = @LoginType
	 End
	 Else
	 Begin
	  select @cnt = count(*) from Athelete where Username = @Username  and logintype = @LoginType
	 End 

	 If(isnull(@DeviceId,'') != '')
	 Begin
		select @devicecnt = count(*) from Device where  DeviceId = @DeviceId and  DeviceType = @DeviceType
		 if(@devicecnt = 0)
		 Begin
			insert into Device(DeviceID,DeviceType,CreateDate) 
			values (@DeviceId,@DeviceType,getutcdate())
			set @getDeviceId = @@Identity
		 End
		 else
		 begin
		  set @getDeviceId = (select Id from Device where DeviceID = @DeviceId and DeviceType=@DeviceType)
		 end
	 End
	 Else
	  set @getDeviceId = 0

	 
	 if(@cnt = 0)
	 Begin
		insert into Athelete (Name,Gender,School,Emailaddress,Description,GPA,Social,SAT,ACT,InstituteId,
		  ProfilePicURL,DOB,Address,FacebookId,[CreatedDate],Latitude,Longitude,AcademicLevel,YearLevel,Premium,
		  Height, Height2, HeightType,Weight, WeightType, WingSpan, WingSpanType, ShoeSize, ShoeSizeType,
		     AtheleteType,Active, Major,Phonenumber,Username,LoginType,DeviceId,
		   VerticalJump,BroadJump,StateId,SchoolCoachName,SchoolCoachEmail,SchoolZipCode,
		   GapyearLevel,GapyearDescription,Country,CountryId,AP,AthleteTransition,
		   [20YardShuttleRun],[60YardShuttleRun],[KneelingMedBallToss],[RotationalMedBallToss],CityId) 
		values (@Name,@Gender,@School,@Emailaddress,@Description,@GPA,@Social,@SAT,@ACT,@InstituteId,
	      @ProfilePicURL,@DOB,@Address,@FacebookId,GETUTCDATE(),@Latitude,@Longitude,@AcademicLevel,@YearLevel,@Premium,
		   @Height1,@Height2,@HeightType,@Weight,@WeightType,@WingSpan,@WingSpanType,@ShoeSize,@ShoeSizeType,
		    @AtheleteType,@Active, @Major,@Phonenumber,@Username,@LoginType,@getDeviceId,
			 @VerticalJump,@BroadJump,@StateId,@SchoolCoachName,@SchoolCoachEmail,@SchoolZipCode,
			@GapyearLevel,@GapyearDescription,@Country,@CountryId,@AP,0,
			@20YardShuttleRun,@60YardShuttleRun,@KneelingMedBallToss,@RotationalMedBallToss,@CityId)
		set @Id = @@Identity 
		  if(isnull(@ClubName,'') != '')
		  Begin
		   Update Athelete set ClubName = @ClubName, ClubId=@ClubId,
		   CoachName=@CoachName, CoachEmailId=@CoachEmailId
		    where Id = @Id
		  End 
	 End
	 Else
	 Begin
	  if(isnull(@LoginType,1) = 1)
	  begin
	 	Update Athelete set Name=@Name,Gender=@Gender,School=@School,Emailaddress=@Emailaddress,
		Description=@Description,GPA=@GPA,Social=@Social,SAT=@SAT,ACT=@ACT,InstituteId=@InstituteId,
		  ProfilePicURL=@ProfilePicURL,DOB=@DOB,Address=@Address,FacebookId=@FacebookId,
		  Latitude = @Latitude,Longitude = @Longitude,AcademicLevel=@AcademicLevel,YearLevel=@YearLevel,
		  Premium=@Premium,Height=@Height1,Height2= @Height2, HeightType = @HeightType,Weight=@Weight,WeightType=@WeightType,
		  WingSpan=@WingSpan,WingSpanType=@WingSpanType,ShoeSize=@ShoeSize,ShoeSizeType=@ShoeSizeType, 
		  AtheleteType = @AtheleteType, Active=@Active, Major = @Major,
		  Phonenumber = @Phonenumber,  Username=@Username, LoginType = @LoginType ,DeviceId = @getDeviceId,
		  VerticalJump=@VerticalJump,BroadJump=@BroadJump,StateId=@StateId,SchoolCoachName=@SchoolCoachName,
		  SchoolCoachEmail=@SchoolCoachEmail,SchoolZipCode=@SchoolZipCode,
		  GapyearLevel=@GapyearLevel,GapyearDescription=@GapyearDescription,Country=@Country,CountryId=@CountryId,AP=@AP,
		   [20YardShuttleRun]=@20YardShuttleRun,[60YardShuttleRun]=@60YardShuttleRun,
		   [KneelingMedBallToss]=@KneelingMedBallToss,[RotationalMedBallToss]=@RotationalMedBallToss,
		   CityId=@CityId
	      where Emailaddress=@Emailaddress and logintype = @LoginType
		  
		  if(isnull(@ClubName,'') != '')
		  Begin
		   Update Athelete set ClubName = @ClubName, ClubId=@ClubId,
		   CoachName=@CoachName, CoachEmailId=@CoachEmailId
		   where Emailaddress=@Emailaddress and logintype = @LoginType
		  End 

	 end
	 else
	 begin
	   	Update Athelete set Name=@Name,Gender=@Gender,School=@School,Emailaddress=@Emailaddress,
		Description=@Description,GPA=@GPA,Social=@Social,SAT=@SAT,ACT=@ACT,InstituteId=@InstituteId,
		  ProfilePicURL=@ProfilePicURL,DOB=@DOB,Address=@Address,FacebookId=@FacebookId,
		  Latitude = @Latitude,Longitude = @Longitude,AcademicLevel=@AcademicLevel,YearLevel=@YearLevel,
		  Premium=@Premium,Height=@Height1,Height2= @Height2, HeightType = @HeightType,Weight=@Weight,WeightType=@WeightType,
		  WingSpan=@WingSpan,WingSpanType=@WingSpanType,ShoeSize=@ShoeSize,ShoeSizeType=@ShoeSizeType, 
		  AtheleteType = @AtheleteType, Active=@Active, Major = @Major,
		  Phonenumber = @Phonenumber,  Username=@Username, LoginType = @LoginType ,DeviceId = @getDeviceId,
		  VerticalJump=@VerticalJump,BroadJump=@BroadJump,StateId=@StateId,SchoolCoachName=@SchoolCoachName,
		  SchoolCoachEmail=@SchoolCoachEmail,SchoolZipCode=@SchoolZipCode,
		  GapyearLevel=@GapyearLevel,GapyearDescription=@GapyearDescription,Country=@Country,CountryId=@CountryId,AP=@AP,
		  [20YardShuttleRun]=@20YardShuttleRun,[60YardShuttleRun]=@60YardShuttleRun,
		   [KneelingMedBallToss]=@KneelingMedBallToss,[RotationalMedBallToss]=@RotationalMedBallToss,
		   CityId=@CityId
	      where Username=@Username and logintype = @LoginType
		  
		  if(isnull(@ClubName,'') != '')
		  Begin
		   Update Athelete set ClubName = @ClubName, ClubId=@ClubId, 
		      CoachName=@CoachName, CoachEmailId=@CoachEmailId
		   where  Username=@Username and logintype = @LoginType
		  End 
	 end 
	End

    if(isnull(@LoginType,1) = 1)
	begin
	     set @Id = (select id from Athelete where Emailaddress=@Emailaddress and logintype = @LoginType)
	end
	else
	begin
	    set @Id = (select id from Athelete where Username = @Username and logintype = @LoginType)
	end

	    exec  SPGetAtheleteAverageRating @Id  
		 Exec @isProfileCompleted = fnAthleteProfileCompleted @Id  
		  Exec @isAcademicProfileCompleted = fnAthleteAcademicProfileCompleted @Id 

		--UsageTracking on Athlete Academic Profile Completed
		if(@isAcademicProfileCompleted = 1)
		Begin
			EXEC [UsersUsageTracking] 5,1,@Id,@DeviceId,@DeviceType
		End
		 
	  select  a.*,ab.AtheleteId,ab.AllAmerican,ab.AcademicAllAmerican,ab.Captain,ab.SwimswamTop,ab.status
	  ,I.Name as UniversityName,I.Address as UnivAddress, I.ClassificationName,I.EmailAddress as UnivEmailAddress,
	  I.ProfilePicURL as UnivProfilePic, I.PhoneNumber as UnviPhoneNumber,I.Size as UnivSize,s.name as StateName
	  ,@isProfileCompleted as  isProfileCompleted
	   from athelete a
	  left join AtheleteBadges ab on ab.AtheleteId = a.Id 
	  left join Institution I on I.Id = a.InstituteId
	  left join states s on a.stateid = s.id
	  where a.Id = @Id 
		 
END

  
 



 





























GO


Alter table Athelete add BasicStateId int



CREATE PROCEDURE [dbo].[GetMutualLikesCount]  
(
 @UserId int,
 @UserType varchar(20)
)
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 if(@UserType = 'HS')
 Begin
		select   count(*) as TotalCount from atheleteinstitutematches aim 
		inner join  instituteatheletematches iam on aim.instituteid=iam.instituteid 
		and aim.atheleteid=iam.atheleteid
		group by aim.atheleteid, aim.collatheletecoachtype , aim.status,iam.atheleteid,
		iam.collatheletecoachtype,iam.Status
		having aim.atheleteid=@UserId and aim.collatheletecoachtype  = 1 and aim.status=1
		and iam.collatheletecoachtype  = 1 and iam.status=1 

 End
 Else
 Begin
		select  aim.coachid,  count(*) as TotalCount from atheleteinstitutematches aim 
		inner join  instituteatheletematches iam on aim.instituteid=iam.instituteid 
		and aim.atheleteid=iam.atheleteid
		group by aim.atheleteid, aim.collatheletecoachtype , aim.status,iam.atheleteid,
		iam.collatheletecoachtype,iam.Status,aim.coachid
		having aim.coachid=@UserId and aim.collatheletecoachtype  = 1 and aim.status=1
		and iam.collatheletecoachtype  = 1 and iam.status=1

 End
END

GO



CREATE PROCEDURE [dbo].[GetNotificationsCount] -- [GetNotificationsCount]96
(
 @UserId int 
)
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 declare @totalCount int
 DECLARE @Now DATETIME = GETDATE();
 DECLARE @7DaysAgo DATETIME = DATEADD(day,-7,@Now);

 select @totalCount = count(*) from notifications n 
 Where n.UserId = @UserId and CreatedDate BETWEEN @7DaysAgo AND @Now 
  
 select @totalCount as [Count]
END

GO

 

 
 
ALTER PROCEDURE [dbo].[GetMutualLikesCount]  
(
 @UserId int,
 @UserType varchar(20)
)
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 declare @cnt int
 if(@UserType = 'HS')
 Begin
		select   @cnt = count(*)   from atheleteinstitutematches aim 
		inner join  instituteatheletematches iam on aim.instituteid=iam.instituteid 
		and aim.atheleteid=iam.atheleteid
		group by aim.atheleteid, aim.collatheletecoachtype , aim.status,iam.atheleteid,
		iam.collatheletecoachtype,iam.Status
		having aim.atheleteid=@UserId and aim.collatheletecoachtype  = 1 and aim.status=1
		and iam.collatheletecoachtype  = 1 and iam.status=1 

 End
 Else
 Begin
		select  @cnt = count(*)  from atheleteinstitutematches aim 
		inner join  instituteatheletematches iam on aim.instituteid=iam.instituteid 
		and aim.atheleteid=iam.atheleteid
		group by aim.atheleteid, aim.collatheletecoachtype , aim.status,iam.atheleteid,
		iam.collatheletecoachtype,iam.Status,aim.coachid
		having aim.coachid=@UserId and aim.collatheletecoachtype  = 1 and aim.status=1
		and iam.collatheletecoachtype  = 1 and iam.status=1

 End

 select isnull(@cnt,0) as TotalCount
END

GO



 

 
ALTER PROCEDURE [dbo].[GetAtheleteProfile_v4] --[GetAtheleteProfile_v4] 'maheshpenmatsa',3
@emailaddress varchar(100),
@type int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 

  declare @Id int
  declare @isProfileCompleted Bit
  
  If(@type = 1)
  Begin
      select @Id = Id  from athelete where emailaddress = @emailaddress 
  End
  Else
  Begin
     select @Id = Id  from athelete where Username = @emailaddress  
  End

  Exec @isProfileCompleted = fnAthleteProfileCompleted @Id 

  Exec DeleteAthleteMediaAfterOneMonth  @Id
   
      select  a.*,ab.*,I.Name as UniversityName,I.Address as UnivAddress, I.ClassificationName,I.EmailAddress as UnivEmailAddress,
	  I.ProfilePicURL as UnivProfilePic, I.PhoneNumber as UnviPhoneNumber,I.Size as UnivSize,
	   s.name as StateName,@isProfileCompleted as  isProfileCompleted,d.DeviceId as DeviceName,d.DeviceType,
	   s1.name as BasicStateName,c.cityname
	   from athelete a
	  left join AtheleteBadges ab on ab.AtheleteId = a.Id 
	  left join Institution I on I.Id = a.InstituteId
	  left join device d on d.id  = a.deviceId
	  left join states s on a.stateid = s.id
	  left join states s1 on a.basicstateid = s1.id
	  left join Cities c on c.id = a.CityID
	  where a.Id = @Id 
  
  END
 
 








GO


  



  
  
ALTER PROCEDURE [dbo].[AddAthlete2_v2] 
	@Name varchar(100),
	@Gender int,
	@School varchar(100) ,
	@Emailaddress varchar(100),
	@Description varchar(1000),
	@GPA float,
	@Social int,
	@SAT int,
	@ACT int,
	@InstituteId int,
	@ProfilePicURL varchar(1000),
	@DOB varchar(50),
    @Address varchar(100),
	@FacebookId varchar(500), 
	@Latitude varchar(50),
	@Longitude varchar(50),
	@AcademicLevel int,
    @YearLevel int,
	@Premium int,
	@Height1 varchar(20),
	@Height2 varchar(20),
	@HeightType varchar(20),
	@Weight varchar(20),
	@WeightType varchar(20),
	@WingSpan varchar(20),
	@WingSpanType varchar(20),
	@ShoeSize varchar(20),
	@ShoeSizeType varchar(20),
	@ClubName varchar(100),
	@CoachName varchar(100),
	@CoachEmailId varchar(100),
	@AtheleteType int,
	@Major varchar(100),
	@Active bit,
	@Phonenumber varchar(50) = NULL,
	@Username varchar(500),
	@LoginType int,
	@DeviceType varchar(50),
	@DeviceId varchar(500),
	@VerticalJump varchar(50),
	@BroadJump varchar(20),
	@StateId int,
	@SchoolCoachName varchar(100),
    @SchoolCoachEmail varchar(100),
	@SchoolZipCode varchar(100), 
	@GapyearLevel int,
	@GapyearDescription varchar(2000),
	@Country varchar(100),
	@CountryId int,
	@AP float,
	@20YardShuttleRun varchar(50),
	@60YardShuttleRun varchar(50),
	@KneelingMedBallToss varchar(50),
	@RotationalMedBallToss varchar(50),
	@CityId int,
	@BasicStateId int,
	@Id int Output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	 declare @cnt int
	 declare @avgRating int
	 declare @athletId int
	 declare @getDeviceId int 
	 declare @devicecnt int
	 declare @ClubId int 
     declare @isProfileCompleted Bit
	 declare @isAcademicProfileCompleted Bit

	 If(@ClubName != '')
	 begin
		 select @ClubId = Id from Clubs where Name =  @ClubName
		 If(isnull(@ClubId,0) = 0)
		 Begin
		   Insert into Clubs(Name,CreatedDate,CreatedBy)
		   values(@ClubName,getutcdate(),2)
		   set @ClubId = @@Identity
		 End
	 End
	 Else
	   set @ClubId = 0 

	If(@Country != '')
	 begin
		 select @CountryId = Id from Country where Name =  @Country
		 If(isnull(@CountryId,0) = 0)
		 Begin
		   Insert into Country(Name)
		   values(@Country)
		   set @CountryId = @@Identity
		 End
	 End
	 Else
	   set @CountryId = 0 


	 if(isnull(@LoginType,1) = 1)
	 Begin
	 select @cnt = count(*) from Athelete where Emailaddress = @Emailaddress and logintype = @LoginType
	 End
	 Else
	 Begin
	  select @cnt = count(*) from Athelete where Username = @Username  and logintype = @LoginType
	 End 

	 If(isnull(@DeviceId,'') != '')
	 Begin
		select @devicecnt = count(*) from Device where  DeviceId = @DeviceId and  DeviceType = @DeviceType
		 if(@devicecnt = 0)
		 Begin
			insert into Device(DeviceID,DeviceType,CreateDate) 
			values (@DeviceId,@DeviceType,getutcdate())
			set @getDeviceId = @@Identity
		 End
		 else
		 begin
		  set @getDeviceId = (select Id from Device where DeviceID = @DeviceId and DeviceType=@DeviceType)
		 end
	 End
	 Else
	  set @getDeviceId = 0

	 
	 if(@cnt = 0)
	 Begin
		insert into Athelete (Name,Gender,School,Emailaddress,Description,GPA,Social,SAT,ACT,InstituteId,
		  ProfilePicURL,DOB,Address,FacebookId,[CreatedDate],Latitude,Longitude,AcademicLevel,YearLevel,Premium,
		  Height, Height2, HeightType,Weight, WeightType, WingSpan, WingSpanType, ShoeSize, ShoeSizeType,
		     AtheleteType,Active, Major,Phonenumber,Username,LoginType,DeviceId,
		   VerticalJump,BroadJump,StateId,SchoolCoachName,SchoolCoachEmail,SchoolZipCode,
		   GapyearLevel,GapyearDescription,Country,CountryId,AP,AthleteTransition,
		   [20YardShuttleRun],[60YardShuttleRun],[KneelingMedBallToss],[RotationalMedBallToss],CityId,
		   BasicStateId) 
		values (@Name,@Gender,@School,@Emailaddress,@Description,@GPA,@Social,@SAT,@ACT,@InstituteId,
	      @ProfilePicURL,@DOB,@Address,@FacebookId,GETUTCDATE(),@Latitude,@Longitude,@AcademicLevel,@YearLevel,@Premium,
		   @Height1,@Height2,@HeightType,@Weight,@WeightType,@WingSpan,@WingSpanType,@ShoeSize,@ShoeSizeType,
		    @AtheleteType,@Active, @Major,@Phonenumber,@Username,@LoginType,@getDeviceId,
			 @VerticalJump,@BroadJump,@StateId,@SchoolCoachName,@SchoolCoachEmail,@SchoolZipCode,
			@GapyearLevel,@GapyearDescription,@Country,@CountryId,@AP,0,
			@20YardShuttleRun,@60YardShuttleRun,@KneelingMedBallToss,@RotationalMedBallToss,@CityId,
			@BasicStateId)
		set @Id = @@Identity 
		  if(isnull(@ClubName,'') != '')
		  Begin
		   Update Athelete set ClubName = @ClubName, ClubId=@ClubId,
		   CoachName=@CoachName, CoachEmailId=@CoachEmailId
		    where Id = @Id
		  End 
	 End
	 Else
	 Begin
	  if(isnull(@LoginType,1) = 1)
	  begin
	 	Update Athelete set Name=@Name,Gender=@Gender,School=@School,Emailaddress=@Emailaddress,
		Description=@Description,GPA=@GPA,Social=@Social,SAT=@SAT,ACT=@ACT,InstituteId=@InstituteId,
		  ProfilePicURL=@ProfilePicURL,DOB=@DOB,Address=@Address,FacebookId=@FacebookId,
		  Latitude = @Latitude,Longitude = @Longitude,AcademicLevel=@AcademicLevel,YearLevel=@YearLevel,
		  Premium=@Premium,Height=@Height1,Height2= @Height2, HeightType = @HeightType,Weight=@Weight,WeightType=@WeightType,
		  WingSpan=@WingSpan,WingSpanType=@WingSpanType,ShoeSize=@ShoeSize,ShoeSizeType=@ShoeSizeType, 
		  AtheleteType = @AtheleteType, Active=@Active, Major = @Major,
		  Phonenumber = @Phonenumber,  Username=@Username, LoginType = @LoginType ,DeviceId = @getDeviceId,
		  VerticalJump=@VerticalJump,BroadJump=@BroadJump,StateId=@StateId,SchoolCoachName=@SchoolCoachName,
		  SchoolCoachEmail=@SchoolCoachEmail,SchoolZipCode=@SchoolZipCode,
		  GapyearLevel=@GapyearLevel,GapyearDescription=@GapyearDescription,Country=@Country,CountryId=@CountryId,AP=@AP,
		   [20YardShuttleRun]=@20YardShuttleRun,[60YardShuttleRun]=@60YardShuttleRun,
		   [KneelingMedBallToss]=@KneelingMedBallToss,[RotationalMedBallToss]=@RotationalMedBallToss,
		   CityId=@CityId,BasicStateId=@BasicStateId
	      where Emailaddress=@Emailaddress and logintype = @LoginType
		  
		  if(isnull(@ClubName,'') != '')
		  Begin
		   Update Athelete set ClubName = @ClubName, ClubId=@ClubId,
		   CoachName=@CoachName, CoachEmailId=@CoachEmailId
		   where Emailaddress=@Emailaddress and logintype = @LoginType
		  End 

	 end
	 else
	 begin
	   	Update Athelete set Name=@Name,Gender=@Gender,School=@School,Emailaddress=@Emailaddress,
		Description=@Description,GPA=@GPA,Social=@Social,SAT=@SAT,ACT=@ACT,InstituteId=@InstituteId,
		  ProfilePicURL=@ProfilePicURL,DOB=@DOB,Address=@Address,FacebookId=@FacebookId,
		  Latitude = @Latitude,Longitude = @Longitude,AcademicLevel=@AcademicLevel,YearLevel=@YearLevel,
		  Premium=@Premium,Height=@Height1,Height2= @Height2, HeightType = @HeightType,Weight=@Weight,WeightType=@WeightType,
		  WingSpan=@WingSpan,WingSpanType=@WingSpanType,ShoeSize=@ShoeSize,ShoeSizeType=@ShoeSizeType, 
		  AtheleteType = @AtheleteType, Active=@Active, Major = @Major,
		  Phonenumber = @Phonenumber,  Username=@Username, LoginType = @LoginType ,DeviceId = @getDeviceId,
		  VerticalJump=@VerticalJump,BroadJump=@BroadJump,StateId=@StateId,SchoolCoachName=@SchoolCoachName,
		  SchoolCoachEmail=@SchoolCoachEmail,SchoolZipCode=@SchoolZipCode,
		  GapyearLevel=@GapyearLevel,GapyearDescription=@GapyearDescription,Country=@Country,CountryId=@CountryId,AP=@AP,
		  [20YardShuttleRun]=@20YardShuttleRun,[60YardShuttleRun]=@60YardShuttleRun,
		   [KneelingMedBallToss]=@KneelingMedBallToss,[RotationalMedBallToss]=@RotationalMedBallToss,
		   CityId=@CityId,BasicStateId=@BasicStateId
	      where Username=@Username and logintype = @LoginType
		  
		  if(isnull(@ClubName,'') != '')
		  Begin
		   Update Athelete set ClubName = @ClubName, ClubId=@ClubId, 
		      CoachName=@CoachName, CoachEmailId=@CoachEmailId
		   where  Username=@Username and logintype = @LoginType
		  End 
	 end 
	End

    if(isnull(@LoginType,1) = 1)
	begin
	     set @Id = (select id from Athelete where Emailaddress=@Emailaddress and logintype = @LoginType)
	end
	else
	begin
	    set @Id = (select id from Athelete where Username = @Username and logintype = @LoginType)
	end

	    exec  SPGetAtheleteAverageRating @Id  
		 Exec @isProfileCompleted = fnAthleteProfileCompleted @Id  
		  Exec @isAcademicProfileCompleted = fnAthleteAcademicProfileCompleted @Id 

		--UsageTracking on Athlete Academic Profile Completed
		if(@isAcademicProfileCompleted = 1)
		Begin
			EXEC [UsersUsageTracking] 5,1,@Id,@DeviceId,@DeviceType
		End
		 
	  select  a.*,ab.AtheleteId,ab.AllAmerican,ab.AcademicAllAmerican,ab.Captain,ab.SwimswamTop,ab.status
	  ,I.Name as UniversityName,I.Address as UnivAddress, I.ClassificationName,I.EmailAddress as UnivEmailAddress,
	  I.ProfilePicURL as UnivProfilePic, I.PhoneNumber as UnviPhoneNumber,I.Size as UnivSize,s.name as StateName
	  ,@isProfileCompleted as  isProfileCompleted,
	   s1.name as BasicStateName,c.cityname
	   from athelete a
	  left join AtheleteBadges ab on ab.AtheleteId = a.Id 
	  left join Institution I on I.Id = a.InstituteId
	  left join states s on a.stateid = s.id
	  left join states s1 on a.basicstateid = s1.id
	  left join Cities c on c.id = a.CityID
	  where a.Id = @Id 
		 
END 


GO



ALTER PROCEDURE [dbo].[GetMutualSeniorInstituteAtheletes]
	@InstituteId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select distinct A.* from Athelete A
	Inner join AtheleteInstituteMatches AIM on A.Id = AIM.AtheleteId and AIM.Status = 1
	Inner join InstituteAtheleteMatches IAM on A.Id = IAM.AtheleteId and IAM.Status = 1
	where AIM.InstituteId = @InstituteId and AIM.InstituteId = IAM.instituteId
	--and A.YearLevel = 4 
	and A.AtheleteType = 0
  

END

 


GO



CREATE TABLE [dbo].[NotificationsLastVisited](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[SenderType] [int] NULL,
	[LastVisitedDate] [datetime] NULL, 
	[CreatedDate] [datetime] NULL
)

GO

 


ALTER PROCEDURE [dbo].[GetNotificationsCount] -- [GetNotificationsCount]5
(
 @UserId int 
)
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 	  Begin Try
		Begin Tran
			 declare @totalCount int 
			 DECLARE @lastVisitedDate DATETIME  
			 Declare @chatvisitedCnt int
			 select @lastVisitedDate = LastVisitedDate From NotificationsLastVisited 

			 If(isnull(@lastVisitedDate,'') = '')
			 Begin
				select @totalCount = count(*) from notifications n 
				Where n.UserId = @UserId  
			 End
			 Else
			 Begin
			 select @totalCount = count(*) from notifications n 
			 Where n.UserId = @UserId and CreatedDate >= @lastVisitedDate
			 End
 
					  select @chatvisitedCnt = count(*) from NotificationsLastVisited where UserId = @UserId 
						 If(@chatvisitedCnt = 0)
						 Begin
							 Insert into NotificationsLastVisited(UserId,LastVisitedDate,[CreatedDate])
							 values(@UserId,getutcdate(),getutcdate())
						 End
						 Else
						 Begin
						   Update NotificationsLastVisited set [LastVisitedDate] = getutcdate() 
						   Where UserId = @UserId
						 End 

			 select @totalCount as [Count]
	      Commit Tran
	End Try
	Begin catch
	 select -1 as [Count]
	 Rollback Tran
	End Catch
END


GO



 


 

ALTER PROCEDURE [dbo].[GetCoachExists]   --[GetCoachExists] 'bindu.bhargavi@gmail.com','123456'
	@Emailaddress varchar(100),
	@Password varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;  
	    
		   select i.*,1 as TotalCount ,c.Emailaddress as CoachEmail , c.FacebookId as CoachFacebook ,
		   c.id as CoachId,c.Active,c.HeadCoach,c.PreferredStrokes,c.AcademicRate,c.Payment_Paid
		    ,d.DeviceId,d.DeviceType
		   from Institution i
		   inner join Coaches c on c.instituteid = i.id
		    left join Device d on d.Id = c.DeviceId
		   Where c.Emailaddress = @Emailaddress and password = @Password 
	 

END
 
GO


Alter table Payment add SKUID varchar(100)

 




ALTER PROCEDURE [dbo].[AddPayment2_v1]  
	@UserID int,
	@UserType int, -- 1 Athlete , 2 - Coach
	@PaymentToken [varchar](max),
	@DeviceType [varchar](50),
	@PaymentType [varchar](50), 
	@SubscriptionType varchar(20),
	@Emailaddress varchar(100),
	@SKUId  varchar(100)
 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
	declare @cnt int
	declare @ExpiryDate datetime
	declare @ExistingExpiryDate datetime

	if(@SubscriptionType = 'Y')
	Begin
	  set @ExpiryDate = DATEADD(year,1,GETUTCDATE())
	End
	Else if(@SubscriptionType = 'M')
	Begin 
	    set @ExpiryDate = DATEADD(month,1,GETUTCDATE())
	End
	Else
	Begin
	     set @ExpiryDate = '01/01/1900'
	End

	If(@UserType  = 1)
	Begin
	   select @UserID = Id from Athelete where emailaddress =  @Emailaddress or username = @Emailaddress
	End
	Else
	Begin
	    select @UserID = Id from Coaches where emailaddress =  @Emailaddress  
	End
	select @cnt = count(*) from Payment where UserID = @UserID and UserType=@UserType
  if(@cnt = 0)
	Begin 
			insert into Payment (UserID, UserType, PaymentToken, DeviceType, PaymentType, CreatedDate,ExpiryDate,SubscriptionType,SKUId) 
			values (@UserID,@UserType,@PaymentToken, @DeviceType, @PaymentType, GETUTCDATE(),@ExpiryDate,@SubscriptionType,@SKUId)
			set @Id = @@Identity
		   
			If(@UserType = 1)
			Begin
			 Update Athelete set Premium = 1 where Id = @UserID
			End
			else
			Begin 
			  Update coaches set payment_paid = 1 where Id = @UserID
			End
		 
	End
	Else
	Begin
	  select @Id  = id from Payment where UserID = @UserID and UserType=@UserType  
	  select @ExistingExpiryDate = ExpiryDate from Payment where Id = @Id
	  if(@ExpiryDate > isnull(@ExistingExpiryDate,'01/01/1900'))
	  Begin
	    Update Payment set PaymentToken=@PaymentToken , DeviceType=@DeviceType, PaymentType=@PaymentType, 
	    CreatedDate=GETUTCDATE(),ExpiryDate= @ExpiryDate,SubscriptionType=@SubscriptionType,SKUId=@SKUId
	    where Id = @Id
		set @Id = @Id
			If(@UserType = 1)
			Begin
			 Update Athelete set Premium = 1 where Id = @UserID
			End
			else
			Begin 
			  Update coaches set payment_paid = 1 where Id = @UserID
			End
	   End
	End

	select @Id as Id
END
 


GO


 

ALTER PROCEDURE [dbo].[GetNotificationsCount] -- [GetNotificationsCount] 337
(
 @UserId int 
)
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 	  Begin Try
		Begin Tran
			 declare @totalCount int 
			 DECLARE @lastVisitedDate DATETIME  
			
			 select @lastVisitedDate = LastVisitedDate From NotificationsLastVisited where UserId = @UserId  

			 If(isnull(@lastVisitedDate,'') = '')
			 Begin
				select @totalCount = count(*) from notifications n 
				Where n.UserId = @UserId  
			 End
			 Else
			 Begin
			 select @totalCount = count(*) from notifications n 
			 Where n.UserId = @UserId and CreatedDate >= @lastVisitedDate
			 End
  
			 select @totalCount as [Count]
	      Commit Tran
	End Try
	Begin catch
	 select -1 as [Count]
	 Rollback Tran
	End Catch
END




GO



 
ALTER PROCEDURE [dbo].[GetNotifications] -- [GetNotifications] 18,0,10
(
 @UserId int,
 @offset int,
 @max int
)
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 declare @totalCount int
  Declare @chatvisitedCnt int

select @chatvisitedCnt = count(*) from NotificationsLastVisited where UserId = @UserId 
If(@chatvisitedCnt = 0)
Begin
	Insert into NotificationsLastVisited(UserId,LastVisitedDate,[CreatedDate])
	values(@UserId,getutcdate(),getutcdate())
End
Else
Begin
Update NotificationsLastVisited set [LastVisitedDate] = getutcdate() 
Where UserId = @UserId
End 


    select @totalCount = count(*) from notifications n
 inner join notificationsmaster nm on n.NotificationId = nm.Id 
 Where n.UserId = @UserId

 select  n.*,nm.Message,nm.NotificationType,@totalCount as TotalCount,
 nm.ShortMessage as ShortMessage1,nm.LongMessage as LongMessage1,
 profilepicurl =
 (
  CASE n.senderType
  WHEN  '0' THEN (select profilepicurl from athelete where id = n.senderId)
  WHEN '1'THEN (select profilepicurl from institution where id = n.senderId)
  ELSE (select profilepicurl from ClubCoach where id = n.senderId)
  END
 ),
 sendername =
 (
  CASE n.senderType
  WHEN  '0' THEN (select name from athelete where id = n.senderId)
  WHEN '1'THEN (select name from institution where id = n.senderId)
  ELSE (select name from ClubCoach where id = n.senderId)
  END
 )
 from notifications n
 inner join notificationsmaster nm on n.NotificationId = nm.Id  
 Where n.UserId = @UserId and n.status <> 3
 order by n.CreatedDate desc, n.status  
    OFFSET @Offset ROWS FETCH NEXT @max  ROWS ONLY
 
END
GO



 
 

  
ALTER PROCEDURE [dbo].[GetAtheleteFavourites_v4]     --[GetAtheleteFavourites_v4] 66,0,30
 @AtheleteId int,
 @offset int,
 @max int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @totalCount int  
     --declare @AtheleteId int 
	 declare @TotalAcceptedCount int
	 declare @totalChatCount int

   -- select @AtheleteId = id from Athelete where Emailaddress = @emailaddress
    
	select @TotalAcceptedCount = count(*) from AtheleteInvitations where AtheleteId = @AtheleteId and Accept = 1

	select @totalCount = count(*) from AtheleteInstituteMatches am
		left  join InstituteAtheleteMatches ia on ia.coachid = am.coachid
		and am.atheleteid = ia.atheleteid
		--inner join institution i on i.id = am.instituteid
		where am.AtheleteId = @AtheleteId and am.status = 1 

	 select @totalChatCount = count(*) from chats where SenderId = @AtheleteId 

	  select distinct i.*,isnull(ia.instituteid,0) as Likes,@totalCount as TotalCount,@totalChatCount as TotalChatCount,
	   am.CreatedDate as AtheleteDate, ia.CreatedDate as InstituteDate,
	    ait.InvitedDate,c.Emailaddress as CoachEmail , c.FacebookId as CoachFacebook,
		ait.Accept,@TotalAcceptedCount as TotalAcceptedCount,
		isnull(c.id,0) as CoachId, isnull(@AtheleteId,0) as AtheleteId
		 ,a.Name as AtheleteName,a.ProfilePicURL as AtheleteProfilePic,c.payment_paid,
		 d.DeviceType, d.DeviceId  
	    from AtheleteInstituteMatches am
	    inner join Coaches c on c.id = am.coachid
		left  join InstituteAtheleteMatches ia 
		on ia.coachid = am.coachid and am.atheleteid = ia.atheleteid and ia.status = 1
		inner join institution i on i.id = c.instituteid
		left join AtheleteInvitations ait on ait.instituteid = i.id and ait.atheleteid = am.atheleteid
		 left join Athelete a on a.id = am.AtheleteId 
		 left join Device d on d.Id = c.DeviceId
		where am.AtheleteId = @AtheleteId and am.status = 1 and am.collAtheleteCoachType = 1
	    
        
END
 


GO




CREATE PROCEDURE [dbo].[AddCoach_2v6] 
	@EmailAddress varchar(100),
	@InstituteId int,
	@Password varchar(100) ,
    @Active Bit,
	@TeamType int,	
	@DeviceType varchar(50),
	@DeviceId varchar(500),
	@FirstName varchar(100),
	@LastName varchar(100),
	@Id int Output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	 declare @cnt int
	 declare @UniVCount int
	 declare @HeadCoach int --0-notclaimed,1-HeadCoach,2-NotHeadCoach
	 declare @activation_code varchar(1000) 
	 declare @getDeviceId int 
	 declare @devicecnt int
 
 	-- exec @getDeviceId = [AddDevice] @DeviceId,@DeviceType
    select @devicecnt = count(*) from Device where  DeviceId = @DeviceId and  DeviceType = @DeviceType
	 if(@devicecnt = 0)
	 Begin
		insert into Device(DeviceID,DeviceType,CreateDate) 
		values (@DeviceId,@DeviceType,getutcdate())
		set @getDeviceId = @@Identity
	 End
	 else
	 begin
	  set @getDeviceId = (select Id from Device where DeviceID = @DeviceId and DeviceType=@DeviceType)
	 end

	 select @UniVCount = count(*) from Coaches where Instituteid = @InstituteId 
	 select @HeadCoach=HeadCoach from Coaches where Instituteid = @InstituteId 
 
		 if(@UniVCount = 1 and @HeadCoach=0)
		 Begin 
		 set @activation_code = (select newid()) 
		  Update Coaches set EmailAddress = @EmailAddress, password=@Password, 
           activation_code=@activation_code,PreferredStrokes='ALL',DeviceId=@getDeviceId,
		   FirstName=@FirstName,LastName=@LastName
		   Where Instituteid = @InstituteId 
		   select @Id = Id from Coaches Where Emailaddress = @Emailaddress 
		 End
		 Else if(@UniVCount >= 1)
		 Begin
		    set @HeadCoach = 2
		    select @cnt = count(*) from Coaches where Emailaddress = @Emailaddress 
				if(@cnt = 0)
				Begin
				   set @activation_code = (select newid()) 
					insert into Coaches(EmailAddress,InstituteId,Password,active,CreatedDate,
					FacebookId,Payment_Paid,HeadCoach,activation_code,TeamType,PreferredStrokes,DeviceId,
					FirstName,LastName) 
					values (@EmailAddress,@InstituteId,@Password,@Active,getutcdate(),
					0,0,@HeadCoach,@activation_code,@TeamType,'ALL',@getDeviceId,
					@FirstName,@LastName)
					set @Id = @@Identity 
				End
				Else
				Begin 
				    select @Id = Id from Coaches Where Emailaddress = @Emailaddress 
				End
	     End
		 
	  
	 select c.*,i.name as UniversityName,d.DeviceId,d.DeviceType from Coaches c
	 inner join institution i on i.id = c.instituteid
	 left join Device d on d.Id = c.DeviceId
	 where c.id = @Id 
		
		 
END 


GO


 
  
 

ALTER PROCEDURE [dbo].[GetCoachExists]   --[GetCoachExists] 'bindu.bhargavi@gmail.com','123456'
	@Emailaddress varchar(100),
	@Password varchar(100)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;  
	    
		   select i.*,1 as TotalCount ,c.Emailaddress as CoachEmail , c.FacebookId as CoachFacebook ,
		   c.id as CoachId,c.Active,c.HeadCoach,c.PreferredStrokes,c.AcademicRate,c.Payment_Paid
		    ,d.DeviceId,d.DeviceType,c.FirstName,c.LastName
		   from Institution i
		   inner join Coaches c on c.instituteid = i.id
		    left join Device d on d.Id = c.DeviceId
		   Where c.Emailaddress = @Emailaddress and password = @Password
	 
    
	   
	 

END

  

GO


 
 
  

ALTER PROCEDURE [dbo].[GetInstituteFavourites_v5]     
 @emailaddress varchar(100),
 @offset int,
 @max int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @totalCount int
  
    --declare @InstituteId  int 

	  declare @Coachid  int 

     --select @InstituteId =  i.id from Institution i
	 --inner join Coaches c on i.id = c.InstituteId
	 --where c.EmailAddress = @emailaddress

	 select @Coachid = id from coaches c where c.EmailAddress = @emailaddress
	  
	select @totalCount =  count(distinct ia.id)  from InstituteAtheleteMatches ia 
		 left join AtheleteInstituteMatches am 
		 on ia.atheleteid = am.atheleteid and ia.instituteid  = am.instituteid
		 inner join athelete a on a.id = ia.atheleteid
		 where ia.coachid = @Coachid and ia.status = 1	

		 SELECT distinct Id,Name,Description,School,emailaddress,GPA,Social,SAT,ACT,InstituteId,Gender,FacebookId,ProfilePicURL,
		 DOB,Address,Latitude,Longitude,TotalCount,FriendsInApp,SharesApp,AverageRating,AcademicLevel,YearLevel,FriendsRate,
		 ShareRate,SocialRate,AcademicRate,AtheleteRate,ISNULL(Max(AtheleteDate) ,'') as AtheleteDate,InstituteDate,
		 Likes,InvitedDate,AtheleteId,CoachId,Premium,UniversityName,UniversityProfilePic,Height,Height2,Weight,
		 WeightType,wingspan,WingSpanType,shoesize,ShoeSizeType,ClubName,CoachName,CoachEmailId,Major,phonenumber,
		 StateId,StateName,VerticalJump,BroadJump,LoginType,AP,Username,AllAmerican,AcademicAllAmerican,
		 Captain,SwimswamTop,AP,[20YardShuttleRun],[60YardShuttleRun],[KneelingMedBallToss],[RotationalMedBallToss],
		 Device_Id, DeviceType
		 FROM (
		 select distinct a.*,ab.AllAmerican,ab.AcademicAllAmerican,
		 ab.Captain,ab.SwimswamTop, isnull(am.atheleteid,0) as Likes,@totalCount as TotalCount, 
		 am.CreatedDate as  AtheleteDate, 
		 CONVERT(date,ia.CreatedDate) as InstituteDate,ait.InvitedDate,
		 isnull(@coachId,0) as CoachId, isnull(a.id,0) as AtheleteId
		 ,i.Name as UniversityName,i.ProfilePicURL as UniversityProfilePic,s.name as StateName,
		 d.DeviceId as Device_Id,d.DeviceType
		 from InstituteAtheleteMatches ia 
		 left join AtheleteInstituteMatches am 
		 on ia.atheleteid = am.atheleteid and ia.instituteid  = am.instituteid and am.collAtheleteCoachtype = 1	
		 inner join athelete a on a.id = ia.atheleteid
		 left join states s on s.Id = a.StateId
		-- left join AthletePersonalCoach apc on apc.AtheleteId = a.id
		 left join AtheleteInvitations ait on ait.atheleteid = a.id and ait.CoachId = @Coachid
		 left join institution i on i.id = ia.instituteid
		 left join atheletebadges ab on ab.atheleteid = a.id
		 left join device d on d.id = a.deviceid
		 where ia.coachid = @Coachid and ia.status = 1	 	
	  
	   ) A 
	   GROUP BY A.id,A.Name,A.Description,A.School,A.emailaddress,A.GPA,A.Social,A.SAT,A.ACT,A.InstituteId,A.Gender,A.FacebookId,A.ProfilePicURL,
		 A.DOB,A.Address,A.Latitude,A.Longitude,A.TotalCount,A.FriendsInApp,A.SharesApp,A.AverageRating,A.AcademicLevel,A.YearLevel,A.FriendsRate,
		 A.ShareRate,A.SocialRate,A.AcademicRate,A.AtheleteRate,A.InstituteDate,A.Likes,A.InvitedDate,A.AtheleteId,A.CoachId,A.Premium,A.UniversityName,
		 A.UniversityProfilePic,A.Height,A.Height2,A.Weight,A.WeightType,A.wingspan,A.WingSpanType,A.shoesize,A.ShoeSizeType,A.ClubName,A.CoachName,A.CoachEmailId,
		 A.Major,A.phonenumber,A.StateId,A.StateName,A.VerticalJump,A.BroadJump,A.LoginType,A.AP,A.Username,
		 A.AllAmerican,A.AcademicAllAmerican,A.Captain,A.SwimswamTop,A.AP,A.[20YardShuttleRun],A.[60YardShuttleRun],A.[KneelingMedBallToss],A.[RotationalMedBallToss],
		 A.Device_Id,A.DeviceType
	      order by   a.Likes Desc ,AtheleteDate 
	     OFFSET @Offset ROWS FETCH NEXT @max  ROWS ONLY

  END
  
 


GO


  
   
ALTER PROCEDURE [dbo].[GetAllAtheletesByCoachEmail2_v1]    -- GetAllAtheletesByCoachEmail2_v1   'binduv@elitecrest.com',0,100,'ALL'
 (
 @emailaddress varchar(100),
 @offset int,
 @max int, 
 @SearchText varchar(1000)
 )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @totalCount int
	declare @SqlStr1 varchar(4000)
    declare @InstitutionId int
	declare @CoachId int
	declare @academicRate int
	declare @preferredStrokes varchar(500)
	declare @year int
	declare @gender int
	declare @location int
	declare @athleteRate int
	
	

	Begin Try
		Begin Tran  

		 select @CoachId = Id from coaches c where c.EmailAddress = @emailaddress
		 select @InstitutionId = instituteId from Coaches where id = @CoachId

		 if((select count(*) from CoachFilters where coachid = @coachid) > 0)
		 Begin
			set @academicRate  =(select isnull(AcademicRate,0) from CoachFilters where coachid = @CoachId)
			set @preferredStrokes =(select isnull(PreferredStrokes,'')  from CoachFilters where coachid = @CoachId)
			set @year =  (select isnull([year],0) from CoachFilters where coachid = @CoachId)
			set @gender = (select isnull(Gender,99)  from CoachFilters where coachid = @CoachId)
			set @location = (select isnull(location,0) from CoachFilters where coachid = @CoachId)
			set @athleteRate = (select isnull(athleteRate,0) from CoachFilters where coachid = @CoachId)
	     End
		 else
		 Begin
		    set @academicRate  = 0
			set @preferredStrokes = ''
			set @year = 0
			set @gender = 99
			set @location = 0
			set @athleteRate = 0
		 End 

		  
		  
		 if(@searchtext  = 'ALL')
		 Begin
			select @totalCount = count(*)  from athelete a where isnull(school,'') != '' and isnull(a.AtheleteType,0)=0 and 
			id not  in(select atheleteid from  InstituteAtheleteMatches where (status=1 and Instituteid =@InstitutionId)
				or Coachid=@CoachId) 
		 End
		 Else
		 Begin
	 		select @totalCount = count(*)  from athelete a where  isnull(school,'') != '' and isnull(a.AtheleteType,0)=0 and
			name like '%'+ @SearchText + '%' and id not  in(
		 		select atheleteid from  InstituteAtheleteMatches where
				(status=1 and Instituteid =@InstitutionId)
				or Coachid=@CoachId) 
		 End  

				 set @SqlStr1 ='  select distinct a.*,ab.*,'+ cast(@totalCount as varchar)+' as TotalCount   
				 ,isnull(ai.atheleteid,0)  as RightSwiped,s.name as StateName
				 ,'''+ @preferredStrokes +''' as Strokes,'+cast(@location as varchar)+' as Location 
				 from athelete a  
				 left join states s on s.Id = a.StateId
				 left join AtheleteBadges ab on ab.AtheleteId = a.Id  
				 left join AtheleteInstituteMatches ai on ai.atheleteid = a.id and ai.status = 1
				 and collAtheleteCoachType=1  
				 and ai.Instituteid =' + cast(@InstitutionId as varchar) + ' 
				 where a.gender = 0 and isnull(school,'''') != '''' and isnull(a.AtheleteType,0)=0 and a.id not in(
		 		 select ia.atheleteid from  InstituteAtheleteMatches ia  
				 where (status=1 and collAtheleteCoachType=1 and Instituteid =' + cast(@InstitutionId as varchar) + ')'+
				 ' or coachid =' + cast(@CoachId as varchar) + ')'
			   if(@searchtext != 'ALL')
				 set @SqlStr1 =  @SqlStr1  + ' and a.name like ''%' + cast(@searchtext  as varchar) + '%'''
			   if(@academicRate <> 0)
				 set @SqlStr1 = @SqlStr1 + ' and  isnull(AcademicRate,0) >= ' + cast(@academicRate as varchar)
			   if(@year <> 0)
				 set @SqlStr1 = @SqlStr1 + ' and  isnull(yearlevel,0) >= ' + cast(@year as varchar)
			   if(@gender <> 99)
				 Begin
				   if(@gender <> 2)
					 set @SqlStr1 = @SqlStr1 + ' and  isnull(Gender,99) = ' + cast(@gender as varchar)
				 End
				if(@athleteRate <> 0)
				 set @SqlStr1 = @SqlStr1 + ' and  isnull(AtheleteRate,0) >= ' + cast(@athleteRate as varchar) 
				 
		  
		set @SqlStr1 = @SqlStr1 + ' order by a.Diamond desc, RightSwiped desc '
	   set @max = @max - 1
	   set @SqlStr1 = @SqlStr1 + ' OFFSET '+ cast(@Offset as varchar) +' ROWS FETCH NEXT '+ cast(@max as varchar) +' ROWS ONLY'
	 print(@SqlStr1)
  exec(@SqlStr1)

	  Commit Tran 
  End Try
  Begin catch
   Rollback Tran
  End Catch


 
END


GO


 
 
 
CREATE PROCEDURE [dbo].[AddDeviceForUser] --[AddDeviceForUser] 'dnkOJSUwfF8:APA91bEB6GCnui1PHZXWjozkduyEiIuORaGEnH4Tbaa81De_fTpYNSC5XF1bsfRhlln2db_lcYkkXgbdfnT6K07GF6IVcOiYyD3pBZ2R7NDlLj3Q3SdYPBIkDa2ytEisXTRIH9dnF5ZC','Iphone','CC',337
	@DeviceId varchar(500),
	@DeviceType varchar(50),
	@UserType varchar(20),
	@UserId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @Id int
	 declare @cnt int 

	 select @cnt = count(*) from Device where  DeviceId = @DeviceId and  DeviceType = @DeviceType
	 if(@cnt = 0)
	 Begin
		insert into Device(DeviceID,DeviceType,CreateDate) 
		values (@DeviceId,@DeviceType,getutcdate())
		set @Id = @@Identity
	 End
	 Else
	 Begin
	     set @Id =(select Id from Device where  DeviceId = @DeviceId and  DeviceType = @DeviceType)

	 End
	  
	  If(@UserType = 'HS' or @UserType = 'CA')
	  Begin
	     Update athelete set DeviceId = @Id Where Id = @UserId
	  End
	 
   	    If(@UserType = 'FFF')
	  Begin
	     Update FamilyFriends set DeviceId = @Id Where Id = @UserId
	  End

	    If(@UserType = 'ClC')
	  Begin
	     Update ClubCoach set DeviceId = @Id Where Id = @UserId
	  End
	  If(@UserType = 'CC')
	  Begin
	     Update Coaches set DeviceId = @Id Where Id = @UserId
	  End
		 
		 select @UserID as Id
END
 


GO


  