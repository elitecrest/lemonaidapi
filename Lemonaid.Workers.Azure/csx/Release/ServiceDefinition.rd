﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="Lemonaid.Workers.Azure" generation="1" functional="0" release="0" Id="7afff495-1b8c-49e3-a2e9-9f5df739c05e" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="Lemonaid.Workers.AzureGroup" generation="1" functional="0" release="0">
      <settings>
        <aCS name="Lemonaid.Worker:APPINSIGHTS_INSTRUMENTATIONKEY" defaultValue="">
          <maps>
            <mapMoniker name="/Lemonaid.Workers.Azure/Lemonaid.Workers.AzureGroup/MapLemonaid.Worker:APPINSIGHTS_INSTRUMENTATIONKEY" />
          </maps>
        </aCS>
        <aCS name="Lemonaid.Worker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/Lemonaid.Workers.Azure/Lemonaid.Workers.AzureGroup/MapLemonaid.Worker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="Lemonaid.WorkerInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/Lemonaid.Workers.Azure/Lemonaid.Workers.AzureGroup/MapLemonaid.WorkerInstances" />
          </maps>
        </aCS>
      </settings>
      <maps>
        <map name="MapLemonaid.Worker:APPINSIGHTS_INSTRUMENTATIONKEY" kind="Identity">
          <setting>
            <aCSMoniker name="/Lemonaid.Workers.Azure/Lemonaid.Workers.AzureGroup/Lemonaid.Worker/APPINSIGHTS_INSTRUMENTATIONKEY" />
          </setting>
        </map>
        <map name="MapLemonaid.Worker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/Lemonaid.Workers.Azure/Lemonaid.Workers.AzureGroup/Lemonaid.Worker/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapLemonaid.WorkerInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/Lemonaid.Workers.Azure/Lemonaid.Workers.AzureGroup/Lemonaid.WorkerInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="Lemonaid.Worker" generation="1" functional="0" release="0" software="E:\WorkingCopies\lemonaidapi\Lemonaid.Workers.Azure\csx\Release\roles\Lemonaid.Worker" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="APPINSIGHTS_INSTRUMENTATIONKEY" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;Lemonaid.Worker&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;Lemonaid.Worker&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/Lemonaid.Workers.Azure/Lemonaid.Workers.AzureGroup/Lemonaid.WorkerInstances" />
            <sCSPolicyUpdateDomainMoniker name="/Lemonaid.Workers.Azure/Lemonaid.Workers.AzureGroup/Lemonaid.WorkerUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/Lemonaid.Workers.Azure/Lemonaid.Workers.AzureGroup/Lemonaid.WorkerFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="Lemonaid.WorkerUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="Lemonaid.WorkerFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="Lemonaid.WorkerInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
</serviceModel>