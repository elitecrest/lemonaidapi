﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Lemonaid_web_api.Models;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using System.Web;

namespace Lemonaid_web_api.Controllers
{
    public class FamilyFriends2V5Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpGet]
        public Utility.CustomResponse GetFamilyFriendsBlogs(int familyFriendId)
        {
            try
            {
                Athletev4Controller athleteV4 = new Athletev4Controller();
                CoachV5Controller coachV5 = new CoachV5Controller();
                FamilyfriendsBlogDTO familyfriendsBlogDTO = new FamilyfriendsBlogDTO();
                List<BlogDTO> blogs = UsersDal.GetBlog();
                familyfriendsBlogDTO.BlogsDTO = blogs;
                var athletes = UsersDalV5.GetFamilyFriendsAtheleteFavourites(familyFriendId, 0, 100);
                athletes = coachV5.AddVideosToList(athletes);
                foreach (var r in athletes)
                {
                    r.AthleteMediaContent = DAL2V5.GetAthleteMediaContentById(r.Id);
                }
                familyfriendsBlogDTO.Athletes = athletes;

                var institutes = UsersDalV5.GetFamilyFriendsInstituteFavourites(familyFriendId, 0, 100);
                familyfriendsBlogDTO.coaches = athleteV4.AddCoachVideos(institutes);
                if (blogs.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = familyfriendsBlogDTO;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends2V5/GetFamilyFriendsBlogs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
    }
}
