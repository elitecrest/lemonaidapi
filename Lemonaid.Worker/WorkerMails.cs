﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Web;
using System.Linq;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Text;

namespace Lemonaid.Worker
{
    //Using LemonaidRowingStaging Profile
    public class WorkerMails : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        string baseConnString = string.Empty;

        public override void Run()
        {
            Trace.TraceInformation("Lemonaid.Worker is running");

            try
            {
                //This is published to Lemonaid Rowing Staging
                //Check For process run today or not 
                var ConnStrings =  GetProductionConnection();
                foreach (var conn in ConnStrings)
                {
                    var lastInsertedDate = GetLastInsertedWorkerDate(conn);
                    if (lastInsertedDate != System.DateTime.Today.ToString("MM/dd/yyyy"))
                    {
                        AddWorkerLastRunDate(conn);
                        SendPushNotificationsToCoaches(conn);
                        AddBlogs(conn);
                        SendMailToCoaches(conn); 
                    }
                }
                Thread.Sleep(TimeSpan.FromDays(3));
                //Thread.Sleep(TimeSpan.FromHours(1));
                Trace.TraceInformation("Working", "Information");

            }
            catch(Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WorkerMails/Run", "", ex.Message, "Exception", 0);
                throw;
            }
       
            finally
            {
                this.runCompleteEvent.Set();
            }
        }


        public static string GetLastInsertedWorkerDate(string conn)
        {
            string lastInsertedDate = string.Empty;
            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();

                EmailContentDTO emailContentDTO = new EmailContentDTO();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("GetLastInsertedWorkerDate", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    lastInsertedDate = (cmd.ExecuteScalar() != null) ? cmd.ExecuteScalar().ToString() : string.Empty;


                    myConn.Close();
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WorkerMails/GetLastInsertedWorkerDate", "", ex.Message, "Exception", 0);
                throw;
            }
            return lastInsertedDate;
        }


    

        public static void SendMailToCoaches(string conn)
        {
            try
            {

                // var connectionString = conn;// ConfigurationManager.ConnectionStrings["LemonaidDB"].ConnectionString;
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();

                EmailContentDTO emailContentDTO = new EmailContentDTO();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("GetUniversityRightSwipes", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        //GetData and send mail
                        emailContentDTO.Emailaddress = (myData["emailaddress"] == DBNull.Value) ? string.Empty : myData["emailaddress"].ToString();
                        emailContentDTO.Rightswipes = myData["RightSwipes"].ToString();
                        emailContentDTO.Id = Convert.ToInt32(myData["Id"].ToString());
                        if (emailContentDTO.Emailaddress != string.Empty)
                        {
                            //Uncomment this later
                            SendMailToUniversities(emailContentDTO.Emailaddress, emailContentDTO.Rightswipes, emailContentDTO.Id,conn);
                        }


                    }

                    myData.Close();
                    myConn.Close();
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WorkerMails/SendMailToCoaches", "", ex.Message, "Exception", 0);
                throw;
            }
        } 

        public static void AddWorkerLastRunDate(string conn)
        {
            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("AddWorkerLastRunDate", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }

            }
            catch (Exception ex)
            {

            }
        } 

        internal static List<string> GetStagingConnection()
        {
            List<string> connStrings = new List<string>();
            //connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            //connStrings.Add("Server =tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidRowingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            //connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidDivingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
             //connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTennisStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            // connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTrackStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            //connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidLacrosseStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            //connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidGolfStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            // connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWaterPoloStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            //connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidFootballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            //connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBasketballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            //connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBaseballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            //connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoccerStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            // connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoftBallStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrestlingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            // connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidIceHockeyStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
           // connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidVolleyBallStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            return connStrings;
        }

        internal static List<string> GetProductionConnection()
        {
            List<string> connStrings = new List<string>();
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBiz;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server =tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidRowing;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidDiving;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTennis;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTrack;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidLacrosse;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidGolf;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWaterPolo;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidFootball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBasketball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBaseball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoccer;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoftBall;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrestling;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidIceHockey;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidVolleyBall;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            return connStrings;
        }

        public static int SendMailToUniversities(string Emailaddress, string Rightswipes, int Id,string Conn)
        {
            string Message = string.Empty;
            SupportMailDTO support = new SupportMailDTO();
            MailMessage Msg = new MailMessage();
            try
            {
                //// Get the Content from table and execute it 
                //string appRoot = HttpRuntime.AppDomainAppPath;
                //string strPath = Path.Combine(appRoot + @"\", @"AppData\EmailSending.html");

                //StreamReader reader = new StreamReader(strPath);
                //string readFile = reader.ReadToEnd();
                string myString = "";
                MailContentDTO mailcontent = GetMailContent("Coaches Right Swipe Mail");
                myString = mailcontent.Content;
               // myString = readFile;
                // myString = myString.Replace("$$Right", Emailaddress);
                myString = myString.Replace("$$Right$$", Rightswipes);


                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(Emailaddress);
                sendGridModel.fromMail = support.EmailAddress;
                sendGridModel.fromName = "Lemonaid";
                sendGridModel.subject = " Sweetness!!! " + Rightswipes + " athletes showed interest in your school on LemonAid";
                sendGridModel.Message = myString;
                sendGridModel.email = support.EmailAddress;
                sendGridModel.password = support.Password;
                sendGridModel.Connection = Conn;
                SendGridSMTPMail.SendEmail(sendGridModel);


                //EmailDTO email = new EmailDTO();
                //email.From = support.EmailAddress;
                //email.To = Emailaddress;
                //email.Status = 1;
                //email.Subject = Msg.Subject;
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog(support.EmailAddress, Emailaddress, Msg.Subject, "Mail", 1);

                return 0;
            }
            catch (SmtpException ex)
            {

                Message = ex.Message;
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("UsersDal/SendMailToUniversities", "", ex.Message, "Exception", 0);
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("UsersDal/SendMailToUniversities", "", ex.Message, "Exception", 0);
                return 1;
            }

        }


        public static void SendPushNotificationsToCoaches(string conn)
        {
            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();

                EmailContentDTO emailContentDTO = new EmailContentDTO();
                string Message = string.Empty;
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("GetUniversityRightSwipes", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        //GetData and send mail
                        emailContentDTO.Emailaddress = (myData["emailaddress"] == DBNull.Value) ? string.Empty : myData["emailaddress"].ToString();
                        emailContentDTO.Rightswipes = myData["RightSwipes"].ToString();
                        emailContentDTO.Name = (myData["name"] == DBNull.Value) ? string.Empty : myData["name"].ToString();
                        if (emailContentDTO.Emailaddress != string.Empty)
                        {
                            emailContentDTO.Name = FirstCap(RemoveWhiteSpace(emailContentDTO.Name));
                            Message = "Congratulations! You had " + emailContentDTO.Rightswipes + " right swipes on LemonAid from last two days";
                            PushNotification(Message, emailContentDTO.Name);

                        }
                    } 

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/SendPushNotificationsToCoaches", "", ex.Message, "Exception", 0);
                throw;
            }


        }

        public static string RemoveWhiteSpace(string self)
        {
            return new string(self.Where(c => !Char.IsWhiteSpace(c)).ToArray());
        }


        public static string FirstCap(string value)
        {
            string result = String.Empty;

            if (!String.IsNullOrEmpty(value))
            {
                if (value.Length == 1)
                {
                    result = value.ToUpper();
                }
                else
                {
                    result = value.Substring(0, 1).ToString().ToUpper() + value.Substring(1).ToLower();
                }
            }

            return result;
        }

       

        internal static bool PushNotification(string Name, string Message)
        {
            bool IsSend = false;
            try
            {
                FirebaseDTO firebase = GetFirebaseCredentials();
                string applicationID = firebase.FirebaseAppId;
                string senderId = firebase.FirebaseSenderId;

                string deviceId = Name;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = Message,
                        title = "Lemonaid Notification",
                        sound = "Enabled"

                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                                IsSend = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
            return IsSend;
        }

        private void AddBlogs(string conn)
        {
            BuzzFeed.GetDataFromXMLandInsert(conn);
        }

        public static MailContentDTO GetMailContent(string name)
        {
            MailContentDTO v = new MailContentDTO();
            try
            {
                string  conn = "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrapper;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();


               
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("GetMailContent", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namePar = cmd.Parameters.Add("@name", SqlDbType.VarChar, 100);
                    namePar.Direction = ParameterDirection.Input;
                    namePar.Value = name;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    { 
                       
                         
                        v.Name = myData["name"].ToString();
                        v.Content = myData["Content"].ToString(); 
                    }
                        myConn.Close();
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("WorkerMails/GetMailContent", "", ex.Message, "Exception", 0);
                throw;
            }
            return v;
        }


        internal static FirebaseDTO GetFirebaseCredentials()
        {
            FirebaseDTO firebaseDTO = new FirebaseDTO();
            try
            {
                string conn = "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrapper;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFirebaseCredentials]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        firebaseDTO.FirebaseAppId = (myData["FirebaseAPPID"] == DBNull.Value) ? "" : (string)myData["FirebaseAPPID"];
                        firebaseDTO.FirebaseSenderId = (myData["FirebaseSenderId"] == DBNull.Value) ? "" : (string)myData["FirebaseSenderId"];

                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2v6/GetFirebaseCredentials ", "", ex.Message, "Exception", 0);
                throw;
            }

            return firebaseDTO;
        }
        public class EmailDTO
        {
            public string From { get; set; }
            public string To { get; set; }
            public string Subject { get; set; }
            public int Status { get; set; }
            public string MailType { get; set; }
        }

        public partial class SupportMailDTO
        {
            public string EmailAddress { get; set; }
            public string Password { get; set; }
            public string Host { get; set; }
            public string Port { get; set; }
            public string SSL { get; set; }

        }

        public class EmailContentDTO
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Emailaddress { get; set; }
            public string Rightswipes { get; set; }
        }

        public class MailContentDTO
        { 
            public string Name { get; set; }
            public string Content { get; set; }
            
        }

        public partial class FirebaseDTO
        {
            public string FirebaseAppId { get; set; }
            public string FirebaseSenderId { get; set; }
        }
    }

}