﻿using System;
using System.Web.Http;
using Lemonaid_web_api.Models;
  
using System.Collections.Generic;
using System.Linq; 

namespace Lemonaid_web_api.Controllers
{
    public class Coach2V4Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpPost]
        public Utility.CustomResponse GetAllAthletesByLocationWithSearch2_v4(CoachDeckDTO coachDeckDTO)
        {
           
            AthleteController athleteContr = new AthleteController();
            CoachV5Controller coachv5 = new CoachV5Controller();
            try
            { 
                var athletes = DAL2V4.GetAllAthletePagingWithSearch(coachDeckDTO.emailaddress, coachDeckDTO.offset, coachDeckDTO.max, coachDeckDTO.searchText, coachDeckDTO.SportId);
                if (athletes.Count > 0)
                {
                    var newList = new List<AthleteDTO>();
                    var newListFromStrokes = new List<AthleteDTO>();
                    var listWithQuestionaire = new List<AthleteDTO>();
                 
                    //Location Type 
                    //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                    if (coachDeckDTO.locationType == 1)
                    {
                        newList = athletes;

                    }
                    else
                        if (coachDeckDTO.locationType == 2)
                    {
                        foreach (var t in athletes)
                        {
                            double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                            double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                            double distance = athleteContr.DistanceTo(coachDeckDTO.latitude, coachDeckDTO.longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(t);
                            }
                        }
                    }
                    else
                            if (coachDeckDTO.locationType == 3)
                    {
                        foreach (var t in athletes)
                        {
                            double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                            double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                            var distance = athleteContr.DistanceTo(coachDeckDTO.latitude, coachDeckDTO.longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 5)
                            {
                                newList.Add(t);
                            }
                        }
                    }

                    if (newList[0].Strokes != "")
                    {
                        if (newList[0].Strokes != "ALL")
                        {
                            string[] strStrokes = newList[0].Strokes.Split(',');

                            var atheleteIds = new List<int>();
                            var atheleteStrokes = UsersDal.GetStrokesForAtheletes();
                            foreach (var t in atheleteStrokes)
                            {
                                foreach (string t1 in strStrokes)
                                {
                                    if (t1 == t.Category)
                                    {
                                        atheleteIds.Add(t.AtheleteId);
                                    }
                                }
                            }

                            foreach (AthleteDTO t in newList)
                            {
                                foreach (int t1 in atheleteIds)
                                {
                                    if (t1 == t.Id)
                                    {

                                        newListFromStrokes.Add(t);
                                    }
                                }
                            }
                            newListFromStrokes = newListFromStrokes.Distinct().ToList();
                        }
                        else
                        {

                            newListFromStrokes = newList;
                        }
                    }
                    else
                    { newListFromStrokes = newList; }

                    AnalyticsDTO analytics = new AnalyticsDTO();

                    foreach (var r in newListFromStrokes)
                    {
                        analytics.Emailaddress = r.Emailaddress;
                        analytics.AthleteId = r.Id;
                        analytics.MonthId = 0;
                        analytics.YearId = 0;
                        analytics = Dal2.GetAthleteAnalyticsData(analytics);
                        r.Rank = analytics.Rank;
                    }

                    CoachDTO coach = UsersDal.GetCoachProfileByEmail(coachDeckDTO.emailaddress);
                    List<QuestionaireDTO> questionaireDto = UsersDalV5.GetQuestionaire_v5(coach.CoachId, "CC");
                    listWithQuestionaire = coachv5.AddQuestionairetoList(questionaireDto, listWithQuestionaire);
                  
                    if (newListFromStrokes.Count > 0)
                    {
                        newListFromStrokes = coachv5.AddBadgesToList(newListFromStrokes);
                        //Add BestTimes
                        newListFromStrokes = athleteContr.AddBesttimesToList(newListFromStrokes);
                        List<AdDTO> ads = DAL2V6.GetChatAdCards(coachDeckDTO.emailaddress, 0 , coachDeckDTO.SportId, Convert.ToInt32(Utility.AdType.NormalAd), Utility.UserTypes.CollegeCoach);
                        newListFromStrokes = athleteContr.GetAdAthletes(ads, newListFromStrokes);
                        //Add Athelete Videos
                        newListFromStrokes = coachv5.AddVideosToList(newListFromStrokes);

                        listWithQuestionaire = coachv5.AddAtheletestoList(newListFromStrokes, listWithQuestionaire);
                        var listWithQuestionaireShufflers = listWithQuestionaire.OrderBy(a => Guid.NewGuid());
                        if (listWithQuestionaireShufflers.ToList().Count > 0)
                        {
                            _result.Status = Utility.CustomResponseStatus.Successful;
                            _result.Response = listWithQuestionaire;
                            _result.Message = CustomConstants.Details_Get_Successfully;
                        }
                        else
                        {
                            _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            _result.Message = CustomConstants.NoRecordsFound;
                        }

                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.NoRecordsFound;
                    }

                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/GetAllAthletesByLocationWithSearch2_v1", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetCoachesFavourites2_v4(string emailaddress, int offset, int max,int SportId)
        {

            try
            {
                CoachV5Controller coachV5 = new CoachV5Controller();
                AthleteController athCtrl = new AthleteController();
                var atheletes = DAL2V4.GetCoachesFavourites(emailaddress, offset, max, SportId);
                CoachDTO coach = UsersDal.GetCoachProfileByEmail(emailaddress);
                int coachId = coach.CoachId;
                foreach (var a in atheletes)
                {
                    int chatCount = UsersDalV4.GetChatCount(a.Id, coachId);
                    a.ChatCount = chatCount;
                }

                int totalLikesCount = atheletes.Count(p => p.Liked);

                for (var i = 0; i < atheletes.Count; i++)
                {
                    atheletes[i].TotalLikedCount = totalLikesCount;
                }
                atheletes = athCtrl.AddBesttimesToList(atheletes);
                atheletes = coachV5.AddVideosToList(atheletes); //Shared and Normal Athlete Videos
               
                foreach (var r in atheletes)
                {
                   r.AthleteMediaContent = DAL2V5.GetAthleteMediaContentById(r.Id); 
                     
                }
                AnalyticsDTO analytics = new AnalyticsDTO();
               
                foreach (var r in atheletes)
                {
                    analytics.Emailaddress = r.Emailaddress;
                    analytics.AthleteId = r.Id;
                    analytics.MonthId = 0;
                    analytics.YearId = 0;
                    analytics = Dal2.GetAthleteAnalyticsData(analytics);
                    r.Rank = analytics.Rank;
                }
                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Matches;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V4/GetCoachesFavourites2_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

    }
}
