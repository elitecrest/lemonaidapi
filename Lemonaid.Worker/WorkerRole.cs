using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using System.Net.Http;
using System.Net.Http.Headers;
//using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Collections.Generic;

namespace Lemonaid.Worker
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("Lemonaid.Worker is running");

            try
            { 
               //while(true)
               //{
               //     //Check For process run today or not 

               //    SendMailToCoaches();
               //    SendPushNotificationsToCoaches();
               //    AddBlogs();
               //    Thread.Sleep(TimeSpan.FromDays(3));
               //    Trace.TraceInformation("Working", "Information");
               //}
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }
        private List<string>  GetBaseurls()
        {
            List<string> urls = new List<string>();
            //urls.Add("http://lemonaidbizapi.azurewebsites.net/");
            //urls.Add("http://lemonaidrowingapi.azurewebsites.net/");
            //urls.Add("http://lemonaiddivingapi.azurewebsites.net/");
            //urls.Add("http://lemonaidtennisapi.azurewebsites.net/");
            //urls.Add("http://lemonaidgolfapi.azurewebsites.net/");
            //urls.Add("http://lemonaidlacrosseapi.azurewebsites.net/");
            //urls.Add("http://lemonaidwaterpoloapi.azurewebsites.net/");
            //urls.Add("http://lemonaidtrackapi.azurewebsites.net/");
            //urls.Add("http://lemonaidbaseballapi.azurewebsites.net/");
            //urls.Add("http://lemonaidbasketballapi.azurewebsites.net/");
            //urls.Add("http://Lemonaidfootballapi.azurewebsites.net/");
            //urls.Add("http://lemonaidsoccerapi.azurewebsites.net/");
            //urls.Add("http://lemonaidsoftballapi.azurewebsites.net/");
            //urls.Add("http://lemonaidwrestlingapi.azurewebsites.net/");
            //urls.Add("http://lemonaidicehockeyapi.azurewebsites.net/");
            //urls.Add("http://lemonaidvolleyballapi.azurewebsites.net/");

            urls.Add("http://lemonaidstagingapi.azurewebsites.net/");
            //urls.Add("http://lemonaidrowingapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaiddivingapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidtennisapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidgolfapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidlacrosseapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidwaterpoloapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidtrackapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidbaseballapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidbasketballapistaging.azurewebsites.net/");
            //urls.Add("http://Lemonaidfootballapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidsoccerapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidsoftballapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidwrestlingapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidicehockeyapistaging.azurewebsites.net/");
            //urls.Add("http://lemonaidvolleyballapistaging.azurewebsites.net/");
            return urls;


        }
        private void SendMailToCoaches()
        { 
            HttpClient client = new HttpClient();
            List<string> baseURLS = GetBaseurls();
            foreach (var b in baseURLS)
            {
                string baseURL = b;// CloudConfigurationManager.GetSetting("BaseURL").ToString();
                client.BaseAddress = new Uri(baseURL);
                string Api = "/coach/sendMailToCoaches";
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response1 = client.GetAsync(Api).Result;
                if (response1.IsSuccessStatusCode)
                {
                }
            }
        }

        private void SendPushNotificationsToCoaches()
        {
            HttpClient client = new HttpClient();

            List<string> baseURLS = GetBaseurls();
            foreach (var b in baseURLS)
            {
                string baseURL = b; //CloudConfigurationManager.GetSetting("BaseURL").ToString();
                client.BaseAddress = new Uri(baseURL);
                string Api = "/coach/SendPushNotificationsToCoaches";
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response1 = client.GetAsync(Api).Result;
                if (response1.IsSuccessStatusCode)
                {
                }
            }
        }

        private void AddBlogs()
        {
            HttpClient client = new HttpClient();

            List<string> baseURLS = GetBaseurls();
            foreach (var b in baseURLS)
            {
                string baseURL = b;// CloudConfigurationManager.GetSetting("BaseURL").ToString();
                client.BaseAddress = new Uri(baseURL);
                string Api = "/BuzzFeed/GetDataFromXMLandInsert";
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response1 = client.GetAsync(Api).Result;
                if (response1.IsSuccessStatusCode)
                {
                }
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("Lemonaid.Worker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("Lemonaid.Worker is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("Lemonaid.Worker has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }
    }
}
