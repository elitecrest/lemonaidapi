﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.IO;
using System.Web;
using System.Net.Mail;
using Lemonaid_web_api.Models;
using System.Configuration;

namespace Lemonaid_web_api.Controllers
{
    public class Athletev4Controller : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse _result = new Utility.CustomResponse();


        [HttpGet]
        public Utility.CustomResponse GetAthleteFavourites_v4(int atheleteId, int offset, int max)
        {

            try
            {
               
                CoachController coachController = new CoachController();
                var coaches = UsersDalV4.GetAtheleteFavourites_v4(atheleteId, offset, max);
                coaches = AddCoachVideos(coaches);

                foreach (var c in coaches)
                {
                    int chatCount = UsersDalV4.GetChatCount(c.CoachId,atheleteId);
                    c.ChatCount = chatCount;
                    //Insert into CaoachAthleteChatCount
                   // Dal2_v3.AddCoachAthleteChatCount(atheleteId, c.CoachId, c.ChatCount, 1, 0);
                }
                var collegeAtheletes = UsersDalV4.GetCollegeAtheletesInAtheleteFavourites_v4(atheleteId, offset, max);
                foreach (var a in collegeAtheletes)
                {
                    int chatCount = UsersDalV4.GetChatCount(a.Id, atheleteId);
                    a.ChatCount = chatCount;
                }
                var frontRushMatches = DAL2V4.GetAthleteFrontRushFavourites(atheleteId, offset, max);
                foreach (var a in frontRushMatches)
                {
                    int chatCount = DAL2V4.GetAthleteFRChatCount(a.Id, atheleteId);
                    a.ChatCount = chatCount;
                }

                //var ChatAdMatches = DAL2V4.GetAthleteChatAdFavourites(atheleteId, offset, max);
                //foreach (var a in frontRushMatches)
                //{
                //    int chatCount = DAL2V4.GetAthleteChatAdChatCount(a.Id, atheleteId);
                //    a.ChatCount = chatCount;
                //}

                List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches = new List<CollegeAtheleteCoachesDTO>();

                collegeAtheleteCoaches = coachController.AddCoachestoList(coaches, collegeAtheleteCoaches);
                collegeAtheleteCoaches = coachController.AddAtheletestoList(collegeAtheletes, collegeAtheleteCoaches);
                collegeAtheleteCoaches = AddFrontRushAdsToList(frontRushMatches, collegeAtheleteCoaches);

                var totalLikesCount = collegeAtheleteCoaches.Count(p => p.Liked);

                foreach (CollegeAtheleteCoachesDTO t in collegeAtheleteCoaches)
                {
                    t.TotalLikedCount = totalLikesCount;
                }

                IEnumerable<CollegeAtheleteCoachesDTO> sortedCollegeAtheleteCoaches = collegeAtheleteCoaches.ToList().OrderByDescending(x => x.Liked);

                IEnumerable<CollegeAtheleteCoachesDTO> enumcollegeAtheleteCoaches = sortedCollegeAtheleteCoaches.Skip(offset).Take(max);


                if (enumcollegeAtheleteCoaches.ToList().Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = enumcollegeAtheleteCoaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Matches;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/GetAthleteFavourites_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        public List<CollegeAtheleteCoachesDTO> AddFrontRushAdsToList(List<AthleteDTO> frontRushMatches, List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches)
        {
            for (int i = 0; i < frontRushMatches.Count; i++)
            {
                CollegeAtheleteCoachesDTO atheletes = new CollegeAtheleteCoachesDTO();
                atheletes.Id = frontRushMatches[i].Id;
                atheletes.Name = frontRushMatches[i].Name;
                atheletes.ProfilePicURL = frontRushMatches[i].ProfilePicURL;
                atheletes.CoverPicURL = frontRushMatches[i].CoverPicURL;
                atheletes.Description = frontRushMatches[i].Description;
                atheletes.AdType = 4;
                atheletes.AtheleteCoachType = 2; //For Front Rush Cards
                atheletes.ChatCount = frontRushMatches[i].ChatCount;
                collegeAtheleteCoaches.Add(atheletes);
            }
            return collegeAtheleteCoaches;
        }

        //Change to Generic Class
        public List<CollegeAtheleteCoachesDTO> AddChatAdsToList(List<AthleteDTO> ChatAdMatches, List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches)
        {
            for (int i = 0; i < ChatAdMatches.Count; i++)
            {
                CollegeAtheleteCoachesDTO atheletes = new CollegeAtheleteCoachesDTO();
                atheletes.Id = ChatAdMatches[i].Id;
                atheletes.Name = ChatAdMatches[i].Name;
                atheletes.ProfilePicURL = ChatAdMatches[i].ProfilePicURL;
                atheletes.CoverPicURL = ChatAdMatches[i].CoverPicURL;
                atheletes.Description = ChatAdMatches[i].Description;
                atheletes.AdType = 5;
                atheletes.AtheleteCoachType = 3; //For Chst Ad Cards
                atheletes.ChatCount = ChatAdMatches[i].ChatCount;
                collegeAtheleteCoaches.Add(atheletes);
            }
            return collegeAtheleteCoaches;
        }

        public List<CoachDTO> AddCoachVideos(List<CoachDTO> Coaches)
        {
           
            foreach (var v in Coaches)
            {
                List<VideoDTO> coachVideos = new List<VideoDTO>();
                List<VideoDTO> totalcoachVideos = new List<VideoDTO>();
                List<int> coachIds = Dal2.GetUniversityCoaches(v.Id);
                foreach (var j in coachIds)
                {
                    coachVideos = Dal2.GetCoachVideos(j);
                    foreach (var Video in coachVideos)
                    {
                        totalcoachVideos.Add(Video);
                    }
                }
               
                v.CoachVideos = totalcoachVideos;
            }
            
            return Coaches;
        }

        [HttpPost]
        public Utility.CustomResponse UpdateFbFriendsandShares_v4(FriendsSharesDTO friendsSharesDto)
        {

            try
            {

                UsersDalV4.UpdateFbFriendsandShares_v4(friendsSharesDto);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/UpdateFbFriendsandShares_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddAthleteBestimes_v4(List<AthleteEventDTO> athleteEventDto)
        {
            try
            {
                UsersDalV4.DeleteAthleteBesttimes_v4(athleteEventDto[1].AtheleteId);
                MetricsDto.SwimmingMetricsDto swimmingMetrics = new MetricsDto.SwimmingMetricsDto();

                for (int i = 0; i < athleteEventDto.Count; i++)
                {
                    var athletes = UsersDalV4.AddAthleteBesttimes_v4(athleteEventDto[i]); 
                 
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athletes;
                    _result.Message = CustomConstants.Athlete_Bestimes_Added;

                }

                for (int i = 0; i < athleteEventDto.Count; i++)
                {
                    swimmingMetrics.AthleteId = athleteEventDto[0].AtheleteId;
                    if (i == 0)
                    {
                        swimmingMetrics.EventType1 = athleteEventDto[i].EventTypeId;
                        swimmingMetrics.BesttimeValue1 = athleteEventDto[i].BesttimeValue;
                    }
                    else if(i == 1)
                    {
                        swimmingMetrics.EventType2 = athleteEventDto[i].EventTypeId;
                        swimmingMetrics.BesttimeValue2 = athleteEventDto[i].BesttimeValue;
                    }
                    else if (i == 2)
                    {
                        swimmingMetrics.EventType3 = athleteEventDto[i].EventTypeId;
                        swimmingMetrics.BesttimeValue3 = athleteEventDto[i].BesttimeValue;
                    }
                  
                    var metrics = DalMetrics.AddSwimmingMetrics(swimmingMetrics);
                }

                    return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AddAthleteBestimes_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddAtheleteMatch_v4(AtheleteMatchDTO atheleteMatchDto)
        {
            try
            {

                var matchId = UsersDalV4.AddAtheleteMatch_v4(atheleteMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (atheleteMatchDto.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AddAthleteMatch_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DeleteAtheleteMatch_v4(AtheleteMatchDTO atheleteMatchDto)
        {

            try
            {

                var matchId = UsersDalV4.DeleteAtheleteMatch_v4(atheleteMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/DeleteAthleteMatch_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse DeleteAtheleteCoaches_v4(int atheleteId)
        { 
            try
            {
                var badges = UsersDalV4.DeleteAtheleteCoaches_v4(atheleteId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/DeleteAthleteCoaches_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
         

        [HttpPost]
        public Utility.CustomResponse AddAthleteInvitation_v4(AtheleteInvitationDTO atheleteInvitationDto)
        {
            try
            {

                var athleteIvnId = UsersDalV4.AddAthleteInvitation_v4(atheleteInvitationDto);
                if (athleteIvnId > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteIvnId;
                    _result.Message = CustomConstants.Athlete_Invitation_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AddAthleteInvitation_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse UpdateAthleteAccept_v4(AtheleteInvitationDTO atheleteInvitationDto)
        {


            try
            {

                var athleteIvnId = UsersDalV4.UpdateAthleteAccept_v4(atheleteInvitationDto);
                if (athleteIvnId > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteIvnId;
                    _result.Message = CustomConstants.Athlete_Accepted_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;

                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/UpdateAthleteAccept_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetBadges_v4(int atheleteId)
        {

            try
            {
                var badges = UsersDalV4.GetAtheletesBlogsBadges_v4(atheleteId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/GetBadges_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse DeleteAtheleteVideo_v4(int atheleteId, int videoId)
        {
            try
            {
                VideoDTO video;
                video = UsersDalV4.DeleteAtheleteVideos_v4(atheleteId, videoId);
                if((video.IsFirstVideoDeleted == 1) && (video.Count > 1))
                {
                    //Do the extraction of video and store it in DB 
                    GetVideoThumbnails(video);
                }

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = videoId;
                _result.Message = CustomConstants.Video_Deleted_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/DeleteAthleteVideo_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }



        }

        private void GetVideoThumbnails(VideoDTO videoResp)
        {
            UploadImage uplImage = new UploadImage();
            //Delete all Video thumbnails 
            UsersDalV5.DeleteAthleteVideoThumbnails(videoResp.Id); 

            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            int totDuration = videoResp.Duration;
            float startPoint = 0;
            float dur = (float)totDuration / 6;
            for (int i = 0; i < 5; i++)
            {
                var frame = startPoint + (float)Math.Round(dur, 2);
                string thumbnailJpeGpath =  HttpContext.Current.Server.MapPath("~/videoThumb" + i + ".jpg");
                ffMpeg.GetVideoThumbnail(videoResp.VideoURL, thumbnailJpeGpath, frame);
                byte[] bytes =  File.ReadAllBytes(thumbnailJpeGpath);
                string base64String =  Convert.ToBase64String(bytes);
                string thumbnailUrl = uplImage.GetImageURL(base64String, ".jpg", "");
                AthleteThumbnailDto athleteThumbnailDto = new AthleteThumbnailDto();
                athleteThumbnailDto.AthleteId = videoResp.AtheleteId;
                athleteThumbnailDto.ThumbnailURL = thumbnailUrl;
                athleteThumbnailDto.VideoId = videoResp.Id;
                //Add Image to the DB 
                UsersDalV5.AddVideosThumbnails(athleteThumbnailDto);
                startPoint = startPoint + (float)Math.Round(dur, 2);
            }
        }

     

         

        [HttpPost]
        public Utility.CustomResponse AddVideos_v4(VideoDTO videoDto)
        {

            try
            {

                var video = UsersDalV4.AddVideos_v4(videoDto);
                if (video > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = video;
                    _result.Message = CustomConstants.Video_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AddVideos_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse UpdateVideoThumbnail_v4(ThumbnailDTO thumbnailDto)
        {

            try
            {

                var video = UsersDalV4.UpdateVideoThumbnail_v4(thumbnailDto);
                if (video.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = video;
                    _result.Message = CustomConstants.Thumbnail_Updated;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/UpdateVideoThumbnail_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
         

        [HttpGet]
        public Utility.CustomResponse GetAllAtheletesByCollegeAtheleteEmail_v4(int atheleteId, double latitude, double longitude, int offset, int max, int locationType, string description, string searchText) // CollegeAthlete email address
        {
            AthleteController atc = new AthleteController();
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }
                List<AthleteDTO> athletes = UsersDalV4.GetAllAthletesByCollegeAthelete_v4(atheleteId, offset, max, description, searchText);

                List<AthleteDTO> newList = new List<AthleteDTO>();


                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = athletes;

                }
                else
                    if (locationType == 2)
                    {
                        foreach (AthleteDTO t in athletes)
                        {
                            double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                            double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                            double distance = atc.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(t);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            foreach (AthleteDTO t in athletes)
                            {
                                double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                                double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                                double distance = atc.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                if (distance <= 5)
                                {
                                    newList.Add(t);
                                }
                            }
                        }


                if (newList.Count > 0)
                {
                    //Add BestTimes
                    newList = atc.AddBesttimesToList(newList);
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newList = atc.GetAdAthletes(ads, newList);
                    //Add Athelete Videos
                    newList = atc.AddVideosToList(newList);

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = newList;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/GetAllAthletesByCollegeAthleteEmail_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetCollegeAthleteFavourites_v4(int atheleteId, int offset, int max)
        {

            try
            {
                AthleteController athCtrl = new AthleteController();
                var atheletes = UsersDalV4.GetCollegeAtheleteFavourites_v4(atheleteId, offset, max);
                foreach (var a in atheletes)
                {
                    int chatCount = UsersDalV4.GetChatCount(a.Id,atheleteId);
                    a.ChatCount = chatCount;
                }

                atheletes = athCtrl.AddBesttimesToList(atheletes);
                atheletes = athCtrl.AddVideosToList(atheletes);
                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Matches;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/GetCollegeAthleteFavourites_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddAtheleteandCollegeAthleteMatch_v4(AtheleteCollegeAtheleteDTO atheleteMatchDto)
        {
            try
            {

                var matchId = UsersDalV4.AddAtheleteandCollegeAthleteMatch_v4(atheleteMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (atheleteMatchDto.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AddAthleteandCollegeAthleteMatch_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddCollegeAtheleteandAtheleteMatch_v4(AtheleteCollegeAtheleteDTO atheleteMatchDto)
        {

            try
            {

                var matchId = UsersDalV4.AddCollegeAtheleteandAtheleteMatch_v4(atheleteMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (atheleteMatchDto.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AddCollegeAthleteandAthleteMatch_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse DeleteAtheleteandCollegeAthleteMatch_v4(AtheleteCollegeAtheleteDTO atheleteCollegeAtheleteDto)
        {

            try
            {

                var matchId = UsersDalV4.DeleteAtheleteandCollegeAthleteMatch_v4(atheleteCollegeAtheleteDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/DeleteAthleteandCollegeAthleteMatch_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DeleteCollegeAtheleteandAtheleteMatch_v4(AtheleteCollegeAtheleteDTO atheleteCollegeAtheleteDto)
        {


            try
            {

                var matchId = UsersDalV4.DeleteCollegeAtheleteandAtheleteMatch_v4(atheleteCollegeAtheleteDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/DeleteCollegeAthleteandAtheleteMatch_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse AtheleteProfile_v4(string emailaddress,int logintype)
        {
            AthleteController atc = new AthleteController();
            try
            {
                AthleteDTO athleteDto = UsersDalV4.GetAthleteProfile_v4(emailaddress, logintype);
                athleteDto.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athleteDto.Id);
                athleteDto.AtheleteVideos = UsersDalV4.GetAtheleteIdealVideos_v4(emailaddress, logintype);
                int measurementcount = atc.GetMeasurementCount(athleteDto.Height, athleteDto.Height2, athleteDto.Weight, athleteDto.WingSpan, athleteDto.ShoeSize);
                athleteDto.MeasurementCount = measurementcount;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athleteDto;
                _result.Message = CustomConstants.Details_Get_Successfully;
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AtheleteProfile_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }

        [HttpGet]
        public Utility.CustomResponse DeleteCollegeAtheleteAtheletes_v4(int atheleteId)
        {

            try
            {
                var id = UsersDalV4.DeleteCollegeAtheleteAtheletes_v4(atheleteId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = id;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/DeleteCollegeAtheleteAtheletes_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetAllCoachesByLocationWithSearch_V4(int atheleteId, double latitude, double longitude, int offset, int max, int ncaa, int conference, int admission, int locationType, string description, string searchText) // Athlete email address
        {
            AthleteController athleteContr = new AthleteController();
            CoachController coachController = new CoachController();
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }

                var caches = UsersDalV4.GetAllCoachesPagingWithSearch_v4(atheleteId, offset, max, ncaa, conference, admission, description, searchText);

                var newList = new List<CoachDTO>();

                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = caches;

                }
                else
                    if (locationType == 2)
                    {
                        for (int i = 0; i < caches.Count; i++)
                        {
                            double athleteLatitude = (caches[i].Latitude == "") ? 0.0 : Convert.ToDouble(caches[i].Latitude);
                            double athleteLongitude = (caches[i].Longitude == "") ? 0.0 : Convert.ToDouble(caches[i].Longitude);
                            double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(caches[i]);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            for (int i = 0; i < caches.Count; i++)
                            {
                                double athleteLatitude = (caches[i].Latitude == "") ? 0.0 : Convert.ToDouble(caches[i].Latitude);
                                double athleteLongitude = (caches[i].Longitude == "") ? 0.0 : Convert.ToDouble(caches[i].Longitude);
                                double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                if (distance <= 5)
                                {
                                    newList.Add(caches[i]);
                                }
                            }
                        }




                if (newList.Count > 0)
                {
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newList = coachController.GetAdCoaches(ads, newList);

                }
                //Get College atheletes and coaches
                List<AthleteDTO> collegeAtheletes = UsersDalV4.GetCollegeAtheletes_v4(searchText, atheleteId);
                int totalcount = collegeAtheletes.Count + newList.Count;

                for (int i = 0; i < collegeAtheletes.Count; i++)
                {
                    collegeAtheletes[i].TotalCount = totalcount;
                }


                List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches = new List<CollegeAtheleteCoachesDTO>();
                if (newList.Count > 0)
                {
                    for (int i = 0; i < newList.Count; i++)
                    {
                        newList[i].TotalCount = totalcount;
                    }
                    collegeAtheleteCoaches = coachController.AddCoachestoList(newList, collegeAtheleteCoaches);
                }
                collegeAtheleteCoaches = coachController.AddAtheletestoList(collegeAtheletes, collegeAtheleteCoaches);
                List<QuestionaireDTO> questionaireDto = UsersDalV4.GetQuestionaire_v4(atheleteId);


                IEnumerable<CollegeAtheleteCoachesDTO> enumcollegeAtheleteCoaches = collegeAtheleteCoaches.Skip(offset).Take(max - offset);
                collegeAtheleteCoaches = enumcollegeAtheleteCoaches.ToList();
                collegeAtheleteCoaches = coachController.AddQuestionairetoList(questionaireDto, collegeAtheleteCoaches);

                if (collegeAtheleteCoaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = collegeAtheleteCoaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/GetAllCoachesByLocationWithSearch_V4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }


        [HttpPost]
        public Utility.CustomResponse AddCoachMatch_v4(AtheleteMatchDTO atheleteMatchDto)
        {

            try
            {

                var matchId = UsersDalV4.AddCoachMatch_v4(atheleteMatchDto);
                matchId = (atheleteMatchDto.Status == false) ? 0 : matchId;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AddCoachMatch_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddAthlete_v4(AthleteDTO athleteDto)
        {

            AthleteController atc = new AthleteController();
            try
            {
                if (athleteDto.AtheleteType == 0)
                {
                    athleteDto.AtheleteType = Convert.ToInt32(Utility.AtheleteType.HighSchoolAthelete);
                    athleteDto.Active = false;
                }

                var athelete = UsersDalV4.AddAthlete_v4(athleteDto);
                athelete.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athelete.Id);
                athelete.AtheleteVideos = UsersDalV4.GetAtheleteIdealVideos_v4(athelete.Emailaddress, athelete.LoginType);
                int measurementcount = atc.GetMeasurementCount(athelete.Height, athelete.Height2, athelete.Weight, athelete.WingSpan, athelete.ShoeSize);
                athelete.MeasurementCount = measurementcount;
                if (athelete.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athelete;
                    _result.Message = CustomConstants.Athlete_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AddAthlete_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse DeleteCoachMatch_v4(AtheleteMatchDTO atheleteMatchDto)
        {

            try
            {

                var matchId = UsersDalV4.DeleteCoachMatch_v4(atheleteMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed; 

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/DeleteCoachMatch_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ChangeStatusOfAthleteBadges_v4(string atheleteId, string typeId, string count,int status)
        {

            AtheleteBadgesDTO athleteBadges = new AtheleteBadgesDTO();

            try
            {

                athleteBadges.AtheleteId = Convert.ToInt32(atheleteId);
                athleteBadges.BadgeTypeId = typeId;
                athleteBadges.BadgeCount = count;
                athleteBadges.Status = status.ToString();

                var badges = UsersDalV4.UpdateAthleteBadgesStatus_v4(athleteBadges);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/ChangeStatusOfAthleteBadges_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse AtheleteSignIn_v4(AtheleteInstituteAccept atheleteInstituteAccept)
        {

            try
            {
                if (atheleteInstituteAccept.InstituteEmail == string.Empty)
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful; 
                    _result.Message = CustomConstants.No_HeadCoach;
                }
                else
                {
                    string message = "Dear " + atheleteInstituteAccept.InstituteEmail + ", An athlete with emailaddress " + atheleteInstituteAccept.AtheleteEmail + " has indicated they are a part of your team. Please accept the athlete and add them to your roster.";
                    string athleteName = string.Empty, athleteEmailAddress = string.Empty; string deviceName = string.Empty; string deviceId = string.Empty; int loginType = 0; string username = string.Empty;
                    UsersDalV4.GetAthleteDetails_v4(atheleteInstituteAccept.AtheleteId, ref athleteName, ref athleteEmailAddress, ref deviceId, ref deviceName, ref loginType, ref username);
                    DAL2V6.PushNotification(message, deviceName);


                    StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailToUniversityCoachAcceptance_v4.html"));
                    string readFile = reader.ReadToEnd();
                    var myString = readFile;
                    myString = myString.Replace("$$UniversityEmail$$", atheleteInstituteAccept.InstituteEmail);
                    myString = myString.Replace("$$AtheleteEmail$$", atheleteInstituteAccept.AtheleteEmail);
                    myString = myString.Replace("$$AtheleteId$$", atheleteInstituteAccept.AtheleteId.ToString());
                    myString = myString.Replace("$$InstituteId$$", atheleteInstituteAccept.InstituteId.ToString());
                    string baseURL = ConfigurationManager.AppSettings["BaseURL"];
                    myString = myString.Replace("$$BaseURL$$", baseURL);

                    int atheleteId = UsersDal.UpdateAtheleteToCollegeAthlete(atheleteInstituteAccept.AtheleteId, 0); //After verification , instituteid will be updated.
                    int athleTran = Dal2.UpdateAtheleteTransition(atheleteInstituteAccept.AtheleteId, 1);

                    //Notifications Insertion 
                    NotificationDTO not = new NotificationDTO();
                    not.Status = 0;
                    int headCoachId = UsersDal.GetHeadCoachForInstitute(atheleteInstituteAccept.InstituteId);
                    not.UserID = headCoachId;
                    not.SenderId = atheleteInstituteAccept.AtheleteId;
                    not.NotificationId = 4;
                    not.ShortMessage = message;
                    not.LongMessage = myString;
                    not.SenderType = 0;
                    not.ReceiverType = 1;
                    UsersDal.AddNotifications(not);


                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheleteId;
                    _result.Message = CustomConstants.Notification_Sent;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AtheleteSignIn_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AtheleteSignInFromBadges_v4(AtheleteInstituteAccept atheleteInstituteAccept)
        {

            try
            {
                if (atheleteInstituteAccept.InstituteEmail == string.Empty)
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_HeadCoach;
                }
                else
                {
                    string message = "Dear " + atheleteInstituteAccept.InstituteEmail + ", An athlete with emailaddress " + atheleteInstituteAccept.AtheleteEmail + " has indicated they are a part of your team. Please accept the athlete and add them to your roster.";
                    string athleteName = string.Empty, athleteEmailAddress = string.Empty; string deviceName = string.Empty; string deviceId = string.Empty; int loginType = 0; string username = string.Empty;
                    UsersDalV4.GetAthleteDetails_v4(atheleteInstituteAccept.AtheleteId, ref athleteName, ref athleteEmailAddress, ref deviceId, ref deviceName, ref loginType, ref username);
                    DAL2V6.PushNotification(message, deviceName);

                    StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailToUniversityCoachAcceptance_v4.html"));
                    string readFile = reader.ReadToEnd();
                    var myString = readFile;
                    myString = myString.Replace("$$UniversityEmail$$", atheleteInstituteAccept.InstituteEmail);
                    myString = myString.Replace("$$AtheleteEmail$$", atheleteInstituteAccept.AtheleteEmail);
                    myString = myString.Replace("$$AtheleteId$$", atheleteInstituteAccept.AtheleteId.ToString());
                    myString = myString.Replace("$$InstituteId$$", atheleteInstituteAccept.InstituteId.ToString());
                    string baseURL = ConfigurationManager.AppSettings["BaseURL"];
                    myString = myString.Replace("$$BaseURL$$", baseURL);

                    // int atheleteId = UsersDAL.UpdateAtheleteToCollegeAthlete(atheleteInstituteAccept.AtheleteId, 0); //After verification , instituteid will be updated.
                    int atheleteId = Dal2.UpdateAtheleteTransition(atheleteInstituteAccept.AtheleteId, 1);

                    //Notifications Insertion 
                    NotificationDTO not = new NotificationDTO();
                    not.Status = 0;
                    int headCoachId = UsersDal.GetHeadCoachForInstitute(atheleteInstituteAccept.InstituteId);
                    not.UserID = headCoachId;
                    not.SenderId = atheleteInstituteAccept.AtheleteId;
                    not.NotificationId = 4;
                    not.ShortMessage = message;
                    not.LongMessage = myString;
                    not.SenderType = 0;
                    not.ReceiverType = 1;
                    UsersDal.AddNotifications(not);

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    //  Result.Response = atheleteId;
                    _result.Message = CustomConstants.Notification_Sent;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/AtheleteSignInFromBadges_v4", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpGet]
        public Utility.CustomResponse EmailToAthlete_v4(int athleteId, int coachId)
        {
            string athleteName = string.Empty, athleteEmailAddress = string.Empty, deviceId = string.Empty, deviceName = string.Empty; int loginType = 0; string username = string.Empty;
            UsersDalV4.GetAthleteDetails_v4(athleteId, ref athleteName, ref athleteEmailAddress, ref deviceId, ref deviceName,ref  loginType, ref username);
            SendEmailToAthlete_v4(athleteEmailAddress, athleteName, athleteId, coachId);
            _result.Status = Utility.CustomResponseStatus.Successful;
            _result.Message = CustomConstants.Notification_Sent;
            return _result;
        }

        private void SendEmailToAthlete_v4(string emailAddress, string athleteName, int athleteId, int coachId)
        {
            try
            {

                string message = "Dear " + emailAddress + ", Great news. A college coach is interested in you for their program! Actually very interested.They just requested to see video from you.";
                string athleteName1 = string.Empty, athleteEmailAddress = string.Empty; string deviceName = string.Empty; string deviceId = string.Empty; int loginType = 0; string username = string.Empty;
                UsersDalV4.GetAthleteDetails_v4(athleteId, ref athleteName1, ref athleteEmailAddress, ref deviceId, ref deviceName, ref loginType, ref username);
                DAL2V6.PushNotification(message, deviceName);

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailToAthlete.html"));
                string readFile = reader.ReadToEnd();
                var myString = readFile;
                myString = myString.Replace("$$AthleteName$$", athleteName);
  
                CoachProfileDTO coach = UsersDal.GetCoachProfile(coachId);
                //Notifications Insertion 
                NotificationDTO not = new NotificationDTO();
                not.Status = 0;
                not.UserID = athleteId;
                not.SenderId = coach.InstituteId;
                not.NotificationId = 3;
                not.SenderType = 1;
                not.ReceiverType = 0;
                not.ShortMessage = message;
                not.LongMessage = myString;
                UsersDal.AddNotifications(not);

            }
            catch (SmtpException ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/SendEmailToAthlete_v4", "", ex.Message, "Exception", 0);
                Message = ex.Message;

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/SendEmailToAthlete_v4", "", ex.Message, "Exception", 0);
                Message = ex.Message;

            }
        }

        [HttpPost]
        public Utility.CustomResponse SendEmailToAthleteCoach_v4(AtheleteSocialDTO athleteSocial)
        {

            AtheleteBadgesDTO athleteBadges = new AtheleteBadgesDTO();
            athleteBadges.AtheleteId = athleteSocial.AtheleteId;
            athleteBadges.Status = "Pending for Approval";
            //UsersDal.AddAthleteBadges(athleteBadges, athleteSocial);  
            SendBadgesToAthleteCoach_v4(athleteSocial);
            _result.Status = Utility.CustomResponseStatus.Successful;
            _result.Message = CustomConstants.Notification_Sent;
            return _result;
        }

        private void SendBadgesToAthleteCoach_v4(AtheleteSocialDTO athleteSocialInfo)
        {
            string badgeName = string.Empty;

            SupportMailDTO support; 
            support = UsersDal.GetSupportMails("SupportEmail");
            MailMessage msg = new MailMessage(); 
            try
            {

                if (athleteSocialInfo.BadgeTypeId == 1)
                {
                    badgeName = "All American";
                }
                else if (athleteSocialInfo.BadgeTypeId == 2)
                {
                    badgeName = "Academic All American";
                }
                else if (athleteSocialInfo.BadgeTypeId == 3)
                {
                    badgeName = "Captain";
                }
                else if (athleteSocialInfo.BadgeTypeId == 4)
                {
                    badgeName = "SwimSwamTop";
                }

                string athleteName = string.Empty, athleteEmailAddress = string.Empty; string deviceName = string.Empty; string deviceId = string.Empty; int loginType = 0; string username = string.Empty;
                UsersDalV4.GetAthleteDetails_v4(athleteSocialInfo.AtheleteId, ref athleteName, ref athleteEmailAddress, ref deviceId, ref deviceName,ref loginType, ref username);
                // UsersDal.PushNotification(message, DeviceName);


                string baseURL = ConfigurationManager.AppSettings["BaseURL"];

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailToCoachV4.html"));
                string readFile = reader.ReadToEnd();
                string myString;
                myString = readFile;
                myString = myString.Replace("$$CoachName$$", athleteSocialInfo.CoachName);
                myString = myString.Replace("$$Name$$", athleteSocialInfo.AthleteName);
                myString = myString.Replace("$$AtheleteId$$", Convert.ToString(athleteSocialInfo.AtheleteId));
                myString = myString.Replace("$$TypeId$$", Convert.ToString(athleteSocialInfo.BadgeTypeId));
                myString = myString.Replace("$$Count$$", Convert.ToString(athleteSocialInfo.Count));
                myString = myString.Replace("$$BadgeName$$", badgeName);
                myString = myString.Replace("$$BaseURL$$", baseURL);

                ClubCoachDTO clubCoach = UsersDal.GetClubCoachProfile(athleteSocialInfo.CoachEmailId);

                if (clubCoach.Id > 0)
                {
                    string message = "Dear " + athleteSocialInfo.CoachEmailId + ", Can you please confirm whether an athlete with name " + athleteSocialInfo.AthleteName + " has badge " + badgeName + " with count " + Convert.ToString(athleteSocialInfo.Count) + ".";
                  

                    //Notifications Insertion 
                    NotificationDTO not = new NotificationDTO();
                    not.Status = 0;
                    not.UserID = clubCoach.Id;//Get ClubCoach Id
                    not.SenderId = athleteSocialInfo.AtheleteId;
                    not.NotificationId = 5;
                    not.ShortMessage = message;
                    not.LongMessage = myString;
                    not.SenderType = 0;
                    not.ReceiverType = 2;
                    not.BadgeTypeId = athleteSocialInfo.BadgeTypeId;
                    not.BadgeCount = athleteSocialInfo.Count;
                    UsersDal.AddNotifications(not); 
                  //  UsersDal.PushNotification(message, clubCoach.DeviceId);
                }
                else
                {
                    SendGridMailModel sendGridModel = new SendGridMailModel();
                    sendGridModel.toMail = new List<string>();
                    sendGridModel.toMail.Add(athleteSocialInfo.CoachEmailId);
                    sendGridModel.fromMail = support.EmailAddress;
                    sendGridModel.fromName = "Lemonaid";
                    sendGridModel.subject = " Athlete Badges to coaches";
                    sendGridModel.Message = myString;
                    sendGridModel.email = support.EmailAddress;
                    sendGridModel.password = support.Password;
                    SendGridSMTPMail.SendEmail(sendGridModel);
                    //smtpMail.Send(msg);
                    EmailLogModel emaillog = new EmailLogModel();
                    emaillog.AddEmailLog(support.EmailAddress, athleteSocialInfo.CoachEmailId, msg.Subject, "Mail", 1);

                }

            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(support.EmailAddress, athleteSocialInfo.CoachEmailId, msg.Subject, "Mail", 0);

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athletev4/SendBadgesToAthleteCoach_v4", "", ex.Message, "Exception", 0);
                Message = ex.Message;

            }
        }
    }
}
