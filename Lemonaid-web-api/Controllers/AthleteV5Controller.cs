﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http; 
using System.Xml.XPath;
using System.IO;  
using Microsoft.WindowsAzure.MediaServices.Client;
 
 

namespace Lemonaid_web_api.Controllers
{
    public class AthleteV5Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        public string WrapperApi = ConfigurationManager.AppSettings["WrapperBaseURL"];

        [HttpPost]
        public Utility.CustomResponse AddAthlete_v5(AthleteDTO athleteDto)
        {

            AthleteController atc = new AthleteController();
            try
            {

                int athleteSportId = AddAthleteToWrapperDB(athleteDto);
                if (athleteSportId > 0)
                {
                    if (athleteDto.AtheleteType == 0)
                    {
                        athleteDto.AtheleteType = Convert.ToInt32(Utility.AtheleteType.HighSchoolAthelete);
                        athleteDto.Active = false;
                    }

                    var athelete = UsersDalV5.AddAthlete_v5(athleteDto);
                    athelete.SportId = athleteDto.SportId;
                    athelete.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athelete.Id);
                    athelete.AtheleteVideos = UsersDalV4.GetAtheleteIdealVideos_v4(athelete.Emailaddress, athelete.LoginType);
                    int measurementcount = atc.GetMeasurementCount(athelete.Height, athelete.Height2, athelete.Weight, athelete.WingSpan, athelete.ShoeSize);
                    athelete.MeasurementCount = measurementcount;
                    if (athelete.Id > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = athelete;
                        _result.Message = CustomConstants.Athlete_Added;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.Already_Exists;
                    }

                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/AddAthlete_v5", "", ex.Message, "Exception", 0);
                return _result;
            }
        }

        private int AddAthleteToWrapperDB(AthleteDTO athleteDto)
        {
            int athleteSportId = 0;

            HttpClient client = new HttpClient();
            string wrapperUrl = ConfigurationManager.AppSettings["WrapperBaseURL"];
            client.BaseAddress = new Uri(wrapperUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            AthleteWrapperDTO athlete = new AthleteWrapperDTO();
            athlete.Emailaddress = athleteDto.Emailaddress;
            athlete.LoginType = athleteDto.LoginType;
            athlete.UserName = athleteDto.UserName;
            athlete.DeviceType = athleteDto.DeviceType;
            athlete.DeviceId = athleteDto.DeviceId;
            athlete.Id = 0;
            var response = client.PostAsJsonAsync("Athlete/AddAthlete", athleteDto).Result;
            if (response.IsSuccessStatusCode)
            {
               Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;

                if(Convert.ToInt32(res.Response) > 0)
                {
                    AthleteSportsDTO athleteSports = new AthleteSportsDTO();
                    athleteSports.Id = 0;
                    athleteSports.SportId = 1;// athleteDto.SportId;
                    athleteSports.AthleteId =  Convert.ToInt32(res.Response);
                    var response1 = client.PostAsJsonAsync("Athlete/AddAthleteSport", athleteSports).Result;
                    if (response1.IsSuccessStatusCode)
                    {
                      Utility.CustomResponse res1 = response1.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                      athleteSportId = Convert.ToInt32(res1.Response);
                    }
                }

            }

            return athleteSportId;
        }

        [HttpGet]
        public Utility.CustomResponse InsertToWrapperDb()
        {
             
            try
            {
                List<string> emails = UsersDalV5.GetAthleteEmailaddress();
                List<string> usernames = UsersDalV5.GetAthleteUsername();
                for (int i = 0; i < emails.Count;i++ )
                {
                    UsersDalV5.InsertEmails(emails[i]);
                }
                for (int j = 0; j < emails.Count; j++)
                {
                    UsersDalV5.InsertUsernames(usernames[j]);
                }

                _result.Status = Utility.CustomResponseStatus.Successful; 
                _result.Message = CustomConstants.Details_Get_Successfully;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/InsertToWrapperDb", "", ex.Message, "Exception", 0);
                return _result;
            }


        }


        [HttpGet]
        public Utility.CustomResponse AtheleteProfile_v5(string emailaddress, int logintype)
        {
            AthleteController atc = new AthleteController();
            try
            {
                AthleteDTO athleteDto = UsersDalV5.GetAthleteProfile_v5(emailaddress, logintype);
                athleteDto.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athleteDto.Id);
                athleteDto.AtheleteVideos = UsersDalV4.GetAtheleteIdealVideos_v4(emailaddress, logintype);
                int measurementcount = atc.GetMeasurementCount(athleteDto.Height, athleteDto.Height2, athleteDto.Weight, athleteDto.WingSpan, athleteDto.ShoeSize);
                athleteDto.MeasurementCount = measurementcount;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athleteDto;
                _result.Message = CustomConstants.Details_Get_Successfully;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/AtheleteProfile_v5", "", ex.Message, "Exception", 0);
                return _result;
            }


        }


        public string GetStateWithLatLong(string latitude, string longitude)
        {
            string state = string.Empty;
            string url = "http://maps.google.com/maps/api/geocode/xml?latlng=" + latitude + "," + longitude + "&sensor=false";

            WebResponse response;
            bool isGeocoded = true;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            response = request.GetResponse();
           if(response != null)
            {
                var document = new XPathDocument(response.GetResponseStream());
                XPathNavigator navigator = document.CreateNavigator();

                // get response status
                XPathNodeIterator statusIterator = navigator.Select("/GeocodeResponse/status");
                while (statusIterator.MoveNext())
                {
                    if (statusIterator.Current.Value != "OK")
                    {
                        isGeocoded = false;
                    }
                }

                // get results
                if (isGeocoded)
                {
                    XPathNodeIterator resultIterator = navigator.Select("/GeocodeResponse/result");
                    if (resultIterator.MoveNext())
                    {  
                        XPathNodeIterator formattedAddressIterator = resultIterator.Current.Select("formatted_address");
                        if (formattedAddressIterator.MoveNext())
                        {

                            var address = formattedAddressIterator.Current.Value.Trim();
                            string[] stradd = address.Split(',');
                            string stateZip = stradd[stradd.Length - 2].Trim();
                            string[] statezipSpace = stateZip.Split(' ');
                            state = statezipSpace[0];
                        }
                    }

                }

            }

            return state;
        }


        [HttpGet]
        public Utility.CustomResponse UpdateStatesToAthletes()
        {

            try
            {
                string state;
                List<AthleteDTO> athletes = UsersDalV5.GetAthleteLatLongValues();
                for (int i = 0; i < athletes.Count; i++)
                {
                   state = GetStateWithLatLong(athletes[i].Latitude, athletes[i].Longitude);
                   UsersDalV5.UpdateStateToAthlete(state, Convert.ToInt32(athletes[i].Id));
                }
             
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Details_Get_Successfully;
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/UpdateStatesToAthletes", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetAllCoachesByLocationWithSearch_V5(int atheleteId, double latitude, double longitude, int offset, int max, int ncaa, int conference, int admission, int locationType, string description, string searchText) // Athlete email address
        {
            AthleteController athleteContr = new AthleteController();
            CoachController coachController = new CoachController();
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }

                var caches = UsersDalV5.GetAllCoachesPagingWithSearch_v5(atheleteId, offset, max, ncaa, conference, admission, description, searchText);

                var newList = new List<CoachDTO>();

                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = caches;

                }
                else
                    if (locationType == 2)
                    {
                        for (int i = 0; i < caches.Count; i++)
                        {
                            double athleteLatitude = (caches[i].Latitude == "") ? 0.0 : Convert.ToDouble(caches[i].Latitude);
                            double athleteLongitude = (caches[i].Longitude == "") ? 0.0 : Convert.ToDouble(caches[i].Longitude);
                            double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(caches[i]);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            for (int i = 0; i < caches.Count; i++)
                            {
                                double athleteLatitude = (caches[i].Latitude == "") ? 0.0 : Convert.ToDouble(caches[i].Latitude);
                                double athleteLongitude = (caches[i].Longitude == "") ? 0.0 : Convert.ToDouble(caches[i].Longitude);
                                double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                if (distance <= 5)
                                {
                                    newList.Add(caches[i]);
                                }
                            }
                        }




                if (newList.Count > 0)
                {
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newList = coachController.GetAdCoaches(ads, newList);

                }
                //Get College atheletes and coaches
                List<AthleteDTO> collegeAtheletes = UsersDalV4.GetCollegeAtheletes_v4(searchText, atheleteId);
                int totalcount = collegeAtheletes.Count + newList.Count;

                for (int i = 0; i < collegeAtheletes.Count; i++)
                {
                    collegeAtheletes[i].TotalCount = totalcount;
                }


                List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches = new List<CollegeAtheleteCoachesDTO>();
                List<QuestionaireDTO> questionaireDto = UsersDalV5.GetQuestionaire_v5(atheleteId,"HA");
                collegeAtheleteCoaches = coachController.AddQuestionairetoList(questionaireDto, collegeAtheleteCoaches);
                if (newList.Count > 0)
                {
                    for (int i = 0; i < newList.Count; i++)
                    {
                        newList[i].TotalCount = totalcount;
                    }
                    collegeAtheleteCoaches = coachController.AddCoachestoList(newList, collegeAtheleteCoaches);
                }
                collegeAtheleteCoaches = coachController.AddAtheletestoList(collegeAtheletes, collegeAtheleteCoaches); 

                IEnumerable<CollegeAtheleteCoachesDTO> enumcollegeAtheleteCoaches = collegeAtheleteCoaches.Skip(offset).Take(max - offset);  
                collegeAtheleteCoaches = enumcollegeAtheleteCoaches.ToList();

                if (collegeAtheleteCoaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = collegeAtheleteCoaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/GetAllCoachesByLocationWithSearch_V5", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse AddAtheleteQuestionaire_v5(QuestionaireDTO questionaireDto)
        {

            try
            {

                var questionaireid = UsersDalV5.AddAtheleteQuestionaire(questionaireDto);
                _result.Response = questionaireid;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/AddAtheleteQuestionaire_v5", "", ex.Message, "Exception", 0);
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse States()
        {

            try
            {
                var states = UsersDalV5.GetStates();        
                if (states.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = states;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/States", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetAllAtheletesByCollegeAtheleteEmail_v5(int atheleteId, double latitude, double longitude, int offset, int max, int locationType, string description, string searchText) // CollegeAthlete email address
        {
            AthleteController atc = new AthleteController();
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }
                List<AthleteDTO> athletes = UsersDalV4.GetAllAthletesByCollegeAthelete_v4(atheleteId, offset, max, description, searchText);

                List<AthleteDTO> newList = new List<AthleteDTO>();
                List<AthleteDTO> listWithQuestionaire = new List<AthleteDTO>();
                CoachV5Controller coachv5Ctrl = new CoachV5Controller();
                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = athletes;

                }
                else
                    if (locationType == 2)
                    {
                        foreach (AthleteDTO t in athletes)
                        {
                            double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                            double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                            double distance = atc.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(t);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            foreach (AthleteDTO t in athletes)
                            {
                                double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                                double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                                double distance = atc.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                if (distance <= 5)
                                {
                                    newList.Add(t);
                                }
                            }
                        }


                List<QuestionaireDTO> questionaireDto = UsersDalV5.GetQuestionaire_v5(atheleteId, "CA");
                listWithQuestionaire = coachv5Ctrl.AddQuestionairetoList(questionaireDto, listWithQuestionaire);

                if (newList.Count > 0)
                {
                    //Add BestTimes
                    newList = atc.AddBesttimesToList(newList);
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newList = atc.GetAdAthletes(ads, newList);
                    //Add Athelete Videos
                    newList = atc.AddVideosToList(newList); 
                    listWithQuestionaire = coachv5Ctrl.AddAtheletestoList(newList, listWithQuestionaire); 
                }
                if (listWithQuestionaire.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = listWithQuestionaire;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/GetAllAtheletesByCollegeAtheleteEmail_v5", "", ex.Message, "Exception", 0);
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse AddVideos_v5(VideoDTO videoDto)
        {

            try
            {

                var video = UsersDalV5.AddVideos_v5(videoDto);
                if (video.Id > 0)
                {  
                    _result.Response = video;
                    _result.Message = CustomConstants.Video_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/AddVideos_v5", "", ex.Message, "Exception", 0);
                return _result;
            }

        }
         
        public string UploadImage1(string fileName, string path, string ext, Stream inputStream)
        {
            string streamingUrl = string.Empty;
            string account = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountName"];
            string key = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountKey"];


            CloudMediaContext context = new CloudMediaContext(account, key);

            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);

            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));

            assetFile.Upload(path);

            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 365;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            if (streamingAsset != null)
            {
                var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
                    AccessPermissions.Read);
             
                var assetFiles = streamingAsset.AssetFiles.ToList();

                var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
                if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
                {
                    var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                    var mp4Uri = new UriBuilder(locator.Path);
                    mp4Uri.Path += "/" + streamingAssetFile.Name;
                    streamingUrl = mp4Uri.ToString();
                }
               
            }
            return streamingUrl;
        }

        public int AddImageToDb(AthleteThumbnailDto athleteThumbnailDto)
        {
            Utility.CustomResponse result = new Utility.CustomResponse();
            int id = 0;
            string baseUrl = ConfigurationManager.AppSettings["BaseURL"] ;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.PostAsJsonAsync("Athletev5/AddVideosThumbnails", athleteThumbnailDto).Result;
            if (response.IsSuccessStatusCode)
            {
                var res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                id = Convert.ToInt32(res.Response);
                result.Status = Utility.CustomResponseStatus.Successful;
                result.Response = res.Response;
                result.Message = CustomConstants.Video_Added;

            }
            return id;
        }

        [HttpPost]
        public Utility.CustomResponse AddVideosThumbnails(AthleteThumbnailDto athleteThumbnailDto)
        { 
            try
            { 
              
                var video = UsersDalV5.AddVideosThumbnails(athleteThumbnailDto);
                if (video > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = video;
                    _result.Message = CustomConstants.Video_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = 0;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/AddVideosThumbnails", "", ex.Message, "Exception", 0);
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse DeleteAthleteVideoThumbnails(int videoId)
        {

            try
            {
                var videoid = UsersDalV5.DeleteAthleteVideoThumbnails(videoId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = videoid;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("AtheleteV5/DeleteAthleteVideoThumbnails", "", ex.Message, "Exception", 0);
                return _result;
            }

        }
    }
}
