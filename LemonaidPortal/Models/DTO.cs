﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LemonaidPortal.Models
{
    public partial class BlogDTO
    {
        public int Id { get; set; }
        public int CoachId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string ImageURL { get; set; }
        public string URL { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
    }
}