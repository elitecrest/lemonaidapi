﻿using System;
using System.Linq;
using System.Web.Http;
using Lemonaid_web_api.Models;
using System.IO;
using System.Configuration;
using Microsoft.WindowsAzure.MediaServices.Client; 
using Microsoft.WindowsAzure.Storage;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Lemonaid_web_api.Controllers
{
    public class UploadController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpPost]
        public Utility.CustomResponse UploadVideoFiles(VideoDTO video)
        { 
            try
            {
                Stream stream = System.Web.HttpContext.Current.Request.InputStream;  
                string videoUrl = Streamaudio(stream, video.VideoFormat);
                var videoInfo = UsersDalV5.AddVideos_v5(video);
                if ((videoInfo.VideoNumber == videoInfo.Firstvideonumber) || videoInfo.CountAthleteVideos == 1)
                {
                    GetVideoThumbnails(videoInfo);
                }
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = videoInfo.Id;
                _result.Message = CustomConstants.Athlete_Deleted_Sport;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Upload/UploadFiles", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            } 
        }


        public string Streamaudio(Stream stream, string format)
        {
            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);
            var fileStream = File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();
            var ext = format;
            Stream fileStreambanners = stream;
            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string videoUrl = UploadImage1(fileNamebanners, path, ext, fileStreambanners);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            return videoUrl;
        }


        public string UploadImage1(string fileName, string path, string ext, Stream inputStream)
        {
            string account = ConfigurationManager.AppSettings["MediaServicesAccountName"];
            string key = ConfigurationManager.AppSettings["MediaServicesAccountKey"];
            CloudMediaContext context = new CloudMediaContext(account, key);
            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);
            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));
            assetFile.Upload(path);
            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 10000;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive), AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;
        }

        private void GetVideoThumbnails(VideoDTO videoResp)
        {
            //Delete all Video thumbnails 
            DeleteAthleteThumbnails(videoResp.Id);  

            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            int totDuration = videoResp.Duration;
            float startPoint = 0;
            float dur = (float)totDuration / 6;
            for (int i = 0; i < 5; i++)
            {
                var frame = startPoint + (float)Math.Round(dur, 2);
                string thumbnailJpeGpath = System.Web.HttpContext.Current.Server.MapPath("~/videoThumb" + i + ".jpg");
                ffMpeg.GetVideoThumbnail(videoResp.VideoURL, thumbnailJpeGpath, frame);
                byte[] bytes = File.ReadAllBytes(thumbnailJpeGpath);
                string base64String = Convert.ToBase64String(bytes);
                string thumbnailUrl = GetImageUrl(base64String, ".jpg", "");
                AthleteThumbnailDto athleteThumbnailDto = new AthleteThumbnailDto();
                athleteThumbnailDto.AthleteId = videoResp.AtheleteId;
                athleteThumbnailDto.ThumbnailURL = thumbnailUrl;
                athleteThumbnailDto.VideoId = videoResp.Id;
                //Add Image to the DB 
                AddImageToDb(athleteThumbnailDto);
                startPoint = startPoint + (float)Math.Round(dur, 2);
            }
        }

        private void DeleteAthleteThumbnails(int videoId)
        {
            var videoid = UsersDalV5.DeleteAthleteVideoThumbnails(videoId);
        }


        public string GetImageUrl(string binary, string format, string contentType)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();
                string filename = "" + r.Next(999999) + DateTime.Now.Millisecond + r.Next(99999) + format;
                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");
                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();
                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });
                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);
                // Create or overwrite the "myblob" blob with contents from a local file.
                byte[] binarydata = Convert.FromBase64String(binary);
                blockBlob.Properties.ContentType = contentType;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);
                //UploadFromStream(fileStream);
                //}
                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);
                imageurl = blockBlob1.Uri.ToString();
            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                _result.Message = "Failed to add image";
            }
            return imageurl;
        }

        private int AddImageToDb(AthleteThumbnailDto athleteThumbnailDto)
        { 
            var id = UsersDalV5.AddVideosThumbnails(athleteThumbnailDto);
            return id;
        }
    }
}
