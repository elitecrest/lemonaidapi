﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data; 
using System.Configuration; 
using System.IO; 
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json.Linq;
using Microsoft.WindowsAzure.Storage;
using System.Data.SqlClient;

namespace Lemonaid_web_api.Models
{
    public static class UsersDal
    {
        public static SqlConnection ConnectToDb()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["LemonaidDB"].ConnectionString;
            var sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        public static SqlConnection ConnectToWrapperDb()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["LemonaidWrapperDB"].ConnectionString;
            var sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        

        internal static List<AthleteDTO> GetCoachesFavourites(string emailaddress, int offset, int max)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();
            DateTime dtAtheleteDate;
            DateTime dtInstituteDate;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstituteFavourites]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailAddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailAddress.Direction = ParameterDirection.Input;
                    emailAddress.Value = emailaddress;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        dtAtheleteDate = (myData["AtheleteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["AtheleteDate"];
                        dtInstituteDate = (myData["InstituteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InstituteDate"];
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.Liked = ((int)myData["Likes"] != 0);
                        if (v.Liked)
                        { v.FavDate = (dtAtheleteDate > dtInstituteDate) ? dtAtheleteDate : dtInstituteDate; }
                        else
                        { v.FavDate = dtInstituteDate; }
                        v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                        v.AtheleteId = (int)myData["AtheleteId"];
                        v.CoachId = (int)myData["CoachId"];
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.UniversityName = myData["UniversityName"].ToString();
                        v.UniversityProfilePic = myData["UniversityProfilePic"].ToString();
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoachesFavourites", "", ex.Message, "Exception", 0);
                // ignored
            }


            return athletes;
        }





        internal static List<AthleteDTO> GetAllAthletePaging(string emailaddress, int offset, int max, int gpa, int social, string description, string strokes)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllAtheletesByCoachEmail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;


                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;


                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;

                    SqlParameter gpaPar = cmd.Parameters.Add("@gpa", SqlDbType.Float);
                    gpaPar.Direction = ParameterDirection.Input;
                    gpaPar.Value = gpa;

                    SqlParameter socialPar = cmd.Parameters.Add("@social", SqlDbType.Int);
                    socialPar.Direction = ParameterDirection.Input;
                    socialPar.Value = social;

                    SqlParameter descriptionPar = cmd.Parameters.Add("@description", SqlDbType.VarChar, 1000);
                    descriptionPar.Direction = ParameterDirection.Input;
                    descriptionPar.Value = description;

                    SqlParameter preferredStrokes = cmd.Parameters.Add("@PreferredStrokes", SqlDbType.VarChar, 500);
                    preferredStrokes.Direction = ParameterDirection.Input;
                    preferredStrokes.Value = strokes;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicLevel"]);
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["YearLevel"]);
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteRate"]);
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Premium"]);
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAllAthletePaging", "", ex.Message, "Exception", 0);
                throw ex;

            }


            return athletes;
        }




        internal static List<CoachDTO> GetCoaches(string emailaddress, int offset, int max)
        {

            List<CoachDTO> coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstitutes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxParameter = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxParameter.Direction = ParameterDirection.Input;
                    maxParameter.Value = max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.AreasInterested = myData["AreasInterested"].ToString();
                        v.Address = Convert.ToString(myData["Address"]);
                        v.Emailaddress = myData["Emailaddress"].ToString();//CoachEmail
                        v.Description = myData["Description"].ToString();
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = myData["ClassificationName"].ToString();
                        v.State = myData["State"].ToString();
                        v.City = myData["City"].ToString();
                        v.Zip = myData["Zip"].ToString();
                        v.WebsiteURL = myData["WebsiteURL"].ToString();
                        v.FaidURL = myData["FaidURL"].ToString();
                        v.ApplURL = myData["ApplURL"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.EnrollmentNo = myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = myData["ApplicationNo"].ToString();
                        v.AdmissionNo = myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.FacebookId = "0";//(string)myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        // v.Payment_Paid = (myData["payment_paid"] == DBNull.Value) ? false : (bool)myData["payment_paid"];
                        coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoaches", "", ex.Message, "Exception", 0);

            }

            return coaches;
        }



        internal static string GetCoachDefaultEmail(int instituteId)
        {

            string email = string.Empty;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstituteDefaultEmail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter instituteIdPar = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteIdPar.Direction = ParameterDirection.Input;
                    instituteIdPar.Value = instituteId;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        email = myData["emailaddress"].ToString();

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoachDefaultEmail", "", ex.Message, "Exception", 0);
                throw;
            }


            return email;
        }


        internal static CoachDTO GetCoachExists(string emailaddress, string password)
        {

            CoachDTO v = new CoachDTO();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachExists]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter passwordPar = cmd.Parameters.Add("@password", SqlDbType.VarChar, 100);
                    passwordPar.Direction = ParameterDirection.Input;
                    passwordPar.Value = password;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.AreasInterested = myData["AreasInterested"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Emailaddress = myData["CoachEmail"].ToString();
                        v.Description = myData["Description"].ToString();
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = myData["ClassificationName"].ToString();
                        v.State = myData["State"].ToString();
                        v.City = myData["City"].ToString();
                        v.Zip = myData["Zip"].ToString();
                        v.WebsiteURL = myData["WebsiteURL"].ToString();
                        v.FaidURL = myData["FaidURL"].ToString();
                        v.ApplURL = myData["ApplURL"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.EnrollmentNo = myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = myData["ApplicationNo"].ToString();
                        v.AdmissionNo = myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.FacebookId = myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.CoachId = (myData["CoachId"] == DBNull.Value) ? 0 : (int)myData["CoachId"];
                        v.CoachActiveStatus = (myData["Active"] != DBNull.Value) && (bool)myData["Active"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        int head = (int)myData["HeadCoach"];
                        v.HeadCoach = (head == 1);
                        v.Description = (myData["description"] == DBNull.Value) ? string.Empty : myData["description"].ToString();
                        v.PreferredStrokes = (myData["PreferredStrokes"] == DBNull.Value) ? string.Empty : myData["PreferredStrokes"].ToString();
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.Payment_Paid = (myData["Payment_Paid"] != DBNull.Value) && (bool)myData["Payment_Paid"];
                        v.InstituteEmailaddress = myData["Emailaddress"].ToString();
                        v.DeviceId = (myData["DeviceId"] == DBNull.Value) ? "" : (string)myData["DeviceId"];
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? "" : (string)myData["DeviceType"];
                        v.FirstName = (myData["FirstName"] == DBNull.Value) ? "" : (string)myData["FirstName"];
                        v.LastName = (myData["LastName"] == DBNull.Value) ? "" : (string)myData["LastName"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoachExists", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static bool CheckCoachPaymentStatus(string emailaddress)
        {

            bool isPaid = false;

            SqlConnection myConn = ConnectToDb();

            if (null != myConn)
            {
                SqlCommand cmd = new SqlCommand("[CheckCoachPaymentStatus]", myConn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                emailaddressPar.Direction = ParameterDirection.Input;
                emailaddressPar.Value = emailaddress;


                SqlDataReader myData = cmd.ExecuteReader();

                while (myData.Read())
                {

                    isPaid = (bool)myData["payment_paid"];

                }
                myData.Close();
                myConn.Close();
            }

            return isPaid;
        }



        internal static bool CheckCoachActiveStatus(int coachId)
        {

            bool isActive = false;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachActiveStatus]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = coachId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        isActive = (bool)myData["active"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return isActive;
        }



        internal static string GetCoachUserExists(string emailaddress)
        {
            string code = string.Empty;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachUserExists]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        code = (string)myData["Code"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoachUserExists", "", ex.Message, "Exception", 0);
                throw;
            }

            return code;
        }

        internal static List<CoachDTO> GetAtheleteFavourites(string emailaddress, int offset, int max)
        {

            List<CoachDTO> coaches = new List<CoachDTO>();
            DateTime dtAtheleteDate;
            DateTime dtInstituteDate;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteFavourites]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.AreasInterested = myData["AreasInterested"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Emailaddress = myData["CoachEmail"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = myData["ClassificationName"].ToString();
                        v.State = myData["State"].ToString();
                        v.City = myData["City"].ToString();
                        v.Zip = myData["Zip"].ToString();
                        v.WebsiteURL = myData["WebsiteURL"].ToString();
                        v.FaidURL = myData["FaidURL"].ToString();
                        v.ApplURL = myData["ApplURL"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.EnrollmentNo = myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = myData["ApplicationNo"].ToString();
                        v.AdmissionNo = myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.FacebookId = myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        dtAtheleteDate = (myData["AtheleteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["AtheleteDate"];
                        dtInstituteDate = (myData["InstituteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InstituteDate"];
                        v.Liked = ((int)myData["Likes"] != 0) ? true : false;
                        if (v.Liked)
                        { v.FavDate = (dtAtheleteDate > dtInstituteDate) ? dtAtheleteDate : dtInstituteDate; }
                        else
                        { v.FavDate = dtAtheleteDate; }
                        v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                        v.Accept = (myData["Accept"] == DBNull.Value) ? false : (bool)(myData["Accept"]);
                        v.TotalAtheleteAcceptedCount = (myData["TotalAcceptedCount"] == DBNull.Value) ? 0 : (int)(myData["TotalAcceptedCount"]);
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.AtheleteId = (int)myData["AtheleteId"];
                        v.CoachId = (int)myData["CoachId"];
                        v.AtheleteName = myData["AtheleteName"].ToString();
                        v.AtheleteProfilePic = myData["AtheleteProfilePic"].ToString();
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];

                        coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAtheleteFavourites", "", ex.Message, "Exception", 0);
                throw;
            }

            return coaches;
        }

        internal static AtheleteBadgesDTO GetAtheletesBlogsBadges(string emailaddress)
        {

            AtheleteBadgesDTO badges = new AtheleteBadgesDTO();

            try
            {
                badges.AtheletesBadgesDTO = GetAtheleteBadges(emailaddress);
                badges.AtheleteInvitationDTO = GetAtheleteInvitations(emailaddress);
                badges.BlogsDTO = GetBlog(emailaddress);

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAtheletesBlogsBadges", "", ex.Message, "Exception", 0);
                throw;
            }

            return badges;
        }

        internal static CoachesBadgesDTO GetCoachesBadges(string emailaddress)
        {

            CoachesBadgesDTO badges = new CoachesBadgesDTO();

            try
            {
                badges.coachBadgesDTO = GetInstituteBadges(emailaddress);
                badges.BlogsDTO = GetBlog(emailaddress);

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoachesBadges", "", ex.Message, "Exception", 0);
                throw;
            }

            return badges;
        }




        internal static List<CoachDTO> GetAllCoachesPaging(string emailaddress, int offset, int max, int ncaa, int conference, int admission, string description)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllInstitutesByAtheleteEmail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;

                    SqlParameter ncaaPar = cmd.Parameters.Add("@ncaa", SqlDbType.Int);
                    ncaaPar.Direction = ParameterDirection.Input;
                    ncaaPar.Value = ncaa;

                    SqlParameter conferencePar = cmd.Parameters.Add("@conference", SqlDbType.Int);
                    conferencePar.Direction = ParameterDirection.Input;
                    conferencePar.Value = conference;

                    SqlParameter admissionPar = cmd.Parameters.Add("@admission", SqlDbType.Int);
                    admissionPar.Direction = ParameterDirection.Input;
                    admissionPar.Value = admission;

                    SqlParameter descriptionPar = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    descriptionPar.Direction = ParameterDirection.Input;
                    descriptionPar.Value = description;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.AreasInterested = myData["AreasInterested"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Emailaddress = myData["CoachEmail"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = myData["ClassificationName"].ToString();
                        v.State = myData["State"].ToString();
                        v.City = myData["City"].ToString();
                        v.Zip = myData["Zip"].ToString();
                        v.WebsiteURL = myData["WebsiteURL"].ToString();
                        v.FaidURL = myData["FaidURL"].ToString();
                        v.ApplURL = myData["ApplURL"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.EnrollmentNo = myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = myData["ApplicationNo"].ToString();
                        v.AdmissionNo = myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.FacebookId = myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AdType = 2;
                        v.Payment_Paid = (myData["payment_paid"] == DBNull.Value) ? false : (bool)myData["payment_paid"];
                        Coaches.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAllCoachesPaging", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }


        internal static int AddAthleteBesttimes(AthleteEventDTO athleteDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteBesttimes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = athleteDto.Emailaddress.Trim();

                    SqlParameter event_Type_Id = cmd.Parameters.Add("@Event_Type_Id", SqlDbType.Int);
                    event_Type_Id.Direction = ParameterDirection.Input;
                    event_Type_Id.Value = athleteDto.EventTypeId;

                    SqlParameter bestTimeValue = cmd.Parameters.Add("@BestTimeValue", SqlDbType.VarChar, 50);
                    bestTimeValue.Direction = ParameterDirection.Input;
                    bestTimeValue.Value = athleteDto.BesttimeValue.Trim();

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }



                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddAthleteBesttimes", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int AddAthlete(AthleteDTO athleteDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthlete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    name.Direction = ParameterDirection.Input;
                    name.Value = athleteDto.Name;

                    SqlParameter gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    gender.Direction = ParameterDirection.Input;
                    gender.Value = athleteDto.Gender;

                    SqlParameter school = cmd.Parameters.Add("@School", SqlDbType.VarChar, 100);
                    school.Direction = ParameterDirection.Input;
                    school.Value = (athleteDto.School == null) ? string.Empty : athleteDto.School;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = athleteDto.Emailaddress;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = (athleteDto.Description == null) ? string.Empty : athleteDto.Description;

                    SqlParameter gpa = cmd.Parameters.Add("@GPA", SqlDbType.Float);
                    gpa.Direction = ParameterDirection.Input;
                    gpa.Value = athleteDto.GPA;

                    SqlParameter social = cmd.Parameters.Add("@Social", SqlDbType.Int);
                    social.Direction = ParameterDirection.Input;
                    social.Value = athleteDto.Social;

                    SqlParameter sAT = cmd.Parameters.Add("@SAT", SqlDbType.Int);
                    sAT.Direction = ParameterDirection.Input;
                    sAT.Value = athleteDto.SAT;

                    SqlParameter aCT = cmd.Parameters.Add("@ACT", SqlDbType.Int);
                    aCT.Direction = ParameterDirection.Input;
                    aCT.Value = athleteDto.ACT;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = athleteDto.InstituteId;

                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = athleteDto.ProfilePicURL;

                    SqlParameter DOB = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    DOB.Direction = ParameterDirection.Input;
                    DOB.Value = athleteDto.DOB;

                    SqlParameter Address = cmd.Parameters.Add("@Address", SqlDbType.VarChar, 100);
                    Address.Direction = ParameterDirection.Input;
                    Address.Value = athleteDto.Address;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = athleteDto.FacebookId;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = athleteDto.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = athleteDto.Longitude;


                    SqlParameter AcademicLevel = cmd.Parameters.Add("@AcademicLevel", SqlDbType.Int);
                    AcademicLevel.Direction = ParameterDirection.Input;
                    AcademicLevel.Value = athleteDto.AcademicLevel;


                    SqlParameter YearLevel = cmd.Parameters.Add("@YearLevel", SqlDbType.Int);
                    YearLevel.Direction = ParameterDirection.Input;
                    YearLevel.Value = athleteDto.YearLevel;


                    SqlParameter Premium = cmd.Parameters.Add("@Premium", SqlDbType.Int);
                    Premium.Direction = ParameterDirection.Input;
                    Premium.Value = athleteDto.Premium;

                    SqlParameter Height = cmd.Parameters.Add("@Height", SqlDbType.VarChar, 20);
                    Height.Direction = ParameterDirection.Input;
                    Height.Value = (athleteDto.Height == null) ? string.Empty : athleteDto.Height;

                    SqlParameter Weight = cmd.Parameters.Add("@Weight", SqlDbType.VarChar, 20);
                    Weight.Direction = ParameterDirection.Input;
                    Weight.Value = (athleteDto.Weight == null) ? string.Empty : athleteDto.Weight;

                    SqlParameter WingSpan = cmd.Parameters.Add("@WingSpan", SqlDbType.VarChar, 20);
                    WingSpan.Direction = ParameterDirection.Input;
                    WingSpan.Value = (athleteDto.WingSpan == null) ? string.Empty : athleteDto.WingSpan;

                    SqlParameter ShoeSize = cmd.Parameters.Add("@ShoeSize", SqlDbType.VarChar, 20);
                    ShoeSize.Direction = ParameterDirection.Input;
                    ShoeSize.Value = (athleteDto.ShoeSize == null) ? string.Empty : athleteDto.ShoeSize;

                    SqlParameter Major = cmd.Parameters.Add("@Major", SqlDbType.VarChar, 50);
                    Major.Direction = ParameterDirection.Input;
                    Major.Value = (athleteDto.Major == null) ? string.Empty : athleteDto.Major;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddAthlete", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int AddAthlete_v2(AthleteDTO athleteDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthlete_v2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = athleteDTO.Name;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    Gender.Direction = ParameterDirection.Input;
                    Gender.Value = athleteDTO.Gender;

                    SqlParameter School = cmd.Parameters.Add("@School", SqlDbType.VarChar, 100);
                    School.Direction = ParameterDirection.Input;
                    School.Value = (athleteDTO.School == null) ? string.Empty : athleteDTO.School;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = athleteDTO.Emailaddress;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = (athleteDTO.Description == null) ? string.Empty : athleteDTO.Description;

                    SqlParameter GPA = cmd.Parameters.Add("@GPA", SqlDbType.Float);
                    GPA.Direction = ParameterDirection.Input;
                    GPA.Value = athleteDTO.GPA;

                    SqlParameter Social = cmd.Parameters.Add("@Social", SqlDbType.Int);
                    Social.Direction = ParameterDirection.Input;
                    Social.Value = athleteDTO.Social;

                    SqlParameter SAT = cmd.Parameters.Add("@SAT", SqlDbType.Int);
                    SAT.Direction = ParameterDirection.Input;
                    SAT.Value = athleteDTO.SAT;

                    SqlParameter ACT = cmd.Parameters.Add("@ACT", SqlDbType.Int);
                    ACT.Direction = ParameterDirection.Input;
                    ACT.Value = athleteDTO.ACT;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = athleteDTO.InstituteId;

                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = athleteDTO.ProfilePicURL;

                    SqlParameter DOB = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    DOB.Direction = ParameterDirection.Input;
                    DOB.Value = athleteDTO.DOB;

                    SqlParameter Address = cmd.Parameters.Add("@Address", SqlDbType.VarChar, 100);
                    Address.Direction = ParameterDirection.Input;
                    Address.Value = athleteDTO.Address;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = athleteDTO.FacebookId;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = athleteDTO.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = athleteDTO.Longitude;


                    SqlParameter AcademicLevel = cmd.Parameters.Add("@AcademicLevel", SqlDbType.Int);
                    AcademicLevel.Direction = ParameterDirection.Input;
                    AcademicLevel.Value = athleteDTO.AcademicLevel;


                    SqlParameter YearLevel = cmd.Parameters.Add("@YearLevel", SqlDbType.Int);
                    YearLevel.Direction = ParameterDirection.Input;
                    YearLevel.Value = athleteDTO.YearLevel;


                    SqlParameter Premium = cmd.Parameters.Add("@Premium", SqlDbType.Int);
                    Premium.Direction = ParameterDirection.Input;
                    Premium.Value = athleteDTO.Premium;

                    SqlParameter Height1 = cmd.Parameters.Add("@Height1", SqlDbType.VarChar, 20);
                    Height1.Direction = ParameterDirection.Input;
                    Height1.Value = (athleteDTO.Height == null) ? string.Empty : athleteDTO.Height;

                    SqlParameter Height2 = cmd.Parameters.Add("@Height2", SqlDbType.VarChar, 20);
                    Height2.Direction = ParameterDirection.Input;
                    Height2.Value = (athleteDTO.Height2 == null) ? string.Empty : athleteDTO.Height2;

                    SqlParameter HeightType = cmd.Parameters.Add("@HeightType", SqlDbType.VarChar, 20);
                    HeightType.Direction = ParameterDirection.Input;
                    HeightType.Value = (athleteDTO.HeightType == null) ? string.Empty : athleteDTO.HeightType;

                    SqlParameter Weight = cmd.Parameters.Add("@Weight", SqlDbType.VarChar, 20);
                    Weight.Direction = ParameterDirection.Input;
                    Weight.Value = (athleteDTO.Weight == null) ? string.Empty : athleteDTO.Weight;

                    SqlParameter WeightType = cmd.Parameters.Add("@WeightType", SqlDbType.VarChar, 20);
                    WeightType.Direction = ParameterDirection.Input;
                    WeightType.Value = (athleteDTO.WeightType == null) ? string.Empty : athleteDTO.WeightType;

                    SqlParameter WingSpan = cmd.Parameters.Add("@WingSpan", SqlDbType.VarChar, 20);
                    WingSpan.Direction = ParameterDirection.Input;
                    WingSpan.Value = (athleteDTO.WingSpan == null) ? string.Empty : athleteDTO.WingSpan;

                    SqlParameter WingSpanType = cmd.Parameters.Add("@WingSpanType", SqlDbType.VarChar, 20);
                    WingSpanType.Direction = ParameterDirection.Input;
                    WingSpanType.Value = (athleteDTO.WingSpanType == null) ? string.Empty : athleteDTO.WingSpanType;

                    SqlParameter ShoeSize = cmd.Parameters.Add("@ShoeSize", SqlDbType.VarChar, 20);
                    ShoeSize.Direction = ParameterDirection.Input;
                    ShoeSize.Value = (athleteDTO.ShoeSize == null) ? string.Empty : athleteDTO.ShoeSize;

                    SqlParameter ShoeSizeType = cmd.Parameters.Add("@ShoeSizeType", SqlDbType.VarChar, 20);
                    ShoeSizeType.Direction = ParameterDirection.Input;
                    ShoeSizeType.Value = (athleteDTO.ShoeSizeType == null) ? string.Empty : athleteDTO.ShoeSizeType;

                    SqlParameter ClubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    ClubName.Direction = ParameterDirection.Input;
                    ClubName.Value = string.IsNullOrWhiteSpace(athleteDTO.ClubName) ? string.Empty : athleteDTO.ClubName;

                    SqlParameter CoachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    CoachName.Direction = ParameterDirection.Input;
                    CoachName.Value = string.IsNullOrWhiteSpace(athleteDTO.CoachName) ? string.Empty : athleteDTO.CoachName;

                    SqlParameter CoachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    CoachEmailId.Direction = ParameterDirection.Input;
                    CoachEmailId.Value = string.IsNullOrWhiteSpace(athleteDTO.CoachEmailId) ? string.Empty : athleteDTO.CoachEmailId;

                    SqlParameter AtheleteType = cmd.Parameters.Add("@AtheleteType", SqlDbType.Int);
                    AtheleteType.Direction = ParameterDirection.Input;
                    AtheleteType.Value = athleteDTO.AtheleteType;

                    SqlParameter Active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    Active.Direction = ParameterDirection.Input;
                    Active.Value = athleteDTO.Active;

                    SqlParameter Major = cmd.Parameters.Add("@Major", SqlDbType.VarChar, 50);
                    Major.Direction = ParameterDirection.Input;
                    Major.Value = string.IsNullOrWhiteSpace(athleteDTO.Major) ? string.Empty : athleteDTO.Major;

                    SqlParameter Phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    Phonenumber.Direction = ParameterDirection.Input;
                    Phonenumber.Value = string.IsNullOrWhiteSpace(athleteDTO.Phonenumber) ? string.Empty : athleteDTO.Phonenumber;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = Convert.ToInt32(cmd.Parameters["@Id"].Value);


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddAthlete_v2", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int EditAthlete(AthleteDTO athleteDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateAthlete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = athleteDTO.Name;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    Gender.Direction = ParameterDirection.Input;
                    Gender.Value = athleteDTO.Gender;

                    SqlParameter School = cmd.Parameters.Add("@School", SqlDbType.VarChar, 100);
                    School.Direction = ParameterDirection.Input;
                    School.Value = athleteDTO.School;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = athleteDTO.Emailaddress;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = athleteDTO.Description;

                    SqlParameter GPA = cmd.Parameters.Add("@GPA", SqlDbType.Float);
                    GPA.Direction = ParameterDirection.Input;
                    GPA.Value = athleteDTO.GPA;

                    SqlParameter Social = cmd.Parameters.Add("@Social", SqlDbType.Int);
                    Social.Direction = ParameterDirection.Input;
                    Social.Value = athleteDTO.Social;

                    SqlParameter SAT = cmd.Parameters.Add("@SAT", SqlDbType.Int);
                    SAT.Direction = ParameterDirection.Input;
                    SAT.Value = athleteDTO.SAT;

                    SqlParameter ACT = cmd.Parameters.Add("@ACT", SqlDbType.Int);
                    ACT.Direction = ParameterDirection.Input;
                    ACT.Value = athleteDTO.ACT;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = athleteDTO.InstituteId;

                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = athleteDTO.ProfilePicURL;

                    SqlParameter DOB = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    DOB.Direction = ParameterDirection.Input;
                    DOB.Value = athleteDTO.DOB;

                    SqlParameter Address = cmd.Parameters.Add("@Address", SqlDbType.VarChar, 100);
                    Address.Direction = ParameterDirection.Input;
                    Address.Value = athleteDTO.Address;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = athleteDTO.FacebookId;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = athleteDTO.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = athleteDTO.Longitude;


                    SqlParameter AcademicLevel = cmd.Parameters.Add("@AcademicLevel", SqlDbType.Int);
                    AcademicLevel.Direction = ParameterDirection.Input;
                    AcademicLevel.Value = athleteDTO.AcademicLevel;


                    SqlParameter YearLevel = cmd.Parameters.Add("@YearLevel", SqlDbType.Int);
                    YearLevel.Direction = ParameterDirection.Input;
                    YearLevel.Value = athleteDTO.YearLevel;

                    SqlParameter Premium = cmd.Parameters.Add("@Premium", SqlDbType.Int);
                    Premium.Direction = ParameterDirection.Input;
                    Premium.Value = athleteDTO.Premium;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.InputOutput;
                    Id.Value = athleteDTO.Id;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/EditAthlete", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int AddChatMessages(ChatDTO chatDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddChatMessages]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SenderId = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderId.Direction = ParameterDirection.Input;
                    SenderId.Value = chatDTO.SenderId;

                    SqlParameter ReceiverId = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverId.Direction = ParameterDirection.Input;
                    ReceiverId.Value = chatDTO.ReceiverId;

                    SqlParameter SenderType = cmd.Parameters.Add("@SenderType", SqlDbType.Int);
                    SenderType.Direction = ParameterDirection.Input;
                    SenderType.Value = chatDTO.SenderType;

                    SqlParameter ReceiverType = cmd.Parameters.Add("@ReceiverType", SqlDbType.Int);
                    ReceiverType.Direction = ParameterDirection.Input;
                    ReceiverType.Value = chatDTO.ReceiverType;


                    SqlParameter Message = cmd.Parameters.Add("@Message", SqlDbType.NVarChar, 4000);
                    Message.Direction = ParameterDirection.Input;
                    Message.Value = chatDTO.Message;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddChatMessages", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        //University insertion
        internal static int AddCoach(CoachDTO coachDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddInstitution]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = coachDTO.Name;

                    SqlParameter AreasInterested = cmd.Parameters.Add("@AreasInterested", SqlDbType.VarChar, 2000);
                    AreasInterested.Direction = ParameterDirection.Input;
                    AreasInterested.Value = coachDTO.AreasInterested;

                    SqlParameter Address = cmd.Parameters.Add("@Address", SqlDbType.VarChar, 1000);
                    Address.Direction = ParameterDirection.Input;
                    Address.Value = coachDTO.Address;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = coachDTO.Description;

                    SqlParameter YearOfEstablish = cmd.Parameters.Add("@YearOfEstablish", SqlDbType.Int);
                    YearOfEstablish.Direction = ParameterDirection.Input;
                    YearOfEstablish.Value = coachDTO.YearOfEstablish;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = coachDTO.Emailaddress;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = coachDTO.FacebookId;

                    SqlParameter NCAA = cmd.Parameters.Add("@NCAA", SqlDbType.Int);
                    NCAA.Direction = ParameterDirection.Input;
                    NCAA.Value = coachDTO.NCAA;

                    SqlParameter Conference = cmd.Parameters.Add("@Conference", SqlDbType.Int);
                    Conference.Direction = ParameterDirection.Input;
                    Conference.Value = coachDTO.Conference;

                    SqlParameter AdmissionStandard = cmd.Parameters.Add("@AdmissionStandard", SqlDbType.Int);
                    AdmissionStandard.Direction = ParameterDirection.Input;
                    AdmissionStandard.Value = coachDTO.AdmissionStandard;

                    SqlParameter ClassificationName = cmd.Parameters.Add("@ClassificationName", SqlDbType.VarChar, 500);
                    ClassificationName.Direction = ParameterDirection.Input;
                    ClassificationName.Value = coachDTO.ClassificationName;

                    SqlParameter State = cmd.Parameters.Add("@State", SqlDbType.VarChar, 50);
                    State.Direction = ParameterDirection.Input;
                    State.Value = coachDTO.State;

                    SqlParameter City = cmd.Parameters.Add("@City", SqlDbType.VarChar, 50);
                    City.Direction = ParameterDirection.Input;
                    City.Value = coachDTO.City;

                    SqlParameter Zip = cmd.Parameters.Add("@Zip", SqlDbType.VarChar, 20);
                    Zip.Direction = ParameterDirection.Input;
                    Zip.Value = coachDTO.Zip;

                    SqlParameter WebsiteURL = cmd.Parameters.Add("@WebsiteURL", SqlDbType.VarChar, 200);
                    WebsiteURL.Direction = ParameterDirection.Input;
                    WebsiteURL.Value = coachDTO.WebsiteURL;

                    SqlParameter FaidURL = cmd.Parameters.Add("@FaidURL", SqlDbType.VarChar, 200);
                    FaidURL.Direction = ParameterDirection.Input;
                    FaidURL.Value = coachDTO.FaidURL;

                    SqlParameter ApplURL = cmd.Parameters.Add("@ApplURL", SqlDbType.VarChar, 200);
                    ApplURL.Direction = ParameterDirection.Input;
                    ApplURL.Value = coachDTO.ApplURL;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = coachDTO.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = coachDTO.Longitude;

                    SqlParameter EnrollmentNo = cmd.Parameters.Add("@EnrollmentNo", SqlDbType.Int);
                    EnrollmentNo.Direction = ParameterDirection.Input;
                    EnrollmentNo.Value = coachDTO.EnrollmentNo;

                    SqlParameter ApplicationNo = cmd.Parameters.Add("@ApplicationNo", SqlDbType.Int);
                    ApplicationNo.Direction = ParameterDirection.Input;
                    ApplicationNo.Value = coachDTO.ApplicationNo;

                    SqlParameter AdmissionNo = cmd.Parameters.Add("@AdmissionNo", SqlDbType.Int);
                    AdmissionNo.Direction = ParameterDirection.Input;
                    AdmissionNo.Value = coachDTO.AdmissionNo;


                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = coachDTO.ProfilePicURL;


                    SqlParameter CoverPicURL = cmd.Parameters.Add("@CoverPicURL", SqlDbType.VarChar, 1000);
                    CoverPicURL.Direction = ParameterDirection.Input;
                    CoverPicURL.Value = coachDTO.CoverPicURL;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddCoach", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }



        internal static int AddAtheleteMatch(AtheleteMatchDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAtheleteInstMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FacebookId = cmd.Parameters.Add("@AtheleteFBId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = atheleteMatchDTO.AtheleteFBId;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteFBId", SqlDbType.VarChar, 500);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = atheleteMatchDTO.CoachFBId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = atheleteMatchDTO.Status;

                    //SqlParameter collAtheleteCoachType = cmd.Parameters.Add("@CollAtheleteCoachType", SqlDbType.Int);
                    //collAtheleteCoachType.Direction = ParameterDirection.Input;
                    //collAtheleteCoachType.Value = atheleteMatchDTO.CollAtheleteCoachType;


                    //SqlParameter CollegeAtheleteId = cmd.Parameters.Add("@CollegeAtheleteId", SqlDbType.Int);
                    //CollegeAtheleteId.Direction = ParameterDirection.Input;
                    //CollegeAtheleteId.Value = atheleteMatchDTO.CollegeAtheleteId;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;
                    // id = (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddAtheleteMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int DeleteAtheleteMatch(AtheleteMatchDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAtheleteInstMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FacebookId = cmd.Parameters.Add("@AtheleteFBId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = atheleteMatchDTO.AtheleteFBId;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteFBId", SqlDbType.VarChar, 500);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = atheleteMatchDTO.CoachFBId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;
                    // id = (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteAtheleteMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int AddCoachMatch(AtheleteMatchDTO atheleteMatchDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddInstAtheleteMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FacebookId = cmd.Parameters.Add("@AtheleteFBId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = atheleteMatchDto.AtheleteFBId;

                    SqlParameter CoachEmail = cmd.Parameters.Add("@CoachEmail", SqlDbType.VarChar, 500);
                    CoachEmail.Direction = ParameterDirection.Input;
                    CoachEmail.Value = atheleteMatchDto.CoachFBId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = atheleteMatchDto.Status;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddCoachMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static CoachUserDTO AddCoachUser(CoachUserDTO coachUserDTO)
        {


            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FacebookId = cmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 100);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = coachUserDTO.Emailaddress;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = coachUserDTO.InstituteId;

                    SqlParameter Password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 100);
                    Password.Direction = ParameterDirection.Input;
                    Password.Value = coachUserDTO.Password;

                    SqlParameter active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    active.Direction = ParameterDirection.Input;
                    active.Value = coachUserDTO.Active;

                    SqlParameter teamType = cmd.Parameters.Add("@TeamType", SqlDbType.Int);
                    teamType.Direction = ParameterDirection.Input;
                    teamType.Value = coachUserDTO.TeamType;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    //New in test
                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        coachUserDTO.CoachId = (int)myData["Id"];
                        coachUserDTO.ActivationCode = (myData["activation_code"] == DBNull.Value) ? "" : (string)myData["activation_code"];
                        coachUserDTO.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        coachUserDTO.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        coachUserDTO.Name = (myData["UniversityName"] == DBNull.Value) ? "" : (string)myData["UniversityName"];
                        int Head = (int)myData["HeadCoach"];
                        coachUserDTO.HeadCoach = (Head == 1) ? true : false;


                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddCoachUser", "", ex.Message, "Exception", 0);
                throw;
            }

            return coachUserDTO;
        }


        internal static int DeleteCoachMatch(AtheleteMatchDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteInstAtheleteMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter FacebookId = cmd.Parameters.Add("@AtheleteFBId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = atheleteMatchDTO.AtheleteFBId;

                    SqlParameter CoachId = cmd.Parameters.Add("@CoachEmail", SqlDbType.VarChar, 500);
                    CoachId.Direction = ParameterDirection.Input;
                    CoachId.Value = atheleteMatchDTO.CoachFBId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteCoachMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int UserValidation(string emailaddress, int id)
        {
            int count = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstituteFacebookDetails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        count = (int)myData["count"];


                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UserValidation", "", ex.Message, "Exception", 0);
                throw;
            }

            return count;
        }


        internal static List<EventTypesDTO> GetEventTypes()
        {

            List<EventTypesDTO> EventTypesDTO = new List<EventTypesDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetEventTypes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        EventTypesDTO v = new EventTypesDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        EventTypesDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetEventTypes", "", ex.Message, "Exception", 0);
                throw;
            }

            return EventTypesDTO;
        }


        internal static List<HelpDTO> GetFAQs(int typeid, int OrgId)
        {

            List<HelpDTO> HelpDTO = new List<HelpDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFAQsHelp]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter type = cmd.Parameters.Add("@type", SqlDbType.Int);
                    type.Direction = ParameterDirection.Input;
                    type.Value = typeid;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = OrgId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        HelpDTO v = new HelpDTO();
                        v.Question = (string)myData["Question"].ToString();
                        v.Answer = (string)myData["Answer"].ToString();
                        v.Date = (DateTime)myData["CreatedDate"];
                        v.ThumbNailURL = (string)myData["ThumbnailURL"].ToString();
                        v.VideoType = (int)myData["VideoType"];
                        HelpDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetFAQs", "", ex.Message, "Exception", 0);
                throw;
            }

            return HelpDTO;
        }

        internal static int DeleteAthleteBesttimes(string emailaddress)
        {
            int numofRows = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAthleteBesttimes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    numofRows = cmd.ExecuteNonQuery();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteAthleteBesttimes", "", ex.Message, "Exception", 0);
                throw;
            }

            return numofRows;
        }



        internal static CoachDTO UpdateCoachEmailToUniversity(string emailaddress, int coachid, int teamtype)
        {
            CoachDTO v = new CoachDTO();
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachEmailToUniversity]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = coachid;

                    SqlParameter TeamType = cmd.Parameters.Add("@TeamType", SqlDbType.Int);
                    TeamType.Direction = ParameterDirection.Input;
                    TeamType.Value = teamtype;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.AreasInterested = myData["AreasInterested"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Emailaddress = myData["CoachEmail"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.FacebookId = (string)myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.CoachId = (myData["CoachId"] == DBNull.Value) ? 0 : (int)myData["CoachId"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateCoachEmailToUniversity", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static bool UpdateCoachPaymentStatus(string emailaddress, string paymentToken, string amount)
        {
            bool isPaid = false;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachPaymentStatus]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter PaymentToken = cmd.Parameters.Add("@paymentToken", SqlDbType.VarChar, 500);
                    PaymentToken.Direction = ParameterDirection.Input;
                    PaymentToken.Value = paymentToken;

                    SqlParameter Amount = cmd.Parameters.Add("@amount", SqlDbType.VarChar, 500);
                    Amount.Direction = ParameterDirection.Input;
                    Amount.Value = amount;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        isPaid = (bool)myData["id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateCoachPaymentStatus", "", ex.Message, "Exception", 0);
                throw;
            }

            return isPaid;
        }



        internal static int UpdateCoachRatings(CoachRatingsDTO coachRatingsDTO)
        {

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachRatings]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter NCAA = cmd.Parameters.Add("@NCAA", SqlDbType.Int);
                    NCAA.Direction = ParameterDirection.Input;
                    NCAA.Value = coachRatingsDTO.NCAA;

                    SqlParameter Social = cmd.Parameters.Add("@Social", SqlDbType.Int);
                    Social.Direction = ParameterDirection.Input;
                    Social.Value = coachRatingsDTO.NoOfAthletesInApp;

                    SqlParameter Conference = cmd.Parameters.Add("@Conference", SqlDbType.Int);
                    Conference.Direction = ParameterDirection.Input;
                    Conference.Value = coachRatingsDTO.Conference;

                    SqlParameter AdmissionStandard = cmd.Parameters.Add("@AdmissionStandard", SqlDbType.Int);
                    AdmissionStandard.Direction = ParameterDirection.Input;
                    AdmissionStandard.Value = coachRatingsDTO.AdmissionStandard;

                    SqlParameter AcceptanceRate = cmd.Parameters.Add("@AcceptanceRate", SqlDbType.Int);
                    AcceptanceRate.Direction = ParameterDirection.Input;
                    AcceptanceRate.Value = coachRatingsDTO.AcceptanceRate;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = coachRatingsDTO.Id;

                    cmd.ExecuteNonQuery();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateCoachRatings", "", ex.Message, "Exception", 0);
                throw;
            }


            return coachRatingsDTO.Id;
        }



        internal static int UpdateCoachActiveStatus(string Code, int CoachId)
        {

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateActiveStatusForCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter code = cmd.Parameters.Add("@Code", SqlDbType.VarChar, 1000);
                    code.Direction = ParameterDirection.Input;
                    code.Value = Code.Trim();

                    SqlParameter coachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachId.Direction = ParameterDirection.Input;
                    coachId.Value = CoachId;


                    cmd.ExecuteNonQuery();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateCoachActiveStatus", "", ex.Message, "Exception", 0);
                throw;
            }
            return CoachId;
        }


        internal static int UpdateFbFriendsandShares(FriendsSharesDTO friendsSharesDTO)
        {
            int num = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateFbFriendsandShares]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter friends = cmd.Parameters.Add("@friends", SqlDbType.Int);
                    friends.Direction = ParameterDirection.Input;
                    friends.Value = friendsSharesDTO.Friends;

                    SqlParameter shares = cmd.Parameters.Add("@shares", SqlDbType.Int);
                    shares.Direction = ParameterDirection.Input;
                    shares.Value = friendsSharesDTO.Shares;

                    SqlParameter flag = cmd.Parameters.Add("@flag", SqlDbType.Int);
                    flag.Direction = ParameterDirection.Input;
                    flag.Value = friendsSharesDTO.Flag;

                    SqlParameter AtheleteEmail = cmd.Parameters.Add("@AtheleteEmail", SqlDbType.VarChar, 100);
                    AtheleteEmail.Direction = ParameterDirection.Input;
                    AtheleteEmail.Value = friendsSharesDTO.AtheleteEmail;

                    num = cmd.ExecuteNonQuery();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateFbFriendsandShares", "", ex.Message, "Exception", 0);
                throw;
            }

            return num;
        }

        internal static BadgesDTO GetAtheleteBadges(string Emailaddress)
        {

            BadgesDTO atheleteBadgesDTO = new BadgesDTO();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteLemonaidStandBadges]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = Emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        atheleteBadgesDTO.Academic = (bool)myData["AcademicStand"];
                        atheleteBadgesDTO.Social = (bool)myData["SocialStand"];
                        atheleteBadgesDTO.Athelete = (bool)myData["AtheleteStand"];
                        atheleteBadgesDTO.Diamond = (bool)myData["DiamondStand"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAtheleteBadges", "", ex.Message, "Exception", 0);
                throw;
            }

            return atheleteBadgesDTO;
        }

        internal static BadgesDTO GetInstituteBadges(string Emailaddress)
        {

            BadgesDTO BadgesDTO = new BadgesDTO();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstituteBadges]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = Emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        BadgesDTO.Academic = (bool)myData["AcademicStand"];
                        BadgesDTO.Social = (bool)myData["SocialStand"];
                        BadgesDTO.Athelete = (bool)myData["AtheleteStand"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetInstituteBadges", "", ex.Message, "Exception", 0);
                throw;
            }

            return BadgesDTO;
        }

        internal static List<BlogDTO> GetBlog(string Emailaddress)
        {

            List<BlogDTO> blogs = new List<BlogDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstituteBlogs]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = Emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BlogDTO blogDTO = new BlogDTO();
                        blogDTO.Title = (string)myData["Title"];
                        blogDTO.Description = (string)myData["Description"];
                        blogDTO.ImageURL = (string)myData["ImageURL"];
                        blogDTO.URL = (string)myData["URL"];
                        blogDTO.Date = (DateTime)myData["CreatedDate"];
                        blogDTO.ShortDesc = myData["ShortDesc"].ToString();
                        blogDTO.Type = (myData["Type"] == DBNull.Value) ? string.Empty : (string)myData["Type"];
                        blogs.Add(blogDTO);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetBlog", "", ex.Message, "Exception", 0);
                throw;
            }

            return blogs;
        }



        internal static int DeleteAtheleteCoaches(string Emailaddress)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAtheleteCoaches]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = Emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteAtheleteCoaches", "", ex.Message, "Exception", 0);
                throw;
            }

            return i;
        }


        internal static int DeleteCoachAtheletes(string Emailaddress)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCoachAtheletes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = Emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteCoachAtheletes", "", ex.Message, "Exception", 0);
                throw;
            }
            return i;
        }

        internal static int GetClass(int AcademicLevel, int YearLevel)
        {
            //Fr-1, So-2 , Jr-3, Sr-4

            int Class = 0;

            try
            {
                int month = DateTime.Now.Month;

                int Currentyear = DateTime.Now.Year;

                switch (YearLevel)
                {
                    case 1:
                        if (month > 5)
                        {
                            Class = Currentyear + 4;
                        }
                        else
                        {
                            Class = Currentyear + 3;
                        }
                        break;
                    case 2:
                        if (month > 5)
                        {
                            Class = Currentyear + 3;
                        }
                        else
                        {
                            Class = Currentyear + 2;
                        }
                        break;

                    case 3:
                        if (month > 5)
                        {
                            Class = Currentyear + 2;
                        }
                        else
                        {
                            Class = Currentyear + 1;
                        }

                        break;
                    case 4:
                        if (month > 5)
                        {
                            Class = Currentyear + 1;
                        }
                        else
                        {
                            Class = Currentyear;
                        }
                        break;
                    default: Class = Currentyear + 1;
                        break;
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetClass", "", ex.Message, "Exception", 0);
                throw;
            }

            return Class;
        }

        internal static int AddAthleteInvitation(AtheleteInvitationDTO atheleteInvitationDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteInvitation]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AtheleteEmailAddress = cmd.Parameters.Add("@AtheleteEmailAddress", SqlDbType.VarChar, 100);
                    AtheleteEmailAddress.Direction = ParameterDirection.Input;
                    AtheleteEmailAddress.Value = atheleteInvitationDTO.AtheleteEmailAddress;

                    SqlParameter InstituteEmailAddress = cmd.Parameters.Add("@InstituteEmailAddress", SqlDbType.VarChar, 100);
                    InstituteEmailAddress.Direction = ParameterDirection.Input;
                    InstituteEmailAddress.Value = atheleteInvitationDTO.CoachEmailAddress;

                    SqlParameter InvitationDate = cmd.Parameters.Add("@InvitationDate", SqlDbType.DateTime);
                    InvitationDate.Direction = ParameterDirection.Input;
                    InvitationDate.Value = atheleteInvitationDTO.InvitationDate;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }



                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddAthleteInvitation", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int UpdateAthleteAccept(AtheleteInvitationDTO atheleteInvitationDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateAthleteAccept]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AtheleteEmailAddress = cmd.Parameters.Add("@AtheleteEmailAddress", SqlDbType.VarChar, 100);
                    AtheleteEmailAddress.Direction = ParameterDirection.Input;
                    AtheleteEmailAddress.Value = atheleteInvitationDTO.AtheleteEmailAddress;

                    SqlParameter InstituteEmailAddress = cmd.Parameters.Add("@InstituteEmailAddress", SqlDbType.VarChar, 100);
                    InstituteEmailAddress.Direction = ParameterDirection.Input;
                    InstituteEmailAddress.Value = atheleteInvitationDTO.CoachEmailAddress;

                    SqlParameter Accept = cmd.Parameters.Add("@Accept", SqlDbType.Bit);
                    Accept.Direction = ParameterDirection.Input;
                    Accept.Value = atheleteInvitationDTO.Accept;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateAthleteAccept", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<AtheleteStrokesDTO> GetStrokesForAtheletes()
        {

            List<AtheleteStrokesDTO> AtheleteStrokesDTO = new List<AtheleteStrokesDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetStrokesForAtheletes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AtheleteStrokesDTO athStrokesDTO = new AtheleteStrokesDTO();
                        athStrokesDTO.AtheleteId = (int)myData["AtheleteId"];
                        athStrokesDTO.Category = (string)myData["category"];
                        AtheleteStrokesDTO.Add(athStrokesDTO);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetStrokesForAtheletes", "", ex.Message, "Exception", 0);
                throw;
            }
            return AtheleteStrokesDTO;
        }


        internal static List<GetAtheleteInvitationDTO> GetAtheleteInvitations(string AtheleteEmailAddress)
        {

            List<GetAtheleteInvitationDTO> getAtheleteInvitationDTO = new List<GetAtheleteInvitationDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteInvitations]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteEmailAddress = cmd.Parameters.Add("@AtheleteEmailAddress", SqlDbType.VarChar, 100);
                    atheleteEmailAddress.Direction = ParameterDirection.Input;
                    atheleteEmailAddress.Value = AtheleteEmailAddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        GetAtheleteInvitationDTO athInvDTO = new GetAtheleteInvitationDTO();
                        athInvDTO.UniversityName = (string)myData["Name"];
                        athInvDTO.InvitationDate = (string)myData["InvitedDate"];
                        getAtheleteInvitationDTO.Add(athInvDTO);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAtheleteInvitations", "", ex.Message, "Exception", 0);
                throw;
            }

            return getAtheleteInvitationDTO;
        }


        internal static int AddBlog(BlogDTO blogDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddBlog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = blogDTO.CoachId;

                    SqlParameter Title = cmd.Parameters.Add("@Title", SqlDbType.NVarChar, 100);
                    Title.Direction = ParameterDirection.Input;
                    Title.Value = blogDTO.Title;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.NVarChar, 4000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = blogDTO.Description;

                    SqlParameter ImageURL = cmd.Parameters.Add("@ImageURL", SqlDbType.NVarChar, 1000);
                    ImageURL.Direction = ParameterDirection.Input;
                    ImageURL.Value = blogDTO.ImageURL;

                    SqlParameter URL = cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 1000);
                    URL.Direction = ParameterDirection.Input;
                    URL.Value = blogDTO.URL;

                    SqlParameter type = cmd.Parameters.Add("@Type", SqlDbType.VarChar, 50);
                    type.Direction = ParameterDirection.Input;
                    type.Value = blogDTO.Type;

                    SqlParameter shortDesc = cmd.Parameters.Add("@ShortDesc", SqlDbType.NVarChar, 1000);
                    shortDesc.Direction = ParameterDirection.Input;
                    shortDesc.Value = blogDTO.ShortDesc;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddBlog", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int EditBlog(BlogDTO blogDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateBlog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter InstituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteId.Direction = ParameterDirection.Input;
                    InstituteId.Value = blogDTO.CoachId;

                    SqlParameter Title = cmd.Parameters.Add("@Title", SqlDbType.NVarChar, 100);
                    Title.Direction = ParameterDirection.Input;
                    Title.Value = blogDTO.Title;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.NVarChar, 4000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = blogDTO.Description;

                    SqlParameter ImageURL = cmd.Parameters.Add("@ImageURL", SqlDbType.NVarChar, 1000);
                    ImageURL.Direction = ParameterDirection.Input;
                    ImageURL.Value = blogDTO.ImageURL;

                    SqlParameter URL = cmd.Parameters.Add("@URL", SqlDbType.NVarChar, 1000);
                    URL.Direction = ParameterDirection.Input;
                    URL.Value = blogDTO.URL;

                    SqlParameter type = cmd.Parameters.Add("@Type", SqlDbType.VarChar, 50);
                    type.Direction = ParameterDirection.Input;
                    type.Value = blogDTO.Type;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.InputOutput;
                    Id.Value = blogDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/EditBlog", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int DeleteBlog(int BlogId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteBlog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    id.Direction = ParameterDirection.Input;
                    id.Value = BlogId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteBlog", "", ex.Message, "Exception", 0);
                throw;
            }

            return i;
        }

        internal static List<BlogDTO> GetBlog()
        {

            List<BlogDTO> BlogDTO = new List<BlogDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetBlog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        BlogDTO v = new BlogDTO();
                        v.Title = (string)myData["Title"].ToString();
                        v.Description = (string)myData["Description"].ToString();
                        v.Date = (DateTime)myData["CreatedDate"];
                        v.ImageURL = (string)myData["ImageURL"].ToString();
                        v.URL = (string)myData["URL"];
                        v.Type = (string)myData["Type"];
                        v.ShortDesc = myData["ShortDesc"].ToString();
                        v.Id = (int)myData["Id"];
                        BlogDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetBlog", "", ex.Message, "Exception", 0);
                throw;
            }

            return BlogDTO;
        }





        public static void AddTestForWorker()
        {

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddTestForWoker]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddTestForWorker", "", ex.Message, "Exception", 0);
                throw;
            }


        }

        public static void SendMailToCoaches()
        {
            try
            {
                SqlConnection myConn = ConnectToDb();
                EmailContentDTO emailContentDTO = new EmailContentDTO();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("GetUniversityRightSwipes", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        //GetData and send mail
                        emailContentDTO.Emailaddress = (myData["emailaddress"] == DBNull.Value) ? string.Empty : myData["emailaddress"].ToString();
                        emailContentDTO.Rightswipes = myData["RightSwipes"].ToString();
                        emailContentDTO.Id = Convert.ToInt32(myData["Id"].ToString());
                        if (emailContentDTO.Emailaddress != string.Empty)
                        {
                            //Uncomment this later
                            SendMailToUniversities(emailContentDTO.Emailaddress, emailContentDTO.Rightswipes, emailContentDTO.Id);
                        }
                        else
                        {
                            //SendMailToUniversities("bindu.bhargavi@gmail.com", "1");
                        }
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/SendMailToCoaches", "", ex.Message, "Exception", 0);
                throw;
            }
        }


        public static void SendPushNotificationsToCoaches()
        {
            try
            {
                SqlConnection myConn = ConnectToDb();
                EmailContentDTO emailContentDTO = new EmailContentDTO();
                string Message = string.Empty;
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("GetUniversityRightSwipes", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        //GetData and send mail
                        emailContentDTO.Emailaddress = (myData["emailaddress"] == DBNull.Value) ? string.Empty : myData["emailaddress"].ToString();
                        emailContentDTO.Rightswipes = myData["RightSwipes"].ToString();
                        emailContentDTO.Name = (myData["name"] == DBNull.Value) ? string.Empty : myData["name"].ToString();
                        if (emailContentDTO.Emailaddress != string.Empty)
                        {
                            emailContentDTO.Name = FirstCap(RemoveWhiteSpace(emailContentDTO.Name));
                            Message = "Congratulations! You had " + emailContentDTO.Rightswipes + " right swipes on LemonAid from last two days";
                            DAL2V6.PushNotification(Message, emailContentDTO.Name);

                        }
                    }


                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/SendPushNotificationsToCoaches", "", ex.Message, "Exception", 0);
                throw;
            }
        }

        public static string RemoveWhiteSpace(this string self)
        {
            return new string(self.Where(c => !Char.IsWhiteSpace(c)).ToArray());
        }

        public static string FirstCap(string value)
        {
            string result = String.Empty;

            if (!String.IsNullOrEmpty(value))
            {
                if (value.Length == 1)
                {
                    result = value.ToUpper();
                }
                else
                {
                    result = value.Substring(0, 1).ToString().ToUpper() + value.Substring(1).ToLower();
                }
            }

            return result;
        }

        public static int SendMailToUniversities(string Emailaddress, string Rightswipes, int Id)
        {
            string Message = string.Empty;
            SupportMailDTO support = new SupportMailDTO();
            MailMessage Msg = new MailMessage();
            try
            {
                support = UsersDal.GetSupportMails("SupportEmail");

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailSending.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                // myString = myString.Replace("$$Right", Emailaddress);
                myString = myString.Replace("$$Right$$", Rightswipes);


                //MailAddress fromMail = new MailAddress(support.EmailAddress);
                //Msg.From = fromMail;
                //Msg.To.Add(new MailAddress("" + Emailaddress + ""));
                //Msg.Subject = " Sweetness!!! " + Rightswipes + " athletes showed interest in your school on LemonAid";
                //Msg.Body = myString.ToString();
                //Msg.IsBodyHtml = true;
                //var smtpMail = new SmtpClient();
                //smtpMail.Host = support.Host;
                //smtpMail.Port = Convert.ToInt32(support.Port);
                //smtpMail.EnableSsl = Convert.ToBoolean(support.SSL);
                //smtpMail.UseDefaultCredentials = false;
                //smtpMail.Credentials = new System.Net.NetworkCredential(support.EmailAddress, support.Password);
                //smtpMail.Timeout = 1000000;
                //smtpMail.Send(Msg);

                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(Emailaddress);
                sendGridModel.fromMail = support.EmailAddress;
                sendGridModel.fromName = "Lemonaid";
                sendGridModel.subject = " Sweetness!!! " + Rightswipes + " athletes showed interest in your school on LemonAid";
                sendGridModel.Message = myString;
                sendGridModel.email = support.EmailAddress;
                sendGridModel.password = support.Password;
                SendGridSMTPMail.SendEmail(sendGridModel);


                //EmailDTO email = new EmailDTO();
                //email.From = support.EmailAddress;
                //email.To = Emailaddress;
                //email.Status = 1;
                //email.Subject = Msg.Subject;
                // EmailLogModel emaillog = new EmailLogModel();
                // emaillog.AddEmailLog(support.EmailAddress, Emailaddress, Msg.Subject, "Mail", 1);

                return 0;
            }
            catch (SmtpException ex)
            {
                // EmailLogModel emaillog = new EmailLogModel();
                //  emaillog.AddEmailLog(support.EmailAddress, Emailaddress, Msg.Subject, "Mail", 0);
                Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/SendMailToUniversities", "", ex.Message, "Exception", 0);
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/SendMailToUniversities", "", ex.Message, "Exception", 0);
                return 1;
            }
        }


        internal static void AddUsageTracking(UsageDTO usageDTO)
        {

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("UsersUsageTracking", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter featureID = cmd.Parameters.Add("@FeatureID", SqlDbType.VarChar, 500);
                    featureID.Direction = ParameterDirection.Input;
                    featureID.Value = usageDTO.FeatureId;

                    SqlParameter count = cmd.Parameters.Add("@Count", SqlDbType.Int);
                    count.Direction = ParameterDirection.Input;
                    count.Value = usageDTO.Count;

                    SqlParameter value = cmd.Parameters.Add("@Value", SqlDbType.VarChar, 500);
                    value.Direction = ParameterDirection.Input;
                    value.Value = usageDTO.Value;

                    SqlParameter user = cmd.Parameters.Add("@DeviceID", SqlDbType.VarChar, 100);
                    user.Direction = ParameterDirection.Input;
                    user.Value = (usageDTO.DeviceId == null) ? string.Empty : usageDTO.DeviceId;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 20);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = (usageDTO.DeviceType == null) ? string.Empty : usageDTO.DeviceType;  

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddUsageTracking", "", ex.Message, "Exception", 0);
                throw;
            }
        }

        internal static List<AthleteEventDTO> GetAthleteBesttimes(int AtheleteId)
        {
            List<AthleteEventDTO> athleteBesttimes = new List<AthleteEventDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteBesttimes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = AtheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteEventDTO v = new AthleteEventDTO();
                        v.EventTypeId = (int)myData["Event_Type_Id"];
                        v.BesttimeValue = myData["BestTimeValue"].ToString();
                        v.Emailaddress = AtheleteId.ToString();
                        v.EventName = myData["name"].ToString();
                        athleteBesttimes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAthleteBesttimes", "", ex.Message, "Exception", 0);
                throw;
            }

            return athleteBesttimes;
        }
        internal static List<AdDTO> GetAds(int Offset)
        {

            List<AdDTO> AdDTO = new List<AdDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAds]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offset.Direction = ParameterDirection.Input;
                    offset.Value = Offset;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AdDTO v = new AdDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? "" : (string)myData["Description"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? "" : (string)myData["ProfilePicURL"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : (string)myData["VideoURL"];
                        v.AdType = (myData["AdType"] == DBNull.Value) ? 0 : (int)myData["AdType"]; 
                        v.Id = (int)myData["Id"];
                        AdDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAds", "", ex.Message, "Exception", 0);
                throw;
            }


            return AdDTO;
        }


        internal static int AddVideos(VideoDTO videoDTO)
        {
            List<VideoDTO> videos = new List<VideoDTO>();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = videoDTO.Name;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    VideoURL.Direction = ParameterDirection.Input;
                    VideoURL.Value = videoDTO.VideoURL;

                    SqlParameter ThumbnailURL = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                    ThumbnailURL.Direction = ParameterDirection.Input;
                    ThumbnailURL.Value = videoDTO.ThumbnailURL;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = videoDTO.Status;

                    SqlParameter AtheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.VarChar, 100);
                    AtheleteId.Direction = ParameterDirection.Input;
                    AtheleteId.Value = videoDTO.AtheleteEmail;

                    SqlParameter videoFormat = cmd.Parameters.Add("@VideoFormat", SqlDbType.VarChar, 50);
                    videoFormat.Direction = ParameterDirection.Input;
                    videoFormat.Value = videoDTO.VideoFormat;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;

                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddVideos", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int AddVideos_v3(VideoDTO videoDTO)
        {
            List<VideoDTO> videos = new List<VideoDTO>();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddVideos_v3]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = string.Empty;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    VideoURL.Direction = ParameterDirection.Input;
                    VideoURL.Value = videoDTO.VideoURL;

                    SqlParameter ThumbnailURL = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                    ThumbnailURL.Direction = ParameterDirection.Input;
                    ThumbnailURL.Value = videoDTO.ThumbnailURL;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = videoDTO.Status;

                    SqlParameter AtheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.VarChar, 100);
                    AtheleteId.Direction = ParameterDirection.Input;
                    AtheleteId.Value = videoDTO.AtheleteEmail;

                    SqlParameter videoFormat = cmd.Parameters.Add("@VideoFormat", SqlDbType.VarChar, 50);
                    videoFormat.Direction = ParameterDirection.Input;
                    videoFormat.Value = videoDTO.VideoFormat;

                    SqlParameter videoNumber = cmd.Parameters.Add("@videoNumber", SqlDbType.Int);
                    videoNumber.Direction = ParameterDirection.Input;
                    videoNumber.Value = videoDTO.VideoNumber;

                    SqlParameter duration = cmd.Parameters.Add("@duration", SqlDbType.VarChar, 50);
                    duration.Direction = ParameterDirection.Input;
                    duration.Value = videoDTO.Duration;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;

                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddVideos_v3", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<VideoDTO> GetAtheleteVideos(string emailaddress)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : (string)myData["VideoURL"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value) ? "" : (string)myData["ThumbnailURL"].ToString();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? "" : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.Count = (int)myData["AtheleteVideoCount"];
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAtheleteVideos", "", ex.Message, "Exception", 0);
                throw;
            }

            return videos;
        }


        internal static List<ChatDTO> GetChatMessages(ChatDTO chatDTO)
        {

            List<ChatDTO> chats = new List<ChatDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetChatMessages]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter SenderId = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderId.Direction = ParameterDirection.Input;
                    SenderId.Value = chatDTO.SenderId;

                    SqlParameter ReceiverId = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverId.Direction = ParameterDirection.Input;
                    ReceiverId.Value = chatDTO.ReceiverId;

                    SqlParameter SenderType = cmd.Parameters.Add("@SenderType", SqlDbType.Int);
                    SenderType.Direction = ParameterDirection.Input;
                    SenderType.Value = chatDTO.SenderType;

                    SqlParameter ReceiverType = cmd.Parameters.Add("@ReceiverType", SqlDbType.Int);
                    ReceiverType.Direction = ParameterDirection.Input;
                    ReceiverType.Value = chatDTO.ReceiverType;

                    SqlParameter Date = cmd.Parameters.Add("@Date", SqlDbType.DateTime);
                    Date.Direction = ParameterDirection.Input;
                    Date.Value = (chatDTO.Date == null) ? Convert.ToDateTime("1900-01-01") : chatDTO.Date;


                    SqlParameter offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offset.Direction = ParameterDirection.Input;
                    offset.Value = chatDTO.Offset;

                    SqlParameter max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    max.Direction = ParameterDirection.Input;
                    max.Value = chatDTO.Max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChatDTO v = new ChatDTO();
                        v.SenderId = (myData["SenderId"] == DBNull.Value) ? 0 : (int)myData["SenderId"];
                        v.ReceiverId = (myData["ReceiverId"] == DBNull.Value) ? 0 : (int)myData["ReceiverId"];
                        v.SenderType = (myData["SenderType"] == DBNull.Value) ? 0 : (int)myData["SenderType"];
                        v.ReceiverType = (myData["ReceiverType"] == DBNull.Value) ? 0 : (int)myData["ReceiverType"];
                        v.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        v.Message = (myData["Message"] == DBNull.Value) ? "" : (string)myData["Message"];
                        chats.Add(v);
                    }
                    foreach (var c in chats)
                    {
                        c.Count = chats.Count;
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetChatMessages", "", ex.Message, "Exception", 0);
                throw;
            }

            return chats;
        }


        internal static List<VideoDTO> UpdateVideoThumbnail(ThumbnailDTO thumbnailDTO)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                string ThumbnailURL = UploadImageFromBase64(thumbnailDTO.ThumbnailURL, thumbnailDTO.ThumbnailFormat);

                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateVideoThumbnail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Thumbnailurl = cmd.Parameters.Add("@Thumbnailurl", SqlDbType.VarChar, 1024);
                    Thumbnailurl.Direction = ParameterDirection.Input;
                    Thumbnailurl.Value = ThumbnailURL;

                    SqlParameter AthleteEmail = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar, 100);
                    AthleteEmail.Direction = ParameterDirection.Input;
                    AthleteEmail.Value = thumbnailDTO.AthleteEmail;

                    SqlParameter VideoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    VideoId.Direction = ParameterDirection.Input;
                    VideoId.Value = thumbnailDTO.VideoId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : (string)myData["VideoURL"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value) ? "" : (string)myData["ThumbnailURL"].ToString();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? "" : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.Count = (int)myData["AtheleteVideoCount"];
                        videos.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateVideoThumbnail", "", ex.Message, "Exception", 0);
                throw;
            }
            return videos;
        }

        internal static List<AthleteDTO> GetCoachFriends(string emailaddress, int offset, int max)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstituteAtheleteFriends]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);

                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoachFriends", "", ex.Message, "Exception", 0);
                throw;
            }

            return athletes;
        }


        internal static AthleteDTO GetAthleteProfile(string emailaddress)
        {
            AthleteDTO v = new AthleteDTO();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteProfile]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = 1;// (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);

                        //v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        //v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        //v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        //v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        //v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        //v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        //v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        //v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivAddress"]);
                        //v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivEmailAddress"]);
                        //v.UniversityName = (myData["UniversityName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UniversityName"]);
                        //v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnviPhoneNumber"]);
                        //v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivProfilePic"]);
                        //v.UniversitySize = (myData["UnivSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivSize"]);
                        //v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClassificationName"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAthleteProfile", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }



        internal static int DeleteAtheleteVideos(string Emailaddress, int VideoId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAthleteVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = Emailaddress;

                    SqlParameter videoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    videoId.Direction = ParameterDirection.Input;
                    videoId.Value = VideoId;


                    cmd.ExecuteNonQuery();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteAtheleteVideos", "", ex.Message, "Exception", 0);
                throw;
            }

            return i;
        }


        internal static int AddPayment(PaymentDTO payment)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();



                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddPayment]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = payment.UserID;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.Int);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = payment.UserType;

                    SqlParameter PaymentToken = cmd.Parameters.Add("@PaymentToken", SqlDbType.VarChar, 8000);
                    PaymentToken.Direction = ParameterDirection.Input;
                    PaymentToken.Value = payment.PaymentToken;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = payment.DeviceType;

                    SqlParameter PaymentType = cmd.Parameters.Add("@PaymentType", SqlDbType.VarChar, 50);
                    PaymentType.Direction = ParameterDirection.Input;
                    PaymentType.Value = payment.PaymentType;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddPayment", "", ex.Message, "Exception", 0);
                throw;
            }
            return id;
        }



        internal static PaymentDTO GetPayment(int UserID, int UserType)
        {
            PaymentDTO payment = new PaymentDTO();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPayment]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    userID.Direction = ParameterDirection.Input;
                    userID.Value = UserID;

                    SqlParameter userType = cmd.Parameters.Add("@UserType", SqlDbType.Int);
                    userType.Direction = ParameterDirection.Input;
                    userType.Value = UserType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        payment.UserID = (int)myData["UserID"];
                        payment.UserType = (int)myData["UserType"];
                        payment.PaymentToken = (string)myData["PaymentToken"];
                        payment.DeviceType = (string)myData["DeviceType"];
                        payment.PaymentType = (string)myData["PaymentType"];
                        payment.CreatedDate = (DateTime)myData["CreatedDate"];
                        payment.ExpiryDate = Convert.ToDateTime(myData["ExpiryDate"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetPayment", "", ex.Message, "Exception", 0);
                throw;
            }

            return payment;
        }

        internal static bool IsHeadCoach(int CoachId, int UniversityId)
        {

            bool IsHeadCoach = false;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[IsHeadCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachId.Direction = ParameterDirection.Input;
                    coachId.Value = CoachId;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = UniversityId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        int Head = (int)myData["HeadCoach"];
                        IsHeadCoach = (Head == 1) ? true : false;

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/IsHeadCoach", "", ex.Message, "Exception", 0);
                throw;
            }
            return IsHeadCoach;
        }


        internal static bool PushNotification11(string pushMessage, string Name)
        {
            bool isPushMessageSend = false;
            try
            {
                string postString = "";
                string urlpath = "https://api.parse.com/1/push";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlpath);
                postString = "{ \"channels\": [ \"" + Name + "\"  ], " +
                                 "\"data\" : {\"alert\":\"" + pushMessage + "\"}" +
                                 "}";


                httpWebRequest.ContentType = "application/json";
                httpWebRequest.ContentLength = postString.Length;
                httpWebRequest.Headers.Add("X-Parse-Application-Id", "1RKjxttEQfvcDBch16isw0Pl3mu7jKZJvDld7zd8");
                httpWebRequest.Headers.Add("X-Parse-REST-API-KEY", "OPQHP2145ut2dilrMo1Wf8aoQt02Ekrk9lmg0Jsc");
                httpWebRequest.Method = "POST";
                StreamWriter requestWriter = new StreamWriter(httpWebRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    JObject jObjRes = JObject.Parse(responseText);
                    if (Convert.ToString(jObjRes).IndexOf("true") != -1)
                    {
                        isPushMessageSend = true;
                    }
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/PushNotification", "", ex.Message, "Exception", 0);
                throw;
            }
            return isPushMessageSend;
        }

        internal static string UploadImageFromBase64(string binary, string format)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999).ToString() + DateTime.Now.Millisecond + "." + format;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(binary);


                blockBlob.Properties.ContentType = "image/jpeg";
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                imageurl = blockBlob1.Uri.ToString();


            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UploadImageFromBase64", "", ex.Message, "Exception", 0);
            }
            return imageurl;

        }


        internal static int AddAthleteBadges(AtheleteBadgesDTO atheleteBadgesDTO, AtheleteSocialDTO athleteSocial)
        {
            int id = 0;
            try
            {
                using (SqlConnection myConn = ConnectToDb())
                {
                    if (null != myConn)
                    {
                        SqlCommand cmd = new SqlCommand("[AddAtheleteBadges]", myConn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter BadgeTypeId = cmd.Parameters.Add("@BadgeTypeId", SqlDbType.Int);
                        BadgeTypeId.Direction = ParameterDirection.Input;
                        BadgeTypeId.Value = athleteSocial.BadgeTypeId;

                        SqlParameter BadgeCount = cmd.Parameters.Add("@BadgeCount", SqlDbType.Int);
                        BadgeCount.Direction = ParameterDirection.Input;
                        BadgeCount.Value = athleteSocial.Count;

                        SqlParameter AtheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                        AtheleteId.Direction = ParameterDirection.Input;
                        AtheleteId.Value = atheleteBadgesDTO.AtheleteId;

                        SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.VarChar, 50);
                        Status.Direction = ParameterDirection.Input;
                        Status.Value = atheleteBadgesDTO.Status;

                        SqlParameter CoachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                        CoachName.Direction = ParameterDirection.Input;
                        CoachName.Value = athleteSocial.CoachName;

                        SqlParameter CoachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                        CoachEmailId.Direction = ParameterDirection.Input;
                        CoachEmailId.Value = athleteSocial.CoachEmailId;

                        SqlParameter ClubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                        ClubName.Direction = ParameterDirection.Input;
                        ClubName.Value = athleteSocial.ClubName;

                        SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                        Id.Direction = ParameterDirection.Output;

                        cmd.ExecuteNonQuery();

                        id = (int)cmd.Parameters["@Id"].Value;


                    }//myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddAthleteBadges", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        //internal static int AddAtheleteSocial(AtheleteSocialDTO atheleteSocialDTO)
        //{
        //    int id = 0;
        //    try
        //    {
        //        SqlConnection myConn = ConnectTODB();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[AddAtheleteSocial]", myConn);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            SqlParameter AboutStatement = cmd.Parameters.Add("@AboutStatement", SqlDbType.VarChar, 500);
        //            AboutStatement.Direction = ParameterDirection.Input;
        //            AboutStatement.Value = atheleteSocialDTO.AboutStatement;

        //            SqlParameter Club = cmd.Parameters.Add("@Club", SqlDbType.VarChar, 500);
        //            Club.Direction = ParameterDirection.Input;
        //            Club.Value = atheleteSocialDTO.Club;

        //            SqlParameter Coach = cmd.Parameters.Add("@Coach", SqlDbType.VarChar, 100);
        //            Coach.Direction = ParameterDirection.Input;
        //            Coach.Value = atheleteSocialDTO.Coach;

        //            SqlParameter Email = cmd.Parameters.Add("@Email", SqlDbType.VarChar, 100);
        //            Email.Direction = ParameterDirection.Input;
        //            Email.Value = atheleteSocialDTO.Email; 

        //            SqlParameter AtheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
        //            AtheleteId.Direction = ParameterDirection.Input;
        //            AtheleteId.Value = atheleteSocialDTO.AtheleteId;

        //            SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
        //            Id.Direction = ParameterDirection.Output;

        //            cmd.ExecuteNonQuery();

        //            id = (int)cmd.Parameters["@Id"].Value;


        //            myConn.Close();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //       
        //        throw;
        //    }

        //    return id;
        //}


        internal static int UpdateCoachProfile(CoachProfileDTO coachProfileDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachProfile]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachid = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachid.Direction = ParameterDirection.Input;
                    coachid.Value = coachProfileDTO.CoachId;

                    SqlParameter description = cmd.Parameters.Add("@description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = coachProfileDTO.Description;

                    SqlParameter PreferredStrokes = cmd.Parameters.Add("@PreferredStrokes", SqlDbType.VarChar, 500);
                    PreferredStrokes.Direction = ParameterDirection.Input;
                    PreferredStrokes.Value = coachProfileDTO.PreferredStrokes;

                    SqlParameter PhoneNumber = cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar, 20);
                    PhoneNumber.Direction = ParameterDirection.Input;
                    PhoneNumber.Value = coachProfileDTO.Phonenumber;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateCoachProfile", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int UpdateAthleteBadgesStatus(AtheleteBadgesDTO athleteBadges)
        {
            int id = 0;
            try
            {
                using (SqlConnection myConn = ConnectToDb())
                {
                    if (null != myConn)
                    {
                        SqlCommand cmd = new SqlCommand("[UpdateAthleteBadgesStatus]", myConn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter AtheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                        AtheleteId.Direction = ParameterDirection.Input;
                        AtheleteId.Value = athleteBadges.AtheleteId;

                        SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.VarChar, 50);
                        Status.Direction = ParameterDirection.Input;
                        Status.Value = athleteBadges.Status;

                        SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                        Id.Direction = ParameterDirection.Output;

                        cmd.ExecuteNonQuery();

                        id = (int)cmd.Parameters["@Id"].Value;


                    }//myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateAthleteBadgesStatus", "", ex.Message, "Exception", 0);
                throw;
            }


            return id;
        }


        internal static List<CoachDTO> GetMutualLikesAthlete(int athleteId)
        {
            List<CoachDTO> institutes = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (myConn != null)
                {
                    SqlCommand cmd = new SqlCommand("[GetMutualAthleteInstitutes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AthleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteId.Direction = ParameterDirection.Input;
                    AthleteId.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO coach = new CoachDTO();
                        coach.Id = (int)myData["id"];
                        coach.Name = (myData["name"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["name"]);
                        coach.AreasInterested = (myData["AreasInterested"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AreasInterested"]);
                        coach.Address = (myData["Address"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Address"]);
                        coach.Emailaddress = (myData["CoachEmail"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmail"]); //Headcoach email
                        coach.Description = (myData["Description"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Description"]);
                        coach.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        coach.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        coach.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        coach.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        coach.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        coach.ClassificationName = (myData["ClassificationName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClassificationName"]);
                        coach.State = (myData["State"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["State"]);
                        coach.City = (myData["City"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["City"]);
                        coach.Zip = (myData["Zip"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Zip"]);
                        coach.WebsiteURL = (myData["WebsiteURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WebsiteURL"]);
                        coach.FaidURL = (myData["FaidURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["FaidURL"]);
                        coach.ApplURL = (myData["ApplURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ApplURL"]);
                        coach.Latitude = (myData["Latitude"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Latitude"]);
                        coach.Longitude = (myData["Longitude"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Longitude"]);
                        coach.EnrollmentNo = (myData["EnrollmentNo"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentNo"]);
                        coach.ApplicationNo = (myData["ApplicationNo"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ApplicationNo"]);
                        coach.AdmissionNo = (myData["AdmissionNo"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AdmissionNo"]);
                        coach.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ProfilePicURL"]);
                        coach.FacebookId = "0";//(string)myData["CoachFacebook"].ToString();
                        //  coach.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        coach.CoverPicURL = (myData["CoverPicURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoverPicURL"]);
                        coach.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        coach.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        coach.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        coach.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        coach.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        coach.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        coach.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        coach.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        coach.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        coach.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        coach.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        coach.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        institutes.Add(coach);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetMutualLikesAthlete", "", ex.Message, "Exception", 0);
                throw;
            }
            return institutes;
        }


        internal static int UpdateAthleteInstitution(int AtheleteId, int InstituteId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateAtheleteInstitution]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = AtheleteId;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = InstituteId;

                    SqlParameter Active = cmd.Parameters.Add("@Active", SqlDbType.Int);
                    Active.Direction = ParameterDirection.Input;
                    Active.Value = true;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateAthleteInstitution", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int UpdateAtheleteToCollegeAthlete(int AtheleteId, int InstituteId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateAtheleteToCollegeAthlete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = AtheleteId;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = InstituteId;

                    SqlParameter AtheleteType = cmd.Parameters.Add("@AtheleteType", SqlDbType.Int);
                    AtheleteType.Direction = ParameterDirection.Input;
                    AtheleteType.Value = Utility.AtheleteType.CollegeAthelete;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateAtheleteToCollegeAthlete", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static List<AthleteDTO> GetMutualSeniorInstituteAtheletes(int InstituteId)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetMutualSeniorInstituteAtheletes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = InstituteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        // v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        //  v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                        // v.AtheleteId = (int)myData["AtheleteId"];
                        // v.CoachId = (int)myData["CoachId"];
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        //  v.UniversityName = (string)myData["UniversityName"].ToString();
                        // v.UniversityProfilePic = (string)myData["UniversityProfilePic"].ToString();
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetMutualSeniorInstituteAtheletes", "", ex.Message, "Exception", 0);
                throw;
            }

            return athletes;
        }

        internal static List<CoachUserDTO> GetCollegeCoachesForInstitute(int InstituteId, int CoachId, int Offset, int Max)
        {

            List<CoachUserDTO> coaches = new List<CoachUserDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCollegeCoachesForInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = InstituteId;

                    SqlParameter offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offset.Direction = ParameterDirection.Input;
                    offset.Value = Offset;

                    SqlParameter max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    max.Direction = ParameterDirection.Input;
                    max.Value = Max;

                    SqlParameter aheleteId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    aheleteId.Direction = ParameterDirection.Input;
                    aheleteId.Value = CoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachUserDTO coachUserDTO = new CoachUserDTO();
                        coachUserDTO.CoachId = (int)myData["Id"];
                        coachUserDTO.Emailaddress = (myData["EmailAddress"] == DBNull.Value) ? "" : (string)myData["EmailAddress"];
                        coachUserDTO.ActivationCode = (myData["activation_code"] == DBNull.Value) ? "" : (string)myData["activation_code"];
                        coachUserDTO.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        coachUserDTO.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        int Head = (int)myData["HeadCoach"];
                        coachUserDTO.HeadCoach = (Head == 1) ? true : false;
                        coachUserDTO.Payment_Paid = (myData["Payment_Paid"] == DBNull.Value) ? false : (bool)myData["Payment_Paid"];
                        coachUserDTO.Active = (myData["Active"] == DBNull.Value) ? false : (bool)myData["Active"];
                        coachUserDTO.PreferredStrokes = (myData["PreferredStrokes"] == DBNull.Value) ? string.Empty : (string)myData["PreferredStrokes"];
                        coachUserDTO.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : (string)myData["ProfilePicURL"];
                        coachUserDTO.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        coachUserDTO.DeviceId = (myData["DeviceId1"] == DBNull.Value) ? string.Empty : (string)myData["DeviceId1"];
                        coachUserDTO.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"];
                        coaches.Add(coachUserDTO);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCollegeCoachesForInstitute", "", ex.Message, "Exception", 0);
                throw;
            }

            return coaches;
        }




        internal static List<AthleteDTO> GetCollegeAtheletes(string SearchText, string Emailaddress)
        {
            List<AthleteDTO> Atheletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCollegeAtheletes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter searchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchText.Direction = ParameterDirection.Input;
                    searchText.Value = SearchText;

                    SqlParameter emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 1000);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = Emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        Atheletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCollegeAtheletes", "", ex.Message, "Exception", 0);
                throw;
            }

            return Atheletes;
        }



        internal static List<AthleteDTO> GetCollegeAtheletesForInstitute(int InstituteId, int AtheleteId, int Offset, int Max)
        {
            List<AthleteDTO> Atheletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCollegeAtheletesForInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = InstituteId;

                    SqlParameter offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offset.Direction = ParameterDirection.Input;
                    offset.Value = Offset;

                    SqlParameter max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    max.Direction = ParameterDirection.Input;
                    max.Value = Max;

                    SqlParameter aheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    aheleteId.Direction = ParameterDirection.Input;
                    aheleteId.Value = AtheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.DeviceId = (myData["DeviceId1"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceId1"]);
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceType"]);
                        Atheletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCollegeAtheletesForInstitute", "", ex.Message, "Exception", 0);
                throw;
            }

            return Atheletes;
        }



        internal static CoachDTO GetUniversityProfile(int InstituteId)
        {

            CoachDTO v = new CoachDTO();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUniversityProfile]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = InstituteId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["Emailaddress"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.FacebookId = "0";// (string)myData["CoachFacebook"].ToString();
                        v.TotalCount = 1;// (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AdType = 2;


                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetUniversityProfile", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static List<AthleteDTO> GetAllAthletesByCollegeAthelete(string emailaddress, int offset, int max, string Description, string SearchText)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllAtheletesByCollegeAtheleteEmail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;


                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;


                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlParameter description = cmd.Parameters.Add("@description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = Description;

                    SqlParameter searchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchText.Direction = ParameterDirection.Input;
                    searchText.Value = SearchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicLevel"]);
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["YearLevel"]);
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteRate"]);
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Premium"]);
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAllAthletesByCollegeAthelete", "", ex.Message, "Exception", 0);
                throw;
            }
            return athletes;
        }




        internal static List<AthleteDTO> GetCollegeAtheleteFavourites(string emailaddress, int offset, int max)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();
            DateTime dtAtheleteDate = new DateTime();
            DateTime dtInstituteDate = new DateTime();
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCollegeAtheleteFavourites]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        dtAtheleteDate = (myData["AtheleteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["AtheleteDate"];
                        dtInstituteDate = (myData["InstituteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(myData["InstituteDate"]);
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.Liked = ((int)myData["Likes"] != 0) ? true : false;
                        if (v.Liked)
                        { v.FavDate = (dtAtheleteDate > dtInstituteDate) ? dtAtheleteDate : dtInstituteDate; }
                        else
                        { v.FavDate = dtAtheleteDate; }
                        // v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                        v.AtheleteId = (int)myData["AtheleteId"];
                        v.CoachId = (int)myData["CoachId"];
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.UniversityName = (string)myData["UniversityName"].ToString();
                        v.UniversityProfilePic = (string)myData["UniversityProfilePic"].ToString();
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCollegeAtheleteFavourites", "", ex.Message, "Exception", 0);
                throw;
            }

            return athletes;
        }



        internal static List<AthleteDTO> GetCollegeAtheletesInAtheleteFavourites(string emailaddress, int offset, int max)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();
            DateTime dtAtheleteDate = new DateTime();
            DateTime dtInstituteDate = new DateTime();
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCollegeAtheletesinAtheleteFavourites]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        dtAtheleteDate = (myData["AtheleteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["AtheleteDate"];
                        dtInstituteDate = (myData["InstituteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(myData["InstituteDate"]);
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.Liked = ((int)myData["Likes"] != 0) ? true : false;
                        if (v.Liked)
                        { v.FavDate = (dtAtheleteDate > dtInstituteDate) ? dtAtheleteDate : dtInstituteDate; }
                        else
                        { v.FavDate = dtAtheleteDate; }
                        // v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                        v.AtheleteId = (int)myData["AtheleteId"];
                        v.CoachId = (int)myData["CoachId"];
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.UniversityName = (string)myData["UniversityName"].ToString();
                        v.UniversityProfilePic = (string)myData["UniversityProfilePic"].ToString();
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCollegeAtheletesInAtheleteFavourites", "", ex.Message, "Exception", 0);
                throw;
            }

            return athletes;
        }

        internal static int AddAtheleteandCollegeAthleteMatch(AtheleteCollegeAtheleteDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAtheleteCollegeAthleteMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AtheleteEmail = cmd.Parameters.Add("@AtheleteEmail", SqlDbType.VarChar, 500);
                    AtheleteEmail.Direction = ParameterDirection.Input;
                    AtheleteEmail.Value = atheleteMatchDTO.AtheleteEmailAddress;

                    SqlParameter CollegeAtheleteId = cmd.Parameters.Add("@CollegeAtheleteId", SqlDbType.VarChar, 500);
                    CollegeAtheleteId.Direction = ParameterDirection.Input;
                    CollegeAtheleteId.Value = atheleteMatchDTO.CollegeAtheleteId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = atheleteMatchDTO.Status;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;
                    // id = (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddAtheleteandCollegeAthleteMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }



        internal static int AddCollegeAtheleteandAtheleteMatch(AtheleteCollegeAtheleteDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCollegeAtheleteAtheleteMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AtheleteEmail = cmd.Parameters.Add("@AtheleteEmail", SqlDbType.VarChar, 500);
                    AtheleteEmail.Direction = ParameterDirection.Input;
                    AtheleteEmail.Value = atheleteMatchDTO.AtheleteEmailAddress;

                    SqlParameter CollegeAtheleteId = cmd.Parameters.Add("@CollegeAtheleteId", SqlDbType.VarChar, 500);
                    CollegeAtheleteId.Direction = ParameterDirection.Input;
                    CollegeAtheleteId.Value = atheleteMatchDTO.CollegeAtheleteId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = atheleteMatchDTO.Status;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;
                    // id = (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddCollegeAtheleteandAtheleteMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }

        internal static int DeleteAtheleteandCollegeAthleteMatch(AtheleteCollegeAtheleteDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAtheleteCollegeAthleteMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AtheleteEmail = cmd.Parameters.Add("@AtheleteEmail", SqlDbType.VarChar, 500);
                    AtheleteEmail.Direction = ParameterDirection.Input;
                    AtheleteEmail.Value = atheleteMatchDTO.AtheleteEmailAddress;

                    SqlParameter CollegeAtheleteId = cmd.Parameters.Add("@CollegeAtheleteId", SqlDbType.VarChar, 500);
                    CollegeAtheleteId.Direction = ParameterDirection.Input;
                    CollegeAtheleteId.Value = atheleteMatchDTO.CollegeAtheleteId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteAtheleteandCollegeAthleteMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }




        internal static int DeleteCollegeAtheleteandAtheleteMatch(AtheleteCollegeAtheleteDTO atheleteMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCollegeAtheleteAtheleteMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AtheleteEmail = cmd.Parameters.Add("@AtheleteEmail", SqlDbType.VarChar, 500);
                    AtheleteEmail.Direction = ParameterDirection.Input;
                    AtheleteEmail.Value = atheleteMatchDTO.AtheleteEmailAddress;

                    SqlParameter CollegeAtheleteId = cmd.Parameters.Add("@CollegeAtheleteId", SqlDbType.VarChar, 500);
                    CollegeAtheleteId.Direction = ParameterDirection.Input;
                    CollegeAtheleteId.Value = atheleteMatchDTO.CollegeAtheleteId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteCollegeAtheleteandAtheleteMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<CoachDTO> GetAllCoachesPagingWithSearch(string emailaddress, int offset, int max, int NCAA, int Conference, int Admission, string Description, string SearchText)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllInstitutesByAtheleteEmail_v2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlParameter ncaa = cmd.Parameters.Add("@ncaa", SqlDbType.Int);
                    ncaa.Direction = ParameterDirection.Input;
                    ncaa.Value = NCAA;

                    SqlParameter conference = cmd.Parameters.Add("@conference", SqlDbType.Int);
                    conference.Direction = ParameterDirection.Input;
                    conference.Value = Conference;

                    SqlParameter admission = cmd.Parameters.Add("@admission", SqlDbType.Int);
                    admission.Direction = ParameterDirection.Input;
                    admission.Value = Admission;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = Description;

                    SqlParameter searchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchText.Direction = ParameterDirection.Input;
                    searchText.Value = SearchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["CoachEmail"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.FacebookId = (string)myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AdType = 2;
                        Coaches.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAllCoachesPagingWithSearch", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }


        internal static List<AthleteDTO> GetAllAthletePagingWithSearch(string emailaddress, int offset, int max, int GPA, int Social, string Description, string Strokes, string SearchText)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();
            var coaches = UsersDal.GetCoachProfileByEmail(emailaddress);
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllAtheletesByCoachEmail_v5]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;


                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;


                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlParameter gpa = cmd.Parameters.Add("@gpa", SqlDbType.Float);
                    gpa.Direction = ParameterDirection.Input;
                    gpa.Value = GPA;

                    SqlParameter social = cmd.Parameters.Add("@social", SqlDbType.Int);
                    social.Direction = ParameterDirection.Input;
                    social.Value = Social;

                    SqlParameter description = cmd.Parameters.Add("@description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = Description;

                    SqlParameter PreferredStrokes = cmd.Parameters.Add("@PreferredStrokes", SqlDbType.VarChar, 500);
                    PreferredStrokes.Direction = ParameterDirection.Input;
                    PreferredStrokes.Value = Strokes;

                    SqlParameter searchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchText.Direction = ParameterDirection.Input;
                    searchText.Value = SearchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicLevel"]);
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["YearLevel"]);
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteRate"]);
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Premium"]);
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.RightSwiped = ((int)myData["RightSwiped"] == 0) ? false : true;
                        var payment = UsersDalV5.GetPayment(coaches.CoachId, 2);
                        if (payment != null)
                        {
                            if (Convert.ToDateTime(payment.ExpiryDate) < DateTime.Now)
                            {
                                v.RightSwiped = false;
                             }
                        }
                        else
                        {
                            v.RightSwiped = false;
                        }
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value) ? string.Empty : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        string email = string.Empty;
                        email = (v.LoginType == 1 || v.LoginType == 4) ? v.Emailaddress : v.UserName;
                        v.MultiSportCount = Dal2.GetMultiSportCount(email, v.LoginType, Lemonaid_web_api.Utility.UserTypes.HighSchooAthlete);
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAllAthletePagingWithSearch", "", ex.Message, "Exception", 0);
                throw;
            }

            return athletes;
        }


        internal static AthleteDTO GetAthleteProfile_v2(string emailaddress)
        {
            AthleteDTO v = new AthleteDTO();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteProfile_v2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = 1;// (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivAddress"]);
                        v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivEmailAddress"]);
                        v.UniversityName = (myData["UniversityName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UniversityName"]);
                        v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnviPhoneNumber"]);
                        v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivProfilePic"]);
                        v.UniversitySize = (myData["UnivSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["UnivSize"]);
                        v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClassificationName"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.ClubId = (myData["ClubId"] == DBNull.Value) ? 0 : (int)myData["ClubId"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAthleteProfile_v2", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static List<CoachDTO> GetCoachesWithSearch(string emailaddress, int offset, int max, string SearchText)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstitutes_v2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;

                    SqlParameter searchText = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchText.Direction = ParameterDirection.Input;
                    searchText.Value = SearchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["coachEmail"].ToString();//CoachEmail
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.FacebookId = "0";//(string)myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.Payment_Paid = (myData["payment_paid"] == DBNull.Value) ? false : (bool)myData["payment_paid"];
                        Coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoachesWithSearch", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }



        internal static void GetAthleteDetails(int athleteId, ref string athleteName, ref string athleteEmailAddress)
        {

            try
            {
                SqlConnection myConn = ConnectToDb();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteDetails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter AthleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteId.Direction = ParameterDirection.Input;
                    AthleteId.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {
                        athleteName = (myData["Name"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Name"]);
                        athleteEmailAddress = (myData["EmailAddress"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EmailAddress"]);
                    }
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAthleteDetails", "", ex.Message, "Exception", 0);
                throw;
            }
        }

        internal static int DeleteCollegeAtheleteAtheletes(string Emailaddress)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCollegeAtheleteAtheletes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = Emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteCollegeAtheleteAtheletes", "", ex.Message, "Exception", 0);
                throw;
            }

            return i;
        }


        internal static bool IsCollegeAthleteActive(int AtheleteId)
        {

            bool IsActive = false;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[IsCollegeAtheleteActive]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = AtheleteId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        int Active = (int)myData["Active"];
                        IsActive = (Active == 1) ? true : false;

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/IsCollegeAthleteActive", "", ex.Message, "Exception", 0);
                throw;
            }

            return IsActive;
        }


        internal static string UpdateCoachPassword(string userName, string token, string confirmPassword)
        {

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachPassword]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Token = cmd.Parameters.Add("@Token", SqlDbType.VarChar, 500);
                    Token.Direction = ParameterDirection.Input;
                    Token.Value = token;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = userName;

                    SqlParameter Password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 100);
                    Password.Direction = ParameterDirection.Input;
                    // Password.Value =  confirmPassword;
                    Password.Value = UsersDalV4.Encrypt(confirmPassword);

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        userName = (string)myData["Email"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateCoachPassword", "", ex.Message, "Exception", 0);
                throw;
            }

            return userName;
        }


        internal static CoachProfileDTO GetCoachProfile(int coachId)
        {
            CoachProfileDTO v = new CoachProfileDTO();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachProfile]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter CoachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    CoachId.Direction = ParameterDirection.Input;
                    CoachId.Value = coachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.CoachId = (int)myData["ID"];
                        v.Description = (myData["description"] == DBNull.Value) ? string.Empty : (string)myData["description"].ToString();
                        v.PreferredStrokes = (myData["PreferredStrokes"] == DBNull.Value) ? string.Empty : (string)myData["PreferredStrokes"].ToString();
                        v.Emailaddress = (myData["emailaddress"] == DBNull.Value) ? string.Empty : (string)myData["emailaddress"].ToString();
                        int Head = (int)myData["HeadCoach"];
                        v.HeadCoach = (Head == 1) ? true : false;
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.payment_paid = (myData["Payment_Paid"] == DBNull.Value) ? false : (bool)myData["Payment_Paid"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoachProfile", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static int UpdateCoachProfile_v2(CoachProfileDTO coachProfileDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachProfile_v2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachid = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachid.Direction = ParameterDirection.Input;
                    coachid.Value = coachProfileDTO.CoachId;

                    SqlParameter description = cmd.Parameters.Add("@description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = coachProfileDTO.Description;

                    SqlParameter PreferredStrokes = cmd.Parameters.Add("@PreferredStrokes", SqlDbType.VarChar, 500);
                    PreferredStrokes.Direction = ParameterDirection.Input;
                    PreferredStrokes.Value = coachProfileDTO.PreferredStrokes;

                    SqlParameter PhoneNumber = cmd.Parameters.Add("@PhoneNumber", SqlDbType.VarChar, 20);
                    PhoneNumber.Direction = ParameterDirection.Input;
                    PhoneNumber.Value = coachProfileDTO.Phonenumber;

                    SqlParameter AcademicRate = cmd.Parameters.Add("@AcademicRate", SqlDbType.Int);
                    AcademicRate.Direction = ParameterDirection.Input;
                    AcademicRate.Value = coachProfileDTO.AcademicRate;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateCoachProfile_v2", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<CoachDTO> GetAtheleteFavourites_v2(string emailaddress, int offset, int max)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();
            DateTime dtAtheleteDate = new DateTime();
            DateTime dtInstituteDate = new DateTime();
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteFavourites_v2]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlParameter Offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    Offset.Direction = ParameterDirection.Input;
                    Offset.Value = offset;

                    SqlParameter Max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    Max.Direction = ParameterDirection.Input;
                    Max.Value = max;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["CoachEmail"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.FacebookId = (string)myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        dtAtheleteDate = (myData["AtheleteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["AtheleteDate"];
                        dtInstituteDate = (myData["InstituteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InstituteDate"];
                        v.Liked = ((int)myData["Likes"] != 0) ? true : false;
                        if (v.Liked)
                        { v.FavDate = (dtAtheleteDate > dtInstituteDate) ? dtAtheleteDate : dtInstituteDate; }
                        else
                        { v.FavDate = dtAtheleteDate; }
                        v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                        v.Accept = (myData["Accept"] == DBNull.Value) ? false : (bool)(myData["Accept"]);
                        v.TotalAtheleteAcceptedCount = (myData["TotalAcceptedCount"] == DBNull.Value) ? 0 : (int)(myData["TotalAcceptedCount"]);
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.AtheleteId = (int)myData["AtheleteId"];
                        v.CoachId = (int)myData["CoachId"];
                        v.AtheleteName = (string)myData["AtheleteName"].ToString();
                        v.AtheleteProfilePic = (string)myData["AtheleteProfilePic"].ToString();
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.Payment_Paid = (myData["payment_paid"] == DBNull.Value) ? false : (bool)myData["payment_paid"];
                        Coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAtheleteFavourites_v2", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }



        internal static List<QuestionaireDTO> GetQuestionaire(string emailaddress)
        {

            List<QuestionaireDTO> questionaire = new List<QuestionaireDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetQuestionaire]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailAddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailAddress.Direction = ParameterDirection.Input;
                    emailAddress.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        QuestionaireDTO v = new QuestionaireDTO();
                        v.QuestionId = (int)myData["id"];
                        v.Question = (string)myData["Question"].ToString();

                        questionaire.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetQuestionaire", "", ex.Message, "Exception", 0);
                throw;
            }

            return questionaire;
        }

        internal static int AddAtheleteQuestionaire(QuestionaireDTO questionaireDto)
        {


            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAtheleteQuestionaire]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = questionaireDto.AtheleteId;

                    SqlParameter questionId = cmd.Parameters.Add("@QuestionId", SqlDbType.Int);
                    questionId.Direction = ParameterDirection.Input;
                    questionId.Value = questionaireDto.QuestionId;

                    SqlParameter answer = cmd.Parameters.Add("@Answer", SqlDbType.Int);
                    answer.Direction = ParameterDirection.Input;
                    answer.Value = questionaireDto.AnswerId;

                    SqlParameter idPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    idPar.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddAtheleteQuestionaire", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<VideoDTO> GetAtheleteIdealVideos(string emailaddress)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteIdealVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? string.Empty : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : (int)myData["VideoType"];
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : (int)myData["Duration"];
                        v.VideoNumber = (myData["VideoNumber"] == DBNull.Value) ? 0 : (int)myData["VideoNumber"];
                        v.Count = (int)myData["AtheleteVideoCount"];
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAtheleteIdealVideos", "", ex.Message, "Exception", 0);
                throw;
            }

            return videos;
        }

        internal static int AddNotifications(NotificationDTO notificationDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();



                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddNotifications]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = notificationDto.UserID;

                    SqlParameter NotificationId = cmd.Parameters.Add("@NotificationId", SqlDbType.Int);
                    NotificationId.Direction = ParameterDirection.Input;
                    NotificationId.Value = notificationDto.NotificationId;

                    SqlParameter Status = cmd.Parameters.Add("@Status", SqlDbType.VarChar, 8000);
                    Status.Direction = ParameterDirection.Input;
                    Status.Value = notificationDto.Status;

                    SqlParameter SenderId = cmd.Parameters.Add("@SenderId", SqlDbType.VarChar, 50);
                    SenderId.Direction = ParameterDirection.Input;
                    SenderId.Value = notificationDto.SenderId;

                    SqlParameter shortMessage = cmd.Parameters.Add("@ShortMessage", SqlDbType.VarChar, 500);
                    shortMessage.Direction = ParameterDirection.Input;
                    shortMessage.Value = notificationDto.ShortMessage;

                    SqlParameter longMessage = cmd.Parameters.Add("@LongMessage", SqlDbType.VarChar, 8000);
                    longMessage.Direction = ParameterDirection.Input;
                    longMessage.Value = notificationDto.LongMessage;

                    SqlParameter badgeTypeId = cmd.Parameters.Add("@BadgeTypeId", SqlDbType.Int);
                    badgeTypeId.Direction = ParameterDirection.Input;
                    badgeTypeId.Value = notificationDto.BadgeTypeId;

                    SqlParameter badgeCount = cmd.Parameters.Add("@BadgeCount", SqlDbType.Int);
                    badgeCount.Direction = ParameterDirection.Input;
                    badgeCount.Value = notificationDto.BadgeCount;

                    SqlParameter senderType = cmd.Parameters.Add("@senderType", SqlDbType.Int);
                    senderType.Direction = ParameterDirection.Input;
                    senderType.Value = notificationDto.SenderType;

                    SqlParameter receiverTypePar = cmd.Parameters.Add("@ReceiverType", SqlDbType.Int);
                    receiverTypePar.Direction = ParameterDirection.Input;
                    receiverTypePar.Value = notificationDto.ReceiverType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddNotifications", "", ex.Message, "Exception", 0);
                throw;
            }
            return id;
        }


        internal static List<NotificationDTO> GetUserNotifications(int UserId, int Offset, int Max)
        {

            List<NotificationDTO> Notifications = new List<NotificationDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetNotifications]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter userIdPar = cmd.Parameters.Add("@UserId", SqlDbType.Int);
                    userIdPar.Direction = ParameterDirection.Input;
                    userIdPar.Value = UserId;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = Offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = Max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        NotificationDTO notification = new NotificationDTO();
                        notification.ID = (int)myData["Id"];
                        notification.NotificationId = (int)myData["NotificationId"];
                        notification.UserID = (int)myData["UserID"];
                        notification.Status = (int)myData["Status"];
                        notification.SenderId = (int)myData["SenderId"];
                        notification.Message = (string)myData["Message"];
                        notification.NotificationType = (int)myData["NotificationType"];
                        notification.Date = (DateTime)myData["CreatedDate"];
                        notification.TotalCount = (int)myData["TotalCount"];
                        notification.ShortMessage = (myData["ShortMessage1"] == DBNull.Value) ? string.Empty : (string)myData["ShortMessage1"];
                        notification.LongMessage = (myData["LongMessage1"] == DBNull.Value) ? string.Empty : (string)myData["LongMessage1"];
                        notification.BadgeTypeId = (myData["BadgeTypeId"] == DBNull.Value) ? 0 : (int)myData["BadgeTypeId"];
                        notification.BadgeCount = (myData["BadgeCount"] == DBNull.Value) ? 0 : (int)myData["BadgeCount"];
                        notification.SenderType = (myData["SenderType"] == DBNull.Value) ? 0 : (int)myData["SenderType"];
                        notification.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : (string)myData["ProfilePicURL"];
                        notification.SenderName = (myData["sendername"] == DBNull.Value) ? string.Empty : (string)myData["sendername"];
                        Notifications.Add(notification);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetUserNotifications", "", ex.Message, "Exception", 0);
                throw;
            }

            return Notifications;
        }


        internal static int UpdateNotificationStatus(int Id, int status)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateNotificationStatus]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter IdPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    IdPar.Direction = ParameterDirection.Input;
                    IdPar.Value = Id;

                    SqlParameter statusPar = cmd.Parameters.Add("@status", SqlDbType.Int);
                    statusPar.Direction = ParameterDirection.Input;
                    statusPar.Value = status;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateNotificationStatus", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static ClubCoachDTO AddClubCoach(ClubCoachDTO athleteDTO)
        {
            ClubCoachDTO v = new ClubCoachDTO();
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddClubCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = athleteDTO.Name;

                    SqlParameter Gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    Gender.Direction = ParameterDirection.Input;
                    Gender.Value = athleteDTO.Gender;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = athleteDTO.Emailaddress;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = (athleteDTO.Description == null) ? string.Empty : athleteDTO.Description;

                    SqlParameter ProfilePicURL = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = athleteDTO.ProfilePicURL;

                    SqlParameter DOB = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    DOB.Direction = ParameterDirection.Input;
                    DOB.Value = athleteDTO.DOB;

                    SqlParameter ClubAddress = cmd.Parameters.Add("@ClubAddress", SqlDbType.VarChar, 100);
                    ClubAddress.Direction = ParameterDirection.Input;
                    ClubAddress.Value = athleteDTO.ClubAddress;

                    SqlParameter FacebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    FacebookId.Direction = ParameterDirection.Input;
                    FacebookId.Value = athleteDTO.FacebookId;

                    SqlParameter CoverPicURL = cmd.Parameters.Add("@CoverPicURL", SqlDbType.VarChar, 1000);
                    CoverPicURL.Direction = ParameterDirection.Input;
                    CoverPicURL.Value = athleteDTO.CoverPicURL;

                    SqlParameter Latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    Latitude.Direction = ParameterDirection.Input;
                    Latitude.Value = athleteDTO.Latitude;

                    SqlParameter Longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    Longitude.Direction = ParameterDirection.Input;
                    Longitude.Value = athleteDTO.Longitude;

                    SqlParameter ClubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    ClubName.Direction = ParameterDirection.Input;
                    ClubName.Value = string.IsNullOrWhiteSpace(athleteDTO.ClubName) ? string.Empty : athleteDTO.ClubName;

                    SqlParameter Active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    Active.Direction = ParameterDirection.Input;
                    Active.Value = athleteDTO.Active;

                    SqlParameter Phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    Phonenumber.Direction = ParameterDirection.Input;
                    Phonenumber.Value = string.IsNullOrWhiteSpace(athleteDTO.Phonenumber) ? string.Empty : athleteDTO.Phonenumber;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = (athleteDTO.DeviceType == null) ? string.Empty : athleteDTO.DeviceType;

                    SqlParameter DeviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 50);
                    DeviceId.Direction = ParameterDirection.Input;
                    DeviceId.Value = (athleteDTO.DeviceId == null) ? string.Empty : athleteDTO.DeviceId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.ClubAddress = (string)myData["ClubAddress"].ToString();
                        v.ClubName = (string)myData["ClubName"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddClubCoach", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }

        internal static ClubCoachDTO GetClubCoachProfile(string emailaddress)
        {
            ClubCoachDTO v = new ClubCoachDTO();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetClubCoachProfile]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.ClubAddress = myData["ClubAddress"].ToString();
                        v.ClubName = myData["ClubName"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.DeviceId = myData["DeviceName"].ToString();
                        v.DeviceType = myData["DeviceType"].ToString();

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetClubCoachProfile", "", ex.Message, "Exception", 0);
                throw;
            }

            return v;
        }


        internal static int AddClubCoachMatch(ClubCoachMatchDTO clubCoachMatchDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddClubCoachInstMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter facebookId = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    facebookId.Direction = ParameterDirection.Input;
                    facebookId.Value = clubCoachMatchDto.ClubCoachId;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = clubCoachMatchDto.InstituteId;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    status.Direction = ParameterDirection.Input;
                    status.Value = clubCoachMatchDto.Status;

                    SqlParameter idpar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    idpar.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddClubCoachMatch", "", ex.Message, "Exception", 0);
                throw;
            }


            return id;
        }


        internal static List<CoachDTO> GetClubCoachFavourites(int clubCoachId, int offset, int max)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetClubCoachFavourites]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = clubCoachId;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["Emailaddress"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];  
                        v.AthleticConference = (myData["AthleticConference"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleticConference"]);
                        v.ScholarshipMen = (myData["ScholarshipMen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipMen"]);
                        v.ScholarshipWomen = (myData["ScholarshipWomen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipWomen"]);
                        v.TuitionInState = (myData["TuitionInState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionInState"]);
                        v.TuitionOutState = (myData["TuitionOutState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionOutState"]);
                        Coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetClubCoachFavourites", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }


        internal static int DeleteClubCoachMatch(ClubCoachMatchDTO clubCoachMatchDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteClubCoachInstMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubCoachId = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubCoachId.Direction = ParameterDirection.Input;
                    clubCoachId.Value = clubCoachMatchDto.ClubCoachId;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = clubCoachMatchDto.InstituteId;

                    SqlParameter idPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    idPar.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteClubCoachMatch", "", ex.Message, "Exception", 0);
                throw;
            }


            return id;
        }

        internal static List<CoachDTO> GetAllInstitutesByClubCoach(int clubCoachId, int offset, int max, string searchText)
        {

            List<CoachDTO> Coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllInstitutesByClubCoach]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = clubCoachId;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;

                    SqlParameter searchTextPar = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchTextPar.Direction = ParameterDirection.Input;
                    searchTextPar.Value = searchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.AreasInterested = myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["CoachEmail"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.FacebookId = (string)myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AthleticConference = (myData["AthleticConference"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["AthleticConference"]);
                        v.EnrollmentFee = (myData["EnrollmentFee"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentFee"]);
                        v.Type = (myData["Type"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Type"]);
                        v.TuitionFee = (myData["TuitionFee"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionFee"]);
                        v.Scholarship = (myData["Scholarship"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Scholarship"]);
                        v.EnrollmentNo = (myData["EnrollmentNo"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["EnrollmentNo"]);
                        v.ScholarshipMen = (myData["ScholarshipMen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipMen"]);
                        v.ScholarshipWomen = (myData["ScholarshipWomen"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ScholarshipWomen"]);
                        v.TuitionInState = (myData["TuitionInState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionInState"]);
                        v.TuitionOutState = (myData["TuitionOutState"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["TuitionOutState"]);
                        v.VirtualVideoURL = (myData["VirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualVideoURL"]);
                        v.VirtualThumbnailURL = (myData["VirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualThumbnailURL"]);
                        v.MatchesVirtualVideoURL = (myData["MatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualVideoURL"]);
                        v.MatchesVirtualThumbnailURL = (myData["MatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualThumbnailURL"]);
                        v.AdType = 2;
                        Coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAllInstitutesByClubCoach", "", ex.Message, "Exception", 0);
                throw;
            }

            return Coaches;
        }


        internal static List<AthleteDTO> GetAthletesinClubCoachClub(int clubcoachid)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthletesinClubCoachClub]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubcoachidPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubcoachidPar.Direction = ParameterDirection.Input;
                    clubcoachidPar.Value = clubcoachid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicLevel"]);
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["YearLevel"]);
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteRate"]);
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Premium"]);
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.ClubName = (myData["CName"] == DBNull.Value) ? string.Empty : (string)myData["CName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.ClubId = (myData["ClubId"] == DBNull.Value) ? 0 : (int)myData["ClubId"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.AthleteShareVideosCount = (myData["AthleteShareCount"] == DBNull.Value) ? 0 : (int)myData["AthleteShareCount"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAthletesinClubCoachClub", "", ex.Message, "Exception", 0);
            }


            return athletes;
        }

        internal static List<AthleteDTO> GetAthletesinClubCoachClubForTeam(int clubcoachid)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthletesinClubCoachClubForTeam]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubcoachidPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubcoachidPar.Direction = ParameterDirection.Input;
                    clubcoachidPar.Value = clubcoachid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicLevel"]);
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["YearLevel"]);
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteRate"]);
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Premium"]);
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.ClubName = (myData["CName"] == DBNull.Value) ? string.Empty : (string)myData["CName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.ClubId = (myData["ClubId"] == DBNull.Value) ? 0 : (int)myData["ClubId"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAthletesinClubCoachClubForTeam", "", ex.Message, "Exception", 0);
            }


            return athletes;
        }

        internal static int RecommendAthletesToUniversities(int athleteId, int clubcoachId, int instituteId)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[RecommendedAthleteToInstitutes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = athleteId;

                    SqlParameter clubCoachPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubCoachPar.Direction = ParameterDirection.Input;
                    clubCoachPar.Value = clubcoachId;

                    SqlParameter instituteIdPar = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteIdPar.Direction = ParameterDirection.Input;
                    instituteIdPar.Value = instituteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["Id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/RecommendAthletesToUniversities", "", ex.Message, "Exception", 0);
                throw;
            }


            return id;
        }


        internal static int DeleteClubCoachInstitutes(int clubCoachId)
        {

            int i = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteClubCoachInstitutes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = clubCoachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        i = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/DeleteClubCoachInstitutes", "", ex.Message, "Exception", 0);
                throw;
            }

            return i;
        }

        internal static int UpdateClubCoachToAthelete(int clubCoachId, int atheleteId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateClubCoachToAthelete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@clubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = clubCoachId;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/UpdateClubCoachToAthelete", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int AddClub(ClubDTO clubDto)
        {
            int Id = 0;
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddClub]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = clubDto.Name;

                    SqlParameter CreatedBy = cmd.Parameters.Add("@CreatedBy", SqlDbType.Int);
                    CreatedBy.Direction = ParameterDirection.Input;
                    CreatedBy.Value = clubDto.UserId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        Id = (int)myData["id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddClub", "", ex.Message, "Exception", 0);
                throw;
            }


            return Id;
        }

        internal static List<ClubDTO> GetClubs()
        {

            List<ClubDTO> clubs = new List<ClubDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetClubs]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ClubDTO v = new ClubDTO();
                        v.Name = myData["name"].ToString();
                        v.Id = (int)myData["Id"];
                        clubs.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetClubs", "", ex.Message, "Exception", 0);
            }

            return clubs;
        }


        internal static List<AthleteDTO> GetRecommendAthletesinInstitutes(int UniversityId, int ClubCoachId)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetRecommendAthletesinInstitutes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter universityIdPar = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    universityIdPar.Direction = ParameterDirection.Input;
                    universityIdPar.Value = UniversityId;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = ClubCoachId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetRecommendAthletesinInstitutes", "", ex.Message, "Exception", 0);
            }


            return athletes;
        }


        internal static SupportMailDTO GetSupportMails(string name)
        {

            SupportMailDTO s = new SupportMailDTO();
            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSupportMails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 500);
                    namePar.Direction = ParameterDirection.Input;
                    namePar.Value = name;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        s.EmailAddress = (string)myData["EmailAddress"];
                        s.Password = (string)myData["Password"];
                        s.Host = (string)myData["Host"];
                        s.Port = (string)myData["Port"];
                        s.SSL = (string)myData["SSL"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetSupportMails", "", ex.Message, "Exception", 0);
                throw;
            }

            return s;
        }


        internal static int AddEmailLog(EmailDTO emailDTO)
        {
            int id = 0;
            SqlConnection myConn = ConnectToDb();

            try
            {
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddEmailLog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter from = cmd.Parameters.Add("@From", SqlDbType.VarChar, 1000);
                    from.Direction = ParameterDirection.Input;
                    from.Value = emailDTO.From;

                    SqlParameter to = cmd.Parameters.Add("@To", SqlDbType.VarChar, 1000);
                    to.Direction = ParameterDirection.Input;
                    to.Value = emailDTO.To;

                    SqlParameter subject = cmd.Parameters.Add("@Subject", SqlDbType.VarChar, 1000);
                    subject.Direction = ParameterDirection.Input;
                    subject.Value = emailDTO.Subject;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    status.Direction = ParameterDirection.Input;
                    status.Value = emailDTO.Status;

                    SqlParameter mailType = cmd.Parameters.Add("@MailType", SqlDbType.VarChar, 50);
                    mailType.Direction = ParameterDirection.Input;
                    mailType.Value = emailDTO.MailType;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {

                        id = (int)myData["Id"];
                    }
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddEmailLog", "", ex.Message, "Exception", 0);
            }
            return id;
        }


        internal static NotificationDTO GetNotificationsById(int notificationId)
        {

            NotificationDTO notification = new NotificationDTO();

            SqlConnection myConn = ConnectToDb();

            try
            {
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetNotificationsById]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter notificationIdPar = cmd.Parameters.Add("@NotificationId", SqlDbType.Int);
                    notificationIdPar.Direction = ParameterDirection.Input;
                    notificationIdPar.Value = notificationId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        notification.ID = (int)myData["Id"];
                        notification.NotificationId = (int)myData["NotificationId"];
                        notification.UserID = (int)myData["UserID"];
                        notification.Status = (int)myData["Status"];
                        notification.SenderId = (int)myData["SenderId"];
                        notification.Message = (string)myData["Message"];
                        notification.NotificationType = (int)myData["NotificationType"];
                        notification.ShortMessage = (myData["ShortMessage"] == DBNull.Value) ? string.Empty : (string)myData["ShortMessage"];
                        notification.LongMessage = (myData["LongMessage"] == DBNull.Value) ? string.Empty : (string)myData["LongMessage"];
                        notification.BadgeTypeId = (myData["BadgeTypeId"] == DBNull.Value) ? 0 : (int)myData["BadgeTypeId"];
                        notification.BadgeCount = (myData["BadgeCount"] == DBNull.Value) ? 0 : (int)myData["BadgeCount"];
                        notification.SenderType = (myData["SenderType"] == DBNull.Value) ? 0 : (int)myData["SenderType"];

                    }
                    myData.Close();
                    myConn.Close();
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetNotificationsById", "", ex.Message, "Exception", 0);
            }


            return notification;
        }


        internal static CoachDTO GetCoachProfileByEmail(string emailaddress)
        {

            CoachDTO v = new CoachDTO();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachProfileByEmail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.AreasInterested = myData["AreasInterested"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Emailaddress = myData["CoachEmail"].ToString();
                        v.Description = myData["Description"].ToString();
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = myData["ClassificationName"].ToString();
                        v.State = myData["State"].ToString();
                        v.City = myData["City"].ToString();
                        v.Zip = myData["Zip"].ToString();
                        v.WebsiteURL = myData["WebsiteURL"].ToString();
                        v.FaidURL = myData["FaidURL"].ToString();
                        v.ApplURL = myData["ApplURL"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.EnrollmentNo = myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = myData["ApplicationNo"].ToString();
                        v.AdmissionNo = myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.FacebookId = myData["CoachFacebook"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.CoachId = (myData["CoachId"] == DBNull.Value) ? 0 : (int)myData["CoachId"];
                        v.CoachActiveStatus = (myData["Active"] != DBNull.Value) && (bool)myData["Active"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        int head = (int)myData["HeadCoach"];
                        v.HeadCoach = (head == 1);
                        v.Description = (myData["description"] == DBNull.Value) ? string.Empty : myData["description"].ToString();
                        v.PreferredStrokes = (myData["PreferredStrokes"] == DBNull.Value) ? string.Empty : myData["PreferredStrokes"].ToString();
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.Payment_Paid = (myData["Payment_Paid"] != DBNull.Value) && (bool)myData["Payment_Paid"];
                        v.FirstName = (myData["FirstName"] == DBNull.Value) ? string.Empty : (string)myData["FirstName"];
                        v.LastName = (myData["LastName"] == DBNull.Value) ? string.Empty : (string)myData["LastName"];
                        v.PlanId = (myData["PlanId"] == DBNull.Value) ? 0 : (int)myData["PlanId"];
                        v.PlanName = (myData["PlanName"] == DBNull.Value) ? string.Empty : (string)myData["PlanName"];
                        v.SubscriptionType = (myData["SubscriptionType"] == DBNull.Value) ? string.Empty : (string)myData["SubscriptionType"];
                        v.Price = (myData["Price"] == DBNull.Value) ? string.Empty : (string)myData["Price"];
                        v.StripeUserId = (myData["StripeUserID"] == DBNull.Value) ? string.Empty : (string)myData["StripeUserID"];
                        v.CardToken = (myData["CardToken"] == DBNull.Value) ? string.Empty : (string)myData["CardToken"];
                        v.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                        //  v.Coach360VideosCnt = (myData["Coach360VideosCnt"] == DBNull.Value) ? 0 : (int)myData["Coach360VideosCnt"];
                        v.VirtualVideoURL = (myData["VirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualVideoURL"]);
                        v.VirtualThumbnailURL = (myData["VirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["VirtualThumbnailURL"]);
                        v.MatchesVirtualVideoURL = (myData["MatchesVirtualVideoURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualVideoURL"]);
                        v.MatchesVirtualThumbnailURL = (myData["MatchesVirtualThumbnailURL"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["MatchesVirtualThumbnailURL"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetCoachProfileByEmail", "", ex.Message, "Exception", 0);
            }

            return v;
        }


        internal static int GetHeadCoachForInstitute(int instituteId)
        {

            int headCoachId = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetHeadCoachForInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter instituteIdPar = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteIdPar.Direction = ParameterDirection.Input;
                    instituteIdPar.Value = instituteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        headCoachId = (int)myData["HeadCoachId"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetHeadCoachForInstitute", "", ex.Message, "Exception", 0);
            }

            return headCoachId;
        }



        internal static List<CoachDTO> GetRecommendInstitutesForAthletes(int athleteId, int clubcoachid)
        {
            List<CoachDTO> coaches = new List<CoachDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetRecommendInstitutesForAthletes]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter athleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlParameter clubCoachIdPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubCoachIdPar.Direction = ParameterDirection.Input;
                    clubCoachIdPar.Value = clubcoachid;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachDTO v = new CoachDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AreasInterested = (string)myData["AreasInterested"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Emailaddress = (string)myData["Emailaddress"].ToString();
                        v.Description = (string)myData["Description"];
                        v.YearOfEstablish = (myData["YearOfEstablish"] == DBNull.Value) ? 0 : (int)myData["YearOfEstablish"];
                        v.NCAA = (myData["NCAA"] == DBNull.Value) ? 0 : (int)myData["NCAA"];
                        v.Conference = (myData["Conference"] == DBNull.Value) ? 0 : (int)myData["Conference"];
                        v.AdmissionStandard = (myData["AdmissionStandard"] == DBNull.Value) ? 0 : (int)myData["AdmissionStandard"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.ClassificationName = (string)myData["ClassificationName"].ToString();
                        v.State = (string)myData["State"].ToString();
                        v.City = (string)myData["City"].ToString();
                        v.Zip = (string)myData["Zip"].ToString();
                        v.WebsiteURL = (string)myData["WebsiteURL"].ToString();
                        v.FaidURL = (string)myData["FaidURL"].ToString();
                        v.ApplURL = (string)myData["ApplURL"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.EnrollmentNo = (string)myData["EnrollmentNo"].ToString();
                        v.ApplicationNo = (string)myData["ApplicationNo"].ToString();
                        v.AdmissionNo = (string)myData["AdmissionNo"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.CoverPicURL = (string)myData["CoverPicURL"].ToString();
                        v.NoOfAthletesInApp = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.AcceptanceRate = (myData["AcceptanceRate"] == DBNull.Value) ? 0 : (int)myData["AcceptanceRate"];
                        v.NCAARate = (myData["NCAARate"] == DBNull.Value) ? 0 : (int)myData["NCAARate"];
                        v.ConferenceRate = (myData["ConferenceRate"] == DBNull.Value) ? 0 : (int)myData["ConferenceRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AdmissionRate = (myData["AdmissionRate"] == DBNull.Value) ? 0 : (int)myData["AdmissionRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Address = v.Address + "\n" + v.City + " , " + v.State + " " + v.Zip;
                        v.Cost = (myData["Cost"] == DBNull.Value) ? 0 : (int)myData["Cost"];
                        v.MensTeam = (myData["MensTeam"] == DBNull.Value) ? 0 : (int)myData["MensTeam"];
                        v.WomenTeam = (myData["WomenTeam"] == DBNull.Value) ? 0 : (int)myData["WomenTeam"];
                        v.Size = (myData["Size"] == DBNull.Value) ? 0 : (int)myData["Size"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        coaches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetRecommendInstitutesForAthletes", "", ex.Message, "Exception", 0);
            }

            return coaches;
        }


        internal static List<AthleteDTO> GetAthletesinClubCoachSchool(int clubcoachid)
        {

            List<AthleteDTO> athletes = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthletesinClubCoachSchool]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter clubcoachidPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    clubcoachidPar.Direction = ParameterDirection.Input;
                    clubcoachidPar.Value = clubcoachid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicLevel"]);
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["YearLevel"]);
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteRate"]);
                        v.Class = GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Premium"]);
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.School = (myData["SchoolName"] == DBNull.Value) ? string.Empty : (string)myData["SchoolName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.ClubId = (myData["ClubId"] == DBNull.Value) ? 0 : (int)myData["ClubId"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.AthleteShareVideosCount = (myData["AthleteShareCount"] == DBNull.Value) ? 0 : (int)myData["AthleteShareCount"];
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetAthletesinClubCoachClub", "", ex.Message, "Exception", 0);
            }


            return athletes;
        }


        internal static int CheckCoachExistsorPassword(string emailaddress, string password)
        {

            int Id = 0;

            try
            {
                SqlConnection myConn = ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[CheckCoachExistsorPassword]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter passwordPar = cmd.Parameters.Add("@password", SqlDbType.VarChar, 100);
                    passwordPar.Direction = ParameterDirection.Input;
                    passwordPar.Value = password;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        Id = (int)myData["Id"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/CheckCoachExistsorPassword", "", ex.Message, "Exception", 0);
                throw;
            }

            return Id;
        }

    }
}