﻿using Lemonaid_web_api.Models;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Lemonaid_web_api.Controllers
{
    public class Payment2Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddPayment2_v1(PaymentDTO paymentDto)
        {
            try
            {
                if (paymentDto.UserType == 1)
                {
                    foreach (var i in paymentDto.Sports)
                    {
                        int id = Dal2.AddPaymentForAthlete(paymentDto, i.Id);
                        if (id > 0)
                        {
                            _result.Response = id;
                            _result.Status = Utility.CustomResponseStatus.Successful;
                            _result.Message = CustomConstants.Payment_Added_Successfully;
                        }
                        else
                        {
                            _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            _result.Message = CustomConstants.Record_Not_Added;
                        }
                    }
                }
                else
                {
                    int id = Dal2.AddPayment(paymentDto);
                    if (id > 0)
                    {
                        _result.Response = id;
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Message = CustomConstants.Payment_Added_Successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.Record_Not_Added;
                    }
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Payment2/AddPayment2_v1", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;
        }


       

        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;

        }
    }
}
