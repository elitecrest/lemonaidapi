﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Lemonaid_web_api.Controllers
{
    public class PushNotificationsController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse GetMessageWaitingNotifications()
        {

            try
            {
                List<UsertypeChatDTO> userChats = new List<UsertypeChatDTO>();
                var UserAdChatCountAthlete = DAL_Notif.GetUsersChatAdChatCount(Utility.UserTypes.HighSchooAthlete);
                userChats = AddChatlist(userChats, UserAdChatCountAthlete);
                var UserAdChatCountCoach = DAL_Notif.GetUsersChatAdChatCount(Utility.UserTypes.CollegeCoach);
                userChats = AddChatlist(userChats, UserAdChatCountCoach);
                var UserAdChatCountClubCoach = DAL_Notif.GetUsersChatAdChatCount(Utility.UserTypes.ClubCoach);
                userChats = AddChatlist(userChats, UserAdChatCountClubCoach);
                var UserAdChatCountFamilyFriends = DAL_Notif.GetUsersChatAdChatCount(Utility.UserTypes.FamilyFriends);
                userChats = AddChatlist(userChats, UserAdChatCountFamilyFriends);

                List<UsertypeChatDTO> AtheleteandCoachChats = new List<UsertypeChatDTO>();
                var AtheleteChatCount = DAL_Notif.GetUsersChatCount(Utility.UserTypes.HighSchooAthlete);
                AtheleteandCoachChats = AddChatlist(AtheleteandCoachChats, AtheleteChatCount);
                var CoachChatCount = DAL_Notif.GetUsersChatCount(Utility.UserTypes.CollegeCoach);
                AtheleteandCoachChats = AddChatlist(AtheleteandCoachChats, CoachChatCount);
                userChats = AddChatlist(userChats, AtheleteandCoachChats);

                foreach (var i in userChats)
                {
                    //PushNotification
                }

                _result.Status = Utility.CustomResponseStatus.Successful;
                
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("PushNotifications/GetMessageWaitingNotifications", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result; 
            }

        }

        private List<UsertypeChatDTO> AddChatlist(List<UsertypeChatDTO> userChats, List<UsertypeChatDTO> ChatsDto)
        {
            foreach (var a in ChatsDto)
            {
                userChats.Add(a);
            }
            return userChats;
        }


        [HttpGet]
        public Utility.CustomResponse GetNoSwipesandRecommendationsNotifications()
        {

            try
            {
                List<UsertypeChatDTO> userChats = new List<UsertypeChatDTO>();
                var UserAtheleteToInstitute = DAL_Notif.GetNoSwipesForAtheleteToInstitute();
                userChats = AddChatlist(userChats, UserAtheleteToInstitute);

                var UserInstituteToAthelete = DAL_Notif.GetNoSwipesForInstituteToAthelete();
                userChats = AddChatlist(userChats, UserInstituteToAthelete);

                var UserFFFToAthelete = DAL_Notif.GetNoSwipesForFFFToAthelete();
                userChats = AddChatlist(userChats, UserFFFToAthelete);

                var UserFFFToInstitute = DAL_Notif.GetNoSwipesForFFFToInstitute();
                userChats = AddChatlist(userChats, UserFFFToInstitute); 

                var UserClubCoachToInstitute = DAL_Notif.GetNoSwipesForClubCoachToInstitute();
                userChats = AddChatlist(userChats, UserClubCoachToInstitute);

                var UserRecomendsForCLCToInstitute = DAL_Notif.GetNoRecomendsForCLCToInstitute();
                userChats = AddChatlist(userChats, UserRecomendsForCLCToInstitute);

                var UserRecomendsForFFFToInstitute = DAL_Notif.GetNoRecomendsForFFFToInstitute();
                userChats = AddChatlist(userChats, UserRecomendsForFFFToInstitute);  

                foreach (var i in userChats)
                {
                    //PushNotification
                }

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = userChats;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("PushNotifications/GetNoSwipesandRecommendationsNotifications", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

    }
}
