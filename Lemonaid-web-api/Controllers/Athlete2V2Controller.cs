﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;

namespace Lemonaid_web_api.Controllers
{
    public class Athlete2V2Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        public string WrapperApi = ConfigurationManager.AppSettings["WrapperBaseURL"];

        [HttpPost]
        public Utility.CustomResponse AddAthlete(AthleteDTO athleteDto)
        {
            Athlete2Controller ath2Ctlr = new Athlete2Controller();
            try
            {
                if (athleteDto.Id == 0)
                {
                    PaymentController paymentCtrl = new PaymentController();

                    List<SportsDTO> sports = ath2Ctlr.AddAthleteToWrapperDB(athleteDto);

                    athleteDto = CreateUpdateAthlete(athleteDto);

                    var item = sports.Single(x => x.Id == athleteDto.SportId);
                    sports.Remove(item);

                    PaymentDTO payment = new PaymentDTO();

                    if (sports.Count > 0)
                    {
                        foreach (var i in sports)
                        {
                            payment = Dal2.GetPaymentDetailsForAthlete(athleteDto.Emailaddress, i.Id);
                            if (payment.UserID > 0)
                                break;
                        }
                        if (payment.UserID > 0)
                        {
                            //Add it in Present DB 
                            var apy = Dal2.AddPaymentForAthlete(payment, athleteDto.SportId);
                        }

                    }
                }
                else
                {
                    athleteDto = CreateUpdateAthlete(athleteDto);

                }
                if (athleteDto.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteDto;
                    _result.Message = CustomConstants.Athlete_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2/AddAthlete", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        public AthleteDTO CreateUpdateAthlete(AthleteDTO athleteDto)
        {
            AthleteController atc = new AthleteController();
            if (athleteDto.AtheleteType == 0)
            {
                athleteDto.AtheleteType = Convert.ToInt32(Utility.AtheleteType.HighSchoolAthelete);
                athleteDto.Active = false;
            }

            var athelete = Dal2_v2.AddAthlete2_v2(athleteDto);
            athelete.SportId = athleteDto.SportId;
            athelete.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athelete.Id);
            athelete.AtheleteVideos = Dal2.GetAtheleteIdealVideos2_v1();
            string email = string.Empty;
            if (athleteDto.LoginType == 2 || athleteDto.LoginType == 3)
            {
                email = athleteDto.UserName;
            }
            else
            {
                email = athleteDto.Emailaddress;
            }
            List<VideoDTO> uploadedVideos = Dal2.GetAtheleteVideos2_v1(email, athleteDto.LoginType );
            if (uploadedVideos.Count > 0)
            {
                foreach (var av in athelete.AtheleteVideos)
                {
                    foreach (var uv in uploadedVideos)
                    {
                        if (av.VideoNumber == uv.VideoNumber)
                        {
                            av.UploadVideoId = uv.Id;
                            av.UploadDuration = uv.UploadDuration;
                            av.UploadThumbnailUrl = uv.UploadThumbnailUrl;
                            av.VideoURL = uv.VideoURL;
                        }
                    }
                }
            }

            int measurementcount = atc.GetMeasurementCount(athelete.Height, athelete.Height2, athelete.Weight,
                athelete.WingSpan, athelete.ShoeSize);
            athelete.MeasurementCount = measurementcount;

            return athelete;
        }
    }
}
