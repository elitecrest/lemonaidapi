﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using SendGrid;
using System.Net; 
using System.Data; 
using System.Data.SqlClient;


namespace Lemonaid.Worker
{
    public class SendGridSMTPMail
    {
        public static int SendEmail(SendGridMailModel sendGridModel)
        {
            try
            {
                SupportMailDTO support = new SupportMailDTO();
                support = GetSupportMails("SendGridMail",sendGridModel.Connection);

                sendGridModel.APIKey = support.Host;
                var myMessage = new SendGridMessage();
                myMessage.From = new MailAddress("info@lemonaidrecruiting.com", sendGridModel.fromName);
                myMessage.AddTo(sendGridModel.toMail);
                myMessage.Subject = sendGridModel.subject;
                myMessage.Html = "<p>" + sendGridModel.Message + "</p>";


                var credentials = new NetworkCredential(support.EmailAddress, support.Password);
                var transportWeb = new Web(credentials);
                transportWeb.DeliverAsync(myMessage);
                return 1;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        internal static SupportMailDTO GetSupportMails(string name,string Conn)
        {

            SupportMailDTO s = new SupportMailDTO();
            try
            {
                SqlConnection myConn = new SqlConnection(Conn);
                myConn.Open();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSupportMails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 500);
                    namePar.Direction = ParameterDirection.Input;
                    namePar.Value = name;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        s.EmailAddress = (string)myData["EmailAddress"];
                        s.Password = (string)myData["Password"];
                        s.Host = (string)myData["Host"];
                        s.Port = (string)myData["Port"];
                        s.SSL = (string)myData["SSL"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/GetSupportMails", "", ex.Message, "Exception", 0);
                throw;
            }

            return s;
        }
    }

    

    public partial class SupportMailDTO
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string SSL { get; set; }

    }


    public class SendGridMailModel {
        public List<string> toMail { set; get; }
        public string fromName { set; get; }
        public string fromMail { set; get; }
        public string subject { set; get; }
        public string Message { set; get; }
        public string APIKey { set; get; }
        public string email { get; set; }
        public string password { get; set; }
        public string Connection { get; set; }
    }
}