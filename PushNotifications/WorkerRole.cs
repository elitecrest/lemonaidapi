using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;

namespace PushNotifications
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("Lemonaid.Worker is running");

            try
            {
                //This is published to Lemonaid Wrestling Staging
                //Check For process run today or not 
                var ConnStrings = GetProductionConnection();
                foreach (var conn in ConnStrings)
                {
                    PushNotifications.GetMessageWaitingNotifications(conn);
                    PushNotifications.GetNoSwipesandRecommendationsNotifications(conn);
                    PushNotifications.GetAcademicProfilesNotCompleted(conn);
                    PushNotifications.GetAthleteProfilesNotCompleted(conn);
                    PushNotifications.GetAboutmeProfilesNotCompleted(conn);
                     
                }
                Thread.Sleep(TimeSpan.FromHours(12));
                //Thread.Sleep(TimeSpan.FromHours(1));
                Trace.TraceInformation("Working", "Information");

            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("WorkerMails/Run", "", ex.Message, "Exception", 0);
                throw;
            }

            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        internal static List<string> GetStagingConnection()
        {
            List<string> connStrings = new List<string>();
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server =tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidRowingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidDivingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTennisStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTrackStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidLacrosseStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidGolfStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWaterPoloStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidFootballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBasketballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBaseballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoccerStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoftBallStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrestlingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidIceHockeyStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidVolleyBallStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            return connStrings;
        }


        internal static List<string> GetProductionConnection()
        {
            List<string> connStrings = new List<string>();
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBiz;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server =tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidRowing;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidDiving;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTennis;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTrack;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidLacrosse;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidGolf;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWaterPolo;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidFootball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBasketball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBaseball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoccer;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoftBall;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrestling;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidIceHockey;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            connStrings.Add("Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidVolleyBall;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;");
            return connStrings;
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("PushNotifications has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("PushNotifications is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("PushNotifications has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }
    }
}
