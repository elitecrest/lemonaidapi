﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace Lemonaid_web_api.Models
{
    public class Twitter
    {
        public string OAuthConsumerSecret { get; set; }
        public string OAuthConsumerKey { get; set; }


        public async Task<string> GetAccessToken()
        {
            var oauth_consumer_key = OAuthConsumerKey;
            var oauth_consumer_secret = OAuthConsumerSecret;
            //Token URL
            var oauth_url = "https://api.twitter.com/oauth2/token";
            var headerFormat = "Basic {0}";
            var authHeader = string.Format(headerFormat,
            Convert.ToBase64String(Encoding.UTF8.GetBytes(Uri.EscapeDataString(oauth_consumer_key) + ":" +
            Uri.EscapeDataString((oauth_consumer_secret)))
            ));

            var postBody = "grant_type=client_credentials";

            ServicePointManager.Expect100Continue = false;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(oauth_url);
            request.Headers.Add("Authorization", authHeader);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";

            using (Stream stream = request.GetRequestStream())
            {
                byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                stream.Write(content, 0, content.Length);
            }

            request.Headers.Add("Accept-Encoding", "gzip");
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            Stream responseStream = new GZipStream(response.GetResponseStream(), CompressionMode.Decompress);
            using (var reader = new StreamReader(responseStream))
            {

                JavaScriptSerializer js = new JavaScriptSerializer();
                var objText = reader.ReadToEnd();
                //   JObject o = JObject.Parse(objText);
                dynamic item = js.Deserialize<object>(objText);
                return item["access_token"];
            }
        }


        public async Task<IEnumerable<string>> GetTwitts(string userName, int count, string accessToken = null)
        {
            if (accessToken == null)
            {
                accessToken = await GetAccessToken();

            }
            var requestUserTimeline = new HttpRequestMessage(HttpMethod.Get, string.Format("https://api.twitter.com/1.1/statuses/user_timeline.json?count={0}&screen_name={1}&trim_user=1&exclude_replies=1", count, userName));
            requestUserTimeline.Headers.Add("Authorization", "Bearer " + accessToken);
            var httpClient1 = new HttpClient();
            HttpResponseMessage responseUserTimeLine = await httpClient1.SendAsync(requestUserTimeline).ConfigureAwait(false);
            var serializer1 = new JavaScriptSerializer();
            dynamic json1 = serializer1.Deserialize<object>(await responseUserTimeLine.Content.ReadAsStringAsync());
            var enumerableTwitts = (json1 as IEnumerable<dynamic>);

            if (enumerableTwitts == null)
            {
                return null;
            }
            var tweets = enumerableTwitts.Select(t => (string)(t["text"].ToString()));
            string pathFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bio.txt");
            if (!File.Exists(pathFile))
            {
                File.Create(pathFile);
            }
            //var file = pathFile;
            File.WriteAllLines(pathFile, tweets);

            return tweets;


        }
    }
}
 