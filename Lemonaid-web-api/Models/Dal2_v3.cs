﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Collections.Generic;

namespace Lemonaid_web_api.Models
{
    public class Dal2_v3
    {
        public string CleverTapAccountId = ConfigurationManager.AppSettings["CleverTapAccountId"];
        public string CleverTapPassCode = ConfigurationManager.AppSettings["CleverTapPassCode"];

        internal bool ClevertapPushNotification(string title,string pushMessage, string Name)
        {
     
          bool isPushMessageSend = false;
            try
            {
                string postString = "";
                string urlpath = "https://api.clevertap.com/1/send/push.json";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(urlpath);
                postString = "{ \"to\": {\"Identity\":\"[ \"" + Name + "\"  ], }," +
                                 "\"respect_frequency_caps\" :  false ," +
                                 "\"content\" : {\"title\":\"" + title + "\", \"body\":\"" + pushMessage + "\"}" +
                                 "}";


                httpWebRequest.ContentType = "application/json";
                httpWebRequest.ContentLength = postString.Length;
                httpWebRequest.Headers.Add("X-CleverTap-Account-Id", CleverTapAccountId);
                httpWebRequest.Headers.Add("X-CleverTap-Passcode", CleverTapPassCode);
                httpWebRequest.Method = "POST";
                StreamWriter requestWriter = new StreamWriter(httpWebRequest.GetRequestStream());
                requestWriter.Write(postString);
                requestWriter.Close();
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var responseText = streamReader.ReadToEnd();
                    JObject jObjRes = JObject.Parse(responseText);
                    if (Convert.ToString(jObjRes).IndexOf("true") != -1)
                    {
                        isPushMessageSend = true;
                    }
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v3/ClevertapPushNotification", "", ex.Message, "Exception", 0);
                throw;
            }
            return isPushMessageSend;
        }



        internal static int GetAthleteInstituteMatchStatus(int AthleteId, int InstituteId)
        {
            int status = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteInstituteMatchStatus]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AthleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteIdPar.Direction = ParameterDirection.Input;
                    AthleteIdPar.Value = AthleteId;

                    SqlParameter InstituteIdPar = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteIdPar.Direction = ParameterDirection.Input;
                    InstituteIdPar.Value = InstituteId;


                      status = Convert.ToInt32(cmd.ExecuteScalar()); 
                  


                 
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v3/GetAthleteInstituteMatchStatus", "", ex.Message, "Exception", 0);
            }


            return status;
        }

        internal static int PostAds(AdDTO adDTO)
        {
            int adId = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[PostAds]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    Name.Direction = ParameterDirection.Input;
                    Name.Value = adDTO.Name;

                    SqlParameter ProfilePicURL  = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.NVarChar, 1000);
                    ProfilePicURL.Direction = ParameterDirection.Input;
                    ProfilePicURL.Value = adDTO.ProfilePicURL;

                    SqlParameter VideoURL = cmd.Parameters.Add("@VideoURL", SqlDbType.NVarChar, 1000);
                    VideoURL.Direction = ParameterDirection.Input;
                    VideoURL.Value = adDTO.VideoURL;

                    SqlParameter Description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 4000);
                    Description.Direction = ParameterDirection.Input;
                    Description.Value = (adDTO.Description == null) ? string.Empty : adDTO.Description;

                    SqlParameter AdTypePar = cmd.Parameters.Add("@AdType", SqlDbType.Int);
                    AdTypePar.Direction = ParameterDirection.Input;
                    AdTypePar.Value = adDTO.AdType;
                     
                    SqlParameter IdPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    IdPar.Direction = ParameterDirection.Input;
                    IdPar.Value = adDTO.Id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        adId = (int)myData["Id"];
                    
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v3/PostAds", "", ex.Message, "Exception", 0);
                throw;
            }

            return adId;
        }


        internal static CoachProfileDTO UpdateCoachProfile(CoachProfileDTO coachProfileDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachProfile2_v3]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachid = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachid.Direction = ParameterDirection.Input;
                    coachid.Value = coachProfileDto.CoachId;

                    SqlParameter description = cmd.Parameters.Add("@description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = coachProfileDto.Description;

                    SqlParameter facebookUrl = cmd.Parameters.Add("@FacebookURL", SqlDbType.VarChar, 2000);
                    facebookUrl.Direction = ParameterDirection.Input;
                    facebookUrl.Value = coachProfileDto.FacebookURL;

                    SqlParameter twitterUrl = cmd.Parameters.Add("@TwitterURL", SqlDbType.VarChar, 2000);
                    twitterUrl.Direction = ParameterDirection.Input;
                    twitterUrl.Value = coachProfileDto.TwitterURL;

                    SqlParameter instagramUrl = cmd.Parameters.Add("@InstagramURL", SqlDbType.VarChar, 2000);
                    instagramUrl.Direction = ParameterDirection.Input;
                    instagramUrl.Value = coachProfileDto.InstagramURL;

                    SqlParameter youtubeUrl = cmd.Parameters.Add("@YoutubeURL", SqlDbType.VarChar, 2000);
                    youtubeUrl.Direction = ParameterDirection.Input;
                    youtubeUrl.Value = coachProfileDto.YoutubeURL;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = coachProfileDto.CoachName ?? string.Empty;

                    SqlParameter title = cmd.Parameters.Add("@Title", SqlDbType.VarChar, 10);
                    title.Direction = ParameterDirection.Input;
                    title.Value = coachProfileDto.Title ?? string.Empty;

                    SqlParameter phone1 = cmd.Parameters.Add("@Phone1", SqlDbType.VarChar, 20);
                    phone1.Direction = ParameterDirection.Input;
                    phone1.Value = coachProfileDto.Phone1 ?? string.Empty;

                    SqlParameter phone2 = cmd.Parameters.Add("@Phone2", SqlDbType.VarChar, 20);
                    phone2.Direction = ParameterDirection.Input;
                    phone2.Value = coachProfileDto.Phone2 ?? string.Empty;

                    SqlParameter accolades = cmd.Parameters.Add("@Accolades", SqlDbType.VarChar, 4000);
                    accolades.Direction = ParameterDirection.Input;
                    accolades.Value = coachProfileDto.Accolades ?? string.Empty;

                    SqlParameter collegeName = cmd.Parameters.Add("@CollegeName", SqlDbType.VarChar, 200);
                    collegeName.Direction = ParameterDirection.Input;
                    collegeName.Value = coachProfileDto.CollegeName ?? string.Empty;

                    SqlParameter year = cmd.Parameters.Add("@Year", SqlDbType.Int);
                    year.Direction = ParameterDirection.Input;
                    year.Value = coachProfileDto.Year;

                    SqlParameter InstituteName = cmd.Parameters.Add("@InstituteName", SqlDbType.VarChar, 100);
                    InstituteName.Direction = ParameterDirection.Input;
                    InstituteName.Value = coachProfileDto.InstituteName ?? string.Empty;

                    SqlParameter InstituteState = cmd.Parameters.Add("@InstituteState", SqlDbType.VarChar, 100);
                    InstituteState.Direction = ParameterDirection.Input;
                    InstituteState.Value = coachProfileDto.InstituteState ?? string.Empty;

                    SqlParameter InstituteCity = cmd.Parameters.Add("@InstituteCity", SqlDbType.VarChar, 100);
                    InstituteCity.Direction = ParameterDirection.Input;
                    InstituteCity.Value = coachProfileDto.InstituteCity ?? string.Empty;

                    SqlParameter profilePicUrl = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    profilePicUrl.Direction = ParameterDirection.Input;
                    profilePicUrl.Value = coachProfileDto.ProfilePicURL;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v1/UpdateCoachProfile", "", ex.Message, "Exception", 0);
            }


            return coachProfileDto;
        }



        

        internal static int AddCoachAthleteChatCount(int AthleteId, int CoachId, int UnreadCount,int senderType,int receiverType)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoachAthleteChatCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AthleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteIdPar.Direction = ParameterDirection.Input;
                    AthleteIdPar.Value = AthleteId;

                    SqlParameter CoachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    CoachIdPar.Direction = ParameterDirection.Input;
                    CoachIdPar.Value = CoachId;

                    SqlParameter UnreadCountPar = cmd.Parameters.Add("@UnreadMsgCount", SqlDbType.Int);
                    UnreadCountPar.Direction = ParameterDirection.Input;
                    UnreadCountPar.Value = UnreadCount; 

                    SqlParameter senderTypePar = cmd.Parameters.Add("@sendertype", SqlDbType.Int);
                    senderTypePar.Direction = ParameterDirection.Input;
                    senderTypePar.Value = senderType;


                    SqlParameter receivertypePar = cmd.Parameters.Add("@receivertype", SqlDbType.Int);
                    receivertypePar.Direction = ParameterDirection.Input;
                    receivertypePar.Value = receiverType;


                    id = Convert.ToInt32(cmd.ExecuteScalar()); 


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v3/AddCoachAthleteChatCount", "", ex.Message, "Exception", 0);
            }


            return id;
        }


        internal static List<CoachUnreadCountDTO> GetCoachUnreadCount()
        {

            List<CoachUnreadCountDTO> lstReadCount = new List<CoachUnreadCountDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachUnreadCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure; 


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CoachUnreadCountDTO v = new CoachUnreadCountDTO();
                        if ((int)myData["cntUnreadCount"] > 0)
                        {
                            v.CoachId = (int)myData["Id"];
                            v.CoachEmail = (string)myData["emailaddress"].ToString();
                            v.InstituteId = (int)myData["InstituteId"];
                            v.InstituteName = (string)myData["name"].ToString();
                            v.Count = (int)myData["cntUnreadCount"];
                            v.DeviceId = (string)myData["DeviceId"].ToString();
                            lstReadCount.Add(v);
                        }
                       
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v3/GetCoachUnreadCount", "", ex.Message, "Exception", 0);
            }

            return lstReadCount;
        }


        internal static int GetChatMessagesUnreadCount(int senderId,int receiverId)
        {

            int Count = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetChatMessagesUnreadCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SenderIdPar = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderIdPar.Direction = ParameterDirection.Input;
                    SenderIdPar.Value = senderId;

                    SqlParameter ReceiverIdPar = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverIdPar.Direction = ParameterDirection.Input;
                    ReceiverIdPar.Value = receiverId; 

                    Count = Convert.ToInt16(cmd.ExecuteScalar()); 
                  
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2_v3/GetChatMessagesUnreadCount", "", ex.Message, "Exception", 0);
            }

            return Count;
        }

    }
}