﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Lemonaid_web_api.Models
{
    public class Badges
    {
        internal static bool GetAnerobicBadge(string verticalJump,string broadJump)
        {

            bool badge = false;

            try
            {
                if (!string.IsNullOrEmpty(verticalJump) || !string.IsNullOrEmpty(broadJump))
                    badge = true;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Badges/GetAnerobicBadge", "", ex.Message, "Exception", 0);
                throw;
            }

            return badge;
        }

        internal static bool GetAerobicBadge(string VO2Max)
        {

            bool badge = false;

            try
            {
                if (!string.IsNullOrEmpty(VO2Max))
                    badge = true;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Badges/GetAerobicBadge", "", ex.Message, "Exception", 0);
                throw;
            }

            return badge;
        }

        internal static bool GetStrengthBadge(string KneelingMedBallToss, string RotationalMedBallToss)
        {

            bool badge = false;

            try
            {
                if (!string.IsNullOrEmpty(KneelingMedBallToss) || !string.IsNullOrEmpty(RotationalMedBallToss))
                    badge = true;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Badges/GetStrengthBadge", "", ex.Message, "Exception", 0);
                throw;
            }

            return badge;
        }

        internal static bool GetAgilityBadge(string _20YardShuttleRun, string _60YardShuttleRun)
        {

            bool badge = false;

            try
            {
                if (!string.IsNullOrEmpty(_20YardShuttleRun) || !string.IsNullOrEmpty(_60YardShuttleRun))
                    badge = true;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Badges/GetAgilityBadge", "", ex.Message, "Exception", 0);
                throw;
            }

            return badge;
        }

        internal static bool GetDiamondBadge(bool anaerobic, bool aerobic, bool strength, bool agility,int SportId)
        {

            bool badge = false;

            try
            {
                if (SportId == 5)
                {
                    if (anaerobic && aerobic && strength && agility)
                        badge = true;
                    else
                        badge = false;
                }
                else
                {
                    if (anaerobic && strength && agility)
                        badge = true;
                    else
                        badge = false;
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Badges/GetDiamondBadge", "", ex.Message, "Exception", 0);
                throw;
            }

            return badge;
        }
    }
}