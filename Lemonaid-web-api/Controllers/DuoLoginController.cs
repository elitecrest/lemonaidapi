﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Http;



namespace Lemonaid_web_api.Controllers
{
    public class DuoLoginController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse Enroll(CoachUserDTO coachUser)
        {
            try
            {

                coachUser = UsersDal.AddCoachUser(coachUser);
                CoachDTO coachInfo = UsersDal.GetCoachExists(coachUser.Emailaddress, coachUser.Password);
                if (coachInfo.Id > 0)
                { 
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = coachInfo; 
                _result.Message = CustomConstants.Enrolled_successfully;
               
                 SendMail(coachUser.Emailaddress,coachUser.ActivationCode,coachUser.CoachId.ToString());
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.User_Does_Not_Exist;
                
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DuoLogin/Enroll", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse Login(string emailaddress, string password)
        {
            try
            {
                int id = UsersDal.CheckCoachExistsorPassword(emailaddress, UsersDalV4.Encrypt(password));
                if (id == -1)
                {
                    _result.Status = Utility.CustomResponseStatus.CoachNewUser;
                    _result.Message = CustomConstants.Coach_NewUser;
                }
                else if (id == -2)
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Go_To_Login;
                }
                else
                {
                    CoachDTO coachInfo = UsersDal.GetCoachExists(emailaddress, UsersDalV4.Encrypt(password));
                    if (coachInfo.Id > 0)
                    {
                        _result.Response = coachInfo;
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Message = CustomConstants.Login_successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = coachInfo;
                        _result.Message = CustomConstants.User_Does_Not_Exist;
                    }
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DuoLogin/Login", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse UpdateCoachActiveStatus(string code,int coachId)
        {
            try
            {

                int coachIdRes = UsersDal.UpdateCoachActiveStatus(code,coachId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = coachIdRes;
                _result.Message = CustomConstants.Verification_Success;
           

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DuoLogin/UpdateCoachActiveStatus", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
         


        public static T JsonDeserialize<T>(string jsonString)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        
        public static int SendMail(string email, string activationcode,string coachId)
        {
            SupportMailDTO support = new SupportMailDTO();
            MailMessage msg = new MailMessage();
            try
            {
              
                support = UsersDal.GetSupportMails("SupportEmail");
                string baseURL = ConfigurationManager.AppSettings["BaseURL"];

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/VerifyEmail.html"));
                var readFile = reader.ReadToEnd(); 
                string myString = readFile;
                myString = myString.Replace("$$Name$$", email);
                myString = myString.Replace("$$ActivationCode$$", activationcode);
                myString = myString.Replace("$$CoachId$$", coachId);
                myString = myString.Replace("$$BaseURL$$", baseURL);

                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(email);
                sendGridModel.fromMail = support.EmailAddress;
                sendGridModel.fromName = "Lemonaid";
                sendGridModel.subject = " Verification link to complete your enrollment";
                sendGridModel.Message = myString;
                SendGridSMTPMail.SendEmail(sendGridModel);


                //smtpMail.Send(msg);
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(support.EmailAddress, email, msg.Subject, "Mail", 1);
                return 0;
            } 
            catch (Exception )
            { 
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(support.EmailAddress, email, msg.Subject, "Mail", 0);
                return 1;
               
            }
        }
         
    }
}
