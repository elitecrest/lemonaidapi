﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Lemonaid_web_api.Models;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.WindowsAzure.MediaServices.Client;

namespace Lemonaid_web_api
{
    public partial class UploadedFiles_v4 : System.Web.UI.Page
    { 
        Utility.CustomResponse Result = new Utility.CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Result = Handlefile();
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string jsonString = javaScriptSerializer.Serialize(Result);
                Response.Write(jsonString);
            }
        }



        private Utility.CustomResponse Handlefile()
        {
            string baseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            int length = (int)Request.InputStream.Length;
            byte[] buffer = new byte[length];
            Stream stream = Request.InputStream;
            string VideoFormat = Request.QueryString["VideoFormat"].Trim();
            string VideoNumber = Request.QueryString["VideoNumber"].Trim().ToString();
            string Duration = Request.QueryString["Duration"].Trim().ToString();

            VideoDTO video = new VideoDTO();
            string videoURL = streamaudio(stream, VideoFormat);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


            video.Id = 0;
            video.VideoURL = videoURL;
            video.ThumbnailURL = string.Empty; 
            video.AtheleteId = Convert.ToInt32(Request.QueryString["AtheleteId"].Trim());  
            video.VideoFormat = VideoFormat;
            video.VideoNumber = Convert.ToInt32(VideoNumber);
            video.Duration = Convert.ToInt32(Duration);
            video.Status = 1;


            var response = client.PostAsJsonAsync("Athletev4/AddVideos_v4", video).Result;

            if (response.IsSuccessStatusCode)
            {
                Lemonaid_web_api.Utility.CustomResponse res = response.Content.ReadAsAsync<Lemonaid_web_api.Utility.CustomResponse>().Result;
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = res.Response;
                Result.Message = CustomConstants.Video_Added;

            }
            else
            {
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                Result.Message = CustomConstants.NoRecordsFound;
            }
            return Result;
        }



        public string streamaudio(Stream stream, string format)
        {

            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);

            var fileStream = System.IO.File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();

            var ext = format;

            Stream fileStreambanners = stream;

            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string videoURL = UploadImage1(fileNamebanners, path, ext, fileStreambanners);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);

            }
            return videoURL;
        }

        //UploadImageFromStream
        public string UploadImage(string fileName, string path, string ext, Stream InputStream, string ContentType)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) + r.Next(9999).ToString() + DateTime.Now.Millisecond;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();



                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                string uniqueBlobName = string.Format(fileName, Guid.NewGuid().ToString(), ext);
                CloudBlockBlob blob = container.GetBlockBlobReference(uniqueBlobName);
                blob.Properties.ContentType = ContentType;
                blob.UploadFromStream(InputStream);


                imageurl = blob.Uri.ToString();

            }
            catch (Exception ex)
            {

            }

            return imageurl;

        }


        public string UploadImage1(string fileName, string path, string ext, Stream InputStream)
        {

            string account = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountName"].ToString();
            string key = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountKey"].ToString();


            CloudMediaContext context = new CloudMediaContext(account, key);

            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);

            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));


            //var accessPolicy1 = context.AccessPolicies.Create(fileName, TimeSpan.FromDays(3), AccessPermissions.Write | AccessPermissions.List);

            //  var locator1 = context.Locators.CreateLocator(LocatorType.Sas, uploadAsset, accessPolicy1);
            //Convert.ToString(Path.GetFileNameWithoutExtension(uploadAsset.Name)).ToUpper()
            assetFile.Upload(path);
            //  locator1.Delete();
            // accessPolicy1.Delete();


            //var encodeAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            //// Preset reference documentation: http://msdn.microsoft.com/en-us/library/windowsazure/jj129582.aspx
            //var encodingPreset = "H264 Broadband 720p";
            //var assetToEncode = context.Assets.Where(a => a.Id == encodeAssetId).FirstOrDefault();
            //if (assetToEncode == null)
            //{
            //    throw new ArgumentException("Could not find assetId: " + encodeAssetId);
            //}

            //IJob job = context.Jobs.Create("Encoding " + assetToEncode.Name + " to " + encodingPreset);

            //IMediaProcessor latestWameMediaProcessor = (from p in context.MediaProcessors where p.Name == "Azure Media Encoder" select p).ToList().OrderBy(wame => new Version(wame.Version)).LastOrDefault();
            //ITask encodeTask = job.Tasks.AddNew("Encoding", latestWameMediaProcessor, encodingPreset, TaskOptions.None);
            //encodeTask.InputAssets.Add(assetToEncode);
            //encodeTask.OutputAssets.AddNew(assetToEncode.Name + " as " + encodingPreset, AssetCreationOptions.None);

            //job.StateChanged += new EventHandler<JobStateChangedEventArgs>((sender, jsc) => Console.WriteLine(string.Format("{0}\n  State: {1}\n  Time: {2}\n\n", ((IJob)sender).Name, jsc.CurrentState, DateTime.UtcNow.ToString(@"yyyy_M_d_hhmmss"))));
            //job.Submit();
            //job.GetExecutionProgressTask(CancellationToken.None).Wait();

            //var preparedAsset = job.OutputMediaAssets.FirstOrDefault();



            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 365;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
                                                     AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            //var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith("m3u8-aapl.ism")).FirstOrDefault();
            //var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().Equals(streamingAsset.Name.ToLower())).FirstOrDefault();
            //if (streamingAssetFile != null)
            //{
            //    var locator = context.Locators.CreateLocator(LocatorType.OnDemandOrigin, streamingAsset, accessPolicy);
            //    Uri hlsUri = new Uri(locator.Path + streamingAssetFile.Name + "/manifest(format=m3u8-aapl)");
            //    streamingUrl = hlsUri.ToString();
            //}
            //streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".ism")).FirstOrDefault();
            //if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            //{
            //    var locator = context.Locators.CreateLocator(LocatorType.OnDemandOrigin, streamingAsset, accessPolicy);
            //    Uri smoothUri = new Uri(locator.Path + streamingAssetFile.Name + "/manifest");
            //    streamingUrl = smoothUri.ToString();
            //}
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;

        }



        public string UploadImageFromBase64(string binary, string format)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999).ToString() + DateTime.Now.Millisecond + "." + format;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(binary);


                blockBlob.Properties.ContentType = "image/jpeg";
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                imageurl = blockBlob1.Uri.ToString();


            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";
            }
            return imageurl;

        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    Result = Handlefile();
        //}


    }

}