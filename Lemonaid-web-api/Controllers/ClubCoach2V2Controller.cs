﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;


namespace Lemonaid_web_api.Controllers
{
    public class ClubCoach2V2Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpPost]
        public Utility.CustomResponse AddClubCoach(ClubCoachDTO clubCoachDto)
        {
            ClubCoach2Controller Clcctrl = new ClubCoach2Controller();

            try
            {
                if (clubCoachDto.Id == 0)
                {
                    List<SportsDTO> sports = Clcctrl.AddClubCoachToWrapperDb(clubCoachDto);
                }
                var clubCoachDet = Dal2_v2.AddClubCoach2_v2(clubCoachDto);
                //if (clubCoachDet.ExistedId != 0)
                //{
                //    var id = DAL2V5.UpdateNewClubCoachId(clubCoachDet.Id, clubCoachDet.ExistedId);
                //    clubCoachDet.ExistedEmailAddress = (clubCoachDet.ExistedLoginType == 1) ? clubCoachDet.ExistedEmailAddress : clubCoachDet.ExistedUsername;
                //    DAL2V5.DeleteUserFromWrapper(clubCoachDet.ExistedEmailAddress, clubCoachDto.SportId, clubCoachDet.ExistedLoginType, Utility.UserTypes.ClubCoach);
                //}
                clubCoachDet.SportId = clubCoachDto.SportId;
                if (clubCoachDet.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = clubCoachDet;
                    _result.Message = CustomConstants.clubCoach_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach2v2/AddClubCoach", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse ClubCoachProfile(string emailAddress,int loginType)
        {
            try
            {
                ClubCoachDTO clubCoachDto = Dal2_v2.GetClubCoachProfile2_v2(emailAddress, loginType);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = clubCoachDto;
                _result.Message = CustomConstants.Details_Get_Successfully;
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach2V2/ClubCoachProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

    }
}
