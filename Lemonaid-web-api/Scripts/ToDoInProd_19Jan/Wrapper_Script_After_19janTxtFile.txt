 CREATE TABLE [dbo].[Device](
	[ID] [int] IDENTITY(0,1) NOT NULL,
	[DeviceID] [varchar](500) NOT NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[DeviceType] [varchar](50) NULL
)

GO




CREATE PROCEDURE [dbo].[GetChatAdCards_2V6]  --'puramamaheshbabu@gmail.com',1,14
(   
  @emailaddress varchar(100),
  @loginType int,
  @SportId int,
  @adType int,
  @UserType varchar(10)
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
     declare @UserId int
	 declare @UserSportId int 

	  If(@LoginType = 1)
	   select @UserId = Id from Users Where Emailaddress = @emailaddress and LoginType=@LoginType and
	    UserType = @UserType
	 Else
	   select @UserId = Id from Users Where Username = @emailaddress and LoginType=@LoginType and
	   UserType = @UserType

	   select  @UserSportId = Id from UserSports where UserId = @UserId and SportId = @SportId

	    

 select * from ChatAds 
 Where Adtype = @adType and id not in(
 select ChatAdId from AtheleteChatAdMatches 
 where UserSportId = @UserSportId and status = 1
 )

END







GO

 
 
 
CREATE PROCEDURE [dbo].[AddAthleteChatAdMatch_2V6]
	@AthleteEmail varchar(100),
	@LoginType int,
	@ChatAdId int,
	@Status bit, 
	@SportId int,
	@UserType varchar(10),
	@Id int Output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	 declare @cnt int	 
     declare @UserId int
	 declare @UserSportId int 

	 If(@LoginType = 1)
	   select @UserId = Id from Users Where Emailaddress = @AthleteEmail and LoginType=@LoginType and
	    UserType =@UserType
	 Else
	   select @UserId = Id from Users Where Username = @AthleteEmail and LoginType=@LoginType and
	   UserType = @UserType

	   select  @UserSportId = Id from UserSports where UserId = @UserId and SportId = @SportId
	
	-- select @cnt = count(*) from AtheleteChatAdMatches where UserSportId = @UserSportId and ChatAdId = @ChatAdId
	 select @cnt = count(*) from AtheleteChatAdMatches where UserId = @UserId and SportId = @SportId and ChatAdId = @ChatAdId
	 
	 if(@cnt = 0)
	 Begin
		insert into AtheleteChatAdMatches (UserSportId,ChatAdId,[Status],CreatedDate,UserId,SportId) 
		values (@UserSportId,@ChatAdId,@Status,GETUTCDATE(),@UserId,@SportId)
		
	 End
	 Else
	 Begin
	    Update AtheleteChatAdMatches set [Status] = @Status 
		where UserId = @UserId and SportId = @SportId and ChatAdId = @ChatAdId
	 End 
        select @cnt = count(*) from AtheleteChatAdMatches where UserId = @UserId and SportId = @SportId
		and status  = 1
		set @Id = @cnt


END
 

GO
 


CREATE PROCEDURE [dbo].[GetUserId_2V6]  
(   
  @AthleteEmail varchar(100),
  @loginType int,
  @UserType varchar(20) 
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 declare @UserId int
	 declare @UserSportId int 

    If(@LoginType = 1)
	   select @UserId = Id from Users Where Emailaddress = @AthleteEmail and LoginType=@LoginType and
	    UserType = @UserType
	 Else
	   select @UserId = Id from Users Where Username = @AthleteEmail and LoginType=@LoginType and
	   UserType = @UserType

	  -- select  @UserSportId = Id from UserSports where UserId = @UserId and SportId = @SportId

	   select @UserId as Id

END
 

GO



CREATE PROCEDURE [dbo].[DeleteChatAdAtheleteMatch_2V6] 
	@AthleteEmail varchar(100),
	@LoginType int,
	@ChatAdId int, 
	@SportId int,
	@UserType varchar(20),
	@Id int Output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

 declare @cnt int	 
     declare @UserId int
	 declare @UserSportId int 

	 If(@LoginType = 1)
	   select @UserId = Id from Users Where Emailaddress = @AthleteEmail and LoginType=@LoginType and
	    UserType = @UserType
	 Else
	   select @UserId = Id from Users Where Username = @AthleteEmail and LoginType=@LoginType and
	   UserType = @UserType

	   select  @UserSportId = Id from UserSports where UserId = @UserId and SportId = @SportId
	 
 
       select @cnt = count(*) from AtheleteChatAdMatches where UserSportId = @UserSportId and ChatAdId = @ChatAdId
	 

	 if(@cnt > 0)
	 Begin
    	Delete from  AtheleteChatAdMatches where UserSportId = @UserSportId and ChatAdId = @ChatAdId 
	 End 
		set @Id = @UserId


END
GO




CREATE TABLE [dbo].[ChatAdSports](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChatAdId] [int] NULL,
	[SportId] [int] NULL,
	[CreatedDate] [datetime] NULL
)

GO





CREATE PROCEDURE [dbo].[AddChatAdSports]
	@AdId int,
	@SportId int, 
	@Id int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
		Insert into  ChatAdSports(ChatAdId,SportId,CreatedDate)
		values(@AdId,@SportId,getutcdate())
		set @ID = @@Identity
	  
	select @ID as Id
 
	 
END


GO


 
 


ALTER PROCEDURE [dbo].[GetAds]   
(   
    @EndAdUserId int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@EndAdUserId = 0)
	Begin
	    select ca.Id,ca.Name,ca.ProfilePicURL,ca.WebsiteURL,ca.Description,ca.Adtype,ca.EndAdUserId
		,eau.Name as EndAdUserName,eau.Emailaddress  as EndAdUserEmail,
		count(aca.ChatAdId) as CntAthlete,count(cas.ChatAdId) as cntAdSports, 
	(
	  CASE 
	     WHEN count(cas.SportID)  <= 1 THEN 100
		 WHEN count(cas.SportID) > 1 THEN 1000
	  END
	) as SportId
		from ChatAds ca 
		inner join endadusers eau on eau.id = ca.endaduserid 
		left join AtheleteChatAdMatches aca on aca.ChatAdId = ca.Id and status =1
		left join ChatAdSports cas on cas.ChatAdId = ca.id
		group by ca.Id,ca.Name,ca.ProfilePicURL,ca.WebsiteURL,ca.Description,ca.Adtype,ca.EndAdUserId
		,eau.Name,eau.Emailaddress,status
		having adtype = 4 
	End
	Else
	Begin
        select ca.Id,ca.Name,ca.ProfilePicURL,ca.WebsiteURL,ca.Description,ca.Adtype,ca.EndAdUserId
		,eau.Name as EndAdUserName,eau.Emailaddress  as EndAdUserEmail,
		count(aca.ChatAdId) as CntAthlete,count(cas.ChatAdId) as cntAdSports
		,
		(
		  CASE 
			 WHEN count(cas.SportID)  <= 1 THEN 100
			 WHEN count(cas.SportID) > 1 THEN 1000
		  END
		) as SportId
		from ChatAds ca 
		inner join endadusers eau on eau.id = ca.endaduserid 
		 left join AtheleteChatAdMatches aca on aca.ChatAdId = ca.Id and status =1
	    left join ChatAdSports cas on cas.ChatAdId = ca.id
		group by ca.Id,ca.Name,ca.ProfilePicURL,ca.WebsiteURL,ca.Description,ca.Adtype,ca.EndAdUserId
		,eau.Name,eau.Emailaddress,status
		having adtype = 4 and endaduserid  = @EndAdUserId  
	 
	End
END




 



GO


  

ALTER PROCEDURE [dbo].[GetDisplayAdsForSuperAdmins]  
(   
    @EndAdUserId int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@EndAdUserId = 0)
	Begin
    select ca.*,eau.Name as EndAdUserName,eau.Emailaddress  as EndAdUserEmail
	,count(cas.SportID) as cntAdSports
	,
	(
	  CASE 
	     WHEN count(cas.SportID)  <= 1 THEN 100
		 WHEN count(cas.SportID) > 1 THEN 1000
	  END
	) as SportId
	  from ChatAds ca 
	  inner join endadusers eau on eau.id = ca.endaduserid
	  left join ChatAdSports cas on cas.ChatAdId = ca.id
	  group by ca.Id,ca.Name,ca.ProfilePicURL,ca.WebsiteURL,ca.Description,ca.Adtype,ca.EndAdUserId
		,eau.Name,eau.Emailaddress,cas.ChatAdId 
	  having adtype = 1 
	End
	Else
	Begin
	    select ca.*,eau.Name as EndAdUserName,eau.Emailaddress  as EndAdUserEmail
		,count(cas.ChatAdId) as cntAdSports, 
		(
		  CASE 
			 WHEN count(cas.SportID)  <= 1 THEN 100
			 WHEN count(cas.SportID) > 1 THEN 1000
		  END
		) as SportId
	  from ChatAds ca 
	  inner join endadusers eau on eau.id = ca.endaduserid
	    left join ChatAdSports cas on cas.ChatAdId = ca.id
		  group by ca.Id,ca.Name,ca.ProfilePicURL,ca.WebsiteURL,ca.Description,ca.Adtype,ca.EndAdUserId
		,eau.Name,eau.Emailaddress,cas.ChatAdId 
	  having endaduserid  = @EndAdUserId and adtype = 1 
	End
END

 






GO

 
 

CREATE PROCEDURE [dbo].[GetDisplayAdsForSuperAdminsBySport]  
(    
	@SportId int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	 
      select ca.* 
	  from ChatAds ca  
	  left join ChatAdSports cas on cas.ChatAdId = ca.id 
	  where adtype = 1 and cas.sportid = @SportId
	 
	 
END

 







GO




USE [LemonaidWrapperStaging]
GO

/****** Object:  StoredProcedure [dbo].[GetChatAdCards_2V6]    Script Date: 1/28/2017 12:06:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






ALTER PROCEDURE [dbo].[GetChatAdCards_2V6]  --[GetChatAdCards_2V6] 'puramamaheshbabu@gmail.com',1,14,4,'HS'
(   
  @emailaddress varchar(100),
  @loginType int,
  @SportId int,
  @adType int,
  @UserType varchar(10)
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
     declare @UserId int
	 declare @UserSportId int 

	  If(@LoginType = 1)
	   select @UserId = Id from Users Where Emailaddress = @emailaddress and LoginType=@LoginType and
	    UserType = @UserType
	 Else
	   select @UserId = Id from Users Where Username = @emailaddress and LoginType=@LoginType and
	   UserType = @UserType

	   select  @UserSportId = Id from UserSports where UserId = @UserId and SportId = @SportId

	    

 select ca.* from ChatAds ca
 left join ChatAdSports cas on cas.ChatAdId = ca.id 
 Where ca.Adtype = @adType and cas.SportId = @SportId  
 and ca.id not in(
 select ChatAdId from AtheleteChatAdMatches 
 where UserSportId = @UserSportId and status = 1
 )

END







GO


 

 
  
 
CREATE PROCEDURE [dbo].[GetAthleteChatAdFavourites_2V6]  --[GetAthleteChatAdFavourites_2V6]  'elitecrest2@gmail.com',1,14,0,100
@AthleteEmail varchar(100),
@LoginType int, 
@SportId int,
@offset int,
@max int,
@UserType varchar(20)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @totalCount int    
	 declare @cnt int	 
     declare @UserId int
	 declare @UserSportId int 

	 If(@LoginType = 1)
	   select @UserId = Id from Users Where Emailaddress = @AthleteEmail and LoginType=@LoginType and
	    UserType = @UserType
	 Else
	   select @UserId = Id from Users Where Username = @AthleteEmail and LoginType=@LoginType and
	   UserType = @UserType

	   select  @UserSportId = Id from UserSports where UserId = @UserId and SportId = @SportId
	    
 
	 select @totalCount = count(*) from AtheleteChatAdMatches acam 
		where acam.UserSportId = @UserSportId and acam.status = 1 


	select ca.*,acam.CreatedDate from ChatAds ca
	left join AtheleteChatAdMatches acam on ca.id = acam.ChatAdId 
	where acam.UserId = @UserId and SportId = @SportId and acam.status = 1 
	 
        
END

 



 





















GO



 Alter table AtheleteChatAdMatches add UserType varchar(20)

Update AtheleteChatAdMatches set UserType = 'HS'
 


 
ALTER PROCEDURE [dbo].[AddAthleteChatAdMatch_2V6]
	@AthleteEmail varchar(100),
	@LoginType int,
	@ChatAdId int,
	@Status bit, 
	@SportId int,
	@UserType varchar(10),
	@Id int Output

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

	 declare @cnt int	 
     declare @UserId int
	 declare @UserSportId int 

	 If(@LoginType = 1)
	   select @UserId = Id from Users Where Emailaddress = @AthleteEmail and LoginType=@LoginType and
	    UserType =@UserType
	 Else
	   select @UserId = Id from Users Where Username = @AthleteEmail and LoginType=@LoginType and
	   UserType = @UserType

	   select  @UserSportId = Id from UserSports where UserId = @UserId and SportId = @SportId
	
	-- select @cnt = count(*) from AtheleteChatAdMatches where UserSportId = @UserSportId and ChatAdId = @ChatAdId
	 select @cnt = count(*) from AtheleteChatAdMatches where UserId = @UserId and SportId = @SportId and ChatAdId = @ChatAdId
	 
	 if(@cnt = 0)
	 Begin
		insert into AtheleteChatAdMatches (UserSportId,ChatAdId,[Status],CreatedDate,UserId,SportId,UserType) 
		values (@UserSportId,@ChatAdId,@Status,GETUTCDATE(),@UserId,@SportId,@UserType)
		
	 End
	 Else
	 Begin
	    Update AtheleteChatAdMatches set [Status] = @Status 
		where UserId = @UserId and SportId = @SportId and ChatAdId = @ChatAdId and UserType = @UserType
	 End 
        select @cnt = count(*) from AtheleteChatAdMatches where UserId = @UserId and SportId = @SportId and UserType = @UserType
		and status  = 1
		set @Id = @cnt


END

 


  






 


GO

USE [LemonaidWrapperStaging]
GO

/****** Object:  StoredProcedure [dbo].[GetAthletesForChatAdByAdId]    Script Date: 2/25/2017 4:42:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[GetUsersForChatAdByAdId_2V6] -- [GetUsersForChatAdByAdId_2V6] 31,'ClC'
(   
 @AdId int,
 @UserType varchar(20)
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
 
 select acam.ChatAdId,ca.Name,ca.ProfilePicURL,ca.WebsiteURL,ca.Description,ca.Adtype,ca.EndAdUserId
 ,u.id as AthleteID,
 min(us.Sportid) as Sportid,
 u.emailaddress,u.username,u.logintype,u.UserType
 from AtheleteChatAdMatches  acam
 Inner join ChatAds ca on ca.id = acam.ChatAdId
 Inner join EndAdUsers eau on eau.id  = ca.EndAdUserId
 Inner join UserSports us on us.id = acam.usersportid
 Inner join Users u on u.id = us.UserId
 group by acam.ChatAdId,ca.Name,ca.ProfilePicURL,ca.WebsiteURL,ca.Description,ca.Adtype,ca.EndAdUserId
 ,u.id,u.emailaddress,u.username,u.logintype,u.UserType,eau.id,acam.status,ca.Id,acam.UserType
 having ca.Id = @AdId and acam.status=1 and acam.UserType = @UserType


END





 

GO


 
ALTER PROCEDURE [dbo].[ChatCountByChatID] --[ChatCountByChatID] 32
(
@ChatAdId int
)
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

    -- Insert statements for procedure here
 declare @HsCount int
   declare @FFFCount int
   declare @ClcCount int


   select @HsCount= count(Chatadid) from AtheleteChatAdMatches 
   where usertype in('HS') and chatadid=@ChatAdId and status = 1
   group by  Chatadid,usertype


   select @FFFCount =count(Chatadid)  from AtheleteChatAdMatches 
   where usertype in('FFF') and chatadid=@ChatAdId  and status = 1
   group by  Chatadid,usertype


   select @ClcCount =  count(Chatadid)  from AtheleteChatAdMatches 
   where usertype in('CLC') and chatadid=@ChatAdId  and status = 1
   group by  Chatadid,usertype 


  
   select isnull(@ChatAdId,0) as ChatAdId,isnull(@HsCount,0) as AthCount,
   isnull(@FFFCount,0) as FFFCount,isnull(@ClcCount,0) as ClcCount
END
GO


  

CREATE PROCEDURE [dbo].[RefreshChatAds] -- 'Puramamaheshbabu@gmail.com',1,14,'HS'
	@EmailAddress varchar(100),
	@LoginType int, 
	@SportId int,
	@UserType varchar(20) 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

 declare @cnt int	 
     declare @UserId int
	 declare @UserSportId int 

	 If(@LoginType = 1)
	   select @UserId = Id from Users Where Emailaddress = @EmailAddress and LoginType=@LoginType and
	    UserType = @UserType
	 Else
	   select @UserId = Id from Users Where Username = @EmailAddress and LoginType=@LoginType and
	   UserType = @UserType

	   select  @UserSportId = Id from UserSports where UserId = @UserId and SportId = @SportId
	   
    	Delete from  AtheleteChatAdMatches where UserSportId = @UserSportId and Status = 0
	  

END

GO



 



ALTER PROCEDURE [dbo].[DeleteAd] 
(   
    @adId int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 Begin Try
		Begin Tran
		   Delete from AtheleteChatAdMatches where ChatAdId = @adId
		   Delete from AthleteChatAdChats where ReceiverId = @adId and ReceiverType = 1 
		   Delete from AthleteChatAdChats where SenderId = @adId and SenderType = 1 
		   Delete from ChatAds where Id  = @adId
		
		   select @adId as Id

      Commit Tran
	End Try
	Begin catch
	 Rollback Tran
	End Catch

END
 


GO


 



ALTER PROCEDURE [dbo].[GetDisplayAdsForSuperAdminsBySport]  --[GetDisplayAdsForSuperAdminsBySport] 14
(    
	@SportId int
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	 
      select ca.* 
	  from ChatAds ca  
	  left join ChatAdSports cas on cas.ChatAdId = ca.id 
	  where adtype = 1 and cas.sportid = @SportId
	  and ca.id not in(select ChatAdId from AtheleteChatAdMatches where sportid = @SportId)
	 
	 
END
GO


CREATE TABLE [dbo].[ChatAdType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[UserType] [varchar] (10) NULL,
	[Type] [Int] NULL,
	 
)

GO

INSERT INTO [dbo].[ChatAdType]([Name],[UserType],[Type]) VALUES('Athlete' ,'HS',0)
GO
INSERT INTO [dbo].[ChatAdType]([Name],[UserType],[Type]) VALUES('Coach' ,'CC',1)
GO
INSERT INTO [dbo].[ChatAdType]([Name],[UserType],[Type]) VALUES('ClubCoach' ,'ClC',2)
GO
INSERT INTO [dbo].[ChatAdType]([Name],[UserType],[Type]) VALUES('FamilyFriends' ,'FFF',3)
GO
INSERT INTO [dbo].[ChatAdType]([Name],[UserType],[Type]) VALUES('ChatAd' ,'Ad',4)
GO


 



ALTER PROCEDURE [dbo].[DeleteUserFromWrapper]  
(
@emailaddress varchar(100),
@logintype int,
@sportId int,
@usertype varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

     declare @userid int
	   declare @usersportcount int

	 Begin try
	 Begin tran
		 if(@logintype = 1 or @logintype = 2)
		 Begin
		  select @userid = id from users where emailaddress = @emailaddress and usertype = @usertype and logintype = @logintype
		 End
		 else
		 Begin
			select @userid = id from users where emailaddress = @emailaddress and usertype = @usertype 
		 end

		 Delete from usersports where userid  = @userid and sportid = @sportId
		 Delete from AtheleteChatAdMatches where UserId = @UserId and SportId = @SportId
		 
		 select @usersportcount = count(*) from usersports where userid  = @userid
		 if(@usersportcount = 0)
		   Delete from users where id = @userid

		select @userid as Id

	commit tran
	End Try
	Begin Catch
	 rollback Tran
	End Catch

END





GO



  

 
  

 
 
ALTER PROCEDURE [dbo].[GetAthleteChatAdChatMessages] 
    @SenderId int,
	@ReceiverId int,
	@SenderType int,
	@ReceiverType int,
	@Date Datetime,
	@offset int,
	@max int
  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements. 
	 
	  declare @chatvisitedCnt int 
	  Begin Try
		Begin Tran
		 if(@Date = '01/01/1900')
		 Begin
			   select * from AthleteChatAdChats where  ((SenderId = @SenderId and ReceiverId = @ReceiverId
			   and  SenderType=@SenderType and  ReceiverType=@ReceiverType)
			   or(SenderId = @ReceiverId and ReceiverId = @SenderId  
			   and  SenderType=@ReceiverType and  ReceiverType=@SenderType))
				order by [Date] desc
				OFFSET @Offset ROWS FETCH NEXT @max ROWS ONLY

			 select @chatvisitedCnt = count(*) from AthleteChatAdChatsLastVisited where [SenderId] = @SenderId and [ReceiverId] = @ReceiverId
		 
			 If(@chatvisitedCnt = 0)
			 Begin
				 Insert into AthleteChatAdChatsLastVisited([SenderId],[ReceiverId],[LastVisitedDate],[SenderType],[ReceiverType],[CreatedDate])
				 values(@SenderId,@ReceiverId,getutcdate(),@SenderType,@ReceiverType,getutcdate())
			 End
			 Else
			 Begin
			   Update AthleteChatAdChatsLastVisited set [LastVisitedDate] = getutcdate() 
			   Where [SenderId] = @SenderId and [ReceiverId] = @ReceiverId
			    and  SenderType=@SenderType and  ReceiverType=@ReceiverType
			 End 
		 End
		 Else
		 Begin
		   select * from AthleteChatAdChats where ((SenderId = @SenderId and ReceiverId = @ReceiverId
			   and  SenderType=@SenderType and  ReceiverType=@ReceiverType)
			   or(SenderId = @ReceiverId and ReceiverId = @SenderId  
			   and  SenderType=@ReceiverType and  ReceiverType=@SenderType))
			 and [Date] > @Date
			order by [Date] desc

			 select @chatvisitedCnt = count(*) from AthleteChatAdChatsLastVisited where [SenderId] = @SenderId and [ReceiverId] = @ReceiverId
		 
			 If(@chatvisitedCnt = 0)
			 Begin
				 Insert into AthleteChatAdChatsLastVisited([SenderId],[ReceiverId],[LastVisitedDate],[SenderType],[ReceiverType],[CreatedDate])
				 values(@SenderId,@ReceiverId,getutcdate(),@SenderType,@ReceiverType,getutcdate())
			 End
			 Else
			 Begin
			   Update AthleteChatAdChatsLastVisited set [LastVisitedDate] = getutcdate() 
			   Where [SenderId] = @SenderId and [ReceiverId] = @ReceiverId
			   and SenderType=@SenderType and  ReceiverType=@ReceiverType
			 End 
		 End  
	   Commit Tran
	End Try
	Begin catch
	 Rollback Tran
	End Catch

END


GO




 Alter table Users add DeviceId int


 

ALTER PROCEDURE [dbo].[AddUser]  --[AddUser] 'elitecrest1.1@gmail.com','',0,'FFF',0
   (
	@Emailaddress varchar(100),
    @Username varchar(500),
	@LoginType int,
	@UserType varchar(20), -- HS,CC,CA,ClC,FFF
    @DeviceType varchar(50),
	@DeviceId varchar(500),
	@Id int Output
	)

	As
	Begin
	declare @cnt int 
	declare @uniqId varchar(1000) 
	declare @Str varchar(10)
	declare @getDeviceId int 
	declare @devicecnt int

	If(isnull(@DeviceId,'') != '')
	 Begin
		select @devicecnt = count(*) from Device where  DeviceId = @DeviceId and  DeviceType = @DeviceType
		 if(@devicecnt = 0)
		 Begin
			insert into Device(DeviceID,DeviceType,CreateDate) 
			values (@DeviceId,@DeviceType,getutcdate())
			set @getDeviceId = @@Identity
		 End
		 else
		 begin
		  set @getDeviceId = (select Id from Device where DeviceID = @DeviceId and DeviceType=@DeviceType)
		 end
	 End
	 Else
	  set @getDeviceId = 0
 
	if(@UserType = 'HS')
	Begin
		if(isnull(@LoginType,1) = 1)
		 Begin
		 select @cnt = count(*) from Users where Emailaddress = @Emailaddress and logintype = @LoginType
		 and (UserType = 'HS' or UserType = 'CA')
		 End
		 Else
		 Begin
		  select @cnt = count(*) from Users where Username = @Username  and logintype = @LoginType
		   and (UserType = 'HS' or UserType = 'CA')
		 End 
	 End
	 Else 
	   if(@UserType != 'CC') 
	   Begin
	     if(isnull(@LoginType,1) = 1 or isnull(@LoginType,1) = 0)
			 Begin
			 select @cnt = count(*) from Users where Emailaddress = @Emailaddress and logintype = @LoginType
			 and  UserType = @UserType 
			 End
			 Else
			 Begin
			  select @cnt = count(*) from Users where Username = @Username  and logintype = @LoginType
			   and UserType = @UserType 
			 End 
	  End
	 Else
	  Begin
	     select @cnt = count(*) from Users where Emailaddress = @Emailaddress and logintype = @LoginType
		  and  UserType = @UserType 
	  End
	 
	  

	  if(@cnt = 0)
	 Begin
		insert into Users(Emailaddress,[CreatedDate],Username,LoginType,UNIQID,UserType,DeviceId) 
		values (@Emailaddress,GETUTCDATE(),@Username,@LoginType,@uniqId,@UserType,@getDeviceId)
		set @Id = @@Identity 

		set @uniqId =  @UserType +  cast(@Id as varchar)
		Update Users set UNIQID = @uniqId where id = @Id
	 End
	 else
	 Begin
		if(@UserType = 'HS')
		Begin
			 if(isnull(@LoginType,1) = 1)
			 Begin
			 select @Id = Id from Users where Emailaddress = @Emailaddress and logintype = @LoginType
			  and (UserType = 'HS' or UserType = 'CA')
			 End
			 Else
			 Begin
			 select @Id = Id from Users where Username = @Username  and logintype = @LoginType
			  and (UserType = 'HS' or UserType = 'CA')
			 End 
		End
		 Else 
	   if(@UserType != 'CC') 
	   Begin
	     if(isnull(@LoginType,1) = 1 or isnull(@LoginType,1) = 0)
			 Begin
			 select @Id = Id from Users where Emailaddress = @Emailaddress and logintype = @LoginType
			 and  UserType = @UserType 
			 End
			 Else
			 Begin
			  select @Id = Id  from Users where Username = @Username  and logintype = @LoginType
			   and UserType = @UserType 
			 End 
	  End
	 Else
	  Begin
	     select @Id = Id  from Users where Emailaddress = @Emailaddress and logintype = @LoginType
		  and  UserType = @UserType 
	  End
	 
	 End 

	if(@UserType = 'HS')
	Begin
		select * from Users
		where  Id = @Id   and (UserType = 'HS' or UserType = 'CA')
	End
	Else
	Begin
		select * from Users
		where  Id = @Id  and UserType = @UserType
	End
	End



GO


 

ALTEr PROCEDURE [dbo].[GetUsersChatAdChatCount]    
 (
    @UserType varchar(100) 
 )
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here  

	Declare @ReceiverType int

	   if(@UserType = 'HS')
	  set @ReceiverType = 0
		if(@UserType = 'CC')
	  set @ReceiverType = 1
	  	if(@UserType = 'ClC')
	  set @ReceiverType = 2
	  	if(@UserType = 'FFF')
	  set @ReceiverType = 3

 select distinct u.Id,u.emailaddress,u.logintype,u.username,
 d.DeviceType,d.DeviceId from users u
 left join AthleteChatAdChats acac on acac.Receiverid = u.Id and acac.ReceiverType = @ReceiverType
 left join AthleteChatAdChatsLastVisited aclv on aclv.ReceiverId = u.Id
 left join Device d on d.id = u.DeviceId  
 where usertype = @UserType and acac.Date > aclv.LastVisitedDate



 END


GO


 

  
 
ALTEr PROCEDURE [dbo].[GetAthleteChatAdFavourites_2V6]  --[GetAthleteChatAdFavourites_2V6]  'elitecrest51',3,14,0,100,'FFF'
@AthleteEmail varchar(100),
@LoginType int, 
@SportId int,
@offset int,
@max int,
@UserType varchar(20)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @totalCount int    
	 declare @cnt int	 
     declare @UserId int
	 declare @UserSportId int 

	 If(@LoginType = 1)
	   select @UserId = Id from Users Where Emailaddress = @AthleteEmail and LoginType=@LoginType and
	    UserType = @UserType
	 Else
	   select @UserId = Id from Users Where Username = @AthleteEmail and LoginType=@LoginType and
	   UserType = @UserType

	   select  @UserSportId = Id from UserSports where UserId = @UserId and SportId = @SportId
	    
 
	 select @totalCount = count(*) from AtheleteChatAdMatches acam 
		where acam.UserSportId = @UserSportId and acam.status = 1 


	select ca.*,acam.CreatedDate from ChatAds ca
	left join AtheleteChatAdMatches acam on ca.id = acam.ChatAdId 
	where acam.UserId = @UserId and SportId = @SportId and acam.status = 1 
	and ca.Adtype = 4
        
END 


GO


USE [LemonaidWrapperStaging]
GO

/****** Object:  StoredProcedure [dbo].[GetUsersForChatAdByAdId_2V6]    Script Date: 3/20/2017 3:22:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






After 2.6V
 

 
ALTER PROCEDURE [dbo].[GetChatAdCards_2V6]  --[GetChatAdCards_2V6] 'elitecrest1@gmail.com',1,15,4,'FFF'
(   
  @emailaddress varchar(100),
  @loginType int,
  @SportId int,
  @adType int,
  @UserType varchar(10)
)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
     declare @UserId int
	 declare @UserSportId int 

	  If(@LoginType = 1)
	   select @UserId = Id from Users Where Emailaddress = @emailaddress and LoginType=@LoginType and
	    UserType = @UserType
	 Else
	   select @UserId = Id from Users Where Username = @emailaddress and LoginType=@LoginType and
	   UserType = @UserType

	   select  @UserSportId = Id from UserSports where UserId = @UserId and SportId = @SportId

	    

 select ca.* from ChatAds ca
 left join ChatAdSports cas on cas.ChatAdId = ca.id 
 Where ca.Adtype = @adType and cas.SportId = @SportId  and ca.id not in(
 select ChatAdId from AtheleteChatAdMatches 
 where  UserId = @UserId  --and status = 1
 )

END


GO


 

 
