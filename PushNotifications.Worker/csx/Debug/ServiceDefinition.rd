﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="PushNotifications.Worker" generation="1" functional="0" release="0" Id="ca984948-6dce-494b-bd97-92b2af6b4993" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="PushNotifications.WorkerGroup" generation="1" functional="0" release="0">
      <settings>
        <aCS name="PushNotifications:APPINSIGHTS_INSTRUMENTATIONKEY" defaultValue="">
          <maps>
            <mapMoniker name="/PushNotifications.Worker/PushNotifications.WorkerGroup/MapPushNotifications:APPINSIGHTS_INSTRUMENTATIONKEY" />
          </maps>
        </aCS>
        <aCS name="PushNotifications:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/PushNotifications.Worker/PushNotifications.WorkerGroup/MapPushNotifications:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="PushNotificationsInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/PushNotifications.Worker/PushNotifications.WorkerGroup/MapPushNotificationsInstances" />
          </maps>
        </aCS>
      </settings>
      <maps>
        <map name="MapPushNotifications:APPINSIGHTS_INSTRUMENTATIONKEY" kind="Identity">
          <setting>
            <aCSMoniker name="/PushNotifications.Worker/PushNotifications.WorkerGroup/PushNotifications/APPINSIGHTS_INSTRUMENTATIONKEY" />
          </setting>
        </map>
        <map name="MapPushNotifications:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/PushNotifications.Worker/PushNotifications.WorkerGroup/PushNotifications/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapPushNotificationsInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/PushNotifications.Worker/PushNotifications.WorkerGroup/PushNotificationsInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="PushNotifications" generation="1" functional="0" release="0" software="E:\WorkingCopies\lemonaidapi\PushNotifications.Worker\csx\Debug\roles\PushNotifications" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="APPINSIGHTS_INSTRUMENTATIONKEY" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;PushNotifications&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;PushNotifications&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/PushNotifications.Worker/PushNotifications.WorkerGroup/PushNotificationsInstances" />
            <sCSPolicyUpdateDomainMoniker name="/PushNotifications.Worker/PushNotifications.WorkerGroup/PushNotificationsUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/PushNotifications.Worker/PushNotifications.WorkerGroup/PushNotificationsFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="PushNotificationsUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="PushNotificationsFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="PushNotificationsInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
</serviceModel>