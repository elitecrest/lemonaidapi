﻿using Lemonaid_web_api.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
 

namespace Lemonaid_web_api.Models
{
    public class Dal2
    {

        internal static AthleteDTO AddAthlete2_v1(AthleteDTO athleteDto)
        {
            AthleteDTO v = new AthleteDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthlete2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    name.Direction = ParameterDirection.Input;
                    name.Value = athleteDto.Name;

                    SqlParameter gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    gender.Direction = ParameterDirection.Input;
                    gender.Value = athleteDto.Gender;

                    SqlParameter school = cmd.Parameters.Add("@School", SqlDbType.VarChar, 100);
                    school.Direction = ParameterDirection.Input;
                    school.Value = athleteDto.School ?? string.Empty;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = athleteDto.Emailaddress;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = athleteDto.Description ?? string.Empty;

                    SqlParameter gpa = cmd.Parameters.Add("@GPA", SqlDbType.Float);
                    gpa.Direction = ParameterDirection.Input;
                    gpa.Value = athleteDto.GPA;

                    SqlParameter social = cmd.Parameters.Add("@Social", SqlDbType.Int);
                    social.Direction = ParameterDirection.Input;
                    social.Value = athleteDto.Social;

                    SqlParameter sat = cmd.Parameters.Add("@SAT", SqlDbType.Int);
                    sat.Direction = ParameterDirection.Input;
                    sat.Value = athleteDto.SAT;

                    SqlParameter act = cmd.Parameters.Add("@ACT", SqlDbType.Int);
                    act.Direction = ParameterDirection.Input;
                    act.Value = athleteDto.ACT;

                    SqlParameter instituteId = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteId.Direction = ParameterDirection.Input;
                    instituteId.Value = athleteDto.InstituteId;

                    SqlParameter profilePicUrl = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    profilePicUrl.Direction = ParameterDirection.Input;
                    profilePicUrl.Value = athleteDto.ProfilePicURL;

                    SqlParameter dob = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    dob.Direction = ParameterDirection.Input;
                    dob.Value = athleteDto.DOB;

                    SqlParameter address = cmd.Parameters.Add("@Address", SqlDbType.VarChar, 100);
                    address.Direction = ParameterDirection.Input;
                    address.Value = athleteDto.Address;

                    SqlParameter facebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    facebookId.Direction = ParameterDirection.Input;
                    facebookId.Value = athleteDto.FacebookId;

                    SqlParameter latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    latitude.Direction = ParameterDirection.Input;
                    latitude.Value = athleteDto.Latitude;

                    SqlParameter longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    longitude.Direction = ParameterDirection.Input;
                    longitude.Value = athleteDto.Longitude;


                    SqlParameter academicLevel = cmd.Parameters.Add("@AcademicLevel", SqlDbType.Int);
                    academicLevel.Direction = ParameterDirection.Input;
                    academicLevel.Value = athleteDto.AcademicLevel;


                    SqlParameter yearLevel = cmd.Parameters.Add("@YearLevel", SqlDbType.Int);
                    yearLevel.Direction = ParameterDirection.Input;
                    yearLevel.Value = athleteDto.YearLevel;


                    SqlParameter premium = cmd.Parameters.Add("@Premium", SqlDbType.Int);
                    premium.Direction = ParameterDirection.Input;
                    premium.Value = athleteDto.Premium;

                    SqlParameter height1 = cmd.Parameters.Add("@Height1", SqlDbType.VarChar, 20);
                    height1.Direction = ParameterDirection.Input;
                    height1.Value = athleteDto.Height ?? string.Empty;

                    SqlParameter height2 = cmd.Parameters.Add("@Height2", SqlDbType.VarChar, 20);
                    height2.Direction = ParameterDirection.Input;
                    height2.Value = athleteDto.Height2 ?? string.Empty;

                    SqlParameter heightType = cmd.Parameters.Add("@HeightType", SqlDbType.VarChar, 20);
                    heightType.Direction = ParameterDirection.Input;
                    heightType.Value = athleteDto.HeightType ?? string.Empty;

                    SqlParameter weight = cmd.Parameters.Add("@Weight", SqlDbType.VarChar, 20);
                    weight.Direction = ParameterDirection.Input;
                    weight.Value = athleteDto.Weight ?? string.Empty;

                    SqlParameter weightType = cmd.Parameters.Add("@WeightType", SqlDbType.VarChar, 20);
                    weightType.Direction = ParameterDirection.Input;
                    weightType.Value = athleteDto.WeightType ?? string.Empty;

                    SqlParameter wingSpan = cmd.Parameters.Add("@WingSpan", SqlDbType.VarChar, 20);
                    wingSpan.Direction = ParameterDirection.Input;
                    wingSpan.Value = athleteDto.WingSpan ?? string.Empty;

                    SqlParameter wingSpanType = cmd.Parameters.Add("@WingSpanType", SqlDbType.VarChar, 20);
                    wingSpanType.Direction = ParameterDirection.Input;
                    wingSpanType.Value = athleteDto.WingSpanType ?? string.Empty;

                    SqlParameter shoeSize = cmd.Parameters.Add("@ShoeSize", SqlDbType.VarChar, 20);
                    shoeSize.Direction = ParameterDirection.Input;
                    shoeSize.Value = athleteDto.ShoeSize ?? string.Empty;

                    SqlParameter shoeSizeType = cmd.Parameters.Add("@ShoeSizeType", SqlDbType.VarChar, 20);
                    shoeSizeType.Direction = ParameterDirection.Input;
                    shoeSizeType.Value = athleteDto.ShoeSizeType ?? string.Empty;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = string.IsNullOrWhiteSpace(athleteDto.ClubName) ? string.Empty : athleteDto.ClubName;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = string.IsNullOrWhiteSpace(athleteDto.CoachName)
                        ? string.Empty
                        : athleteDto.CoachName;

                    SqlParameter coachEmailId = cmd.Parameters.Add("@CoachEmailId", SqlDbType.VarChar, 100);
                    coachEmailId.Direction = ParameterDirection.Input;
                    coachEmailId.Value = string.IsNullOrWhiteSpace(athleteDto.CoachEmailId)
                        ? string.Empty
                        : athleteDto.CoachEmailId;

                    SqlParameter atheleteType = cmd.Parameters.Add("@AtheleteType", SqlDbType.Int);
                    atheleteType.Direction = ParameterDirection.Input;
                    atheleteType.Value = athleteDto.AtheleteType;

                    SqlParameter active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    active.Direction = ParameterDirection.Input;
                    active.Value = athleteDto.Active;

                    SqlParameter major = cmd.Parameters.Add("@Major", SqlDbType.VarChar, 50);
                    major.Direction = ParameterDirection.Input;
                    major.Value = string.IsNullOrWhiteSpace(athleteDto.Major) ? string.Empty : athleteDto.Major;

                    SqlParameter phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    phonenumber.Direction = ParameterDirection.Input;
                    phonenumber.Value = string.IsNullOrWhiteSpace(athleteDto.Phonenumber)
                        ? string.Empty
                        : athleteDto.Phonenumber;

                    SqlParameter username = cmd.Parameters.Add("@Username", SqlDbType.VarChar, 500);
                    username.Direction = ParameterDirection.Input;
                    username.Value = string.IsNullOrWhiteSpace(athleteDto.UserName) ? string.Empty : athleteDto.UserName;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = (athleteDto.LoginType == null) ? 0 : athleteDto.LoginType;

                    SqlParameter deviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    deviceType.Direction = ParameterDirection.Input;
                    deviceType.Value = (athleteDto.DeviceType == null) ? string.Empty : athleteDto.DeviceType;

                    SqlParameter deviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 50);
                    deviceId.Direction = ParameterDirection.Input;
                    deviceId.Value = (athleteDto.DeviceId == null) ? string.Empty : athleteDto.DeviceId;

                    SqlParameter verticalJump = cmd.Parameters.Add("@VerticalJump", SqlDbType.VarChar, 20);
                    verticalJump.Direction = ParameterDirection.Input;
                    verticalJump.Value = (athleteDto.VerticalJump == null) ? string.Empty : athleteDto.VerticalJump;

                    SqlParameter broadJump = cmd.Parameters.Add("@BroadJump", SqlDbType.VarChar, 20);
                    broadJump.Direction = ParameterDirection.Input;
                    broadJump.Value = (athleteDto.BroadJump == null) ? string.Empty : athleteDto.BroadJump;

                    SqlParameter stateId = cmd.Parameters.Add("@StateId", SqlDbType.Int);
                    stateId.Direction = ParameterDirection.Input;
                    stateId.Value = athleteDto.StateId;

                    SqlParameter schoolCoachName = cmd.Parameters.Add("@SchoolCoachName", SqlDbType.VarChar, 100);
                    schoolCoachName.Direction = ParameterDirection.Input;
                    schoolCoachName.Value = (athleteDto.SchoolCoachName == null)
                        ? string.Empty
                        : athleteDto.SchoolCoachName;

                    SqlParameter schoolCoachEmail = cmd.Parameters.Add("@SchoolCoachEmail", SqlDbType.VarChar, 100);
                    schoolCoachEmail.Direction = ParameterDirection.Input;
                    schoolCoachEmail.Value = (athleteDto.SchoolCoachEmail == null)
                        ? string.Empty
                        : athleteDto.SchoolCoachEmail;

                    SqlParameter schoolZipCode = cmd.Parameters.Add("@SchoolZipCode", SqlDbType.VarChar, 100);
                    schoolZipCode.Direction = ParameterDirection.Input;
                    schoolZipCode.Value = (athleteDto.SchoolZipCode == null) ? string.Empty : athleteDto.SchoolZipCode;

                    SqlParameter gapYearLevel = cmd.Parameters.Add("@GapYearLevel", SqlDbType.Int);
                    gapYearLevel.Direction = ParameterDirection.Input;
                    gapYearLevel.Value = athleteDto.GapyearLevel;

                    SqlParameter gapYearDesc = cmd.Parameters.Add("@GapyearDescription", SqlDbType.VarChar, 2000);
                    gapYearDesc.Direction = ParameterDirection.Input;
                    gapYearDesc.Value = (athleteDto.GapyearDescription == null)
                        ? string.Empty
                        : athleteDto.GapyearDescription;

                    SqlParameter ap = cmd.Parameters.Add("@AP", SqlDbType.Float);
                    ap.Direction = ParameterDirection.Input;
                    ap.Value = athleteDto.AP;

                    SqlParameter country = cmd.Parameters.Add("@Country", SqlDbType.VarChar, 100);
                    country.Direction = ParameterDirection.Input;
                    country.Value = (athleteDto.Country == null) ? string.Empty : athleteDto.Country;

                    SqlParameter countryId = cmd.Parameters.Add("@CountryId", SqlDbType.Int);
                    countryId.Direction = ParameterDirection.Input;
                    countryId.Value = athleteDto.CountryId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    SqlDataReader myData = cmd.ExecuteReader();

                    v = GetAthlete(myData);

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/AddAthlete2_v1", "", ex.Message, "Exception", 0);
            }

            return v;
        }


        internal static AthleteDTO GetAthleteProfile(string emailaddress, int type)
        {
            AthleteDTO v = new AthleteDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteProfile_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter typePar = cmd.Parameters.Add("@type", SqlDbType.Int);
                    typePar.Direction = ParameterDirection.Input;
                    typePar.Value = type;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.TotalCount = 1; // (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value)
                            ? 0
                            : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UnivAddress"]);
                        v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UnivEmailAddress"]);
                        v.UniversityName = (myData["UniversityName"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UniversityName"]);
                        v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UnviPhoneNumber"]);
                        v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UnivProfilePic"]);
                        v.UniversitySize = (myData["UnivSize"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UnivSize"]);
                        v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["ClassificationName"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        v.SchoolCoachName = (myData["SchoolCoachName"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachName"];
                        v.SchoolCoachEmail = (myData["SchoolCoachEmail"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachEmail"];
                        v.SchoolZipCode = (myData["SchoolZipCode"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolZipCode"];
                        v.GapyearLevel = (myData["GapyearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["GapyearLevel"]);
                        v.GapyearDescription = (myData["GapyearDescription"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["GapyearDescription"];
                        v.Country = (myData["Country"] == DBNull.Value) ? string.Empty : (string)myData["Country"];
                        v.CountryId = (myData["CountryId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CountryId"]);
                        v.AP = (myData["AP"] == DBNull.Value) ? 0.0 : Convert.ToDouble(myData["AP"]);
                        v.isProfileCompleted = (myData["isProfileCompleted"] == DBNull.Value) ? false : Convert.ToBoolean(myData["isProfileCompleted"]);
                        v.AthleteTransition = (myData["AthleteTransition"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteTransition"]);
                        v._20YardShuttleRun = (myData["20YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["20YardShuttleRun"];
                        v._60YardShuttleRun = (myData["60YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["60YardShuttleRun"];
                        v.KneelingMedBallToss = (myData["KneelingMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["KneelingMedBallToss"];
                        v.RotationalMedBallToss = (myData["RotationalMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["RotationalMedBallToss"]; 
                        v.DeviceId = (myData["DeviceName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceName"]);
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceType"]); 
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetAthleteProfile", "", ex.Message, "Exception", 0);
            }

            return v;
        }

        internal static AthleteDTO GetAthlete(SqlDataReader myData)
        {
            AthleteDTO v = new AthleteDTO();
            while (myData.Read())
            {

                v.Id = (int)myData["id"];
                v.Name = myData["name"].ToString();
                v.Description = myData["description"].ToString();
                v.School = myData["School"].ToString();
                v.Emailaddress = myData["emailaddress"].ToString();
                v.GPA = Convert.ToDouble(myData["GPA"]);
                v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                v.FacebookId = myData["FacebookId"].ToString();
                v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                v.DOB = myData["DOB"].ToString();
                v.Address = myData["Address"].ToString();
                v.Latitude = myData["Latitude"].ToString();
                v.Longitude = myData["Longitude"].ToString();
                v.TotalCount = 1; // (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                v.AdType = 2;
                v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                v.HeightType = (myData["HeightType"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["HeightType"]);
                v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                v.WeightType = (myData["WeightType"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["WeightType"]);
                v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                v.WingSpanType = (myData["WingSpanType"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["WingSpanType"]);
                v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["ShoeSizeType"]);
                v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                v.CoachName = (myData["CoachName"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["CoachName"]);
                v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["CoachEmailId"]);
                v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value)
                    ? 0
                    : Convert.ToInt32(myData["AcademicAllAmerican"]);
                v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["UnivAddress"]);
                v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["UnivEmailAddress"]);
                v.UniversityName = (myData["UniversityName"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["UniversityName"]);
                v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["UnviPhoneNumber"]);
                v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["UnivProfilePic"]);
                v.UniversitySize = (myData["UnivSize"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["UnivSize"]);
                v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value)
                    ? string.Empty
                    : Convert.ToString(myData["ClassificationName"]);
                v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                v.VerticalJump = (myData["VerticalJump"] == DBNull.Value)
                    ? string.Empty
                    : (string)myData["VerticalJump"];
                v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                v.SchoolCoachName = (myData["SchoolCoachName"] == DBNull.Value)
                    ? string.Empty
                    : (string)myData["SchoolCoachName"];
                v.SchoolCoachEmail = (myData["SchoolCoachEmail"] == DBNull.Value)
                    ? string.Empty
                    : (string)myData["SchoolCoachEmail"];
                v.SchoolZipCode = (myData["SchoolZipCode"] == DBNull.Value)
                    ? string.Empty
                    : (string)myData["SchoolZipCode"];
                v.GapyearLevel = (myData["GapyearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["GapyearLevel"]);
                v.GapyearDescription = (myData["GapyearDescription"] == DBNull.Value)
                    ? string.Empty
                    : (string)myData["GapyearDescription"];
                v.Country = (myData["Country"] == DBNull.Value) ? string.Empty : (string)myData["Country"];
                v.CountryId = (myData["CountryId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CountryId"]);
                v.AP = (myData["AP"] == DBNull.Value) ? 0.0 : Convert.ToDouble(myData["AP"]);
                v.isProfileCompleted = (myData["isProfileCompleted"] == DBNull.Value) ? false : Convert.ToBoolean(myData["isProfileCompleted"]);
                v.AthleteTransition = (myData["AthleteTransition"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteTransition"]);
                v._20YardShuttleRun = (myData["20YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["20YardShuttleRun"];
                v._60YardShuttleRun = (myData["60YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["60YardShuttleRun"];
                v.KneelingMedBallToss = (myData["KneelingMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["KneelingMedBallToss"];
                v.RotationalMedBallToss = (myData["RotationalMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["RotationalMedBallToss"];
                v.CityId = (myData["CityId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CityId"]);
                v.BasicStateId = (myData["BasicStateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["BasicStateId"]);
                v.BasicStateName = (myData["BasicStateName"] == DBNull.Value) ? string.Empty : (string)myData["BasicStateName"];
                v.CityName = (myData["CityName"] == DBNull.Value) ? string.Empty : (string)myData["CityName"];
            }
            return v;
        }


        internal static List<VideoDTO> GetAtheleteIdealVideos2_v1()
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteIdealVideos2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value)
                            ? ""
                            : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : (int)myData["VideoType"];
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : (int)myData["Duration"];
                        v.VideoNumber = (myData["VideoNumber"] == DBNull.Value) ? 0 : (int)myData["VideoNumber"];
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetAtheleteIdealVideos2_v1", "", ex.Message, "Exception", 0);
            }

            return videos;
        }

        internal static List<VideoDTO> GetAtheleteVideos2_v1(string emailaddress, int logintype)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteVideos2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter logintypepar = cmd.Parameters.Add("@logintype", SqlDbType.Int);
                    logintypepar.Direction = ParameterDirection.Input;
                    logintypepar.Value = logintype;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value)
                            ? ""
                            : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : (int)myData["VideoType"];
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : (int)myData["Duration"];
                        v.VideoNumber = (myData["VideoNumber"] == DBNull.Value) ? 0 : (int)myData["VideoNumber"];
                        v.Count = (int)myData["AtheleteVideoCount"];

                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetAtheleteVideos2_v1", "", ex.Message, "Exception", 0);
            }

            return videos;
        }


        internal static List<VideoDTO> GetAtheleteVideosFromAthleteId(int athleteId)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteVideosFromAthleteId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdpar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdpar.Direction = ParameterDirection.Input;
                    athleteIdpar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value)
                            ? ""
                            : Convert.ToString(myData["VideoURL"]).Trim();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["ThumbnailURL"]).Trim();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.VideoType = (myData["VideoType"] == DBNull.Value) ? 0 : (int)myData["VideoType"];
                        v.Duration = (myData["Duration"] == DBNull.Value) ? 0 : (int)myData["Duration"];
                        v.VideoNumber = (myData["VideoNumber"] == DBNull.Value) ? 0 : (int)myData["VideoNumber"];
                        v.Count = (int)myData["AtheleteVideoCount"];

                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetAtheleteVideosFromAthleteId", "", ex.Message, "Exception", 0);
            }
            return videos;
        }

        internal static ClubCoachDTO AddClubCoach(ClubCoachDTO clubCoachDto)
        {
            ClubCoachDTO v = new ClubCoachDTO();
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddClubCoach2_V1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 100);
                    name.Direction = ParameterDirection.Input;
                    name.Value = clubCoachDto.Name;

                    SqlParameter gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    gender.Direction = ParameterDirection.Input;
                    gender.Value = clubCoachDto.Gender;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = clubCoachDto.Emailaddress;

                    SqlParameter description = cmd.Parameters.Add("@Description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = (clubCoachDto.Description == null) ? string.Empty : clubCoachDto.Description;

                    SqlParameter profilePicUrl = cmd.Parameters.Add("@ProfilePicURL", SqlDbType.VarChar, 1000);
                    profilePicUrl.Direction = ParameterDirection.Input;
                    profilePicUrl.Value = clubCoachDto.ProfilePicURL;

                    SqlParameter dob = cmd.Parameters.Add("@DOB", SqlDbType.VarChar, 50);
                    dob.Direction = ParameterDirection.Input;
                    dob.Value = clubCoachDto.DOB;

                    //SqlParameter clubAddress = cmd.Parameters.Add("@ClubAddress", SqlDbType.VarChar, 100);
                    //clubAddress.Direction = ParameterDirection.Input;
                    //clubAddress.Value = clubCoachDto.ClubAddress;

                    SqlParameter facebookId = cmd.Parameters.Add("@FacebookId", SqlDbType.VarChar, 500);
                    facebookId.Direction = ParameterDirection.Input;
                    facebookId.Value = clubCoachDto.FacebookId;

                    SqlParameter coverPicUrl = cmd.Parameters.Add("@CoverPicURL", SqlDbType.VarChar, 1000);
                    coverPicUrl.Direction = ParameterDirection.Input;
                    coverPicUrl.Value = clubCoachDto.CoverPicURL;

                    SqlParameter latitude = cmd.Parameters.Add("@Latitude", SqlDbType.VarChar, 50);
                    latitude.Direction = ParameterDirection.Input;
                    latitude.Value = clubCoachDto.Latitude;

                    SqlParameter longitude = cmd.Parameters.Add("@Longitude", SqlDbType.VarChar, 50);
                    longitude.Direction = ParameterDirection.Input;
                    longitude.Value = clubCoachDto.Longitude;

                    SqlParameter clubName = cmd.Parameters.Add("@ClubName", SqlDbType.VarChar, 100);
                    clubName.Direction = ParameterDirection.Input;
                    clubName.Value = string.IsNullOrWhiteSpace(clubCoachDto.ClubName)
                        ? string.Empty
                        : clubCoachDto.ClubName;

                    SqlParameter active = cmd.Parameters.Add("@Active", SqlDbType.Bit);
                    active.Direction = ParameterDirection.Input;
                    active.Value = clubCoachDto.Active;

                    SqlParameter phonenumber = cmd.Parameters.Add("@Phonenumber", SqlDbType.VarChar, 50);
                    phonenumber.Direction = ParameterDirection.Input;
                    phonenumber.Value = string.IsNullOrWhiteSpace(clubCoachDto.Phonenumber)
                        ? string.Empty
                        : clubCoachDto.Phonenumber;

                    SqlParameter deviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    deviceType.Direction = ParameterDirection.Input;
                    deviceType.Value = (clubCoachDto.DeviceType == null) ? string.Empty : clubCoachDto.DeviceType;

                    SqlParameter deviceId = cmd.Parameters.Add("@DeviceId", SqlDbType.VarChar, 50);
                    deviceId.Direction = ParameterDirection.Input;
                    deviceId.Value = (clubCoachDto.DeviceId == null) ? string.Empty : clubCoachDto.DeviceId;

                    SqlParameter teamType = cmd.Parameters.Add("@TeamType", SqlDbType.Int);
                    teamType.Direction = ParameterDirection.Input;
                    teamType.Value = (clubCoachDto.TeamType == null) ? 0 : Convert.ToInt32(clubCoachDto.TeamType);

                    SqlParameter location = cmd.Parameters.Add("@Location", SqlDbType.VarChar, 200);
                    location.Direction = ParameterDirection.Input;
                    location.Value = (clubCoachDto.Location == null) ? string.Empty : clubCoachDto.Location;

                    SqlParameter schoolName = cmd.Parameters.Add("@SchoolName", SqlDbType.VarChar, 50);
                    schoolName.Direction = ParameterDirection.Input;
                    schoolName.Value = (clubCoachDto.SchoolName == null) ? string.Empty : clubCoachDto.SchoolName;

                    SqlParameter zipCode = cmd.Parameters.Add("@ZipCode", SqlDbType.VarChar, 50);
                    zipCode.Direction = ParameterDirection.Input;
                    zipCode.Value = (clubCoachDto.ZipCode == null) ? string.Empty : clubCoachDto.ZipCode;

                    SqlParameter stateId = cmd.Parameters.Add("@StateId", SqlDbType.Int);
                    stateId.Direction = ParameterDirection.Input;
                    stateId.Value = (clubCoachDto.StateId == null) ? 0 : Convert.ToInt32(clubCoachDto.StateId);

                    SqlParameter schoolType = cmd.Parameters.Add("@SchoolType", SqlDbType.Int);
                    schoolType.Direction = ParameterDirection.Input;
                    schoolType.Value = (clubCoachDto.SchoolType == null) ? 0 : Convert.ToInt32(clubCoachDto.SchoolType);

                    SqlParameter clubPhoneNumber = cmd.Parameters.Add("@ClubPhoneNumber", SqlDbType.VarChar, 50);
                    clubPhoneNumber.Direction = ParameterDirection.Input;
                    clubPhoneNumber.Value = string.IsNullOrWhiteSpace(clubCoachDto.ClubPhoneNumber)
                        ? string.Empty
                        : clubCoachDto.ClubPhoneNumber;

                    SqlParameter schoolId = cmd.Parameters.Add("@SchoolId", SqlDbType.Int);
                    schoolId.Direction = ParameterDirection.Input;
                    schoolId.Value = (clubCoachDto.SchoolId == null) ? 0 : Convert.ToInt32(clubCoachDto.SchoolId);

                    SqlParameter idPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    idPar.Direction = ParameterDirection.Output;

                    SqlDataReader myData = cmd.ExecuteReader();

                    v = GetClubCoach(myData);
                    //v.DeviceId = clubCoachDto.DeviceId;
                    //v.DeviceType = clubCoachDto.DeviceType;

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/AddClubCoach", "", ex.Message, "Exception", 0);
            }

            return v;
        }
        
        internal static ClubCoachDTO GetClubCoachProfile(string emailaddress)
        {
            ClubCoachDTO v = new ClubCoachDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetClubCoachProfile]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    v = GetClubCoach(myData);
                   

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetClubCoachProfile", "", ex.Message, "Exception", 0);
            }


            return v;
        }

        internal static ClubCoachDTO GetClubCoach(SqlDataReader myData)
        {
            ClubCoachDTO v = new ClubCoachDTO();

            while (myData.Read())
            {

                v.Id = (int)myData["id"];
                v.Name = myData["name"].ToString();
                v.Description = myData["description"].ToString();
                v.Emailaddress = myData["emailaddress"].ToString();
                v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                v.FacebookId = myData["FacebookId"].ToString();
                v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                v.DOB = myData["DOB"].ToString();
                v.ClubAddress = string.Empty;
                v.ClubName = myData["ClubName"].ToString();
                v.Latitude = myData["Latitude"].ToString();
                v.Longitude = myData["Longitude"].ToString();
                v.Active = (myData["Active"] != DBNull.Value) && Convert.ToBoolean(myData["Active"]);
                v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                v.TeamType = (myData["TeamType"] == DBNull.Value) ? 0 : (int)myData["TeamType"];
                v.Location = myData["Location"].ToString();
                v.SchoolName = myData["SchoolName"].ToString();
                v.ZipCode = myData["ZipCode"].ToString();
                v.StateId = myData["StateId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["StateId"]);
                v.StateName = myData["StateName"].ToString();
                v.SchoolType = myData["SchoolType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolType"]);
                v.ClubPhoneNumber = (myData["ClubPhoneNumber"] == DBNull.Value) ? string.Empty : myData["ClubPhoneNumber"].ToString();
                v.SchoolId = myData["SchoolId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["SchoolId"]);
                v.ClubId = (myData["ClubId"] == DBNull.Value) ? 0 : (int)myData["ClubId"]; 
                v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["LoginType"]);
                v.Username = (myData["Username"] == DBNull.Value) ? string.Empty : (string)myData["Username"];
                v.DeviceId = (myData["DeviceName"] == DBNull.Value) ? string.Empty : (string)myData["DeviceName"];
                v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"];
                v.Almamater = (myData["Almamater"] == DBNull.Value) ? string.Empty : (string)myData["Almamater"];
                v.TrainedSport = (myData["TrainedSport"] == DBNull.Value) ? string.Empty : (string)myData["TrainedSport"];

            }

            return v;
        }
         
        internal static int AddCoachFilters(CoachFiltersDto coachFilters)
        {
            int coach = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoachFilters]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter coachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachId.Direction = ParameterDirection.Input;
                    coachId.Value = coachFilters.CoachId;

                    SqlParameter academicRate = cmd.Parameters.Add("@AcademicRate", SqlDbType.Int);
                    academicRate.Direction = ParameterDirection.Input;
                    academicRate.Value = coachFilters.AcademicRate;

                    SqlParameter preferredStrokes = cmd.Parameters.Add("@PreferredStrokes", SqlDbType.VarChar, 500);
                    preferredStrokes.Direction = ParameterDirection.Input;
                    preferredStrokes.Value = coachFilters.PreferredStrokes ?? string.Empty;

                    SqlParameter year = cmd.Parameters.Add("@Year", SqlDbType.Int);
                    year.Direction = ParameterDirection.Input;
                    year.Value = coachFilters.HsYear;

                    SqlParameter gender = cmd.Parameters.Add("@Gender", SqlDbType.Int);
                    gender.Direction = ParameterDirection.Input;
                    gender.Value = coachFilters.Gender;

                    SqlParameter location = cmd.Parameters.Add("@Location", SqlDbType.VarChar, 10);
                    location.Direction = ParameterDirection.Input;
                    location.Value = Convert.ToInt32(coachFilters.Location);

                    SqlParameter athleteRate = cmd.Parameters.Add("@AthleteRate", SqlDbType.Int);
                    athleteRate.Direction = ParameterDirection.Input;
                    athleteRate.Value = Convert.ToInt32(coachFilters.AthleteRate);

                    //New in test
                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        coach = (int)myData["CoachId"];


                    }
                    myData.Close();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/AddCoachFilters", "", ex.Message, "Exception", 0);
            }


            return coach;
        }

        internal static int AddCoachVideos(VideoDTO videoDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoachVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    name.Direction = ParameterDirection.Input;
                    name.Value = videoDto.Name;

                    SqlParameter videoUrl = cmd.Parameters.Add("@VideoURL", SqlDbType.VarChar, 1024);
                    videoUrl.Direction = ParameterDirection.Input;
                    videoUrl.Value = videoDto.VideoURL;

                    SqlParameter thumbnailUrl = cmd.Parameters.Add("@ThumbnailURL", SqlDbType.VarChar, 1024);
                    thumbnailUrl.Direction = ParameterDirection.Input;
                    thumbnailUrl.Value = videoDto.ThumbnailURL;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    status.Direction = ParameterDirection.Input;
                    status.Value = videoDto.Status;

                    SqlParameter coachId = cmd.Parameters.Add("@CoachId", SqlDbType.VarChar, 100);
                    coachId.Direction = ParameterDirection.Input;
                    coachId.Value = videoDto.CoachId;

                    SqlParameter videoFormat = cmd.Parameters.Add("@VideoFormat", SqlDbType.VarChar, 50);
                    videoFormat.Direction = ParameterDirection.Input;
                    videoFormat.Value = videoDto.VideoFormat;

                    SqlParameter videoNumber = cmd.Parameters.Add("@videoNumber", SqlDbType.Int);
                    videoNumber.Direction = ParameterDirection.Input;
                    videoNumber.Value = videoDto.VideoNumber;

                    SqlParameter duration = cmd.Parameters.Add("@duration", SqlDbType.VarChar, 50);
                    duration.Direction = ParameterDirection.Input;
                    duration.Value = videoDto.Duration;

                    SqlParameter idPar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    idPar.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;

                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/AddCoachVideos", "", ex.Message, "Exception", 0);
            }

            return id;
        }
         
        internal static CoachProfileDTO GetCoachProfile(int coachId)
        {
            CoachProfileDTO v = new CoachProfileDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachProfile2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = coachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.CoachId = (int)myData["ID"];
                        v.Description = (myData["description"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["description"].ToString();
                        v.Emailaddress = (myData["emailaddress"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["emailaddress"].ToString();
                        int Head = (int)myData["HeadCoach"];
                        v.HeadCoach = (Head == 1) ? true : false;
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["Phonenumber"];
                        v.payment_paid = (myData["Payment_Paid"] == DBNull.Value)
                            ? false
                            : (bool)myData["Payment_Paid"];
                        v.Payment_Paid = (myData["Payment_Paid"] == DBNull.Value)
                         ? false
                         : (bool)myData["Payment_Paid"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.FacebookURL = (myData["FacebookURL"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["FacebookURL"];
                        v.TwitterURL = (myData["TwitterURL"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["TwitterURL"];
                        v.InstagramURL = (myData["InstagramURL"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["InstagramURL"];
                        v.YoutubeURL = (myData["YoutubeURL"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["YoutubeURL"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? "" : (string)myData["CoachName"];
                        v.Title = (myData["Title"] == DBNull.Value) ? "" : (string)myData["Title"];
                        v.Phone1 = (myData["Phone1"] == DBNull.Value) ? "" : (string)myData["Phone1"];
                        v.Phone2 = (myData["Phone2"] == DBNull.Value) ? "" : (string)myData["Phone2"];
                        v.Accolades = (myData["Accolades"] == DBNull.Value) ? "" : (string)myData["Accolades"];
                        v.CollegeName = (myData["CollegeName"] == DBNull.Value) ? "" : (string)myData["CollegeName"];
                        v.Year = (myData["Year"] == DBNull.Value) ? 0 : (int)myData["Year"];
                        v.InstituteName = (myData["name"] == DBNull.Value) ? "" : (string)myData["name"];
                        v.InstituteState = (myData["State"] == DBNull.Value) ? "" : (string)myData["State"];
                        v.InstituteCity = (myData["City"] == DBNull.Value) ? "" : (string)myData["City"];
                        v.AcademicRate = (myData["CoachAcademicRate"] == DBNull.Value) ? 0 : (int)myData["CoachAcademicRate"];
                        v.PreferredStrokes = (myData["CoachPreferredStrokes"] == DBNull.Value)
                           ? string.Empty
                           : myData["CoachPreferredStrokes"].ToString();
                        v.HsYear = (myData["HSYear"] == DBNull.Value) ? 0 : (int)myData["HSYear"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.Location = (myData["Location"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Location"]);
                        v.AthleteRate = (myData["AthleteRate"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteRate"]);
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value)
                        ? string.Empty
                        : (string)myData["ProfilePicURL"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetCoachProfile", "", ex.Message, "Exception", 0);
            }


            return v;
        }
 
        internal static List<VideoDTO> UpdateCoachVideoThumbnail(ThumbnailDTO thumbnailDto)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                string thumbnailUrl = UsersDal.UploadImageFromBase64(thumbnailDto.ThumbnailURL,
                    thumbnailDto.ThumbnailFormat);

                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachVideoThumbnail]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter thumbnailurlpar = cmd.Parameters.Add("@Thumbnailurl", SqlDbType.VarChar, 1024);
                    thumbnailurlpar.Direction = ParameterDirection.Input;
                    thumbnailurlpar.Value = thumbnailUrl;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = thumbnailDto.CoachId;

                    SqlParameter videoId = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    videoId.Direction = ParameterDirection.Input;
                    videoId.Value = thumbnailDto.VideoId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : myData["VideoURL"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value)
                            ? ""
                            : myData["ThumbnailURL"].ToString();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? "" : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        v.Count = (int)myData["CoachVideoCount"];
                        videos.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/UpdateCoachVideoThumbnail", "", ex.Message, "Exception", 0);
            }


            return videos;
        }
 
        internal static List<VideoDTO> GetCoachVideos(int coachId)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachVideos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@coachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = coachId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : myData["VideoURL"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value)
                            ? ""
                            : myData["ThumbnailURL"].ToString();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? "" : (string)myData["VideoFormat"];
                        v.Id = Convert.ToInt32(myData["Id"]);
                        v.Count = Convert.ToInt32(myData["CoachVideoCount"]);
                        v.VideoType = Convert.ToInt32(myData["VideoPhotoType"]);
                        v.Duration = Convert.ToInt32(myData["Duration"]);
                        videos.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetCoachVideos", "", ex.Message, "Exception", 0);
            }


            return videos;
        }
 
        internal static List<VideoDTO> UploadCoachPhotos(ThumbnailDTO thumbnailDto)
        {

            List<VideoDTO> videos = new List<VideoDTO>();

            try
            {
                string thumbnailUrl = UsersDal.UploadImageFromBase64(thumbnailDto.ThumbnailURL,
                    thumbnailDto.ThumbnailFormat);

                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddCoachPhotos]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namepar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 50);
                    namepar.Direction = ParameterDirection.Input;
                    namepar.Value = "Photo_" + thumbnailDto.CoachId;

                    SqlParameter thumbnailurlpar = cmd.Parameters.Add("@Thumbnailurl", SqlDbType.VarChar, 1024);
                    thumbnailurlpar.Direction = ParameterDirection.Input;
                    thumbnailurlpar.Value = thumbnailUrl;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = thumbnailDto.CoachId;

                    SqlParameter thumbnailFormat = cmd.Parameters.Add("@ThumbnailFormat", SqlDbType.VarChar, 50);
                    thumbnailFormat.Direction = ParameterDirection.Input;
                    thumbnailFormat.Value = thumbnailDto.ThumbnailFormat;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        VideoDTO v = new VideoDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : myData["Name"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : myData["VideoURL"].ToString();
                        v.ThumbnailURL = (myData["ThumbnailURL"] == DBNull.Value)
                            ? ""
                            : myData["ThumbnailURL"].ToString();
                        v.VideoFormat = (myData["VideoFormat"] == DBNull.Value) ? "" : (string)myData["VideoFormat"];
                        v.Id = (int)myData["Id"];
                        videos.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/UploadCoachPhotos", "", ex.Message, "Exception", 0);
            }


            return videos;
        }
 
        internal static int UpdateCoachProfile(CoachProfileDTO coachProfileDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateCoachProfile2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachid = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachid.Direction = ParameterDirection.Input;
                    coachid.Value = coachProfileDto.CoachId;

                    SqlParameter description = cmd.Parameters.Add("@description", SqlDbType.VarChar, 1000);
                    description.Direction = ParameterDirection.Input;
                    description.Value = coachProfileDto.Description;

                    SqlParameter facebookUrl = cmd.Parameters.Add("@FacebookURL", SqlDbType.VarChar, 2000);
                    facebookUrl.Direction = ParameterDirection.Input;
                    facebookUrl.Value = coachProfileDto.FacebookURL;

                    SqlParameter twitterUrl = cmd.Parameters.Add("@TwitterURL", SqlDbType.VarChar, 2000);
                    twitterUrl.Direction = ParameterDirection.Input;
                    twitterUrl.Value = coachProfileDto.TwitterURL;

                    SqlParameter instagramUrl = cmd.Parameters.Add("@InstagramURL", SqlDbType.VarChar, 2000);
                    instagramUrl.Direction = ParameterDirection.Input;
                    instagramUrl.Value = coachProfileDto.InstagramURL;

                    SqlParameter youtubeUrl = cmd.Parameters.Add("@YoutubeURL", SqlDbType.VarChar, 2000);
                    youtubeUrl.Direction = ParameterDirection.Input;
                    youtubeUrl.Value = coachProfileDto.YoutubeURL;

                    SqlParameter coachName = cmd.Parameters.Add("@CoachName", SqlDbType.VarChar, 100);
                    coachName.Direction = ParameterDirection.Input;
                    coachName.Value = coachProfileDto.CoachName ?? string.Empty;

                    SqlParameter title = cmd.Parameters.Add("@Title", SqlDbType.VarChar, 10);
                    title.Direction = ParameterDirection.Input;
                    title.Value = coachProfileDto.Title ?? string.Empty;

                    SqlParameter phone1 = cmd.Parameters.Add("@Phone1", SqlDbType.VarChar, 20);
                    phone1.Direction = ParameterDirection.Input;
                    phone1.Value = coachProfileDto.Phone1 ?? string.Empty;

                    SqlParameter phone2 = cmd.Parameters.Add("@Phone2", SqlDbType.VarChar, 20);
                    phone2.Direction = ParameterDirection.Input;
                    phone2.Value = coachProfileDto.Phone2 ?? string.Empty;

                    SqlParameter accolades = cmd.Parameters.Add("@Accolades", SqlDbType.VarChar, 4000);
                    accolades.Direction = ParameterDirection.Input;
                    accolades.Value = coachProfileDto.Accolades ?? string.Empty;

                    SqlParameter collegeName = cmd.Parameters.Add("@CollegeName", SqlDbType.VarChar, 200);
                    collegeName.Direction = ParameterDirection.Input;
                    collegeName.Value = coachProfileDto.CollegeName ?? string.Empty;

                    SqlParameter year = cmd.Parameters.Add("@Year", SqlDbType.Int);
                    year.Direction = ParameterDirection.Input;
                    year.Value = coachProfileDto.Year;


                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/UpdateCoachProfile", "", ex.Message, "Exception", 0);
            }


            return id;
        }
 
        internal static List<AthleteDTO> GetAllAthletePagingWithSearch(string emailaddress, int offset, int max,
            string searchText)
        {
            AthleteController atc = new AthleteController();
            List<AthleteDTO> athletes = new List<AthleteDTO>();
            var coaches = UsersDal.GetCoachProfileByEmail(emailaddress);
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllAtheletesByCoachEmail2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;


                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;


                    SqlParameter maxpar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxpar.Direction = ParameterDirection.Input;
                    maxpar.Value = max;

                    SqlParameter searchTextPar = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchTextPar.Direction = ParameterDirection.Input;
                    searchTextPar.Value = searchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value)
                            ? 0
                            : Convert.ToInt32(myData["AcademicLevel"]);
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["YearLevel"]);
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value)
                            ? 0
                            : Convert.ToInt32(myData["AtheleteRate"]);
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Premium"]);
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["ShoeSizeType"];
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["Phonenumber"];
                        v.RightSwiped = ((int)myData["RightSwiped"] == 0) ? false : true;
                     
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["StateName"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["BroadJump"];
                        v.Strokes = (myData["Strokes"] == DBNull.Value) ? string.Empty : (string)myData["Strokes"];
                        v.Location = (myData["Location"] == DBNull.Value) ? 0 : (int)myData["Location"];

                        v.SchoolCoachName = (myData["SchoolCoachName"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachName"];
                        v.SchoolCoachEmail = (myData["SchoolCoachEmail"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachEmail"];
                        v.SchoolZipCode = (myData["SchoolZipCode"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolZipCode"];
                        v.GapyearLevel = (myData["GapyearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["GapyearLevel"]);
                        v.GapyearDescription = (myData["GapyearDescription"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["GapyearDescription"];
                        v.Country = (myData["Country"] == DBNull.Value) ? string.Empty : (string)myData["Country"];
                        v.CountryId = (myData["CountryId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CountryId"]);
                        v.AP = (myData["AP"] == DBNull.Value) ? 0.0 : Convert.ToInt32(myData["AP"]);

                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        //  v.AthleteVideosCount = (myData["AthleteVideosCount"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteVideosCount"]);
                        int measurementcount = atc.GetMeasurementCount(v.Height, v.Height2, v.Weight, v.WingSpan, v.ShoeSize);
                        v.MeasurementCount = measurementcount;
                        string email = string.Empty;
                        email = (v.LoginType == 1 || v.LoginType == 4) ? v.Emailaddress : v.UserName;
                        v.MultiSportCount = GetMultiSportCount(email, v.LoginType, Lemonaid_web_api.Utility.UserTypes.HighSchooAthlete);
                        v._20YardShuttleRun = (myData["20YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["20YardShuttleRun"];
                        v._60YardShuttleRun = (myData["60YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["60YardShuttleRun"];
                        v.KneelingMedBallToss = (myData["KneelingMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["KneelingMedBallToss"];
                        v.RotationalMedBallToss = (myData["RotationalMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["RotationalMedBallToss"];
                    
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetAllAthletePagingWithSearch", "", ex.Message, "Exception", 0);
            }

            return athletes;
        }

        public static int GetMultiSportCount(string Emailaddress, int LoginType, string UserType)
        {
            List<SportsDTO> sports = new List<SportsDTO>();
            string baseUrl = ConfigurationManager.AppSettings["WrapperBaseURL"];
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("Login/GetSportsFromEmail?emailaddress=" + Emailaddress + "&loginType=" + LoginType + "&userType=" + UserType).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res1 = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                sports = JsonDeserialize<List<SportsDTO>>(res1.Response.ToString());

            }
            return sports.Count;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        public static List<SportsDTO> GetMultiSports(string Emailaddress, int LoginType, string UserType)
        {
            List<SportsDTO> sports = new List<SportsDTO>();
            string baseUrl = ConfigurationManager.AppSettings["WrapperBaseURL"];
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = client.GetAsync("Login/GetSportsFromEmail?emailaddress=" + Emailaddress + "&loginType=" + LoginType + "&userType=" + UserType).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res1 = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                sports = JsonDeserialize<List<SportsDTO>>(res1.Response.ToString());

            }
            return sports;
        }

        internal static List<int> GetUniversityCoaches(int instituteId)
        {
            List<int> coachIds = new List<int>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUniversityCoaches]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter instituteIdPar = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    instituteIdPar.Direction = ParameterDirection.Input;
                    instituteIdPar.Value = instituteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        var v = (int)myData["Id"];
                        coachIds.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetUniversityCoaches", "", ex.Message, "Exception", 0);
            }


            return coachIds;
        }

        internal static void DeleteCoachVideo(int coachId, int videoId)
        {
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCoachVideo]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter coachIdPar = cmd.Parameters.Add("@CoachId", SqlDbType.VarChar, 100);
                    coachIdPar.Direction = ParameterDirection.Input;
                    coachIdPar.Value = coachId;

                    SqlParameter videoIdPar = cmd.Parameters.Add("@VideoId", SqlDbType.Int);
                    videoIdPar.Direction = ParameterDirection.Input;
                    videoIdPar.Value = videoId;

                    cmd.ExecuteNonQuery();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/DeleteCoachVideo", "", ex.Message, "Exception", 0);
            }



        } 

        internal static List<HelpDTO> GetFAQs(int typeid, string userType)
        {

            List<HelpDTO> HelpDTO = new List<HelpDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFAQsHelp2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter type = cmd.Parameters.Add("@type", SqlDbType.Int);
                    type.Direction = ParameterDirection.Input;
                    type.Value = typeid;

                    SqlParameter userTypePar = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 20);
                    userTypePar.Direction = ParameterDirection.Input;
                    userTypePar.Value = userType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        HelpDTO v = new HelpDTO();
                        v.Question = (string)myData["Question"].ToString();
                        v.Answer = (string)myData["Answer"].ToString();
                        v.Date = (DateTime)myData["CreatedDate"];
                        v.ThumbNailURL = (string)myData["ThumbnailURL"].ToString();
                        v.VideoType = (int)myData["VideoType"];
                        HelpDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetFAQs", "", ex.Message, "Exception", 0);
            }


            return HelpDTO;
        }


        internal static List<CountryDTO> GetCountries()
        {

            List<CountryDTO> Countries = new List<CountryDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCountry]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CountryDTO v = new CountryDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        Countries.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetCountries", "", ex.Message, "Exception", 0);
                throw;
            }

            return Countries;
        }

        internal static string GetConnection(int sportId)
        {
            string connString = string.Empty;
            var envirmt = ConfigurationManager.AppSettings["Environment"].ToString();
            if (envirmt == "1") 
                connString = GetTestConnection(sportId);
            else if (envirmt == "2") 
                    connString = GetStagingConnection(sportId); 
                else 
                    connString = GetProductionConnection(sportId);
               
        
            return connString;
        }

        internal static int AddPaymentForAthlete(PaymentDTO payment, int sportId)
        {

            int id = 0;
            try
            { 
                string connString = GetConnection(sportId);
                SqlConnection myConn = new SqlConnection(connString); 
                myConn.Open();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddPayment2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = payment.UserID;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.Int);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = payment.UserType;

                    SqlParameter PaymentToken = cmd.Parameters.Add("@PaymentToken", SqlDbType.VarChar, 8000);
                    PaymentToken.Direction = ParameterDirection.Input;
                    PaymentToken.Value = payment.PaymentToken;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = payment.DeviceType;

                    SqlParameter PaymentType = cmd.Parameters.Add("@PaymentType", SqlDbType.VarChar, 50);
                    PaymentType.Direction = ParameterDirection.Input;
                    PaymentType.Value = payment.PaymentType;

                    SqlParameter subscriptionType = cmd.Parameters.Add("@SubscriptionType", System.Data.SqlDbType.VarChar, 20);
                    subscriptionType.Direction = System.Data.ParameterDirection.Input;
                    subscriptionType.Value = (payment.SubscriptionType == null) ? string.Empty : payment.SubscriptionType;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = payment.Emailaddress;

                    SqlParameter SKUIdPar = cmd.Parameters.Add("@SKUId", SqlDbType.VarChar, 100);
                    SKUIdPar.Direction = ParameterDirection.Input;
                    SKUIdPar.Value = (payment.SKUId == null) ? string.Empty : payment.SKUId; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
            }
 
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/AddPayment", "", ex.Message, "Exception", 0);
                throw;
            }
            return id;
        }

        internal static PaymentDTO GetPaymentDetailsForAthlete(string emailaddress, int sportId)
        {
            PaymentDTO payment = new PaymentDTO();

            try
            {
                string connString = GetConnection(sportId);
                SqlConnection myConn = new SqlConnection(connString);
                myConn.Open();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPaymentDetails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddresspar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddresspar.Direction = ParameterDirection.Input;
                    emailaddresspar.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        payment.UserID = (int)myData["UserID"];
                        payment.UserType = (int)myData["UserType"];
                        payment.PaymentToken = (string)myData["PaymentToken"];
                        payment.DeviceType = (string)myData["DeviceType"];
                        payment.PaymentType = (string)myData["PaymentType"];
                        payment.CreatedDate = (DateTime)myData["CreatedDate"];
                        payment.ExpiryDate = Convert.ToDateTime(myData["ExpiryDate"]);
                        payment.Emailaddress = emailaddress;
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetPaymentDetailsForAthlete", "", ex.Message, "Exception", 0);
                throw;
            }

            return payment;
        }

        internal static int DeleteCollegeAthlete(int sportId, string emailaddress, int loginType)
        {
            int athleteId = 0;

            try
            {
                string connString = GetConnection(sportId);
                SqlConnection myConn = new SqlConnection(connString);
                myConn.Open();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCollegeAthlete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter EmailaddressPar = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    EmailaddressPar.Direction = ParameterDirection.Input;
                    EmailaddressPar.Value = emailaddress;

                    SqlParameter loginTypePar = cmd.Parameters.Add("@loginType", SqlDbType.Int);
                    loginTypePar.Direction = ParameterDirection.Input;
                    loginTypePar.Value = loginType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        athleteId = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/DeleteCollegeAthlete", "", ex.Message, "Exception", 0);
                throw;
            }

            return athleteId;
        }


        internal static int AddPayment(PaymentDTO payment)
        {

            int id = 0;
            try
            {

                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddPayment2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter UserID = cmd.Parameters.Add("@UserID", SqlDbType.Int);
                    UserID.Direction = ParameterDirection.Input;
                    UserID.Value = payment.UserID;

                    SqlParameter UserType = cmd.Parameters.Add("@UserType", SqlDbType.Int);
                    UserType.Direction = ParameterDirection.Input;
                    UserType.Value = payment.UserType;

                    SqlParameter PaymentToken = cmd.Parameters.Add("@PaymentToken", SqlDbType.VarChar, 8000);
                    PaymentToken.Direction = ParameterDirection.Input;
                    PaymentToken.Value = payment.PaymentToken;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", SqlDbType.VarChar, 50);
                    DeviceType.Direction = ParameterDirection.Input;
                    DeviceType.Value = payment.DeviceType;

                    SqlParameter PaymentType = cmd.Parameters.Add("@PaymentType", SqlDbType.VarChar, 50);
                    PaymentType.Direction = ParameterDirection.Input;
                    PaymentType.Value = payment.PaymentType;

                    SqlParameter subscriptionType = cmd.Parameters.Add("@SubscriptionType", System.Data.SqlDbType.VarChar, 20);
                    subscriptionType.Direction = System.Data.ParameterDirection.Input;
                    subscriptionType.Value = (payment.SubscriptionType == null) ? string.Empty : payment.SubscriptionType;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = payment.Emailaddress;

                    SqlParameter SKUIdPar = cmd.Parameters.Add("@SKUId", SqlDbType.VarChar, 100);
                    SKUIdPar.Direction = ParameterDirection.Input;
                    SKUIdPar.Value = (payment.SKUId == null) ? string.Empty : payment.SKUId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = (int)myData["ID"];

                    }

                    myConn.Close();
                    myData.Close();
                }
           }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/AddPaymentForAthlete", "", ex.Message, "Exception", 0);
                throw;
            }
            return id;
         }


   

        internal static List<SchoolsDTO> GetSchools()
        {

            List<SchoolsDTO> Schools = new List<SchoolsDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSchools]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SchoolsDTO v = new SchoolsDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        Schools.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetSchools", "", ex.Message, "Exception", 0);
                throw;
            }

            return Schools;
        }



        internal static AthleteDTO GetAthleteProfileById(int athleteId)
        {
            AthleteDTO athlete = new AthleteDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteProfileById]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        athlete.Id = (int)myData["id"];
                        athlete.Emailaddress = (myData["Emailaddress"] == DBNull.Value) ? string.Empty : (string)myData["Emailaddress"].ToString();
                        athlete.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : (int)myData["AtheleteType"];
                        athlete.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        athlete.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"].ToString();
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetAthleteProfileById", "", ex.Message, "Exception", 0);
                throw;
            }

            return athlete;

      }

        internal static int DeleteCollegeAthleteInWrapper(string emailaddress, int sportId, int loginType)
        {
            int userId = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteCollegeAthlete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter EmailaddressPar = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    EmailaddressPar.Direction = ParameterDirection.Input;
                    EmailaddressPar.Value = emailaddress;

                    SqlParameter sportIdPar = cmd.Parameters.Add("@sportId", SqlDbType.Int);
                    sportIdPar.Direction = ParameterDirection.Input;
                    sportIdPar.Value = sportId;

                    SqlParameter loginTypePar = cmd.Parameters.Add("@loginType", SqlDbType.Int);
                    loginTypePar.Direction = ParameterDirection.Input;
                    loginTypePar.Value = loginType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        userId = (int)myData["Id"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/DeleteCollegeAthleteInWrapper", "", ex.Message, "Exception", 0);
                throw;
            }

            return userId;
        }

       

        internal static int UpdateAtheleteTransition(int AtheleteId, int Atheletetransition)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateAtheleteTransition]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteId = cmd.Parameters.Add("@AtheleteId", SqlDbType.Int);
                    atheleteId.Direction = ParameterDirection.Input;
                    atheleteId.Value = AtheleteId;

                    SqlParameter Atheletetransitionpar = cmd.Parameters.Add("@Atheletetransition", SqlDbType.Int);
                    Atheletetransitionpar.Direction = ParameterDirection.Input;
                    Atheletetransitionpar.Value = Atheletetransition;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["Id"];
                    }


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/UpdateAtheleteTransition", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static string GetTestConnection(int sportId)
        {
            string connectionString = string.Empty;

            switch (sportId)
            {
                case 1: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBizTest;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 2: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidRowingTest;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 3: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidDivingTest;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 4: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTennisStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 5: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTrackStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 6: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidLacrosseStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 7: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidGolfStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 8: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWaterPoloStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 9: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidFootballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 10: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBasketballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 11: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBaseballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 12: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoccerStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 13: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoftBallStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 14: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrestlingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 15: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidIceHockeyStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 16: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidVolleyBallStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                default: return "0";
                    break;
            }
        }

        internal static string GetStagingConnection(int sportId)
        {
            switch (sportId)
            {
                case 1: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 2: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidRowingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 3: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidDivingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 4: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTennisStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 5: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTrackStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 6: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidLacrosseStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 7: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidGolfStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 8: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWaterPoloStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 9: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidFootballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 10: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBasketballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 11: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBaseballStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 12:
                    return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoccerStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 13:
                    return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoftBallStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 14:
                    return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrestlingStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 15:
                    return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidIceHockeyStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 16:
                    return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidVolleyBallStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                default: return "0";
                    break;
            }
        }

        internal static string GetProductionConnection(int sportId)
        {
            switch (sportId)
            {
                case 1: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=Lemonaidbiz;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 2: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidRowing;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 3: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidDiving;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 4: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTennis;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 5: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidTrack;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 6: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidLacrosse;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 7: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidGolf;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 8: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWaterPolo;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 9: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidFootball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 10: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBasketball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 11: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidBaseball;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 12: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoccer;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 13: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidSoftBall;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 14: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrestling;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 15: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidIceHockey;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                case 16: return "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidVolleyBall;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                    break;
                default: return "0";
                    break;
            }
        }

        internal static AnalyticsDTO GetCoachAnalyticsData(AnalyticsDTO analyticsDTO)
        {
            AnalyticsDTO analytics  = new AnalyticsDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetCoachAnalyticsData]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = analyticsDTO.Emailaddress;

                    SqlParameter coachId = cmd.Parameters.Add("@CoachId", SqlDbType.Int);
                    coachId.Direction = ParameterDirection.Input;
                    coachId.Value = analyticsDTO.CoachId;

                    SqlParameter month = cmd.Parameters.Add("@month", SqlDbType.Int);
                    month.Direction = ParameterDirection.Input;
                    month.Value = analyticsDTO.MonthId;

                    SqlParameter year = cmd.Parameters.Add("@year", SqlDbType.Int);
                    year.Direction = ParameterDirection.Input;
                    year.Value = analyticsDTO.YearId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        analytics.ProfileViews = (int)myData["ProfileViews"];
                        analytics.ProfileViewsPercentage = (int)myData["ProfileViewsPercentage"];
                        analytics.Matches = (int)myData["Matches"];
                        analytics.MatchesPercentage = (int)myData["MatchesPercentage"];
                        analytics.RightSwipesGiven = (int)myData["RightSwipesGiven"];
                        analytics.RightSwipesGivenPercentage = (int)myData["RightSwipesGivenPercentage"];
                        analytics.RightSwipesReceived = (int)myData["RightSwipesReceived"];
                        analytics.RightSwipesReceivedPercentage = (int)myData["RightSwipesReceivedPercentage"];
                        analytics.Rank = (int)myData["CoachRank"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetCoachAnalyticsData", "", ex.Message, "Exception", 0);
                throw;
            }

            return analytics;

        }  

        internal static AnalyticsDTO GetAthleteAnalyticsData(AnalyticsDTO analyticsDTO)
        {
            AnalyticsDTO analytics = new AnalyticsDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteAnalyticsData]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    emailaddress.Direction = ParameterDirection.Input;
                    emailaddress.Value = analyticsDTO.Emailaddress;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = analyticsDTO.AthleteId;

                    SqlParameter month = cmd.Parameters.Add("@month", SqlDbType.Int);
                    month.Direction = ParameterDirection.Input;
                    month.Value = analyticsDTO.MonthId;

                    SqlParameter year = cmd.Parameters.Add("@year", SqlDbType.Int);
                    year.Direction = ParameterDirection.Input;
                    year.Value = analyticsDTO.YearId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        analytics.ProfileViews = (int)myData["ProfileViews"];
                        analytics.ProfileViewsPercentage = (int)myData["ProfileViewsPercentage"];
                        analytics.Matches = (int)myData["Matches"];
                        analytics.MatchesPercentage = (int)myData["MatchesPercentage"];
                        analytics.RightSwipesGiven = (int)myData["RightSwipesGiven"];
                        analytics.RightSwipesGivenPercentage = (int)myData["RightSwipesGivenPercentage"];
                        analytics.RightSwipesReceived = (int)myData["RightSwipesReceived"];
                        analytics.RightSwipesReceivedPercentage = (int)myData["RightSwipesReceivedPercentage"];
                        analytics.Rank = (int)myData["AthleteRank"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetAthleteAnalyticsData", "", ex.Message, "Exception", 0);
                throw;
            }

            return analytics;

        }
}
}
       

 