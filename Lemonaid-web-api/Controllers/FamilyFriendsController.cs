﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Runtime.Serialization.Json;
using System.Text; 
using System.IO;

namespace Lemonaid_web_api.Controllers
{
    public class FamilyFriendsController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse GetAthletesFromState(string latitude, string longitude, int offset, int max, string searchText,int familyFriendId)
        {
            AthleteV5Controller athv5 = new AthleteV5Controller();
            AthleteController atc = new AthleteController();
         
            try
            {
                string state;
                state = athv5.GetStateWithLatLong(latitude, longitude);

                List<AthleteDTO> athletes;
                athletes = UsersDalV5.GetAthletesFromState(state, offset, max, searchText, familyFriendId);
                foreach (AthleteDTO t in athletes)
                {
                    t.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(t.Id);
                    t.AtheleteVideos = UsersDalV4.GetAtheleteIdealVideos_v4(t.Emailaddress, t.LoginType);
                    int measurementcount = atc.GetMeasurementCount(t.Height, t.Height2, t.Weight, t.WingSpan, t.ShoeSize);
                    t.MeasurementCount = measurementcount;
                }
               
                if (athletes.Count > 0)
                { 
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athletes;
                _result.Message = CustomConstants.Details_Get_Successfully; 
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = string.Empty;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/GetAthletesFromState", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse AddfamilyFriends(FamilyFriendsDTO familyFriendsDto) 
        {
            try
            {
                if (familyFriendsDto.Id == 0)
                {
                    List<SportsDTO> sports  = AddFFFToWrapperDb(familyFriendsDto);
                }

                var familyFriends = UsersDalV5.AddfamilyFriends(familyFriendsDto);
                if (familyFriends.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = familyFriends;
                    _result.Message = CustomConstants.FamilyFriend_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/AddfamilyFriends", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;
        
        }

        public List<SportsDTO> AddFFFToWrapperDb(FamilyFriendsDTO familyFriendsDto)
        {
            List<SportsDTO> sports = new List<SportsDTO>();

            HttpClient client = new HttpClient();
            string wrapperUrl = ConfigurationManager.AppSettings["WrapperBaseURL"];
            client.BaseAddress = new Uri(wrapperUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            AthleteWrapperDTO athlete = new AthleteWrapperDTO();
            athlete.Emailaddress = familyFriendsDto.Emailaddress;
            if ((familyFriendsDto.LoginType == null) || (familyFriendsDto.LoginType == 0))
            {
                athlete.LoginType = 1;
            }
            else
            {
                athlete.LoginType = familyFriendsDto.LoginType;
            }
            athlete.UserName = familyFriendsDto.Username;
            athlete.UserType = Utility.UserTypes.FamilyFriends;
            athlete.DeviceType = familyFriendsDto.DeviceType;
            athlete.DeviceId = familyFriendsDto.DeviceId;
            athlete.Id = 0;
            var response = client.PostAsJsonAsync("User/AddUser", athlete).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;

                if (Convert.ToInt32(res.Response) > 0)
                {
                    AthleteSportsDTO athleteSports = new AthleteSportsDTO();
                    athleteSports.Id = 0;
                    athleteSports.SportId = familyFriendsDto.SportId;
                    athleteSports.UserId = Convert.ToInt32(res.Response);
                    var response1 = client.PostAsJsonAsync("User/AddUserSport", athleteSports).Result;
                    if (response1.IsSuccessStatusCode)
                    {
                        Utility.CustomResponse res1 = response1.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                        sports = JsonDeserialize<List<SportsDTO>>(res1.Response.ToString());
                    }
                }

            }

            return sports;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        [HttpGet]
        public Utility.CustomResponse GetFamilyFriendsProfile(string emailaddress)
        { 
            try
            { 
                var familyFriends = UsersDalV5.GetFamilyFriendsProfile(emailaddress);
             
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = familyFriends;
                    _result.Message = CustomConstants.Details_Get_Successfully;
               
              
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/GetFamilyFriendsProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse AddFamilyFriendsAthleteMatch(FamilyFriendsMatchDTO familyFriendsMatchDto)
        {
            try
            {

                var matchId = UsersDalV5.AddFamilyFriendsAthleteMatch(familyFriendsMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                  matchId = (familyFriendsMatchDto.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/GetFamilAddFamilyFriendsAthleteMatchyFriendsProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DeleteFamilyFriendsAthleteMatch(FamilyFriendsMatchDTO familyFriendsMatchDto)
        {

            try
            {

                var matchId = UsersDalV5.DeleteFamilyFriendsAthleteMatch(familyFriendsMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/DeleteFamilyFriendsAthleteMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddFamilyFriendsInstituteMatch(FamilyFriendsMatchDTO familyFriendsMatchDto)
        {
            try
            {

                var matchId = UsersDalV5.AddFamilyFriendsInstituteMatches(familyFriendsMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (familyFriendsMatchDto.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/AddFamilyFriendsInstituteMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DeleteFamilyFriendsInstituteMatch(FamilyFriendsMatchDTO familyFriendsMatchDto)
        {

            try
            {

                var matchId = UsersDalV5.DeleteFamilyFriendsInstituteMatches(familyFriendsMatchDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/DeleteFamilyFriendsInstituteMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetFamilyFriendsAtheleteFavourites(int familyFriendId,int offset, int max)
        {
            try
            {
                var athletes = UsersDalV5.GetFamilyFriendsAtheleteFavourites(familyFriendId,offset , max);
                for (int i = 0; i < athletes.Count; i++)
                {
                    athletes[i].AtheleteBesttimes = new List<AthleteEventDTO>();
                    athletes[i].AtheleteVideos = new List<VideoDTO>();                  
                    athletes[i].MeasurementCount = 0;
                }
                for (int i = 0; i < athletes.Count; i++)
                {
                    List<CoachDTO> institutes = UsersDalV5.GetRecommendInstitutesinAthletesbyfff(athletes[i].Id, familyFriendId);
                    athletes[i].RecommendedInstitutes = institutes;
                    athletes[i].RecommendedTotalCount = athletes[i].RecommendedInstitutes.Count;
                }

                if (athletes.Count > 0)
                { 
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athletes;
                _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = string.Empty;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/GetFamilyFriendsAtheleteFavourites", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse GetFamilyFriendsInstituteFavourites(int familyFriendId, int offset, int max)
        {
            try
            {
                var institutes = UsersDalV5.GetFamilyFriendsInstituteFavourites(familyFriendId, offset, max);
                for (int i = 0; i < institutes.Count; i++)
                {
                    List<AthleteDTO> atheletes = UsersDalV5.GetRecommendAthletesinInstitutesbyfff(institutes[i].Id, familyFriendId);
                    institutes[i].RecommendedAtheletes = atheletes;
                }

                if (institutes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = institutes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = string.Empty;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/GetFamilyFriendsInstituteFavourites", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetInstitutesForFamilyFriends(int offset, int max, string searchText, int familyFriendId)
        {
            CoachController coachController = new CoachController();

            try
            {
                var coaches = UsersDalV5.GetInstitutesForFamilyFriends(offset,max,searchText,familyFriendId);
                List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                coaches = coachController.GetAdCoaches(ads, coaches); 

                if (coaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = string.Empty;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/GetInstitutesForFamilyFriends", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse RecommendAthletesToUniversitiesByFff(int athleteId, int familyFriendsId, int instituteId)
        {
            try
            {
                int id = UsersDalV5.RecommendAthletesToUniversitiesByFFF(athleteId, familyFriendsId, instituteId);
                if (id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = id;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/RecommendAthletesToUniversitiesByFff", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        //Decline institutes
        [HttpGet]
        public Utility.CustomResponse DeleteFamilyfriendInstitutes(int familyFriendId)
        {

            try
            {
                var clubCoachId = UsersDalV5.DeleteFamilyfriendInstitutes(familyFriendId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = clubCoachId;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/DeleteFamilyfriendInstitutes", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        //Decline Athletes 
        [HttpGet]
        public Utility.CustomResponse DeleteFamilyfriendAthletes(int familyFriendId)
        {

            try
            {
                var clubCoachId = UsersDalV5.DeleteFamilyfriendAthletes(familyFriendId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = clubCoachId;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/DeleteFamilyfriendAthletes", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
    }
}