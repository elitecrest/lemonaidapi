﻿using System; 
using System.Web.Http;
using Lemonaid_web_api.Models;
 

namespace Lemonaid_web_api.Controllers
{
    public class Coach2V2Controller : ApiController
    {

        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse InsertUniversity(CoachDTO coachDto)
        {

            try
            {

                var coachid = Dal2_v2.InsertUniversity(coachDto);
                if (coachid > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coachid;
                    _result.Message = CustomConstants.University_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        } 

        [HttpPost]
        public Utility.CustomResponse InsertCoach(CoachUserDTO coachuserDto)
        { 
            try
            { 
                var coachid = Dal2_v2.InsertCoachUser(coachuserDto);
                if (coachid > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coachid;
                    _result.Message = CustomConstants.University_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        //[HttpPost]
        //public Utility.CustomResponse ValidateCoach(string emailaddress, string password)
        //{
        //    try
        //    {
        //        var coachid = Dal2_v2.ValidateCoach(emailaddress, password);
        //        if (coachid > 0)
        //        {
        //            _result.Status = Utility.CustomResponseStatus.Successful;
        //            _result.Response = coachid;
        //            _result.Message = CustomConstants.University_Added;
        //        }
        //        else
        //        {
        //            _result.Status = Utility.CustomResponseStatus.UnSuccessful;
        //            _result.Message = CustomConstants.Already_Exists;
        //        }



        //        return _result;
        //    }
        //    catch (Exception ex)
        //    {
        //        _result.Status = Utility.CustomResponseStatus.Exception;
        //        _result.Message = ex.Message;
        //        return _result;
        //    }
        //}


    }
}
