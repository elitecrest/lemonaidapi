﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PushNotifications.PushNotifications;

namespace PushNotifications
{
    public class DAL_Notif
    {
        
        internal static List<UsertypeChatDTO> GetUsersChatAdChatCount(string UserType,string conn)
        {

            List<UsertypeChatDTO> UserTypeChats = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUsersChatAdChatCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usertypeparam = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 50);
                    usertypeparam.Direction = ParameterDirection.Input;
                    usertypeparam.Value = UserType;

                    SqlDataReader myData = cmd.ExecuteReader();
                    UserTypeChats = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/GetUsersChatAdChatCount", "", ex.Message, "Exception", 0);
            }

            return UserTypeChats;
        }

        internal static List<UsertypeChatDTO> GetUsersChatCount(string UserType, string conn)
        {

            List<UsertypeChatDTO> UserTypeChats = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn   = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUsersChatCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter usertypeparam = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 50);
                    usertypeparam.Direction = ParameterDirection.Input;
                    usertypeparam.Value = UserType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    UserTypeChats = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/GetUsersChatCount", "", ex.Message, "Exception", 0);
            }

            return UserTypeChats;
        }

        internal static List<UsertypeChatDTO> GetChatsReader(SqlDataReader myData)
        {
            List<UsertypeChatDTO> chatlist = new List<UsertypeChatDTO>();
            while (myData.Read())
            {
                UsertypeChatDTO Chats = new UsertypeChatDTO();
               // Chats.ID = (myData["id"] == DBNull.Value) ? 0 : (int)myData["id"];
               // Chats.Email = (string)myData["emailaddress"].ToString();
               // Chats.LoginType = (myData["logintype"] == DBNull.Value) ? 0 : (int)myData["logintype"];
               // Chats.UserName = (myData["username"] == DBNull.Value) ? string.Empty : (string)myData["username"].ToString();
                Chats.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"].ToString();
                Chats.DeviceID = (myData["DeviceId"] == DBNull.Value) ? string.Empty : (string)myData["DeviceId"].ToString(); 
                chatlist.Add(Chats);
            }
            return chatlist;
        }

        internal static List<UsertypeChatDTO> GetAthletesProfileReader(SqlDataReader myData)
        {
            List<UsertypeChatDTO> chatlist = new List<UsertypeChatDTO>();
            while (myData.Read())
            {
                UsertypeChatDTO Chats = new UsertypeChatDTO();
                Chats.ID = (myData["id"] == DBNull.Value) ? 0 : (int)myData["id"];
                Chats.Email = (string)myData["emailaddress"].ToString();
                Chats.LoginType = (myData["logintype"] == DBNull.Value) ? 0 : (int)myData["logintype"];
                Chats.UserName = (myData["username"] == DBNull.Value) ? string.Empty : (string)myData["username"].ToString();
                Chats.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : (string)myData["DeviceType"].ToString();
                Chats.DeviceID = (myData["DeviceId"] == DBNull.Value) ? string.Empty : (string)myData["DeviceId"].ToString();
                Chats.CreatedDate = (myData["CreatedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(myData["CreatedDate"].ToString());
                chatlist.Add(Chats);
            }
            return chatlist;
        }

        internal static List<UsertypeChatDTO> GetNoSwipesForAtheleteToInstitute(string conn)
        {

            List<UsertypeChatDTO> NoSwipesforAtheletetoInst = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoSwipesForAtheleteToInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoSwipesforAtheletetoInst = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/GetNoSwipesForAtheleteToInstitute", "", ex.Message, "Exception", 0);
            }

            return NoSwipesforAtheletetoInst;
        }

        internal static List<UsertypeChatDTO> GetNoSwipesForInstituteToAthelete( string conn) 
        {

            List<UsertypeChatDTO> NoSwipesforInsttoAthelete = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoSwipesForInstituteToAthelete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoSwipesforInsttoAthelete = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoSwipesForInstituteToAthelete", "", ex.Message, "Exception", 0);
            }

            return NoSwipesforInsttoAthelete;
        }

        internal static List<UsertypeChatDTO> GetNoSwipesForFFFToAthelete( string conn) 
        {

            List<UsertypeChatDTO> NoSwipesforFFFtoAthelete = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn    = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoSwipesForFFFToAthelete]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoSwipesforFFFtoAthelete = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoSwipesforFFFtoAthelete", "", ex.Message, "Exception", 0);
            }

            return NoSwipesforFFFtoAthelete;
        }

        internal static List<UsertypeChatDTO> GetNoSwipesForFFFToInstitute(string conn)
        {

            List<UsertypeChatDTO> NoSwipesforFFFtoInst = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoSwipesForFFFToInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoSwipesforFFFtoInst = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoSwipesforFFFtoInst", "", ex.Message, "Exception", 0);
            }

            return NoSwipesforFFFtoInst;
        }

        internal static List<UsertypeChatDTO> GetNoSwipesForClubCoachToInstitute(string conn)
        {

            List<UsertypeChatDTO> NoSwipesforClctoInst = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoSwipesForClubCoachToInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoSwipesforClctoInst = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoSwipesforClctoInst", "", ex.Message, "Exception", 0);
            }

            return NoSwipesforClctoInst;
        }

        internal static List<UsertypeChatDTO> GetNoRecomendsForCLCToInstitute(string conn)
        {

            List<UsertypeChatDTO> NoRecomendsforClctoInst = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoRecomendsForCLCToInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoRecomendsforClctoInst = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoRecomendsforClctoInst", "", ex.Message, "Exception", 0);
            }

            return NoRecomendsforClctoInst;
        }

        internal static List<UsertypeChatDTO> GetNoRecomendsForFFFToInstitute(string conn)
        {

            List<UsertypeChatDTO> NoRecomendsforFFFtoInst = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[NoRecomendsForFFFToInstitute]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    NoRecomendsforFFFtoInst = GetChatsReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoRecomendsforFFFtoInst", "", ex.Message, "Exception", 0);
            }

            return NoRecomendsforFFFtoInst;
        }


        internal static void UpdateWorkerDate(string conn,int Id)
        {
             
            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdatepushNotifRunDate]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter PushNotificationId = cmd.Parameters.Add("@PushNotificationId", SqlDbType.Int);
                    PushNotificationId.Direction = ParameterDirection.Input;
                    PushNotificationId.Value = Id;

                   cmd.ExecuteNonQuery(); 

                  
                    myConn.Close(); 
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoRecomendsforFFFtoInst", "", ex.Message, "Exception", 0);
            }

           
        }


        internal static List<PushNotifMessages> GetPushNotifMessages(string conn,int PushNotifId)
        {
           List<PushNotifMessages> msgs = new List<PushNotifMessages>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetPushNotificationMessages]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter PushNotificationIdPar = cmd.Parameters.Add("@PushNotificationId", SqlDbType.Int);
                    PushNotificationIdPar.Direction = ParameterDirection.Input;
                    PushNotificationIdPar.Value = PushNotifId;

                    SqlDataReader myData = cmd.ExecuteReader();

                     while(myData.Read())
                    {
                        PushNotifMessages v = new PushNotifMessages();
                        v.PushNotificationId = (int)myData["ID"];
                        v.Message = (string)myData["Message"];
                        v.LastRunDate = (myData["LastRunDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(myData["LastRunDate"]);
                        msgs.Add(v);

                    }
                       

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                
            }


            return msgs;
        }


        internal static List<UsertypeChatDTO> GetAthleteProfilesNotCompleted(string conn)
        {

            List<UsertypeChatDTO> athletes = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteProfilesNotCompleted]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    athletes = GetAthletesProfileReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoSwipesforClctoInst", "", ex.Message, "Exception", 0);
            }

            return athletes;
        }


        internal static List<UsertypeChatDTO> GetAboutmeProfilesNotCompleted(string conn)
        {

            List<UsertypeChatDTO> athletes = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAboutmeProfilesNotCompleted]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    athletes = GetAthletesProfileReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoSwipesforClctoInst", "", ex.Message, "Exception", 0);
            }

            return athletes;
        }


        internal static List<UsertypeChatDTO> GetAcademicProfilesNotCompleted(string conn)
        {

            List<UsertypeChatDTO> athletes = new List<UsertypeChatDTO>();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAcademicProfilesNotCompleted]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    athletes = GetAthletesProfileReader(myData);
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
             {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoSwipesforClctoInst", "", ex.Message, "Exception", 0);
            }

            return athletes;
        }


        internal static int UpdateAthletePushNotifRunDate(string conn, int Id,int athleteId)
        {
            int ResId = 0;
            try
            {
               
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateAthletePushNotifRunDate]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter PushNotificationId = cmd.Parameters.Add("@PushNotificationId", SqlDbType.Int);
                    PushNotificationId.Direction = ParameterDirection.Input;
                    PushNotificationId.Value = Id;

                    SqlParameter AthleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteIdPar.Direction = ParameterDirection.Input;
                    AthleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();
                    ResId = Convert.ToInt16(myData["Id"]);


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL_Notif/NoRecomendsforFFFtoInst", "", ex.Message, "Exception", 0);
            }

            return ResId;
        }

        internal static PushNotifMessages GetAthletePushNotifLastRunDate(string conn, int PushNotifId, int AthleteId)
        {
            PushNotifMessages v = new PushNotifMessages();

            try
            {
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthletePushNotifLastRunDate]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter PushNotificationIdPar = cmd.Parameters.Add("@PushNotificationId", SqlDbType.Int);
                    PushNotificationIdPar.Direction = ParameterDirection.Input;
                    PushNotificationIdPar.Value = PushNotifId;

                    SqlParameter AthleteIdPar = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    AthleteIdPar.Direction = ParameterDirection.Input;
                    AthleteIdPar.Value = AthleteId;  

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                      
                        v.PushNotificationId = (myData["NotificationId"] == null) ? 0 : (int)myData["NotificationId"];
                        v.LastRunDate = (myData["LastRunDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : Convert.ToDateTime(myData["LastRunDate"]);
                        v.isFirst = (myData["isFirst"] == null) ? 0 : (int)myData["isFirst"];  

                    }


                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }


            return v;
        }
    }
}
