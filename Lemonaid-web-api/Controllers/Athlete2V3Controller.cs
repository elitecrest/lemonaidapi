﻿using System;
using System.Collections.Generic;
using Lemonaid_web_api.Models;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Net;
using Newtonsoft.Json;
using System.Globalization;
using System.Runtime.Serialization.Json;

namespace Lemonaid_web_api.Controllers
{
    public class Athlete2V3Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        public string WrapperApi = ConfigurationManager.AppSettings["WrapperBaseURL"];
        public string TwitterOAuthCustomerKey = ConfigurationManager.AppSettings["TwitterOAuthCustomerKey"];
        public string TwitterOAuthCustomerSecret = ConfigurationManager.AppSettings["TwitterOAuthCustomerSecret"];
        public string IBMUserName = ConfigurationManager.AppSettings["IBMUserName"];
        public string IBMPasword = ConfigurationManager.AppSettings["IBMPasword"];

        [HttpGet]
        public Utility.CustomResponse GetAthleteInstituteMatchStatus(int athleteId, int instituteId)
        {
          
            try
            {
                var status = Dal2_v3.GetAthleteInstituteMatchStatus(athleteId, instituteId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = status;
                _result.Message = CustomConstants.Details_Get_Successfully; 

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse PostAds(AdDTO adDTO)
        {

            try
            {
                //Adtype 1 - WebsiteAd, 5 - Chat Ad
                var Id = Dal2_v3.PostAds(adDTO);
                if (Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = Id;
                    _result.Message = CustomConstants.Ad_Uploaded;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athelete3/PostAds", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


       [HttpPost]
        public Utility.CustomResponse AddAthlete(AthleteDTO athleteDto)
        {
            Athlete2Controller ath2Ctlr = new Athlete2Controller();
            Athlete2V2Controller ath2v2Ctlr = new Athlete2V2Controller();
            try
            {
                if (!athleteDto.ProfilePicURL.StartsWith("http"))
                {
                    athleteDto.ProfilePicURL = UsersDal.UploadImageFromBase64(athleteDto.ProfilePicURL, "png");
                }


                if (athleteDto.Id == 0)
                {
                    PaymentController paymentCtrl = new PaymentController();

                    List<SportsDTO> sports = ath2Ctlr.AddAthleteToWrapperDB(athleteDto);

                    athleteDto = ath2v2Ctlr.CreateUpdateAthlete(athleteDto);

                    var item = sports.Single(x => x.Id == athleteDto.SportId);
                    sports.Remove(item);

                    PaymentDTO payment = new PaymentDTO();

                    string emailUser = string.Empty;
                    if (athleteDto.LoginType == 2 || athleteDto.LoginType == 3)
                    {
                        emailUser = athleteDto.UserName;
                    }
                    else
                    {
                        emailUser = athleteDto.Emailaddress;
                    }

                    if (sports.Count > 0)
                    {
                        foreach (var i in sports)
                        {
                            payment = Dal2.GetPaymentDetailsForAthlete(emailUser, i.Id);
                            if (payment.UserID > 0)
                                break;
                        }
                        if (payment.UserID > 0)
                        {
                            //Add it in Present DB 
                            var apy = Dal2.AddPaymentForAthlete(payment, athleteDto.SportId);
                        }

                    }
                }
                else
                {
                    athleteDto = ath2v2Ctlr.CreateUpdateAthlete(athleteDto);

                }
                if (athleteDto.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteDto;
                    _result.Message = CustomConstants.Athlete_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V3/AddAthlete", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
         

        [HttpGet]
        public Utility.CustomResponse IBMWatson(string Username)
        {

            try
            {
                var twitter = new Twitter
                {
                    OAuthConsumerKey = TwitterOAuthCustomerKey,
                    OAuthConsumerSecret = TwitterOAuthCustomerSecret
                };
                IEnumerable<string> twitts = twitter.GetTwitts(Username, 200).Result;
                if(twitts == null || twitts.ToList().Count == 0)
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoTweets;
                }
                else 
                {
                    var response = IBMWatson();

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = response;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                } 
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message =CustomConstants.Data_Not_Found;
                return _result;
            }

        }


        //public string GetIBMWatson()
        //{ 
        //    String baseURL = "https://gateway.watsonplatform.net/personality-insights/api/v2/profile";
        //    String username = IBMUserName;
        //    String password = IBMPasword;

        //    //Input text file here
        //    string inPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bio.txt");
        //    string text = File.ReadAllText(inPath);
        //    StringBuilder uri = new StringBuilder();
        //    HttpWebRequest profileRequest = (HttpWebRequest)WebRequest.Create(baseURL);
        //    string _auth = string.Format("{0}:{1}", username, password);
        //    string enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(_auth));
        //    string cred = string.Format("{0} {1}", "Basic", enc);
        //    profileRequest.Headers[HttpRequestHeader.Authorization] = cred;
        //    profileRequest.Accept = "application/json";
        //    profileRequest.ContentType = "text/plain";
        //    byte[] bytes = Encoding.UTF8.GetBytes(text);
        //    profileRequest.Method = "POST";
        //    profileRequest.ContentLength = bytes.Length;
        //    using (Stream requeststream = profileRequest.GetRequestStream())
        //    {
        //        requeststream.Write(bytes, 0, bytes.Length);
        //        requeststream.Close();
        //    }
        //    string response;
        //    //string outPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Output.json");
        //  // StreamWriter file = new StreamWriter(outPath);

        //    using (HttpWebResponse webResponse = (HttpWebResponse)profileRequest.GetResponse())
        //    {
        //        using (StreamReader sr = new StreamReader(webResponse.GetResponseStream()))
        //        {
        //            response = sr.ReadToEnd();
        //            ////jsonResponse.InnerText = response.ToString();
        //            //dynamic formatted = JsonConvert.DeserializeObject(response);
        //            //JsonConvert.SerializeObject(formatted, Formatting.Indented);
        //            //file.WriteLine(response);  
        //            sr.Close();
        //        }
        //        System.IO.File.Delete(inPath);
        //        webResponse.Close();
        //    }
        //    return response;
        //}


        public  List<IBMProperties> IBMWatson()
        {
            List<IBMProperties> lstIBMProperties = new List<IBMProperties>();


            String baseURL = "https://gateway.watsonplatform.net/personality-insights/api/v2/profile";
            String username = IBMUserName;
            String password = IBMPasword;

            //Input text file here

            string inPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bio.txt");
            string text = File.ReadAllText(inPath);
            StringBuilder uri = new StringBuilder();
            HttpWebRequest profileRequest = (HttpWebRequest)WebRequest.Create(baseURL);
            string _auth = string.Format("{0}:{1}", username, password);
            string enc = Convert.ToBase64String(Encoding.ASCII.GetBytes(_auth));
            string cred = string.Format("{0} {1}", "Basic", enc);
            profileRequest.Headers[HttpRequestHeader.Authorization] = cred;
            profileRequest.Accept = "application/json";
            profileRequest.ContentType = "text/plain";
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            profileRequest.Method = "POST";
            profileRequest.ContentLength = bytes.Length;
            using (Stream requeststream = profileRequest.GetRequestStream())
            {
                requeststream.Write(bytes, 0, bytes.Length);
                requeststream.Close();
            }
            string response;
            //string outPath =  Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "sample1.txt");
            //StreamWriter file = new StreamWriter(outPath);

            using (HttpWebResponse webResponse = (HttpWebResponse)profileRequest.GetResponse())
            {
                using (StreamReader sr = new StreamReader(webResponse.GetResponseStream()))
                {
                    response = sr.ReadToEnd(); 

                    IBMWatson ibm = JsonDeserialize<IBMWatson>(response);
                    int childCount = ibm.tree.children.Count;
                    Child c1 = new Child();
                    Child2 c2 = new Child2();
                    Child3 c3 = new Child3();
                    if (ibm.tree.children.Count > 0)
                    {
                        foreach (var i in ibm.tree.children)
                        {
                            c1.id = i.id;
                            c1.name = i.name;
                            c1.category = i.category;
                            c1.children = i.children;

                            if (c1.children.Count > 0)
                            {
                                foreach (var j in c1.children)
                                {
                                    c2.id = j.id;
                                    c2.name = j.name;
                                    c2.category = j.category;
                                    double per = double.Parse((j.percentage * 100).ToString(), CultureInfo.InvariantCulture);
                                    string s = string.Format("{0:0.00}", per);
                                    c2.percentage = Convert.ToDouble(s);
                                    c2.children = j.children;
                                }
                                IBMProperties prop = new IBMProperties();
                                if (c2.children.Count > 0)
                                {

                                    if (c1.id == "personality")
                                    {
                                        prop.Name = "Personality";
                                        List<IBMDTO> Personfea = new List<IBMDTO>();
                                        foreach (var k in c2.children)
                                        {
                                            c3.id = k.id;
                                            c3.name = k.name;
                                            double per = double.Parse((k.percentage * 100).ToString(), CultureInfo.InvariantCulture);
                                            string s = string.Format("{0:0.00}", per);
                                            c3.percentage = Convert.ToDouble(s);


                                            IBMDTO abc = new IBMDTO();
                                            abc.Name = c3.name;
                                            abc.ID = c3.id;
                                            abc.Percentage = c3.percentage;

                                            Personfea.Add(abc);


                                        }
                                        prop.Result = Personfea;
                                        lstIBMProperties.Add(prop);
                                    }
                                    else if (c1.id == "needs")
                                    {
                                        prop.Name = "Consumer Needs";

                                        List<IBMDTO> Personfea = new List<IBMDTO>();

                                        foreach (var k in c1.children)
                                        {
                                            foreach (var l in k.children)
                                            {
                                                c2.id = l.id;
                                                c2.name = l.name;
                                                double per = double.Parse((l.percentage * 100).ToString(), CultureInfo.InvariantCulture);
                                                string s = string.Format("{0:0.00}", per);
                                                c2.percentage = Convert.ToDouble(s);


                                                IBMDTO abc = new IBMDTO();
                                                abc.Name = c2.name;
                                                abc.ID = c2.id;
                                                abc.Percentage = c2.percentage;

                                                Personfea.Add(abc);
                                            }

                                        }
                                        prop.Result = Personfea;
                                        lstIBMProperties.Add(prop);
                                    }
                                    else if (c1.id == "values")
                                    {
                                        prop.Name = "Values";

                                        List<IBMDTO> Personfea = new List<IBMDTO>();

                                        foreach (var k in c1.children)
                                        {
                                            foreach (var m in k.children)
                                            {
                                                c2.id = m.id;
                                                c2.name = m.name;
                                                double per = double.Parse((m.percentage * 100).ToString(), CultureInfo.InvariantCulture);
                                                string s = string.Format("{0:0.00}", per);
                                                c2.percentage = Convert.ToDouble(s);


                                                IBMDTO abc = new IBMDTO();
                                                abc.Name = c2.name;
                                                abc.ID = c2.id;
                                                abc.Percentage = c2.percentage;

                                                Personfea.Add(abc);
                                            }

                                        }
                                        prop.Result = Personfea;
                                        lstIBMProperties.Add(prop);
                                    }
                                }
                            }
                        }
                    }

                    sr.Close();
                }
                System.IO.File.WriteAllText(inPath, string.Empty);
                webResponse.Close();
            }
            return lstIBMProperties;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }
    }
}
