﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Lemonaid_web_api.Controllers
{
    public class Coach2V1Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpPost]
        public Utility.CustomResponse UpdateCoachProfile(CoachProfileDTO coachProfileDto)
        {

            try
            {
                if (coachProfileDto.Description == "-1")
                {
                    coachProfileDto.Description = string.Empty;
                }

                if (coachProfileDto.Phonenumber == "-1")
                {
                    coachProfileDto.Phonenumber = string.Empty;
                }

                Dal2_v1.UpdateCoachProfile(coachProfileDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Coach_Profile_Updated;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V1/UpdateCoachProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
    }
}
