﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Lemonaid_web_api.Controllers
{
    public class ClubCoach2V6Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpPost]
        public Utility.CustomResponse AddClubCoach(ClubCoachDTO clubCoachDto)
        {
            ClubCoach2Controller Clcctrl = new ClubCoach2Controller();

            try
            {

                if (!clubCoachDto.ProfilePicURL.StartsWith("http"))
                {
                    clubCoachDto.ProfilePicURL = UsersDal.UploadImageFromBase64(clubCoachDto.ProfilePicURL, "png");
                }
                 

                if (clubCoachDto.Id == 0)
                {
                    List<SportsDTO> sports = Clcctrl.AddClubCoachToWrapperDb(clubCoachDto);
                }
                var clubCoachDet = DAL2V6.AddClubCoach2_v6(clubCoachDto);

                clubCoachDet.SportId = clubCoachDto.SportId;
                if (clubCoachDet.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = clubCoachDet;
                    _result.Message = CustomConstants.clubCoach_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach2v6/AddClubCoach", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse GetAllInstitutesByClubCoach(ClubCoachDeckDTO clubCoachDeck)
        {
            CoachController coachController = new CoachController();
            ClubCoachController clubcoachController = new ClubCoachController();
            try
            {
                List<CoachDTO> listWithQuestionaire = new List<CoachDTO>();
                List<QuestionaireDTO> questionaireDto = UsersDalV5.GetQuestionaire_v5(clubCoachDeck.ClubCoachId, Utility.UserTypes.ClubCoach);
                listWithQuestionaire = clubcoachController.AddQuestionairetoList(questionaireDto, listWithQuestionaire);
                List<CoachDTO> institutes = UsersDal.GetAllInstitutesByClubCoach(clubCoachDeck.ClubCoachId, clubCoachDeck.offset, clubCoachDeck.max, clubCoachDeck.searchText);
                if (institutes.Count > 0)
                {
                    listWithQuestionaire = clubcoachController.AddinstitutestoList(institutes, listWithQuestionaire);
                }
                if (listWithQuestionaire.Count > 0)
                {
                    listWithQuestionaire = clubcoachController.AddCoachVideos(listWithQuestionaire);
                    List<AdDTO> ads = DAL2V6.GetChatAdCards(clubCoachDeck.Emailaddress, clubCoachDeck.loginType, clubCoachDeck.SportId, Convert.ToInt32(Utility.AdType.NormalAd), Utility.UserTypes.ClubCoach);
                    listWithQuestionaire = coachController.GetAdCoaches(ads, listWithQuestionaire);
                    List<AdDTO> ChatAds = DAL2V6.GetChatAdCards(clubCoachDeck.Emailaddress, clubCoachDeck.loginType, clubCoachDeck.SportId, Convert.ToInt32(Utility.AdType.FrontRushAd), Utility.UserTypes.ClubCoach);
                    listWithQuestionaire = coachController.GetAdCoaches(ChatAds, listWithQuestionaire);

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = listWithQuestionaire;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach2V6/GetAllInstitutesByClubCoach", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse GetClubCoachFavourites(ClubCoachDeckDTO clubCoachDeckDTO)
        {
            // NO need of videos in Fav - disussed on 15 Nov
            try
            {
                ClubCoachController clubcoachController = new ClubCoachController();

                List<CoachDTO> coaches = DAL2V6.GetClubCoachFavourites_2V6(clubCoachDeckDTO.ClubCoachId);
                coaches = clubcoachController.AddCoachVideos(coaches);
                for (int i = 0; i < coaches.Count; i++)
                {
                    List<AthleteDTO> atheletes = UsersDal.GetRecommendAthletesinInstitutes(coaches[i].Id, clubCoachDeckDTO.ClubCoachId);
                    coaches[i].RecommendedAtheletes = atheletes;
                }

                AthleteFavouriteDTO athleteFavouriteDTO = new AthleteFavouriteDTO();
                athleteFavouriteDTO.AthleteEmail = clubCoachDeckDTO.Emailaddress;
                athleteFavouriteDTO.LoginType = clubCoachDeckDTO.loginType;
                athleteFavouriteDTO.UserType = Utility.UserTypes.ClubCoach;
                athleteFavouriteDTO.Offset = clubCoachDeckDTO.offset;
                athleteFavouriteDTO.Max = clubCoachDeckDTO.max;
                athleteFavouriteDTO.SportId = clubCoachDeckDTO.SportId;

                athleteFavouriteDTO.UserType = Utility.UserTypes.ClubCoach;
                var ChatAdMatches = DAL2V6.GetAthleteChatAdFavourites(athleteFavouriteDTO);
                //GetUserID -- From athleteemail , logintype, ClC
                int AthleteWrapperId = DAL2V6.GetUserId(athleteFavouriteDTO.AthleteEmail, athleteFavouriteDTO.LoginType, athleteFavouriteDTO.UserType);
                foreach (var a in ChatAdMatches)
                {
                    int chatCount = DAL2V4.GetAthleteChatAdChatCount(a.Id, AthleteWrapperId);
                    a.ChatCount = chatCount;
                }
                FamilyFriends2V6Controller familyFriends2V6 = new FamilyFriends2V6Controller();
                coaches = familyFriends2V6.AddChatAdsToList(ChatAdMatches, coaches);

                var Shuffleres = coaches.OrderBy(a => Guid.NewGuid());

                IEnumerable<CoachDTO> enumcollegeAtheleteCoaches = Shuffleres.Skip(clubCoachDeckDTO.offset).Take(clubCoachDeckDTO.max);
                var coaches1 = enumcollegeAtheleteCoaches.ToList();

                if (coaches1.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coaches1;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Matches;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach2V6/GetClubCoachFavourites", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse Cities(int StateId)
        {

            try
            {
                var cities = DAL2V6.GetCities(StateId);
                if (cities.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = cities;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach2V6/Cities", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet] 
        public Utility.CustomResponse GetClubandSchoolCoaches()
        { 
            try
            {
                ClubSchoolCoaches clubSchoolCoaches = new ClubSchoolCoaches();
                clubSchoolCoaches.clubCoaches = DAL2V6.GetClubCoaches();
                clubSchoolCoaches.schoolCoaches = DAL2V6.GetSchoolCoaches();
             
               
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = clubSchoolCoaches;
                    _result.Message = CustomConstants.Details_Get_Successfully; 

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach2V6/GetClubandSchoolCoaches", "", ex.Message, "Exception", 0);
                return _result;
            }

        }

    }
}
