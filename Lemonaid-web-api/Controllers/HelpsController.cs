﻿using System;
using System.Collections.Generic; 
using System.Web.Http;
using Lemonaid_web_api.Models;
 
 

namespace Lemonaid_web_api.Controllers
{ 
    public class HelpsController : ApiController    
    {
        public static string Message = "";
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse FaQs(int organisationId)  
        {
          
            try
            {
                var coaches = UsersDal.GetFAQs(1, organisationId);
                if (coaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Helps/Faqs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse Videos(int organisationId)  
        { 
            try
            {
                var coaches = UsersDal.GetFAQs(2, organisationId);
                if (coaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Helps/Videos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        // Add Usage Tracking
        [HttpPost]
        public Utility.CustomResponse UsageTracking(IEnumerable<UsageDTO> usageDto)
        {
            try
            {
                foreach (var u in usageDto)
                {
                    if (u.FeatureId == "Athlete Profile Status")
                    {
                        u.FeatureId = "1";
                    }
                    if (u.FeatureId == "Coach Profile Status")
                    {
                        u.FeatureId = "2";
                    }
                    if (u.FeatureId == "Blogs")
                    {
                        u.FeatureId = "3";
                    }

                    UsersDal.AddUsageTracking(u);
                }


                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Usage_Tracking_Successfully;


            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Helps/UsageTracking", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;
        }

    }
}
