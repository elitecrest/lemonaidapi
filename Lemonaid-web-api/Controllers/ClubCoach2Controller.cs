﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;

namespace Lemonaid_web_api.Controllers
{
    public class ClubCoach2Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpPost]
        public Utility.CustomResponse AddClubCoach(ClubCoachDTO clubCoachDto)
        {
            try
            {
               
                if (clubCoachDto.Id == 0)
                {
                    List<SportsDTO> sports = AddClubCoachToWrapperDb(clubCoachDto);
                }
                var clubCoachId = Dal2.AddClubCoach(clubCoachDto);
                    if (clubCoachId.Id > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = clubCoachId;
                        _result.Message = CustomConstants.clubCoach_Added;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.Already_Exists;
                    }
               


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach2/AddClubCoach", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        public List<SportsDTO> AddClubCoachToWrapperDb(ClubCoachDTO clubCoachDto)
        {
            List<SportsDTO> sports = new List<SportsDTO>();

            HttpClient client = new HttpClient();
            string wrapperUrl = ConfigurationManager.AppSettings["WrapperBaseURL"];
            client.BaseAddress = new Uri(wrapperUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            AthleteWrapperDTO athlete = new AthleteWrapperDTO();

            athlete.Emailaddress = clubCoachDto.Emailaddress;
            if ((clubCoachDto.LoginType == null) || (clubCoachDto.LoginType == 0))
            {
                athlete.LoginType = 1;
            }
            else
            {
                athlete.LoginType = clubCoachDto.LoginType;
            }
            athlete.UserName = clubCoachDto.Username;
            athlete.UserType = Utility.UserTypes.ClubCoach;
            athlete.DeviceType = clubCoachDto.DeviceType;
            athlete.DeviceId = clubCoachDto.DeviceId;

            athlete.Id = 0;
            var response = client.PostAsJsonAsync("User/AddUser", athlete).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;

                if (Convert.ToInt32(res.Response) > 0)
                {
                    AthleteSportsDTO athleteSports = new AthleteSportsDTO();
                    athleteSports.Id = 0;
                    athleteSports.SportId = clubCoachDto.SportId;
                    athleteSports.UserId = Convert.ToInt32(res.Response);
                    var response1 = client.PostAsJsonAsync("User/AddUserSport", athleteSports).Result;
                    if (response1.IsSuccessStatusCode)
                    {
                        Utility.CustomResponse res1 = response1.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                        sports = JsonDeserialize<List<SportsDTO>>(res1.Response.ToString());
                    }
                }

            }

            return sports;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        [HttpGet]
        public Utility.CustomResponse ClubCoachProfile(string emailAddress)
        {
            try
            {
                ClubCoachDTO clubCoachDto = Dal2.GetClubCoachProfile(emailAddress);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = clubCoachDto;
                _result.Message = CustomConstants.Details_Get_Successfully;
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach2/ClubCoachProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse Schools()
        { 
            try
            {
                   var schools = Dal2.GetSchools(); 
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = schools;
                    _result.Message = CustomConstants.Details_Get_Successfully; 

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("ClubCoach2/Schools", "", ex.Message, "Exception", 0);
                return _result;
            }

        }

         
     

    }
}
