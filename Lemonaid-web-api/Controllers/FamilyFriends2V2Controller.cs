﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic; 
using System.Web.Http; 

namespace Lemonaid_web_api.Controllers
{
    public class FamilyFriends2V2Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddfamilyFriends(FamilyFriendsDTO familyFriendsDto)
        {
            FamilyFriendsController ffctrl = new FamilyFriendsController();
            try
            {
                if (familyFriendsDto.Id == 0)
                {
                    List<SportsDTO> sports = ffctrl.AddFFFToWrapperDb(familyFriendsDto);
                }

                var familyFriends = Dal2_v2.AddfamilyFriends2_v2(familyFriendsDto);
                familyFriends.SportId = familyFriendsDto.SportId;
                if (familyFriends.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = familyFriends;
                    _result.Message = CustomConstants.FamilyFriend_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends2V2/AddfamilyFriends", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;

        }


        [HttpGet]
        public Utility.CustomResponse GetFamilyFriendsProfile(string emailaddress,int loginType)
        {
            try
            {
                var familyFriends = Dal2_v2.GetFamilyFriendsProfile2_V2(emailaddress, loginType);  
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = familyFriends;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/GetFamilyFriendsProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
    }
}
