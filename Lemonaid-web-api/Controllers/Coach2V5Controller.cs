﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Lemonaid_web_api.Models;

namespace Lemonaid_web_api.Controllers
{
    public class Coach2V5Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddCoachVideoContent(MediaContent mediaContent)
        {

            try
            {
                int id = DAL2V5.AddCoachVideoContent(mediaContent);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Athlete_Video_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V5/AddCoachVideoContent", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse UpdateCoachThumbnailContent(MediaContent mediaContent)
        {

            try
            {
                UploadImage uploadImage = new UploadImage();
                mediaContent.ThumbnailURL = uploadImage.GetImageURL(mediaContent.ThumbnailBase64String, ".jpg", "");
                int id = DAL2V5.UpdateCoachThumbnailContent(mediaContent);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.ShareVideo_Content_Updated;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V5/UpdateCoachThumbnailContent", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse GetCoachDetails(int coachId)
        {
            try
            {

                var coachDet = DAL2V5.GetCoachDetails(coachId);
                coachDet.CoachVideos = Dal2.GetCoachVideos(coachId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = coachDet;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V5/GetCoachDetails", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse UploadCoachPhotos(ThumbnailDTO thumbnailDto)
        {
            try
            {
                var video = DAL2V5.UploadCoachPhotos_2V5(thumbnailDto);
                if (video.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = video;
                    _result.Message = CustomConstants.Photo_Uploaded;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V5/UploadCoachPhotos_2V5", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse UpdateCoachEmailaddress(CoachProfileDTO coachProfile)
        {
            try
            {

                var id = DAL2V5.UpdateCoachEmailaddress(coachProfile); 
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = id;
                _result.Message = CustomConstants.CoachEmail_Updated;

                return _result;

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V5/UpdateCoachEmailaddress", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

    }
}
