﻿ 

namespace Lemonaid_web_api.Models
{
    public class EmailLogModel
    {
        public int AddEmailLog(string @from, string to, string subjectorErr, string mailType, int status)
        {
            EmailDTO email = new EmailDTO
            {
                From = @from,
                To = to,
                Status = status,
                Subject = subjectorErr,
                MailType = mailType
            };
            UsersDal.AddEmailLog(email);
            return 1;
        }
    }
}