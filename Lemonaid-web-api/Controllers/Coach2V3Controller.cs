﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Lemonaid_web_api.Controllers
{
    public class Coach2V3Controller : ApiController
    {

        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpPost]
        public Utility.CustomResponse UpdateCoachProfile(CoachProfileDTO coachProfileDto)
        {

            try
            {
                if (coachProfileDto.Description == "-1")
                {
                    coachProfileDto.Description = string.Empty;
                }

                if (coachProfileDto.Phonenumber == "-1")
                {
                    coachProfileDto.Phonenumber = string.Empty;
                }
                if (!coachProfileDto.ProfilePicURL.StartsWith("http"))
                {
                    coachProfileDto.ProfilePicURL = UsersDal.UploadImageFromBase64(coachProfileDto.ProfilePicURL, "png");
                }

                coachProfileDto = Dal2_v3.UpdateCoachProfile(coachProfileDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Coach_Profile_Updated;
                _result.Response = coachProfileDto;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V3/UpdateCoachProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }


        [HttpGet]
        public Utility.CustomResponse GetCoachUnreadCount()
        {

            try
            {
                var cntofCoaches = Dal2_v3.GetCoachUnreadCount();
                if (cntofCoaches.ToList().Count > 0)
                {
                    _result.Response = cntofCoaches;
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

             

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        //[HttpGet]
        //public Utility.CustomResponse UnreadChat(int athleteId,int CoachId)
        //{ 
        //    try
        //    {
        //        Dal2_v3.AddCoachAthleteChatCount(athleteId, CoachId, 0, 0, 1); 
        //        _result.Status = Utility.CustomResponseStatus.Successful;
        //        _result.Message = CustomConstants.Details_Get_Successfully;

        //        return _result;
        //    }
        //    catch (Exception ex)
        //    {
        //        _result.Status = Utility.CustomResponseStatus.Exception;
        //        _result.Message = ex.Message;
        //        return _result;
        //    }
        //}
    }
}
