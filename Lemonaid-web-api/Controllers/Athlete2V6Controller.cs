﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic; 
using System.Web.Http;
 

namespace Lemonaid_web_api.Controllers
{
    public class Athlete2V6Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        [HttpPost] 
        public Utility.CustomResponse GetAllAtheletesByCollegeAtheleteEmail(AthleteDeckDTO athleteDeckDto) // CollegeAthlete email address
        {
            AthleteController atc = new AthleteController();
            try
            {
                if (athleteDeckDto.Description  == "-1")
                {
                    athleteDeckDto.Description = string.Empty;
                }
                List<AthleteDTO> athletes = UsersDalV4.GetAllAthletesByCollegeAthelete_v4(athleteDeckDto.AthleteId, athleteDeckDto.Offset, athleteDeckDto.Max, athleteDeckDto.Description, athleteDeckDto.SearchText);

                List<AthleteDTO> newList = new List<AthleteDTO>();
                List<AthleteDTO> listWithQuestionaire = new List<AthleteDTO>();
                CoachV5Controller coachv5Ctrl = new CoachV5Controller();
                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (athleteDeckDto.LocationType == 1)
                {
                    newList = athletes;

                }
                else
                    if (athleteDeckDto.LocationType == 2)
                {
                    foreach (AthleteDTO t in athletes)
                    {
                        double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                        double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                        double distance = atc.DistanceTo(athleteDeckDto.Latitude, athleteDeckDto.Longitude, athleteLatitude, athleteLongitude);

                        if (distance <= 100)
                        {
                            newList.Add(t);
                        }
                    }
                }
                else
                        if (athleteDeckDto.LocationType == 3)
                {
                    foreach (AthleteDTO t in athletes)
                    {
                        double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                        double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                        double distance = atc.DistanceTo(athleteDeckDto.Latitude, athleteDeckDto.Longitude, athleteLatitude, athleteLongitude);

                        if (distance <= 5)
                        {
                            newList.Add(t);
                        }
                    }
                }


                List<QuestionaireDTO> questionaireDto = UsersDalV5.GetQuestionaire_v5(athleteDeckDto.AthleteId, "CA");
                listWithQuestionaire = coachv5Ctrl.AddQuestionairetoList(questionaireDto, listWithQuestionaire);

                if (newList.Count > 0)
                {
                    //Add BestTimes
                    newList = atc.AddBesttimesToList(newList);
                   // check later with DAL2V6.GetChatAdCards(athleteEmailAddress, loginType, athleteDeckDTO.SportId, Convert.ToInt32(Utility.AdType.NormalAd), Utility.UserTypes.HighSchooAthlete);
                    //List<AdDTO> ads = DAL2V6.GetDisplayCardsBySport(athleteDeckDto.SportId);
                    //newList = atc.GetAdAthletes(ads, newList);


                    //Add Athelete Videos
                    newList = atc.AddVideosToList(newList);
                    listWithQuestionaire = coachv5Ctrl.AddAtheletestoList(newList, listWithQuestionaire);
                }
                if (listWithQuestionaire.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = listWithQuestionaire;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athelete2V6/GetAllAtheletesByCollegeAtheleteEmail", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetNotificationsLikesCount(int userid,string userType)
        {

            try
            {
                int senderType = 0;
                NotificationsLikesCount notif = new NotificationsLikesCount();
               switch(userType)
                {
                    case "HS": senderType = 0; break;
                    case "CC": senderType = 1; break;
                    case "ClC": senderType = 2; break;

                }
                var cnt = DAL2V6.GetNotificationsCount(userid, senderType);
                notif.NotificationCount = cnt;

                var mutualcount = DAL2V6.GetMutualLikesCount(userid, userType);
                notif.MutualLikeCount = mutualcount;

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = notif;
                    _result.Message = CustomConstants.Details_Get_Successfully;
              
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

       
    }
}
