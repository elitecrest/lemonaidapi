﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Lemonaid_web_api
{
    public partial class ConfirmAtheleteByCoach_v4 : System.Web.UI.Page
    {
        Utility.CustomResponse Res = new Utility.CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmit_Click(sender, e);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string baseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            string AtheleteId = Request.QueryString["AtheleteId"].ToString();
            string TypeId = Request.QueryString["typeId"].ToString();
            string Count = Request.QueryString["count"].ToString();


            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("/Athletev4/ChangeStatusOfAthleteBadges_v4?AtheleteId=" + AtheleteId + "&typeId=" + TypeId + "&count=" + Count + "&status=1").Result;
            if (response.IsSuccessStatusCode)
            {
                Res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                if (Res.Status == Utility.CustomResponseStatus.Successful)
                {

                    lblmsg.Visible = true;
                    lblmsg.Text = Res.Message;
                    btnSubmit.Visible = false;
                    Response.Redirect("http://www.lemonaidrecruiting.com/", true);
                }
                else
                {
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                    lblmsg.Visible = true;
                    lblmsg.Text = Res.Message;
                }

            }
        }
    }
}