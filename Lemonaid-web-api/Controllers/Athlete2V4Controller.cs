﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.IO;
using System.Web;
using System.Net.Mail;
using Lemonaid_web_api.Models;
using System.Configuration;

namespace Lemonaid_web_api.Controllers
{

    public class Athlete2V4Controller : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse DeleteAthleteSport(int athleteId, int loginType, int deleteSportId, int activeSportId)
        {

            try
            {
                string emailaddress = string.Empty;
                var ath = DAL2V4.DeleteAthleteSport(athleteId, loginType, deleteSportId);
                string userType = (ath.AthleteType == 0) ? "HS" : "CA";
                var id = DAL2V4.DeleteAthleteSportFromWrapper(ath.AthleteEmail, loginType, deleteSportId, userType, activeSportId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athleteId;
                _result.Message = CustomConstants.Athlete_Deleted_Sport;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V4/DeleteAthleteSport", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse GetAllCoachesByLocationWithSearch2_v4(AthleteDeckDTO athleteDeckDTO)
        {
            AthleteController athleteContr = new AthleteController();
            CoachController coachController = new CoachController();
            try
            {
                if (athleteDeckDTO.Description == "-1")
                {
                    athleteDeckDTO.Description = string.Empty;
                }

                var caches = UsersDalV5.GetAllCoachesPagingWithSearch_v5(athleteDeckDTO.AthleteId , athleteDeckDTO.Offset, athleteDeckDTO.Max, athleteDeckDTO.Ncaa, athleteDeckDTO.Conference, athleteDeckDTO.Admission, athleteDeckDTO.Description, athleteDeckDTO.SearchText);
                if (caches.Count > 0)
                {
                    var newList = new List<CoachDTO>();

                    //Location Type 
                    //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                    if (athleteDeckDTO.LocationType == 1)
                    {
                        newList = caches;

                    }
                    else
                        if (athleteDeckDTO.LocationType == 2)
                    {
                        for (int i = 0; i < caches.Count; i++)
                        {
                            double athleteLatitude = (caches[i].Latitude == "") ? 0.0 : Convert.ToDouble(caches[i].Latitude);
                            double athleteLongitude = (caches[i].Longitude == "") ? 0.0 : Convert.ToDouble(caches[i].Longitude);
                            double distance = athleteContr.DistanceTo(athleteDeckDTO.Latitude, athleteDeckDTO.Longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(caches[i]);
                            }
                        }
                    }
                    else
                            if (athleteDeckDTO.LocationType == 3)
                    {
                        for (int i = 0; i < caches.Count; i++)
                        {
                            double athleteLatitude = (caches[i].Latitude == "") ? 0.0 : Convert.ToDouble(caches[i].Latitude);
                            double athleteLongitude = (caches[i].Longitude == "") ? 0.0 : Convert.ToDouble(caches[i].Longitude);
                            double distance = athleteContr.DistanceTo(athleteDeckDTO.Latitude, athleteDeckDTO.Longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 5)
                            {
                                newList.Add(caches[i]);
                            }
                        }
                    }




                    if (newList.Count > 0)
                    {
                        //Chat Ads Cards from Wrapper
                        string athleteName = string.Empty, athleteEmailAddress = string.Empty; string deviceName = string.Empty; string deviceId = string.Empty; int loginType = 0; string username = string.Empty;
                        UsersDalV4.GetAthleteDetails_v4(athleteDeckDTO.AthleteId, ref athleteName, ref athleteEmailAddress, ref deviceId, ref deviceName, ref loginType, ref username);
                          
                        if (loginType == 2 || loginType == 3) 
                            athleteEmailAddress = username;

                        List<AdDTO> ads = DAL2V6.GetChatAdCards(athleteEmailAddress, loginType, athleteDeckDTO.SportId, Convert.ToInt32(Utility.AdType.NormalAd), Utility.UserTypes.HighSchooAthlete);
                        newList = coachController.GetAdCoaches(ads, newList);


                        List<AdDTO> ChatAds = DAL2V6.GetChatAdCards(athleteEmailAddress, loginType, athleteDeckDTO.SportId, Convert.ToInt32(Utility.AdType.FrontRushAd), Utility.UserTypes.HighSchooAthlete);
                        newList = GetFrontRushCoaches(ChatAds, newList);

                        //List<AdDTO> ChatAds = DAL2V4.GetChatAdCards(athleteEmailAddress, loginType, athleteDeckDTO.SportId,Convert.ToInt32(Utility.AdType.ChatAd));
                        //newList = GetFrontRushCoaches(ChatAds, newList);

                        
                    }
                    //Get College atheletes and coaches
                    List<AthleteDTO> collegeAtheletes = UsersDalV4.GetCollegeAtheletes_v4(athleteDeckDTO.SearchText, athleteDeckDTO.AthleteId);
                    int totalcount = collegeAtheletes.Count + newList.Count;

                    for (int i = 0; i < collegeAtheletes.Count; i++)
                    {
                        collegeAtheletes[i].TotalCount = totalcount;
                    }


                    List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches = new List<CollegeAtheleteCoachesDTO>();
                    List<QuestionaireDTO> questionaireDto = UsersDalV5.GetQuestionaire_v5(athleteDeckDTO.AthleteId, "HA");
                    collegeAtheleteCoaches = coachController.AddQuestionairetoList(questionaireDto, collegeAtheleteCoaches);
                    if (newList.Count > 0)
                    {
                        for (int i = 0; i < newList.Count; i++)
                        {
                            newList[i].TotalCount = totalcount;
                        }
                        collegeAtheleteCoaches = coachController.AddCoachestoList(newList, collegeAtheleteCoaches);
                    }
                    collegeAtheleteCoaches = coachController.AddAtheletestoList(collegeAtheletes, collegeAtheleteCoaches);

                    IEnumerable<CollegeAtheleteCoachesDTO> enumcollegeAtheleteCoaches = collegeAtheleteCoaches.Skip(athleteDeckDTO.Offset).Take(collegeAtheleteCoaches.ToList().Count - athleteDeckDTO.Offset);
                    collegeAtheleteCoaches = enumcollegeAtheleteCoaches.ToList();

                    Athlete2Controller athlete2 = new Athlete2Controller();

                    collegeAtheleteCoaches = athlete2.AddCoachVideos(collegeAtheleteCoaches);
                    var collegeAtheleteCoachesShuffleres = collegeAtheleteCoaches.OrderBy(a => a.EnrollmentNo);
                    

                    if (collegeAtheleteCoachesShuffleres.ToList().Count > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = collegeAtheleteCoachesShuffleres;
                        _result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2/GetAllCoachesByLocationWithSearch2_v1", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        private List<CoachDTO> GetFrontRushCoaches(List<AdDTO> frontRushCards, List<CoachDTO> newList)
        {
            
            for (int i = 0; i < frontRushCards.Count; i++)
            {
                CoachDTO coach = new CoachDTO();
                coach.Id = frontRushCards[i].Id;
                coach.Name = frontRushCards[i].Name;
                coach.ProfilePicURL = frontRushCards[i].ProfilePicURL;
                coach.Description = frontRushCards[i].Description;
                coach.CoverPicURL = frontRushCards[i].VideoURL;
                coach.AdType = frontRushCards[i].AdType;
                coach.AreasInterested = "";
                coach.Address = "";
                coach.Emailaddress = "";
                coach.YearOfEstablish = 0;
                coach.NCAA = 0;
                coach.Conference = 0;
                coach.AdmissionStandard = 0;
                coach.AverageRating = 0;
                coach.ClassificationName = "";
                coach.State = "";
                coach.City = "";
                coach.Zip = "";
                coach.WebsiteURL = "";
                coach.FaidURL = "";
                coach.ApplURL = "";
                coach.Latitude = "";
                coach.Longitude = "";
                coach.EnrollmentNo = "";
                coach.ApplicationNo = "";
                coach.AdmissionNo = "";
                coach.FacebookId = "";
                coach.TotalCount = 0;
                coach.NoOfAthletesInApp = 0;
                coach.AcceptanceRate = 0;
                coach.NCAARate = 0;
                coach.ConferenceRate = 0;
                coach.SocialRate = 0;
                coach.AdmissionRate = 0;
                coach.AtheleteRate = 0;
                coach.Address = "";
                coach.Cost = 0;
                coach.MensTeam = 0;
                coach.WomenTeam = 0;
                newList.Add(coach);
            }

            return newList;
        }


        [HttpPost]
        public Utility.CustomResponse AddAtheleteFrontRushMatch(AthleteFrontRushMatchDTO athleteFrontRushMatchDTO)
        {
            try
            {

                var matchId = DAL2V4.AddAtheleteFrontRushMatch(athleteFrontRushMatchDTO);
                _result.Status = Utility.CustomResponseStatus.Successful;
                matchId = (athleteFrontRushMatchDTO.Status == false) ? 0 : matchId;
                _result.Response = matchId;
                _result.Message = CustomConstants.Status_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2v4/AddAtheleteFrontRushMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddAthleteFrontRushChat(ChatDTO chatDto)
        {
            try
            {
                var chatid = DAL2V4.AddAthleteFrontRushChat(chatDto);               
                //if (chatDto.ReceiverType == 1)
                //{
                //    int count = Dal2_v3.GetChatMessagesUnreadCount(chatDto.SenderId, chatDto.ReceiverId);
                //    //Insert into CaoachAthleteChatCount
                //    Dal2_v3.AddCoachAthleteChatCount(chatDto.SenderId, chatDto.ReceiverId, count, 0, 1);
                //}
                if (chatid > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = chatid;
                    _result.Message = CustomConstants.Chat_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetAthletesForFrontRush(int frontRushId)
        {

            try
            {
                var athletes = DAL2V4.GetAthletesForFrontRush(frontRushId);
                foreach(var a in athletes)
                {
                    a.ChatCount = DAL2V4.GetAthleteFrontRushChatMessagesUnreadCount(a.Id, frontRushId);
                }

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athletes;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse GetAthleteFrontRushChatMessages(ChatDTO chatDto)
        {
            try
            {

                var chats = DAL2V4.GetAthleteFrontRushChatMessages(chatDto); 
               
                if (chats.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = chats;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }



                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse DeleteFrontRushAtheleteMatch(AthleteFrontRushMatchDTO athleteFrontRushMatchDTO)
        {

            try
            {

                var matchId = DAL2V4.DeleteFrontRushAtheleteMatch(athleteFrontRushMatchDTO);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2v4/DeleteFrontRushAtheleteMatch", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse GetAthleteFavourites(AthleteFavouriteDTO athleteFavouriteDTO)
        {
            Athletev4Controller athleteV4 = new Athletev4Controller();
            try
            {

                CoachController coachController = new CoachController();
                var coaches = UsersDalV4.GetAtheleteFavourites_v4(athleteFavouriteDTO.AthleteId , athleteFavouriteDTO.Offset , athleteFavouriteDTO.Max);
                coaches = athleteV4.AddCoachVideos(coaches);

                foreach (var c in coaches)
                {
                    int chatCount = UsersDalV4.GetChatCount(c.CoachId, athleteFavouriteDTO.AthleteId);
                    c.ChatCount = chatCount;  

                    
                    //Insert into CaoachAthleteChatCount
                    // Dal2_v3.AddCoachAthleteChatCount(atheleteId, c.CoachId, c.ChatCount, 1, 0);
                }
                //Get College Athlete Videos for 2.6
              var collegeAtheletes = UsersDalV4.GetCollegeAtheletesInAtheleteFavourites_v4(athleteFavouriteDTO.AthleteId, athleteFavouriteDTO.Offset, athleteFavouriteDTO.Max);
                foreach (var a in collegeAtheletes)
                {
                    int chatCount = UsersDalV4.GetChatCount(a.Id, athleteFavouriteDTO.AthleteId);
                    a.ChatCount = chatCount;
                }
                var frontRushMatches = DAL2V4.GetAthleteFrontRushFavourites(athleteFavouriteDTO.AthleteId, athleteFavouriteDTO.Offset, athleteFavouriteDTO.Max);
                foreach (var a in frontRushMatches)
                {
                    int chatCount = DAL2V4.GetAthleteFRChatCount(a.Id, athleteFavouriteDTO.AthleteId);
                    a.ChatCount = chatCount;
                }
                athleteFavouriteDTO.UserType = Utility.UserTypes.HighSchooAthlete;

                var ChatAdMatches = DAL2V6.GetAthleteChatAdFavourites(athleteFavouriteDTO);
                //GetUserID -- From athleteemail , logintype, HS
                int AthleteWrapperId = DAL2V6.GetUserId(athleteFavouriteDTO.AthleteEmail, athleteFavouriteDTO.LoginType, athleteFavouriteDTO.UserType);
                foreach (var a in ChatAdMatches)
                {
                    int chatCount = DAL2V4.GetAthleteChatAdChatCount(a.Id, AthleteWrapperId);
                    a.ChatCount = chatCount;
                }

              
                List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches = new List<CollegeAtheleteCoachesDTO>();

                collegeAtheleteCoaches = coachController.AddCoachestoList(coaches, collegeAtheleteCoaches);
                collegeAtheleteCoaches = coachController.AddAtheletestoList(collegeAtheletes, collegeAtheleteCoaches);
                collegeAtheleteCoaches = athleteV4.AddFrontRushAdsToList(frontRushMatches, collegeAtheleteCoaches);
                collegeAtheleteCoaches = athleteV4.AddChatAdsToList(ChatAdMatches, collegeAtheleteCoaches);

                var totalLikesCount = collegeAtheleteCoaches.Count(p => p.Liked);

                foreach (CollegeAtheleteCoachesDTO t in collegeAtheleteCoaches)
                {
                    t.TotalLikedCount = totalLikesCount;
                }

                IEnumerable<CollegeAtheleteCoachesDTO> sortedCollegeAtheleteCoaches = collegeAtheleteCoaches.ToList().OrderByDescending(x => x.Liked);

                IEnumerable<CollegeAtheleteCoachesDTO> enumcollegeAtheleteCoaches = sortedCollegeAtheleteCoaches.Skip(athleteFavouriteDTO.Offset).Take(athleteFavouriteDTO.Max);


                if (enumcollegeAtheleteCoaches.ToList().Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = enumcollegeAtheleteCoaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Matches;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2v4/GetAthleteFavourites", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse AtheleteProfile(string emailaddress, int logintype,int sportId)
        {
            AthleteController atc = new AthleteController();
            try
            {
                AthleteDTO athleteDto = DAL2V4.GetAthleteProfile(emailaddress, logintype, sportId);
                if (athleteDto.Id > 0)
                {
                    athleteDto.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athleteDto.Id);
                    athleteDto.AtheleteVideos = Dal2.GetAtheleteIdealVideos2_v1();
                    string email = string.Empty;
                    if (athleteDto.LoginType == 1 || athleteDto.LoginType == 4)
                    {
                        email = athleteDto.Emailaddress;
                    }
                    else if (athleteDto.LoginType == 2 || (athleteDto.LoginType == 3))
                    { email = athleteDto.UserName; }

                    List<VideoDTO> uploadedVideos = Dal2.GetAtheleteVideos2_v1(email, athleteDto.LoginType);
                    if (uploadedVideos.Count > 0)
                    {
                        foreach (var av in athleteDto.AtheleteVideos)
                        {
                            foreach (var uv in uploadedVideos)
                            {
                                if (av.VideoNumber == uv.VideoNumber)
                                {
                                    av.UploadVideoId = uv.Id;
                                    av.UploadDuration = uv.Duration;
                                    av.UploadThumbnailUrl = uv.ThumbnailURL;
                                    av.UploadVideoUrl = uv.VideoURL;
                                }
                            }
                        }
                    }
                  //  athleteDto.AthleteVideosCount = uploadedVideos.Count;
                    List<VideoDTO> shareAtheleteVideoDto = DAL2V5.GetShareTOAthleteVideosById(athleteDto.Id);
                    athleteDto.SharedAtheleteVideos = shareAtheleteVideoDto;
                    athleteDto.AthleteVideosCount = athleteDto.SharedAtheleteVideos.Count;
                    athleteDto.AthleteMediaContent = DAL2V5.GetAthleteMediaContentById(athleteDto.Id);
                    int measurementcount = atc.GetMeasurementCount(athleteDto.Height, athleteDto.Height2,
                        athleteDto.Weight,
                        athleteDto.WingSpan, athleteDto.ShoeSize);
                    athleteDto.MeasurementCount = measurementcount;
                }
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athleteDto;
                _result.Message = CustomConstants.Details_Get_Successfully;
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V4/AtheleteProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
         

    }


}