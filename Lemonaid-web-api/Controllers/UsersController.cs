﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Lemonaid_web_api.Controllers
{
    public class UsersController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddDeviceForUser(DeviceDTO DeviceDto)
        {
            int DeviceID = 0;
            try
            {
                DeviceID = DAL2V6.AddDeviceForUser(DeviceDto);

                if (DeviceID > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = DeviceID;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = string.Empty;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Users/AddDeviceForUser", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
    }
}
