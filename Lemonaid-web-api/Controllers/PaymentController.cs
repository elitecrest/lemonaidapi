﻿using Lemonaid_web_api.Models;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Lemonaid_web_api.Controllers
{
    public class PaymentController : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddPayment(PaymentDTO paymentDto)
        {
            try
            {   
                int id = UsersDal.AddPayment(paymentDto);
                if (id > 0)
                {
                    _result.Response = id;
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.Payment_Added_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Record_Not_Added;
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Payment/AddPayment", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;
        }


        [HttpGet]
        public Utility.CustomResponse GetPayment(int userId, int userType)
        {
            try
            {
                var payment = UsersDal.GetPayment(userId, userType);
                if (payment.UserID == 0)
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = payment;
                    _result.Message = CustomConstants.Details_Get_Successfully;

                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Payment/GetPayment", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddPayment_v5(PaymentDTO paymentDto)
        {
            try
            {
               
                int id = UsersDalV5.AddPayment(paymentDto);
                if (id > 0)
                {
                    _result.Response = id;
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.Payment_Added_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Record_Not_Added;
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Payment/AddPayment_v5", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;
        }


        [HttpGet]
        public Utility.CustomResponse GetPayment_v5(int userId, int userType)
        { 
            try
            {

                var payment = UsersDalV5.GetPayment(userId, userType);
                if (payment.UserID == 0)
                {

                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = false;
                    _result.Message = CustomConstants.Details_Get_Successfully;

                }
                else
                {
                    if (payment.UserType == 1 & payment.SubscriptionType.ToUpper() == "L")
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = payment;
                        _result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else if (payment.SubscriptionType.ToUpper() == "M" || payment.SubscriptionType.ToUpper() == "Y")
                    {
                        if (payment.DeviceType.ToLower() == "iphone")
                        {
                            if (Convert.ToDateTime(payment.ExpiryDate) < DateTime.Now)
                            {
                                JavaScriptSerializer serializer1 = new JavaScriptSerializer();
                                serializer1.MaxJsonLength = 1000000000;
                                HttpClient client = new HttpClient();
                                // client.BaseAddress = new Uri("https://sandbox.itunes.apple.com/verifyReceipt"); //Sandbox
                                //client.BaseAddress = new Uri("https://buy.itunes.apple.com/verifyReceipt"); //Production
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                                AppleDTO objInputDt = new AppleDTO { password = "74461cb0eda04c399ffc26f4a64ad813", receiptdata = payment.PaymentToken };


                                HttpResponseMessage response = client.PostAsJsonAsync(payment.Link, objInputDt).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    Stream st1 = response.Content.ReadAsStreamAsync().Result;
                                    StreamReader reader1 = new StreamReader(st1);
                                    string content = reader1.ReadToEnd();
                                    AppleOutputDTO appleOutputDto = JsonDeserialize<AppleOutputDTO>(content);
                                    DateTime expiredDate = Convert.ToDateTime(appleOutputDto.receipt.expires_date_formatted.Substring(0, 10).Trim());

                                    //Should change to appleOutputDTO.latest_recept_info


                                    //Update ExpiredDate to DB
                                    if (expiredDate >= Convert.ToDateTime(payment.ExpiryDate))
                                    {
                                        UsersDalV5.UpdatePaymentExpiryDate(payment.UserID.ToString(), expiredDate);

                                        _result.Status = Utility.CustomResponseStatus.Successful;
                                        _result.Response = true;
                                        _result.Message = CustomConstants.Details_Get_Successfully;
                                    }
                                    else
                                    {
                                        //User didnt pay the amount

                                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                                        _result.Response = false;
                                        _result.Message = CustomConstants.Details_Get_Successfully;
                                    }


                                }
                            }
                            else
                            {
                                //Date Expires 

                                _result.Status = Utility.CustomResponseStatus.Successful;
                                _result.Response = true;
                                _result.Message = CustomConstants.Details_Get_Successfully;
                            }



                        }
                        else
                        {
                            if (Convert.ToDateTime(payment.ExpiryDate) < DateTime.Now)
                            {
                                //Android
                                string clientId = ConfigurationManager.AppSettings["client_id"];
                                string clientSecret = ConfigurationManager.AppSettings["client_secret"];
                                string grantType = ConfigurationManager.AppSettings["grant_type"];
                                string refreshToken = ConfigurationManager.AppSettings["refresh_token"];
                                string monthlySubscriptionId = ConfigurationManager.AppSettings["MonthlySubscriptionId"];
                                string yearlySubscriptionId = ConfigurationManager.AppSettings["YearlySubscriptionId"];
                                string subscriptionId = (payment.SubscriptionType == "M") ? monthlySubscriptionId : yearlySubscriptionId;
                                string packageName = ConfigurationManager.AppSettings["PackageName"];
                                string URLAuth = "https://accounts.google.com/o/oauth2/token";

                                string postString = string.Format("grant_type={0}&client_secret={1}&client_id={2}&refresh_token={3}", grantType, clientSecret, clientId, refreshToken);

                                const string contentType = "application/x-www-form-urlencoded";
                                ServicePointManager.Expect100Continue = false;

                                CookieContainer cookies = new CookieContainer();
                                HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
                                if (webRequest != null)
                                {
                                    webRequest.Method = "POST";
                                    webRequest.ContentType = contentType;
                                    webRequest.CookieContainer = cookies;
                                    webRequest.ContentLength = postString.Length;
                                    //  webRequest.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1";
                                    //  webRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
                                    //  webRequest.Referer = "https://accounts.craigslist.org";

                                    StreamWriter requestWriter = new StreamWriter(webRequest.GetRequestStream());
                                    requestWriter.Write(postString);
                                    requestWriter.Close();

                                    StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
                                    string responseData = responseReader.ReadToEnd();

                                    AndriodOutputDTO andOutputDto = JsonDeserialize<AndriodOutputDTO>(responseData);
                                    responseReader.Close();
                                    webRequest.GetResponse().Close();

                                    string accessUrl = "https://www.googleapis.com/androidpublisher/v1/applications/" + packageName + "/subscriptions/" + subscriptionId + "/purchases/" + payment.PaymentToken + "?access_token=" + andOutputDto.access_token;
                                    JavaScriptSerializer serializer2 = new JavaScriptSerializer();
                                    serializer2.MaxJsonLength = 1000000000;
                                    HttpClient client1 = new HttpClient();
                                    client1.DefaultRequestHeaders.Accept.Clear();
                                    client1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    HttpResponseMessage response1 = client1.GetAsync(accessUrl).Result;
                                    if (response1.IsSuccessStatusCode)
                                    {
                                        Stream st2 = response1.Content.ReadAsStreamAsync().Result;
                                        StreamReader reader2 = new StreamReader(st2);
                                        string content1 = reader2.ReadToEnd();
                                        AndriodExpiryTimeDTO andexpiryOutputDto = JsonDeserialize<AndriodExpiryTimeDTO>(content1);

                                        DateTime expiredDate = UnixTimeStampToDateTime(andexpiryOutputDto.validUntilTimestampMsec);


                                        //Update ExpiredDate to DB
                                        if (expiredDate >= Convert.ToDateTime(payment.ExpiryDate))
                                        {
                                            UsersDalV5.UpdatePaymentExpiryDate(payment.UserID.ToString(), expiredDate);

                                            _result.Status = Utility.CustomResponseStatus.Successful;
                                            _result.Response = true;
                                            _result.Message = CustomConstants.Details_Get_Successfully;
                                        }
                                        else
                                        {
                                            //User didnt pay the amount

                                            _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                                            _result.Response = false;
                                            _result.Message = CustomConstants.Details_Get_Successfully;
                                        }


                                    }
                                }
                            }
                            else
                            {
                                //Date not expired
                                _result.Status = Utility.CustomResponseStatus.Successful;
                                _result.Response = true;
                                _result.Message = CustomConstants.Details_Get_Successfully;
                            }
                        }
                    }

                    //PaymentStatus = true;
                    //Result.Status = Utility.CustomResponseStatus.Successful;
                    //Result.Response = PaymentStatus;
                    //Result.Message = CustomConstants.Details_Get_Successfully;
                }



            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Payment/GetPayment_v5", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;
        }
         
        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;

        }

       
    }
}
