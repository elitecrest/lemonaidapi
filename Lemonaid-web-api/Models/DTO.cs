﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace Lemonaid_web_api.Models
{
    [AttributeUsage(AttributeTargets.Property,
                Inherited = false,
                AllowMultiple = false)]
    internal sealed class OptionalAttribute : Attribute
    {
    }


    public partial class AthleteDTO
    {
        public AthleteDTO()
        {
            this.AtheleteBesttimes = new List<AthleteEventDTO>();
            this.AtheleteVideos = new List<VideoDTO>();
            this.AthleteBadges = new BadgesDTO();
            this.AthleteMediaContent = new List<MediaContent>();
            this.SharedAtheleteVideos = new List<VideoDTO>();
            this.CityId = 0;

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string School { get; set; }
        public string Emailaddress { get; set; }
        public string Description { get; set; }
        public double GPA { get; set; }
        public int Social { get; set; }
        public int SAT { get; set; }
        public int ACT { get; set; }
        public int InstituteId { get; set; }
        public string ProfilePicURL { get; set; }
        public string DOB { get; set; }
        public string Address { get; set; }
        public string FacebookId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int TotalCount { get; set; }
        public bool Liked { get; set; }
        public int FriendsInApp { get; set; }
        public int SharesApp { get; set; }
        public int AverageRating { get; set; }
        public int AcademicLevel { get; set; } //1-HighSchool 2-College
        public int YearLevel { get; set; } // 1- FR , 2-SO, 3-JR, 4-SR
        public int Class { get; set; }
        public int FriendsRate { get; set; }
        public int ShareRate { get; set; }
        public int SocialRate { get; set; }
        public int AcademicRate { get; set; }
        public int AtheleteRate { get; set; }
        public DateTime FavDate { get; set; }
        public DateTime InvitationDate { get; set; }
        public string CoverPicURL { get; set; }
        public int AdType { get; set; }
        public int Premium { get; set; }
        public int CoachId { get; set; }
        public int AtheleteId { get; set; }
        [Optional]
        public string Height { get; set; }
        [Optional]
        public string Height2 { get; set; }
        [Optional]
        public string HeightType { get; set; }
        [Optional]
        public string Weight { get; set; }
        [Optional]
        public string WeightType { get; set; }
        [Optional]
        public string WingSpan { get; set; }
        [Optional]
        public string WingSpanType { get; set; }
        [Optional]
        public string ShoeSize { get; set; }
        [Optional]
        public string ShoeSizeType { get; set; }
        public string ClubName { get; set; }
        public string CoachName { get; set; }
        public string CoachEmailId { get; set; }
        public int AllAmerican { get; set; }
        public int AcademicAllAmerican { get; set; }
        public int Captain { get; set; }
        public int SwimswamTop { get; set; }
        public string Status { get; set; }
        public int AtheleteType { get; set; }
        public bool Active { get; set; }
        public string UniversityName { get; set; }
        public string UniversityProfilePic { get; set; }
        public string UniversityEmailaddress { get; set; }
        public string UniversityAddress { get; set; }
        public string UniversityPhoneNumber { get; set; }
        public string UniversitySize { get; set; }
        public string UniversityClassificationName { get; set; }
        public int MeasurementCount { get; set; }
        public string Major { get; set; }
        public string Phonenumber { get; set; }
        public int TotalLikedCount { get; set; }
        public bool Diamond { get; set; }
        public string UserName { get; set; }
        public int LoginType { get; set; }
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }
        public int ClubId { get; set; }
        public bool RightSwiped { get; set; }
        public int ChatCount { get; set; }
        [Optional]
        public int SportId { get; set; }
        public string VerticalJump { get; set; }
        public string BroadJump { get; set; }
        public List<AthleteEventDTO> AtheleteBesttimes { get; set; }
        // public List<VideoDTO> IdealVideos { get; set; }
        public List<VideoDTO> AtheleteVideos { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int QuestionId { get; set; }
        public string Question { get; set; }
        public string SchoolCoachName { get; set; }
        public string SchoolCoachEmail { get; set; }
        public string SchoolZipCode { get; set; }
        public int GapyearLevel { get; set; } //Takingclass..
        public string GapyearDescription { get; set; }
        public string Country { get; set; }
        public string Strokes { get; set; }
        public int Location { get; set; }
        public int CountryId { get; set; }
        public double AP { get; set; }
        public bool isProfileCompleted { get; set; }
        public List<CoachDTO> RecommendedInstitutes { get; set; }
        public List<SportsDTO> Sports { get; set; }
        public int AthleteVideosCount { get; set; }
        public int RecommendedTotalCount { get; set; }
        public int MultiSportCount { get; set; }
        public int AthleteTransition { get; set; }
        public string _20YardShuttleRun { get; set; }
        public string _60YardShuttleRun { get; set; }
        public string KneelingMedBallToss { get; set; }
        public string RotationalMedBallToss { get; set; }
        public bool AnerobicBadge { get; set; }
        public bool AerobicBadge { get; set; }
        public bool StrengthBadge { get; set; }
        public bool AgilityBadge { get; set; }
        public bool DiamondBadge { get; set; }
        public int TotalChatCount { get; set; }
        public BadgesDTO AthleteBadges { get; set; }
        public List<MediaContent> AthleteMediaContent { get; set; }
        public List<VideoDTO> SharedAtheleteVideos { get; set; }
        public int AthleteShareVideosCount { get; set; }
        [Optional]
        public int CityId { get; set; }
        [Optional]
        public int BasicStateId { get; set; }
        public string BasicStateName { get; set; }
        [Optional]
        public string CityName { get; set; }
        public int Rank { get; set; }
    }



    public partial class CoachDTO
    {
        public CoachDTO()
        {
            this.CoachVideos = new List<VideoDTO>();

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string AreasInterested { get; set; }
        public string Address { get; set; }
        public string Emailaddress { get; set; }
        public string Description { get; set; }
        public int YearOfEstablish { get; set; }
        public int NCAA { get; set; }
        public int Conference { get; set; }
        public int AdmissionStandard { get; set; }
        public int AverageRating { get; set; }
        public string FacebookId { get; set; }
        public string ClassificationName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string WebsiteURL { get; set; }
        public string FaidURL { get; set; }
        public string ApplURL { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string EnrollmentNo { get; set; }
        public string ApplicationNo { get; set; }
        public string AdmissionNo { get; set; }
        public string ProfilePicURL { get; set; }
        public string CoverPicURL { get; set; }
        public int TotalCount { get; set; }
        public bool Liked { get; set; }
        public int NoOfAthletesInApp { get; set; } //No of Athletes
        public int AcceptanceRate { get; set; }
        public int NCAARate { get; set; }
        public int ConferenceRate { get; set; }
        public int AdmissionRate { get; set; }
        public int SocialRate { get; set; }
        public int AtheleteRate { get; set; }
        public DateTime FavDate { get; set; }
        public DateTime InvitationDate { get; set; }
        public bool Accept { get; set; }
        public int TotalAtheleteAcceptedCount { get; set; }
        public int Cost { get; set; }
        public int MensTeam { get; set; }
        public int WomenTeam { get; set; }
        public int Size { get; set; }
        public int AdType { get; set; }
        public int CoachId { get; set; }
        public int AtheleteId { get; set; }
        public string AtheleteName { get; set; }
        public string AtheleteProfilePic { get; set; }
        public bool CoachActiveStatus { get; set; }
        public string Phonenumber { get; set; }
        public bool HeadCoach { get; set; }
        public string PreferredStrokes { get; set; }
        public int AcademicRate { get; set; }
        public bool Payment_Paid { get; set; }
        public bool Recommended { get; set; }
        public List<AthleteDTO> RecommendedAtheletes { get; set; }
        public bool RightSwiped { get; set; }
        public int ChatCount { get; set; }
        public string InstituteEmailaddress { get; set; }
        public bool RecommendedByFFF { get; set; }
        public int QuestionId { get; set; }
        public string Question { get; set; }
        public string AthleticConference { get; set; }
        public string EnrollmentFee { get; set; }
        public string Type { get; set; }
        public string TuitionFee { get; set; }
        public string Scholarship { get; set; }
        public string ScholarshipMen { get; set; }
        public string ScholarshipWomen { get; set; }
        public List<VideoDTO> CoachVideos { get; set; }
        public string Enabled { get; set; }
        public float AdmittanceRate { get; set; }
        public int Rank { get; set; }
        public int TotalChatCount { get; set; }
        public string TuitionInState { get; set; }
        public string TuitionOutState { get; set; }
        public int AtheleteCoachType { get; set; }
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; } 
        public int PlanId { get; set; }
        public string PlanName { get;   set; }
        public string SubscriptionType { get;   set; }
        public string Price { get;   set; }
        public string VirtualVideoURL { get;   set; }
        public string MatchesVirtualVideoURL { get;   set; }
        public string VirtualThumbnailURL { get;   set; }
        public string MatchesVirtualThumbnailURL { get;   set; }
        public string StripeUserId { get;   set; }
        public string CardToken { get;   set; }
        public int TeamType { get;   set; }
        public int Coach360VideosCnt { get;   set; }
        public string AthleteMatchesVirtualVideoURL { get;   set; }
        public string AthleteMatchesVirtualThumbnailURL { get;   set; }
    }

    public partial class CoachUserDTO
    {
        public int CoachId { get; set; }
        public int InstituteId { get; set; }
        public bool Active { get; set; }
        public string Emailaddress { get; set; }
        public string Password { get; set; }
        public int TeamType { get; set; }
        public string ActivationCode { get; set; }
        public bool HeadCoach { get; set; }
        public string Name { get; set; }
        public bool Payment_Paid { get; set; }
        public string PreferredStrokes { get; set; }
        public string ProfilePicURL { get; set; }
        [Optional]
        public string DeviceType { get; set; }
        [Optional]
        public string DeviceId { get; set; }
        public int TotalCount { get; set; }
        public int ChatCount { get; set; }
        [Optional]
        public string EncryptedPassword { get; set; }
        public int SportId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public partial class CoachProfileDTO
    {
        public CoachProfileDTO()
        {
            CoachVideos = new List<VideoDTO>();
        }
        public string Phonenumber { get; set; }

        public string Description { get; set; }
        public int CoachId { get; set; }
        public string Emailaddress { get; set; }
        public bool HeadCoach { get; set; }
        [Optional]
        public int AcademicRate { get; set; }
        public bool payment_paid { get; set; }
        public bool Payment_Paid { get; set; }
        public int InstituteId { get; set; }
        [Optional]
        public object FacebookURL { get; set; }
        [Optional]
        public object TwitterURL { get; set; }
        [Optional]
        public object InstagramURL { get; set; }
        [Optional]
        public object YoutubeURL { get; set; }
        public string EncryptedPassword { get; set; }
        public string CoachName { get; set; }
        public string Title { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Accolades { get; set; }
        public string CollegeName { get; set; }
        public int Year { get; set; }
        public List<VideoDTO> CoachVideos { get; set; }
        public string InstituteName { get; set; }
        public string InstituteState { get; set; }
        public string InstituteCity { get; set; }
        public string PreferredStrokes { get; set; }
        public int Gender { get; set; }
        public int Location { get; set; }
        public int AthleteRate { get; set; }
        public int HsYear { get; set; }
        public string ProfilePicURL { get; set; }
    }

    public class ResponseDTO
    {
        public CoachUserDTO response { get; set; }
        public string stat { get; set; }
    }
    public class EnrollStatusDTO
    {
        public string stat { get; set; }
        public string response { get; set; }
    }

    public class EmailContentDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Emailaddress { get; set; }
        public string Rightswipes { get; set; }
    }
    public partial class CoachRatingsDTO
    {
        public int NCAA { get; set; }
        public int Conference { get; set; }
        public int AdmissionStandard { get; set; }
        public int AcceptanceRate { get; set; }
        public int NoOfAthletesInApp { get; set; }
        public int Id { get; set; }

    }

    public partial class AthleteEventDTO
    {
        [Optional]
        public int AtheleteId { get; set; }
        public string Emailaddress { get; set; }
        public int EventTypeId { get; set; }
        public string BesttimeValue { get; set; }
        public string EventName { get; set; }

    }

    public partial class ChatDTO
    {
        public int SenderId { get; set; }
        public int ReceiverId { get; set; }
        public int SenderType { get; set; }
        public int ReceiverType { get; set; }
        public DateTime Date { get; set; }
        public string Message { get; set; }
        public int Offset { get; set; }
        public int Max { get; set; }
        public int Count { get; set; }
        public string Name { get; set; }
        public string ProfilePicURL { get; set; }

    }

    public partial class AtheleteMatchDTO
    {
        public string AtheleteFBId { get; set; }
        public string CoachFBId { get; set; } //InstituteId
        public bool Status { get; set; }
        [Optional]
        public string AtheleteId { get; set; }
        [Optional]
        public string InstituteId { get; set; }
    }

    public partial class AtheleteBadgesDTO
    {
        public int AllAmerican { get; set; }
        public int AcademicAllAmerican { get; set; }
        public int Captain { get; set; }
        public int SwimswamTop { get; set; }
        //public string MultiSport { get; set; }
        public int AtheleteId { get; set; }
        public string Status { get; set; }
        public object BadgeTypeId { get; set; }
        public object BadgeCount { get; set; }
        public List<CoachDTO> Coaches { get; set; }
    }

    public partial class AtheleteSocialDTO
    {
        public string ClubName { get; set; }
        public string CoachName { get; set; }
        public string CoachEmailId { get; set; }
        public string AthleteName { get; set; }
        public int AtheleteId { get; set; }
        public int BadgeTypeId { get; set; }
        public int Count { get; set; }
    }

    public partial class FacebookDTO
    {
        public string id { get; set; }
        public string first_name { get; set; }
        public string gender { get; set; }
        public string last_name { get; set; }
        public string link { get; set; }
        public string locale { get; set; }
        public string name { get; set; }
        public string username { get; set; }
    }


    public partial class EventTypesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public partial class HelpDTO
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public DateTime Date { get; set; }
        public string ThumbNailURL { get; set; }
        public int VideoType { get; set; } // 1- Server 2. youtube whereas type: 1- FAQ 2 - Video
    }
    public partial class FriendsSharesDTO
    {
        [Optional]
        public int AtheleteId { get; set; }
        public string AtheleteEmail { get; set; }
        public int Friends { get; set; }
        public int Shares { get; set; }
        public int Flag { get; set; }
    }
    public partial class BadgesDTO
    {
        public bool Academic { get; set; }
        public bool Social { get; set; }
        public bool Athelete { get; set; }
        public bool Diamond { get; set; }
    }


    public partial class BlogDTO
    {
        public int Id { get; set; }
        public int CoachId { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public string ImageURL { get; set; }
        public string URL { get; set; }
        public DateTime? Date { get; set; }
        public string Type { get; set; }
        public string ShortDesc { get; set; }
        public int TotalCount { get; set; }
    }




    public partial class AtheleteInvitationDTO
    {
        public string AtheleteEmailAddress { get; set; }
        public string CoachEmailAddress { get; set; }
        public string InvitationDate { get; set; }
        public bool Accept { get; set; }
        [Optional]
        public int AtheleteId { get; set; }
        [Optional]
        public int InstituteId { get; set; }

    }

    public partial class GetAtheleteInvitationDTO
    {
        public string UniversityName { get; set; }
        public string InvitationDate { get; set; }
    }


    public partial class AtheleteStrokesDTO
    {
        public int AtheleteId { get; set; }
        public string Category { get; set; }

    }

    public partial class AdDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ProfilePicURL { get; set; }
        public string VideoURL { get; set; }
        public int AdType { get; set; }

    }


    public partial class AtheleteBadgesDTO
    {
        public BadgesDTO AtheletesBadgesDTO { get; set; }
        public List<GetAtheleteInvitationDTO> AtheleteInvitationDTO { get; set; }
        public List<BlogDTO> BlogsDTO { get; set; }

    }

    public partial class CoachesBadgesDTO
    {
        public BadgesDTO coachBadgesDTO { get; set; }
        public List<BlogDTO> BlogsDTO { get; set; }
        public List<AthleteDTO> Athletes { get; set; }

    }

    public partial class UsageDTO
    {
        public string FeatureId { get; set; }
        public int Count { get; set; }
        public string Value { get; set; }
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }

    }

    public partial class VideoDTO
    {
        public VideoDTO()
        {
            this.ExtractedThumbnailURLs = new List<string>();

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string VideoURL { get; set; }
        public string ThumbnailURL { get; set; }
        public int Status { get; set; }
        public string AtheleteEmail { get; set; }
        public string VideoFormat { get; set; }
        public int Count { get; set; }
        [Optional]
        public int VideoType { get; set; }
        [Optional]
        public int VideoNumber { get; set; }
        [Optional]
        public int Duration { get; set; }
        [Optional]
        public int AtheleteId { get; set; }
        public int Firstvideonumber { get; set; }
        public List<string> ExtractedThumbnailURLs { get; set; }
        public int CountAthleteVideos { get; set; }
        public int IsFirstVideoDeleted { get; set; }
        public int CoachId { get; set; }
        public int UploadVideoId { get; set; }
        public int UploadDuration { get; set; }
        public string UploadVideoUrl { get; set; }
        public string UploadThumbnailUrl { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int ApproveFromClubCoach { get; set; }
    }

    public partial class ThumbnailDTO
    {
        public int VideoId { get; set; }
        public string ThumbnailURL { get; set; }
        public string ThumbnailFormat { get; set; }
        public string AthleteEmail { get; set; }
        [Optional]
        public int AthleteId { get; set; }
        public int CoachId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }

    public partial class PaymentDTO
    {
        public PaymentDTO()
        {
            this.SKUId = string.Empty;
            this.PlanId = 0;
        }
        public int  ID { get; set; }
        public int UserID { get; set; }
        public int UserType { get; set; }
        public string PaymentToken { get; set; }
        public string DeviceType { get; set; }
        public string PaymentType { get; set; }
        public DateTime CreatedDate { get; set; }
        public object ExpiryDate { get; set; }
        public string Link { get; set; }
        public string SubscriptionType { get; set; }
        public List<SportsDTO> Sports { get; set; }
        public string Emailaddress { get; set; }
        [Optional]
        public string SKUId { get; set; }
        public string TransactionStatus { get; set; }
        public int PlanId { get;  set; }
    }

    public partial class SportsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ThumbnailUrl { get; set; }
        public string SwitchingThumbnailUrl { get; set; }
        public URL urls { get; set; }
        public int Active { get; set; }
        public int AthleteId { get; set; }

    }

    public partial class URL
    {
        public string Test { get; set; }
        public string Staging { get; set; }
        public string Production { get; set; }
    }

    public partial class AtheleteInstituteAccept
    {
        public int AtheleteId { get; set; }
        public int InstituteId { get; set; }
        public string AtheleteEmail { get; set; }
        public string InstituteEmail { get; set; }
    }

    public partial class TeamRoasterDTO
    {
        public List<AthleteDTO> CollegeAtheletes { get; set; }
        public List<CoachUserDTO> CollegeCoaches { get; set; }
    }

    public partial class CollegeAtheleteCoachesDTO
    {
        public CollegeAtheleteCoachesDTO()
        {
            this.AtheleteBesttimes = new List<AthleteEventDTO>();
            this.CoachVideos = new List<VideoDTO>();

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string AreasInterested { get; set; }
        public string Address { get; set; }
        public string Emailaddress { get; set; }
        public string Description { get; set; }
        public int YearOfEstablish { get; set; }
        public int NCAA { get; set; }
        public int Conference { get; set; }
        public int AdmissionStandard { get; set; }
        public int AverageRating { get; set; }
        public string FacebookId { get; set; }
        public string ClassificationName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string WebsiteURL { get; set; }
        public string FaidURL { get; set; }
        public string ApplURL { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string EnrollmentNo { get; set; }
        public string ApplicationNo { get; set; }
        public string AdmissionNo { get; set; }
        public string ProfilePicURL { get; set; }
        public string CoverPicURL { get; set; }
        public int TotalCount { get; set; }
        public bool Liked { get; set; }
        public int NoOfAthletesInApp { get; set; } //No of Athletes
        public int AcceptanceRate { get; set; }
        public int NCAARate { get; set; }
        public int ConferenceRate { get; set; }
        public int AdmissionRate { get; set; }
        public int SocialRate { get; set; }
        public int AtheleteRate { get; set; }
        public int Cost { get; set; }
        public int MensTeam { get; set; }
        public int WomenTeam { get; set; }
        public int Size { get; set; }
        public string Phonenumber { get; set; }
        public DateTime FavDate { get; set; }
        public DateTime InvitationDate { get; set; }
        public bool Accept { get; set; }
        public int CoachId { get; set; }
        public bool Payment_Paid { get; set; }
        public bool Recommended { get; set; }
        public string AthleticConference { get; set; }
        public string EnrollmentFee { get; set; }
        public string Type { get; set; }
        public string TuitionFee { get; set; }
        public string Scholarship { get; set; }
        public string ScholarshipMen { get; set; }
        public string ScholarshipWomen { get; set; }
        public string TuitionInState { get; set; }
        public string TuitionOutState { get; set; }

        public int AtheleteId { get; set; }
        public string AtheleteName { get; set; }
        public int Gender { get; set; }
        public string School { get; set; }
        public string AtheleteEmailaddress { get; set; }
        public string AtheleteDescription { get; set; }
        public double GPA { get; set; }
        public int Social { get; set; }
        public int SAT { get; set; }
        public int ACT { get; set; }
        public int InstituteId { get; set; }
        public string AtheleteProfilePicURL { get; set; }
        public string DOB { get; set; }
        public string AtheleteAddress { get; set; }
        public string AtheleteFacebookId { get; set; }
        public string AtheleteLatitude { get; set; }
        public string AtheleteLongitude { get; set; }
        public int FriendsInApp { get; set; }
        public int SharesApp { get; set; }
        public int AtheleteAverageRating { get; set; }
        public int AcademicLevel { get; set; } //1-HighSchool 2-College
        public int YearLevel { get; set; } // 1- FR , 2-SO, 3-JR, 4-SR
        public int Class { get; set; }
        public int FriendsRate { get; set; }
        public int ShareRate { get; set; }
        public int AtheleteSocialRate { get; set; }
        public int AcademicRate { get; set; }
        public int AtheleteAtheleteRate { get; set; }
        public string AtheleteCoverPicURL { get; set; }
        public int AdType { get; set; }
        public int Premium { get; set; }
        [Optional]
        public string Height { get; set; }
        [Optional]
        public string Height2 { get; set; }
        [Optional]
        public string HeightType { get; set; }
        [Optional]
        public string Weight { get; set; }
        [Optional]
        public string WeightType { get; set; }
        [Optional]
        public string WingSpan { get; set; }
        [Optional]
        public string WingSpanType { get; set; }
        [Optional]
        public string ShoeSize { get; set; }
        [Optional]
        public string ShoeSizeType { get; set; }
        public string ClubName { get; set; }
        public string CoachName { get; set; }
        public string CoachEmailId { get; set; }
        public int AllAmerican { get; set; }
        public int AcademicAllAmerican { get; set; }
        public int Captain { get; set; }
        public int SwimswamTop { get; set; }
        public string Status { get; set; }
        public int AtheleteType { get; set; }
        public bool Active { get; set; }
        public int AtheleteCoachType { get; set; } // 0 - athelete , 1 - Coach
        public DateTime AtheleteFavDate { get; set; }
        public DateTime AtheleteInvitationDate { get; set; }
        public bool AtheleteLiked { get; set; }
        public List<AthleteEventDTO> AtheleteBesttimes { get; set; }
        public string Major { get; set; }
        public string AtheletePhonenumber { get; set; }
        public int TotalLikedCount { get; set; }
        public int QuestionId { get; set; }
        public string Question { get; set; }
        public bool Diamond { get; set; }
        public bool RightSwiped { get; set; }
        public int ChatCount { get; set; }
        [Optional]
        public int SportId { get; set; }
        public bool RecommendedByFFF { get; set; }
        public List<VideoDTO> CoachVideos { get; set; }
        public int CountryId { get; set; }
        public double AP { get; set; }
        public string VerticalJump { get; set; }
        public string BroadJump { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public string SchoolCoachName { get; set; }
        public string SchoolCoachEmail { get; set; }
        public string SchoolZipCode { get; set; }
        public int GapyearLevel { get; set; } //Takingclass..
        public string GapyearDescription { get; set; }
        public string Country { get; set; }
        public string Strokes { get; set; }
        public int Location { get; set; }
        public bool isProfileCompleted { get; set; }
        public int AthleteVideosCount { get; set; }
        public int RecommendedTotalCount { get; set; }
        public int MultiSportCount { get; set; }
        public int AthleteTransition { get; set; }
        public string _20YardShuttleRun { get; set; }
        public string _60YardShuttleRun { get; set; }
        public string KneelingMedBallToss { get; set; }
        public string RotationalMedBallToss { get; set; }
        public string UserName { get; set; }
        public int LoginType { get; set; }

        public int TotalChatCount { get; set; }
        public BadgesDTO AthleteBadges { get; set; }
        public List<VideoDTO> SharedAtheleteVideos { get; set; }

        public string DeviceId { get; set; }

        public string DeviceType { get; set; }

        public string VirtualVideoURL { get; set; }
        public string MatchesVirtualVideoURL { get; set; }
        public string VirtualThumbnailURL { get; set; }
        public string MatchesVirtualThumbnailURL { get; set; }

    }

    public partial class AddTeamRoasterDTO
    {
        public int InstituteId { get; set; }
        public int AtheleteId { get; set; }

    }
    public partial class AtheleteCollegeAtheleteDTO
    {
        public string AtheleteEmailAddress { get; set; }
        public int CollegeAtheleteId { get; set; }
        public bool Status { get; set; }
        [Optional]
        public int AtheleteId { get; set; }

    }

    public partial class QuestionaireDTO
    {
        [Optional]
        public string Question { get; set; }
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public int AtheleteId { get; set; }
        public String UserType { get; set; }
    }
    public partial class NotificationDTO
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int NotificationId { get; set; }
        public int Status { get; set; }
        public int SenderId { get; set; }
        public string Message { get; set; }
        public int NotificationType { get; set; }
        public DateTime Date { get; set; }
        public int TotalCount { get; set; }
        public string ShortMessage { get; set; }
        public string LongMessage { get; set; }
        public string ProfilePicURL { get; set; }
        public int BadgeTypeId { get; set; }
        public int BadgeCount { get; set; }
        public int SenderType { get; set; }
        public string SenderName { get; set; }
        public int ReceiverType { get; set; }  
    }

    public partial class ClubCoachDTO
    {
        public ClubCoachDTO()
        {
            Almamater = string.Empty;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string Emailaddress { get; set; }
        public string Description { get; set; }
        public string ProfilePicURL { get; set; }
        public string DOB { get; set; }
        public string ClubAddress { get; set; }
        public string FacebookId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CoverPicURL { get; set; }
        public string ClubName { get; set; }
        public bool Active { get; set; }
        public string Phonenumber { get; set; }
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }
        public int TeamType { get; set; }
        public string Location { get; set; }
        public string SchoolName { get; set; }
        public string ZipCode { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int SchoolType { get; set; }
        public int SportId { get; set; }
        public string ClubPhoneNumber { get; set; }
        public int SchoolId { get; set; }
        public int ClubId { get; set; }
        public int LoginType { get; set; }
        public string Username { get; set; }
        [Optional]
        public string Almamater { get; set; }
        public int ExistedId { get; set; }
        public string ExistedEmailAddress { get; set; }
        public int ExistedLoginType { get; set; }
        public string ExistedUsername { get; set; }
        public string TrainedSport { get; set; }
    }

    public partial class ClubCoachMatchDTO
    {
        public int ClubCoachId { get; set; }
        public int InstituteId { get; set; }
        public bool Status { get; set; }
    }

    public partial class ClubCoachInvitationDTO
    {
        public string AtheleteEmail { get; set; }
        public string ClubCoachEmail { get; set; }
        public int AtheleteId { get; set; }
        public int ClubCoachId { get; set; }


    }

    public partial class ClubDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
    }


    public partial class SupportMailDTO
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string SSL { get; set; }

    }

    public class EmailDTO
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public int Status { get; set; }
        public string MailType { get; set; }
    }


    public partial class AthleteWrapperDTO
    {
        public int Id { get; set; }
        public string Emailaddress { get; set; }
        public string UserName { get; set; }
        public int LoginType { get; set; }
        public string UserType { get; set; }
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }
    }

    public partial class AthleteSportsDTO
    {
        public int Id { get; set; }
        public int AthleteId { get; set; }
        public int SportId { get; set; }
        public string SportName { get; set; }
        public int UserId { get; set; }
    }

    public partial class AthleteThumbnailDto
    {
        public int Id { get; set; }
        public int AthleteId { get; set; }
        public int VideoId { get; set; }
        public string ThumbnailURL { get; set; }
        public int VideoNumber { get; set; }
    }

    #region Extracting States

    public class Location
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class Southwest
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class Northeast
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class Viewport
    {
        public Southwest southwest { get; set; }
        public Northeast northeast { get; set; }
    }

    public class Southwest2
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class Northeast2
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class Bounds
    {
        public Southwest2 southwest { get; set; }
        public Northeast2 northeast { get; set; }
    }

    public class Geometry
    {
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
        public Bounds bounds { get; set; }
    }

    public class Result
    {
        public object type { get; set; }
        public string formatted_address { get; set; }
        public object address_component { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> postcode_locality { get; set; }
    }

    public class GeocodeResponse
    {
        public string status { get; set; }
        public List<Result> result { get; set; }
    }

    public class RootObject
    {
        public GeocodeResponse GeocodeResponse { get; set; }
    }

    #endregion ;



    public partial class FamilyFriendsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public string Emailaddress { get; set; }
        public string Description { get; set; }
        public string ProfilePicURL { get; set; }
        public string DOB { get; set; }
        public string FacebookId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Phonenumber { get; set; }
        public string DeviceType { get; set; }
        public string DeviceId { get; set; }
        public int StateId { get; set; }
        public string StateName { get; set; }
        public int SportId { get; set; }
        public string Almamater { get; set; }
        public string Sport { get; set; }
        public int LoginType { get; set; }
        public string Username { get; set; }
    }

    public partial class FamilyFriendsMatchDTO
    {
        public string FamilyFriendsId { get; set; }
        public string AthleteId { get; set; }
        public string InstituteId { get; set; }
        public bool Status { get; set; }
    }


    //Apple DTO
    public class AppleDTO
    {
        [JsonProperty("receipt-data")]
        public string receiptdata { get; set; }
        public string password { get; set; }
    }


    public class Receipt
    {
        public string original_purchase_date_pst { get; set; }
        public string unique_identifier { get; set; }
        public string original_transaction_id { get; set; }
        public string expires_date { get; set; }
        public string transaction_id { get; set; }
        public string quantity { get; set; }
        public string product_id { get; set; }
        public string item_id { get; set; }
        public string bid { get; set; }
        public string unique_vendor_identifier { get; set; }
        public string web_order_line_item_id { get; set; }
        public string bvrs { get; set; }
        public string expires_date_formatted { get; set; }
        public string purchase_date { get; set; }
        public string purchase_date_ms { get; set; }
        public string expires_date_formatted_pst { get; set; }
        public string purchase_date_pst { get; set; }
        public string original_purchase_date { get; set; }
        public string original_purchase_date_ms { get; set; }
    }

    public class LatestExpiredReceiptInfo
    {
        public string original_purchase_date_pst { get; set; }
        public string unique_identifier { get; set; }
        public string original_transaction_id { get; set; }
        public string expires_date { get; set; }
        public string transaction_id { get; set; }
        public string quantity { get; set; }
        public string product_id { get; set; }
        public string item_id { get; set; }
        public string bid { get; set; }
        public string unique_vendor_identifier { get; set; }
        public string web_order_line_item_id { get; set; }
        public string bvrs { get; set; }
        public string expires_date_formatted { get; set; }
        public string purchase_date { get; set; }
        public string purchase_date_ms { get; set; }
        public string expires_date_formatted_pst { get; set; }
        public string purchase_date_pst { get; set; }
        public string original_purchase_date { get; set; }
        public string original_purchase_date_ms { get; set; }
    }

    public class AppleOutputDTO
    {
        public Receipt receipt { get; set; }
        public LatestExpiredReceiptInfo latest_expired_receipt_info { get; set; }
        public int status { get; set; }
    }

    public class AndriodDTO
    {
        public string grant_type { get; set; }
        public string client_secret { get; set; }
        public string client_id { get; set; }
        public string refresh_token { get; set; }
    }

    public class AndriodOutputDTO
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }

    }
    public class AndriodExpiryTimeDTO
    {
        public string kind { get; set; }
        public double initiationTimestampMsec { get; set; }
        public double validUntilTimestampMsec { get; set; }
        public string autoRenewing { get; set; }
    }

    public partial class StatesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class CoachFiltersDto
    {
        public int CoachId { get; set; }
        public int AcademicRate { get; set; }
        public string PreferredStrokes { get; set; }
        public int HsYear { get; set; }
        public int Gender { get; set; }
        public int Location { get; set; }
        public int AthleteRate { get; set; }
    }

    public partial class CountryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public partial class SchoolsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public partial class AnalyticsDTO
    {
        public int MonthId { get; set; }
        public int YearId { get; set; }
        public int AthleteId { get; set; }
        public int CoachId { get; set; }
        public string Emailaddress { get; set; }
        public int ProfileViews { get; set; }
        public int ProfileViewsPercentage { get; set; }
        public int RightSwipesGiven { get; set; }
        public int RightSwipesGivenPercentage { get; set; }
        public int RightSwipesReceived { get; set; }
        public int RightSwipesReceivedPercentage { get; set; }
        public int Matches { get; set; }
        public int MatchesPercentage { get; set; }
        public int Rank { get; set; }

    }

    public partial class RanksDTO
    {
        public string Name { get; set; }
        public string ProfilePicURL { get; set; }
        public int Rank { get; set; }
        public int TotalCount { get; set; }
    }

    #region IBMWatson

    public class IBMDTO
    {

        public string ID { get; set; }
        public string Name { get; set; }
        public double Percentage { get; set; }

    }
    public class IBMProperties
    {
        public string Name { get; set; }
        public List<IBMDTO> Result { get; set; }
    }


    public class Child4
    {
        public string id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public double percentage { get; set; }
        public double sampling_error { get; set; }
    }

    public class Child3
    {
        public string id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public double percentage { get; set; }
        public double sampling_error { get; set; }
        public List<Child4> children { get; set; }
    }

    public class Child2
    {
        public string id { get; set; }
        public string name { get; set; }
        public string category { get; set; }
        public double percentage { get; set; }
        public List<Child3> children { get; set; }
    }

    public class Child
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<Child2> children { get; set; }
        public string category { get; set; }
    }

    public class Tree
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<Child> children { get; set; }
    }

    public class IBMWatson
    {
        public string id { get; set; }
        public string source { get; set; }
        public int word_count { get; set; }
        public string processed_lang { get; set; }
        public Tree tree { get; set; }
        public List<object> warnings { get; set; }
    }
    #endregion

    public partial class CoachUnreadCountDTO
    {
        public int CoachId { get; set; }
        public string CoachEmail { get; set; }
        public string InstituteName { get; set; }
        public int Count { get; set; }
        public int InstituteId { get; set; }
        public string DeviceId { get; set; }
    }

    public partial class DeleteAthleteSportDTO
    {
        public string AthleteEmail { get; set; }
        public int AthleteType { get; set; }
    }

    public partial class AthleteFrontRushMatchDTO
    {
        public int AthleteId { get; set; }
        public int FrontRushId { get; set; }
        public bool Status { get; set; }

    }

    public partial class AthleteFavouriteDTO
    {
        public int AthleteId { get; set; }
        public string AthleteEmail { get; set; }
        public int LoginType { get; set; }
        public int SportId { get; set; }
        public int Offset { get; set; }
        public int Max { get; set; }
        public string UserType { get; set; }
    }


    public partial class AthleteDeckDTO
    {
        public int AthleteId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Offset { get; set; }
        public int Max { get; set; }
        public int Ncaa { get; set; }
        public int Conference { get; set; }
        public int Admission { get; set; }
        public int LocationType { get; set; }
        public string Description { get; set; }
        public string SearchText { get; set; }
        public int SportId { get; set; }

    }
    public partial class CoachDeckDTO
    {
        public string emailaddress { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int offset { get; set; }
        public int max { get; set; }
        public int locationType { get; set; }
        public string searchText { get; set; }
        public int SportId { get; set; }
    }
    public partial class ChatCountDTO
    {
        public int ChatCount { get; set; }
        public int TotalChatCount { get; set; }
    }

    public partial class FamilyFriendsDeckDTO
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
        public int offset { get; set; }
        public int max { get; set; }
        public string searchText { get; set; }
        public int familyFriendId { get; set; }
        public int SportId { get; set; }
        public string Emailaddress { get; set; }
        public int loginType { get; set; }
    }

    public partial class CollegeAthleteDeckDTO
    {
        public int atheleteId { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
        public int offset { get; set; }
        public int max { get; set; }
        public int locationType { get; set; }
        public string description { get; set; }
        public string searchText { get; set; }
        public int SportId { get; set; }
    }

    public partial class MediaContent
    {
        public int MediaContentId { get; set; }
        public int AthleteId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Name { get; set; }
        public string VideoURL { get; set; }
        public string VideoFormat { get; set; }
        public string ThumbnailBase64String { get; set; }
        public string ThumbnailURL { get; set; }
        public int Duration { get; set; }
        public int VideoType { get; set; }
        public string Caption { get; set; }
        public int CoachId { get; set; }
        public DateTime RecordedDate { get; set; }
        public int? ClubCoachId { get; set; }
        public int? ApproveFromClubCoach { get; set; }
        public string ClubCoachEmail { get; set; }
        public bool IsShareVideo { get; set; }
        public string CoachEmailId { get; set; }
        public string SchoolCoachEmail { get; set; }
        public int LoginType { get; set; }
    }

    public partial class ClubCoachBlogDTO
    {
        public List<BlogDTO> BlogsDTO { get; set; }
        public List<AthleteDTO> Athletes { get; set; }
        public List<CoachDTO> coaches { get; set; }

    }

    public partial class FamilyfriendsBlogDTO
    {
        public List<BlogDTO> BlogsDTO { get; set; }
        public List<AthleteDTO> Athletes { get; set; }
        public List<CoachDTO> coaches { get; set; }

    }


    public partial class UsageTrackingDTO
    {
        public bool SendFirstMessage { get; set; }
        public bool AllAcademicInfo { get; set; }
        public bool UsedShareButton { get; set; }
        public bool SportSpecificMetrics { get; set; }
        public string TimeSinceLastUseOfApp { get; set; }
    }


    public partial class ClubCoachDeckDTO
    {
        public string latitude { get; set; }
        public string longitude { get; set; }
        public int offset { get; set; }
        public int max { get; set; }
        public string searchText { get; set; }
        public int ClubCoachId { get; set; }
        public int SportId { get; set; }
        public string Emailaddress { get; set; }
        public int loginType { get; set; }
    }

    public partial class Video360TypeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string VideoURL { get; set; }
        public string ThumbNailURL { get; set; }
        public int CoachId { get; set; }
        public int Status { get; set; }
        public string VideoFormat { get; set; }
        public int Duration { get; set; }
        public int VideoType { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Description { get; set; }
    }

    public partial class BlogInputDTO
    {
        public int Id { get; set; }
        public int Offset { get; set; }
        public int Max { get; set; }
        public string SearchText { get; set; }
        public string Emailaddress { get; set; }
    }

    public partial class DeviceDTO
    {
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }
        public int UserId { get; set; }
        public string UserType { get; set; }

    }

    public partial class CityDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int StateID { get;  set; }
    }

    public partial class NotificationsLikesCount
    {
        public int NotificationCount { get; set; }
        public int MutualLikeCount { get; set; }

    }

    public partial class AthleteAdDTO
    {
        public string Emailaddress { get; set; }
        public int LoginType { get; set; }
        public int SportId { get; set; }
        public int AdId { get; set; }
        public bool Status { get; set; }
        public string UserType { get; set; }
    }
    public partial class RefreshDTO
    {
        public int Id { get; set; }
        public string Emailaddress { get; set; }
        public int LoginType { get; set; }
        public string UserType { get; set; }
        public int SportId { get; set; }
    }

    public partial class PushNotificationDTO
    {
        public string Emailaddress { get; set; }
        public int LoginType { get; set; }
        public string UserType { get; set; }
        public string DeviceId { get; set; }
        public string DeviceType { get; set; }
    }


    public partial class UsertypeChatDTO
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public int LoginType { get; set; }
        public string UserName { get; set; }
        public string DeviceType { get; set; }
        public string DeviceID { get; set; }
    }


    public partial class FirebaseDTO
    { 
        public string FirebaseAppId { get; set; }
        public string FirebaseSenderId { get; set; }
    }

    public partial class ClubSchoolCoaches
    { 
        public List<ClubCoachDTO> schoolCoaches { get; set; }
        public List<ClubCoachDTO> clubCoaches { get; set; }
    }

    public partial class ContactUsDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public int CoachId { get; set; }
    }

    public partial class UniversityvirtualDTO
    {
        public int Id { get; set; }
        public int InstituteId { get; set; }
        public string VirtualVideoURL { get; set; } 
        public int AthleteId { get; set; }
        public int CoachId { get; set; }
        public object VirtualThumbnailURL { get; set; }
    }

    public partial class CoachStripeAccDetailsDTO
    {
        public int CoachId { get; set; }
        public string StripeUserId { get; set; }
        [Optional]
        public string CardToken { get; set; }
    }
    

   public partial class CoachVideo360
    { 
        public string SearchText { get; set; }
        public int Offset { get; set; } 
        public int Max { get; set; }
    }

    public partial class NotificationsMessagesCount
    {
        public int NotificationCount { get; set; }
        public int MessageCount { get; set; } 
        
    }

    public partial class MatchesVirtualVideoDTO
    {
        public int CoachId { get; set; }
        public int InstituteId { get; set; } 
        public int AthleteId { get; set; }
        public int VideoFlag { get; set; } //0 - Virtual , 1 - MatchesVirtual
        public int VideoId { get; set; }
        public string VirtualVideoURL { get; set; } 
        public string VirtualThumbnailURL { get; set; }
        public string MatchesVirtualVideoURL { get; set; } 
        public string MatchesVirtualThumbnailURL { get; set; }
        public string AthleteMatchesVirtualVideoURL { get; set; }
        public string AthleteMatchesVirtualThumbnailURL { get; set; }

    }

    public partial class UserNotifications
    {
        public int Userid { get; set; }
        public int Offset { get; set; } 
        public int Max { get; set; } 
        public int SenderType { get; set; }  

    }


}