﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;


namespace Lemonaid_web_api.Controllers
{
    public class Coach2V7Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse UpdateUniversityVirtualVideo(UniversityvirtualDTO universityvirtualDTO)
        {
            try
            {
                int Id =  DAL2V7.UpdateUniversityVirtualVideo(universityvirtualDTO);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = Id;
                _result.Message = CustomConstants.VirtualVideo_Added;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/UpdateUniversityVirtualVideo", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse UpdateUniversityMatchedVirtualVideo(UniversityvirtualDTO universityvirtualDTO)
        {
            try
            {
                int Id = DAL2V7.UpdateUniversityMatchedVirtualVideo(universityvirtualDTO);
                if (Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = Id;
                    _result.Message = CustomConstants.VirtualVideo_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful; 
                    _result.Message = CustomConstants.Record_Not_Added;
                }
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/UpdateUniversityVirtualVideo", "", ex.Message, "Exception", 0);
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse GetCoachVideos360TypeBySearchText(CoachVideo360 coachVideo360)
        {

            try
            {
                List<Video360TypeDTO> lstVideo360Type = DAL2V7.GetCoachVideos360TypeBySearchText(coachVideo360);

                if (lstVideo360Type.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = lstVideo360Type;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V7/GetCoachVideos360TypeBySearchText", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddStripeAccDetails(CoachStripeAccDetailsDTO StripeAccDetails)
        {
            try
            {
                int id = DAL2V7.AddStripeAccDetails(StripeAccDetails);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.AddStripe_AccDetails;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V7/AddStripeAccDetails", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DeleteVirtualMatchesVideos(MatchesVirtualVideoDTO matchesVirtualVideoDTO)
        {
            try
            {
                 DAL2V7.DeleteVirtualMatchesVideos(matchesVirtualVideoDTO);
                _result.Response = matchesVirtualVideoDTO.InstituteId;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.VirtualVideo_Deleted;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V7/DeleteVirtualMatchesVideos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse GetVirtualMatchesVideos(MatchesVirtualVideoDTO matchesVirtualVideoDTO)
        {
            try
            {
                MatchesVirtualVideoDTO  matchesVirtualVideoLst = DAL2V7.GetVirtualMatchesVideos(matchesVirtualVideoDTO); 
                _result.Response = matchesVirtualVideoLst;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.VirtualVideo_Deleted;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V7/GetVirtualMatchesVideos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse GetAthleteInstituteMatchedVirtualVideos(MatchesVirtualVideoDTO matchesVirtualVideoDTO)
        {
            try
            {
                MatchesVirtualVideoDTO matchesVirtualVideoLst = DAL2V7.GetAthleteInstituteMatchedVirtualVideos(matchesVirtualVideoDTO);
                if (matchesVirtualVideoLst.MatchesVirtualThumbnailURL != string.Empty)
                {
                    _result.Response = matchesVirtualVideoLst;
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Response = string.Empty;
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V7/GetAthleteInstituteMatchedVirtualVideos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
    }
}
