﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Lemonaid_web_api.Models;
namespace Lemonaid_web_api.Controllers
{
    public class Athlete2V7Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddCity(CityDTO citydto)
        {

            try
            {

                int id = DAL2V7.AddCity(citydto);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Athlete_Video_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V7/AddCity", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse GetUserNotifications(UserNotifications userNotifications)
        {

            try
            {
                var results = DAL2V7.GetUserNotifications(userNotifications);
                if (results.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = results;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = 0;
                    _result.Message = CustomConstants.No_Notifications;
                }
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

 


    }
}
