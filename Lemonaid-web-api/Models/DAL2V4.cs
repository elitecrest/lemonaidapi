﻿using Lemonaid_web_api.Controllers;
using System;
using System.Collections.Generic;
 
using System.Data;
using System.Data.SqlClient;
 

namespace Lemonaid_web_api.Models
{
    public class DAL2V4
    {
        internal static DeleteAthleteSportDTO DeleteAthleteSport(int atheleteId, int loginType, int sportId)
        {

            DeleteAthleteSportDTO ath = new DeleteAthleteSportDTO();

            try
            {
                string connString = Dal2.GetConnection(sportId);
                SqlConnection myConn = new SqlConnection(connString);
                myConn.Open();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAthleteSport]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlParameter loginTypePar = cmd.Parameters.Add("@loginType", SqlDbType.Int);
                    loginTypePar.Direction = ParameterDirection.Input;
                    loginTypePar.Value = loginType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ath.AthleteEmail = (string)myData["Email"];
                        ath.AthleteType = (int)myData["AthleteType"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/DeleteAthleteSport", "", ex.Message, "Exception", 0);
            }


            return ath;
        }

        internal static int GetAthleteFRChatCount(int frId, int atheleteId)
        {

            int chatCount = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteFRChatCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SenderIdPar = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderIdPar.Direction = ParameterDirection.Input;
                    SenderIdPar.Value = frId;

                    SqlParameter ReceiverIdPar = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverIdPar.Direction = ParameterDirection.Input;
                    ReceiverIdPar.Value = atheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        chatCount = (int)myData["ChatCount"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV4/GetChatCount", "", ex.Message, "Exception", 0);
                throw;
            }

            return chatCount;
        }

        internal static int DeleteAthleteSportFromWrapper(string emailaddress, int loginType, int deletedSportId, string userType, int activatedSportId)
        {

            int id = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteAthleteWithNoSports]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailAddressPar = cmd.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 100);
                    emailAddressPar.Direction = ParameterDirection.Input;
                    emailAddressPar.Value = emailaddress;

                    SqlParameter loginTypePar = cmd.Parameters.Add("@loginType", SqlDbType.Int);
                    loginTypePar.Direction = ParameterDirection.Input;
                    loginTypePar.Value = loginType;

                    SqlParameter userTypePar = cmd.Parameters.Add("@UserType", SqlDbType.VarChar, 50);
                    userTypePar.Direction = ParameterDirection.Input;
                    userTypePar.Value = userType;

                    SqlParameter deletedSportIdPar = cmd.Parameters.Add("@deletedSportId", SqlDbType.Int);
                    deletedSportIdPar.Direction = ParameterDirection.Input;
                    deletedSportIdPar.Value = deletedSportId;

                    SqlParameter activatedSportIdPar = cmd.Parameters.Add("@activateSportId", SqlDbType.Int);
                    activatedSportIdPar.Direction = ParameterDirection.Input;
                    activatedSportIdPar.Value = activatedSportId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        id = (int)myData["ID"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/DeleteAthleteSport", "", ex.Message, "Exception", 0);
            }


            return id;
        }



        internal static List<AdDTO> GetFrontRushCards(int athleteId)
        {

            List<AdDTO> AdDTO = new List<AdDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFrontRushCards]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteIdPar = cmd.Parameters.Add("@athleteId", SqlDbType.Int);
                    athleteIdPar.Direction = ParameterDirection.Input;
                    athleteIdPar.Value = athleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AdDTO v = new AdDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? "" : (string)myData["Description"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? "" : (string)myData["ProfilePicURL"].ToString();
                        v.VideoURL = (myData["VideoURL"] == DBNull.Value) ? "" : (string)myData["VideoURL"];
                        v.Id = (int)myData["Id"];
                        v.AdType = 4;
                        AdDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2v4/GetFrontRushCards", "", ex.Message, "Exception", 0);
                throw;
            }


            return AdDTO;
        }


        internal static int AddAtheleteFrontRushMatch(AthleteFrontRushMatchDTO athleteFrontRushMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAtheleteFrontRushMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteFrontRushMatchDTO.AthleteId;

                    SqlParameter frontRushId = cmd.Parameters.Add("@FrontRushId", SqlDbType.Int);
                    frontRushId.Direction = ParameterDirection.Input;
                    frontRushId.Value = athleteFrontRushMatchDTO.FrontRushId;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Bit);
                    status.Direction = ParameterDirection.Input;
                    status.Value = athleteFrontRushMatchDTO.Status;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/AddAtheleteFrontRushMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static int AddAthleteFrontRushChat(ChatDTO chatDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddAthleteFrontRushChat]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SenderId = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderId.Direction = ParameterDirection.Input;
                    SenderId.Value = chatDTO.SenderId;

                    SqlParameter ReceiverId = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverId.Direction = ParameterDirection.Input;
                    ReceiverId.Value = chatDTO.ReceiverId;

                    SqlParameter SenderType = cmd.Parameters.Add("@SenderType", SqlDbType.Int);
                    SenderType.Direction = ParameterDirection.Input;
                    SenderType.Value = chatDTO.SenderType;

                    SqlParameter ReceiverType = cmd.Parameters.Add("@ReceiverType", SqlDbType.Int);
                    ReceiverType.Direction = ParameterDirection.Input;
                    ReceiverType.Value = chatDTO.ReceiverType;


                    SqlParameter Message = cmd.Parameters.Add("@Message", SqlDbType.NVarChar, 4000);
                    Message.Direction = ParameterDirection.Input;
                    Message.Value = chatDTO.Message;


                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/AddAthleteFrontRushChat", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        //internal static int GetAthleteFrontChatUnreadCount(int senderId, int receiverId)
        //{

        //    int Count = 0;

        //    try
        //    {
        //        SqlConnection myConn = UsersDal.ConnectToDb();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[GetAthleteFrontChatUnreadCount]", myConn);
        //            cmd.CommandType = CommandType.StoredProcedure;

        //            SqlParameter SenderIdPar = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
        //            SenderIdPar.Direction = ParameterDirection.Input;
        //            SenderIdPar.Value = senderId;

        //            SqlParameter ReceiverIdPar = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
        //            ReceiverIdPar.Direction = ParameterDirection.Input;
        //            ReceiverIdPar.Value = receiverId;

        //            Count = Convert.ToInt16(cmd.ExecuteScalar());

        //            myConn.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        EmailLogModel emaillog = new EmailLogModel();
        //        emaillog.AddEmailLog("Dal2V4/GetAthleteFrontChatUnreadCount", "", ex.Message, "Exception", 0);
        //    }

        //    return Count;
        //}


        internal static List<AthleteDTO> GetAthletesForFrontRush(int frontRushId)
        {
            List<AthleteDTO> athleteDTO = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthletesForFrontRush]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter frontRushIdPar = cmd.Parameters.Add("@frontRushId", SqlDbType.Int);
                    frontRushIdPar.Direction = ParameterDirection.Input;
                    frontRushIdPar.Value = frontRushId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (myData["Id"] == DBNull.Value) ? 0 : Convert.ToInt16(myData["Id"]);
                        v.Name = (myData["Name"] == DBNull.Value) ? string.Empty : myData["Name"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? string.Empty : myData["ProfilePicURL"].ToString();
                        v.DeviceId = (myData["DeviceId"] == DBNull.Value) ? string.Empty : myData["DeviceId"].ToString();
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : myData["DeviceType"].ToString();
                        athleteDTO.Add(v);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetAthletesForFrontRush", "", ex.Message, "Exception", 0);
                throw;
            }

            return athleteDTO;
        }



        internal static int GetAthleteFrontRushChatMessagesUnreadCount(int senderId, int receiverId)
        {

            int Count = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteFrontRushChatMessagesUnreadCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SenderIdPar = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderIdPar.Direction = ParameterDirection.Input;
                    SenderIdPar.Value = senderId;

                    SqlParameter ReceiverIdPar = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverIdPar.Direction = ParameterDirection.Input;
                    ReceiverIdPar.Value = receiverId;

                    Count = Convert.ToInt16(cmd.ExecuteScalar());

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V4/GetAthleteFrontRushChatMessagesUnreadCount", "", ex.Message, "Exception", 0);
            }

            return Count;
        }


        internal static List<ChatDTO> GetAthleteFrontRushChatMessages(ChatDTO chatDTO)
        {

            List<ChatDTO> chats = new List<ChatDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteFrontRushChatMessages]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;


                    SqlParameter SenderId = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderId.Direction = ParameterDirection.Input;
                    SenderId.Value = chatDTO.SenderId;

                    SqlParameter ReceiverId = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverId.Direction = ParameterDirection.Input;
                    ReceiverId.Value = chatDTO.ReceiverId;

                    SqlParameter SenderType = cmd.Parameters.Add("@SenderType", SqlDbType.Int);
                    SenderType.Direction = ParameterDirection.Input;
                    SenderType.Value = chatDTO.SenderType;

                    SqlParameter ReceiverType = cmd.Parameters.Add("@ReceiverType", SqlDbType.Int);
                    ReceiverType.Direction = ParameterDirection.Input;
                    ReceiverType.Value = chatDTO.ReceiverType;

                    SqlParameter Date = cmd.Parameters.Add("@Date", SqlDbType.DateTime);
                    Date.Direction = ParameterDirection.Input;
                    Date.Value = (chatDTO.Date == null) ? Convert.ToDateTime("1900-01-01") : chatDTO.Date;


                    SqlParameter offset = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offset.Direction = ParameterDirection.Input;
                    offset.Value = chatDTO.Offset;

                    SqlParameter max = cmd.Parameters.Add("@max", SqlDbType.Int);
                    max.Direction = ParameterDirection.Input;
                    max.Value = chatDTO.Max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChatDTO v = new ChatDTO();
                        v.SenderId = (myData["SenderId"] == DBNull.Value) ? 0 : (int)myData["SenderId"];
                        v.ReceiverId = (myData["ReceiverId"] == DBNull.Value) ? 0 : (int)myData["ReceiverId"];
                        v.SenderType = (myData["SenderType"] == DBNull.Value) ? 0 : (int)myData["SenderType"];
                        v.ReceiverType = (myData["ReceiverType"] == DBNull.Value) ? 0 : (int)myData["ReceiverType"];
                        v.Date = (myData["Date"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["Date"];
                        v.Message = (myData["Message"] == DBNull.Value) ? "" : (string)myData["Message"];
                        chats.Add(v);
                    }
                    foreach (var c in chats)
                    {
                        c.Count = chats.Count;
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetAthleteFrontRushChatMessages", "", ex.Message, "Exception", 0);
                throw;
            }

            return chats;
        }


        internal static List<AthleteDTO> GetAthleteFrontRushFavourites(int atheleteId, int offset, int max)
        {
            List<AthleteDTO> frontRush = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteFrontRushFavourites]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter atheleteIdPar = cmd.Parameters.Add("@atheleteId", SqlDbType.Int);
                    atheleteIdPar.Direction = ParameterDirection.Input;
                    atheleteIdPar.Value = atheleteId;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.CoverPicURL = myData["VideoURL"].ToString();
                        v.Description = myData["Description"].ToString();
                        v.FavDate = (DateTime)myData["CreatedDate"];
                        v.Liked = true;
                        frontRush.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetAthleteFrontRushFavourites", "", ex.Message, "Exception", 0);
                throw;
            }

            return frontRush;
        }

        internal static int DeleteFrontRushAtheleteMatch(AthleteFrontRushMatchDTO athleteFrontRushMatchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[DeleteFrontRushAtheleteMatch]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteId", SqlDbType.Int);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteFrontRushMatchDTO.AthleteId;

                    SqlParameter frontRushId = cmd.Parameters.Add("@FrontRushId", SqlDbType.Int);
                    frontRushId.Direction = ParameterDirection.Input;
                    frontRushId.Value = athleteFrontRushMatchDTO.FrontRushId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    Id.Direction = ParameterDirection.Output;


                    cmd.ExecuteNonQuery();

                    id = (cmd.Parameters["@Id"].Value == DBNull.Value) ? 0 : (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2v4/DeleteFrontRushAtheleteMatch", "", ex.Message, "Exception", 0);
                throw;
            }

            return id;
        }


        internal static List<AdDTO> GetChatAdCards(string emailaddress, int loginType, int sportId, int AdType)
        {
            List<AdDTO> AdDTO = new List<AdDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetChatAdCards]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter loginTypePar = cmd.Parameters.Add("@loginType", SqlDbType.Int);
                    loginTypePar.Direction = ParameterDirection.Input;
                    loginTypePar.Value = loginType;

                    SqlParameter sportIdPar = cmd.Parameters.Add("@sportId", SqlDbType.Int);
                    sportIdPar.Direction = ParameterDirection.Input;
                    sportIdPar.Value = sportId;

                    SqlParameter adTypePar = cmd.Parameters.Add("@adType", SqlDbType.Int);
                    adTypePar.Direction = ParameterDirection.Input;
                    adTypePar.Value = AdType; 

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AdDTO v = new AdDTO();
                        v.Name = (myData["Name"] == DBNull.Value) ? "" : (string)myData["Name"].ToString();
                        v.Description = (myData["Description"] == DBNull.Value) ? "" : (string)myData["Description"].ToString();
                        v.ProfilePicURL = (myData["ProfilePicURL"] == DBNull.Value) ? "" : (string)myData["ProfilePicURL"].ToString();
                        v.VideoURL = (myData["WebsiteURL"] == DBNull.Value) ? "" : (string)myData["WebsiteURL"];
                        v.Id = (int)myData["Id"];
                        v.AdType = AdType;
                        AdDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("DAL2v4/GetChatAdCards", "", ex.Message, "Exception", 0);
                throw;
            }


            return AdDTO;
        }


        internal static List<AthleteDTO> GetAthleteChatAdFavourites(AthleteFavouriteDTO athleteFavouriteDTO)
        {
            List<AthleteDTO> ChatAds = new List<AthleteDTO>();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteChatAdFavourites]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar, 100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteFavouriteDTO.AthleteEmail;

                    SqlParameter loginType = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginType.Direction = ParameterDirection.Input;
                    loginType.Value = athleteFavouriteDTO.LoginType;

                    SqlParameter sportId = cmd.Parameters.Add("@SportId", SqlDbType.Int);
                    sportId.Direction = ParameterDirection.Input;
                    sportId.Value = athleteFavouriteDTO.SportId;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = athleteFavouriteDTO.Offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = athleteFavouriteDTO.Max;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.CoverPicURL = myData["WebsiteURL"].ToString();
                        v.Description = myData["Description"].ToString();
                        v.FavDate = (DateTime)myData["CreatedDate"];
                        v.Liked = true;
                        ChatAds.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetAthleteFrontRushFavourites", "", ex.Message, "Exception", 0);
                throw;
            }

            return ChatAds;
        }


        internal static int GetAthleteChatAdChatCount(int adId, int atheleteId)
        {

            int chatCount = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAthleteChatAdChatCount]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter SenderIdPar = cmd.Parameters.Add("@SenderId", SqlDbType.Int);
                    SenderIdPar.Direction = ParameterDirection.Input;
                    SenderIdPar.Value = adId;

                    SqlParameter ReceiverIdPar = cmd.Parameters.Add("@ReceiverId", SqlDbType.Int);
                    ReceiverIdPar.Direction = ParameterDirection.Input;
                    ReceiverIdPar.Value = atheleteId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        chatCount = (int)myData["ChatCount"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(" Dal2V4/GetAthleteChatAdChatCount", "", ex.Message, "Exception", 0);
                throw;
            }

            return chatCount;
        }


        internal static int GetClubCoachInstituteMatchStatus(int clubCoachId, int instituteId)
        {
            int status = 0;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetClubCoachInstituteMatchStatus]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter AthleteIdPar = cmd.Parameters.Add("@ClubCoachId", SqlDbType.Int);
                    AthleteIdPar.Direction = ParameterDirection.Input;
                    AthleteIdPar.Value = clubCoachId;

                    SqlParameter InstituteIdPar = cmd.Parameters.Add("@InstituteId", SqlDbType.Int);
                    InstituteIdPar.Direction = ParameterDirection.Input;
                    InstituteIdPar.Value = instituteId;


                    status = Convert.ToInt32(cmd.ExecuteScalar()); 


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2V4/GetAthleteInstituteMatchStatus", "", ex.Message, "Exception", 0);
            }


            return status;
        }


        internal static List<AthleteDTO> GetAllAthletePagingWithSearch(string emailaddress, int offset, int max,
      string searchText, int sportId)
        {
            AthleteController atc = new AthleteController();
            List<AthleteDTO> athletes = new List<AthleteDTO>();
            var coaches = UsersDal.GetCoachProfileByEmail(emailaddress);
            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAllAtheletesByCoachEmail2_v1]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;


                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;


                    SqlParameter maxpar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxpar.Direction = ParameterDirection.Input;
                    maxpar.Value = max;

                    SqlParameter searchTextPar = cmd.Parameters.Add("@SearchText", SqlDbType.VarChar, 1000);
                    searchTextPar.Direction = ParameterDirection.Input;
                    searchTextPar.Value = searchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.School = (string)myData["School"].ToString();
                        v.Emailaddress = (string)myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = (string)myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = (string)myData["DOB"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.Latitude = (string)myData["Latitude"].ToString();
                        v.Longitude = (string)myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value)
                            ? 0
                            : Convert.ToInt32(myData["AcademicLevel"]);
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["YearLevel"]);
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value)
                            ? 0
                            : Convert.ToInt32(myData["AtheleteRate"]);
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Premium"]);
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["ShoeSizeType"];
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["Phonenumber"];
                        v.RightSwiped = ((int)myData["RightSwiped"] == 0) ? false : true;

                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["StateName"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["BroadJump"];
                        v.Strokes = (myData["Strokes"] == DBNull.Value) ? string.Empty : (string)myData["Strokes"];
                        v.Location = (myData["Location"] == DBNull.Value) ? 0 : (int)myData["Location"];

                        v.SchoolCoachName = (myData["SchoolCoachName"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachName"];
                        v.SchoolCoachEmail = (myData["SchoolCoachEmail"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachEmail"];
                        v.SchoolZipCode = (myData["SchoolZipCode"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolZipCode"];
                        v.GapyearLevel = (myData["GapyearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["GapyearLevel"]);
                        v.GapyearDescription = (myData["GapyearDescription"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["GapyearDescription"];
                        v.Country = (myData["Country"] == DBNull.Value) ? string.Empty : (string)myData["Country"];
                        v.CountryId = (myData["CountryId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CountryId"]);
                        v.AP = (myData["AP"] == DBNull.Value) ? 0.0 : Convert.ToInt32(myData["AP"]);

                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        //  v.AthleteVideosCount = (myData["AthleteVideosCount"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteVideosCount"]);
                        int measurementcount = atc.GetMeasurementCount(v.Height, v.Height2, v.Weight, v.WingSpan, v.ShoeSize);
                        v.MeasurementCount = measurementcount;
                        string email = string.Empty;
                        email = (v.LoginType == 1 || v.LoginType == 4) ? v.Emailaddress : v.UserName;
                        v.MultiSportCount = Dal2.GetMultiSportCount(email, v.LoginType, Lemonaid_web_api.Utility.UserTypes.HighSchooAthlete);
                        v._20YardShuttleRun = (myData["20YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["20YardShuttleRun"];
                        v._60YardShuttleRun = (myData["60YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["60YardShuttleRun"];
                        v.KneelingMedBallToss = (myData["KneelingMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["KneelingMedBallToss"];
                        v.RotationalMedBallToss = (myData["RotationalMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["RotationalMedBallToss"];
                       
                        v.AnerobicBadge = Badges.GetAnerobicBadge(v.VerticalJump, v.BroadJump);
                        if (sportId == 5)
                        {
                            MetricsDto.TrackMetricsDto metrics = new MetricsDto.TrackMetricsDto();
                            metrics = (MetricsDto.TrackMetricsDto)DalMetrics.GetTrackMetrics(v.Id);
                            v.AerobicBadge = Badges.GetAerobicBadge(metrics.VO2Max);
                        }
                        v.StrengthBadge = Badges.GetStrengthBadge(v.KneelingMedBallToss, v.RotationalMedBallToss);
                        v.AgilityBadge = Badges.GetAgilityBadge(v._20YardShuttleRun, v._60YardShuttleRun);
                        v.DiamondBadge = Badges.GetDiamondBadge(v.AnerobicBadge, v.AerobicBadge, v.StrengthBadge, v.AgilityBadge, sportId);
                        //GetBadges
                        BadgesDTO badges = UsersDalV4.GetAtheleteBadges_v4(v.Id);
                        v.AthleteBadges = badges; 
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2/GetAllAthletePagingWithSearch", "", ex.Message, "Exception", 0);
            }

            return athletes;
        }

        internal static AthleteDTO GetAthleteProfile(string emailaddress, int type,int sportId)
        {
            AthleteDTO v = new AthleteDTO();

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetAtheleteProfile_v4]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlParameter typePar = cmd.Parameters.Add("@type", SqlDbType.Int);
                    typePar.Direction = ParameterDirection.Input;
                    typePar.Value = type;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = (string)myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.TotalCount = 1; // (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.AdType = 2;
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height"]);
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Height2"]);
                        v.HeightType = (myData["HeightType"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["HeightType"]);
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Weight"]);
                        v.WeightType = (myData["WeightType"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["WeightType"]);
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["WingSpan"]);
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["WingSpanType"]);
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ShoeSize"]);
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["ShoeSizeType"]);
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["ClubName"]);
                        v.CoachName = (myData["CoachName"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["CoachName"]);
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["CoachEmailId"]);
                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value)
                            ? 0
                            : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);
                        v.Status = (myData["Status"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Status"]);
                        v.AtheleteType = (myData["AtheleteType"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AtheleteType"]);
                        v.Active = (myData["Active"] == DBNull.Value) ? false : Convert.ToBoolean(myData["Active"]);
                        v.UniversityAddress = (myData["UnivAddress"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UnivAddress"]);
                        v.UniversityEmailaddress = (myData["UnivEmailAddress"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UnivEmailAddress"]);
                        v.UniversityName = (myData["UniversityName"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UniversityName"]);
                        v.UniversityPhoneNumber = (myData["UnviPhoneNumber"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UnviPhoneNumber"]);
                        v.UniversityProfilePic = (myData["UnivProfilePic"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UnivProfilePic"]);
                        v.UniversitySize = (myData["UnivSize"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["UnivSize"]);
                        v.UniversityClassificationName = (myData["ClassificationName"] == DBNull.Value)
                            ? string.Empty
                            : Convert.ToString(myData["ClassificationName"]);
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        v.SchoolCoachName = (myData["SchoolCoachName"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachName"];
                        v.SchoolCoachEmail = (myData["SchoolCoachEmail"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolCoachEmail"];
                        v.SchoolZipCode = (myData["SchoolZipCode"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["SchoolZipCode"];
                        v.GapyearLevel = (myData["GapyearLevel"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["GapyearLevel"]);
                        v.GapyearDescription = (myData["GapyearDescription"] == DBNull.Value)
                            ? string.Empty
                            : (string)myData["GapyearDescription"];
                        v.Country = (myData["Country"] == DBNull.Value) ? string.Empty : (string)myData["Country"];
                        v.CountryId = (myData["CountryId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CountryId"]);
                        v.AP = (myData["AP"] == DBNull.Value) ? 0.0 : Convert.ToDouble(myData["AP"]);
                        v.isProfileCompleted = (myData["isProfileCompleted"] == DBNull.Value) ? false : Convert.ToBoolean(myData["isProfileCompleted"]);
                        v.AthleteTransition = (myData["AthleteTransition"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AthleteTransition"]);
                        v._20YardShuttleRun = (myData["20YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["20YardShuttleRun"];
                        v._60YardShuttleRun = (myData["60YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["60YardShuttleRun"];
                        v.KneelingMedBallToss = (myData["KneelingMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["KneelingMedBallToss"];
                        v.RotationalMedBallToss = (myData["RotationalMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["RotationalMedBallToss"];
                        v.DeviceId = (myData["DeviceName"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceName"]);
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceType"]);
                        v.AnerobicBadge = Badges.GetAnerobicBadge(v.VerticalJump, v.BroadJump);
                        if (sportId == 5)
                        {
                            MetricsDto.TrackMetricsDto metrics = new MetricsDto.TrackMetricsDto();
                            metrics = (MetricsDto.TrackMetricsDto)DalMetrics.GetTrackMetrics(v.Id);
                            v.AerobicBadge = Badges.GetAerobicBadge(metrics.VO2Max);
                        }
                        v.StrengthBadge = Badges.GetStrengthBadge(v.KneelingMedBallToss, v.RotationalMedBallToss);
                        v.AgilityBadge = Badges.GetAgilityBadge(v._20YardShuttleRun, v._60YardShuttleRun);
                        v.DiamondBadge = Badges.GetDiamondBadge(v.AnerobicBadge, v.AerobicBadge, v.StrengthBadge, v.AgilityBadge, sportId);
                        v.CityId = (myData["CityId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["CityId"]);
                        v.BasicStateId = (myData["BasicStateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["BasicStateId"]);
                        v.BasicStateName = (myData["BasicStateName"] == DBNull.Value) ? string.Empty : (string)myData["BasicStateName"];
                        v.CityName = (myData["CityName"] == DBNull.Value) ? string.Empty : (string)myData["CityName"];
                        v.MultiSportCount = Dal2.GetMultiSportCount(emailaddress, v.LoginType,  Utility.UserTypes.HighSchooAthlete);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetAthleteProfile", "", ex.Message, "Exception", 0);
            }

            return v;
        }

        internal static List<AthleteDTO> GetCoachesFavourites(string emailaddress, int offset, int max,int SportId)
        {
            AthleteController atc = new AthleteController();
            List<AthleteDTO> athletes = new List<AthleteDTO>();
            DateTime dtAtheleteDate;
            DateTime dtInstituteDate;

            try
            {
                SqlConnection myConn = UsersDal.ConnectToDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetInstituteFavourites_v5]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailAddress = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailAddress.Direction = ParameterDirection.Input;
                    emailAddress.Value = emailaddress;

                    SqlParameter offsetPar = cmd.Parameters.Add("@offset", SqlDbType.Int);
                    offsetPar.Direction = ParameterDirection.Input;
                    offsetPar.Value = offset;

                    SqlParameter maxPar = cmd.Parameters.Add("@max", SqlDbType.Int);
                    maxPar.Direction = ParameterDirection.Input;
                    maxPar.Value = max;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        AthleteDTO v = new AthleteDTO();
                        v.Id = (int)myData["id"];
                        v.Name = myData["name"].ToString();
                        v.Description = myData["description"].ToString();
                        v.School = myData["School"].ToString();
                        v.Emailaddress = myData["emailaddress"].ToString();
                        v.GPA = Convert.ToDouble(myData["GPA"]);
                        v.Social = (myData["Social"] == DBNull.Value) ? 0 : (int)myData["Social"];
                        v.SAT = (myData["SAT"] == DBNull.Value) ? 0 : (int)myData["SAT"];
                        v.ACT = (myData["ACT"] == DBNull.Value) ? 0 : (int)myData["ACT"];
                        v.InstituteId = (myData["InstituteId"] == DBNull.Value) ? 0 : (int)myData["InstituteId"];
                        v.Gender = (myData["Gender"] == DBNull.Value) ? 0 : (int)myData["Gender"];
                        v.FacebookId = myData["FacebookId"].ToString();
                        v.ProfilePicURL = myData["ProfilePicURL"].ToString();
                        v.DOB = myData["DOB"].ToString();
                        v.Address = myData["Address"].ToString();
                        v.Latitude = myData["Latitude"].ToString();
                        v.Longitude = myData["Longitude"].ToString();
                        v.TotalCount = (myData["TotalCount"] == DBNull.Value) ? 0 : (int)myData["TotalCount"];
                        v.FriendsInApp = (myData["FriendsInApp"] == DBNull.Value) ? 0 : (int)myData["FriendsInApp"];
                        v.SharesApp = (myData["SharesApp"] == DBNull.Value) ? 0 : (int)myData["SharesApp"];
                        v.AverageRating = (myData["AverageRating"] == DBNull.Value) ? 0 : (int)myData["AverageRating"];
                        v.AcademicLevel = (myData["AcademicLevel"] == DBNull.Value) ? 0 : (int)myData["AcademicLevel"];
                        v.YearLevel = (myData["YearLevel"] == DBNull.Value) ? 0 : (int)myData["YearLevel"];
                        v.FriendsRate = (myData["FriendsRate"] == DBNull.Value) ? 0 : (int)myData["FriendsRate"];
                        v.ShareRate = (myData["ShareRate"] == DBNull.Value) ? 0 : (int)myData["ShareRate"];
                        v.SocialRate = (myData["SocialRate"] == DBNull.Value) ? 0 : (int)myData["SocialRate"];
                        v.AcademicRate = (myData["AcademicRate"] == DBNull.Value) ? 0 : (int)myData["AcademicRate"];
                        v.AtheleteRate = (myData["AtheleteRate"] == DBNull.Value) ? 0 : (int)myData["AtheleteRate"];
                        dtAtheleteDate = (myData["AtheleteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["AtheleteDate"];
                        dtInstituteDate = (myData["InstituteDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InstituteDate"];
                        v.Class = UsersDal.GetClass(v.AcademicLevel, v.YearLevel);
                        v.Liked = ((int)myData["Likes"] != 0);
                        if (v.Liked)
                        { v.FavDate = (dtAtheleteDate > dtInstituteDate) ? dtAtheleteDate : dtInstituteDate; }
                        else
                        { v.FavDate = dtInstituteDate; }
                        v.InvitationDate = (myData["InvitedDate"] == DBNull.Value) ? Convert.ToDateTime("1900-01-01") : (DateTime)myData["InvitedDate"];
                        v.AtheleteId = (int)myData["AtheleteId"];
                        v.CoachId = (int)myData["CoachId"];
                        v.Premium = (myData["Premium"] == DBNull.Value) ? 0 : (int)myData["Premium"];
                        v.Height = (myData["Height"] == DBNull.Value) ? string.Empty : (string)myData["Height"];
                        v.Height2 = (myData["Height2"] == DBNull.Value) ? string.Empty : (string)myData["Height2"];
                        v.Weight = (myData["Weight"] == DBNull.Value) ? string.Empty : (string)myData["Weight"];
                        v.WeightType = (myData["WeightType"] == DBNull.Value) ? string.Empty : (string)myData["WeightType"];
                        v.WingSpan = (myData["WingSpan"] == DBNull.Value) ? string.Empty : (string)myData["WingSpan"];
                        v.WingSpanType = (myData["WingSpanType"] == DBNull.Value) ? string.Empty : (string)myData["WingSpanType"];
                        v.ShoeSize = (myData["ShoeSize"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSize"];
                        v.ShoeSizeType = (myData["ShoeSizeType"] == DBNull.Value) ? string.Empty : (string)myData["ShoeSizeType"];
                        v.UniversityName = myData["UniversityName"].ToString();
                        v.UniversityProfilePic = myData["UniversityProfilePic"].ToString();
                        v.ClubName = (myData["ClubName"] == DBNull.Value) ? string.Empty : (string)myData["ClubName"];
                        v.CoachName = (myData["CoachName"] == DBNull.Value) ? string.Empty : (string)myData["CoachName"];
                        v.CoachEmailId = (myData["CoachEmailId"] == DBNull.Value) ? string.Empty : (string)myData["CoachEmailId"];
                        v.Major = (myData["Major"] == DBNull.Value) ? string.Empty : (string)myData["Major"];
                        v.Phonenumber = (myData["Phonenumber"] == DBNull.Value) ? string.Empty : (string)myData["Phonenumber"];
                        v.StateId = (myData["StateId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["StateId"]);
                        v.StateName = (myData["StateName"] == DBNull.Value) ? string.Empty : (string)myData["StateName"];
                        v.VerticalJump = (myData["VerticalJump"] == DBNull.Value) ? string.Empty : (string)myData["VerticalJump"];
                        v.BroadJump = (myData["BroadJump"] == DBNull.Value) ? string.Empty : (string)myData["BroadJump"];
                        int measurementcount = atc.GetMeasurementCount(v.Height, v.Height2, v.Weight, v.WingSpan, v.ShoeSize);
                        v.MeasurementCount = measurementcount;
                        v.UserName = (myData["UserName"] == DBNull.Value) ? string.Empty : (string)myData["UserName"];
                        v.LoginType = (myData["LoginType"] == DBNull.Value) ? 0 : (int)myData["LoginType"];

                        v.AllAmerican = (myData["AllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AllAmerican"]);
                        v.AcademicAllAmerican = (myData["AcademicAllAmerican"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["AcademicAllAmerican"]);
                        v.Captain = (myData["Captain"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["Captain"]);
                        v.SwimswamTop = (myData["SwimswamTop"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SwimswamTop"]);

                        string email = string.Empty;
                        email = (v.LoginType == 1 || v.LoginType == 4) ? v.Emailaddress : v.UserName;
                        v.MultiSportCount = Dal2.GetMultiSportCount(email, v.LoginType, Utility.UserTypes.HighSchooAthlete);
                        v.AP = (myData["AP"] == DBNull.Value) ? 0.0 : Convert.ToDouble(myData["AP"]);
                        v._20YardShuttleRun = (myData["20YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["20YardShuttleRun"];
                        v._60YardShuttleRun = (myData["60YardShuttleRun"] == DBNull.Value) ? string.Empty : (string)myData["60YardShuttleRun"];
                        v.KneelingMedBallToss = (myData["KneelingMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["KneelingMedBallToss"];
                        v.RotationalMedBallToss = (myData["RotationalMedBallToss"] == DBNull.Value) ? string.Empty : (string)myData["RotationalMedBallToss"];
                        v.AnerobicBadge = Badges.GetAnerobicBadge(v.VerticalJump, v.BroadJump);
                        if (SportId == 5)
                        {
                            MetricsDto.TrackMetricsDto metrics = new MetricsDto.TrackMetricsDto();
                            metrics = (MetricsDto.TrackMetricsDto)DalMetrics.GetTrackMetrics(v.Id);
                            v.AerobicBadge = Badges.GetAerobicBadge(metrics.VO2Max);
                        }
                        v.StrengthBadge = Badges.GetStrengthBadge(v.KneelingMedBallToss, v.RotationalMedBallToss);
                        v.AgilityBadge = Badges.GetAgilityBadge(v._20YardShuttleRun, v._60YardShuttleRun);
                        v.DiamondBadge = Badges.GetDiamondBadge(v.AnerobicBadge, v.AerobicBadge, v.StrengthBadge, v.AgilityBadge, SportId);
                        v.DeviceId = (myData["Device_Id"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["Device_Id"]);
                        v.DeviceType = (myData["DeviceType"] == DBNull.Value) ? string.Empty : Convert.ToString(myData["DeviceType"]);
                        athletes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDalV5/GetCoachesFavourites_v5", "", ex.Message, "Exception", 0);
            }


            return athletes;
        }


        internal static int GetUserId(string athleteEmail,int loginType)
        {
            int userId = 0;
            try
            {
                SqlConnection myConn = UsersDal.ConnectToWrapperDb();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetUserId]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter athleteId = cmd.Parameters.Add("@AthleteEmail", SqlDbType.VarChar, 100);
                    athleteId.Direction = ParameterDirection.Input;
                    athleteId.Value = athleteEmail;

                    SqlParameter loginTypepar = cmd.Parameters.Add("@LoginType", SqlDbType.Int);
                    loginTypepar.Direction = ParameterDirection.Input;
                    loginTypepar.Value = loginType; 

                    userId = Convert.ToInt32(cmd.ExecuteScalar());


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Dal2v4/GetUserId", "", ex.Message, "Exception", 0);
                throw;
            }

            return userId;
        }
    }
}