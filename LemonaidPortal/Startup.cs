﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LemonaidPortal.Startup))]
namespace LemonaidPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
