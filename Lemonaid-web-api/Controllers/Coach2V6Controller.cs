﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;


namespace Lemonaid_web_api.Controllers
{
    public class Coach2V6Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse Login(string emailaddress, string password)
        {
            try
            {
                int id = UsersDal.CheckCoachExistsorPassword(emailaddress, UsersDalV4.Encrypt(password));
                if (id == -1)
                {
                    _result.Status = Utility.CustomResponseStatus.CoachNewUser;
                    _result.Message = CustomConstants.Coach_NewUser;
                }
                else if (id == -2)
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Go_To_Login;
                }
                else
                {
                    CoachDTO coachInfo = UsersDal.GetCoachExists(emailaddress, UsersDalV4.Encrypt(password));
                    if (coachInfo.Id > 0)
                    {
                        _result.Response = coachInfo;
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Message = CustomConstants.Login_successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Response = coachInfo;
                        _result.Message = CustomConstants.User_Does_Not_Exist;
                    }
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/Login", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpGet]
        public Utility.CustomResponse PushNotification(string Deviceid,string Message)
        {
            try
            {
                bool Status = DAL2V6.PushNotification(Deviceid, Message);
                if (Status == true)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.Notification_Sent;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = "Notification Sending Failed!!!";
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/PushNotification", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddCoach360Videos(Video360TypeDTO Video360)
        {
            try
            {
                int id = DAL2V6.AddCoachVideo360Content(Video360);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Video360_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/AddCoach360Videos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse DeleteCoach360Videos(int Video360ID)
        {
            try
            {
                var DeleteID = DAL2V6.DeleteCoachVideo360Type(Video360ID);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = Video360ID;
                _result.Message = CustomConstants.Video_Deleted_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/DeleteCoach360Videos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetCoachVideos360Type()
        {

            try
            {
                List<Video360TypeDTO> lstVideo360Type = DAL2V6.GetCoachVideos360Type();

                if (lstVideo360Type.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = lstVideo360Type;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/GetCoachVideos360Type", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        public Utility.CustomResponse AddCoach(CoachUserDTO coachUser)
        {
            CoachV4Controller coachv4Ctrl = new CoachV4Controller();
            Coach2Controller coach2Ctrl = new Coach2Controller();
            try
            {
                List<SportsDTO> sports = coach2Ctrl.AddCoachToWrapperDB(coachUser);
                if (sports.Count > 0)
                {
                    coachUser = DAL2V6.AddCoachUser_2V6(coachUser);
                    CoachDTO coachInfo = UsersDal.GetCoachExists(coachUser.Emailaddress, coachUser.Password);
                    if (coachInfo.Id > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = coachInfo;
                        _result.Message = CustomConstants.Enrolled_successfully;

                        CoachController cc = new CoachController();
                        bool isvalid = cc.CheckForValidEmail(coachInfo.InstituteEmailaddress, coachUser.Emailaddress);
                        //Compare with the domain name . if matched send to coach or else to support and UnivEmail
                        if (isvalid)
                        {
                            DuoLoginController.SendMail(coachUser.Emailaddress, coachUser.ActivationCode,
                                coachUser.CoachId.ToString());
                        }
                        else
                        {
                            coachv4Ctrl.SendMailToSupportandUniversity(coachUser.Emailaddress, coachUser.ActivationCode,
                                coachUser.CoachId.ToString(), coachInfo.InstituteEmailaddress);
                        }
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.User_Does_Not_Exist; 
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2v6/AddCoach", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetCoachVideos360Type(int CoachId)
        {

            try
            {
                List<Video360TypeDTO> lstVideo360Type = DAL2V6.GetCoachVideos360Type(CoachId);

                if (lstVideo360Type.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = lstVideo360Type;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/GetCoachVideos360Type", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse AddCoachPayment(PaymentDTO paymentDto)
        {
            try
            { 
                    int id = DAL2V6.AddCoachPayment(paymentDto);
                    if (id > 0)
                    {
                        _result.Response = id;
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Message = CustomConstants.Payment_Added_Successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.Record_Not_Added;
                    }
                 
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/AddCoachPayment", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;
        }

        [HttpPost]
        public Utility.CustomResponse UpdateCoachPayment(PaymentDTO paymentDto)
        {
            try
            {
                int id = DAL2V6.UpdateCoachPayment(paymentDto);
                if (id > 0)
                {
                    _result.Response = id;
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.Payment_Updated_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Record_Not_Added;
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/UpdateCoachPayment", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;
        }
        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;

        }

        [HttpGet]
        public Utility.CustomResponse GetCoachPayment(int userId)
        {
            try
            {
                var payment = DAL2V6.GetCoachPayment(userId);
                if (payment.UserID == 0)
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = payment;
                    _result.Message = CustomConstants.Details_Get_Successfully;

                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/GetCoachPayment", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost] 
        public Utility.CustomResponse ContactUs(ContactUsDTO contactUsDto)
        {
            try
            {
                int id = DAL2V6.ContactUs(contactUsDto);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.ContactUs_added_successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/ContactUs", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet] 
        public Utility.CustomResponse GetSchoolClubCoachesForCoach(int CoachId)
        {
            try
            {
              
            List<ClubCoachDTO> clubCoaches = DAL2V6.GetSchoolClubCoachesForCoach(CoachId); 

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = clubCoaches;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/GetSchoolClubCoachesForCoach", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetLatestMessagesForCoach(int CoachId)
        {
            try
            { 
                List<ChatDTO> chats = DAL2V6.GetLatestMessagesForCoach(CoachId);
                foreach (var a in chats)
                {
                    int chatCount = UsersDalV4.GetChatCount(CoachId, a.SenderId);
                    a.Count = chatCount;
                }
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = chats;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/GetLatestMessagesForCoach", "", ex.Message, "Exception", 0);
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse UpdateIsSeenNotifications(int NotificationId)
        {
            try
            {
                DAL2V6.UpdateIsSeenNotifications(NotificationId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = NotificationId;
                _result.Message = CustomConstants.Notification_Updated;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/UpdateIsSeenNotifications", "", ex.Message, "Exception", 0);
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetNotificationMessagesCount(int CoachId)
        {
            try
            {
                var results = UsersDal.GetUserNotifications(CoachId, 0, 100);
                var chats = DAL2V6.GetLatestMessagesForCoach(CoachId);
                NotificationsMessagesCount notifMsgCnt = new NotificationsMessagesCount();
                notifMsgCnt.NotificationCount = results.Count;
                notifMsgCnt.MessageCount = chats.Count;

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = notifMsgCnt;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2V6/GetNotificationMessagesCount", "", ex.Message, "Exception", 0);
                return _result;
            }

        }
    }
}
