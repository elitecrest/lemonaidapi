﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.Web.Http; 
using Lemonaid_web_api.Models;
 
namespace Lemonaid_web_api.Controllers
{
    public class CoachV5Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        //Replacement of Athlete/GetAllAthletesByLocationWithSearch
        [HttpGet]
        public Utility.CustomResponse GetAllAthletesByLocationWithSearch_v5(string emailaddress, double latitude, double longitude, int offset, int max, int gpa, int social, int locationType, string strokes, string description, string searchText) // Coach email address
        {
            AthleteController athleteContr = new AthleteController(); 

            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }
                var athletes = UsersDal.GetAllAthletePagingWithSearch(emailaddress, offset, max, gpa, social, description, strokes, searchText);

                var newList = new List<AthleteDTO>();
                var newListFromStrokes = new List<AthleteDTO>();
                var listWithQuestionaire = new List<AthleteDTO>();
                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = athletes;

                }
                else
                    if (locationType == 2)
                    {
                        foreach (var t in athletes)
                        {
                            double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                            double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                            double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(t);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            foreach (var t in athletes)
                            {
                                double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                                double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                                var distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                if (distance <= 5)
                                {
                                    newList.Add(t);
                                }
                            }
                        }


                if (strokes != "ALL")
                {
                    string[] strStrokes = strokes.Split(',');

                    var atheleteIds = new List<int>();
                    var atheleteStrokes = UsersDal.GetStrokesForAtheletes();
                    foreach (var t in atheleteStrokes)
                    {
                        foreach (string t1 in strStrokes)
                        {
                            if (t1 == t.Category)
                            {
                                atheleteIds.Add(t.AtheleteId);
                            }
                        }
                    }

                    foreach (AthleteDTO t in newList)
                    {
                        foreach (int t1 in atheleteIds)
                        {
                            if (t1 == t.Id)
                            {

                                newListFromStrokes.Add(t);
                            }
                        }
                    }
                    newListFromStrokes = newListFromStrokes.Distinct().ToList();
                }
                else
                {

                    newListFromStrokes = newList;
                }

                CoachDTO coach = UsersDal.GetCoachProfileByEmail(emailaddress);
                List<QuestionaireDTO> questionaireDto = UsersDalV5.GetQuestionaire_v5(coach.CoachId, "CC");
                listWithQuestionaire = AddQuestionairetoList(questionaireDto, listWithQuestionaire);
                if (newListFromStrokes.Count > 0)
                {
                    newListFromStrokes = AddBadgesToList(newListFromStrokes);
                    //Add BestTimes
                    newListFromStrokes = athleteContr.AddBesttimesToList(newListFromStrokes);
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newListFromStrokes = athleteContr.GetAdAthletes(ads, newListFromStrokes);
                    //Add Athelete Videos
                    newListFromStrokes = AddVideosToList(newListFromStrokes);

                    listWithQuestionaire = AddAtheletestoList(newListFromStrokes, listWithQuestionaire);
                   // ListWithQuestionaire = ListWithQuestionaire.OrderBy(x => x.Diamond).ToList();
                }
                if (listWithQuestionaire.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = listWithQuestionaire;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV5/GetAllAthletesByLocationWithSearch_v5", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }

        public List<AthleteDTO> AddBadgesToList(List<AthleteDTO> newListFromStrokes)
        {

            BadgesDTO badges;
            for (int i = 0; i < newListFromStrokes.Count; i++)
            {
                badges = UsersDalV4.GetAtheleteBadges_v4(newListFromStrokes[i].Id);
                newListFromStrokes[i].Diamond = badges.Diamond;
            }

            return newListFromStrokes;

        }


        public List<AthleteDTO> AddVideosToList(List<AthleteDTO> newListFromStrokes)
        {
            
            foreach (var t in newListFromStrokes)
            {
                List<VideoDTO> shareAtheleteVideoDto = new List<VideoDTO>();
                List<VideoDTO> atheleteVideoDto = new List<VideoDTO>();

                shareAtheleteVideoDto = DAL2V5.GetShareTOAthleteVideosById(t.Id);
                t.SharedAtheleteVideos = shareAtheleteVideoDto;
                foreach (var at in t.SharedAtheleteVideos)
                {
                    var shareAtheleteVideoThumbnails = DAL2V5.GetShareAtheleteVideosThumbnails(t.Id);
                    at.ExtractedThumbnailURLs = shareAtheleteVideoThumbnails;
                }
                atheleteVideoDto = UsersDalV4.GetAtheleteVideos_v4(t.Id);
                t.AtheleteVideos = atheleteVideoDto;
                foreach (var at in t.AtheleteVideos)
                {
                    var atheleteVideoThumbnails = UsersDalV5.GetAtheleteVideosThumbnails(t.Id);
                    at.ExtractedThumbnailURLs = atheleteVideoThumbnails;
                }
                t.AthleteVideosCount = t.AtheleteVideos.Count;
            } 
            return newListFromStrokes;
        }

       

        public List<AthleteDTO> AddQuestionairetoList(List<QuestionaireDTO> questionaire, List<AthleteDTO> athleteDto)
        {
            foreach (QuestionaireDTO t in questionaire)
            {
                AthleteDTO atheletes = new AthleteDTO();
                atheletes.ProfilePicURL = string.Empty;
                atheletes.Name = string.Empty;
                atheletes.QuestionId = t.QuestionId;
                atheletes.Question = t.Question;
                atheletes.AdType = 3;
                athleteDto.Add(atheletes);
            }
            return athleteDto;
        }


        public List<AthleteDTO> AddAtheletestoList(List<AthleteDTO> newListFromStrokes, List<AthleteDTO> listWithQuestionaire)
        {

            for (int i = 0; i < newListFromStrokes.Count; i++)
            {
                AthleteDTO atheletes = new AthleteDTO();
                atheletes.Id = newListFromStrokes[i].Id;
                atheletes.Name = newListFromStrokes[i].Name;
                atheletes.Gender = newListFromStrokes[i].Gender;
                atheletes.School = newListFromStrokes[i].School;
                atheletes.Emailaddress = newListFromStrokes[i].Emailaddress;
                atheletes.Description = newListFromStrokes[i].Description;
                atheletes.GPA = newListFromStrokes[i].GPA;
                atheletes.Social = newListFromStrokes[i].Social;
                atheletes.SAT = newListFromStrokes[i].SAT;
                atheletes.ACT = newListFromStrokes[i].ACT;
                atheletes.InstituteId = newListFromStrokes[i].InstituteId;
                atheletes.ProfilePicURL = newListFromStrokes[i].ProfilePicURL;
                atheletes.DOB = newListFromStrokes[i].DOB;
                atheletes.Address = newListFromStrokes[i].Address;
                atheletes.FacebookId = newListFromStrokes[i].FacebookId;
                atheletes.Latitude = newListFromStrokes[i].Latitude;
                atheletes.Longitude = newListFromStrokes[i].Longitude;
                atheletes.FriendsInApp = newListFromStrokes[i].FriendsInApp;
                atheletes.SharesApp = newListFromStrokes[i].SharesApp;
                atheletes.AverageRating = newListFromStrokes[i].AverageRating;
                atheletes.AcademicLevel = newListFromStrokes[i].AcademicLevel;
                atheletes.YearLevel = newListFromStrokes[i].YearLevel;
                atheletes.Class = newListFromStrokes[i].Class;
                atheletes.FriendsRate = newListFromStrokes[i].FriendsRate;
                atheletes.ShareRate = newListFromStrokes[i].ShareRate;
                atheletes.SocialRate = newListFromStrokes[i].SocialRate;
                atheletes.AcademicRate = newListFromStrokes[i].AcademicRate;
                atheletes.AtheleteRate = newListFromStrokes[i].AtheleteRate;
                atheletes.CoverPicURL = newListFromStrokes[i].CoverPicURL;
                atheletes.AdType = newListFromStrokes[i].AdType;
                atheletes.Premium = newListFromStrokes[i].Premium;
                atheletes.Height = newListFromStrokes[i].Height;
                atheletes.Height2 = newListFromStrokes[i].Height2;
                atheletes.HeightType = newListFromStrokes[i].HeightType;
                atheletes.Weight = newListFromStrokes[i].Weight;
                atheletes.WeightType = newListFromStrokes[i].WeightType;
                atheletes.WingSpan = newListFromStrokes[i].WingSpan;
                atheletes.WingSpanType = newListFromStrokes[i].WingSpanType;
                atheletes.ShoeSize = newListFromStrokes[i].ShoeSize;
                atheletes.ShoeSizeType = newListFromStrokes[i].ShoeSizeType;
                atheletes.ClubName = newListFromStrokes[i].ClubName;
                atheletes.CoachName = newListFromStrokes[i].CoachName;
                atheletes.CoachEmailId = newListFromStrokes[i].CoachEmailId;
                atheletes.AllAmerican = newListFromStrokes[i].AllAmerican;
                atheletes.AcademicAllAmerican = newListFromStrokes[i].AcademicAllAmerican;
                atheletes.Captain = newListFromStrokes[i].Captain;
                atheletes.SwimswamTop = newListFromStrokes[i].SwimswamTop;
                atheletes.Status = newListFromStrokes[i].Status;
                atheletes.AtheleteType = newListFromStrokes[i].AtheleteType;
                atheletes.Active = newListFromStrokes[i].Active;
               // atheletes.AtheleteCoachType = 0;
               // atheletes.AdType = 2;
                atheletes.FavDate = newListFromStrokes[i].FavDate;
                atheletes.Liked = newListFromStrokes[i].Liked;
                atheletes.InvitationDate = newListFromStrokes[i].InvitationDate;
                List<AthleteEventDTO> atheleteEventDto;
                atheleteEventDto = UsersDal.GetAthleteBesttimes(newListFromStrokes[i].Id);
                atheletes.AtheleteBesttimes = atheleteEventDto;
                atheletes.Major = newListFromStrokes[i].Major;
                atheletes.Phonenumber = newListFromStrokes[i].Phonenumber;
                atheletes.RightSwiped = newListFromStrokes[i].RightSwiped;
                atheletes.ChatCount = newListFromStrokes[i].ChatCount;
                atheletes.AtheleteVideos = newListFromStrokes[i].AtheleteVideos;
                atheletes.StateId = newListFromStrokes[i].StateId;
                atheletes.StateName = newListFromStrokes[i].StateName;
                atheletes.Diamond = newListFromStrokes[i].Diamond;
                atheletes.VerticalJump = newListFromStrokes[i].VerticalJump;
                atheletes.BroadJump = newListFromStrokes[i].BroadJump; 
                atheletes.Location = newListFromStrokes[i].Location;
                atheletes.Strokes = newListFromStrokes[i].Strokes;
                atheletes.SchoolCoachName = newListFromStrokes[i].SchoolCoachName;
                atheletes.SchoolCoachEmail = newListFromStrokes[i].SchoolCoachEmail;
                atheletes.SchoolZipCode = newListFromStrokes[i].SchoolZipCode;
                atheletes.GapyearLevel = newListFromStrokes[i].GapyearLevel;
                atheletes.GapyearDescription = newListFromStrokes[i].GapyearDescription;
                atheletes.Country = newListFromStrokes[i].Country;
                atheletes.CountryId = newListFromStrokes[i].CountryId;
                atheletes.AP = newListFromStrokes[i].AP;
                atheletes.MeasurementCount = newListFromStrokes[i].MeasurementCount;
                atheletes.MultiSportCount = newListFromStrokes[i].MultiSportCount;
                atheletes.AthleteVideosCount = newListFromStrokes[i].AthleteVideosCount;
                atheletes._20YardShuttleRun = newListFromStrokes[i]._20YardShuttleRun;
                atheletes._60YardShuttleRun = newListFromStrokes[i]._60YardShuttleRun;
                atheletes.RotationalMedBallToss = newListFromStrokes[i].RotationalMedBallToss;
                atheletes.KneelingMedBallToss = newListFromStrokes[i].KneelingMedBallToss;
                atheletes.AnerobicBadge = newListFromStrokes[i].AnerobicBadge;
                atheletes.AerobicBadge = newListFromStrokes[i].AerobicBadge;
                atheletes.StrengthBadge = newListFromStrokes[i].StrengthBadge;
                atheletes.AgilityBadge = newListFromStrokes[i].AgilityBadge;
                atheletes.DiamondBadge = newListFromStrokes[i].DiamondBadge;
                atheletes.AthleteBadges = newListFromStrokes[i].AthleteBadges;
                atheletes.SharedAtheleteVideos = newListFromStrokes[i].SharedAtheleteVideos;
                atheletes.Rank = newListFromStrokes[i].Rank;
                listWithQuestionaire.Add(atheletes);
            }
            return listWithQuestionaire;

        }


        [HttpGet]
        public Utility.CustomResponse GetCoachesFavourites_v5(string emailaddress, int offset, int max)
        {

            try
            {
                AthleteController athCtrl = new AthleteController();
                var atheletes = UsersDalV5.GetCoachesFavourites_v5(emailaddress, offset, max);
                CoachDTO coach = UsersDal.GetCoachProfileByEmail(emailaddress);
                int coachId = coach.CoachId;
                foreach (var a in atheletes)
                {
                    int chatCount = UsersDalV4.GetChatCount(a.Id, coachId);
                    a.ChatCount = chatCount;  
                }

                int totalLikesCount = atheletes.Count(p => p.Liked);

                for (var i = 0; i < atheletes.Count; i++)
                {
                    atheletes[i].TotalLikedCount = totalLikesCount;
                }
                atheletes = athCtrl.AddBesttimesToList(atheletes);
                atheletes = AddVideosToList(atheletes);
                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Matches;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV5/GetCoachesFavourites_v5", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse CoachProfile(string emailaddress)
        {

            try
            {
                var coach = UsersDal.GetCoachProfileByEmail(emailaddress);
                int coach360VideosCnt = DAL2V7.GetCoach360VideosCount(emailaddress);
                coach.Coach360VideosCnt = coach360VideosCnt;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = coach;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("CoachV5/CoachProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
    }
}
 