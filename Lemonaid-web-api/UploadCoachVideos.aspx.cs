﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using Lemonaid_web_api.Models;
using Microsoft.WindowsAzure.MediaServices.Client;

namespace Lemonaid_web_api
{
    public partial class UploadCoachVideos : Page
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _result = Handlefile();
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string jsonString = javaScriptSerializer.Serialize(_result);
                Response.Write(jsonString);
            }

        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    _result = Handlefile();
        //}



        private Utility.CustomResponse Handlefile()
        {
            string baseUrl = ConfigurationManager.AppSettings["BaseURL"];
            Stream stream = Request.InputStream;
            string videoFormat = Request.QueryString["VideoFormat"].Trim();  
            string Duration = Request.QueryString["Duration"].Trim() ;

            VideoDTO video = new VideoDTO();
            string videoUrl = Streamaudio(stream, videoFormat);

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));  

            video.Id = 0;
            video.VideoURL = videoUrl;
            video.ThumbnailURL = string.Empty; 
            video.CoachId = Convert.ToInt32(Request.QueryString["CoachId"].Trim());
            video.Name = "Video_" + video.CoachId;
            video.VideoFormat = videoFormat;
            video.VideoNumber = 1;
            video.Duration = Convert.ToInt32(Duration);
            video.Status = 1;

            var response = client.PostAsJsonAsync("Coach2/AddCoachVideos", video).Result;

            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = res.Response;
                _result.Message = CustomConstants.Video_Added;

            }
            else
            {
                _result.Status = Utility.CustomResponseStatus.UnSuccessful; 
                _result.Message = CustomConstants.NoRecordsFound;
            }
            return _result;
        }

        public string Streamaudio(Stream stream, string format)
        {

            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(HttpContext.Current.Server.MapPath("~/"), fileNamebanners);

            var fileStream = File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();

            var ext = format;

            Stream fileStreambanners = stream;

            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string videoUrl = UploadImage1(fileNamebanners, path, ext, fileStreambanners);
            if (File.Exists(path))
            {
                File.Delete(path);

            }
            return videoUrl;
        }

        public string UploadImage1(string fileName, string path, string ext, Stream inputStream)
        {

            string account = ConfigurationManager.AppSettings["MediaServicesAccountName"];
            string key = ConfigurationManager.AppSettings["MediaServicesAccountKey"];


            CloudMediaContext context = new CloudMediaContext(account, key);

            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);

            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path)); 
     
            assetFile.Upload(path); 

            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 1825;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
                                                     AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;

        }
    }
}