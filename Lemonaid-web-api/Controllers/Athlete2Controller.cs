﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;
 

namespace Lemonaid_web_api.Controllers
{
    public class Athlete2Controller : ApiController
    {

        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        public string WrapperApi = ConfigurationManager.AppSettings["WrapperBaseURL"];

        [HttpPost]
        public Utility.CustomResponse AddAthlete(AthleteDTO athleteDto)
        {  
            try
            {
                if (athleteDto.Id == 0)
                {
                    PaymentController paymentCtrl = new PaymentController();

                    List<SportsDTO> sports = AddAthleteToWrapperDB(athleteDto);

                    athleteDto = CreateUpdateAthlete(athleteDto);

                    var item = sports.Single(x => x.Id == athleteDto.SportId);
                    sports.Remove(item);

                    PaymentDTO payment = new PaymentDTO();

                    if (sports.Count > 0)
                    {
                        foreach (var i in sports)
                        {
                            payment = Dal2.GetPaymentDetailsForAthlete(athleteDto.Emailaddress, i.Id);
                            if (payment.UserID > 0)
                                break;
                        }
                        if (payment.UserID > 0)
                        {
                            //Add it in Present DB 
                            var apy = Dal2.AddPaymentForAthlete(payment, athleteDto.SportId);
                        }

                    }
                }
                else
                {
                    athleteDto = CreateUpdateAthlete(athleteDto);

                }
                if (athleteDto.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteDto;
                    _result.Message = CustomConstants.Athlete_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2/AddAthlete", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        private AthleteDTO CreateUpdateAthlete(AthleteDTO athleteDto)
        {
            AthleteController atc = new AthleteController();
            if (athleteDto.AtheleteType == 0)
            {
                athleteDto.AtheleteType = Convert.ToInt32(Utility.AtheleteType.HighSchoolAthelete);
                athleteDto.Active = false;
            }

            var athelete = Dal2.AddAthlete2_v1(athleteDto);
            athelete.SportId = athleteDto.SportId;
            athelete.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athelete.Id);
            athelete.AtheleteVideos = Dal2.GetAtheleteIdealVideos2_v1();
            string email = string.Empty;
            if (athleteDto.LoginType == 2 || athleteDto.LoginType == 3)
            {
                email = athleteDto.UserName;
            }
            else
            {
                email = athleteDto.Emailaddress;
            }
            List<VideoDTO> uploadedVideos = Dal2.GetAtheleteVideos2_v1(email, athleteDto.LoginType);
            if (uploadedVideos.Count > 0)
            {
                foreach (var av in athelete.AtheleteVideos)
                {
                    foreach (var uv in uploadedVideos)
                    {
                        if (av.VideoNumber == uv.VideoNumber)
                        {
                            av.UploadVideoId = uv.Id;
                            av.UploadDuration = uv.UploadDuration;
                            av.UploadThumbnailUrl = uv.UploadThumbnailUrl;
                            av.VideoURL = uv.VideoURL;
                        }
                    }
                }
            }

            int measurementcount = atc.GetMeasurementCount(athelete.Height, athelete.Height2, athelete.Weight,
                athelete.WingSpan, athelete.ShoeSize);
            athelete.MeasurementCount = measurementcount;

            return athelete;
        }

        public List<SportsDTO> AddAthleteToWrapperDB(AthleteDTO athleteDto)
        {
            List<SportsDTO> sports = new List<SportsDTO>();

            HttpClient client = new HttpClient();
            string wrapperUrl = ConfigurationManager.AppSettings["WrapperBaseURL"];
            client.BaseAddress = new Uri(wrapperUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            AthleteWrapperDTO athlete = new AthleteWrapperDTO();
            athlete.Emailaddress = athleteDto.Emailaddress;
            athlete.LoginType = athleteDto.LoginType;
            athlete.UserName = athleteDto.UserName;
            athlete.UserType = Utility.UserTypes.HighSchooAthlete;
            athlete.DeviceType = athleteDto.DeviceType;
            athlete.DeviceId = athleteDto.DeviceId;
            athlete.Id = 0;
            var response = client.PostAsJsonAsync("User/AddUser", athlete).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;

                if (Convert.ToInt32(res.Response) > 0)
                {
                    AthleteSportsDTO athleteSports = new AthleteSportsDTO();
                    athleteSports.Id = 0;
                    athleteSports.SportId = athleteDto.SportId;
                    athleteSports.UserId = Convert.ToInt32(res.Response);
                    var response1 = client.PostAsJsonAsync("User/AddUserSport", athleteSports).Result;
                    if (response1.IsSuccessStatusCode)
                    {

                        Utility.CustomResponse res1 = response1.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                        sports =  JsonDeserialize<List<SportsDTO>>(res1.Response.ToString());
                    }
                }

            }

            return sports;
        }
        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }


        [HttpGet]
        public Utility.CustomResponse AtheleteProfile(string emailaddress, int logintype)
        {
            AthleteController atc = new AthleteController();
            try
            {
                AthleteDTO athleteDto = Dal2.GetAthleteProfile(emailaddress, logintype);
                if (athleteDto.Id > 0)
                {
                    athleteDto.AtheleteBesttimes = UsersDal.GetAthleteBesttimes(athleteDto.Id);
                    athleteDto.AtheleteVideos = Dal2.GetAtheleteIdealVideos2_v1();
                    string email = string.Empty;
                    if (athleteDto.LoginType == 1 || athleteDto.LoginType == 4)
                    {
                        email = athleteDto.Emailaddress;
                    }
                    else if (athleteDto.LoginType == 2 || (athleteDto.LoginType == 3))
                    { email = athleteDto.UserName; }
                 
                    List<VideoDTO> uploadedVideos = Dal2.GetAtheleteVideos2_v1(email, athleteDto.LoginType);
                    if (uploadedVideos.Count > 0)
                    {
                        foreach (var av in athleteDto.AtheleteVideos)
                        {
                            foreach (var uv in uploadedVideos)
                            {
                                if (av.VideoNumber == uv.VideoNumber)
                                {
                                    av.UploadVideoId = uv.Id;
                                    av.UploadDuration = uv.Duration;
                                    av.UploadThumbnailUrl = uv.ThumbnailURL;
                                    av.UploadVideoUrl = uv.VideoURL;
                                }
                            }
                        }
                    }
                    athleteDto.AthleteVideosCount = uploadedVideos.Count;
                    int measurementcount = atc.GetMeasurementCount(athleteDto.Height, athleteDto.Height2,
                        athleteDto.Weight,
                        athleteDto.WingSpan, athleteDto.ShoeSize);
                    athleteDto.MeasurementCount = measurementcount;
                }
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athleteDto;
                _result.Message = CustomConstants.Details_Get_Successfully;
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2/AtheleteProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetAllCoachesByLocationWithSearch2_v1(int atheleteId, double latitude, double longitude, int offset, int max, int ncaa, int conference, int admission, int locationType, string description, string searchText) // Athlete email address
        {
            AthleteController athleteContr = new AthleteController();
            CoachController coachController = new CoachController();
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }

                var caches = UsersDalV5.GetAllCoachesPagingWithSearch_v5(atheleteId, offset, max, ncaa, conference, admission, description, searchText);
                if (caches.Count > 0)
                {
                    var newList = new List<CoachDTO>();

                    //Location Type 
                    //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                    if (locationType == 1)
                    {
                        newList = caches;

                    }
                    else
                        if (locationType == 2)
                        {
                            for (int i = 0; i < caches.Count; i++)
                            {
                                double athleteLatitude = (caches[i].Latitude == "") ? 0.0 : Convert.ToDouble(caches[i].Latitude);
                                double athleteLongitude = (caches[i].Longitude == "") ? 0.0 : Convert.ToDouble(caches[i].Longitude);
                                double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                if (distance <= 100)
                                {
                                    newList.Add(caches[i]);
                                }
                            }
                        }
                        else
                            if (locationType == 3)
                            {
                                for (int i = 0; i < caches.Count; i++)
                                {
                                    double athleteLatitude = (caches[i].Latitude == "") ? 0.0 : Convert.ToDouble(caches[i].Latitude);
                                    double athleteLongitude = (caches[i].Longitude == "") ? 0.0 : Convert.ToDouble(caches[i].Longitude);
                                    double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                    if (distance <= 5)
                                    {
                                        newList.Add(caches[i]);
                                    }
                                }
                            }




                    if (newList.Count > 0)
                    {
                        List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                        newList = coachController.GetAdCoaches(ads, newList);

                    }
                    //Get College atheletes and coaches
                    List<AthleteDTO> collegeAtheletes = UsersDalV4.GetCollegeAtheletes_v4(searchText, atheleteId);
                    int totalcount = collegeAtheletes.Count + newList.Count;

                    for (int i = 0; i < collegeAtheletes.Count; i++)
                    {
                        collegeAtheletes[i].TotalCount = totalcount;
                    }


                    List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches = new List<CollegeAtheleteCoachesDTO>();
                    List<QuestionaireDTO> questionaireDto = UsersDalV5.GetQuestionaire_v5(atheleteId, "HA");
                    collegeAtheleteCoaches = coachController.AddQuestionairetoList(questionaireDto, collegeAtheleteCoaches);
                    if (newList.Count > 0)
                    {
                        for (int i = 0; i < newList.Count; i++)
                        {
                            newList[i].TotalCount = totalcount;
                        }
                        collegeAtheleteCoaches = coachController.AddCoachestoList(newList, collegeAtheleteCoaches);
                    }
                    collegeAtheleteCoaches = coachController.AddAtheletestoList(collegeAtheletes, collegeAtheleteCoaches);

                    IEnumerable<CollegeAtheleteCoachesDTO> enumcollegeAtheleteCoaches = collegeAtheleteCoaches.Skip(offset).Take(max - offset);
                    collegeAtheleteCoaches = enumcollegeAtheleteCoaches.ToList();

                    collegeAtheleteCoaches = AddCoachVideos(collegeAtheleteCoaches);

                    if (collegeAtheleteCoaches.Count > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = collegeAtheleteCoaches;
                        _result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2/GetAllCoachesByLocationWithSearch2_v1", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        public List<CollegeAtheleteCoachesDTO> AddCoachVideos(List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches)
        { 
            foreach (var v in collegeAtheleteCoaches)
            {
                List<VideoDTO> coachVideos = new List<VideoDTO>();
                List<VideoDTO> totalcoachVideos = new List<VideoDTO>();
                List<int> coachIds = Dal2.GetUniversityCoaches(v.Id);
                foreach (var j in coachIds)
                { 
                    coachVideos = Dal2.GetCoachVideos(j);
                    foreach (var Video in coachVideos)
                    {
                        totalcoachVideos.Add(Video);
                    }
                  
                }

                v.CoachVideos = totalcoachVideos;
            }

            return collegeAtheleteCoaches;
        }

     


        //Not in Use

        //[HttpGet]
        //public Utility.CustomResponse GetAthleteFavourites2_v1(int atheleteId , int offset , int max)
        //{

        //    try
        //    {
        //        CoachController coachController = new CoachController();
        //        var coaches = UsersDalV4.GetAtheleteFavourites_v4(atheleteId, offset, max);
        //        foreach (var c in coaches)
        //        {
        //            int chatCount = UsersDalV4.GetChatCount(c.CoachId, atheleteId);
        //            c.ChatCount = chatCount;

        //        }
        //        var collegeAtheletes = UsersDalV4.GetCollegeAtheletesInAtheleteFavourites_v4(atheleteId, offset, max);
        //        foreach (var a in collegeAtheletes)
        //        {
        //            int chatCount = UsersDalV4.GetChatCount(a.Id, atheleteId);
        //            a.ChatCount = chatCount;
        //        }
        //        List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches = new List<CollegeAtheleteCoachesDTO>();

        //        collegeAtheleteCoaches = coachController.AddCoachestoList(coaches, collegeAtheleteCoaches);
        //        collegeAtheleteCoaches = coachController.AddAtheletestoList(collegeAtheletes, collegeAtheleteCoaches);

        //        var totalLikesCount = collegeAtheleteCoaches.Count(p => p.Liked);

        //        foreach (CollegeAtheleteCoachesDTO t in collegeAtheleteCoaches)
        //        {
        //            t.TotalLikedCount = totalLikesCount;
        //        }

        //        IEnumerable<CollegeAtheleteCoachesDTO> sortedCollegeAtheleteCoaches = collegeAtheleteCoaches.ToList().OrderByDescending(x => x.Liked);

        //        IEnumerable<CollegeAtheleteCoachesDTO> enumcollegeAtheleteCoaches = sortedCollegeAtheleteCoaches.Skip(offset).Take(max);

        //        collegeAtheleteCoaches = enumcollegeAtheleteCoaches.ToList();

        //        collegeAtheleteCoaches = AddCoachVideos(collegeAtheleteCoaches);

        //        if (collegeAtheleteCoaches.Count > 0)
        //        {
        //            _result.Status = Utility.CustomResponseStatus.Successful;
        //            _result.Response = collegeAtheleteCoaches;
        //            _result.Message = CustomConstants.Details_Get_Successfully;
        //        }
        //        else
        //        {
        //            _result.Status = Utility.CustomResponseStatus.UnSuccessful;
        //            _result.Message = CustomConstants.No_Matches;
        //        }

        //        return _result;
        //    }
        //    catch (Exception ex)
        //    {
        //        EmailLogModel emaillog = new EmailLogModel();
        //        emaillog.AddEmailLog("Athlete2/GetAthleteFavourites2_v1", "", ex.Message, "Exception", 0);
        //        _result.Status = Utility.CustomResponseStatus.Exception;
        //        _result.Message = ex.Message;
        //        return _result;
        //    }
        //}




        [HttpGet]
        public Utility.CustomResponse InsertIntoSwimmingMetrics()
        {
            try
            {
                MetricsDto.SwimmingMetricsDto metrics = new MetricsDto.SwimmingMetricsDto();
                int CurrathleteId;
                int prevAthId = 0;
                List<AthleteEventDTO> besttimes = DalMetrics.GetAthleteBestimes();
                for (int i = 0; i < besttimes.Count; i++)
                {
                    CurrathleteId = besttimes[i].AtheleteId;
                    if (CurrathleteId != prevAthId)
                    {
                        List<AthleteEventDTO> athbesttimes = GetAthleteBestimes(besttimes, besttimes[i].AtheleteId);
                        if (athbesttimes.Count == 3)
                        {
                            metrics.AthleteId = athbesttimes[0].AtheleteId;
                            metrics.EventType1 = athbesttimes[0].EventTypeId;
                            metrics.BesttimeValue1 = athbesttimes[0].BesttimeValue;
                            metrics.EventType2 = athbesttimes[1].EventTypeId;
                            metrics.BesttimeValue2 = athbesttimes[1].BesttimeValue;
                            metrics.EventType3 = athbesttimes[2].EventTypeId;
                            metrics.BesttimeValue3 = athbesttimes[2].BesttimeValue;
                        }
                        else if (athbesttimes.Count == 2)
                        {
                            metrics.AthleteId = athbesttimes[0].AtheleteId;
                            metrics.EventType1 = athbesttimes[0].EventTypeId;
                            metrics.BesttimeValue1 = athbesttimes[0].BesttimeValue;
                            metrics.EventType2 = athbesttimes[1].EventTypeId;
                            metrics.BesttimeValue2 = athbesttimes[1].BesttimeValue;
                            metrics.EventType3 = 0;
                            metrics.BesttimeValue3 = string.Empty;
                        }
                        else
                        {
                            metrics.AthleteId = athbesttimes[0].AtheleteId;
                            metrics.EventType1 = athbesttimes[0].EventTypeId;
                            metrics.BesttimeValue1 = athbesttimes[0].BesttimeValue;
                            metrics.EventType2 = 0;
                            metrics.BesttimeValue2 = string.Empty;
                            metrics.EventType3 = 0;
                            metrics.BesttimeValue3 = string.Empty;
                        }

                        DalMetrics.AddSwimmingMetrics(metrics);
                    }
                    prevAthId = CurrathleteId;
                }


                _result.Status = Utility.CustomResponseStatus.Successful; 
                    _result.Message = CustomConstants.Athlete_Added;
             
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2/InsertIntoSwimmingMetrics", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        private List<AthleteEventDTO> GetAthleteBestimes(List<AthleteEventDTO> besttimes, int athId)
        {

            List<AthleteEventDTO> athbesttimes = besttimes.Where(s => s.AtheleteId == athId).ToList();
            return athbesttimes;
        }


        [HttpGet]
        public Utility.CustomResponse Countries()
        {

            try
            {
                var countries = Dal2.GetCountries();
                if (countries.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = countries;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athelete2/Countries", "", ex.Message, "Exception", 0);
                return _result;
            }

        }



        [HttpGet]
        public Utility.CustomResponse UpdateNotificationStatus(int id, int status,int sportId)
        {
            try
            {
                var athleteInsId = UsersDal.UpdateNotificationStatus(id, status);

                 
                if (UpdateNotifications(id, status, sportId))
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteInsId;
                    _result.Message = CustomConstants.Notification_Updated;
                } 
                else
                {

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = athleteInsId;
                    _result.Message = CustomConstants.Notification_Updated;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        private bool UpdateNotifications(int id, int status, int sportId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();

            bool istrue = false;
            string baseUrl = ConfigurationManager.AppSettings["BaseURL"];
            NotificationDTO notification = UsersDal.GetNotificationsById(id);
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            if (notification.NotificationId == 4)
            {
                if (status == 1)
                {
                    //get instituteId from CoachId
                    CoachProfileDTO coach = UsersDal.GetCoachProfile(notification.UserID);
                    var response = client.GetAsync("/Athlete/UpdateAthleteInstitution?AtheleteId=" + notification.SenderId + "&InstituteId=" + coach.InstituteId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                        if (res.Status == Utility.CustomResponseStatus.Successful)
                        {
                            istrue = true;
                        }

                        //Delete from Wrapper UserSports and Update to CA 
                        DeleteCollegeAthleteInOtherSports(notification.SenderId, sportId);
                    }
                }
                else
                {
                    
                    int atheleteId = Dal2.UpdateAtheleteTransition(notification.SenderId, 0);

                    if (atheleteId > 0)
                    {
                        istrue = true;
                    }
                }

            }
            else if (notification.NotificationId == 7)
            {
                if (status == 1)
                {
                    var response = client.GetAsync("/Athlete/UpdateAthleteInstitution?AtheleteId=" + notification.UserID + "&InstituteId=" + notification.SenderId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                        if (res.Status == Utility.CustomResponseStatus.Successful)
                        {
                            istrue = true;
                        }

                        //Delete from Wrapper UserSports and Update to CA 
                        DeleteCollegeAthleteInOtherSports(notification.UserID, sportId);
                    }
                }
                else
                { 
                    int atheleteId = Dal2.UpdateAtheleteTransition(notification.UserID, 0); 
                    if (atheleteId > 0)
                    {

                        istrue = true;
                    }
                }
            }
            else if (notification.NotificationId == 5)
            {
                if (status == 1)
                {
                    var response = client.GetAsync("/Athletev4/ChangeStatusOfAthleteBadges_v4?AtheleteId=" + notification.SenderId + "&typeId=" + notification.BadgeTypeId + "&count=" + notification.BadgeCount + "&status=" + status).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                        if (res.Status == Utility.CustomResponseStatus.Successful)
                        {
                            istrue = true;
                        }
                    }
                }
            }
            else if (notification.NotificationId == 9)
            {
                if (status == 1)
                {
                    var response = client.GetAsync("/ClubCoach/UpdateClubCoachToAthelete?clubCoachId=" + notification.SenderId + "&atheleteId=" + notification.UserID).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                        if (res.Status == Utility.CustomResponseStatus.Successful)
                        {
                            istrue = true;
                        }
                    }
                }
            }


            return istrue;
        }

       

        private void DeleteCollegeAthleteInOtherSports(int athleteId, int sportId)
        {
           
            //Get Emailaddress from atheleteId , send to Wrapper and Execute the DeleteCollegeAthelete in the current sport
            AthleteDTO athlete = Dal2.GetAthleteProfileById(athleteId);
            if(athlete.LoginType == 2 || athlete.LoginType == 3)
            {
                athlete.Emailaddress = athlete.UserName;
            }
            List<SportsDTO> athleteSports = Dal2.GetMultiSports(athlete.Emailaddress, athlete.LoginType, Utility.UserTypes.HighSchooAthlete);

            int Id = Dal2.DeleteCollegeAthleteInWrapper(athlete.Emailaddress, sportId, athlete.LoginType);
             var item = athleteSports.Single(x => x.Id == sportId);
             if (athleteSports.Count > 1)
             {
                 athleteSports.Remove(item);
                 foreach (var i in athleteSports)
                 {
                     int Aid = Dal2.DeleteCollegeAthlete(i.Id, athlete.Emailaddress, athlete.LoginType);
                   
                 }
             }
        }

        [HttpPost]
        public Utility.CustomResponse GetAthleteAnalyticsData(AnalyticsDTO analytics)
        {
            try
            {
                AnalyticsDTO athleteAnalytics = Dal2.GetAthleteAnalyticsData(analytics);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = athleteAnalytics;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2/GetAthleteAnalyticsData", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }



        [HttpGet]
        public Utility.CustomResponse GetAllUniversitiesRank(int offset, int max)
        { 
            try
            {
                var ranks = Dal2_v1.GetAllUniversitiesRank(offset,  max);
               
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = ranks;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse GetNAIAUniversitiesRank(int offset, int max)
        {

            try
            {
                var ranks = Dal2_v1.GetNAIAUniversitiesRank(offset, max);
              
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = ranks;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
    }
}
