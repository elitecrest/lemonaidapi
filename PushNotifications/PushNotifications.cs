﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace PushNotifications
{
    public class PushNotifications
    {
        
        public static void GetMessageWaitingNotifications(string conn)
        {
            //string wrapperConn = "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrapperStaging;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30";
            string wrapperConn = "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrapper;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30";
            try
            {
                List<PushNotifMessages> msgs = DAL_Notif.GetPushNotifMessages(conn, 1);
                if (msgs.Count > 0 && Convert.ToDateTime(msgs[0].LastRunDate) != Convert.ToDateTime("1900-01-01"))
                { 
                    TimeSpan ts = DateTime.Now - msgs[0].LastRunDate;
                    if (ts.TotalHours > 12)
                    {
                        List<UsertypeChatDTO> userChats = new List<UsertypeChatDTO>();
                        var UserAdChatCountAthlete = DAL_Notif.GetUsersChatAdChatCount(UserTypes.HighSchooAthlete, wrapperConn);
                        userChats = AddChatlist(userChats, UserAdChatCountAthlete);
                        var UserAdChatCountCoach = DAL_Notif.GetUsersChatAdChatCount(UserTypes.CollegeCoach, wrapperConn);
                        userChats = AddChatlist(userChats, UserAdChatCountCoach);
                        var UserAdChatCountClubCoach = DAL_Notif.GetUsersChatAdChatCount(UserTypes.ClubCoach, wrapperConn);
                        userChats = AddChatlist(userChats, UserAdChatCountClubCoach);
                        var UserAdChatCountFamilyFriends = DAL_Notif.GetUsersChatAdChatCount(UserTypes.FamilyFriends, wrapperConn);
                        userChats = AddChatlist(userChats, UserAdChatCountFamilyFriends);

                        List<UsertypeChatDTO> AtheleteandCoachChats = new List<UsertypeChatDTO>();
                        var AtheleteChatCount = DAL_Notif.GetUsersChatCount(UserTypes.HighSchooAthlete, conn);
                        AtheleteandCoachChats = AddChatlist(AtheleteandCoachChats, AtheleteChatCount);
                        var CoachChatCount = DAL_Notif.GetUsersChatCount(UserTypes.CollegeCoach, conn);
                        AtheleteandCoachChats = AddChatlist(AtheleteandCoachChats, CoachChatCount);
                        userChats = AddChatlist(userChats, AtheleteandCoachChats);

                         
                        foreach (var i in userChats)
                        {
                            //PushNotification
                            PushNotification(i.DeviceID, msgs[0].Message);
                        }

                        DAL_Notif.UpdateWorkerDate(conn, 1);
                    }
                }
                else
                {
                    List<UsertypeChatDTO> userChats = new List<UsertypeChatDTO>();
                    var UserAdChatCountAthlete = DAL_Notif.GetUsersChatAdChatCount(UserTypes.HighSchooAthlete, wrapperConn);
                    userChats = AddChatlist(userChats, UserAdChatCountAthlete);
                    var UserAdChatCountCoach = DAL_Notif.GetUsersChatAdChatCount(UserTypes.CollegeCoach, wrapperConn);
                    userChats = AddChatlist(userChats, UserAdChatCountCoach);
                    var UserAdChatCountClubCoach = DAL_Notif.GetUsersChatAdChatCount(UserTypes.ClubCoach, wrapperConn);
                    userChats = AddChatlist(userChats, UserAdChatCountClubCoach);
                    var UserAdChatCountFamilyFriends = DAL_Notif.GetUsersChatAdChatCount(UserTypes.FamilyFriends, wrapperConn);
                    userChats = AddChatlist(userChats, UserAdChatCountFamilyFriends);

                    List<UsertypeChatDTO> AtheleteandCoachChats = new List<UsertypeChatDTO>();
                    var AtheleteChatCount = DAL_Notif.GetUsersChatCount(UserTypes.HighSchooAthlete, conn);
                    AtheleteandCoachChats = AddChatlist(AtheleteandCoachChats, AtheleteChatCount);
                    var CoachChatCount = DAL_Notif.GetUsersChatCount(UserTypes.CollegeCoach, conn);
                    AtheleteandCoachChats = AddChatlist(AtheleteandCoachChats, CoachChatCount);
                    userChats = AddChatlist(userChats, AtheleteandCoachChats);


                    foreach (var i in userChats)
                    {
                        //PushNotification
                        PushNotification(i.DeviceID, msgs[0].Message);
                    }

                    DAL_Notif.UpdateWorkerDate(conn, 1);
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("PushNotifications/GetMessageWaitingNotifications", "", ex.Message, "Exception", 0);
             
            }

        }

        public static List<UsertypeChatDTO> AddChatlist(List<UsertypeChatDTO> userChats, List<UsertypeChatDTO> ChatsDto)
        {
            foreach (var a in ChatsDto)
            {
                userChats.Add(a);
            }
            return userChats;
        }


     
        public static void GetNoSwipesandRecommendationsNotifications(string conn)
        {

            try
            {
                List<PushNotifMessages> msgs = DAL_Notif.GetPushNotifMessages(conn, 2);
                if (msgs.Count > 0 && Convert.ToDateTime(msgs[0].LastRunDate) != Convert.ToDateTime("1900-01-01"))
                {
                    TimeSpan ts = DateTime.Now - msgs[0].LastRunDate;
                    if (ts.TotalHours > 48)
                    {
                        List<UsertypeChatDTO> userChats = new List<UsertypeChatDTO>();
                        var UserAtheleteToInstitute = DAL_Notif.GetNoSwipesForAtheleteToInstitute(conn);
                        userChats = AddChatlist(userChats, UserAtheleteToInstitute);

                        var UserInstituteToAthelete = DAL_Notif.GetNoSwipesForInstituteToAthelete(conn);
                        userChats = AddChatlist(userChats, UserInstituteToAthelete);

                        var UserFFFToAthelete = DAL_Notif.GetNoSwipesForFFFToAthelete(conn);
                        userChats = AddChatlist(userChats, UserFFFToAthelete);

                        var UserFFFToInstitute = DAL_Notif.GetNoSwipesForFFFToInstitute(conn);
                        userChats = AddChatlist(userChats, UserFFFToInstitute);

                        var UserClubCoachToInstitute = DAL_Notif.GetNoSwipesForClubCoachToInstitute(conn);
                        userChats = AddChatlist(userChats, UserClubCoachToInstitute);

                        var UserRecomendsForCLCToInstitute = DAL_Notif.GetNoRecomendsForCLCToInstitute(conn);
                        userChats = AddChatlist(userChats, UserRecomendsForCLCToInstitute);

                        var UserRecomendsForFFFToInstitute = DAL_Notif.GetNoRecomendsForFFFToInstitute(conn);
                        userChats = AddChatlist(userChats, UserRecomendsForFFFToInstitute);

                        foreach (var i in userChats)
                        {
                            //PushNotification
                            PushNotification(i.DeviceID, msgs[0].Message);
                        }
                        DAL_Notif.UpdateWorkerDate(conn, 2);
                    }
                }
                else
                {
                    List<UsertypeChatDTO> userChats = new List<UsertypeChatDTO>();
                    var UserAtheleteToInstitute = DAL_Notif.GetNoSwipesForAtheleteToInstitute(conn);
                    userChats = AddChatlist(userChats, UserAtheleteToInstitute);

                    var UserInstituteToAthelete = DAL_Notif.GetNoSwipesForInstituteToAthelete(conn);
                    userChats = AddChatlist(userChats, UserInstituteToAthelete);

                    var UserFFFToAthelete = DAL_Notif.GetNoSwipesForFFFToAthelete(conn);
                    userChats = AddChatlist(userChats, UserFFFToAthelete);

                    var UserFFFToInstitute = DAL_Notif.GetNoSwipesForFFFToInstitute(conn);
                    userChats = AddChatlist(userChats, UserFFFToInstitute);

                    var UserClubCoachToInstitute = DAL_Notif.GetNoSwipesForClubCoachToInstitute(conn);
                    userChats = AddChatlist(userChats, UserClubCoachToInstitute);

                    var UserRecomendsForCLCToInstitute = DAL_Notif.GetNoRecomendsForCLCToInstitute(conn);
                    userChats = AddChatlist(userChats, UserRecomendsForCLCToInstitute);

                    var UserRecomendsForFFFToInstitute = DAL_Notif.GetNoRecomendsForFFFToInstitute(conn);
                    userChats = AddChatlist(userChats, UserRecomendsForFFFToInstitute);

                    foreach (var i in userChats)
                    {
                        //PushNotification
                        PushNotification(i.DeviceID, msgs[0].Message);
                    }
                    DAL_Notif.UpdateWorkerDate(conn, 2);
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("PushNotifications/GetNoSwipesandRecommendationsNotifications", "", ex.Message, "Exception", 0);
               
            }

        }

        public static void GetAcademicProfilesNotCompleted(string conn)
        {

            try
            {
                List<PushNotifMessages> msgs = DAL_Notif.GetPushNotifMessages(conn, 3); 
                var athleteProfiles = DAL_Notif.GetAcademicProfilesNotCompleted(conn);

                foreach (var i in athleteProfiles)
                {
                    PushNotifMessages notif = DAL_Notif.GetAthletePushNotifLastRunDate(conn, 3, i.ID);
                    if (notif.PushNotificationId == 0)
                    {
                        TimeSpan ts = DateTime.Now - i.CreatedDate;
                        if (ts.TotalHours > 48)
                        {
                            PushNotification(i.DeviceID, msgs[0].Message);
                            DAL_Notif.UpdateAthletePushNotifRunDate(conn, 3, i.ID);
                        }
                    }
                    else
                    {
                        TimeSpan ts = DateTime.Now - notif.LastRunDate;
                        if (ts.Days > 30)
                        {
                            PushNotification(i.DeviceID, msgs[0].Message);
                            DAL_Notif.UpdateAthletePushNotifRunDate(conn, 3, i.ID);
                        }
                    }


                }

            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("PushNotifications/GetNoSwipesandRecommendationsNotifications", "", ex.Message, "Exception", 0);

            }

        }

        public static void GetAthleteProfilesNotCompleted(string conn)
        {

            try
            {
                List<PushNotifMessages> msgs = DAL_Notif.GetPushNotifMessages(conn, 4); 
                var athleteProfiles = DAL_Notif.GetAthleteProfilesNotCompleted(conn); 

                foreach (var i in athleteProfiles)
                {
                    PushNotifMessages notif = DAL_Notif.GetAthletePushNotifLastRunDate(conn, 4, i.ID);
                    if(notif.PushNotificationId == 0)
                    {
                        TimeSpan ts = DateTime.Now - i.CreatedDate;
                        if(ts.Days > 7)
                        {
                            PushNotification(i.DeviceID, msgs[0].Message);
                            DAL_Notif.UpdateAthletePushNotifRunDate(conn, 4, i.ID);
                        }
                    }
                    else
                    {
                        TimeSpan ts = DateTime.Now - notif.LastRunDate;
                        if (ts.Days > 30)
                        {
                            PushNotification(i.DeviceID, msgs[0].Message);
                            DAL_Notif.UpdateAthletePushNotifRunDate(conn, 4, i.ID);
                        }
                    }

                   
                }
               
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("PushNotifications/GetNoSwipesandRecommendationsNotifications", "", ex.Message, "Exception", 0);

            }

        }


        public static void GetAboutmeProfilesNotCompleted(string conn)
        {

            try
            {
                List<PushNotifMessages> msgs = DAL_Notif.GetPushNotifMessages(conn, 5);
                 
                var athleteProfiles = DAL_Notif.GetAboutmeProfilesNotCompleted(conn);

                foreach (var i in athleteProfiles)
                {
                    PushNotifMessages notif = DAL_Notif.GetAthletePushNotifLastRunDate(conn, 5, i.ID);
                    if (notif.PushNotificationId == 0)
                    {
                        TimeSpan ts = DateTime.Now - i.CreatedDate;
                        if (ts.Days > 7)
                        {
                            PushNotification(i.DeviceID, msgs[0].Message);
                            DAL_Notif.UpdateAthletePushNotifRunDate(conn, 5, i.ID);
                        }
                    }
                    else
                    {
                        TimeSpan ts = DateTime.Now - notif.LastRunDate;
                        if (ts.Days > 30)
                        {
                            PushNotification(i.DeviceID, msgs[0].Message);
                            DAL_Notif.UpdateAthletePushNotifRunDate(conn, 5, i.ID);
                        }
                    }


                }

            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("PushNotifications/GetNoSwipesandRecommendationsNotifications", "", ex.Message, "Exception", 0);

            }

        }


        internal static bool PushNotification(string Name, string Message)
        {
            bool IsSend = false;
            if (Name != string.Empty || Name != null)
            {

                try
                {
                    FirebaseDTO firebase = GetFirebaseCredentials();
                    string applicationID = firebase.FirebaseAppId;
                    string senderId = firebase.FirebaseSenderId;

                    string deviceId = Name;

                    WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                    tRequest.Method = "post";
                    tRequest.ContentType = "application/json";
                    var data = new
                    {
                        to = deviceId,
                        notification = new
                        {
                            body = Message,
                            title = "Lemonaid Notification",
                            sound = "Enabled"

                        }
                    };
                    var serializer = new JavaScriptSerializer();
                    var json = serializer.Serialize(data);
                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                    tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                    tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                    tRequest.ContentLength = byteArray.Length;
                    using (Stream dataStream = tRequest.GetRequestStream())
                    {
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        using (WebResponse tResponse = tRequest.GetResponse())
                        {
                            using (Stream dataStreamResponse = tResponse.GetResponseStream())
                            {
                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                    string str = sResponseFromServer;
                                    IsSend = true;
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    string str = ex.Message;
                }
            }
            return IsSend;
        }


        internal static FirebaseDTO GetFirebaseCredentials()
        {
            FirebaseDTO firebaseDTO = new FirebaseDTO();
            try
            {
                string conn = "Server=tcp:ir7b6blvx5.database.windows.net,1433;Database=LemonaidWrapper;User ID=LemonaidDB@ir7b6blvx5;Password=Lemon%23;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";
                SqlConnection myConn = new SqlConnection(conn);
                myConn.Open();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFirebaseCredentials]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        firebaseDTO.FirebaseAppId = (myData["FirebaseAPPID"] == DBNull.Value) ? "" : (string)myData["FirebaseAPPID"];
                        firebaseDTO.FirebaseSenderId = (myData["FirebaseSenderId"] == DBNull.Value) ? "" : (string)myData["FirebaseSenderId"];

                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                //EmailLogModel emaillog = new EmailLogModel();
                //emaillog.AddEmailLog("DAL2v6/GetFirebaseCredentials ", "", ex.Message, "Exception", 0);
                //throw;
            }

            return firebaseDTO;
        }

        public partial class FirebaseDTO
        {
            public string FirebaseAppId { get; set; }
            public string FirebaseSenderId { get; set; }
        }
        public  class UsertypeChatDTO
        {
            public int ID { get; set; }
            public string Email { get; set; }
            public int LoginType { get; set; }
            public string UserName { get; set; }
            public string DeviceType { get; set; }
            public string DeviceID { get; set; }
            public DateTime CreatedDate { get; set; }
        }

        public static class UserTypes
        {
            public static string HighSchooAthlete = "HS";
            public static string CollegeCoach = "CC";
            public static string ClubCoach = "ClC";
            public static string CollegeAthlete = "CA";
            public static string FamilyFriends = "FFF";
        }


        public partial class PushNotifMessages
        {
            public int PushNotificationId { get; set; }
            public string Message { get; set; } 
            public DateTime LastRunDate { get; set; } 
            public int AthleteId { get; set; } 
            public int isFirst { get; set; }
        }
    }
}
