﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.IO;


namespace Lemonaid_web_api.Controllers
{
    public class FamilyFriends2Controller : ApiController
    {

        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddfamilyFriends(FamilyFriendsDTO familyFriendsDto)
        {
            FamilyFriendsController ffctrl = new FamilyFriendsController();
            try
            {
                if (familyFriendsDto.Id == 0)
                {
                    List<SportsDTO> sports = ffctrl.AddFFFToWrapperDb(familyFriendsDto);
                }

                var familyFriends = Dal2_v1.AddfamilyFriends2_v1(familyFriendsDto);
                if (familyFriends.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = familyFriends;
                    _result.Message = CustomConstants.FamilyFriend_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }

            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("FamilyFriends/AddfamilyFriends", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
            return _result;

        }
    }
}
