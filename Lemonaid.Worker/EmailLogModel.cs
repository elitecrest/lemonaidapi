﻿using System; 
using System.Data; 
using System.Data.SqlClient;
using System.Configuration;

namespace Lemonaid.Worker
{
    public class EmailLogModel
    {
        public class EmailDTO
        {
            public string From { get; set; }
            public string To { get; set; }
            public string Subject { get; set; }
            public int Status { get; set; }
            public string MailType { get; set; }
        }

        public int AddEmailLog(string @from, string to, string subjectorErr, string mailType, int status)
        {
            EmailDTO email = new EmailDTO
            {
                From = @from,
                To = to,
                Status = status,
                Subject = subjectorErr,
                MailType = mailType
            };
            AddEmailLog(email);
            return 1;
        }

        internal static int AddEmailLog(EmailDTO emailDTO)
        {
            int id = 0;
            SqlConnection myConn = null;//ConnectToDb();

            try
            {
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddEmailLog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter from = cmd.Parameters.Add("@From", SqlDbType.VarChar, 1000);
                    from.Direction = ParameterDirection.Input;
                    from.Value = emailDTO.From;

                    SqlParameter to = cmd.Parameters.Add("@To", SqlDbType.VarChar, 1000);
                    to.Direction = ParameterDirection.Input;
                    to.Value = emailDTO.To;

                    SqlParameter subject = cmd.Parameters.Add("@Subject", SqlDbType.VarChar, 1000);
                    subject.Direction = ParameterDirection.Input;
                    subject.Value = emailDTO.Subject;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    status.Direction = ParameterDirection.Input;
                    status.Value = emailDTO.Status;

                    SqlParameter mailType = cmd.Parameters.Add("@MailType", SqlDbType.VarChar, 50);
                    mailType.Direction = ParameterDirection.Input;
                    mailType.Value = emailDTO.MailType;

                    SqlDataReader myData = cmd.ExecuteReader();
                    while (myData.Read())
                    {

                        id = (int)myData["Id"];
                    }
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("UsersDal/AddEmailLog", "", ex.Message, "Exception", 0);
            }
            return id;
        }

      
    }
}
