﻿using System;
 

namespace Lemonaid_web_api
{
    public class Utility
    {
        public class CustomResponse
        {
            public CustomResponseStatus Status;
            public Object Response;
            public string Message;
        }

   

        public enum CustomResponseStatus
        {
            Successful,
            UnSuccessful,
            Exception,
            UnmappedUniversityUser, // Exceptional only for when coach is not mapped to the university
            PaymentInProgress, // Coach has not done the Payment
            CoachNewUser, // Coach does not exist
            AthleteInOtherSport //Athlete Profile Check
        }

        public enum AtheleteType
        {
           HighSchoolAthelete,
           CollegeAthelete
        }

        public enum SenderType
        {
            Athelete,
            Coach,
            CollegeAthlete,
            ClubCoach,
            FamilyFriends
        }

        public static class UserTypes
        { 
            public static string HighSchooAthlete = "HS";
            public static string CollegeCoach = "CC";
            public static string ClubCoach = "ClC";
            public static string CollegeAthlete = "CA";
            public static string FamilyFriends = "FFF";
        }

        public enum AdType
        {
            NormalAd = 1,
            FrontRushAd = 4,
            ChatAd = 5 
        }
    }

    public static class CustomConstants
    {
        public static string User_Does_Not_Exist = "User does not exist";
        public static string User_Exist_With_Email = "User already exist";
        public static string InValid_Credential = "Invalid credential";
        public static string Registered_Successfully = "Registered successfully";
        public static string NoRecordsFound = "No records found";
        public static string Details_Get_Successfully = "Data retrieved successfully";
        public static string Already_Exists = "Record already exists";
        public static string Athlete_Bestimes_Added = "Data saved";
        public static string Athlete_Bestimes_Updated = "Data updated successfully";
        public static string Chat_Added = "Chat added successfully";
        public static string Coach_Added = "Coach added successfully";
        public static string Athlete_Added = "Athlete added successfully";
        public static string Status_Added = "Status added successfully";
        public static string Athlete_Invitation_Added = "Data added successfully"; 
        public static string Blog_Added = "Blog added successfully";
        public static string Match_Removed = "Match removed successfully";
        public static string Athlete_Accepted_Added = "Athlete accepted successfully";
        //public static string Enrolled_successfully = "Please click on the verification link which is sent to your email address. After confirmation, please login to the app with your credentials."; 
        public static string Enrolled_successfully = "Thanks for registering. Please authenticate yourself by clicking the link we sent to email . Didn’t get the email? First check your spam or junk folder. If you still can’t find it, please contact our support team.";
        public static string Coach_Active_Status = "Please authenticate yourself by clicking the link we sent to email . Didn’t get the email? First check your spam or junk folder. If you still can’t find it, please contact our support team."; 
        public static string Login_successfully = "Login Successfully";
        public static string Response_Invalid = "The code is expired or otherwise not valid for the specified user.";
        public static string Response_Waiting = "The code has not been claimed yet.";
        public static string Usage_Tracking_Successfully = "Usage tracking inserted successfully"; 
        public static string Blog_Updated = "Blog updated successfully"; 
        public static string Blog_Deleted = "Blog deleted successfully";
        public static string Invalid_HostName = "Please enter valid emailaddress for the university";
        public static string Video_Added = "Video added successfully";
        public static string Thumbnail_Updated = "Thumbnail updated successfully"; 
        public static string Video_Deleted_Successfully = "Deleted successfully";
        public static string Payment_Added_Successfully = "Payment added successfully";
        public static string Record_Not_Added = "Record has not been added";
        public static string Verification_Success = "Hi User,verification done successfully.Please login into app with your credentials."; 
        public static string Coach_Profile_Updated = "Coach profile has been updated successfully"; 
        public static string Mail_Sent = "Email has been sent successfully";
        public static string Athlete_Institution_Added = "Sweetness!  Athlete has been added to your roster.";
        public static string SignIn_Mail = "An email has been sent to the coach of this program to confirm you.";
        public static string TeamRoaster_Added = "Teamroster added successfully"; 
        public static string ForgotPassword_successfully = "Mail sent successfully to registered email address"; 
        public static string ResetPassword_Failed = "Resetting password has been failed.Try again!"; 
        public static string ResetPassword = "Password has been reset successfully";
        public static string Mail_Sent_To_Athlete = "Email has been sent to the athlete successfully"; 
        public static string Notification_Updated = "Notification updated successfully"; 
        public static string clubCoach_Added = "Club coach added successfully";
        public static string Athlete_ClubCoach_Updated = "Club has been updated to the athlete profile successfully";
        public static string Club_Added = "Club added successfully";
        public static string Notification_Sent = "Notification sent successfully"; 
        public static string Notify_University = "University not registered - please notify the administrator or search using search button on the top.";
        public static string Athelete_Already_ClubCoach = "The selected athlete is already associated with other club.";
        public static string FamilyFriend_Added = "Data saved successfully";
        public static string No_Matches = "Right swipe to create your personal list and an opportunity to match message";
        public static string No_Records_Roster = "Please add athletes to your roster";
        public static string No_Notifications = "No Notifications to show";
        public static string No_Staff = "No staff to show";
        public static string No_Universities = "You have to match with the universities to get university list";
        public static string Filters_Added_Successfully = "Filters added successfully";
        public static string Data_not_Inserted = "Data not inserted";
        public static string Photo_Uploaded = "Photo uploaded successfully";
        public static string Metrics_added = "Metrics added successfully";
        public static string No_HeadCoach = "No headcoach for the selected university"; 
        public static string University_Added = "University added successfully";
        public static string Coach_NewUser = "Coach does not exist"; 
        public static string Go_To_Login = "User already exists. Please login with the given credentials.";
        public static string NoTweets = "No Tweets found or your tweets are protected. Please change your settings.";
        public static string Ad_Uploaded = "Ad posted successfully";
        public static string Athlete_Deleted_Sport = "Athlete sport has been deleted successfully"; 
        public static string Data_Not_Found = "No data found";
        public static string Athlete_Video_Added = "Video uploaded successfully";
        public static string ShareVideo_Content_Updated = "Media content uploaded successfully";
        public static string Media_Content_Approved = "Video authenticated and shared with Athlete."; 
        public static string Media_Content_Shared = "Media content shared to clubcoach successfully";
        public static string Athlete_ShareVideo_Added = "Video shared successfully";
        public static string Deleted_Athlete_ShareVideo = "Athlete share video deleted successfully";
        public static string Deleted_Athlete_MediaContent = "Athlete media content deleted successfully"; 
        public static string CoachEmail_Updated = "Coach emailaddress updated successfully";
        public static string Video360_Added = "Coach Video360Type Added Successfully"; 
        public static string Payment_Updated_Successfully = "Payment Updated Successfully";
        public static string ContactUs_added_successfully = "Contactus added Successfully";
        public static string VirtualVideo_Added = "Virtual video added Successfully";
        public static string AddStripe_AccDetails = "Stripe Account Details Added Successfully";
        public static string VirtualVideo_Deleted = "Virtual video deleted successfully";
        public static string Athlete_In_Other_Sport = "Athlete is in another sport";
    }
}