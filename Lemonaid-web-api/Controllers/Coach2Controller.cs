﻿using System; 
using System.Web.Http;
using Lemonaid_web_api.Models;
using System.Net.Http;
using System.Configuration;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Linq; 
using System.IO;
using System.Web;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Http;
using System.IO;


namespace Lemonaid_web_api.Controllers
{
    public class Coach2Controller : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddCoach(CoachUserDTO coachUser)
        {
            CoachV4Controller coachv4Ctrl = new CoachV4Controller();
            try
            {
                List<SportsDTO> sports = AddCoachToWrapperDB(coachUser);
                if (sports.Count > 0)
                {
                    coachUser = UsersDalV4.AddCoachUser_v4(coachUser);
                    CoachDTO coachInfo = UsersDal.GetCoachExists(coachUser.Emailaddress, coachUser.Password);
                    if (coachInfo.Id > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = coachInfo;
                        _result.Message = CustomConstants.Enrolled_successfully;

                        CoachController cc = new CoachController();
                        bool isvalid = cc.CheckForValidEmail(coachInfo.InstituteEmailaddress, coachUser.Emailaddress);
                        //Compare with the domain name . if matched send to coach or else to support and UnivEmail
                        if (isvalid)
                        {
                            DuoLoginController.SendMail(coachUser.Emailaddress, coachUser.ActivationCode,
                                coachUser.CoachId.ToString());
                        }
                        else
                        {
                            coachv4Ctrl.SendMailToSupportandUniversity(coachUser.Emailaddress, coachUser.ActivationCode,
                                coachUser.CoachId.ToString(), coachInfo.InstituteEmailaddress);
                        }
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.User_Does_Not_Exist;

                    }
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/AddCoach", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        public List<SportsDTO> AddCoachToWrapperDB(CoachUserDTO coachDto)
        {
            List<SportsDTO> sports = new List<SportsDTO>();

            HttpClient client = new HttpClient();
            string wrapperUrl = ConfigurationManager.AppSettings["WrapperBaseURL"];
            client.BaseAddress = new Uri(wrapperUrl);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            AthleteWrapperDTO athlete = new AthleteWrapperDTO();
            athlete.Emailaddress = coachDto.Emailaddress;
            athlete.LoginType = 0;
            athlete.UserName = string.Empty;
            athlete.UserType = Utility.UserTypes.CollegeCoach;
            athlete.DeviceType = coachDto.DeviceType;
            athlete.DeviceId = coachDto.DeviceId;
            athlete.Id = 0;
            var response = client.PostAsJsonAsync("User/AddUser", athlete).Result;
            if (response.IsSuccessStatusCode)
            {
                Utility.CustomResponse res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;

                if (Convert.ToInt32(res.Response) > 0)
                {
                    AthleteSportsDTO athleteSports = new AthleteSportsDTO();
                    athleteSports.Id = 0;
                    athleteSports.SportId = coachDto.SportId;
                    athleteSports.UserId = Convert.ToInt32(res.Response);
                    var response1 = client.PostAsJsonAsync("User/AddUserSport", athleteSports).Result;
                    if (response1.IsSuccessStatusCode)
                    {
                        Utility.CustomResponse res1 = response1.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                        sports = JsonDeserialize<List<SportsDTO>>(res1.Response.ToString());
                    }
                }

            }

            return sports;
        }

        public static T JsonDeserialize<T>(string jsonString)
        {

            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        [HttpPost]
        public Utility.CustomResponse AddCoachFilters(CoachFiltersDto coachFiltersDto)
        {
           
            try
            {
                int id = Dal2.AddCoachFilters(coachFiltersDto); 
                if (id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = id;
                    _result.Message = CustomConstants.Coach_Profile_Updated;  
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Data_not_Inserted;

                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/AddCoachFilters", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse CoachProfile(int coachId)
        {

            try
            {
                var coach = Dal2.GetCoachProfile(coachId);
                coach.CoachVideos = Dal2.GetCoachVideos(coach.CoachId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = coach;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/CoachProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse AddCoachVideos(VideoDTO videoDto)
        {

            try
            { 
                var video = Dal2.AddCoachVideos(videoDto);
                if (video > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = video;
                    _result.Message = CustomConstants.Video_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/AddCoachVideos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse UpdateCoachVideoThumbnail(ThumbnailDTO thumbnailDto)
        {

            try
            {

                var video = Dal2.UpdateCoachVideoThumbnail(thumbnailDto);
                if (video.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = video;
                    _result.Message = CustomConstants.Thumbnail_Updated;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/UpdateCoachVideoThumbnail", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse UploadCoachPhotos(ThumbnailDTO thumbnailDto)
        { 
            try
            { 
                var video = Dal2.UploadCoachPhotos(thumbnailDto);
                if (video.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = video;
                    _result.Message = CustomConstants.Photo_Uploaded;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/UploadCoachPhotos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse UpdateCoachProfile(CoachProfileDTO coachProfileDto)
        {

            try
            {
                if (coachProfileDto.Description == "-1")
                {
                    coachProfileDto.Description = string.Empty;
                }

                if (coachProfileDto.Phonenumber == "-1")
                {
                    coachProfileDto.Phonenumber = string.Empty;
                }

                Dal2.UpdateCoachProfile(coachProfileDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Coach_Profile_Updated;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/UpdateCoachProfile", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetAllAthletesByLocationWithSearch2_v1(string emailaddress, double latitude, double longitude, int offset, int max,  int locationType,  string searchText)  
        {
            //Location is not implemented

            AthleteController athleteContr = new AthleteController();
            CoachV5Controller coachv5 = new CoachV5Controller();
            try
            {
                //if (description == "-1")
                //{
                //    description = string.Empty;
                //}
                var athletes = Dal2.GetAllAthletePagingWithSearch(emailaddress, offset, max,  searchText);
                if (athletes.Count > 0)
                {
                    var newList = new List<AthleteDTO>();
                    var newListFromStrokes = new List<AthleteDTO>();
                    var listWithQuestionaire = new List<AthleteDTO>();
                    //Location Type 
                    //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                    if (locationType == 1)
                    {
                        newList = athletes;

                    }
                    else
                        if (locationType == 2)
                        {
                            foreach (var t in athletes)
                            {
                                double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                                double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                                double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                if (distance <= 100)
                                {
                                    newList.Add(t);
                                }
                            }
                        }
                        else
                            if (locationType == 3)
                            {
                                foreach (var t in athletes)
                                {
                                    double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                                    double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                                    var distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                    if (distance <= 5)
                                    {
                                        newList.Add(t);
                                    }
                                }
                            }

                    if (newList[0].Strokes != "")
                    {
                        if (newList[0].Strokes != "ALL")
                        {
                            string[] strStrokes = newList[0].Strokes.Split(',');

                            var atheleteIds = new List<int>();
                            var atheleteStrokes = UsersDal.GetStrokesForAtheletes();
                            foreach (var t in atheleteStrokes)
                            {
                                foreach (string t1 in strStrokes)
                                {
                                    if (t1 == t.Category)
                                    {
                                        atheleteIds.Add(t.AtheleteId);
                                    }
                                }
                            }

                            foreach (AthleteDTO t in newList)
                            {
                                foreach (int t1 in atheleteIds)
                                {
                                    if (t1 == t.Id)
                                    {

                                        newListFromStrokes.Add(t);
                                    }
                                }
                            }
                            newListFromStrokes = newListFromStrokes.Distinct().ToList();
                        }
                        else
                        {

                            newListFromStrokes = newList;
                        }
                    }
                    else
                    { newListFromStrokes = newList; }
                    CoachDTO coach = UsersDal.GetCoachProfileByEmail(emailaddress);
                    List<QuestionaireDTO> questionaireDto = UsersDalV5.GetQuestionaire_v5(coach.CoachId, "CC");
                    listWithQuestionaire = coachv5.AddQuestionairetoList(questionaireDto, listWithQuestionaire);
                    if (newListFromStrokes.Count > 0)
                    {
                        newListFromStrokes = coachv5.AddBadgesToList(newListFromStrokes);
                        //Add BestTimes
                        newListFromStrokes = athleteContr.AddBesttimesToList(newListFromStrokes);
                        List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                        newListFromStrokes = athleteContr.GetAdAthletes(ads, newListFromStrokes);
                        //Add Athelete Videos
                        newListFromStrokes = coachv5.AddVideosToList(newListFromStrokes);

                        listWithQuestionaire = coachv5.AddAtheletestoList(newListFromStrokes, listWithQuestionaire);
                        // ListWithQuestionaire = ListWithQuestionaire.OrderBy(x => x.Diamond).ToList();
                    }
                    if (listWithQuestionaire.Count > 0)
                    {
                        _result.Status = Utility.CustomResponseStatus.Successful;
                        _result.Response = listWithQuestionaire;
                        _result.Message = CustomConstants.Details_Get_Successfully;
                    }
                    else
                    {
                        _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        _result.Message = CustomConstants.NoRecordsFound;
                    }
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/GetAllAthletesByLocationWithSearch2_v1", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse DeleteCoachVideo(int coachId, int videoId)
        {
            try
            {
                Dal2.DeleteCoachVideo(coachId, videoId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = videoId;
                _result.Message = CustomConstants.Video_Deleted_Successfully; 

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/DeleteCoachVideo", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }


        [HttpGet]
        public Utility.CustomResponse AddNewCoach(int coachId, string newCoachEmail)
        {
            SupportMailDTO support = new SupportMailDTO();
            SendGridMailModel sendGridModel = new SendGridMailModel();
            try
            { 
                support = UsersDal.GetSupportMails("RequiredEmail");

                CoachProfileDTO coachProfile = Dal2.GetCoachProfile(coachId);


                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/AddNewCoach.html"));
                var readFile = reader.ReadToEnd();
                string myString = readFile;
                myString = myString.Replace("$$Name$$", newCoachEmail);
                myString = myString.Replace("$$CoachEmail$$", coachProfile.Emailaddress);
                myString = myString.Replace("$$InstituteName$$", coachProfile.InstituteName); 

              
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(newCoachEmail);
                sendGridModel.fromMail = coachProfile.Emailaddress;
                sendGridModel.fromName = coachProfile.CoachName;
                sendGridModel.subject = "Invitation to join Lemonaid App";
                sendGridModel.Message = myString;
                sendGridModel.email = support.EmailAddress;
                sendGridModel.password = support.Password;
                SendGridSMTPMail.SendEmail(sendGridModel);

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(sendGridModel.fromMail, newCoachEmail, sendGridModel.subject, "Mail", 1);

                _result.Status = Utility.CustomResponseStatus.Successful; 
                _result.Message = CustomConstants.Mail_Sent; 
            }
            catch (Exception)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(sendGridModel.fromMail, newCoachEmail, sendGridModel.subject, "Mail", 0);
                

            }

            return _result;
        }


        [HttpPost]
        public Utility.CustomResponse GetCoachAnalyticsData(AnalyticsDTO analytics)
        {
            try
            {
                AnalyticsDTO coachAnalytics = Dal2.GetCoachAnalyticsData(analytics);  
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = coachAnalytics;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Coach2/GetCoachAnalyticsData", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }


        }
    }
}
