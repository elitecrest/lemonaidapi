﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AthleteAnalytics.aspx.cs" Inherits="LemonaidCoach.AthleteAnalytics" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Athlete Analytics</title>
    <script src="Scripts/circles.js"></script>
    <link href="Styles/bootstrap.min.css" rel="stylesheet" />
    <link href="Styles/style.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <table width="80%" align="center">
            <tr>
                <td>
                    <a href="LemonaidInformation.aspx">See lemonaid info</a> | 
                    <a href="SwipesInformation.aspx">See swipes info</a> | 
                    <a href="UsersReport.aspx">Users Report</a> | 
                    <a href="FailedReports.aspx">Misregistrations</a> |
                    <a href="Analytics.aspx">Analytics</a>
                    <asp:LinkButton ID="logout" runat="server" OnClick="logout_Click" Text="Logout" CssClass="logout"></asp:LinkButton>
                </td>
            </tr>
        </table>
    <div class="container">
            <div class="page-header">
                <h3>Athlete Analytics <small class="pull-right"><a href="CoachAnalytics.aspx">Coach Analytics</a> | Athlete Analytics</small></h3>
            </div>
            <div class="jumbotron text-center">
                <div class="row">
                    <div class="col-md-2 col-sm-4 col-xs-4">
                        <label for="athletes">Select Athlete:</label>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <asp:DropDownList ID="athletes" runat="server" CssClass="form-control user-list-dropdown" OnSelectedIndexChanged="athletes_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row" id="canvas">
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="circle" id="circles-1"></div>
                        <h5>In depth<br />
                            Profile Views</h5>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="circle" id="circles-2"></div>
                        <h5>Matches</h5>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="circle" id="circles-3"></div>
                        <h5>Right-Swipes<br />
                            Given</h5>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="circle" id="circles-4"></div>
                        <h5>Right-Swipes<br />
                            Received</h5>
                    </div>
                </div>
            </div>
            <input type="hidden" id="maxProfileViews" runat="server" />
            <input type="hidden" id="profileViews" runat="server" />
            <input type="hidden" id="maxRightSwipesGiven" runat="server" />
            <input type="hidden" id="rightSwipesGiven" runat="server" />
            <input type="hidden" id="maxRightSwipesReceived" runat="server" />
            <input type="hidden" id="rightSwipesReceived" runat="server" />
            <input type="hidden" id="maxMatches" runat="server" />
            <input type="hidden" id="matches" runat="server" />
            <script>
                function BindDataToCircles() {
                    var colors = [
                    ['#edf0f5', '#ffcc00'], ['#edf0f5', '#23b7d1'], ['#edf0f5', '#df4e5f'], ['#edf0f5', '#47dac0']
                    ], circles = [];
                    circles.push(Circles.create({
                        id: document.getElementById('circles-1').id,
                        value: document.getElementById('profileViews').value,
                        radius: 50,
                        width: 10,
                        maxValue: document.getElementById('maxProfileViews').value,
                        colors: colors[0]
                    }));
                    circles.push(Circles.create({
                        id: document.getElementById('circles-2').id,
                        value: document.getElementById('matches').value,
                        radius: 50,
                        width: 10,
                        maxValue: document.getElementById('maxMatches').value,
                        colors: colors[1]
                    }));
                    circles.push(Circles.create({
                        id: document.getElementById('circles-3').id,
                        value: document.getElementById('rightSwipesGiven').value,
                        radius: 50,
                        width: 10,
                        maxValue: document.getElementById('maxRightSwipesGiven').value,
                        colors: colors[2]
                    }));
                    circles.push(Circles.create({
                        id: document.getElementById('circles-4').id,
                        value: document.getElementById('rightSwipesReceived').value,
                        radius: 50,
                        width: 10,
                        maxValue: document.getElementById('maxRightSwipesReceived').value,
                        colors: colors[3]
                    }));
                }
            </script>
        </div>
    </form>
</body>
</html>
