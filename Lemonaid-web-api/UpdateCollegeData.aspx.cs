﻿using OfficeOpenXml;
using System; 
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;


namespace Lemonaid_web_api
{
    public partial class UpdateCollegeData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            ExcelBulkUpload();
        }
        public void ExcelBulkUpload()
        {
            try
            {
                //Get uploaded Excel file
                string filename =  Path.GetFileName(fileuploadExcel.PostedFile.FileName);
                fileuploadExcel.SaveAs(Server.MapPath("Images/" + filename));
                string filepath = Server.MapPath("Images/" + filename);
                //Connect to Excel sheet
                ExcelPackage excelFile = new ExcelPackage(fileuploadExcel.FileContent);
                DataTable dtExcelSchema = excelFile.ToDataTable();
                //Fill DataSet object with Excel sheet data
                DataSet ds = new DataSet();
                ds.Tables.Add(dtExcelSchema);
                //Connect to Database
                string sConnectionString;
                sConnectionString = ConfigurationManager.ConnectionStrings["LemonaidDB"].ToString();
                SqlConnection objConn = new SqlConnection(sConnectionString);
                objConn.Open();
                //Take Institution data to DataSet
                SqlDataAdapter daInstitutions = new SqlDataAdapter("Select * From Institution", objConn);
                DataSet dsPubs = new DataSet("LemonaidBizTest");
                daInstitutions.FillSchema(dsPubs, SchemaType.Source, "Institution");
                daInstitutions.Fill(dsPubs, "Institution");
                DataTable tblInstitutions;
                tblInstitutions = dsPubs.Tables["Institution"];

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string fbURL = dr.Field<string>("facebookurl");
                    string profilePicURL = fbURL;
                    if (profilePicURL != null)
                    {
                        if (profilePicURL.Contains("?"))
                        {
                            profilePicURL = profilePicURL.Split('?')[0];
                        }
                        if (fbURL.Contains("pages"))
                        {
                            if (profilePicURL.Split('/').Length >= 5)
                                profilePicURL = "https://www.facebook.com/" + profilePicURL.Split('/')[5];
                        }
                        profilePicURL = profilePicURL.Replace("https://www", "https://graph") + "/picture";
                        dr.SetField<string>("profilepicurl", profilePicURL);
                        //Update data to database row from Excel dataset
                        DataRow drCurrent;

                        drCurrent = tblInstitutions.Rows.Find(dr.Field<string>("id"));
                        if (drCurrent != null)
                        {
                            drCurrent.BeginEdit();
                            drCurrent["facebookurl"] = dr.Field<string>("facebookurl");
                            drCurrent["profilepicurl"] = dr.Field<string>("profilepicurl");
                            drCurrent.EndEdit();
                        }
                    }
                }
                SqlCommandBuilder objCommandBuilder = new SqlCommandBuilder(daInstitutions);
                daInstitutions.Update(dsPubs, "Institution");
                objConn.Close();
                lbl_Message.Text = "Your file data has been successfully updated.";
                lbl_Message.Visible = true;
                File.Delete(filepath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public static class ExcelPackageExtensions
    {
        public static DataTable ToDataTable(this ExcelPackage package)
        {
            ExcelWorksheet workSheet = package.Workbook.Worksheets.First();
            DataTable table = new DataTable();
            foreach (var firstRowCell in workSheet.Cells[1, 1, 1, workSheet.Dimension.End.Column])
            {
                table.Columns.Add(firstRowCell.Text);
            }

            for (var rowNumber = 2; rowNumber <= workSheet.Dimension.End.Row; rowNumber++)
            {
                var row = workSheet.Cells[rowNumber, 1, rowNumber, workSheet.Dimension.End.Column];
                var newRow = table.NewRow();
                foreach (var cell in row)
                {
                    newRow[cell.Start.Column - 1] = cell.Text;
                }
                table.Rows.Add(newRow);
            }
            return table;
        }
    }
}