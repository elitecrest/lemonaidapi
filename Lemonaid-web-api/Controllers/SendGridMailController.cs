﻿using Lemonaid_web_api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Lemonaid_web_api.Controllers
{
    public class SendGridMailController : ApiController
    {
        readonly Utility.CustomResponse _result = new Utility.CustomResponse();
        
        [HttpGet]
        public Utility.CustomResponse SendGridSendMail()
        {
            //API KEY: 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
            try
            {
                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.APIKey = "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";
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add("mrkiran31@gmail.com");
                sendGridModel.fromMail = "bindubhargavi@elitecrest.com";
                sendGridModel.fromName = "bindu";
                sendGridModel.subject = "Sending with SendGrid in c#";
                sendGridModel.Message = "Test";
                int result = SendGridSMTPMail.SendEmail(sendGridModel);
                _result.Message = "";
                _result.Response = result.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return _result;
        }
    }
}
