﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateCollegeData.aspx.cs" Inherits="Lemonaid_web_api.UpdateCollegeData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <b>Please Select Excel File: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:FileUpload ID="fileuploadExcel" runat="server" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnUpload" runat="server" CssClass="DefaultButton" Text="Upload" OnClick="btnUpload_Click" />
    </div>
    <asp:Label ID="lbl_Message" runat="server" Visible="false"></asp:Label>
    </form>
</body>
</html>
