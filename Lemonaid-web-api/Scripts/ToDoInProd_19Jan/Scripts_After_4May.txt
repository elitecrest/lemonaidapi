 
------------------------------------------------------------------------------------------------
wrapper
Alter table [CoachVideos360Type] add Author varchar(1000)


 
 

ALTER PROCEDURE [dbo].[AddCoachVideos360Type] --[AddCoachVideos360Type] 'Test','hi/hi','hei/hisgf/dfsf',1,336,'mp4',45,'123.12','325.25353','hai'
    @Name varchar(50),
	@VideoURL varchar(1024),
	@ThumbnailURL varchar(1024), 
	@Status int,
	@CoachId int,
	@VideoFormat varchar(50),
	@duration int,
	@Latitude varchar(100),
	@Longitude varchar(100),
	@Description varchar(500),
	@Author varchar(1000)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON; 
	declare @VideoId int

	 
 Begin Try
 Begin Tran 
		 
		insert into CoachVideos360Type(Name,VideoURL,ThumbnailURL,Status,CoachId,CreatedDate,VideoFormat,duration,videoType,latitude,longitude,description,Author) 
		values (@Name,@VideoURL,@ThumbnailURL,@Status,@CoachId,getutcdate(),@VideoFormat,@duration,360,@Latitude,@Longitude,@Description,@Author)
		set @VideoId = @@Identity

		select @VideoId as Video360ID
 Commit Tran 
 End Try
Begin catch
 Rollback Tran
 --set @Id = -1
End catch

END 

GO


 
  

 
Create PROCEDURE [dbo].[UpdateCoach360VideoName] 
    @Name varchar(50),
	@Coach360Id int  
	 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	 
	 Update CoachVideos360Type set Name = @Name 
	 Where Id = @Coach360Id

	 select @Coach360Id as Id

END

  

GO






