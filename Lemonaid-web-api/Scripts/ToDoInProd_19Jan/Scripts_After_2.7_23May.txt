 
  

ALTER PROCEDURE [dbo].[UpdateAthleteShareVideoThumbnail]  
	@Name varchar(50), 
	@ThumbnailURL varchar(1024), 
	@VideoType int,
	@Caption varchar(500),
    @AthleteId int,
    @Latitude varchar(100),
	@Longitude varchar(100),
	@sharevideoid int,
	@RecordedDate datetime 
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	 
	declare @cnt int
	declare @Id int
	 
	 if(@sharevideoid = 0 and @videotype = 4)
	 Begin
		insert into AthleteShareVideos (Name,ThumbnailURL,AthleteId,
		VideoType,Caption,Latitude,Longitude,Createddate,format,RecordedDate) 
		values (@Name,@ThumbnailURL,@AthleteId,
		@VideoType,@Caption,@Latitude,@Longitude,getutcdate(),'jpg',@RecordedDate)
		set @Id = @@Identity 
	End
	Else
	Begin
	   Update AthleteShareVideos set Name = @Name ,ThumbnailURL=@ThumbnailURL,
	   VideoType = @VideoType,Caption=@Caption,Latitude=@Latitude,Longitude=@Longitude,
	   RecordedDate=@RecordedDate 
	   Where Id = @sharevideoid
	   set @Id = @sharevideoid
	End
	select @Id as Id
END
 


GO


CREATE PROCEDURE [dbo].[GetAthleteInstituteMatchedVirtualVideos]-- 'gopinath.iphonedev@gmail.com'
@AtheleteId int,
@CoachId int,
@InstituteId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 
 select * from AthleteInstituteMatchedVirtualVideos where AthleteId=@AtheleteId 
and CoachId=@CoachId and InstituteId=@InstituteId


  END
 

GO


  

ALTER PROCEDURE [dbo].[DeleteAtheleteShareVideoThumbnails]  
	@AthleteShareVideoId int
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	 
	   
	  Delete from AtheleteShareVideoThumbnails Where  AthleteShareVideoId = @AthleteShareVideoId
	 
		select @AthleteShareVideoId as Id
	 

END

 
  	
GO


 




 
ALTER PROCEDURE [dbo].[GetShareAtheleteVideosThumbnails]  --'elitecrest1.2@gmail.com'
@AthleteShareVideoId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  select * from AtheleteShareVideoThumbnails 
 Where AthleteShareVideoId = @AthleteShareVideoId

  END
  
 
GO


