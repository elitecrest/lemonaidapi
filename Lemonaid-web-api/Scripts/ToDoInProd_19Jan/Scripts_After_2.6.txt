  


Create PROCEDURE [dbo].[GetCoachVideos360TypeByCoach]  
(
   @CoachId int
)
 

AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
 SET NOCOUNT ON;

 Select * from CoachVideos360Type 
 Where CoachId = @CoachId
 Order By Id Desc
   
END
  

GO


  ALTER table Payment add TransactionStatus varchar(100) 


ALTER PROCEDURE [dbo].[AddCoachPayment]  
	@UserID int,
	@UserType int, -- 1 Athlete , 2 - Coach
	@PaymentToken [varchar](max),
	@DeviceType [varchar](50),
	@PaymentType [varchar](50), 
	@SubscriptionType varchar(20),
	@Emailaddress varchar(100),
	@SKUId  varchar(100),
	@TransactionStatus varchar(100)
 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
	declare @cnt int
	declare @ExpiryDate datetime
	declare @ExistingExpiryDate datetime

	 
	     set @ExpiryDate = '01/01/1900'
 
	  
	   -- select @UserID = Id from Coaches where emailaddress =  @Emailaddress  
	 
	--select @cnt = count(*) from Payment where UserID = @UserID and UserType=@UserType
 -- if(@cnt = 0)
	--Begin 
			insert into Payment (UserID, UserType, PaymentToken, DeviceType, PaymentType, CreatedDate,ExpiryDate,SubscriptionType,SKUId,
			TransactionStatus) 
			values (@UserID,@UserType,@PaymentToken, @DeviceType, @PaymentType, GETUTCDATE(),@ExpiryDate,@SubscriptionType,@SKUId
			,@TransactionStatus)
			set @Id = @@Identity   
	--End 
	 

	select isnull(@Id,0) as Id
END
  

GO

 

Create PROCEDURE [dbo].[GetCoachPayment] -- [GetCoachPayment] 337
	@UserID int 
     
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
declare @Link varchar(1000)
select @Link = link from applelink
	 select top 1  *,@Link as Link from Payment  
	 where UserID = @UserID and UserType=2
	 order by CreatedDate Desc
END
 

 




GO


  
 
 

Create PROCEDURE [dbo].[GetSchoolCoaches]    
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here  
 
 select CC.*,d.DeviceId as DeviceName,d.DeviceType,
 s.Name as StateName,c.name as ClubName
 From ClubCoach cc 
 inner join Device d on d.Id  = CC.DeviceId
 inner join Schools sc on sc.Id = CC.SchoolId
   left join States s on s.id = cc.stateid
   left join Clubs c on cc.ClubId = c.Id
 Where SchoolId <> 0

 END




GO


  
 

Create PROCEDURE [dbo].[GetClubCoaches]    
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here  
 
 select CC.*,d.DeviceId as DeviceName,d.DeviceType, s.Name as StateName,c.name as ClubName
 From ClubCoach cc 
 inner join Device d on d.Id  = CC.DeviceId
 inner join Clubs c on c.Id = CC.ClubId
 left join States s on s.id = cc.stateid 
 Where ClubId <> 0

 END



GO

 


 
 
 
Create PROCEDURE [dbo].[UpdateCoachPayment]   
	@PaymentToken [varchar](max), 
	@TransactionStatus varchar(100),
	@TransactionId int
 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
 
	 declare @UserId int

	    Update Payment set PaymentToken=@PaymentToken,  
		TransactionStatus = @TransactionStatus
	    where Id = @TransactionId  

		select @UserId = UserId from Payment Where Id = @TransactionId  
		  
		Update Coaches set Payment_paid = 1 where Id = @UserId

	select @TransactionId as Id
END


 

GO


 
 
 

CREATE PROCEDURE [dbo].[GetSchoolClubCoachesForCoach]   
 (
   @CoachId int
 )

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here  
 
 declare @InstituteId int
 select @InstituteId = Instituteid from Coaches where Id = @CoachId

 
 select CC.*,d.DeviceId as DeviceName,d.DeviceType,
 s.Name as StateName,c.name as ClubName 
 From ClubCoach cc 
 inner join ClubCoachInstitutematches ccim on ccim.ClubCoachId = cc.Id
 left join Device d on d.Id  = CC.DeviceId
 left join Schools sc on sc.Id = CC.SchoolId
 left join States s on s.id = cc.stateid
 left join Clubs c on cc.ClubId = c.Id 
 Where ccim.status = 1 and ccim.Instituteid = @InstituteId

 END
 
 

GO


  
 
CREATE PROCEDURE [dbo].[GetLatestMessagesForCoach]  
	@CoachId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;

select a.name,a.Profilepicurl,senderid,receiverid,c.Date,Message from chats c
inner join athelete a on a.id  = c.senderid  
where receiverid = @CoachId  and receivertype = 1 and sendertype = 0 
and c.Date >= DATEADD(day,-7, GETDATE()) 
order by  c.Date desc

 
		 
END
GO


 
 
CREATE PROCEDURE [dbo].[AddContactUs]  
    @Name varchar(500),
	@Email varchar(200),
	@CoachId int,
	@Message varchar(2000) 
	 
	 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
 
  Declare @Id int
 
	   Insert into ContactUs(Name,Email,[Message],CoachId,CreatedDate)
		values(@Name,@Email,@Message,@CoachId,getutcdate())

	   set @Id = @@Identity 

 		select @Id as Id

  
END

 


GO

------------------------------------------------------------------------------------------------------------------------

--Notifications

 

CREATE PROCEDURE [dbo].[GetUsersChatCount]    
 (
    @UserType varchar(100) 
 )
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here  

	Declare @ReceiverType int

	   if(@UserType = 'HS')
	  set @ReceiverType = 0
		if(@UserType = 'CC')
	  set @ReceiverType = 1
	 
if(@UserType = 'HS')
Begin
 select distinct a.Id,a.emailaddress,a.logintype,a.username,
 d.DeviceType,d.DeviceId from athelete a
 left join Chats acac on acac.Receiverid = a.Id and acac.ReceiverType = @ReceiverType
 left join ChatsLastVisited aclv on aclv.ReceiverId = a.Id
 left join Device d on d.id = a.DeviceId  
 where acac.Date > aclv.LastVisitedDate and a.atheletetype = 0 
End
Else
Begin
 select distinct c.Id,c.emailaddress,
 d.DeviceType,d.DeviceId from coaches c
 left join Chats acac on acac.Receiverid = c.Id and acac.ReceiverType = @ReceiverType
 left join ChatsLastVisited aclv on aclv.ReceiverId = c.Id
 left join Device d on d.id = c.DeviceId  
 where acac.Date > aclv.LastVisitedDate and c.headcoach != 0
End



 END



GO




CREATE PROCEDURE [dbo].[NoSwipesForAtheleteToInstitute]
 -- Add the parameters for the stored procedure here
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

    -- Insert statements for procedure here

select a.id,emailaddress,username,logintype,d.DeviceType,d.DeviceId
from athelete a 
left join Device d on d.id  =  a.Deviceid
where a.id not in (select Atheleteid from atheleteinstitutematches) 
and a.atheletetype=0
END

GO


 
CREATE PROCEDURE [dbo].[NoSwipesForInstituteToAthelete]
 -- Add the parameters for the stored procedure here
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

    -- Insert statements for procedure here

select c.id,emailaddress,d.devicetype,d.deviceid from coaches c
left join device d on d.id = c.deviceid
where c.id not in (select coachid from instituteatheletematches where collatheletecoachtype=1) and headcoach in (1,2)

END

GO


CREATE PROCEDURE [dbo].[NoSwipesForFFFToAthelete]
 -- Add the parameters for the stored procedure here
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

    -- Insert statements for procedure here



select ff.name,emailaddress,logintype ,d.devicetype,d.deviceid from familyfriends ff 
left join device d on d.id=ff.deviceid
where ff.id not in (select familyfriendsid from FamilyFriendsAthleteMatches)


END

GO



CREATE PROCEDURE [dbo].[NoSwipesForFFFToInstitute]
 -- Add the parameters for the stored procedure here
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

    -- Insert statements for procedure here



select ff.name,emailaddress,logintype ,d.devicetype,d.deviceid from familyfriends ff 
left join device d on d.id=ff.deviceid
where ff.id not in (select familyfriendsid from FamilyFriendsinstituteMatches)



END

GO




CREATE PROCEDURE [dbo].[NoSwipesForClubCoachToInstitute]
 -- Add the parameters for the stored procedure here
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

    -- Insert statements for procedure here

	
select cc.id,cc.name,cc.emailaddress,cc.logintype,d.devicetype,d.deviceid from clubcoach cc
left join device d on d.id=cc.DeviceId
where cc.id not in (select Clubcoachid from clubcoachinstitutematches)




END

GO


CREATE PROCEDURE [dbo].[NoRecomendsForCLCToInstitute]
 -- Add the parameters for the stored procedure here
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

    -- Insert statements for procedure here

select * from clubcoach cc
where id not in (select distinct clubcoachid from RecommendedInstitutes)


END

GO



CREATE PROCEDURE [dbo].[NoRecomendsForFFFToInstitute]
 -- Add the parameters for the stored procedure here
 
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

    -- Insert statements for procedure here


select * from familyfriends ff
where id not in 
(select distinct familyfriendsid from RecommendedInstitutesFFF)


END

GO



CREATE PROCEDURE [dbo].[UpdatepushNotifRunDate]  
(
   @PushNotificationId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.

	
	Update PushNotificationsMaster set LastRunDate = getutcdate()
	Where id = @PushNotificationId 
 
	 
END

 
  


GO



 
CREATE PROCEDURE [dbo].[GetPushNotificationMessages]   
@PushNotificationId int	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	 
if(@PushNotificationId  = 0)
Begin
 select * from PushNotificationsMaster
End
Else
Begin
   select * from PushNotificationsMaster where id = @PushNotificationId
End
	 
END
GO


 
 
CREATE PROCEDURE [dbo].[GetAcademicProfilesNotCompleted]    
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
 SET NOCOUNT ON;
  
 select a.id,emailaddress,username,logintype,
d.DeviceType,d.DeviceId,a.CreatedDate from athelete a
 left join Device d on d.id  =  a.Deviceid
 left join Wrestlingmetrics wm on wm.atheleteid = a.Id
where  Atheletetype = 0  and 
 (
   (isnull(GPA,'') = '')  or ((isnull(SAT,'') = '') or (isnull(ACT,'') = ''))    
 )
END  

GO

  	

 
CREATE PROCEDURE [dbo].[UpdateAthletePushNotifRunDate]  
(
   @PushNotificationId int,
   @AthleteId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.

	declare @cnt int
	declare @Id int

	Begin Try
	Begin Tran
		select @cnt = count(*) from PushNotificationAthleteLastVisited Where AthleteId = @AthleteId
		 and NotificationId = @PushNotificationId


	   Insert into PushNotificationAthleteLastVisited(AthleteId,NotificationId,LastRunDate,CreatedDate)
	   Values(@AthleteId,@PushNotificationId,Getutcdate(),Getutcdate())
		set @Id = @@Identity

		if(@cnt = 0)
		Begin
		  Update PushNotificationAthleteLastVisited set isFirst = 1 where Id = @Id
		End
	Commit Tran
	End Try
	Begin Catch
	  set @Id = -1
	  Rollback Tran 
	End Catch

	select @Id as ID
END
  

GO




 
CREATE PROCEDURE [dbo].[GetAthletePushNotifLastRunDate] 
(
   @PushNotificationId int,
   @AthleteId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.

	 select top 1 * from PushNotificationAthleteLastVisited 
	 Where AthleteId = @AthleteId and NotificationId = @PushNotificationId 
	 Order by LastRunDate desc
END

 
  



GO
  

ALTER PROCEDURE [dbo].[GetCoachProfileByEmail]   --[GetCoachProfileByEmail] 'binduv@elitecrest.com' 
	@Emailaddress varchar(100) 
	 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;  
	    
		   select i.*,1 as TotalCount ,c.Emailaddress as CoachEmail , c.FacebookId as CoachFacebook ,
		   c.id as CoachId,c.Active,c.HeadCoach,c.PreferredStrokes,c.AcademicRate,c.Payment_Paid,
		   C.FirstName,C.LastName
		   from Institution i
		   inner join Coaches c on c.instituteid = i.id
		   and c.Emailaddress = @Emailaddress  
	 
    
	   
	 

END

   

GO


 

 
ALTER PROCEDURE [dbo].[GetAtheleteProfile_v4] --[GetAtheleteProfile_v4] 'elitecrest100',2
@emailaddress varchar(100),
@type int
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;
 

  declare @Id int
  declare @isProfileCompleted Bit
  
  If(@type = 1 or @type = 4)
  Begin
      select @Id = Id  from athelete where emailaddress = @emailaddress and logintype = @type
  End
  Else
  Begin
     select @Id = Id  from athelete where Username = @emailaddress  and logintype = @type
  End

  Exec @isProfileCompleted = fnAthleteProfileCompleted @Id 

  Exec DeleteAthleteMediaAfterOneMonth  @Id
   
      select  a.*,ab.*,I.Name as UniversityName,I.Address as UnivAddress, I.ClassificationName,I.EmailAddress as UnivEmailAddress,
   I.ProfilePicURL as UnivProfilePic, I.PhoneNumber as UnviPhoneNumber,I.Size as UnivSize,
    s.name as StateName,@isProfileCompleted as  isProfileCompleted,d.DeviceId as DeviceName,d.DeviceType,
    s1.name as BasicStateName,c.cityname
    from athelete a
   left join AtheleteBadges ab on ab.AtheleteId = a.Id 
   left join Institution I on I.Id = a.InstituteId
   left join device d on d.id  = a.deviceId
   left join states s on a.stateid = s.id
   left join states s1 on a.basicstateid = s1.id
   left join Cities c on c.id = a.CityID
   where a.Id = @Id 
  
  END
 
  

GO

-----------------------------------------------------------------------------------------------------------------

 

Alter table Notifications add isSeen int
GO

Update Notifications set isseen = 1  
 GO
 
CREATE PROCEDURE [dbo].[UpdateIsSeenNotifications]  
	@NotificationId int 
  
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
  
    Update Notifications set isSeen = 1 Where Id = @NotificationId
   
	 
END
 

GO
 

ALTER PROCEDURE [dbo].[AddNotifications]  
	@UserID int,
	@NotificationId int, 
	@Status int,
	@SenderId int,
	@ShortMessage varchar(500),
	@LongMessage varchar(max),
	@BadgeTypeId int,
	@BadgeCount int,
	@senderType int
	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	 declare @Id int

		insert into Notifications (NotificationId, UserId,SenderId,Status, CreatedDate,UpdatedDate,ShortMessage,LongMessage,BadgeTypeId,BadgeCount,senderType,isSeen) 
		values (@NotificationId,@UserId,@SenderId, @Status, GETUTCDATE(),GETUTCDATE(),@ShortMessage,@LongMessage,@BadgeTypeId,@BadgeCount,@senderType,0)
		set @Id = @@Identity
		 
	    select @Id as Id
END
 


GO

CREATE TABLE [dbo].[PlanTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[SubscriptionType] [varchar](100) NULL,
	[Price] [varchar](50) NULL
)

GO



INSERT INTO [dbo].[PlanTypes]([Name],[CreatedDate],SubscriptionType,Price)VALUES('Mobile App',Getutcdate(),'FREE',0)
GO
INSERT INTO [dbo].[PlanTypes]([Name],[CreatedDate],SubscriptionType,Price)VALUES('Team 360 Package',Getutcdate(),'Year','$369')
GO
INSERT INTO [dbo].[PlanTypes]([Name],[CreatedDate],SubscriptionType,Price)VALUES('Team Web App Application',Getutcdate(),'Year','$149')
GO
INSERT INTO [dbo].[PlanTypes]([Name],[CreatedDate],SubscriptionType,Price)VALUES('Athletic Department Package',Getutcdate(),'Year','$295/team')
GO


 Alter table [dbo].[Payment] add PlanId int
 GO


ALTER PROCEDURE [dbo].[AddCoachPayment]  
	@UserID int,
	@UserType int, -- 1 Athlete , 2 - Coach
	@PaymentToken [varchar](max),
	@DeviceType [varchar](50),
	@PaymentType [varchar](50), 
	@SubscriptionType varchar(20),
	@Emailaddress varchar(100),
	@SKUId  varchar(100),
	@TransactionStatus varchar(100),
	@PlanId int
 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Id int
	declare @cnt int
	declare @ExpiryDate datetime
	declare @ExistingExpiryDate datetime

	 
	     set @ExpiryDate = '01/01/1900'
 
	  
	   -- select @UserID = Id from Coaches where emailaddress =  @Emailaddress  
	 
	--select @cnt = count(*) from Payment where UserID = @UserID and UserType=@UserType
 -- if(@cnt = 0)
	--Begin 
			insert into Payment (UserID, UserType, PaymentToken, DeviceType, PaymentType, CreatedDate,ExpiryDate,SubscriptionType,SKUId,
			TransactionStatus,PlanId) 
			values (@UserID,@UserType,@PaymentToken, @DeviceType, @PaymentType, GETUTCDATE(),@ExpiryDate,@SubscriptionType,@SKUId
			,@TransactionStatus,@PlanId)
			set @Id = @@Identity   
	--End 
	 

	select isnull(@Id,0) as Id
END
  

GO



 
ALTER PROCEDURE [dbo].[GetCoachProfileByEmail]   --[GetCoachProfileByEmail] 'binduv@elitecrest.com' 
	@Emailaddress varchar(100) 
	 

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
	SET NOCOUNT ON;  
	   
	   declare @PlanId int
	   declare @CoachId int

	   select @CoachId = Id from Coaches where emailaddress = @Emailaddress
	    set @PlanId = (select top 1 PlanId  from Payment where UserID = @CoachId and UserType=2 order by CreatedDate Desc)

		   select i.*,1 as TotalCount ,c.Emailaddress as CoachEmail , c.FacebookId as CoachFacebook ,
		   c.id as CoachId,c.Active,c.HeadCoach,c.PreferredStrokes,c.AcademicRate,c.Payment_Paid,
		   C.FirstName,C.LastName,@PlanId as PlanId,PT.Name as PlanName,PT.SubscriptionType,PT.Price
		   from Institution i
		   inner join Coaches c on c.instituteid = i.id and c.Emailaddress = @Emailaddress  
		   left join PlanTypes PT on PT.Id = @PlanId  
	 

END

 
