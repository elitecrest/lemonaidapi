﻿using System;
using System.Web.Http;
using Lemonaid_web_api.Models;
using System.Collections.Generic;
using System.Web.Http;
using System.IO;
using System.Web;

namespace Lemonaid_web_api.Controllers
{
    public class Athlete2V5Controller : ApiController
    {
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse AddAthleteVideoContent(MediaContent mediaContent)
        {

            try
            {
                int id = DAL2V5.AddAthleteVideoContent(mediaContent);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Athlete_Video_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/AddAthleteVideoContent", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse UpdateAthleteThumbnailContent(MediaContent mediaContent)
        {

            try
            {
                UploadImage uploadImage = new UploadImage();
                mediaContent.ThumbnailURL =  uploadImage.GetImageURL(mediaContent.ThumbnailBase64String, ".jpg", "");
                int id = DAL2V5.UpdateAthleteThumbnailContent(mediaContent);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.ShareVideo_Content_Updated;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/UpdateAthleteThumbnailContent", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        } 

        [HttpGet]
        public Utility.CustomResponse GetAthleteMediaContent(int athleteId)
        {

            try
            { 
                List<MediaContent> lstAthleteMedia = DAL2V5.GetAthleteMediaContentById(athleteId);
                if (lstAthleteMedia.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = lstAthleteMedia;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/GetAthleteMediaContent", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddAthleteShareVideos(MediaContent mediaContent)
        {

            try
            {
                int id = DAL2V5.AddAthleteShareVideos(mediaContent);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Athlete_ShareVideo_Added;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/AddAthleteShareVideos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse UpdateAthleteShareVideoThumbnail(MediaContent mediaContent)
        {

            try
            {
                UploadImage uploadImage = new UploadImage();
                mediaContent.ThumbnailURL = uploadImage.GetImageURL(mediaContent.ThumbnailBase64String, ".jpg", "");
                int id = DAL2V5.UpdateAthleteShareVideoThumbnail(mediaContent);
                _result.Response = id;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.ShareVideo_Content_Updated;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/UpdateAthleteShareVideoThumbnail", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result; 
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetAthleteShareVideos(int athleteId)
        {

            try
            {
                List<MediaContent> lstAthleteMedia = DAL2V5.GetAthleteShareVideosById(athleteId);
                if (lstAthleteMedia.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = lstAthleteMedia;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/GetAthleteShareVideos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        
        [HttpGet]
        public Utility.CustomResponse ShareToBlog(int shareVideoId, int AthleteId)
        {

            try
            {
                int id = DAL2V5.AddAthleteSharetoMediaContent(shareVideoId, AthleteId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = id;
                _result.Message = CustomConstants.Athlete_ShareVideo_Added;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/ShareToBlog", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse ShareToAthleteVideos(int shareVideoId, int AthleteId)
        {

            try
            {
                VideoDTO v = DAL2V5.AddAthleteSharetoAthleteVideos(shareVideoId, AthleteId);
                GetAthleteShareVideoThumbnails(v);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = v.Id;
                _result.Message = CustomConstants.Athlete_ShareVideo_Added;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/ShareToAthleteVideos", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }
         

        [HttpGet]
        public Utility.CustomResponse DeleteAthleteShareVideo(int athleteShareVideoId)
        {
            try
            {
                var id = DAL2V5.DeleteAthleteShareVideo(athleteShareVideoId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = id;
                _result.Message = CustomConstants.Deleted_Athlete_ShareVideo;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/DeleteAthleteShareVideo", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetAthleteShareVideosForClubCoach(int athleteId)
        {

            try
            {
                List<MediaContent> lstAthleteMedia = DAL2V5.GetAthleteShareVideosForClubCoach(athleteId);
                if (lstAthleteMedia.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = lstAthleteMedia;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/GetAthleteShareVideosForClubCoach", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse DeleteAthleteMediaContent(int athleteMediaContentId)
        {
            try
            {
                var id = DAL2V5.DeleteAthleteMediaContent(athleteMediaContentId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = id;
                _result.Message = CustomConstants.Deleted_Athlete_MediaContent;

                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/DeleteAthleteMediaContent", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetUsageTracking(int userId, int logintype)
        {

            try
            {

                var usageTracking = DAL2V5.GetUsageTracking(userId, logintype); 

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = usageTracking;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2sV5/GetUsageTracking", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        } 

        [HttpGet]
        public Utility.CustomResponse GetVideosFromUniversityLatLongValues(int universityId)
        {

            try
            {
                var videos = DAL2V5.GetVideosFromUniversityLatLongValues(universityId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = videos;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog("Athlete2V5/GetVideosFromUniversityLatLongValues", "", ex.Message, "Exception", 0);
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }
        private void GetAthleteShareVideoThumbnails(VideoDTO videoResp)
        {
            UploadImage uplImage = new UploadImage();
            //Delete all Video thumbnails 
            DAL2V5.DeleteAtheleteShareVideoThumbnails(videoResp.AtheleteId);

            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
            int totDuration = videoResp.Duration;
            float startPoint = 0;
            float dur = (float)totDuration / 6;
            for (int i = 0; i < 5; i++)
            {
                var frame = startPoint + (float)Math.Round(dur, 2);
                string thumbnailJpeGpath = HttpContext.Current.Server.MapPath("~/sharevideoThumb" + i + ".jpg");
                ffMpeg.GetVideoThumbnail(videoResp.VideoURL, thumbnailJpeGpath, frame);
                byte[] bytes = File.ReadAllBytes(thumbnailJpeGpath);
                string base64String = Convert.ToBase64String(bytes);
                string thumbnailUrl = uplImage.GetImageURL(base64String, ".jpg", "");
                AthleteThumbnailDto athleteThumbnailDto = new AthleteThumbnailDto(); 
                athleteThumbnailDto.ThumbnailURL = thumbnailUrl;
                athleteThumbnailDto.VideoId = videoResp.Id;
                athleteThumbnailDto.AthleteId = videoResp.AtheleteId;
                //Add Image to the DB 
                DAL2V5.AddAtheleteShareVideoThumbnails(athleteThumbnailDto);
                startPoint = startPoint + (float)Math.Round(dur, 2);
            }
        }

    }
}
