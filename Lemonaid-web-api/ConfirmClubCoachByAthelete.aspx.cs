﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lemonaid_web_api
{
    public partial class ConfirmClubCoachByAthelete : System.Web.UI.Page
    {
        Utility.CustomResponse Res = new Utility.CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string baseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            string AtheleteId = Request.QueryString["athleteid"].ToString();
            string ClubCoachId = Request.QueryString["ClubCoachId"].ToString();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = client.GetAsync("/ClubCoach/UpdateClubCoachToAthelete?clubCoachId=" + ClubCoachId + "&atheleteId=" + AtheleteId).Result;
            if (response.IsSuccessStatusCode)
            {
                Res = response.Content.ReadAsAsync<Utility.CustomResponse>().Result;
                if (Res.Status == Utility.CustomResponseStatus.Successful)
                {

                    lblmsg.Visible = true;
                    lblmsg.Text = Res.Message;
                    btnSubmit.Visible = false;
                    Response.Redirect("http://www.lemonaidrecruiting.com/", true);
                }
                else
                {
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                    lblmsg.Visible = true;
                    lblmsg.Text = Res.Message;
                }

            }
        }
    }
}