﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LemonaidCoach
{
    public partial class Analytics : System.Web.UI.Page
    {
        public static SqlConnection ConnectTodb()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["LemonaidDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["userId"] == null)
            {
                Response.Redirect("LemonaidAdmin.aspx");
            }
            BindCounts();
        }
        private void BindCounts()
        {
            SqlConnection myConn = ConnectTodb();
            string email = Session["loggedInEmail"].ToString();
            string userId = Session["userId"].ToString();
            using (SqlCommand cmd = new SqlCommand("select count from usagetracking where DeviceId='" + email + "' and createdate=CONVERT(date, getdate())"))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = myConn;
                    sda.SelectCommand = cmd;
                    if (cmd.ExecuteScalar() != null)
                        profileViews.Value = cmd.ExecuteScalar().ToString();
                    else
                        profileViews.Value = "0";
                    using (SqlCommand cmd1 = new SqlCommand("select max(count) from usagetracking where DeviceId='" + email + "'"))
                    {
                        using (SqlDataAdapter sda1 = new SqlDataAdapter())
                        {
                            cmd1.Connection = myConn;
                            sda1.SelectCommand = cmd1;
                            if (cmd1.ExecuteScalar() != null)
                                maxProfileViews.Value = cmd1.ExecuteScalar().ToString();
                            else
                                maxProfileViews.Value = "0";
                        }
                    }
                }
            }

            
            
            
            
            //Matches
            StringBuilder sb = new StringBuilder();
            sb.Append(@"select top 1  (count(ai.createddate)) as cnt from AtheleteInstituteMatches ai 
 inner join InstituteAtheleteMatches ia on 
 ai.AtheleteId = ia.AtheleteId and ai.InstituteId = ia.InstituteId
 where isnull(ai.CollAtheleteCoachType,1) = 1
  and isnull(ia.CollAtheleteCoachType,1) = 1
  group by ai.createddate,ia.createddate,ai.status,ia.status
  having ai.status = 1 and ia.status = 1
 and ai.createddate = CONVERT(date, getdate())
 and ia.createddate = CONVERT(date, getdate())
 order by cnt desc");
            using (SqlCommand cmd = new SqlCommand(sb.ToString()))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = myConn;
                    sda.SelectCommand = cmd;
                    if (cmd.ExecuteScalar() != null)
                        matches.Value = cmd.ExecuteScalar().ToString();
                    else
                        matches.Value = "0";
                    sb.Clear();
                    sb.Append(@"select top 1  (count(ai.createddate)) as cnt from AtheleteInstituteMatches ai 
 inner join InstituteAtheleteMatches ia on 
 ai.AtheleteId = ia.AtheleteId and ai.InstituteId = ia.InstituteId
 where isnull(ai.CollAtheleteCoachType,1) = 1
  and isnull(ia.CollAtheleteCoachType,1) = 1
  group by ai.createddate,ia.createddate,ai.status,ia.status
  having ai.status = 1 and ia.status = 1
 order by cnt desc");
                    using (SqlCommand cmd1 = new SqlCommand(sb.ToString()))
                    {
                        using (SqlDataAdapter sda1 = new SqlDataAdapter())
                        {
                            cmd1.Connection = myConn;
                            sda1.SelectCommand = cmd1;
                            if (cmd1.ExecuteScalar() != null)
                                maxMatches.Value = cmd1.ExecuteScalar().ToString();
                            else
                                maxMatches.Value = "0";
                        }
                    }
                }
            }


            //Right swipes Given
            sb.Clear();
            sb.Append(@"select top 1  (count(createddate)) as cnt From instituteatheletematches where CoachId = " + userId + @"
 group by cast(createddate as varchar),status
 having   status = 1 
 and cast(createddate as varchar) = CONVERT(date, getdate())
 order by cnt desc");
            using (SqlCommand cmd = new SqlCommand(sb.ToString()))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = myConn;
                    sda.SelectCommand = cmd;
                    if (cmd.ExecuteScalar() != null)
                        rightSwipesGiven.Value = cmd.ExecuteScalar().ToString();
                    else
                        rightSwipesGiven.Value = "0";
                    sb.Clear();
                    sb.Append(@"select top 1  (count(createddate)) as cnt From instituteatheletematches where CoachId = " + userId + @"
 group by cast(createddate as varchar),status
 having status = 1 order by cnt desc");
                    using (SqlCommand cmd1 = new SqlCommand(sb.ToString()))
                    {
                        using (SqlDataAdapter sda1 = new SqlDataAdapter())
                        {
                            cmd1.Connection = myConn;
                            sda1.SelectCommand = cmd1;
                            if (cmd1.ExecuteScalar() != null)
                                maxRightSwipesGiven.Value = cmd1.ExecuteScalar().ToString();
                            else
                                maxRightSwipesGiven.Value = "0";
                        }
                    }
                }
            }


            //Right Swipes Received
            sb.Clear();
            sb.Append(@"select top 1  (count(createddate)) as cnt From atheleteinstitutematches where CoachId = " + userId + @"
 group by cast(createddate as varchar),status
 having   status = 1 
 and cast(createddate as varchar) = CONVERT(date, getdate())
 order by cnt desc");
            using (SqlCommand cmd = new SqlCommand(sb.ToString()))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = myConn;
                    sda.SelectCommand = cmd;
                    if (cmd.ExecuteScalar() != null)
                        rightSwipesReceived.Value = cmd.ExecuteScalar().ToString();
                    else
                        rightSwipesReceived.Value = "0";
                    sb.Clear();
                    sb.Append(@"select top 1  (count(createddate)) as cnt From atheleteinstitutematches where CoachId = " + userId + @"
 group by cast(createddate as varchar),status
 having status = 1 order by cnt desc");
                    using (SqlCommand cmd1 = new SqlCommand(sb.ToString()))
                    {
                        using (SqlDataAdapter sda1 = new SqlDataAdapter())
                        {
                            cmd1.Connection = myConn;
                            sda1.SelectCommand = cmd1;
                            if (cmd1.ExecuteScalar() != null)
                                maxRightSwipesReceived.Value = cmd1.ExecuteScalar().ToString();
                            else
                                maxRightSwipesReceived.Value = "0";
                        }
                    }
                }
            }




            ScriptManager.RegisterStartupScript(Page, typeof(System.Web.UI.Page), "Script", "BindDataToCircles();", true);
        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Session.Clear();
            Response.Redirect("LemonaidAdmin.aspx");
        }
    }
}