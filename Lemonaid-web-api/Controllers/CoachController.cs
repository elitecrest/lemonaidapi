﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.Web.Http;
using Lemonaid_web_api.Models;
using System.Configuration; 
using System.IO; 
using System.Net.Mail;
using System.Web;
 

namespace Lemonaid_web_api.Controllers
{
    public class CoachController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse _result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse GetCoaches(string emailaddress,int offset , int max)  
        {
            
            try
            { 
                var coaches = UsersDal.GetCoaches(emailaddress, offset, max); 
                if (coaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetCoachesFavourites(string emailaddress, int offset, int max)
        {
          
            try
            {
                AthleteController athCtrl = new AthleteController();
                var atheletes = UsersDal.GetCoachesFavourites(emailaddress, offset, max);
                int totalLikesCount = atheletes.Count(p => p.Liked);

                for (var i = 0; i < atheletes.Count; i++)
                {
                    atheletes[i].TotalLikedCount = totalLikesCount;
                }
                atheletes = athCtrl.AddBesttimesToList(atheletes);
                atheletes = athCtrl.AddVideosToList(atheletes);
                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse SendMailToCoaches()
        {
            
            try
            {
                 UsersDal.SendMailToCoaches();
                

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse SendPushNotificationsToCoaches()
        {
            
            try
            {
                UsersDal.SendPushNotificationsToCoaches();


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddCoach(CoachDTO coachDto)
        {
            
            try
            {

                var coachid = UsersDal.AddCoach(coachDto);
                if (coachid > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coachid;
                    _result.Message = CustomConstants.Coach_Added;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Already_Exists;
                }



                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse AddCoachMatch(AtheleteMatchDTO atheleteMatchDto)
        {
             
            try
            {

                var matchId = UsersDal.AddCoachMatch(atheleteMatchDto);
                matchId = (atheleteMatchDto.Status == false) ? 0 : matchId;
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId; 
                _result.Message = CustomConstants.Status_Added; 
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse DeleteCoachMatch(AtheleteMatchDTO atheleteMatchDto)
        {
            
            try
            {

                var matchId = UsersDal.DeleteCoachMatch(atheleteMatchDto); 
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = matchId;
                _result.Message = CustomConstants.Match_Removed;




                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse UserValidation(string emailaddress, int id)
        {
           
            try
            {

                var cnt = UsersDal.UserValidation(emailaddress, id);
                var val = (cnt > 0);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = val;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse UpdateCoachEmailToUniversity(string emailaddress, int coachid,int teamtype)
        {
          
            try
            {
                var univEmail = UsersDal.GetCoachDefaultEmail(coachid);
                bool isvalid = CheckForValidEmail(univEmail, emailaddress);
                if (isvalid)
                {
                    var coach = UsersDal.UpdateCoachEmailToUniversity(emailaddress, coachid, teamtype);
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coach;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Response = emailaddress;
                    _result.Message = CustomConstants.Invalid_HostName;

                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        public bool CheckForValidEmail(string univemail, string coachemailaddress)
        {
            MailAddress uniaddress = new MailAddress(univemail);
            string unihost = uniaddress.Host;
            var mainunihost = unihost.Substring(unihost.IndexOf('.') + 1);
            if (mainunihost.Contains("."))
            {
                unihost = mainunihost;
            }

            MailAddress coachaddress = new MailAddress(coachemailaddress);
            string coachhost = coachaddress.Host;
            var maincoachhost = coachhost.Substring(coachhost.IndexOf('.') + 1);
            if (maincoachhost.Contains("."))
            {
                coachhost = maincoachhost;
            }

            var isvalid = (unihost == coachhost);

            return isvalid;
        }


        [HttpGet]
        public Utility.CustomResponse UpdateCoachPaymentStatus(string emailaddress, string paymentToken, string amount)
        { 
            try
            {

                var isPaid = UsersDal.UpdateCoachPaymentStatus(emailaddress, paymentToken, amount);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = isPaid;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse UpdateCoachRatings(CoachRatingsDTO coachRatingsDto)
        { 
            try
            { 
                int id = UsersDal.UpdateCoachRatings(coachRatingsDto);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = id;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


 

        [HttpGet]
        public Utility.CustomResponse GetBadges(string emailAddress)
        {
           
            try
            {
                var badges = UsersDal.GetCoachesBadges(emailAddress); 
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse CheckCoachPaymentStatus(string emailAddress)
        {
           
            try
            {
                var paymentPaid = UsersDal.CheckCoachPaymentStatus(emailAddress);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = paymentPaid;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse CheckCoachActiveStatus(int coachId)
        {
            
            try
            {
                var status = UsersDal.CheckCoachActiveStatus(coachId);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = status;
                if (status)
                {

                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Message = CustomConstants.Coach_Active_Status;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }



        [HttpGet]
        public Utility.CustomResponse DeleteCoachAtheletes(string emailAddress)
        {
             
            try
            {
                var badges = UsersDal.DeleteCoachAtheletes(emailAddress);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = badges;
                _result.Message = CustomConstants.Details_Get_Successfully;


                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

       

        [HttpGet]
        public Utility.CustomResponse GetAllCoachesByLocation(string emailaddress, double latitude, double longitude, int offset, int max, int ncaa, int conference, int admission, int locationType, string description) // Athlete email address
        {
            AthleteController athleteContr = new AthleteController();
           
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }

               var coaches = UsersDal.GetAllCoachesPaging(emailaddress, offset, max, ncaa, conference, admission, description);

               var newList = new List<CoachDTO>();

                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = coaches;

                }
                else
                    if (locationType == 2)
                    {
                        foreach (CoachDTO t in coaches)
                        {
                            double athleteLatitude = (t.Latitude == "") ? 0.0 : Convert.ToDouble(t.Latitude);
                            double athleteLongitude = (t.Longitude == "") ? 0.0 : Convert.ToDouble(t.Longitude);
                            double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(t);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            for (int i = 0; i < coaches.Count; i++)
                            {
                                double athleteLatitude = (coaches[i].Latitude == "") ? 0.0 : Convert.ToDouble(coaches[i].Latitude);
                                double athleteLongitude = (coaches[i].Longitude == "") ? 0.0 : Convert.ToDouble(coaches[i].Longitude);
                                double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                                if (distance <= 5)
                                {
                                    newList.Add(coaches[i]);
                                }
                            }
                        }




                if (newList.Count > 0)
                {
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newList = GetAdCoaches(ads, newList);
                    //newList = RemoveDuplicates(newList);

                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = newList;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }



        public List<CollegeAtheleteCoachesDTO> AddCoachestoList(List<CoachDTO> newList, List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches)
        {

            for (int i = 0; i < newList.Count; i++)
            {
                CollegeAtheleteCoachesDTO coaches = new CollegeAtheleteCoachesDTO();
                coaches.Id = newList[i].Id;
                coaches.Name = newList[i].Name;
                coaches.AreasInterested = newList[i].AreasInterested;
                coaches.Address = newList[i].Address;
                coaches.Emailaddress = newList[i].Emailaddress;
                coaches.Description = newList[i].Description;
                coaches.YearOfEstablish = newList[i].YearOfEstablish;
                coaches.NCAA = newList[i].NCAA;
                coaches.Conference = newList[i].Conference;
                coaches.AdmissionStandard = newList[i].AdmissionStandard;
                coaches.AverageRating = newList[i].AverageRating;
                coaches.FacebookId = newList[i].FacebookId;
                coaches.ClassificationName = newList[i].ClassificationName;
                coaches.State = newList[i].State;
                coaches.City = newList[i].City;
                coaches.Zip = newList[i].Zip;
                coaches.WebsiteURL = newList[i].WebsiteURL;
                coaches.FaidURL = newList[i].FaidURL;
                coaches.ApplURL = newList[i].ApplURL;
                coaches.Latitude = newList[i].Latitude;
                coaches.Longitude = newList[i].Longitude;
                coaches.EnrollmentNo = newList[i].EnrollmentNo;
                coaches.ApplicationNo = newList[i].ApplicationNo;
                coaches.AdmissionNo = newList[i].AdmissionNo;
                coaches.ProfilePicURL = newList[i].ProfilePicURL;
                coaches.CoverPicURL = newList[i].CoverPicURL;
                coaches.TotalCount = newList[i].TotalCount;
                coaches.Liked = newList[i].Liked;
                coaches.NoOfAthletesInApp = newList[i].NoOfAthletesInApp;
                coaches.AcceptanceRate = newList[i].AcceptanceRate;
                coaches.NCAARate = newList[i].NCAARate;
                coaches.ConferenceRate = newList[i].ConferenceRate;
                coaches.AdmissionRate = newList[i].AdmissionRate;
                coaches.SocialRate = newList[i].SocialRate;
                coaches.AtheleteRate = newList[i].AtheleteRate;
                coaches.Cost = newList[i].Cost;
                coaches.MensTeam = newList[i].MensTeam;
                coaches.WomenTeam = newList[i].WomenTeam;
                coaches.Size = newList[i].Size;
                coaches.Phonenumber = newList[i].Phonenumber;
                coaches.AtheleteCoachType = 1;
                coaches.AdType = newList[i].AdType;
                coaches.FavDate = newList[i].FavDate;
                coaches.InvitationDate = newList[i].InvitationDate;
                coaches.Accept = newList[i].Accept;
                coaches.CoachId = newList[i].CoachId;
                coaches.Payment_Paid = newList[i].Payment_Paid;
                coaches.Recommended = newList[i].Recommended;
                coaches.RightSwiped = newList[i].RightSwiped;
                coaches.ChatCount = newList[i].ChatCount;
                coaches.RecommendedByFFF = newList[i].RecommendedByFFF;
                coaches.AthleticConference = newList[i].AthleticConference;
                coaches.EnrollmentFee = newList[i].EnrollmentFee;
                coaches.Type = newList[i].Type;
                coaches.TuitionFee = newList[i].TuitionFee;
                coaches.Scholarship = newList[i].Scholarship;
                coaches.CoachVideos = newList[i].CoachVideos;
                coaches.TotalChatCount = newList[i].TotalChatCount;
                coaches.ScholarshipMen = newList[i].ScholarshipMen;
                coaches.ScholarshipWomen = newList[i].ScholarshipWomen;
                coaches.TuitionInState = newList[i].TuitionInState;
                coaches.TuitionOutState = newList[i].TuitionOutState;
                coaches.DeviceId = newList[i].DeviceId;
                coaches.DeviceType = newList[i].DeviceType;
                coaches.VirtualVideoURL = newList[i].VirtualVideoURL;
                coaches.MatchesVirtualVideoURL = newList[i].MatchesVirtualVideoURL;
                coaches.VirtualThumbnailURL = newList[i].VirtualThumbnailURL;
                coaches.MatchesVirtualThumbnailURL = newList[i].MatchesVirtualThumbnailURL; 
                collegeAtheleteCoaches.Add(coaches);
            }
            return collegeAtheleteCoaches;
        }

        public List<CollegeAtheleteCoachesDTO> AddAtheletestoList(List<AthleteDTO> collegeAtheletes, List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches)
        {

            for (int i = 0; i < collegeAtheletes.Count; i++)
            {
                CollegeAtheleteCoachesDTO atheletes = new CollegeAtheleteCoachesDTO();
                atheletes.AtheleteId = collegeAtheletes[i].Id;
                atheletes.AtheleteName = collegeAtheletes[i].Name;
                atheletes.Gender = collegeAtheletes[i].Gender;
                atheletes.School = collegeAtheletes[i].School;
                atheletes.AtheleteEmailaddress = collegeAtheletes[i].Emailaddress;
                atheletes.AtheleteDescription = collegeAtheletes[i].Description;
                atheletes.GPA = collegeAtheletes[i].GPA;
                atheletes.Social = collegeAtheletes[i].Social;
                atheletes.SAT = collegeAtheletes[i].SAT;
                atheletes.ACT = collegeAtheletes[i].ACT;
                atheletes.InstituteId = collegeAtheletes[i].InstituteId;
                atheletes.AtheleteProfilePicURL = collegeAtheletes[i].ProfilePicURL;
                atheletes.DOB = collegeAtheletes[i].DOB;
                atheletes.AtheleteAddress = collegeAtheletes[i].Address;
                atheletes.AtheleteFacebookId = collegeAtheletes[i].FacebookId;
                atheletes.AtheleteLatitude = collegeAtheletes[i].Latitude;
                atheletes.AtheleteLongitude = collegeAtheletes[i].Longitude;
                atheletes.FriendsInApp = collegeAtheletes[i].FriendsInApp;
                atheletes.SharesApp = collegeAtheletes[i].SharesApp;
                atheletes.AtheleteAverageRating = collegeAtheletes[i].AverageRating;
                atheletes.AcademicLevel = collegeAtheletes[i].AcademicLevel;
                atheletes.YearLevel = collegeAtheletes[i].YearLevel;
                atheletes.Class = collegeAtheletes[i].Class;
                atheletes.FriendsRate = collegeAtheletes[i].FriendsRate;
                atheletes.ShareRate = collegeAtheletes[i].ShareRate;
                atheletes.AtheleteSocialRate = collegeAtheletes[i].SocialRate;
                atheletes.AcademicRate = collegeAtheletes[i].AcademicRate;
                atheletes.AtheleteAtheleteRate = collegeAtheletes[i].AtheleteRate;
                atheletes.AtheleteCoverPicURL = collegeAtheletes[i].CoverPicURL;
                atheletes.AdType = collegeAtheletes[i].AdType;
                atheletes.Premium = collegeAtheletes[i].Premium;
                atheletes.Height = collegeAtheletes[i].Height;
                atheletes.Height2 = collegeAtheletes[i].Height2;
                atheletes.HeightType = collegeAtheletes[i].HeightType;
                atheletes.Weight = collegeAtheletes[i].Weight;
                atheletes.WeightType = collegeAtheletes[i].WeightType;
                atheletes.WingSpan = collegeAtheletes[i].WingSpan;
                atheletes.WingSpanType = collegeAtheletes[i].WingSpanType;
                atheletes.ShoeSize = collegeAtheletes[i].ShoeSize;
                atheletes.ShoeSizeType = collegeAtheletes[i].ShoeSizeType;
                atheletes.ClubName = collegeAtheletes[i].ClubName;
                atheletes.CoachName = collegeAtheletes[i].CoachName;
                atheletes.CoachEmailId = collegeAtheletes[i].CoachEmailId;
                atheletes.AllAmerican = collegeAtheletes[i].AllAmerican;
                atheletes.AcademicAllAmerican = collegeAtheletes[i].AcademicAllAmerican;
                atheletes.Captain = collegeAtheletes[i].Captain;
                atheletes.SwimswamTop = collegeAtheletes[i].SwimswamTop;
                atheletes.Status = collegeAtheletes[i].Status;
                atheletes.AtheleteType = collegeAtheletes[i].AtheleteType;
                atheletes.Active = collegeAtheletes[i].Active;
                atheletes.AtheleteCoachType = 0;              
                atheletes.AtheleteFavDate = collegeAtheletes[i].FavDate;
                atheletes.Liked = collegeAtheletes[i].Liked;
                atheletes.AtheleteLiked = collegeAtheletes[i].Liked;
                atheletes.AtheleteInvitationDate = collegeAtheletes[i].InvitationDate;
                List<AthleteEventDTO> atheleteEventDto;
                atheleteEventDto = UsersDal.GetAthleteBesttimes(collegeAtheletes[i].Id);
                atheletes.AtheleteBesttimes = atheleteEventDto;
                atheletes.Major = collegeAtheletes[i].Major;
                atheletes.Phonenumber = collegeAtheletes[i].Phonenumber;
                atheletes.RightSwiped = collegeAtheletes[i].RightSwiped;
                atheletes.ChatCount = collegeAtheletes[i].ChatCount;
                atheletes.AP = collegeAtheletes[i].AP;
                atheletes.UserName = collegeAtheletes[i].UserName;
                atheletes.LoginType = collegeAtheletes[i].LoginType;
                atheletes.VerticalJump = collegeAtheletes[i].VerticalJump;
                atheletes.BroadJump = collegeAtheletes[i].BroadJump;
                atheletes.StateId = collegeAtheletes[i].StateId;
                atheletes.StateName = collegeAtheletes[i].StateName;
                atheletes.SchoolCoachName = collegeAtheletes[i].SchoolCoachName;
                atheletes.SchoolCoachEmail = collegeAtheletes[i].SchoolCoachEmail;
                atheletes.SchoolZipCode = collegeAtheletes[i].SchoolZipCode;
                atheletes.GapyearLevel = collegeAtheletes[i].GapyearLevel;
                atheletes.GapyearDescription = collegeAtheletes[i].GapyearDescription;
                atheletes.Country = collegeAtheletes[i].Country;
                atheletes.CountryId = collegeAtheletes[i].CountryId;
                atheletes.isProfileCompleted = collegeAtheletes[i].isProfileCompleted;
                atheletes.AthleteTransition = collegeAtheletes[i].AthleteTransition;
                atheletes._20YardShuttleRun = collegeAtheletes[i]._20YardShuttleRun;
                atheletes._60YardShuttleRun = collegeAtheletes[i]._60YardShuttleRun;
                atheletes.KneelingMedBallToss = collegeAtheletes[i].KneelingMedBallToss;
                atheletes.RotationalMedBallToss = collegeAtheletes[i].RotationalMedBallToss;
                atheletes.DeviceId = collegeAtheletes[i].DeviceId;
                atheletes.DeviceType = collegeAtheletes[i].DeviceType;
                collegeAtheleteCoaches.Add(atheletes);
            }
            return collegeAtheleteCoaches;

        }

        //private List<CoachDTO> RemoveDuplicates(List<CoachDTO> newList)
        //{
        //    List<CoachDTO> newDuplicateList = new List<CoachDTO>();
        //    for(int i = 0 ;i < newList.Count;i++)
        //    {
        //        string email = newList[i].Emailaddress;
        //        if (!newDuplicateList.Any(s => s.Emailaddress == email))
        //        {
        //            //Get the list 
        //            newDuplicateList.Add(newList[i]);
        //        }
        //    }

        //    return newDuplicateList;
        //}

        public List<CoachDTO> GetAdCoaches(List<AdDTO> ads, List<CoachDTO> coaches)
        {
            int i = 0;
         
            for(i = 0;i< ads.Count;i++)
            {
                CoachDTO coach = new CoachDTO();
                coach.Id = ads[i].Id;
                coach.Name = ads[i].Name;
                coach.ProfilePicURL = ads[i].ProfilePicURL;
                coach.Description = ads[i].Description;
                coach.CoverPicURL = ads[i].VideoURL;
                coach.AdType = ads[i].AdType; 
                coach.AreasInterested = "";
                coach.Address = "";
                coach.Emailaddress = "";
                coach.YearOfEstablish = 0;
                coach.NCAA = 0;
                coach.Conference = 0;
                coach.AdmissionStandard = 0;
                coach.AverageRating = 0;
                coach.ClassificationName = "";
                coach.State = "";
                coach.City = "";
                coach.Zip = "";
                coach.WebsiteURL = "";
                coach.FaidURL = "";
                coach.ApplURL = "";
                coach.Latitude = "";
                coach.Longitude = "";
                coach.EnrollmentNo = "";
                coach.ApplicationNo = "";
                coach.AdmissionNo = "";
                coach.FacebookId = "";
                coach.TotalCount = 0;
                coach.NoOfAthletesInApp = 0;
                coach.AcceptanceRate = 0;
                coach.NCAARate = 0;
                coach.ConferenceRate = 0;
                coach.SocialRate = 0;
                coach.AdmissionRate = 0;
                coach.AtheleteRate = 0;
                coach.Address = "";
                coach.Cost = 0;
                coach.MensTeam = 0;
                coach.WomenTeam = 0;
                coaches.Add(coach);
            }
            
            return coaches;
        }

        [HttpGet]
        public Utility.CustomResponse GetCoachFriends(string emailaddress, int offset, int max)
        {
           
            try
            {
                
                var atheletes = UsersDal.GetCoachFriends(emailaddress, offset, max);
               
                if (atheletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = atheletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse IsHeadCoach(int coachId, int universityId)
        {
           
            try
            {
                var results = UsersDal.IsHeadCoach(coachId, universityId);
                
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = results;
                    _result.Message = CustomConstants.Details_Get_Successfully;
              
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpPost]
        public Utility.CustomResponse UpdateCoachProfile(CoachProfileDTO coachProfileDto)
        {
           
            try
            {
                if (coachProfileDto.Description == "-1")
                {
                    coachProfileDto.Description = string.Empty;
                }

                if (coachProfileDto.Phonenumber == "-1")
                {
                    coachProfileDto.Phonenumber = string.Empty;
                }

                UsersDal.UpdateCoachProfile(coachProfileDto);
                _result.Status = Utility.CustomResponseStatus.Successful; 
                _result.Message = CustomConstants.Coach_Profile_Updated;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        //Get College Atheletes ForTeamRoaster 
        [HttpGet]
        public Utility.CustomResponse GetCollegeAtheletes(int instituteId,int atheleteId, int offset , int max)
        { 
            try
            { 
                var collegeAthletes = UsersDal.GetCollegeAtheletesForInstitute(instituteId, atheleteId, offset, max);
               
                if (collegeAthletes.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = collegeAthletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                } 
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        //Get College Coaches ForTeamRoaster 
        [HttpGet]
        public Utility.CustomResponse GetCollegeCoaches(int instituteId, int coachId, int offset, int max)
        { 
            try
            { 
                var collegeCoaches = UsersDal.GetCollegeCoachesForInstitute(instituteId, coachId, offset, max); 
                if (collegeCoaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = collegeCoaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }
                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        //ForTeamRoaster adding from AddMember
        [HttpGet]
        public Utility.CustomResponse AddAtheleteToTeamRoaster(int atheleteId, int instituteId, string atheleteEmail, string coachEmail)
        {
            Utility.CustomResponse result = new Utility.CustomResponse();
            MailMessage msg = new MailMessage();
            MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["ReqEmailAddress"]);
            try
            {

                //Send Mail To athelete 
              
                try
                {

                    StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/TeamRosterCoachToAthelete.html"));
                    string readFile = reader.ReadToEnd();
                    string myString;
                    myString = readFile;
                    myString = myString.Replace("$$AtheleteEmail$$", atheleteEmail);
                    myString = myString.Replace("$$AtheleteId$$", atheleteId.ToString());
                    myString = myString.Replace("$$InstituteId$$", instituteId.ToString());
                    myString = myString.Replace("$$CoachEmail$$", coachEmail);
                    string baseURL = ConfigurationManager.AppSettings["BaseURL"];
                    myString = myString.Replace("$$BaseURL$$", baseURL);

                    //msg.From = fromMail;
                    //msg.To.Add(new MailAddress("" + atheleteEmail + ""));
                    //msg.Subject = " Athelete Acceptance for an University";
                    //msg.Body = myString;
                    //msg.IsBodyHtml = true;
                    //SmtpClient smtpMail = new SmtpClient();
                    //smtpMail.Host = ConfigurationManager.AppSettings["ReqEmailHost"];
                    //smtpMail.Port = Convert.ToInt32(ConfigurationManager.AppSettings["ReqPort"]);
                    //smtpMail.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["ReqEnableSSL"]);
                    //smtpMail.UseDefaultCredentials = false;
                    //smtpMail.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["ReqEmailAddress"], ConfigurationManager.AppSettings["ReqPassword"]);
                    //smtpMail.Timeout = 1000000;
                    //smtpMail.Send(msg);

                    SendGridMailModel sendGridModel = new SendGridMailModel();
                    sendGridModel.toMail = new List<string>();
                    sendGridModel.toMail.Add(atheleteEmail);
                    sendGridModel.fromMail = fromMail.ToString();
                    sendGridModel.fromName = "Lemonaid";
                    sendGridModel.subject = "Athelete Acceptance for an University";
                    sendGridModel.Message = myString;
                    SendGridSMTPMail.SendEmail(sendGridModel);


                    EmailLogModel emaillog = new EmailLogModel();
                    emaillog.AddEmailLog(fromMail.ToString(), atheleteEmail, msg.Subject, "Mail", 1);

                    result.Status = Utility.CustomResponseStatus.Successful;
                    result.Message = CustomConstants.Mail_Sent;

                    return result;
                }
                catch (Exception ex)
                {
                    EmailLogModel emaillog = new EmailLogModel();
                    emaillog.AddEmailLog(fromMail.ToString(), atheleteEmail, msg.Subject, "Mail", 0);
                    result.Status = Utility.CustomResponseStatus.Exception;
                    result.Message = ex.Message;
                    return result;
                }

                //int Id = UsersDAL.UpdateAthleteInstitution(AtheleteId, InstituteId);
                //Result.Status = Utility.CustomResponseStatus.Successful;
                //Result.Response = Id;
                //Result.Message = CustomConstants.TeamRoaster_Added;


            }
            catch (Exception ex)
            {
                result.Status = Utility.CustomResponseStatus.Exception;
                result.Message = ex.Message;
                return result;
            }

        }
         
         
      
        [HttpGet]
        public Utility.CustomResponse GetMutualSeniorInstituteAtheletes(int instituteId)
        { 
            try
            {
                var seniorAthletes = UsersDal.GetMutualSeniorInstituteAtheletes(instituteId);
                if (seniorAthletes.Count > 0)
                {
                    //for (int i = 0; i < seniorAthletes.Count; i++)
                    //{
                    //    seniorAthletes[i].AtheleteBesttimes = new List<AthleteEventDTO>();
                    //    seniorAthletes[i].AtheleteVideos = new List<VideoDTO>();
                    //    seniorAthletes[i].MeasurementCount = 0;
                    //}
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = seniorAthletes;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.No_Records_Roster;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpGet]
        public Utility.CustomResponse GetUniversityProfile(int instituteId)
        {
           
            try
            {
                var unv = UsersDal.GetUniversityProfile(instituteId);
                if (unv.Id > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = unv;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetUniversityForCoachLogin(string emailaddress) 
        {
           
            try
            {
                var coaches = UsersDal.GetCoaches("ALL", 0, 5000);
                var address = new MailAddress(emailaddress);
                string emailHost = address.Host;
                var mainunihost = emailHost.Substring(emailHost.IndexOf('.') + 1);
                if (mainunihost.Contains("."))
                {
                    emailHost = mainunihost;
                }


                var university = GetMatchedUniversity(emailHost, coaches);

                if (university.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = university;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.Notify_University;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        private List<CoachDTO> GetMatchedUniversity(string emailHost, List<CoachDTO> coaches)
        {
           var univ = new List<CoachDTO>();
            string coachEmailHost;
            for (int i = 0; i < coaches.Count; i++)
            {
                CoachDTO coach;
                MailAddress coachemail = new MailAddress(coaches[i].Emailaddress);
                coachEmailHost = coachemail.Host;
                var mainunihost = coachEmailHost.Substring(coachEmailHost.IndexOf('.') + 1);
                if (mainunihost.Contains("."))
                {
                    coachEmailHost = mainunihost;
                }

                if(emailHost == coachEmailHost)
                {
                    coach = coaches[i];
                    univ.Add(coach);
                    
                }
            }
                return univ;
        }

        [HttpGet]
        public Utility.CustomResponse GetAllCoachesByLocationWithSearch(string emailaddress, double latitude, double longitude, int offset, int max, int ncaa, int conference, int admission, int locationType, string description, string searchText) // Athlete email address
        {
            AthleteController athleteContr = new AthleteController();
        
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }

               var coaches = UsersDal.GetAllCoachesPagingWithSearch(emailaddress, offset, max, ncaa, conference, admission, description, searchText);

              var newList = new List<CoachDTO>();

                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = coaches;

                }
                else
                    if (locationType == 2)
                    {
                        for (int i = 0; i < coaches.Count; i++)
                        {
                            double athleteLatitude = (coaches[i].Latitude == "") ? 0.0 : Convert.ToDouble(coaches[i].Latitude);
                            double athleteLongitude = (coaches[i].Longitude == "") ? 0.0 : Convert.ToDouble(coaches[i].Longitude);
                            var distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude);

                            if (distance <= 100)
                            {
                                newList.Add(coaches[i]);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            for (int i = 0; i < coaches.Count; i++)
                            {
                                double athleteLatitude = (coaches[i].Latitude == "") ? 0.0 : Convert.ToDouble(coaches[i].Latitude);
                                double athleteLongitude = (coaches[i].Longitude == "") ? 0.0 : Convert.ToDouble(coaches[i].Longitude);
                                double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude );

                                if (distance <= 5)
                                {
                                    newList.Add(coaches[i]);
                                }
                            }
                        }




                if (newList.Count > 0)
                {
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newList = GetAdCoaches(ads, newList);

                }
                //Get College atheletes and coaches
                List<AthleteDTO> collegeAtheletes = UsersDal.GetCollegeAtheletes(searchText, emailaddress);
                int totalcount = collegeAtheletes.Count + newList.Count;

                for (int i = 0; i < collegeAtheletes.Count; i++)
                {
                    collegeAtheletes[i].TotalCount = totalcount;
                }


                List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches = new List<CollegeAtheleteCoachesDTO>();
                if (newList.Count > 0)
                {
                    for (int i = 0; i < newList.Count; i++)
                    {
                        newList[i].TotalCount = totalcount;
                    }
                    collegeAtheleteCoaches = AddCoachestoList(newList, collegeAtheleteCoaches);
                }
                collegeAtheleteCoaches = AddAtheletestoList(collegeAtheletes, collegeAtheleteCoaches);
               // List<QuestionaireDTO> questionaireDto = UsersDal.GetQuestionaire(emailaddress);


                IEnumerable<CollegeAtheleteCoachesDTO> enumcollegeAtheleteCoaches = collegeAtheleteCoaches.Skip(offset).Take(max - offset);
                collegeAtheleteCoaches = enumcollegeAtheleteCoaches.ToList();
                //collegeAtheleteCoaches = AddQuestionairetoList(questionaireDto, collegeAtheleteCoaches);

                if (collegeAtheleteCoaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = collegeAtheleteCoaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        [HttpGet]
        public Utility.CustomResponse GetAllCoachesByLocationWithSearch_V3(string emailaddress, double latitude, double longitude, int offset, int max, int ncaa, int conference, int admission, int locationType, string description, string searchText) // Athlete email address
        {
            AthleteController athleteContr = new AthleteController();
           
            try
            {
                if (description == "-1")
                {
                    description = string.Empty;
                }

               var caches = UsersDal.GetAllCoachesPagingWithSearch(emailaddress, offset, max, ncaa, conference, admission, description, searchText);

               var newList = new List<CoachDTO>();

                //Location Type 
                //Type =  1 : ALL , 2 - Current Location , 3 - Custom Location

                if (locationType == 1)
                {
                    newList = caches;

                }
                else
                    if (locationType == 2)
                    {
                        for (int i = 0; i < caches.Count; i++)
                        {
                            double athleteLatitude = (caches[i].Latitude == "") ? 0.0 : Convert.ToDouble(caches[i].Latitude);
                            double athleteLongitude = (caches[i].Longitude == "") ? 0.0 : Convert.ToDouble(caches[i].Longitude);
                            double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude );

                            if (distance <= 100)
                            {
                                newList.Add(caches[i]);
                            }
                        }
                    }
                    else
                        if (locationType == 3)
                        {
                            for (int i = 0; i < caches.Count; i++)
                            {
                                double athleteLatitude = (caches[i].Latitude == "") ? 0.0 : Convert.ToDouble(caches[i].Latitude);
                                double athleteLongitude = (caches[i].Longitude == "") ? 0.0 : Convert.ToDouble(caches[i].Longitude);
                                double distance = athleteContr.DistanceTo(latitude, longitude, athleteLatitude, athleteLongitude );

                                if (distance <= 5)
                                {
                                    newList.Add(caches[i]);
                                }
                            }
                        }




                if (newList.Count > 0)
                {
                    List<AdDTO> ads = DAL2V5.GetDisplayCards(0);
                    newList = GetAdCoaches(ads, newList);

                }
                //Get College atheletes and coaches
                List<AthleteDTO> collegeAtheletes = UsersDal.GetCollegeAtheletes(searchText, emailaddress);
                int totalcount = collegeAtheletes.Count + newList.Count;

                for (int i = 0; i < collegeAtheletes.Count; i++)
                {
                    collegeAtheletes[i].TotalCount = totalcount;
                }


                List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches = new List<CollegeAtheleteCoachesDTO>();
                if (newList.Count > 0)
                {
                    for (int i = 0; i < newList.Count; i++)
                    {
                        newList[i].TotalCount = totalcount;
                    }
                    collegeAtheleteCoaches = AddCoachestoList(newList, collegeAtheleteCoaches);
                }
                collegeAtheleteCoaches = AddAtheletestoList(collegeAtheletes, collegeAtheleteCoaches);
                List<QuestionaireDTO> questionaireDto = UsersDal.GetQuestionaire(emailaddress);
               

                IEnumerable<CollegeAtheleteCoachesDTO>  enumcollegeAtheleteCoaches = collegeAtheleteCoaches.Skip(offset).Take(max - offset);
                collegeAtheleteCoaches = enumcollegeAtheleteCoaches.ToList();
                collegeAtheleteCoaches = AddQuestionairetoList(questionaireDto, collegeAtheleteCoaches); 

                if (collegeAtheleteCoaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = collegeAtheleteCoaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            } 
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        public List<CollegeAtheleteCoachesDTO> AddQuestionairetoList(List<QuestionaireDTO> questionaire , List<CollegeAtheleteCoachesDTO> collegeAtheleteCoaches)
        {
            for (int i = 0; i < questionaire.Count; i++)
            {
                CollegeAtheleteCoachesDTO atheletes = new CollegeAtheleteCoachesDTO();
                atheletes.ProfilePicURL = string.Empty;
                atheletes.Name = string.Empty; 
                atheletes.QuestionId = questionaire[i].QuestionId;
                atheletes.Question = questionaire[i].Question;
                atheletes.AdType = 3;
                collegeAtheleteCoaches.Add(atheletes);
            }
            return collegeAtheleteCoaches;
        }

        [HttpGet]
        public Utility.CustomResponse GetCoachesWithSearch(string emailaddress, int offset, int max,string searchText)
        {
            
            try
            { 
                var coaches = UsersDal.GetCoachesWithSearch(emailaddress, offset, max, searchText);
               
                if (coaches.Count > 0)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Response = coaches;
                    _result.Message = CustomConstants.Details_Get_Successfully;
                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.NoRecordsFound;
                }

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ForgotPassword(string userName)
        {
           
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {

                string code = UsersDal.GetCoachUserExists(userName);
                if (code != string.Empty)
                {
                    string token = code;

                    //need to send a mail to user

                    if (SendMailForForgotPassword(userName, token) == 0)
                    {
                        res.Status = Utility.CustomResponseStatus.Successful;
                        res.Message = CustomConstants.ForgotPassword_successfully;
                    }
                    else
                    {
                        res.Status = Utility.CustomResponseStatus.UnSuccessful;
                        res.Message = Message;

                        //"Failed to Send Email, Please try again later";
                    }
                }
                else
                {
                    res.Status = Utility.CustomResponseStatus.UnSuccessful;
                    res.Message = CustomConstants.User_Does_Not_Exist;
                }
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }

            return res;
        }

        [HttpGet]
        public Utility.CustomResponse ResetPassword(string userName, string token, string confirmPassword)
        {
            
            try
            {
                //isReset = WebSecurity.ResetPassword(token, confirmPassword);
                string email = UsersDal.UpdateCoachPassword(userName,token, confirmPassword);
                if (email != string.Empty)
                {
                    _result.Status = Utility.CustomResponseStatus.Successful;
                    _result.Message = CustomConstants.ResetPassword;

                }
                else
                {
                    _result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    _result.Message = CustomConstants.ResetPassword_Failed;
                }


            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;// javaScriptSerializer.Serialize(_result);
            }
            return _result;
        }

        public static int SendMailForForgotPassword(string username, string token)
        {
            SupportMailDTO support = new SupportMailDTO();
            MailMessage msg = new MailMessage();
            string apiUrl = string.Empty;
            try
            {
               
                support = UsersDal.GetSupportMails("RequiredEmail"); 

                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ForgotPassword.html"));
                string readFile = reader.ReadToEnd();
                var myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                myString = myString.Replace("$$Token$$", token);
                string baseURL = ConfigurationManager.AppSettings["BaseURL"];
                myString = myString.Replace("$$BaseURL$$", baseURL);

                SendGridMailModel sendGridModel = new SendGridMailModel();
                sendGridModel.toMail = new List<string>();
                sendGridModel.toMail.Add(username);
                sendGridModel.fromMail = support.EmailAddress;
                sendGridModel.fromName = "Lemonaid";
                sendGridModel.subject = " Forgot Password";
                sendGridModel.Message = myString;
                SendGridSMTPMail.SendEmail(sendGridModel);

                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(support.EmailAddress, username, msg.Subject, "Mail", 1);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                EmailLogModel emaillog = new EmailLogModel();
                emaillog.AddEmailLog(support.EmailAddress, username, msg.Subject, "Mail", 0);
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }


        [HttpGet]
        public Utility.CustomResponse CoachProfile(int coachId)
        {
         
            try
            {
                var coach = UsersDal.GetCoachProfile(coachId);

                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Response = coach;
                _result.Message = CustomConstants.Details_Get_Successfully;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }

        [HttpPost]
        public Utility.CustomResponse UpdateCoachProfile_V2(CoachProfileDTO coachProfileDto)
        {
            
            try
            {
                if (coachProfileDto.Description == "-1")
                {
                    coachProfileDto.Description = string.Empty;
                }

                if (coachProfileDto.Phonenumber == "-1")
                {
                    coachProfileDto.Phonenumber = string.Empty;
                }

                UsersDal.UpdateCoachProfile_v2(coachProfileDto);
                _result.Status = Utility.CustomResponseStatus.Successful;
                _result.Message = CustomConstants.Coach_Profile_Updated;

                return _result;
            }
            catch (Exception ex)
            {
                _result.Status = Utility.CustomResponseStatus.Exception;
                _result.Message = ex.Message;
                return _result;
            }

        }


        //[HttpGet]
        //public Utility.CustomResponse NotifyCoachUniversity(int athleteId, int coachId)
        //{
        //    string Message = string.Empty;
        //    SupportMailDTO support = new SupportMailDTO();
        //    MailMessage Msg = new MailMessage();
        //    try
        //    {
        //        support = UsersDal.GetSupportMails("SupportEmail");

        //        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailSending.html"));
        //        string readFile = reader.ReadToEnd();
        //        string myString = "";
        //        myString = readFile;
              
        //        myString = myString.Replace("$$Right$$", Rightswipes);


        //        MailAddress fromMail = new MailAddress(support.EmailAddress);
        //        Msg.From = fromMail;
        //        Msg.To.Add(new MailAddress("" + Emailaddress + ""));
        //        Msg.Subject = " Sweetness!!! " + Rightswipes + " athletes showed interest in your school on LemonAid";
        //        Msg.Body = myString.ToString();
        //        Msg.IsBodyHtml = true;
        //        var smtpMail = new SmtpClient();
        //        smtpMail.Host = support.Host;
        //        smtpMail.Port = Convert.ToInt32(support.Port);
        //        smtpMail.EnableSsl = Convert.ToBoolean(support.SSL);
        //        smtpMail.UseDefaultCredentials = false;
        //        smtpMail.Credentials = new System.Net.NetworkCredential(support.EmailAddress, support.Password);
        //        smtpMail.Timeout = 1000000;
        //        smtpMail.Send(Msg);
        //        _result.Status = Utility.CustomResponseStatus.Successful;
        //        _result.Message = CustomConstants.Notification_Sent;
        //        return _result;

        //    }
        //    catch (SmtpException exx)
        //    {
        //        EmailDTO email = new EmailDTO();
        //        email.From = support.EmailAddress;
        //        email.To = Emailaddress;
        //        email.Status = 2;
        //        email.Subject = Msg.Subject;
        //        UsersDal.AddEmailLog(email);
        //        Message = exx.Message;
        //        return 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        Message = ex.Message;
        //        return 1;
        //    }
        //}



    }
}
